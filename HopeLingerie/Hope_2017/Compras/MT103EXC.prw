#include 'protheus.ch'
#include 'parmtype.ch'

user function MT103EXC()

Local lRet

	if __cuserid $ GETMV("HP_DENTEXC")

		lRet := .T.

	else 

		msgAlert("Nao � permitida exclus�o do documento de Entrada, Favor falar com o setor Fiscal ","MT103EXC") 	
		lRet := .F. 
	endif 
	
return lRet 