#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �  MT110GRV� Autor � Roberto Le�o          � Data � 21/06/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Ponto de entrada apos grava��o da SC.                      ���
�������������������������������������������������������������������������Ĵ��
���Uso       � MP8                                                        ���
���          � Necessario Criar Campo                                     ���
���          � Nome			Tipo	Tamanho	Titulo			OBS           ���
���          � C1_CODAPROV   C         6    Cod Aprovador                 ���
���          �                                                            ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function MT110GRV()

Local aArea     := GetArea()
Local cRet := .T.
Local cTotItem := Strzero(Len(aCols),4)	  
Local _cNumSC1 := SC1->C1_NUM
Local lGrava  := .T.
Local nPoRat  := AScan(aHeader,{|x|RTrim(x[2])=="C1_RATEIO"}) 
Local nPoCC   := AScan(aHeader,{|x|RTrim(x[2])=="C1_CC"}) 
Local nPoPro  := AScan(aHeader,{|x|RTrim(x[2])=="C1_PRODUTO"})    
LOCAL nX := 0 
Private cCodUser 	:= __cUserId
 
//GRAVA O NOME DA FUNCAO NA Z03
//U_CFGRD001(FunName())

//��������������������������������������������������������Ŀ
//�Envia Workflow para aprovacao da Solicitacao de Compras �
//����������������������������������������������������������    
dbSelectArea("SY1")	// Compradores
dbSetOrder(3)
IF dbSeek(xFilial("SY1")+cCodUser) .AND. (INCLUI .OR. ALTERA)
	lCompra  := .T.   
	RECLOCK('SC1',.F.)
	SC1->C1_APROV := 'L'
	SC1->(MSUNLOCK())
ELSE
	lCompra  := .F.
ENDIF 
 

DBSELECTAREA('SAI')
DBSETORDER(1)	
IF DBSEEK(XFILIAL('SAI')+cCodUser) .AND. (INCLUI .OR. ALTERA)
	lAprov  := .T.   
	RECLOCK('SC1',.F.)
	SC1->C1_APROV := 'L'
	SC1->(MSUNLOCK())
ELSE
	lAprov  := .F.
ENDIF


If (INCLUI .OR. ALTERA) .AND. SC1->C1_ITEM == cTotItem .and. SC1->C1_TPOP = "F" .AND. !lAprov //Verifica se e Inclusao ou Alteracao da Solicitacao//Verifica se e Inclusao ou Alteracao da Solicitacao
	MsgRun("Enviando Workflow para Aprovador da Solicita��o, Aguarde...","",{|| CursorWait(), U_TUMIW005(SC1->C1_NUM) ,CursorArrow()})
EndIf

dbSelectArea('SC1')
dbSetOrder(1)
If dbSeek(xFilial("SC1")+_cNumSC1)
	RecLock("SC1",.F.)
	SC1->C1_APROV := "B"
	SC1->(MsUnlock())
EndIf
RestArea(aArea)

Return 
