#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"

User Function  MT110TOK()

Local nPosPrd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C1_PRODUTO'})
Local nPosDtI    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C1_XDTINI'})
Local nPosDtE    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C1_DATPRF'})
Local lValido := .T.
Local _i

_area := GetArea()

dbSelectArea('SB1')
_areaSB1 := GetArea()

For _i := 1 to len(aCols)
	dbSelectArea('SB1')
	dbSetOrder(1)
	DbSeek(xfilial("SB1")+aCols[_i,nPosPrd])
	
	If SB1->B1_TIPE = "D"
		_lead := SB1->B1_PE
	ElseIf SB1->B1_TIPE = "S"
		_lead := SB1->B1_PE*7
	ElseIf SB1->B1_TIPE = "M"
		_lead := SB1->B1_PE*30
	ElseIf SB1->B1_TIPO = "A"
		_lead := SB1->B1_PE*365
	Else 
		_lead := 1
	Endif
	
	acols[_i,nPosDtI] := aCols[_i,nPosDtE]-_lead

Next


IF LEN(aCPISCX) > 0 
	lValido := .T.
ELSE 
	Alert("Favor, informar o centro de custo no Rateio")
	lValido := .F.
ENDIF


RestArea(_areaSB1)
RestArea(_area)

Return(lValido) 