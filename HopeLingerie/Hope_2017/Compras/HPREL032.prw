#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL032
Performance de atendimento por SKU
@author Weskley Silva
@since 13/04/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/'

user function HPREL032()

Local aRet := {}
Local aPerg := {}
Private oReport
Private cPergCont	:= 'HPREL032' 
	

//AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

return 

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 13 de Abril de 2018
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'CPE', 'PEDIDO X NOTA FISCAL', , {|oReport| ReportPrint( oReport ), 'PEDIDO X NOTA FISCAL' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PEDIDO X NOTA FISCAL', { 'CPE', 'SC7','SD1','SA2'})
			
	TRCell():New( oSection1, 'PEDIDO'			    ,'PFF', 		'PEDIDO',		  					"@!"				        ,10)
	TRCell():New( oSection1, 'QTDE_PEDIDO'	   	    ,'PFF', 		'QTDE_PEDIDO',      	   			"@E 999,99"					,10)
	TRCell():New( oSection1, 'VLR_UNIT_PEDIDO'		,'PFF', 		'VLR_UNIT_PEDIDO',			   		"@E 999,99"					,10)
	TRCell():New( oSection1, 'VLRTOT_PEDIDO'		,'PFF', 		'VLRTOT_PEDIDO',				   	"@E 999,99"					,10)
	TRCell():New( oSection1, 'NF'					,'PFF', 		'NF',			   					"@!"						,10)
	TRCell():New( oSection1, 'QUANT_NF'			    ,'PFF', 		'QUANT_NF',						    "@E 999,99"					,10)
	TRCell():New( oSection1, 'VLRUNIT_NF'			,'PFF', 		'VLRUNIT_NF',		   				"@E 999,99"					,10)
	TRCell():New( oSection1, 'VLRTOT_NF'			,'PFF', 		'VLRTOT_NF',			   			"@E 999,99"					,10)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 13 de Abril de 2018
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("CPE") > 0
		CPE->(dbCloseArea())
	Endif
	
    cQuery := " SELECT C7_NUM AS PEDIDO, "
	cQuery += " C7_QUANT AS QTDE_PEDIDO, "
	cQuery += " C7_PRECO AS VLR_UNIT_PEDIDO, "
	cQuery += " C7_TOTAL AS VLRTOT_PEDIDO, "
	cQuery += " D1_DOC AS NF, "
	cQuery += " D1_QUANT AS QUANT_NF, "
	cQuery += " D1_VUNIT AS VLRUNIT_NF, "
	cQuery += " D1_TOTAL AS VLRTOT_NF "	
	cQuery += " A2_COD AS COD_FORNECE,"
	cQuery += " FROM SC7010 "  
	cQuery += " JOIN SA2010 ON A2_COD = C7_FORNECE AND A2_LOJA = C7_LOJA AND SA2010.D_E_L_E_T_ = '' "
	cQuery += " JOIN SD1010 ON D1_FORNECE = C7_FORNECE AND D1_PEDIDO = C7_NUM AND D1_LOJA = C7_LOJA AND D1_COD = C7_PRODUTO AND SD1010.D_E_L_E_T_ = '' "
	cQuery += " WHERE SC7010.D_E_L_E_T_ = '' "
	cQuery += " AND '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"'  "

	cQuery += " GROUP BY C7_NUM,C7_QUANT,C7_PRECO,C7_TOTAL,D1_DOC,D1_QUANT,D1_VUNIT,D1_TOTAL " 

	TCQUERY cQuery NEW ALIAS CPE

	While PFF->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("EMISSAO"):SetValue(PFF->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")

		oSection1:Cell("PEDIDO"):SetValue(PFF->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(PFF->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(PFF->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COR"):SetValue(PFF->COR)
		oSection1:Cell("COR"):SetAlign("LEFT")
			
		oSection1:Cell("TAMANHO"):SetValue(PFF->TAMANHO)
		oSection1:Cell("TAMANHO"):SetAlign("LEFT")
				
		oSection1:Cell("COLECAO"):SetValue(PFF->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
				
		oSection1:Cell("SUBCOLECAO"):SetValue(PFF->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("SITUACAO_PROD"):SetValue(PFF->SITUACAO_PROD)
		oSection1:Cell("SITUACAO_PROD"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PEDIDO"):SetValue(PFF->QTDE_PEDIDO)
		oSection1:Cell("QTDE_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_ENTREGUE"):SetValue(PFF->QTDE_ENTREGUE)
		oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PERDA"):SetValue(PFF->QTDE_PERDA)
		oSection1:Cell("QTDE_PERDA"):SetAlign("LEFT")
						
		oSection1:Cell("SALDO"):SetValue(PFF->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_TOTAL"):SetValue(PFF->VALOR_TOTAL)
		oSection1:Cell("VALOR_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE"):SetValue(PFF->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(PFF->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("COND_PGTO"):SetValue(PFF->COND_PGTO)
		oSection1:Cell("COND_PGTO"):SetAlign("LEFT")
		
		oSection1:Cell("POLITICA"):SetValue(PFF->POLITICA)
		oSection1:Cell("POLITICA"):SetAlign("LEFT")
		
		
		oSection1:PrintLine()
		
		PFF->(DBSKIP()) 
	enddo
	PFF->(DBCLOSEAREA())
Return( Nil )
