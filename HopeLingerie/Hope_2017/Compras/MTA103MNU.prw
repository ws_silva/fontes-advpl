#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"

User Function MTA103MNU()

aAdd(aRotina,{ "Selo Fiscal", "U_HSELO", 0 , 2, 0, .F.})
aAdd(aRotina,{ "Endere�ar"  , "U_HENDE", 0 , 2, 0, .F.})


Return()

User Function HSELO

Local oButton1
Local oButton2
Local oFont1 := TFont():New("MS Sans Serif",,022,,.T.,,,,,.F.,.F.)
Local oGet1
Local cGet1 := Space(16)
Local oSay1
Static oDlg

	DEFINE MSDIALOG oDlg TITLE "Selo Fiscal" FROM 000, 000  TO 250, 600 COLORS 0, 16777215 PIXEL

    @ 099, 248 BUTTON oButton1 PROMPT "Ok" 			SIZE 037, 012 ACTION (u_atuselo(cGet1,SF1->F1_DOC,SF1->F1_SERIE,SF1->F1_FORNECE,SF1->F1_LOJA),oDlg:End()) OF oDlg PIXEL
    @ 099, 204 BUTTON oButton2 PROMPT "Cancela" 	SIZE 037, 012 ACTION Close(oDlg) OF oDlg PIXEL

    @ 018, 013 SAY oSay1 PROMPT "Informe o Selo Fiscal:" SIZE 172, 019 OF oDlg FONT oFont1 COLORS 0, 16777215 PIXEL
    @ 054, 013 MSGET oGet1 VAR cGet1 SIZE 266, 010 OF oDlg COLORS 0, 16777215 PIXEL

	ACTIVATE MSDIALOG oDlg CENTERED

Return

User Function AtuSelo(_par1,_par2,_par3,_par4,_par5)
	_qry := "Update "+RetSqlname("SF1")+" Set F1_XSELO = '"+_par1+"' where F1_DOC = '"+_par2+"' and F1_SERIE = '"+_par3+"' and F1_FORNECE = '"+_par4+"' and F1_LOJA = '"+_par5+"' and F1_FILIAL = '"+xfilial("SF1")+"' "
	TcSqlExec(_qry)
	
	_qry1 := "UPDATE "+RetSqlName("SF3")+" Set F3_XSELO = '"+_par1+"' WHERE F3_NFISCAL = '"+_par2+"' AND F3_SERIE = '"+_par3+"' AND F3_CLIEFOR = '"+_par4+"' AND F3_LOJA = '"+_par5+"' AND F3_FILIAL = '"+xFilial("SF3")+"' "
	TcSqlExec(_qry1)
	
Return

User Function HENDE()

_area := GetArea()

_doc   := SF1->F1_DOC
_Serie := SF1->F1_SERIE
_forne := SF1->F1_FORNECE
_loja  := SF1->F1_LOJA
_Tipo  := AllTrim(SF1->F1_TIPO)

_qry := "Select * from "+RetSqlName("SDA")+" where D_E_L_E_T_ = '' and DA_DOC = '"+_doc+"' and DA_SERIE = '"+_serie+"' and DA_SALDO > 0 "
_Qry := ChangeQuery(_qry)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSDA",.T.,.T.)

DbSelectArea("TMPSDA")
DbGoTop()

If !EOF()
	While !EOF()
		_acab  := {}
		_aitem := {}
		
			Aadd(_acab, {"DA_PRODUTO"		, TMPSDA->DA_PRODUTO		,NIL})
			Aadd(_acab, {"DA_LOTECTL"		, TMPSDA->DA_LOTECTL		,NIL})
			Aadd(_acab, {"DA_NUMLOTE"		, TMPSDA->DA_NUMLOTE		,NIL})
			Aadd(_acab, {"DA_LOCAL"			, TMPSDA->DA_LOCAL		,NIL})
			Aadd(_acab, {"DA_DOC"			, TMPSDA->DA_DOC			,NIL})
			Aadd(_acab, {"DA_SERIE"			, TMPSDA->DA_SERIE		,NIL})
			Aadd(_acab, {"DA_CLIFOR"		, TMPSDA->DA_CLIFOR		,NIL})
			Aadd(_acab, {"DA_LOJA"			, TMPSDA->DA_LOJA			,NIL})
			Aadd(_acab, {"DA_NUMSEQ"		, TMPSDA->DA_NUMSEQ		,NIL})

			DbSelectArea("SDB")
			DbSetOrder(1)
			DbSeek(xfilial("SDB")+TMPSDA->DA_PRODUTO+TMPSDA->DA_LOCAL+TMPSDA->DA_NUMSEQ+TMPSDA->DA_DOC)

			Aadd(_aitem,{"DB_ITEM"			, "0001"				,NIL})
			Aadd(_aitem,{"DB_ESTORNO"		, " "					,NIL})
			Aadd(_aitem,{"DB_DATA"			, ddatabase			,NIL})
			Aadd(_aitem,{"DB_QUANT"			, TMPSDA->DA_SALDO	,NIL})
			Aadd(_aitem,{"DB_LOTECTL"		, SDB->DB_LOTECTL		,NIL})
			Aadd(_aitem,{"DB_NUMLOTE"		, SDB->DB_NUMLOTE		,NIL})
			If _Tipo == "D"
				Aadd(_aitem,{"DB_LOCALIZ"	, "DEV"				,NIL})
			Else
				Aadd(_aitem,{"DB_LOCALIZ"	, "PADRAO"				,NIL})
			End
			Aadd(_aitem,{"DB_PRODUTO"		, SDB->DB_PRODUTO		,NIL})
			Aadd(_aitem,{"DB_LOCAL"			, SDB->DB_LOCAL		,NIL})
			Aadd(_aitem,{"DB_DOC"			, SDB->DB_DOC			,NIL})
			Aadd(_aitem,{"DB_SERIE"			, SDB->DB_SERIE		,NIL})
			Aadd(_aitem,{"DB_CLIFOR"			, SDB->DB_CLIFOR		,NIL})
			Aadd(_aitem,{"DB_LOJA"			, SDB->DB_LOJA		,NIL})
			Aadd(_aitem,{"DB_NUMSEQ"			, SDB->DB_NUMSEQ		,NIL})
			Aadd(_aitem,{"DB_ATUEST"			, "S"					,NIL})

			lmsErroAuto := .F.
			msExecAuto({|X,Y,Z|MATA265(X,Y,Z)},_acab,{_aitem},3)

			if lMsErroAuto      // variavel do sigaauto indicando erro na operacao
				MostraErro() //MS 14.01.04
			Endif

		DbSelectArea("TMPSDA")
		DbSkip()
	End
	Alert("Endere�amento efetuado!")
Else
	Alert("Nota Fiscal j� endere�ada")
Endif
DbCloseArea()

RestArea(_area)
Return