// #########################################################################################
// Projeto: Hope
// Modulo : WS  -  Produtos Para Loja Hope Resort
// Fonte  : hFATP001
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 20/11/17 | Daniel Souza-Hope | Cadastro de Produtos a serem disponibilizado a Loja
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

user function HPCIGPRO01()
	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	local cAlias  := "ZAT"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	axCadastro(cAlias, "Cadastro de Produtos Disponivel para Loja", cVldExc, cVldAlt)
	
return
