#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH" 
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'       
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"

user function HPREL013()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\TEMP\"				
	Local oFont1 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont2 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont3 	:= TFont():New( "Arial",,16,,.T.,,,,,  .F. )
	Local cFilePrint := ""
	Local nLin   := 100
	Local nTmLin := 10
	Local nSalto := 10
	Local cLinha :=''
	
	U_HPREL()			
	
Return

User Function HPREL()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\TEMP\"					
	Local cFilePrint := "Romaneio_faturamento.pdf"
	Local xArea := GetArea()	
	Local cLogo := "\system\logohope.png"
	Local cLinha := ""
	Private oPrinter
	Private oFont1 	:= TFont():New( "Arial",,11,,.T.,,,,,  .F. )
	Private oFont2 	:= TFont():New( "Arial",,10,,.F.,,,,,  .F. )
	Private oFont3 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Private nQuantNf := 0
	Private nValNF := 0
	Private nCont := 0
	Private nLin   := 25
	Private nTmLin := 15
	Private nSalto := 20
		
	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()
	oPrinter:StartPage()	

	ProcRegua(0)

	oPrinter:Say ( nLin, 200, "ROMANEIO DE ENTREGA",  oFont3 )
	oPrinter:SayBitmap( nLin-010, 050, cLogo, 100, 058)
	nLin := nLin + nTmLin
	
	if Select("T01") > 0
		T01->(dbCloseArea())
	endif

	cQuery := " SELECT B1_CODBAR AS BARRA,D2_COD AS COD,BV_CHAVE AS COD_COR,BV_DESCRI AS COR,REPLACE(SUBSTRING(B1_COD,13,3),'00','') AS TAMANHO,B1_DESC AS DESCR,D2_PRCVEN AS VALOR,D2_QUANT AS QTD, * " 
	cQuery += " FROM "+ RetSqlName("SD2") +" SD2 (NOLOCK) JOIN "+RetSqlname("SB1")+ " SB1 (NOLOCK) ON (B1_COD = D2_COD AND SB1.D_E_L_E_T_ = '' ) "
	cQuery += " JOIN "+ RetSqlName("SBV") +" (NOLOCK) ON SUBSTRING(B1_COD,9,3) = BV_CHAVE AND BV_TABELA = 'COR' AND SBV010.D_E_L_E_T_ = '' AND "
	cQuery += " SD2.D_E_L_E_T_ = '' AND D2_DOC = '000629858' " 
	
	TcQuery cQuery New Alias T01	 

	cNF := T01->D2_DOC
	cSerie :=T01->D2_SERIE
	dData := date() 

	oPrinter:Say ( nLin, 200, "Nome Cliente: ", oFont3 )
	oPrinter:Say ( nLin, 300, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+T01->D2_CLIENTE+T01->D2_LOJA ,"A1_NREDUZ")),0,50), oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Nota Fiscal:  ", oFont3 )
	oPrinter:Say ( nLin, 300, cNF , oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Data ", oFont3 )
	oPrinter:Say ( nLin, 250, Dtoc(dData), oFont2 )
	nLin := nLin + nTmLin 

	nLin := nLin + 25
	oPrinter:Say ( nLin, 20, "BARRA",       			oFont1 )
	oPrinter:Say ( nLin, 80, "COD PRODUTO",  			oFont1 )
	oPrinter:Say ( nLin, 165, "COD COR",     			oFont1 )
	oPrinter:Say ( nLin, 210, "COR",   					oFont1 )
	oPrinter:Say ( nLin, 250, "TAMANHO",      			oFont1 )
	oPrinter:Say ( nLin, 300, "DESCRICAO DO MATERIAL", 	oFont1 )
	oPrinter:Say ( nLin, 520, "VLR ITEM", 				oFont1 )
	oPrinter:Say ( nLin, 570, "QTD ITEM",       		oFont1 )


	While !T01->(EOF())

		IF nLin >= 750
			nLin   := 25
			oPrinter:EndPage()
			oPrinter:StartPage()
		ENDIF

		oPrinter:Say ( nLin+10,  20,  T01->BARRA,                      oFont2 )
		oPrinter:Say ( nLin+10,  80,  T01->COD,                	   	   oFont2 )
		oPrinter:Say ( nLin+10,  165, T01->COD_COR,              	   oFont2 )
		oPrinter:Say ( nLin+10,  210, T01->COR,              		   oFont2 )
		oPrinter:Say ( nLin+10,  270, T01->TAMANHO,         		   oFont2 )
		oPrinter:Say ( nLin+10,  300, T01->DESCR,         		       oFont2 )
		oPrinter:Say ( nLin+10,  530, cValToChar(T01->VALOR),		   oFont2 )
		oPrinter:Say ( nLin+10,  590, cValToChar(T01->QTD), 		   oFont2 )
		
		nQuantNf += T01->QTD
		nValNF += T01->VALOR
		

		T01->(dbSkip())
		nLin := nLin + 10
		nCont++
		
	ENDDO
	
	nLin := nLin + 30
	FunRdp()
	
	T01->(DbCloseArea())

	oPrinter:EndPage()

	oPrinter:Preview()
	oPrinter:SetViewPDF ( .T. )
	Restarea(xArea)	
return

Static Function FunRdp()

	nLin := nLin + 10
	
    IF nLin >= 750
		oPrinter:EndPage()
	ENDIF
	
	nValNF := POSICIONE("SF2",1,XFILIAL("SF2")+cNF+cSerie, "F2_VALBRUT")
	nDesco := POSICIONE("SF2",1,XFILIAL("SF2")+cNF+cSerie, "F2_DESCONT")
	nCont := nVAlNF + nDesco
	
	nLin := nLin + 100	
	oPrinter:Say ( nLin, 20, "QTD PE�A",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nQuantNf, "@E 999,999,999"),20),  oFont2 )	
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "VLR BRUTO",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nCont, "@E 9999,999,999.99"),20),  oFont2 )
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "DESCONTO",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nDesco, "@E 9999,999,999.99"),20),  oFont2 )	
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "VLR LIQUIDO",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nValNF, "@E 9999,999,999.99"),20),  oFont2 )			
	
	oPrinter:EndPage()
				
Return
