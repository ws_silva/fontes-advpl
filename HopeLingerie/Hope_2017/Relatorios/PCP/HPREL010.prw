#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL010 
Romaneio Pe�as
@author Weskley Silva
@since 06/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL010()

Private oReport
Private cPergCont	:= 'HPREL010' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 06 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'ROMA1', 'ROMANEIO PE�AS ', , {|oReport| ReportPrint( oReport ), 'ROMANEIO PE�AS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetPortrait()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'ROMANEIO PE�AS', { 'ROMA1', 'SC6','SC5','SZJ','SZR','SZQ','SB4','SBV'})	
	TRCell():New( oSection1, 'COD_CLIENTE'	    	,'ROMA1', 		'COD_CLIENTE',						"@!"                        ,15)
	TRCell():New( oSection1, 'CLIENTE'	    	    ,'ROMA1', 		'CLIENTE',							"@!"                        ,35)
	
	
	oSection2 := TRSection():New( oReport, 'PE�AS', { 'ROMA1', 'SC6','SC5','SZJ','SZR','SZQ','SB4','SBV'})
	TRCell():New( oSection2, 'PRE_FATURAMENTO'      ,'ROMA1', 		'PRE_FATURAMENTO', 					"@!"				        ,20)
	TRCell():New( oSection2, 'PEDIDO' 		        ,'ROMA1', 		'PEDIDO',              				"@!"						,10)
	TRCell():New( oSection2, 'VOLUME'           	,'ROMA1', 		'VOLUME',	              			"@E 999,99"					,10)
	TRCell():New( oSection2, 'TOT_VOLUMES' 			,'ROMA1', 		'TOT_VOLUMES' ,       				"@E 999,99"	                ,10)
	TRCell():New( oSection2, 'CODIGO'		       	,'ROMA1', 		'CODIGO',          					"@!"				        ,20)
	TRCell():New( oSection2, 'COR' 					,'ROMA1', 		'COR' ,			    			    "@!"				        ,20)
	TRCell():New( oSection2, 'TAMANHO'	        	,'ROMA1', 		'TAMANHO',			       			"@!"		    	        ,10)
	TRCell():New( oSection2, 'DESCRICAO'			,'ROMA1', 		'DESCRICAO',			   			"@!"						,35)
	TRCell():New( oSection2, 'QUANTIDADE'			,'ROMA1', 		'QUANTIDADE',		   				"@E 999,99"					,06)
		
	oReport:SetTotalInLine(.F.)	
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	IF Select("ROMA1") > 0
		ROMA1->(dbCloseArea())
	Endif
      
    cQuery := " SELECT C5_XNOMCLI AS CLIENTE, C5_CLIENTE AS 'COD_CLIENTE', " 
    cQuery += " ISNULL(ZJ_NUMPF,'-') AS 'PRE_FATURAMENTO', " 
	cQuery += " C6_NUM AS PEDIDO, "
	cQuery += " ISNULL(ZR_VOLUME,'-') AS VOLUME, "
	cQuery += " C5_VOLUME1 AS 'TOT_VOLUMES', "
	cQuery += " B4_COD AS CODIGO, "
	cQuery += " BV_DESCRI AS COR, "
	cQuery += " SUBSTRING(C6_PRODUTO,12,4) AS TAMANHO, "
	cQuery += " ISNULL(ZQ_DESCRIC,'-') AS DESCRICAO, "
	cQuery += " C6_QTDVEN  AS QUANTIDADE " 
    cQuery += " FROM "+RETSQLNAME('SC6')+"  "   
	cQuery += " JOIN "+RETSQLNAME('SC5')+" (NOLOCK) ON C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND SC5010.D_E_L_E_T_ = '' "
	cQuery += " LEFT JOIN "+RETSQLNAME('SZJ')+" (NOLOCK) ON C6_NUM = ZJ_PEDIDO AND C6_CLI = ZJ_CLIENTE AND C6_LOJA = ZJ_LOJA AND SZJ010.D_E_L_E_T_ = '' "
	cQuery += " LEFT JOIN "+RETSQLNAME('SZR')+" (NOLOCK) ON ZR_NUMPF = ZJ_NUMPF AND SZR010.D_E_L_E_T_ = '' "
	cQuery += " LEFT JOIN "+RETSQLNAME('SZQ')+" (NOLOCK) ON ZR_TPVOL = ZQ_CODIGO AND SZQ010.D_E_L_E_T_ = '' "
	cQuery += " LEFT JOIN "+RETSQLNAME('SB4')+" (NOLOCK) ON LEFT(C6_PRODUTO,8) = B4_COD AND SB4010.D_E_L_E_T_ = '' "
	cQuery += " LEFT JOIN "+RETSQLNAME('SBV')+" (NOLOCK) ON SUBSTRING(C6_PRODUTO,9,3) = BV_CHAVE  AND BV_TABELA = 'COR' AND SBV010.D_E_L_E_T_ = '' " 

	cQuery += " WHERE ZJ_NUMPF = '"+Alltrim(mv_par01)+"' AND SC6010.D_E_L_E_T_ = '' " 
	cQuery += " GROUP BY C5_XNOMCLI,C5_CLIENTE,ZJ_NUMPF,C6_NUM,ZR_VOLUME,C5_VOLUME1,B4_COD,BV_DESCRI,C6_PRODUTO,ZQ_DESCRIC,C6_QTDVEN "
	
	
	TCQUERY cQuery NEW ALIAS ROMA1

	While ROMA1->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		
		oSection1:Init()
		oSection1:SetHeaderSection(.T.)
		
		oReport:IncMeter()
		
		cCliente := ROMA1->COD_CLIENTE
		
		oSection1:Cell("COD_CLIENTE"):SetValue(ROMA1->COD_CLIENTE)
		oSection1:Cell("COD_CLIENTE"):SetAlign("LEFT")

		oSection1:Cell("CLIENTE"):SetValue(ROMA1->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:PrintLine()
	
		oSection2:init()

		WHILE ROMA1->COD_CLIENTE == cCliente
		
		oSection2:Cell("PRE_FATURAMENTO"):SetValue(ROMA1->PRE_FATURAMENTO)
		oSection2:Cell("PRE_FATURAMENTO"):SetAlign("LEFT")
		
		oSection2:Cell("PEDIDO"):SetValue(ROMA1->PEDIDO)
		oSection2:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection2:Cell("VOLUME"):SetValue(ROMA1->VOLUME)
		oSection2:Cell("VOLUME"):SetAlign("LEFT")
		
		oSection2:Cell("TOT_VOLUMES"):SetValue(ROMA1->TOT_VOLUMES)
		oSection2:Cell("TOT_VOLUMES"):SetAlign("LEFT")
			
		oSection2:Cell("CODIGO"):SetValue(ROMA1->CODIGO)
		oSection2:Cell("CODIGO"):SetAlign("LEFT")
				
		oSection2:Cell("COR"):SetValue(ROMA1->COR)
		oSection2:Cell("COR"):SetAlign("LEFT")
				
		oSection2:Cell("DESCRICAO"):SetValue(ROMA1->DESCRICAO)
		oSection2:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection2:Cell("QUANTIDADE"):SetValue(ROMA1->QUANTIDADE)
		oSection2:Cell("QUANTIDADE"):SetAlign("LEFT")
		
		oSection2:PrintLine()									
		
		ROMA1->(DBSKIP())
		
		enddo
		
	oSection2:Finish()
	oReport:ThinLine()	
	oSection1:Finish()		
	enddo
	ROMA1->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 06 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Pre-Faturamento ?"		        ,""		,""		,"mv_ch1","C",10,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	
Return