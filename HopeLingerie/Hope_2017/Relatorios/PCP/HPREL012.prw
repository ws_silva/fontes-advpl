#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL010 
Pe�as bipadas
@author Weskley Silva
@since 07/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL012()

Private oReport
Private cPergCont	:= 'HPREL012' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'PEC', 'PE�AS BIPADAS', , {|oReport| ReportPrint( oReport ), 'PE�AS BIPADAS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PE�AS BIPADAS', { 'PEC', 'SZR','SZJ','SA1','SA2','SC5','SZ1'})
			
	TRCell():New( oSection1, 'DATA_CONFERIDA'	    ,'PEC', 		'DATA_CONFERIDA',					"@!"                        ,10)
	TRCell():New( oSection1, 'HORA_CONFERIDA'	    ,'PEC', 		'HORA_CONFERIDA',  					"@!"				        ,35)
	TRCell():New( oSection1, 'CONFERENTE' 	        ,'PEC', 		'CONFERENTE',          				"@!"						,15)
	TRCell():New( oSection1, 'NOME_CONFERENTE'     	,'PEC', 		'NOME_CONFERENTE',	       			"@!"						,30)
	TRCell():New( oSection1, 'NUMERO_PEDIDO'		,'PEC', 		'NUMERO_PEDIDO',       				"@!"		                ,15)
	TRCell():New( oSection1, 'NUM_PRE_FATURAMENTO'  ,'PEC', 		'NUM_PRE_FATURAMENTO', 				"@!"				        ,15)
	TRCell():New( oSection1, 'CLIENTE'  			,'PEC', 		'CLIENTE',		    			    "@!"				        ,35)
	TRCell():New( oSection1, 'LOJA' 				,'PEC', 	    'LOJA',                      		"@!"		    	        ,15)
	TRCell():New( oSection1, 'RAZAO_SOCIAL'	        ,'PEC', 		'RAZAO_SOCIAL',      	   			"@!"						,15)
	TRCell():New( oSection1, 'ITEM'					,'PEC', 		'ITEM',				   				"@!"						,15)
	TRCell():New( oSection1, 'PRODUTO'				,'PEC', 		'PRODUTO',				   			"@!"						,15)
	TRCell():New( oSection1, 'DESCRICAO'			,'PEC', 		'DESCRICAO',			   			"@!"						,15)
	TRCell():New( oSection1, 'QTD_CONFERIDA'		,'PEC', 		'QTD_CONFERIDA',					"@!"						,06)
	TRCell():New( oSection1, 'TIPO_PEDIDO'			,'PEC', 		'TIPO_PEDIDO',   					"@!"						,40)
	TRCell():New( oSection1, 'CANAL_VENDA'			,'PEC', 		'CANAL_VENDA',   					"@!"						,40)
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("PEC") > 0
		PEC->(dbCloseArea())
	Endif
      
    cQuery := " SELECT CONVERT(CHAR, CAST(SZR.ZR_DATA AS SMALLDATETIME), 103) AS [DATA_CONFERIDA], " 
	cQuery += " SZR.ZR_HORA AS [HORA_CONFERIDA], " 
	cQuery += " SZR.ZR_USR AS CONFERENTE, " 
	cQuery += " SZR.ZR_NOMEUSR AS [NOME_CONFERENTE], " 
	cQuery += " SZP.ZP_PEDIDO AS [NUMERO_PEDIDO], " 
	cQuery += " SZR.ZR_NUMPF AS [NUM_PRE_FATURAMENTO], " 
	cQuery += " CASE SC5.C5_TIPO WHEN 'B' THEN LTRIM(RTRIM(ISNULL(SA2.A2_COD, ''))) ELSE LTRIM(RTRIM(ISNULL(SA1.A1_COD, ''))) END AS CLIENTE, "
    cQuery += " CASE SC5.C5_TIPO WHEN 'B' THEN LTRIM(RTRIM(ISNULL(SA2.A2_LOJA, ''))) ELSE LTRIM(RTRIM(ISNULL(SA1.A1_LOJA, ''))) END AS LOJA, "
	cQuery += " CASE SC5.C5_TIPO WHEN 'B' THEN LTRIM(RTRIM(ISNULL(SA2.A2_NOME, ''))) ELSE LTRIM(RTRIM(ISNULL(SA1.A1_NOME, ''))) END AS [RAZAO_SOCIAL], "
	cQuery += " SZR.ZR_ITEM AS ITEM, "
	cQuery += " SZR.ZR_PRODUTO AS PRODUTO, "
	cQuery += " SB1.B1_DESC AS DESCRICAO, "
	cQuery += " SZR.ZR_QUANT AS [QTD_CONFERIDA], "
	cQuery += " Z1.Z1_DESC AS TIPO_PEDIDO, "  
	cQuery += " ISNULL(ZA4.ZA4_REGION, '') AS [CANAL_VENDA] "

	cQuery += " FROM  "+RetSqlName('SZR')+" SZR "
	cQuery += " INNER JOIN "+RetSqlName('SB1')+"  SB1 ON SB1.B1_COD = SZR.ZR_PRODUTO AND SB1.D_E_L_E_T_ <> '*' "
	cQuery += " INNER JOIN "+RetSqlName('SZP')+"  SZP ON SZP.ZP_NUMPF = SZR.ZR_NUMPF AND SZP.ZP_CODBAR = SZR.ZR_CODBAR AND SZP.ZP_PRODUTO = SZR.ZR_PRODUTO AND SZP.D_E_L_E_T_ <> '*' "
	cQuery += " INNER JOIN "+RetSqlName('SC5')+"  SC5 ON SC5.C5_NUM = SZP.ZP_PEDIDO AND SC5.D_E_L_E_T_ <> '*' "
	cQuery += " LEFT OUTER JOIN "+RetSqlName('SA1')+"  SA1 ON SC5.C5_CLIENTE = SA1.A1_COD AND SC5.C5_LOJAENT = SA1.A1_LOJA  "
	cQuery += " LEFT OUTER JOIN "+RetSqlName('SA2')+"  SA2 ON SC5.C5_CLIENTE = SA2.A2_COD AND SC5.C5_LOJAENT = SA2.A2_LOJA  "
	cQuery += " LEFT OUTER JOIN "+RetSqlName('ZA4')+"  ZA4 ON SA1.A1_XMICRRE = ZA4.ZA4_CODMAC AND ZA4.D_E_L_E_T_ <> '*' "
	cQuery += " INNER JOIN "+RetSqlName('SZ1')+" Z1 ON Z1_CODIGO = C5_TPPED AND Z1.D_E_L_E_T_ = '' "
	cQuery += " WHERE SZR.D_E_L_E_T_ <> '*' "  
    
    IF EMPTY(mv_par01)
    	 cQuery += "  "
    else
    	 cQuery += " AND SZR.ZR_NUMPF BETWEEN  '"+mv_par02+"' AND '"+mv_par03+"' "
    endif
   
    IF EMPTY(mv_par03)
    	cQuery += " "
    ELSE
    	cQuery += " AND SZJ.ZJ_PEDIDO = '"+Alltrim(mv_par04)+"' "
    ENDIF
    
    if EMPTY(mv_par04) 
    	cQuery += " "
    else
    	cQuery += " AND SZR.ZR_DATA BETWEEN "+ DTOS(mv_par04)+ " AND "+ DTOS(mv_par05)+ "  "
    endif
    
   
   	cQuery += " GROUP BY SZR.ZR_DATA, " 
	cQuery += "	 SZR.ZR_HORA, "
	cQuery += "	 SZR.ZR_USR, "
	cQuery += "	 SZR.ZR_NOMEUSR, "
	cQuery += "	 SZP.ZP_PEDIDO, "
	cQuery += "	 SZR.ZR_NUMPF, "
	cQuery += "	 SC5.C5_TIPO, "
	cQuery += "	 SA2.A2_COD, "
	cQuery += "	 SA1.A1_COD, "
	cQuery += "	 SA2.A2_LOJA, "
	cQuery += "	 SA1.A1_LOJA, "
	cQuery += "	 SA2.A2_NOME, "
	cQuery += "	 SA1.A1_NOME, "
	cQuery += "	 SZR.ZR_ITEM, "
	cQuery += "	 SZR.ZR_PRODUTO, "
	cQuery += "	 SB1.B1_DESC, "
	cQuery += "	 SZR.ZR_QUANT, "
	cQuery += "	 Z1.Z1_DESC, "
	cQuery += "	 ZA4.ZA4_REGION  " 
	
	TCQUERY cQuery NEW ALIAS PEC

	While PEC->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("DATA_CONFERIDA"):SetValue(PEC->DATA_CONFERIDA)
		oSection1:Cell("DATA_CONFERIDA"):SetAlign("LEFT")

		oSection1:Cell("HORA_CONFERIDA"):SetValue(PEC->HORA_CONFERIDA)
		oSection1:Cell("HORA_CONFERIDA"):SetAlign("LEFT")
		
		oSection1:Cell("CONFERENTE"):SetValue(PEC->CONFERENTE)
		oSection1:Cell("CONFERENTE"):SetAlign("LEFT")
		
		oSection1:Cell("NOME_CONFERENTE"):SetValue(PEC->NOME_CONFERENTE)
		oSection1:Cell("NOME_CONFERENTE"):SetAlign("LEFT")
		
		oSection1:Cell("NUMERO_PEDIDO"):SetValue(PEC->NUMERO_PEDIDO)
		oSection1:Cell("NUMERO_PEDIDO"):SetAlign("LEFT")
			
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetValue(PEC->NUM_PRE_FATURAMENTO)
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetAlign("LEFT")
				
		oSection1:Cell("CLIENTE"):SetValue(PEC->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
				
		oSection1:Cell("LOJA"):SetValue(PEC->LOJA)
		oSection1:Cell("LOJA"):SetAlign("LEFT")
		
		oSection1:Cell("RAZAO_SOCIAL"):SetValue(PEC->RAZAO_SOCIAL)
		oSection1:Cell("RAZAO_SOCIAL"):SetAlign("LEFT")
		
		oSection1:Cell("ITEM"):SetValue(PEC->ITEM)
		oSection1:Cell("ITEM"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(PEC->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
						
		oSection1:Cell("DESCRICAO"):SetValue(PEC->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_CONFERIDA"):SetValue(PEC->QTD_CONFERIDA)
		oSection1:Cell("QTD_CONFERIDA"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(PEC->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("CANAL_VENDA"):SetValue(PEC->CANAL_VENDA)
		oSection1:Cell("CANAL_VENDA"):SetAlign("LEFT")
											
		oSection1:PrintLine()
		
		PEC->(DBSKIP()) 
	enddo
	PEC->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Pr�-Fatuamento de ?"            ,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Pr�-Fatuamento ate ?"		    ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Pedido ?"		                ,""		,""		,"mv_ch3","C",06,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Data Conferencia de ?"		    ,""		,""		,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data Conferencia at� ?"		    ,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return