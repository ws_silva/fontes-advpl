#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL006 
Relatório PERFORMANCE 
@author Weskley Silva
@since 06/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL009()

Private oReport
Private cPergCont	:= 'HPREL009' 
Private cTipos := " "

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

cMvPar05:= AllTrim(mv_par05) 

If !Empty(cMvPar05)
	For nx := 1 to len(cMvPar05) step 2
		cAux := SubStr(cMvPar05,nx,2)
		If cAux <> '**'
			cTipos += "'0"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 03 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'PER', 'PERFORMANCE DE ATENDIMENTO POR PEDIDO ', , {|oReport| ReportPrint( oReport ), 'PERFORMANCE DE ATENDIMENTO POR PEDIDO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PERFORMANCE DE ATENDIMENTO POR PEDIDO', { 'PER', 'SC5','SC6','SF2','SA1','SZ1','SZ2','SE4'})
			
	TRCell():New( oSection1, 'ORIGEM'	    	    ,'PER', 		'ORIGEM',							"@!"                        ,20)
	TRCell():New( oSection1, 'EMISSAO'      	    ,'PER', 		'EMISSAO',	    					"@!"				        ,08)
	TRCell():New( oSection1, 'PEDIDO' 		        ,'PER', 		'PEDIDO',              				"@!"						,06)
	TRCell():New( oSection1, 'QTDE_PEDIDO'         	,'PER', 		'QTDE_PEDIDO',             			"@E 999,99"					,06)
	TRCell():New( oSection1, 'QTDE_PREFAT' 			,'PER', 		'QTDE_PREFAT',        				"@E 999,99"	            	,06)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'       	,'PER', 		'QTDE_ENTREGUE',       				"@E 999,99"		        	,06)
	TRCell():New( oSection1, 'QTDE_PERDA' 			,'PER', 		'QTDE_PERDA' ,	    			    "@E 999,99"					,06)
	TRCell():New( oSection1, 'SALDO'	        	,'PER', 		'SALDO' 	,		       			"@E 999.999,99"	   	        ,08)
	TRCell():New( oSection1, 'VALOR_TOTAL'			,'PER', 		'VALOR_TOTAL',			   			"@E 999.999,99"				,08)
	TRCell():New( oSection1, 'CLIENTE'				,'PER', 		'CLIENTE',			   				"@!"						,40)
	TRCell():New( oSection1, 'TIPO_PEDIDO'			,'PER', 		'TIPO_PEDIDO',			   			"@!"						,40)
	TRCell():New( oSection1, 'COND_PGTO'			,'PER', 		'COND_PGTO',			   			"@!"						,35)
	TRCell():New( oSection1, 'POLITICA'			    ,'PER', 		'POLITICA',			   			    "@!"						,35)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("PER") > 0
		PER->(dbCloseArea())
	Endif
      
    cQuery := " SELECT R.C5_ORIGEM AS ORIGEM, R.C5_EMISSAO AS EMISSAO, R.PEDIDO , R.QTDE_PEDIDO, R.QTDE_PREFAT, R.QTDE_ENTREGUE, R.QTDE_PERDA,((R.QTDE_PEDIDO - R.QTDE_ENTREGUE)- R.QTDE_PERDA ) AS SALDO, R.VALOR_TOTAL, R.CLIENTE, R.TIPO_PEDIDO, R.COND_PGTO, R.POLITICA " 
    cQuery += " FROM (SELECT C5_ORIGEM, " 
    cQuery += " C5_EMISSAO, " 
	cQuery += " F2.F2_DAUTNFE AS DATA_FATURAMENTO, " 
    cQuery += " C5_NUM AS PEDIDO, " 
    cQuery += "   (SELECT SUM(C6_QTDVEN) FROM SC6010 WHERE SC6010.D_E_L_E_T_ = '' AND C6_NUM = C5_NUM AND C6_PRODUTO = C6_PRODUTO " 
    cQuery += "    )AS QTDE_PEDIDO, " 
	cQuery += "   ISNULL((SELECT SUM(ZJ_QTDLIB) FROM SZJ010 ZJ WHERE ZJ.D_E_L_E_T_ = '' AND ZJ_PEDIDO = C5_NUM  " 
	cQuery += "   ),0)AS QTDE_PREFAT, " 
	cQuery += "   ISNULL((SELECT SUM(C6_QTDENT) FROM SC6010 WHERE SC6010.D_E_L_E_T_ = '' AND C6_NUM = C5_NUM AND C6_PRODUTO = C6_PRODUTO AND C6_NOTA <> '' " 
	cQuery += "   ),0)AS QTDE_ENTREGUE, " 
	cQuery += "   ISNULL((SELECT SUM(C6_QTDVEN) FROM SC6010 WHERE SC6010.D_E_L_E_T_ = '' AND C6_NUM = C5_NUM AND C6_PRODUTO = C6_PRODUTO AND C6_NOTA = '' AND C6_BLQ = 'R' " 
	cQuery += "   ),0)AS QTDE_PERDA, " 
	cQuery += "  SUM(C6.C6_VALOR) AS VALOR_TOTAL, " 
	cQuery += "  A1_NOME AS CLIENTE, " 
	cQuery += "   C5_CONDPAG + ' - ' + E4_DESCRI AS COND_PGTO, " 
	cQuery += "   C5_POLCOM + ' - ' + Z2_DESC AS POLITICA, " 
	cQuery += "   Z1_CODIGO + ' - ' + Z1_DESC AS TIPO_PEDIDO " 
	cQuery += " FROM "+RetSqlName('SC6')+" C6 " 
	cQuery += " INNER JOIN "+RetSqlName('SC5')+" C5 ON (C5_NUM = C6_NUM AND C5.D_E_L_E_T_ = '') " 
	cQuery += " INNER JOIN "+RetSqlName('SA1')+" A1 ON (A1.A1_COD = C5.C5_CLIENTE AND A1.D_E_L_E_T_ = '') " 
	cQuery += " INNER JOIN "+RetSqlName('SZ1')+" Z1 ON (Z1_CODIGO = C5_TPPED AND Z1.D_E_L_E_T_ = '') " 
	cQuery += " INNER JOIN "+RetSqlName('SF2')+" F2 ON (F2.F2_CLIENT = C5.C5_CLIENTE AND F2.F2_LOJA = C5.C5_LOJACLI AND F2.F2_DOC = C5.C5_NOTA AND F2.F2_SERIE = C5.C5_SERIE AND F2.D_E_L_E_T_ = '') " 
	cQuery += " LEFT JOIN  "+RetSqlName('SZ2')+" Z2 ON (Z2_CODIGO = C5_POLCOM AND Z2.D_E_L_E_T_ = '') " 
	cQuery += " LEFT JOIN  "+RetSqlName('SE4')+" E4 ON (E4.E4_CODIGO = C5_CONDPAG AND E4.D_E_L_E_T_ = '') " 
	cQuery += " WHERE C6.D_E_L_E_T_ = '' " 
	cQuery += " AND C6.C6_NOTA <> '' " 
	If !Empty(cTipos)
	cQuery += CRLF + "AND C5_TPPED IN ("+cTipos+") "
	EndIf
	
	if !Empty(mv_par03)
		cQuery += " AND C5_NUM BETWEEN '"+Alltrim(mv_par03)+"' AND '"+Alltrim(mv_par04)+"' "
	endif
	
	cQuery += " GROUP BY C5_ORIGEM, C5_EMISSAO, C5_NUM, A1_NOME,C5_CONDPAG + ' - ' + E4_DESCRI, C5_POLCOM + ' - ' + Z2_DESC, Z1_CODIGO + ' - ' + Z1_DESC ,F2_DAUTNFE " 
	cQuery += " ) R WHERE R.F2_DAUTNFE BETWEEN '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"' " 
	
	
	TCQUERY cQuery NEW ALIAS PER

	While PER->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("ORIGEM"):SetValue(PER->ORIGEM)
		oSection1:Cell("ORIGEM"):SetAlign("LEFT")

		oSection1:Cell("EMISSAO"):SetValue(PER->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("PEDIDO"):SetValue(PER->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PEDIDO"):SetValue(PER->QTDE_PEDIDO)
		oSection1:Cell("QTDE_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PREFAT"):SetValue(PER->QTDE_PREFAT)
		oSection1:Cell("QTDE_PREFAT"):SetAlign("LEFT")
			
		oSection1:Cell("QTDE_ENTREGUE"):SetValue(PER->QTDE_ENTREGUE)
		oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
				
		oSection1:Cell("QTDE_PERDA"):SetValue(PER->QTDE_PERDA)
		oSection1:Cell("QTDE_PERDA"):SetAlign("LEFT")
				
		oSection1:Cell("SALDO"):SetValue(PER->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_TOTAL"):SetValue(PER->VALOR_TOTAL)
		oSection1:Cell("VALOR_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE"):SetValue(PER->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(PER->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
						
		oSection1:Cell("COND_PGTO"):SetValue(PER->COND_PGTO)
		oSection1:Cell("COND_PGTO"):SetAlign("LEFT")
		
		oSection1:Cell("POLITICA"):SetValue(PER->POLITICA)
		oSection1:Cell("POLITICA"):SetAlign("LEFT")
									
		oSection1:PrintLine()
		
		PER->(DBSKIP()) 
	enddo
	PER->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 06 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Dt Inicial"		        ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt Final"			    ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Pedido de Venda de ?"   ,""		,""		,"mv_ch3","C",06,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Pedido de Venda Ate ?"  ,""		,""		,"mv_ch4","C",06,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont,"05","Tipo dos Pedidos     ","","","Mv_ch5","C",99,0,0,"G","U_HF002TP()","","","N","Mv_par05","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os tipos de pedido.",""},{""},{""},"")
Return