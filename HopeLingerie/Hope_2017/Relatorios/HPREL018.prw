#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL018 
Saldo por Armazem
@author Weskley Silva
@since 13/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL018()

Private oReport
Private cPergCont	:= 'HPREL018' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'SLD', 'SALDO POR ARMAZEM', , {|oReport| ReportPrint( oReport ), 'SALDO POR ARMAZEM' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'SALDO POR ARMAZEM', { 'SLD', 'SB2','SB1','SBM'})
			
	TRCell():New( oSection1, 'FILIAL'				,'SLD', 		'FILIAL',							"@!"                        ,10)
	TRCell():New( oSection1, 'ARMAZEN'				,'SLD', 		'ARMAZEN',			  			    "@!"				        ,06)
	TRCell():New( oSection1, 'TIPO'				    ,'SLD', 		'TIPO',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'GRUPO'				,'SLD', 		'GRUPO',			  			    "@!"				        ,25)
	TRCell():New( oSection1, 'SKU'					,'SLD', 		'SKU',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'REFERENCIA'			,'SLD', 		'REFERENCIA',		  			    "@!"				        ,15)
	TRCell():New( oSection1, 'COD_COR'				,'SLD', 		'COD_COR',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'DESC_COR'				,'SLD', 		'DESC_COR',			  			    "@!"				        ,35)
	TRCell():New( oSection1, 'TAM'					,'SLD', 		'TAM',				  			    "@!"				        ,15)	
	TRCell():New( oSection1, 'DESCRICAO'			,'SLD', 		'DESCRICAO',		  			    "@!"				        ,35)
	TRCell():New( oSection1, 'UNID_MED'				,'SLD', 		'UNID_MED',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'SALDO'				,'SLD', 		'SALDO',	  					    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'EMPENHO'				,'SLD', 		'EMPENHO',			  			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'TOTAL'				,'SLD', 		'TOTAL',			  			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'QTDE_PREVISTA_ENTRAR'	,'SLD', 		'QTDE_PREVISTA_ENTRAR',			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'QTDE_PED_VENDA'		,'SLD', 		'QTDE_PED_VENDA',	  	     	 	"@E 999,99" 		        ,06)
	TRCell():New( oSection1, 'QTDE_PODER_TERCEIROS' ,'SLD', 		'QTDE_PODER_TERCEIROS',			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'EMPENHO_PREVISTO'		,'SLD', 		'EMPENHO_PREVISTO',	  			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'EMPENHO_NF'			,'SLD', 		'EMPENHO_NF',	     			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'RESERVA'				,'SLD', 		'RESERVA',			  			    "@E 999,99"			        ,06)	
	TRCell():New( oSection1, 'SALDO_PREVISTO_OP'	,'SLD', 		'SALDO_PREVISTO_OP',  			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'SALDO_A_ENDERECAR'	,'SLD', 		'SALDO_A_ENDERECAR',  			    "@E 999,99"			        ,06)
			
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("SLD") > 0
		SLD->(dbCloseArea())
	Endif
      
    cQuery := " SELECT SB2.B2_FILIAL AS FILIAL, " 
	cQuery += " SB2.B2_COD AS SKU, "
	cQuery += " SB1.B1_DESC as DESCRICAO, "
	cQuery += " LEFT(B2_COD,8) AS REFERENCIA, "
	cQuery += " B1_TIPO AS TIPO, "
	cQuery += " BM_DESC AS GRUPO, "
	cQuery += " B2_LOCAL AS ARMAZEN, "
	cQuery += " B2_QATU AS SALDO, "
	cQuery += " B2_RESERVA AS RESERVA, "
	cQuery += " B2_QEMP AS EMPENHO, "
	cQuery += " B2_QEMPN AS EMPENHO_NF, "
	cQuery += " LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AS COD_COR,"
    cQuery += " SBV.BV_DESCRI AS DESC_COR,"
    cQuery += " RIGHT(SB1.B1_COD,4)  AS TAM, "
    cQuery += " B1_UM AS UNID_MED,"
	cQuery += " B2_QPEDVEN AS QTDE_PED_VENDA, "
	cQuery += " B2_SALPEDI AS QTDE_PREVISTA_ENTRAR, "
	cQuery += " B2_QNPT AS QTDE_PODER_TERCEIROS, "
	cQuery += " B2_QACLASS AS SALDO_A_ENDERECAR, "
	cQuery += " B2_QEMPPRE AS EMPENHO_PREVISTO, "
	cQuery += " B2_SALPPRE AS SALDO_PREVISTO_OP, "
	cQuery += " (B2_QEMP + B2_QATU) AS TOTAL "
	cQuery += " FROM "+RetSqlName('SB2')+" SB2 "
	cQuery += " JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_COD=SB2.B2_COD AND SB1.D_E_L_E_T_='' "
	cQuery += " JOIN "+RetSqlName('SBM')+" SBM ON SBM.BM_GRUPO=B1_GRUPO AND SBM.D_E_L_E_T_='' "
	cQuery += " JOIN "+RetSqlName('SBV')+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
	cQuery += " WHERE SB2.D_E_L_E_T_='' " 
	cQuery += " AND SB1.B1_TIPO NOT IN ('MO','SV') " 
	
	if !Empty(mv_par01)
		cQuery += " AND B2_LOCAL = '"+Alltrim(mv_par01)+"'  " 
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par02) 
		cQuery += " AND B2_FILIAL = '"+Alltrim(mv_par02)+"' "
	else
		cQuery += " "
	endif
	
	
	TCQUERY cQuery NEW ALIAS SLD

	While SLD->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(SLD->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection1:Cell("ARMAZEN"):SetValue(SLD->ARMAZEN)
		oSection1:Cell("ARMAZEN"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(SLD->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(SLD->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")

		oSection1:Cell("SKU"):SetValue(SLD->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(SLD->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(SLD->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(SLD->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(SLD->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(SLD->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("UNID_MED"):SetValue(SLD->UNID_MED)
		oSection1:Cell("UNID_MED"):SetAlign("LEFT")
		
		oSection1:Cell("SALDO"):SetValue(SLD->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("EMPENHO"):SetValue(SLD->EMPENHO)
		oSection1:Cell("EMPENHO"):SetAlign("LEFT")
		
		oSection1:Cell("TOTAL"):SetValue(SLD->TOTAL)
		oSection1:Cell("TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PREVISTA_ENTRAR"):SetValue(SLD->QTDE_PREVISTA_ENTRAR)
		oSection1:Cell("QTDE_PREVISTA_ENTRAR"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PED_VENDA"):SetValue(SLD->QTDE_PED_VENDA)
		oSection1:Cell("QTDE_PED_VENDA"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PODER_TERCEIROS"):SetValue(SLD->QTDE_PODER_TERCEIROS)
		oSection1:Cell("QTDE_PODER_TERCEIROS"):SetAlign("LEFT")
		
		oSection1:Cell("EMPENHO_PREVISTO"):SetValue(SLD->EMPENHO_PREVISTO)
		oSection1:Cell("EMPENHO_PREVISTO"):SetAlign("LEFT")
	
		oSection1:Cell("EMPENHO_NF"):SetValue(SLD->EMPENHO_NF)
		oSection1:Cell("EMPENHO_NF"):SetAlign("LEFT")
		
		oSection1:Cell("RESERVA"):SetValue(SLD->RESERVA)
		oSection1:Cell("RESERVA"):SetAlign("LEFT")
		
		oSection1:Cell("SALDO_PREVISTO_OP"):SetValue(SLD->SALDO_PREVISTO_OP)
		oSection1:Cell("SALDO_PREVISTO_OP"):SetAlign("LEFT")
			
		oSection1:Cell("SALDO_A_ENDERECAR"):SetValue(SLD->SALDO_A_ENDERECAR)
		oSection1:Cell("SALDO_A_ENDERECAR"):SetAlign("LEFT")
															
		oSection1:PrintLine()
		
		SLD->(DBSKIP()) 
	enddo
	SLD->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Armazem ?"            	,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Filial ? "	            ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
Return
