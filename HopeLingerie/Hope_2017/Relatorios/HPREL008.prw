#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL006 
Relatório de Logs
@author Weskley Silva
@since 03/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL008()

Private oReport
Private cPergCont	:= 'HPREL008' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 03 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'EST1', 'RELATORIO ESTRUTURAS ', , {|oReport| ReportPrint( oReport ), 'RELATORIO ESTRUTURAS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO ESTRUTURAS', { 'EST1', 'SG1'})
			
	TRCell():New( oSection1, 'CODIGO'	    	    ,'EST1', 		'CODIGO',							"@!"                        ,15)
	TRCell():New( oSection1, 'COLECAO'      	    ,'EST1', 		'COLECAO',	    					"@!"				        ,35)
	TRCell():New( oSection1, 'SUBCOLECAO'      	    ,'EST1', 		'SUBCOLECAO',   					"@!"				        ,35)
	TRCell():New( oSection1, 'CICLO'      	        ,'EST1', 		'CICLO',	    					"@!"				        ,35)	
	TRCell():New( oSection1, 'DESCRI'      	        ,'EST1', 		'DESCRI',	    					"@!"				        ,35)
	TRCell():New( oSection1, 'TIPO' 		        ,'EST1', 		'TIPO',              				"@!"						,03)
	TRCell():New( oSection1, 'SITUACAO'         	,'EST1', 		'SITUACAO',	              			"@!"						,10)
	TRCell():New( oSection1, 'GRUPO' 				,'EST1', 		'GRUPO' ,	        				"@!"		                ,06)
	TRCell():New( oSection1, 'COD_PAI'		       	,'EST1', 		'COD_PAI',          				"@!"				        ,15)
	TRCell():New( oSection1, 'DESC_PAI' 			,'EST1', 		'DESC_PAI' ,	    			    "@!"				        ,35)
	TRCell():New( oSection1, 'TIPO_PAI'	        	,'EST1', 		'TIPO_PAI' ,		       			"@!"		    	        ,03)
	TRCell():New( oSection1, 'GRUPO_PAI'			,'EST1', 		'GRUPO_PAI',			   			"@!"						,06)
	TRCell():New( oSection1, 'COD_COMP'				,'EST1', 		'COD_COMP',			   				"@!"						,15)
	TRCell():New( oSection1, 'DESC_COMP'			,'EST1', 		'DESC_COMP',			   			"@!"						,35)
	TRCell():New( oSection1, 'TIPO_COMP'			,'EST1', 		'TIPO_COMP',			   			"@!"						,03)
	TRCell():New( oSection1, 'GRUPO_COMP'			,'EST1', 		'GRUPO_COMP',			   			"@!"						,06)
	TRCell():New( oSection1, 'QTD'					,'EST1', 		'QTD',			   					"@E 999.999,99"				,06)
	TRCell():New( oSection1, 'UM_COMP'				,'EST1', 		'UM_COMP',			   				"@!"						,03)
	TRCell():New( oSection1, 'DT_INI'				,'EST1', 		'DT_INI',			   				"@!"						,08)
	TRCell():New( oSection1, 'DT_FIM'				,'EST1', 		'DT_FIM',			   				"@!"						,08)
	TRCell():New( oSection1, 'NIVEL'				,'EST1', 		'NIVEL',			   				"@!"						,03)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 03 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("EST1") > 0
		EST1->(dbCloseArea())
	Endif
      
    cQuery := " SELECT "  
    cQuery += " VE.CODIGO, "
    cQuery += " SB4.B4_YNCOLEC AS COLECAO, "
    cQuery += " SB4.B4_YSUBCOL AS SUBCOLECAO, "
    cQuery += " SB4.B4_YNCICLO AS CICLO, " 
    cQuery += " VE.DESCRI, " 
    cQuery += " VE.TIPO, " 
    cQuery += " VE.SITUACAO, " 
    cQuery += " VE.GRUPO, " 
    cQuery += " VE.COD_PAI, " 
    cQuery += " VE.DESC_PAI, "  
    cQuery += " VE.TIPO_PAI, " 
    cQuery += " VE.GRUPO_PAI, " 
    cQuery += " VE.COD_COMP, " 
    cQuery += " VE.DESC_COMP, " 
    cQuery += " VE.TIPO_COMP, " 
    cQuery += " VE.GRUPO_COMP, " 
    cQuery += " VE.QTD, " 
    cQuery += " VE.UM_COMP, " 
    cQuery += " RIGHT(VE.DT_INI,2)+'/'+SUBSTRING(VE.DT_INI,5,2)+'/'+LEFT(VE.DT_INI,4) AS DT_INI, " 
    cQuery += " RIGHT(VE.DT_FIM,2)+'/'+SUBSTRING(VE.DT_FIM,5,2)+'/'+LEFT(VE.DT_FIM,4) AS DT_FIM, " 
    cQuery += " VE.NIVEL " 
    cQuery += " FROM V_ESTRUTURA VE " 
    cQuery += " INNER JOIN SB4010 SB4 ON LEFT(VE.CODIGO,8)=SB4.B4_COD " 
	
	IF Empty(mv_par01)
		cQuery += "  "
	Else
		cQuery += " WHERE VE.CODIGO = '"+ALLTRIM(mv_par01)+"' "
	Endif
	
	cQuery += " ORDER BY VE.CODIGO "
	
	TCQUERY cQuery NEW ALIAS EST1

	While EST1->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("CODIGO"):SetValue(EST1->CODIGO)
		oSection1:Cell("CODIGO"):SetAlign("LEFT")
		
		oSection1:Cell("COLECAO"):SetValue(EST1->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBCOLECAO"):SetValue(EST1->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO"):SetValue(EST1->CICLO)
		oSection1:Cell("CICLO"):SetAlign("LEFT")

		oSection1:Cell("DESCRI"):SetValue(EST1->DESCRI)
		oSection1:Cell("DESCRI"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(EST1->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("SITUACAO"):SetValue(EST1->SITUACAO)
		oSection1:Cell("SITUACAO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(EST1->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")
			
		oSection1:Cell("COD_PAI"):SetValue(EST1->COD_PAI)
		oSection1:Cell("COD_PAI"):SetAlign("LEFT")
				
		oSection1:Cell("DESC_PAI"):SetValue(EST1->DESC_PAI)
		oSection1:Cell("DESC_PAI"):SetAlign("LEFT")
				
		oSection1:Cell("TIPO_PAI"):SetValue(EST1->TIPO_PAI)
		oSection1:Cell("TIPO_PAI"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO_PAI"):SetValue(EST1->GRUPO_PAI)
		oSection1:Cell("GRUPO_PAI"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COMP"):SetValue(EST1->COD_COMP)
		oSection1:Cell("COD_COMP"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COMP"):SetValue(EST1->DESC_COMP)
		oSection1:Cell("DESC_COMP"):SetAlign("LEFT")
						
		oSection1:Cell("TIPO_COMP"):SetValue(EST1->TIPO_COMP)
		oSection1:Cell("TIPO_COMP"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO_COMP"):SetValue(EST1->GRUPO_COMP)
		oSection1:Cell("GRUPO_COMP"):SetAlign("LEFT")
		
		oSection1:Cell("QTD"):SetValue(EST1->QTD)
		oSection1:Cell("QTD"):SetAlign("LEFT")
		
		oSection1:Cell("UM_COMP"):SetValue(EST1->UM_COMP)
		oSection1:Cell("UM_COMP"):SetAlign("LEFT")
		
		oSection1:Cell("DT_INI"):SetValue(EST1->DT_INI)
		oSection1:Cell("DT_INI"):SetAlign("LEFT")
		
		oSection1:Cell("DT_FIM"):SetValue(EST1->DT_FIM)
		oSection1:Cell("DT_FIM"):SetAlign("LEFT")
		
		oSection1:Cell("NIVEL"):SetValue(EST1->NIVEL)
		oSection1:Cell("NIVEL"):SetAlign("LEFT")
									
		oSection1:PrintLine()
		
		EST1->(DBSKIP()) 
	enddo
	EST1->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 03 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Cod Produto Pai ?"		        ,""		,""		,"mv_ch1","C",15,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	
Return