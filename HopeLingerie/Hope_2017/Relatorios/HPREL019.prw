#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL019 
Saldo por Armazem
@author Weskley Silva
@since 13/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL019()

Private oReport
Private cPergCont	:= 'HPREL019' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'BXC', 'CONSUMO X BAIXA', , {|oReport| ReportPrint( oReport ), 'CONSUMO X BAIXA' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'CONSUMO X BAIXA', { 'BXC', 'SD4','SC2','SB1','SBV'})
	
	
	TRCell():New( oSection1, 'TIPO_OP'				,'BXC', 		'TIPO_OP',			  			    "@!"				        ,35)		
	TRCell():New( oSection1, 'DATA_OP'				,'BXC', 		'DATA_OP',						    "@!"                        ,10)
	TRCell():New( oSection1, 'OP'					,'BXC', 		'OP',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'OP_SEQUEN'			,'BXC', 		'OP_SEQUEN',		  			    "@!"				        ,15)
	TRCell():New( oSection1, 'TIPO'					,'BXC', 		'TIPO',				  	     	 	"@!"		 		        ,06)
	TRCell():New( oSection1, 'GRUPO'				,'BXC', 		'GRUPO',			  	     	 	"@!"		 		        ,25)
	TRCell():New( oSection1, 'SKU'				    ,'BXC', 		'SKU',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'REFERENCIA'			,'BXC', 		'REFERENCIA',		  			    "@!"				        ,25)
	TRCell():New( oSection1, 'COD_COR'				,'BXC', 		'COD_COR',			  			    "@!"				        ,06)
	TRCell():New( oSection1, 'DESC_COR'				,'BXC', 		'DESC_COR',			  			    "@!"				        ,06)
	TRCell():New( oSection1, 'TAM'					,'BXC', 		'TAM',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'DESCRICAO'			,'BXC', 		'DESCRICAO',		  			    "@!"				        ,06)
	TRCell():New( oSection1, 'UNID_MED'				,'BXC', 		'UNID_MED',		     			    "@!"				        ,06)
	TRCell():New( oSection1, 'TOTAL_EMPENHO'		,'BXC', 		'TOTAL_EMPENHO',				    "@E 999.999,99"			    ,06)
	TRCell():New( oSection1, 'TOTAL_BAIXA' 			,'BXC', 		'TOTAL_BAIXA',					    "@E 999.999,99"			    ,06)

			
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("BXC") > 0
		BXC->(dbCloseArea())
	Endif
      
    cQuery := " SELECT "  
    cQuery += " RIGHT(SD4.D4_DATA,2)+'/'+SUBSTRING(SD4.D4_DATA,5,2)+'/'+LEFT(SD4.D4_DATA,4) AS DATA_OP, " 
    cQuery += " LEFT(D4_OP,6) as OP, "  
    cQuery += " C2_TPOP AS TIPO_OP, " 
    cQuery += " D4_OP AS OP_SEQUEN, " 
    cQuery += " D4_COD AS SKU, " 
    cQuery += " LEFT (D4_COD,8) AS REFERENCIA, " 
    cQuery += " SB1.B1_DESC AS DESCRICAO, " 
    cQuery += " SBM.BM_DESC AS GRUPO,  "
    cQuery += " LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AS COD_COR, " 
    cQuery += " SBV.BV_DESCRI AS DESC_COR, " 
    cQuery += " RIGHT(SB1.B1_COD,4)  AS TAM, "
    cQuery += " SB1.B1_UM AS UNID_MED, "
    cQuery += " B1_TIPO AS TIPO,  "
    cQuery += " D4_QTDEORI AS TOTAL_EMPENHO,  "
    cQuery += " D3_QUANT AS TOTAL_BAIXA "
    cQuery += " FROM "+RetSqlName('SD4')+" SD4
    cQuery += " JOIN "+RetSqlName('SC2')+" SC2 ON SC2.C2_NUM+SC2.C2_ITEM+SC2.C2_SEQUEN+SC2.C2_ITEMGRD=SD4.D4_OP "
    cQuery += " JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_COD=D4_COD AND SB1.D_E_L_E_T_ = ''  "
    cQuery += " JOIN "+RetSqlName('SBV')+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_ = '' "
    cQuery += " JOIN "+RetSqlName('SBM')+" SBM ON SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN "+RetSqlName('SD3')+" SD3 ON SD4.D4_COD=SD3.D3_COD AND SD4.D4_OP=SD3.D3_OP AND SD3.D_E_L_E_T_ = '' "
    cQuery += " WHERE SD4.D_E_L_E_T_='' " 
    
      
	if !Empty(mv_par01)
		cQuery += " AND LEFT(D4_COD,8 )= '"+Alltrim(mv_par01)+"'  " 
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par02) 
		cQuery += " AND SC2.C2_NUM = '"+Alltrim(mv_par02)+"' "
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par03)
		cQuery += " AND C2_TPOP = '"+Alltrim(mv_par03)+"' "
	else 
		cQuery += " "
	endif
	
	if !Empty(mv_par04)
		cQuery += " AND SD4.D4_DATA BETWEEN '"+ DTOS(mv_par04)+ "' AND '"+ DTOS(mv_par05)+ "'  "
	else 
		cQuery += " "
	endif
	
	
	TCQUERY cQuery NEW ALIAS BXC

	While BXC->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		
		oSection1:Cell("TIPO_OP"):SetValue(BXC->TIPO_OP)
		oSection1:Cell("TIPO_OP"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_OP"):SetValue(BXC->DATA_OP)
		oSection1:Cell("DATA_OP"):SetAlign("LEFT")

		oSection1:Cell("OP"):SetValue(BXC->OP)
		oSection1:Cell("OP"):SetAlign("LEFT")
		
		oSection1:Cell("OP_SEQUEN"):SetValue(BXC->OP_SEQUEN)
		oSection1:Cell("OP_SEQUEN"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(BXC->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(BXC->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(BXC->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
				
		oSection1:Cell("REFERENCIA"):SetValue(BXC->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(BXC->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(BXC->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(BXC->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(BXC->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
			
		oSection1:Cell("UNID_MED"):SetValue(BXC->UNID_MED)
		oSection1:Cell("UNID_MED"):SetAlign("LEFT")
		
		oSection1:Cell("TOTAL_EMPENHO"):SetValue(BXC->TOTAL_EMPENHO)
		oSection1:Cell("TOTAL_EMPENHO"):SetAlign("LEFT")
		
		oSection1:Cell("TOTAL_BAIXA"):SetValue(BXC->TOTAL_BAIXA)
		oSection1:Cell("TOTAL_BAIXA"):SetAlign("LEFT")
		
		oSection1:PrintLine()
		
		BXC->(DBSKIP()) 
	enddo
	BXC->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Referencia ?"           ,""		,""		,"mv_ch1","C",15,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","OP ? "	                ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","TIPO OP ? "	            ,""		,""	    ,"mv_ch3","C",01,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Periodo de ? "	        ,""		,""		,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Periodo Ate ? "	        ,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return
