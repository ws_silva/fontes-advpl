#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL010 
Roteiros cadastrados 
@author Weskley Silva
@since 10/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL016()

Private oReport
Private cPergCont	:= 'HPREL016' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'FLW', 'FOLLOW-UP DE COMPRAS', , {|oReport| ReportPrint( oReport ), 'FOLLOW-UP DE COMPRAS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'FOLLOW-UP DE COMPRAS', { 'FLW', 'SC1','SB4','CTT','SBV','SBM','SC7','SA2'})
			
	TRCell():New( oSection1, 'FILIAL_PEDIDO'		,'FLW', 		'FILIAL_PEDIDO',				    "@!"                        ,10)
	TRCell():New( oSection1, 'FILIAL_ENTREGA'		,'FLW', 		'FILIAL_ENTREGA',	  			    "@!"				        ,10)
	TRCell():New( oSection1, 'TIPO'					,'FLW', 		'TIPO',				  			    "@!"				        ,04)
	TRCell():New( oSection1, 'SKU'					,'FLW', 		'SKU',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'PRODUTO'				,'FLW', 		'PRODUTO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'DESCRICAO'			,'FLW', 		'DESCRICAO',		  			    "@!"				        ,35)
	TRCell():New( oSection1, 'COD_COR'				,'FLW', 		'COD_COR',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DESC_COR'				,'FLW', 		'DESC_COR',			  			    "@!"				        ,35)
	TRCell():New( oSection1, 'TAM'					,'FLW', 		'TAM',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'UNID_MEDIDA'			,'FLW', 		'UNID_MEDIDA',		  			    "@!"				        ,04)
	TRCell():New( oSection1, 'SOLICITACAO'			,'FLW', 		'SOLICITACAO',		  			    "@!"				        ,10)
	TRCell():New( oSection1, 'SOLICITANTE'			,'FLW', 		'SOLICITANTE',		  			    "@!"				        ,25)
	TRCell():New( oSection1, 'DT_EMISSAO_SC'		,'FLW', 		'DT_EMISSAO_SC',     			    "@!"				        ,10)
	TRCell():New( oSection1, 'QTDE_SOLICITADA'		,'FLW', 		'QTDE_SOLICITADA',	  			    "@E 999,99"			        ,06)
	TRCell():New( oSection1, 'PEDIDO'				,'FLW', 		'PEDIDO',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'STATUS_APROVACAO'		,'FLW', 		'STATUS_APROVACAO',	  			    "@!"				        ,06)
	TRCell():New( oSection1, 'FORNECEDOR'			,'FLW', 		'FORNECEDOR',		  			    "@!"				        ,35)
	TRCell():New( oSection1, 'CCUSTO'				,'FLW', 		'CCUSTO',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DT_EMISSAO_PED'		,'FLW', 		'DT_EMISSAO_PED',	  			    "@!"				        ,08)
	TRCell():New( oSection1, 'VLR_TOTAL_ITEM'		,'FLW', 		'VLR_TOTAL_ITEM',	  			    "@E 99.999,99"		        ,08)
	TRCell():New( oSection1, 'QTDE_PEDIDO'			,'FLW', 		'QTDE_PEDIDO',		  			    "@E 99.999,99"		        ,08)
	TRCell():New( oSection1, 'DATA_ENTREGA'			,'FLW', 		'DATA_ENTREGA',		  			    "@!"				        ,08)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'		,'FLW', 		'QTDE_ENTREGUE',	  			    "@E 999,99"	           		,06)
	TRCell():New( oSection1, 'SALDO'				,'FLW', 		'SALDO',			  			    "@E 99.999,99"		        ,08)
	TRCell():New( oSection1, 'CUSTO'				,'FLW', 		'CUSTO',			  			    "@E 99.999,99"		        ,08)
	TRCell():New( oSection1, 'QUITADO'				,'FLW', 		'QUITADO',			  			    "@!"				        ,06)
		
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("FLW") > 0
		FLW->(dbCloseArea())
	Endif
      
    cQuery := " SELECT " 
	cQuery += " SC1.C1_FILIAL AS FILIAL_PEDIDO, "  
	cQuery += " SC7.C7_FILENT AS FILIAL_ENTREGA, " 
	cQuery += " SB4.B4_TIPO AS TIPO, " 
	cQuery += " SC1.C1_PRODUTO AS SKU, "  
	cQuery += " SB4.B4_COD AS PRODUTO, "
	cQuery += " SB4.B4_DESC AS DESCRICAO, "
	cQuery += " SUBSTRING(SC1.C1_PRODUTO,9,3) AS COD_COR, " 
	cQuery += " SBV.BV_DESCRI AS DESC_COR, "
	cQuery += " RIGHT(SC1.C1_PRODUTO,4)  AS TAM, "
	cQuery += " SB4.B4_UM AS UNID_MEDIDA, "
	cQuery += " SC1.C1_NUM AS SOLICITACAO, "
	cQuery += " SC1.C1_SOLICIT AS SOLICITANTE, "
	cQuery += " RIGHT(SC1.C1_EMISSAO,2)+'/'+SUBSTRING(SC1.C1_EMISSAO,5,2)+'/'+LEFT(SC1.C1_EMISSAO,4) AS DT_EMISSAO_SC, " 
	cQuery += " SC1.C1_QUANT AS QTDE_SOLICITADA, "
	cQuery += " SC7.C7_NUM AS PEDIDO, "
	cQuery += " SC7.C7_CONAPRO AS STATUS_APROVACAO, " 
	cQuery += " SA2.A2_NOME AS FORNECEDOR, "
	cQuery += " CTT.CTT_DESC01 AS CCUSTO, "
	cQuery += " RIGHT(SC7.C7_EMISSAO,2)+'/'+SUBSTRING(SC7.C7_EMISSAO,5,2)+'/'+LEFT(SC7.C7_EMISSAO,4) AS DT_EMISSAO_PED, "
	cQuery += " SC7.C7_TOTAL AS VLR_TOTAL_ITEM, "
	cQuery += " SC7.C7_QUANT AS QTDE_PEDIDO, "
	cQuery += " RIGHT(SC7.C7_DATPRF,2)+'/'+SUBSTRING(SC7.C7_DATPRF,5,2)+'/'+LEFT(SC7.C7_DATPRF,4) AS DATA_ENTREGA, "
	cQuery += " SC7.C7_QUJE AS QTDE_ENTREGUE , "
	cQuery += " (SC7.C7_QUANT - SC7.C7_QUJE) AS SALDO, "
	cQuery += " (SC7.C7_TOTAL / SC7.C7_QUANT ) AS CUSTO, "
	cQuery += " SC7.C7_RESIDUO AS QUITADO  " 
	cQuery += " FROM "+RetSqlName('SC1')+" SC1 "
	cQuery += " JOIN "+RetSqlName('SB4')+" SB4 ON SB4.B4_COD=LEFT(SC1.C1_PRODUTO,8) AND SB4.D_E_L_E_T_='' " 
	cQuery += " LEFT JOIN "+RetSqlName('CTT')+" CTT ON CTT.CTT_CUSTO=SC1.C1_CC "
	cQuery += " JOIN "+RetSqlName('SBV')+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC1.C1_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
	cQuery += " JOIN "+RetSqlName('SBM')+" SBM ON SBM.BM_GRUPO=SB4.B4_GRUPO AND SBM.D_E_L_E_T_='' "
	cQuery += " LEFT JOIN "+RetSqlName('SC7')+" SC7 ON SC7.C7_NUMSC=SC1.C1_NUM AND SC7.C7_ITEMSC=SC1.C1_ITEM AND SC7.C7_FILIAL=SC1.C1_FILIAL AND  SC7.D_E_L_E_T_='' " 
	cQuery += " JOIN SA2010 SA2 ON SA2.A2_COD=SC7.C7_FORNECE "
	cQuery += " WHERE SC1.D_E_L_E_T_= ''  " 
	
	if !Empty(mv_par04)
		cQuery += " AND SC1.C1_EMISSAO BETWEEN '"+ DTOS(mv_par04) +"' AND '"+ DTOS(mv_par05) +"' " 
	else 
		cQuery += " "
	endif
	
	if !Empty(mv_par02)
		cQuery += " OR SC1.C1_NUM = '"+Alltrim(mv_par02)+"' " 
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par01)
		cQuery += " OR B4_TIPO = '"+Alltrim(mv_par01)+"'  " 
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par03) 
		cQuery += " OR B4_COD = '"+Alltrim(mv_par03)+"' "
	else
		cQuery += " "
	endif
	
	
	TCQUERY cQuery NEW ALIAS FLW

	While FLW->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL_PEDIDO"):SetValue(FLW->FILIAL_PEDIDO)
		oSection1:Cell("FILIAL_PEDIDO"):SetAlign("LEFT")

		oSection1:Cell("FILIAL_ENTREGA"):SetValue(FLW->FILIAL_ENTREGA)
		oSection1:Cell("FILIAL_ENTREGA"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(FLW->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(FLW->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(FLW->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(FLW->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(FLW->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(FLW->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(FLW->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("UNID_MEDIDA"):SetValue(FLW->UNID_MEDIDA)
		oSection1:Cell("UNID_MEDIDA"):SetAlign("LEFT")
		
		oSection1:Cell("SOLICITACAO"):SetValue(FLW->SOLICITACAO)
		oSection1:Cell("SOLICITACAO"):SetAlign("LEFT")
		
		oSection1:Cell("SOLICITANTE"):SetValue(FLW->SOLICITANTE)
		oSection1:Cell("SOLICITANTE"):SetAlign("LEFT")
		
		oSection1:Cell("DT_EMISSAO_SC"):SetValue(FLW->DT_EMISSAO_SC)
		oSection1:Cell("DT_EMISSAO_SC"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_SOLICITADA"):SetValue(FLW->QTDE_SOLICITADA)
		oSection1:Cell("QTDE_SOLICITADA"):SetAlign("LEFT")
		
		oSection1:Cell("PEDIDO"):SetValue(FLW->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("STATUS_APROVACAO"):SetValue(FLW->STATUS_APROVACAO)
		oSection1:Cell("STATUS_APROVACAO"):SetAlign("LEFT")
		
		oSection1:Cell("FORNECEDOR"):SetValue(FLW->FORNECEDOR)
		oSection1:Cell("FORNECEDOR"):SetAlign("LEFT")
		
		oSection1:Cell("CCUSTO"):SetValue(FLW->CCUSTO)
		oSection1:Cell("CCUSTO"):SetAlign("LEFT")
		
		oSection1:Cell("DT_EMISSAO_PED"):SetValue(FLW->DT_EMISSAO_PED)
		oSection1:Cell("DT_EMISSAO_PED"):SetAlign("LEFT")
		
		oSection1:Cell("VLR_TOTAL_ITEM"):SetValue(FLW->VLR_TOTAL_ITEM)
		oSection1:Cell("VLR_TOTAL_ITEM"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PEDIDO"):SetValue(FLW->QTDE_PEDIDO)
		oSection1:Cell("QTDE_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_ENTREGA"):SetValue(FLW->DATA_ENTREGA)
		oSection1:Cell("DATA_ENTREGA"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_ENTREGUE"):SetValue(FLW->QTDE_ENTREGUE)
		oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
		
		oSection1:Cell("SALDO"):SetValue(FLW->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("CUSTO"):SetValue(FLW->CUSTO)
		oSection1:Cell("CUSTO"):SetAlign("LEFT")
		
		oSection1:Cell("QUITADO"):SetValue(FLW->QUITADO)
		oSection1:Cell("QUITADO"):SetAlign("LEFT")		
												
		oSection1:PrintLine()
		
		FLW->(DBSKIP()) 
	enddo
	FLW->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Tipo de Produto?"            	,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Solicitação ?"		    		,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Referencia ? "	                ,""		,""		,"mv_ch3","C",15,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Data da Solicitação de?"	    ,""		,""		,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data da Solicitação ate?"	    ,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return
