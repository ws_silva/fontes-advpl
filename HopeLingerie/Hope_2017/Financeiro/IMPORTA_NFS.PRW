#INCLUDE "PROTHEUS.CH"
/*
Le arquivo CSV (planilha com dados de titulos a pagar
Conforme seguencia abaixo:
PREFIXO	NUMERO PARCELA TIPO	NATUREZA PORTADOR FORNECEDOR LOJA NOME_FORNECEDOR EMISSAO VENCIMENTO VALOR CODIGO_DE_BARRAS	HISTORICO
PREFIXO	NUMERO DOC	PARCELA	TIPO	NATUREZA	PORTADOR	AGENCIA	CODIGO CLIENTE	LOJA	NOME	EMISSAO	VENCIENTO	VALOR	CENTRO CUSTO	EMISSAO	HISTORICO

1	             2	       3	      4	      5      	6	          7	            8	     9	10	       11	   12      	13	      14        	15	     16	           17	        18	       19	   20	      21	       22	      23	      24	      25	   26	       27	       28	          29	           30	  31	   32	      33	        34	       35	    36	       37	        38	        39	       40	        41	        42	        43	       44	      45	         46
E1_PREFIXO	N_DOCUMENTO	CHECK NF	CONTA	E1_NUM	E1_PARCELA	CODIGO_PROTHEUS	E1_NATUREZA	CPF	CNPJ	E1_LOJA	BLABAL	E1_EMISSAO	E1_VENCTO	E1_VENCREA	E1_VALOR	E1_NUMBCO	E1_HIST	E1_STUACA	E1_SALDO	E1_DESCONT	E1_MULTA	E1_JUROS	CUSTAS	E1_NUMNOTA	E1_VALLIQ	E1_IDCNAB	CODIGO_MILLENIUM	E1_XCOD	E1_XNSU	E1_XAUTNSU	E1_XCANAL	E1_XCANALD	E1_XEMP	E1_XDESEMP	E1_XGRUPO	E1_XGRUPON	E1_XCODDIV	E1_XNONDIV	E1_XMICRRE	E1_XMICRDE	E1_XCODREG	E1_XDESREG	E1_XFORREC	TIPO_GERADOR	EVENTO


*/
USER FUNCTION IMPNFS()  //U_IMPSE1()
IF MsgYesno("Confirma importa��o do Cabe�alho de Notas Fiscais de Saidas? " )
	Processa({|| u_IMPNFSC()})
ElseIF MsgYesno("Confirma Importa��o dos Itens de Notas Fiscais de Saidas? " )
	Processa({|| u_IMPNFSI()})
Endif

Return

USER FUNCTION IMPNFSC()
Local aSF2      := {}
Local aCrateio  := {}
Local aErros    := {}
Local lSim      := .T.
Local nlin      := 0
Local aRet      := {} //
Local cTexto    := ""
Local cxTexto   := ""
Local NPARC     := 0
PRIVATE lMsHelpAuto := .F.
PRIVATE lMsErroAuto := .F.


nVisibilidade := Nil													// Exibe todos os diret�rios do servidor e do cliente
//		nVisibilidade := GETF_ONLYSERVER										// Somente exibe o diret�rio do servidor
//		nVisibilidade := GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE	// Somente exibe o diret�rio do cliente

cArquivo := cGetFile("Todos os Arquivos ()*.*)     | *.* | ","Selecione um arquivo",0,,.T.,nVisibilidade, .T.)
nHandle := FT_FUse(cArquivo)
if nHandle = -1
	MsgStop(" Problemas na abertura do Arquivo")
	return
endif
nLinTot := FT_FLastRec()
Procregua(nLinTot)
FT_FGoTop()
While !FT_FEOF()
	cLine := FT_FReadLn()
	if Len(alltrim(cLine))== 0
		FT_FSKIP()
		Loop
	Endif
	nRecno := FT_FRecno()
	cLinha := StrTran(cLine,'"',"'")
	cLinha := '{"'+cLinha+'"}'
	//adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
	cLinha := StrTran(cLinha,';','","')
	aAdd(aRet, &cLinha)
	nlin++
	IncProc("Processando  Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)) )
	//If nlin >= 2
	If alltrim(aRet[1][1]) == '0101' .or. alltrim(aRet[1][1]) == '0102'  
	   cCgc := alltrim(aRet[1][04])	
	   cCliente   :=  Posicione("SA1",3,xFilial("SA1 ")+PADR(cCgc, 14),"A1_COD")
       cA1LOJA    := SA1->A1_LOJA
	   NomCli     := SA1->A1_NOME
       xcFIlial   := alltrim(aRet[1][01])
	   cDoc       := alltrim(aRet[1][02])
	   cSerie     := alltrim(aRet[1][03])
       cCond      := alltrim(aRet[1][06])
       cDUPL      := alltrim(aRet[1][07])	
       dEMISSAO   := alltrim(aRet[1][08])
       cEST	      := alltrim(aRet[1][09])
       cTIPOCLI	  := alltrim(aRet[1][10])
       nPBRUTO	  := PegaVal(alltrim(aRet[1][11]))
       nVALICM	  := PegaVal(alltrim(aRet[1][12]))
       nBASEICM	  := PegaVal(alltrim(aRet[1][13]))
       nVALOMERCA := PegaVal(alltrim(aRet[1][14]))
       nDESCONT	  := PegaVal(alltrim(aRet[1][15]))
       cTRANSP	  := alltrim(aRet[1][16])
       cCHVNFE    := alltrim(aRet[1][17])
 	   
 	   	//F2_FILIAL	F2_DOC	  F2_SERIE	F2_CLIENTE	F2_LOJA	F2_FORMUL	F2_TIPO
        //    0101	000000046	1     	157433	    0001            	 	N   
 	   
 	   dbSelectArea("SF2")
	   SF2->(dbSetOrder(1))   //F2_FILIAL+F2_DOC+F2_SERIE+F2_CLIENTE+F2_LOJA+F2_FORMUL+F2_TIPO
	   If SF2->(!dbSeek(xFilial("SF2")+Padr(Alltrim(cDoc),9)+Padr(Alltrim(cSerie),3)+Padr(Alltrim(cCliente),6)+Padr(Alltrim(cA1LOJA),4)+Space(1)+'N' ))
	   	  SF2->(RecLock("SF2",.T.))

			SF2->F2_FILIAL   := xcFilial
			SF2->F2_DOC      := cDoc
			SF2->F2_SERIE    := cSerie
			SF2->F2_CLIENTE  := cCliente
			SF2->F2_LOJA     := cA1LOJA
			SF2->F2_FORMUL   := 'N'
			SF2->F2_COND     := cCond
			SF2->F2_DUPL     := cDUPL         
			SF2->F2_EMISSAO  := ctod(substr(dEmissao,7,2)+'/'+substr(dEmissao,5,2)+'/'+substr(dEmissao,3,2))   
 			SF2->F2_EST      := cEst
			SF2->F2_TIPOCLI  := cTipoCli
			SF2->F2_PBRUTO   := nPBRUTO
			SF2->F2_VALICM   := nVALICM
			SF2->F2_BASEICM  := nBASEICM
			SF2->F2_VALBRUT  := nVALOMERCA
			SF2->F2_VALMERC  := nVALOMERCA
			SF2->F2_VALFAT   := nVALOMERCA
            SF2->F2_ESPECI1  := 'VOLUME'
            SF2->F2_VOLUME1  := 1
            SF2->F2_PREFIXO  := cSerie
            SF2->F2_CLIENTE  := cCliente
            SF2->F2_LOJENT   := cA1LOJA
            SF2->F2_LOJA     := cA1LOJA
			SF2->F2_DESCONT  := nDESCONT
			SF2->F2_TRANSP   := cTRANSP
			SF2->F2_CHVNFE   := cCHVNFE			
			SF2->(MsUnlock())
		Endif
	Endif
	// Pula para pr�xima linha
	FT_FSKIP()
	aRet      := {}
Enddo
Return()

USER FUNCTION IMPNFSI()
Local aSD2      := {}
Local aCrateio  := {}
Local aErros    := {}
Local lSim      := .T.
Local nlin      := 0
Local aRet      := {} //
Local cTexto    := ""
Local cxTexto   := ""
Local NPARC     := 0
PRIVATE lMsHelpAuto := .F.
PRIVATE lMsErroAuto := .F.


nVisibilidade := Nil													// Exibe todos os diret�rios do servidor e do cliente
//		nVisibilidade := GETF_ONLYSERVER										// Somente exibe o diret�rio do servidor
//		nVisibilidade := GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE	// Somente exibe o diret�rio do cliente

cArquivo := cGetFile("Todos os Arquivos ()*.*)     | *.* | ","Selecione um arquivo",0,,.T.,nVisibilidade, .T.)
nHandle := FT_FUse(cArquivo)
if nHandle = -1
	MsgStop(" Problemas na abertura do Arquivo")
	return
endif
nLinTot := FT_FLastRec()
Procregua(nLinTot)
FT_FGoTop()
While !FT_FEOF()
	cLine := FT_FReadLn()
	if Len(alltrim(cLine))== 0
		FT_FSKIP()
		Loop
	Endif
	nRecno := FT_FRecno()
	cLinha := StrTran(cLine,'"',"'")
	cLinha := '{"'+cLinha+'"}'
	//adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
	cLinha := StrTran(cLinha,';','","')
	aAdd(aRet, &cLinha)
	nlin++
	IncProc("Processando  Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)) )
	//If nlin >= 2
	If alltrim(aRet[1][1]) == '0101' .or. alltrim(aRet[1][1]) == '0102'
	   cCgc := alltrim(aRet[1][15])	
	   _CLIENTE  :=  Posicione("SA1",3,xFilial("SA1 ")+PADR(cCgc, 14),"A1_COD")
       cA1LOJA   := SA1->A1_LOJA
	   NomCli    := SA1->A1_NOME
	   cEst      := SA1->A1_EST
	   DbselectArea("SB1")
	   SB1->(DbSetorder(5)) // B1_FILIAL+B1_CODBAR
	   If SB1->(DbSeek(xFilial("SB1")+alltrim(aRet[1][3])))      
	      _COD       := SB1->B1_COD    
	      _FILIAL    := alltrim(aRet[1][1])
	      _ITEM	     := PEGAITEM(alltrim(aRet[1][2]))
	      _SEGUM	 := SB1->B1_SEGUM //alltrim(aRet[1][4])
	      _UM	     := alltrim(aRet[1][5])
	      _QUANT	 := VAL(alltrim(aRet[1][06])) 
	      _PRCVEN	 := PegaVal(alltrim(aRet[1][07]))
	      _TOTAL	 := PegaVal(alltrim(aRet[1][08]))
	      _VALIPI	 := PegaVal(alltrim(aRet[1][09]))
	      _VALICM	 := PegaVal(alltrim(aRet[1][10]))
	      _CF	     := alltrim(aRet[1][12])
	      If Select("DTMP") > 0 
		     DbSelectArea("DTMP")
		     DTMP->(DbCloseArea())
	      EndIf
	      cQuery     := "SELECT * FROM SF4010 WHERE SUBSTRING(F4_CF,2,3) = SUBSTRING('"+_CF+"',2,3) AND SUBSTRING(F4_CF,1,1) >= '5' "
	      cQuery     := ChangeQuery(cQuery)
          DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"DTMP", .F., .T.)       
        
          iF DTMP->(!EOF())
             _TES	 := DTMP->F4_CODIGO
          Else 
             _TES    := '501'
          eNDIF      
	      _PICM	 := PegaVal(alltrim(aRet[1][13]))
	      _CONTA	 := SB1->B1_CONTA //alltrim(aRet[1][14])
	      _LOCAL	 := 'E0'  //alltrim(aRet[1][16])
	      _DOC	     := alltrim(aRet[1][17])
	      _SERIE	 := alltrim(aRet[1][18])
	      _EMISSAO   := alltrim(aRet[1][19])
	      DbselectArea("SD2")
	      SD2->(DbSetOrder(3)) //D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
	      If SD2->(!DbSeek(_FILIAL+_DOC+padr(_SERIE,3)+_CLIENTE+cA1LOJA+_COD+_ITEM))
	         SD2->(RecLock("SD2",.T.))
	         SD2->D2_FILIAL    := _FILIAL  	
	         SD2->D2_ITEM	   := _ITEM
	         SD2->D2_COD	   := _COD
	         SD2->D2_SEGUM	   := _SEGUM
	         SD2->D2_UM	       := _UM
	         SD2->D2_QUANT	   := _QUANT
	         SD2->D2_PRCVEN	   := _PRCVEN
	         SD2->D2_TOTAL	   := _TOTAL
	         SD2->D2_VALIPI	   := _VALIPI
	         SD2->D2_VALICM	   := _VALICM
	         SD2->D2_TES	   := _TES
	         SD2->D2_CF	       := _CF
	         SD2->D2_PICM	   := _PICM
	         SD2->D2_CONTA	   := _CONTA
	         SD2->D2_CLIENTE   := _CLIENTE
	         SD2->D2_LOJA      := cA1LOJA
	         SD2->D2_LOCAL     := 'E0'
	         SD2->D2_DOC	   := _DOC
	         SD2->D2_SERIE	   := _SERIE
	         SD2->D2_EMISSAO   := ctod(substr(_Emissao,7,2)+'/'+substr(_Emissao,5,2)+'/'+substr(_Emissao,3,2))
	         SD2->D2_GRUPO     := SB1->B1_GRUPO
	         SD2->D2_TP        := SB1->B1_TIPO 
	         SD2->D2_PRUNIT    := _PRCVEN
	         SD2->D2_NUMSEQ    := SUBSTR(_DOC,4,6)+_ITEM 
	         SD2->D2_EST       := cEst   
	         SD2->D2_TIPO      := SB1->B1_TIPO
	         SD2->D2_DTVALID   := ctod(substr(_Emissao,7,2)+'/'+substr(_Emissao,5,2)+'/'+substr(_Emissao,3,2))
 	         SD2->D2_ALQIMP5   := 7.6
	         SD2->D2_ALQIMP6   := 1.65
	         SD2->D2_VALBRUT   := _TOTAL
	         SD2->D2_ESTOQUE   := 'N'
	  	     SD2->(MsUnlock()) 
		  Endif
	   Endif	  
	Endif
	// Pula para pr�xima linha
	FT_FSKIP()
	aRet      := {}
Enddo
Return()


Static Function RetVal(n_xValor)
Local nRet := 0
If n_xValor < 0
	nRet := n_xValor*-1
Else
	nRet := n_xValor
Endif
Return(nRet)



Static Function PegaVal(xValor)
Local nRet  := 0 
Local cVal  := 0 //Substr(alltrim(xValor),3,16) 
Local cVa2  := 0
Local cVa3  := 0
Local cVa4  := 0
Local npos1 := 0
Local npos2 := 0
if substr(alltrim(xvalor),1,2) == 'R$'
	cVal  := Substr(alltrim(xValor),3,16)
Else
	cVal := alltrim(xValor)
Endif    
  
faz := .t.
do while  faz 
    npos1:=at('.',cVal)
	if npos1 >= 1 
		cVal := substr(cVal,1,npos1-1)+substr(cVal,npos1+1,16)
	ELse
		npos2 := at(",",cVal)
		if npos2 >= 1 
			cVal:=substr(cVal,1,npos2-1)+substr(cVal,npos2+1,2)
		Else
			nRet:= val(cVal)/100
			faz := .f.
		Endif 
	Endif  
Enddo
Return(nRet)

Static Function PEGAITEM(xItem)
Local cRet   := ''
Local V1     := ''
Local V2     := ''
Local cvAB   := 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
//               12345678901234567890123456
//                        1         2
Local nValor := val(xItem)
Local xItem  := val(xItem)
If nvalor <= 99
   cRet := strzero(nvalor,2)
   
Else

   DO CASE
      CASE xItem >= 100 .and. xItem <= 125    
           cRet := 'A'+substr(cvAB,xItem-99,1)
      CASE xItem >= 126 .and. xItem <= 151    
           cRet := 'B'+substr(cvAB,xItem-125,1)
      CASE xItem >= 152 .and. xItem <= 177    
           cRet := 'C'+substr(cvAB,xItem-151,1)
      CASE xItem >= 178 .and. xItem <= 203    
           cRet := 'D'+substr(cvAB,xItem-177,1)
      CASE xItem >= 204 .and. xItem <= 229    
           cRet := 'E'+substr(cvAB,xItem-203,1)
      CASE xItem >= 230 .and. xItem <= 255    
           cRet := 'F'+substr(cvAB,xItem-229,1)
      CASE xItem >= 256 .and. xItem <= 281    
           cRet := 'G'+substr(cvAB,xItem-255,1)
      CASE xItem >= 282 .and. xItem <= 307    
           cRet := 'H'+substr(cvAB,xItem-281,1)
      CASE xItem >= 308 .and. xItem <= 333    
           cRet := 'I'+substr(cvAB,xItem-307,1)
      CASE xItem >= 334 .and. xItem <= 359    
           cRet := 'J'+substr(cvAB,xItem-333,1)
      CASE xItem >= 360 .and. xItem <= 385    
           cRet := 'K'+substr(cvAB,xItem-359,1)
      CASE xItem >= 386 .and. xItem <= 411    
           cRet := 'L'+substr(cvAB,xItem-385,1)
      CASE xItem >= 412 .and. xItem <= 437    
           cRet := 'M'+substr(cvAB,xItem-411,1)
      CASE xItem >= 438 .and. xItem <= 463    
           cRet := 'N'+substr(cvAB,xItem-437,1)
      CASE xItem >= 464 .and. xItem <= 489    
           cRet := 'O'+substr(cvAB,xItem-463,1)
      CASE xItem >= 490 .and. xItem <= 515    
           cRet := 'P'+substr(cvAB,xItem-489,1)
      CASE xItem >= 516 .and. xItem <= 541    
           cRet := 'Q'+substr(cvAB,xItem-515,1)
      CASE xItem >= 542 .and. xItem <= 567    
           cRet := 'R'+substr(cvAB,xItem-541,1)
      CASE xItem >= 568 .and. xItem <= 593    
           cRet := 'S'+substr(cvAB,xItem-567,1)
      CASE xItem >= 594 .and. xItem <= 619    
           cRet := 'T'+substr(cvAB,xItem-593,1)
      CASE xItem >= 620 .and. xItem <= 645    
           cRet := 'U'+substr(cvAB,xItem-619,1)
       CASE xItem >= 646 .and. xItem <= 671    
           cRet := 'V'+substr(cvAB,xItem-645,1)
       CASE xItem >= 672 .and. xItem <= 697    
           cRet := 'W'+substr(cvAB,xItem-671,1)
       CASE xItem >= 698 .and. xItem <= 723    
           cRet := 'X'+substr(cvAB,xItem-697,1)
       CASE xItem >= 724 .and. xItem <= 749    
           cRet := 'Y'+substr(cvAB,xItem-723,1)
       CASE xItem >= 750 .and. xItem <= 775    
           cRet := 'Z'+substr(cvAB,xItem-749,1)
    ENDCASE
ENDIF
Return cRet
           
 
 
 