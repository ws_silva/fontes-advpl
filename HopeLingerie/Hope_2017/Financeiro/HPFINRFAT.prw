#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"  
#INCLUDE "RWMAKE.CH"  

/*/{Protheus.doc} RF013 
Relat髍io de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

//User function FINDER1A()
//Local aArea := GetArea()
//If MsgYesNo("Confirma a impress鉶 do Relat髍io de devolu珲es? ")
//	Processa({|| HPFINDEV02()})
//Endif
//RestArea(aArea)
//Return


User Function HPFINRFAT()

	Private oReport
	Private cPergCont	:= 'HPFINRFAT0' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf
	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________ 
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'FATREL', 'RELA敲O DE TITULOS POR FATURAMENTO ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELA敲O DE DEVOLU钦ES POR NF' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'TITULOS POR FATURAMENTO', { 'XREL', 'SA1','SE1','SC5','SF2','SD2'})
//GRUPO DE LOJAS	DATA	TITULO	PARCELA	VENDA CODIGO OPERA敲O	VENDA TIPO DE OPERA敲O	EVENTO	MACRO_REGIAO	DESC.MACRO	REGIONAL	CODIGO	RAZAO SOCIAL	CONDICAO DE PAGAMENTO	PEDIDO DE VENDA	QUANTIDADE FATURADA	VALOR FATURADO
    TRCell():New( oSection1, 'GRUPO DE LOJAS'        ,'XREL','GRPLJAS'              , "@!"              ,10)
	TRCell():New( oSection1, 'DATA'                  ,'XREL','DATA'                 , "@D"              ,08)
	TRCell():New( oSection1, 'VENCIMENTO'            ,'XREL','VENCIMENTO'           , "@D"              ,08)
	TRCell():New( oSection1, 'TITULO'                ,'XREL','TITULO'               , "@!" 		  	    ,09)
	TRCell():New( oSection1, 'PARCELA'               ,'XREL','PARCELA'              , "@!" 			    ,03)
	TRCell():New( oSection1, 'EVENTO'                ,'XREL','EVENTO'               , "@!" 			    ,30)
    TRCell():New( oSection1, 'SITUACAO'              ,'XREL','SITUACAO'             , "@!" 			    ,30)
	TRCell():New( oSection1, 'MACRO_REGIAO'          ,'XREL','MACRO_REGIAO'         , "@!" 			    ,30)
	TRCell():New( oSection1, 'DESC.MACRO'            ,'XREL','DESC.MACRO'           , "@!" 			    ,30)
	TRCell():New( oSection1, 'REGIONAL'              ,'XREL','REGIONAL'             , "@!" 			    ,30)
	TRCell():New( oSection1, 'CODIGO'                ,'XREL','CODIGO'               , "@!" 			    ,06)
	TRCell():New( oSection1, 'RAZAO SOCIAL'          ,'XREL','RAZAO SOCIAL'         , "@!" 			    ,30)
	TRCell():New( oSection1, 'CONDICAO DE PAGAMENTO' ,'XREL','CONDICAO DE PAGAMENTO', "@!" 			    ,25)
	TRCell():New( oSection1, 'PEDIDO DE VENDA'       ,'XREL','PEDIDO DE VENDA'      , "@!" 	   		    ,06)
	TRCell():New( oSection1, 'QUANTIDADE FATURADA'   ,'XREL','QUANTIDADE FATURADA'  , "@E       999,999",13)
	TRCell():New( oSection1, 'VALOR FATURADO'        ,'XREL','VALOR FATURADO'       , "@E 99,999,999.99",13)
    TRCell():New( oSection1, 'SALDO EXISTENTE'       ,'XREL','SALDO EXISTENTE'      , "@E 99,999,999.99",13)                       
    TRCell():New( oSection1, 'DATA DA BAIXA'         ,'XREL','DATA DA BAIXA'        , "@D"              ,08)
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
    LocaL lApaga := .f.
	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	                    
   
	IF Select("xREL") > 0
		xREL->(dbCloseArea())
	Endif             
// xREL-> TITULO PARCELA EVENTO SITUACAO MREGIAO DESCMACRO DESCMACRO REGIONAL CODIGO 	RSOCIAL CONDPAG  PED_VDA QTD_FATU VLRFAT
 
 	cQuery := " SELECT A1_XGRUPO AS [GRPLJAS],E1_EMISSAO AS DATA,E1_VENCREA as VENCIMENTO, E1_NUM AS TITULO, E1_PARCELA AS PARCELA, "
	cQuery += " E1_XHISTMI AS  EVENTO ,E1_XCTBANC AS SITUACAO,  "
	cQuery += " ZA4_CODMAC AS MREGIAO, ZA4_DESCMA AS [DESCMACRO], ZA4.ZA4_REGION AS REGIONAL, "
	cQuery += " E1_CLIENTE AS CODIGO, A1_NOME AS [RSOCIAL] ,C5_XDESCOD AS [CONDPAG] ,C5_NUM AS [PED_VDA] , "
	cQuery += " (SELECT SUM(D2_QUANT) FROM SD2010 D2 WHERE D2_DOC = E1_NUM AND D2.D_E_L_E_T_ = '' ) AS [QTD_FATU] , "
	cQuery += " F2_VALBRUT AS [VLRFAT] ,E1_SALDO AS SALDO  ,E1_BAIXA AS DTBAIXA"
	cQuery += " FROM "+RetSqlName("SE1")+" E1 (NOLOCK) "
	cQuery += "  INNER JOIN "+RetSqlName("SA1")+" A1  (NOLOCK) ON A1_COD = E1_CLIENTE  AND ( A1_XGRUPO BETWEEN '"+mv_par07+"' AND '"+mv_par08+"' ) AND (A1_COD BETWEEN '"+mv_par01+"' AND '"+mv_par02+"') AND A1.D_E_L_E_T_ = ''  "
	cQuery += "  INNER JOIN "+RetSqlName("SD2")+" D2  (NOLOCK) ON D2_DOC = E1_NUM AND (D2_DOC BETWEEN '"+mv_par03+"' AND '"+mv_par04+"') AND D2.D_E_L_E_T_ = ''  "
	cQuery += "  INNER JOIN "+RetSqlName("SC5")+" C5  (NOLOCK) ON C5_NUM  = D2_PEDIDO AND C5.D_E_L_E_T_ = ''  "
	cQuery += "  INNER JOIN "+RetSqlName("ZA4")+" ZA4 (NOLOCK) ON A1.A1_XMICRRE = ZA4.ZA4_CODMAC AND ZA4.D_E_L_E_T_ <> '*'  " 
 	cQuery += "  INNER JOIN "+RetSqlName("SF2")+" F2  (NOLOCK) ON F2_DOC = E1_NUM AND (F2_DOC BETWEEN '"+mv_par03+"' AND '"+mv_par04+"') AND  F2.D_E_L_E_T_ = ''  "
 	cQuery += " WHERE  E1.D_E_L_E_T_ = '' "
 	cQuery += " AND E1_EMISSAO BETWEEN '"+DtoS(MV_PAR05)+"' AND '"+DtoS(MV_PAR06)+"' " 
 	cQuery += " GROUP BY A1_XGRUPO, E1_EMISSAO , E1_NUM  ,E1_PARCELA, E1_CLIENTE, A1_NOME ,C5_XDESCOD,C5_NUM ,ZA4.ZA4_CODMAC,ZA4.ZA4_DESCMA,ZA4.ZA4_REGION, F2_VALBRUT,E1_XHISTMI ,E1_XCTBANC ,E1_VENCREA ,E1_SALDO, E1_BAIXA" 
	cQuery += " ORDER BY A1_XGRUPO,E1_CLIENTE,E1_NUM " 
 
    TCQUERY cQuery NEW ALIAS xREL 
    xREL->(DBGOTOP())
    
  While xREL->(!EOF())      
	
	  IF oReport:Cancel()
		 Exit
	  EndIf
	
	  oReport:IncMeter()
	  oReport:IncMeter()


		oSection1:Cell("GRUPO DE LOJAS"):SetValue(xREL->GRPLJAS)
		oSection1:Cell("GRUPO DE LOJAS"):SetAlign("LEFT")
	
		oSection1:Cell("DATA"):SetValue(CTOD(SUBSTR(xREL->DATA,7,2)+'/'+SUBSTR(xREL->DATA,5,2)+'/'+SUBSTR(xREL->DATA,3,2))) 
		oSection1:Cell("DATA"):SetAlign("LEFT")
	
		oSection1:Cell("VENCIMENTO"):SetValue(CTOD(SUBSTR(xREL->VENCIMENTO,7,2)+'/'+SUBSTR(xREL->VENCIMENTO,5,2)+'/'+SUBSTR(xREL->VENCIMENTO,3,2)))
		oSection1:Cell("VENCIMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("TITULO"):SetValue(xREL->TITULO)
		oSection1:Cell("TITULO"):SetAlign("LEFT")
	
		oSection1:Cell("PARCELA"):SetValue(xREL->PARCELA)
		oSection1:Cell("PARCELA"):SetAlign("LEFT")
			
		oSection1:Cell("EVENTO"):SetValue(xREL->EVENTO)
		oSection1:Cell("EVENTO"):SetAlign("LEFT")
	
		oSection1:Cell("SITUACAO"):SetValue(xREL->SITUACAO)
		oSection1:Cell("SITUACAO"):SetAlign("LEFT")
	
		oSection1:Cell("MACRO_REGIAO"):SetValue(xREL->MREGIAO)
		oSection1:Cell("MACRO_REGIAO"):SetAlign("LEFT")
	
		oSection1:Cell("DESC.MACRO"):SetValue(xREL->DESCMACRO)
		oSection1:Cell("DESC.MACRO"):SetAlign("LEFT")
	
		oSection1:Cell("REGIONAL"):SetValue(xREL->REGIONAL)
		oSection1:Cell("REGIONAL"):SetAlign("LEFT")
	
		oSection1:Cell("CODIGO"):SetValue(xREL->CODIGO)
		oSection1:Cell("CODIGO"):SetAlign("LEFT")
	
		oSection1:Cell("RAZAO SOCIAL"):SetValue(xREL->RSOCIAL)
		oSection1:Cell("RAZAO SOCIAL"):SetAlign("LEFT")
	
		oSection1:Cell("CONDICAO DE PAGAMENTO"):SetValue(xREL->CONDPAG)
		oSection1:Cell("CONDICAO DE PAGAMENTO"):SetAlign("LEFT")
	
		oSection1:Cell("PEDIDO DE VENDA"):SetValue(xREL->PED_VDA)
		oSection1:Cell("PEDIDO DE VENDA"):SetAlign("LEFT")	
	
		oSection1:Cell("QUANTIDADE FATURADA"):SetValue(xREL->QTD_FATU)
		oSection1:Cell("QUANTIDADE FATURADA"):SetAlign("LEFT")
	
		oSection1:Cell("VALOR FATURADO"):SetValue(xREL->VLRFAT)
		oSection1:Cell("VALOR FATURADO"):SetAlign("LEFT")

		oSection1:Cell("SALDO EXISTENTE"):SetValue(xREL->SALDO)
		oSection1:Cell("SALDO EXISTENTE"):SetAlign("LEFT")

		oSection1:Cell("DATA DA BAIXA"):SetValue(CTOD(SUBSTR(xREL->DTBAIXA,7,2)+'/'+SUBSTR(xREL->DTBAIXA,5,2)+'/'+SUBSTR(xREL->DTBAIXA,3,2)))
		oSection1:Cell("DATA DA BAIXA"):SetAlign("LEFT")
 	
		oSection1:PrintLine()
		
		xREL->(DBSKIP()) 
	enddo
	xREL->(DBCLOSEAREA())
Return( Nil )


/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Verifica/cria SX1 a partir de matriz para verificacao          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Especifico para Clientes Microsiga                             潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

//	Aadd(aPergs,{"Ate Cliente","","","mv_ch8","C",6,0,0,"G","","MV_PAR08","","","","ZZZZZZ","","","","","","","","","","","","","","","","","","","","","SE1","","","",""})
	Aadd(aPergs,{"Cliente de ?" ,"","","mv_ch1","C",06,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","SA1","","","",""})
	Aadd(aPergs,{"Cliente At�?" ,"","","mv_ch2","C",06,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","SA1","","","",""})
	Aadd(aPergs,{"Nota Inicial" ,"","","mv_ch3","C",09,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Nota Final"   ,"","","mv_ch4","C",09,0,0,"G","","mv_par04","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Dt Emiss.de? ","","","mv_ch5","D",08,0,0,"G","","mv_par05","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Dt Emiss.ate?","","","mv_ch6","D",08,0,0,"G","","mv_par06","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Grp Loja de? ","","","mv_ch7","C",10,0,0,"G","","mv_par07","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Grp Loja At�?","","","mv_ch8","C",10,0,0,"G","","mv_par08","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})

dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next
