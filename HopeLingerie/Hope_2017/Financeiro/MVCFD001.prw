#include 'protheus.ch'
#include 'parmtype.ch'
#Include 'FwMVCDef.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "RWMAKE.CH"
#Include "ApWizard.ch"
#include "fileio.ch"

user function MVCFD001()
Local aArea := GetArea()
IF MsgYesno("Confirma A importa��o de dados do Fundo de Marketing? " )
	Processa({|| u_IMPZAV()})
ELseif MsgYesno("Confirma A importa��o dos Email do Fundo de Marketing? " )
	Processa({|| U_IMPZAVA()})
Endif
RestArea(aArea)
Return

USER FUNCTION IMPZAV()
Local aErros        := {}
Local aRet          := {}
Local nRecno        := 0
Local cLinha        := ''
Local nLinTot       := 0
Local lLinha        := .f.
Local lGrava        := .f.
Local nlin          := 0
Local nReg          := 0 
PRIVATE lMsHelpAuto := .F.
PRIVATE lMsErroAuto := .F.
Private cPerg     := "MVC_FDMKG0"

//AjustaSX1(cPerg)
If !Pergunte(cPerg,.T.)
	Return
EndIf

// Estrutura abaixo refe-se ao arquivo CSV
//Filial	C�d. Millennium	CNPJ	Raz�o Social	 Valor Liquido 	 Fundo de Marketing (1%)
//  1              2          3            4               5                  6

nVisibilidade := Nil													// Exibe todos os diret�rios do servidor e do cliente
//		nVisibilidade := GETF_ONLYSERVER										// Somente exibe o diret�rio do servidor
//		nVisibilidade := GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE	// Somente exibe o diret�rio do cliente

cArquivo := cGetFile("Todos os Arquivos ()*.*)     | *.* | ","Selecione um arquivo",0,,.T.,nVisibilidade, .T.)
nHandle := FT_FUse(cArquivo)
if nHandle = -1
	MsgStop(" Problemas na abertura do Arquivo")
	return
endif
nLinTot := FT_FLastRec()
Procregua(nLinTot)
FT_FGoTop()
While !FT_FEOF()
	cLine := FT_FReadLn()
	if Len(alltrim(cLine))== 0
		FT_FSKIP()
		Loop
	Endif
	nRecno := FT_FRecno()
	cLinha := StrTran(cLine,'"',"'")
	cLinha := '{"'+cLinha+'"}'
	//adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
	cLinha := StrTran(cLinha,';','","')
	aAdd(aRet, &cLinha)
	nlin++
	if len(Alltrim(aRet[1][1])) <> 0
		IncProc("Processando  Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)) )
		If lLinha
			_xFILIAL  := FwXFilial("ZAR")
			_xSTATUS  := '1'
			_xCNPJ	  := pegCGC(Alltrim(aRet[1][3]))  //10.904.855/0002-81
			_xCODIGO  := Posicione("SA1",3,xFilial("SA1")+PADR(_xCNPJ,14),"A1_COD")
			_xLOJA	  := SA1->A1_LOJA
			_xNOMECL	 := Alltrim(aRet[1][4])
			_xEND	 := Alltrim(aRet[1][1])
			_xVLRLIQ	 :=	PegaVal(Alltrim(aRet[1][5]))
			_xVLRMKT	 := PegaVal(Alltrim(aRet[1][6]))
			_xMESREF	 := MV_PAR01
			_xVENCTO	 := MV_PAR02
			lGrava       := .f.
			If Len(alltrim(_xCodigo)) <> 0
				dbSelectArea("ZAV")
				ZAV->(dbSetOrder(1))  //FILIAL+MESREF+ZAV_CNPJ
				ZAV->(dbGotop()) 
				If ZAV->(dbSeek(xFilial("ZAV")+Padr(MV_PAR01,8)+PADR(_xCNPJ,14)))
					If ZAV_STATUS <> '1'
						aadd(aErros,{aRet[1][3]+ " Dados j� com T�tulo emitidos, n�o ser� alterados-  Linha ref. "+strzero(nlin,6) })
						lGrava       := .f.
					Else                                        
						ZAV->(RecLock("ZAV",.F.))
						lGrava       := .t.
					endif
				Else
					ZAV->(RecLock("ZAV",.T.))
					lGrava       := .t.
				Endif
			Else
				aadd(aErros,{aRet[1][3]+ "  Cnpj ou Codigo n�o encontrado no cadastro de Clientes -  Linha ref. "+strzero(nlin,6) })
				lGrava       := .f.
			Endif
		endif
		If lGrava 
			ZAV->ZAV_FILIAL := _xFILIAL
			ZAV->ZAV_STATUS := _xSTATUS
			ZAV->ZAV_CODIGO := _xCODIGO
			ZAV->ZAV_LOJA   := _xLOJA
			ZAV->ZAV_CNPJ   := _xCNPJ
			ZAV->ZAV_NOMECL := _xNOMECL
			ZAV->ZAV_END	   := _xEND
			ZAV->ZAV_VLRLIQ := _xVLRLIQ
			ZAV->ZAV_VLRMKT := _xVLRMKT
			ZAV->ZAV_MESREF := _xMESREF
			ZAV->ZAV_VENCTO := _xVENCTO
			ZAV->ZAV_TITULO := ''
			ZAV->(MsUnLock())
			nReg++
		Endif
		If Alltrim(aRet[1][1]) == 'Filial'
			lLinha:= .t.
		Endif
	endif
	// Pula para pr�xima linha
	FT_FSKIP()
	aRet      := {}
Enddo
if nReg >0
   aadd(aErros,{'Total de Registros Incluidos ou Alterados '+transform(nreg,"@R 99999") })
Else
   aadd(aErros,{'Nenhum Registro foi Incluidos ou Alterados ' })
Endif   
cxTexto:=""
for i := 1 to len(aErros)
	cxTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
Next i
aRet      := {}
lSim      := .T.
cTexto    := cxTexto
If len(aErros) >0
	cTexto := "Log de Importa��o, Dados n�o Importados. "+CHR(13)+CHR(10)
	for i := 1 to len(aErros)
		cTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
	Next i
	__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
Endif
if len(alltrim(cTexto)) == 0
	MsgInfo("Fim da Importa��o -  com sucesso        "+CHR(13)+CHR(10)+cTexto)
	
Else
	MsgInfo("Fim da Importa��o -  Verifique erros   "+CHR(13)+CHR(10)+cTexto)
Endif

Return()


Static Function pegCGC(__cnpj)
Local cRet := ''
Local nPos := 0
nPos       := at('.',__cnpj)         //10.904.855/0002-81
if nPos <> 0
	cRet := substr(__cnpj,1,nPos-1)+Substr(__cnpj,nPos+1,15)
Endif
nPos := at('.',cRet)   //10904.855/0002-81
if nPos <> 0
	cRet := substr(cRet,1,nPos-1)+Substr(cRet,nPos+1,15)
Endif
nPos := at('/',cRet)   //10904855/0002-81
if nPos <> 0
	cRet := substr(cRet,1,nPos-1)+Substr(cRet,nPos+1,15)
Endif
nPos := at('-',cRet)   //109048550002-81
if nPos <> 0
	cRet := substr(cRet,1,nPos-1)+Substr(cRet,nPos+1,2)
Endif
cRet := alltrim(cRet)
Return(cRet)

Static Function PegaVal(xValor)
Local nRet  := 0
Local cVal  := 0 //Substr(alltrim(xValor),3,16)
Local cVa2  := 0
Local cVa3  := 0
Local cVa4  := 0
Local npos1 := 0
Local npos2 := 0
if substr(alltrim(xvalor),1,2) == 'R$'
	cVal  := Substr(alltrim(xValor),3,16)
Else
	cVal := alltrim(xValor)
Endif

faz := .t.
do while  faz
	npos1:=at('.',cVal)
	if npos1 >= 1
		cVal := substr(cVal,1,npos1-1)+substr(cVal,npos1+1,16)
	ELse
		npos2 := at(",",cVal)
		if npos2 >= 1
			cVal:=substr(cVal,1,npos2-1)+substr(cVal,npos2+1,2)
		Else
			nRet:= val(cVal)/100
			faz := .f.
		Endif
	Endif
Enddo
Return(nRet)
