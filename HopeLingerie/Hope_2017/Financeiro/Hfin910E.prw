#Include 'Protheus.ch'
#Include 'FwMVCDef.ch'
#INCLUDE "TBICONN.CH"
#include "TOTVS.CH"
#INCLUDE "topconn.ch"
#include "rwmake.ch"    
#INCLUDE "Ap5Mail.ch"     
#Define DS_MODALFRAME   128

User Function HFIN910E()
	Local _astru   := {}
	Local _afields := {}     
	Local _carq    := ""         
	Local cQuery   := ""
	Local xFornec := ""
	Local xLoja   := ""
	Local xVecto  := ctod(space(8))
	Local nValor  := 0
	Local xItens  := ""
	Local xRazao  := ""
	Local cFiltraSE1
	Local ca097User 	:= RetCodUsr()
	Local aSize   := MsAdvSize()  
	Local oFont1  := TFont():New("Verdana",,012,,.T.,,,,,.F.,.F.)
	Local oFont2  := TFont():New("Verdana",,012,,.F.,,,,,.F.,.F.)
	Local aColunas := {	{"E1_OK"     		,,""       		},;
	{"E1_NUM"   	,,"Titulo"    ,"@!" 	},;
	{"E1_PARCELA"   ,,"Parcela"   ,"@!" 	},;
	{"E1_PREFIXO"   ,,"Prefixo"   ,"@!" 	},;
	{"E1_CLIENTE"  	,,"Cliente"	  ,"@!"		},;
	{"E1_LOJA" 		,,"Loja"	  ,"@!"		},;     					
	{"E1_NOMCLI" 	,,"Nome"	  ,"@!"		},;
	{"E1_EMISSAO" 	,,"Emiss�o"	  ,"@D"		},;
	{"E1_VENCREA" 	,,"Vencimento","@D"		},;
	{"E1_VALOR" 	,,"Valor"	  ,"@E 999,999.99"  },;		
	{"E1_VLTXCC"    ,,"Vl.Taxa"	  ,"@E 999,999.99"  },;
	{"E1_XTXCC"     ,,"% Taxa"    ,"@E     999.99"  },;
	{"E1_SALDO"     ,,"Saldo"	  ,"@E 999,999.99"  },;
	{"E1_XFORREC"   ,,"TpVenda"	  ,"@!"	    },;
	{"E1_XTPPAG"    ,,"Tipo Pagto","@!"		},;
	{"E1_TIPO"      ,,"Tipo T�t. ","@!"		}}
	
		
	Private cliSE1    := ""
	Private lojSE1    := ""
	Private preSE1    := ""
	Private numSE1    := ""
	Private parSE1    := ""
	Private tipSE1    := ""
	Private cli_RA    := ""
	Private loj_RA    := ""
	Private pre_RA    := ""
	Private num_RA    := ""
	Private par_RA    := ""
	Private tip_RA    := ""
	Private lMarcar   := .F.
	Private oMark
	Private cPerg     := "HPCOMP0003"
	Private aRotina   := {} 
	Private lContinua := .F.
	Private atuar     := ""
	Private cMark     := GetMark() 
	Private lInverte  := .F.    
	Private oChk      
	Private lChkSel   := .F.  
	Private lRefresh  := .T.  
	Private oDlgT   
	Private onVlrCom
	Private onVlrSld
	Private onVlrMar
	Private onVlrfIL
	Private onVlrSob
	Private nVlrCom   := 0
	Private nVlrSld   := 0
	Private nVlrMar   := 0
	Private nVlrfIL   := 0
	Private nVlrSob   := 0
	Private aRecSE1   := {}
	Private nRecnoRA  := RECNO() 
    If Select("XTRB") > 0
	   XTRB->(DbCloseArea())
	EndIf
	//Estrutura da tabela temporaria
	AADD(_astru,{"E1_OK"     ,"C",  2,0})
	AADD(_astru,{"E1_PREFIXO","C",  3,0})
	AADD(_astru,{"E1_NUM"    ,"C",  9,0})
	AADD(_astru,{"E1_PARCELA","C",  3,0})
	AADD(_astru,{"E1_CLIENTE","C",  6,0})
	AADD(_astru,{"E1_LOJA"   ,"C",  4,0})
	AADD(_astru,{"E1_NOMCLI" ,"C", 15,0})
	AADD(_astru,{"E1_EMISSAO","D",  8,0})
	AADD(_astru,{"E1_VENCREA","D",  8,0})
	AADD(_astru,{"E1_VALOR"  ,"N", 16,2})
	AADD(_astru,{"E1_VLTXCC" ,"N", 16,2})
	AADD(_astru,{"E1_XTXCC"  ,"N",  8,2})
	AADD(_astru,{"E1_SALDO"  ,"N", 16,2})
	AADD(_astru,{"E1_XFORREC","C", 15,0})
	AADD(_astru,{"E1_XTPPAG" ,"C", 15,0})
	AADD(_astru,{"E1_RECNO"  ,"N", 15,0})
    AADD(_astru,{"E1_TIPO"   ,"C",  3,0})
    AADD(_astru,{"E1_DECRESC","N", 16,2})
	
	cArqTrab  := CriaTrab(_astru)
	dbUseArea( .T.,, cArqTrab, "XTRB", .F., .F. )
   //Atribui a tabela temporaria ao alias TRB
	//-------------------------------------------------------------------
	// Verifica se o usuario possui direito de liberacao.           
	//-------------------------------------------------------------------
	AjustaSX1(cPerg)
	Pergunte(cPerg, .T.)
	cQuery := "SELECT '' AS E1_OK,E1_NUM,E1_CLIENTE,E1_LOJA,E1_NOMCLI,E1_EMISSAO,E1_VENCREA,E1_VALOR,E1_VLTXCC,E1_SALDO,E1_XTXCC,E1_XFORREC,E1_XTPPAG,"
	cQuery += " E1_PREFIXO, E1_PARCELA, E1_TIPO,  R_E_C_N_O_ AS E1_RECNO, E1_DECRESC  " 
	cQuery += "FROM "+RetSqlName("SE1")+" E1 WITH (NOLOCK) WHERE E1_XFORREC = 'PAYPAL' AND E1_VENCREA BETWEEN '"+DtOS(MV_PAR01)+"' AND '"+DtOS(MV_PAR02)+"' " 
	cQuery += "AND E1.D_E_L_E_T_= ''  AND  E1_SALDO <> 0  ORDER BY E1_NUM "
	If Select("YTRB") > 0
		YTRB->(DbCloseArea())
	EndIf
	//cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"YTRB",.T.,.T.)
	DbSelectArea("YTRB")
	YTRB->(DbGotop())   
	While YTRB->(!EOF())
		DbSelectArea("XTRB")        
		XTRB->(RecLock("XTRB",.T.))
		XTRB->E1_NUM     := YTRB->E1_NUM    
		XTRB->E1_CLIENTE := YTRB->E1_CLIENTE
		XTRB->E1_LOJA    := YTRB->E1_LOJA
		XTRB->E1_NOMCLI  := YTRB->E1_NOMCLI
		XTRB->E1_EMISSAO := CTOD(SUBSTR(YTRB->E1_EMISSAO,7,2)+'/'+SUBSTR(YTRB->E1_EMISSAO,5,2)+'/'+SUBSTR(YTRB->E1_EMISSAO,3,2))
		XTRB->E1_VENCREA := CTOD(SUBSTR(YTRB->E1_VENCREA,7,2)+'/'+SUBSTR(YTRB->E1_VENCREA,5,2)+'/'+SUBSTR(YTRB->E1_VENCREA,3,2))
		XTRB->E1_VALOR   := YTRB->E1_VALOR
		XTRB->E1_VLTXCC  := YTRB->E1_VLTXCC
	    XTRB->E1_XTXCC   := YTRB->E1_XTXCC 
		XTRB->E1_SALDO   := YTRB->E1_SALDO - YTRB->E1_DECRESC
		XTRB->E1_XFORREC := YTRB->E1_XFORREC
		XTRB->E1_XTPPAG  := YTRB->E1_XTPPAG
		XTRB->E1_PREFIXO := YTRB->E1_PREFIXO
		XTRB->E1_PARCELA := YTRB->E1_PARCELA
		XTRB->E1_TIPO    := YTRB->E1_TIPO
		XTRB->E1_RECNO   := YTRB->E1_RECNO
		XTRB->E1_DECRESC := YTRB->E1_DECRESC
		nVlrfIL := ROUND(nVlrfIL+XTRB->E1_SALDO,2)
		nVlrCom := nVlrCom+YTRB->E1_SALDO-YTRB->E1_DECRESC
		XTRB->(MsUnlock())        
		YTRB->(DbSkip())
	Enddo
	DbSelectArea("XTRB")
	XTRB->(DbGotop())   
	DEFINE MSDIALOG oDlgT TITLE "HOPE - Baixas PAY PALL " FROM aSize[7],100 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
	//		DEFINE MSDIALOG oDlgT TITLE "HOPE - Compensa��o SHOPLINE" FROM aSize[7],0 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
	cMarca    := GetMark()
	@ 005, 005 SAY oSay2 PROMPT "Lista de t�tulos a serem baixados como PAYPAL." SIZE 242, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
	oMark := MsSelect():New("XTRB","E1_OK",,aColunas,lInverte,cMarca,{ 015, 003, 242, 650}) 
	oMark:bAval:= {||(CSINT05E(cMarca),oMark:oBrowse:Refresh())}
	oMark:oBrowse:Refresh(.F.)	   	
	oMark:oBrowse:lHasMark    := .T.
	oMark:oBrowse:lCanAllMark := .T.
	oMark:oBrowse:bAllMark := {|| U_CSINT05F(cMarca),oMark:oBrowse:Refresh()} 
	@ 250, 005 BUTTON oButton1 PROMPT "BAIXAR" 	SIZE 032, 013 OF oDlgT ACTION U_CSINT05G(cMarca,1) PIXEL
	@ 250, 040 BUTTON oButton1 PROMPT "Fechar" 	  	SIZE 035, 013 OF oDlgT ACTION U_CSINT05G(cMarca,5) PIXEL
	@ 250, 200 Say oSay3 prompt " Valor Total Ser BAIXADO:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
//   		@ 260, 200 Say oSay5 prompt " Saldo Total da RA a Ser Compensado:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
	@ 270, 200 Say oSay7 prompt " Total Marcado para BAIXAR......:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
//			@ 280, 200 Say oSay7 prompt " Saldo a Compensar ................:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
	@ 250,315 MSGET onVlrCom VAR nVlrCom When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"
//			@ 260,315 MSGET onVlrSld VAR nVlrSld When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"
	@ 270,315 MSGET onVlrMar VAR nVlrMar When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"
//			@ 280,315 MSGET onVlrSob VAR nVlrSob When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"

	ACTIVATE MSDIALOG oDlgT CENTERED
	if !Empty('XTRB')
    	XTRB->(DbCloseArea())
	Endif
Return Nil


Static Function CSINT05E(cMarca)            

	If Alltrim(XTRB->E1_OK) == ""
			XTRB->(RecLock( "XTRB", .F. ))          
			XTRB->E1_OK   := cMarca 	
			XTRB->(msUnlock())  
			nVlrMar :=	nVlrMar+(XTRB->E1_VALOR-XTRB->E1_DECRESC)
	Else
		XTRB->(RecLock( "XTRB", .F. ))          
		XTRB->E1_OK   := Space(2)
		XTRB->(msUnlock())  
		nVlrMar :=	nVlrMar-(XTRB->E1_VALOR-XTRB->E1_DECRESC)
	EndIf
	onVlrMar:Refresh()
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � CSINT04F � Autor � Edgar Benuto Frastrone� Data �21/07/2016���
�������������������������������������������������������������������������Ĵ��
���Locacao   � Comsystem�Contato� edgar.frastrone@comsystem.com.br        ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Rotina que marca e desmarca Browse total.					  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Alianca Francesa		                                      ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���Edgar B.F.    �21/07/16�      �Desenvolvimento da Rotina.              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                               
User Function CSINT05F(cMarca) 

	dbSelectArea("XTRB")
	XTRB->(dbGoTop())
	While XTRB->(!Eof())
		If Alltrim(XTRB->E1_OK) == ""
			If nVlrSob == 0
				MsgStop("Valor a Compensar j� Utilizado,Verifique","Aten��o" )
			Else   
				RecLock( "TIT", .F. )          
				XTRB->E1_OK   := cMarca 	
				XTRB->(msUnlock())
				nVlrMar :=	nVlrMar+(XTRB->E1_VALOR-XTRB->E1_DECRESC)

				//msgStop("Diminuindo saldo Pois foi Marcado")
			Endif
		Else
			XTRB->(RecLock( "XTRB", .F. ))          
			XTRB->E1_OK   := Space(2)
			XTRB->(msUnlock())
			nVlrSob := 0      
			nVlrMar :=	nVlrMar-(XTRB->E1_VALOR-XTRB->E1_DECRESC)

			//msgStop("Somonado o saldo Pois foi desMarcado")
		EndIf

		XTRB->(DBSKIP()) 
	End 
	onVlrMar:Refresh()
	XTRB->(dbGoTop())	 
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  � CSINT04F � Autor � Edgar Benuto Frastrone� Data �21/07/2016���
�������������������������������������������������������������������������Ĵ��
���Locacao   � Comsystem�Contato� edgar.frastrone@comsystem.com.br        ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Rotina que marca e desmarca Browse total.					  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � Alianca Francesa		                                      ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���Edgar B.F.    �21/07/16�      �Desenvolvimento da Rotina.              ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/                                               
User Function CSINT05G(cMarca,nOpc)

	If(nOpc == 5) 
		oDlgT:End()
	ElseIf(nOpc == 1) 		
		Processa( {|| CSINT05H(cMarca) }, "Aten��o", "Iniciando a Baixa", .F.)
		oDlgT:End()
	EndIf

Return(Nil)

Static Function CSINT05H(cMarca)
    Local AregSe1 := {} 
    Local nSeq    := 0
	Private nSaldoComp:= 0
	Private aLog  := {} 
	dbSelectArea("XTRB")
	XTRB->(dbGoTop())
	While XTRB->(!Eof())
		If Alltrim(XTRB->E1_OK) <> ""
   	       VDT_NUM    := XTRB->E1_NUM // aRet[1][05]  //numero
    	   VDT_CODCL  := XTRB->E1_CLIENTE //AllTrim(aRet[1][03])  //Codigo do cliente
		   VDT_DESCR  := XTRB->E1_NOMCLI // Ret[1][04]  //nome do cliente
		   VDT_NPARC  := XTRB->E1_PARCELA      //Parcela
		   VDT_DTPAG  := dDatabase //data do pagamento igual ao vencimento
		   VDT_VLPAG  := XTRB->E1_VALOR-XTRB->E1_VLTXCC  
		   VDT_PTAXA  := XTRB->E1_XTXCC
           VDT_VLTIT  := XTRB->E1_VALOR     //Valor Total do t�tulo
           VDT_CDBCO  := '237'               
           VDT_CDAGE  := '1579'
           VDT_CDCC   := '5273'
           CON_CDBCO  := 'PAY'
           CON_CDAGE  := 'PAYPA'
           CON_CDCC   := 'PAYPAL'
                 
      	   lBaixar := .f.
	       DbSelectArea("SE1")
	       SE1->(DbSetOrder(1))   //E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, R_E_C_N_O_, D_E_L_E_T_
	       iF DbSeek(xFilial("SE1")+ XTRB->E1_PREFIXO+XTRB->E1_NUM+XTRB->E1_PARCELA+XTRB->E1_TIPO,.t.)
    
	       If SE1->E1_NUM == VDT_NUM  .AND. SE1->E1_VALOR == VDT_VLTIT
   	          IF SE1->E1_XTXCC == 0
                 SE1->(RecLock("SE1",.F.))
                 SE1->E1_XTXCC   := 4.5
                 SE1->E1_VLTXCC  := ROUND((SE1->E1_VALOR*4.5)/100,2)
 	             SE1->E1_DECRESC := ROUND((SE1->E1_VALOR*4.5)/100,2)
                 SE1->E1_SDDECRE := ROUND((SE1->E1_VALOR*4.5)/100,2) 
                 SE1->(msUnlock())
              Else
                 If SE1->E1_DECRESC == 0 
                    SE1->(RecLock("SE1",.F.))
 	                SE1->E1_DECRESC := ROUND((SE1->E1_VALOR*4.5)/100,2)
                    SE1->E1_SDDECRE := ROUND((SE1->E1_VALOR*4.5)/100,2) 
                    SE1->(msUnlock())
                 Endif   
              Endif   
              nSeq++
              VDT_VLPAG  := SE1->E1_VALOR-SE1->E1_VLTXCC
   	          VDT_NUM    := SE1->E1_NUM // aRet[1][05]  //numero
    	      VDT_CODCL  := SE1->E1_CLIENTE //AllTrim(aRet[1][03])  //Codigo do cliente
		      VDT_DESCR  := SE1->E1_NOMCLI // Ret[1][04]  //nome do cliente
		      VDT_NPARC  := SE1->E1_PARCELA      //Parcela
		      VDT_DTPAG  := dDatabase //data do pagamento igual ao vencimento
		      VDT_VLPAG  := SE1->E1_VALOR-XTRB->E1_VLTXCC  
		      VDT_PTAXA  := SE1->E1_XTXCC
              VDT_VLTIT  := SE1->E1_VALOR     //Valor Total do t�tulo
	          xNatureza := POSICIONE("SA1",1,xFilial("SA1")+SE1->E1_CLIENTE+SE1->E1_LOJA,"A1_NATUREZ")  
         	  AADD(AregSe1,{SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,'NF ',SE1->E1_CLIENTE,SE1->E1_LOJA,;
  			  "NOR",VDT_CDBCO,VDT_CDAGE,ALLTRIM(VDT_CDCC),VDT_DTPAG,VDT_DTPAG,"Pagto. por Pay-Pal", 0 , 0, 0 , 0 , 0 ,VDT_VLPAG,xNatureza})
              //7       8        9                 10                               11         12             13          14  15  16  17  18      19            20
//     		  AADD(aRegSE1,{SE1->E1_PREFIXO,SE1->E1_NUM,SE1->E1_PARCELA,'NF ',SE1->E1_CLIENTE,SE1->E1_LOJA,;
//	     	  "NOR",CON_CDBCO,CON_CDAGE,CON_CDCC,VDT_DTPAG,VDT_DTPAG,"Taxa C.Credito - Pay-Pal ",0,0,0,0,0,VDT_PTAXA,xNatureza})
              LOG_INFO := HFIN910BX(aRegSE1)
              if  LOG_INFO == "N�o baixado-Verifique"
                  aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_INFO,"Detalhes.  Erro na Baixa Verifique -> ", VDT_NUM,,0,VDT_PTAXA,SE1->E1_XTXCC,VDT_VLPAG,SE1->E1_VALOR  })
              else
                  aAdd(aLog,{"Linha-> "+AllTrim(Str(nSeq)) ,LOG_INFO,"Detalhes.  Foi encontrado e Baixado Normal -> ", VDT_NUM,,0,VDT_PTAXA,SE1->E1_XTXCC,VDT_VLPAG,SE1->E1_VALOR })
              endif
              LOG_INFO  := "Informativo"
              aRegSE1 := {}
           Endif	    
	    Else
		   aAdd(aLog,{	"Linha-> "+AllTrim(Str(nSeq)) ,LOG_ERRO,"Detalhes n�o encontrado correspondente -> ",VDT_NUM,,0,,0,VDT_VLPAG,0 })
 	    Endif
 	    endif
		XTRB->(DBSKIP()) 
	End 
	onVlrMar:Refresh()
	XTRB->(dbGoTop())	 
//	lRet := U_COMPCR(nSaldoComp)
	oMark:oBrowse:Refresh(.F.)
	oDlgT:Refresh(.F.)
Return(Nil)


/*/
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 ���
�����������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica/cria SX1 a partir de matriz parverificacao          ���
�����������������������������������������������������������������������������Ĵ��
���Uso       � Especifico para Clientes Microsiga                             ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
����������������������������������������������������������������������������������
/*/
Static Function AjustaSX1(cPerg)

	Local _sAlias	:= Alias()
	Local aCposSX1	:= {}
	Local aPergs	:= {}
	Local nX 		:= 0
	Local lAltera	:= .F.
	Local nCondicao
	Local cKey		:= ""
	Local nJ		:= 0

	aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
	"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
	"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
	"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
	"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
	"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
	"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
	"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

	//	Aadd(aPergs,{"Ate Cliente","","","mv_ch8","C",6,0,0,"G","","MV_PAR08","","","","ZZZZZZ","","","","","","","","","","","","","","","","","","","","","SE1","","","",""})
//	Aadd(aPergs,{"Tipo Pgto.?      ","","","mv_ch1","C",20,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data Emiss�o de? ","","","mv_ch1","D",08,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data Emiss�o ate?","","","mv_ch2","D",08,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	dbSelectArea("SX1")
	dbSetOrder(1)
	For nX:=1 to Len(aPergs)
		lAltera := .F.
		If MsSeek(cPerg+Right(aPergs[nX][11], 2))
			If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
				aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
				lAltera := .T.
			Endif
		Endif

		If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
			lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
		Endif

		If ! Found() .Or. lAltera
			RecLock("SX1",If(lAltera, .F., .T.))
			Replace X1_GRUPO with cPerg
			Replace X1_ORDEM with Right(aPergs[nX][11], 2)
			For nj:=1 to Len(aCposSX1)
				If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
					Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
				Endif
			Next nj
			MsUnlock()
			cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."


			If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
				aHelpSpa := aPergs[nx][Len(aPergs[nx])]
			Else
				aHelpSpa := {}
			Endif

			If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
				aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
			Else
				aHelpEng := {}
			Endif

			If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
				aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
			Else
				aHelpPor := {}
			Endif

			PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
		Endif
	Next
Return() 


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910GrvArq�Autor  �Rafael Rosa da Silva� Data �  08/12/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao que efetua a gravacao dos registrosn na tabela FIF	  ���
���          �(Conciliacao do SITEF)									  ���
�������������������������������������������������������������������������͹��
���Uso       �															  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function HFIN910BX(aRegSE1)
Local aArea   := GetArea()
Local aTitInd := {}
Local nTamPref 	:= 3  //TamSX3("E1_PREFIXO")[3]
Local nTamNume 	:= 9  //TamSX3("E1_NUM")[1]
Local nTamTipo 	:= 3  //TamSX3("E1_TIPO")[1]
Local nTamParc 	:= 3  //TamSX3("E1_PARCELA")[1]
Local nTamBanc 	:= 3  //TamSX3("E8_BANCO")[1]
Local nTamAgec 	:= 5  //TamSX3("E8_AGENCIA")[1]
Local nTamCont 	:= 10 //TamSX3("E8_CONTA")[1]
Local cRet      := space(21)

dbSelectArea("SE1")
CONOUTR("Conciliador Concil - HFIN910B - Hfin910bx - INICIO baixa de t�tulos - " + DToC(Date()) + " - Hora: " + TIME())
For T := 1 to Len(aRegSE1)

aTitInd	:=	{{"E1_PREFIXO"		,PADR(aRegSE1[T][01],nTamPref) ,NiL}                                                                                                                                     ,;
			 {"E1_NUM"			,PADR(aRegSE1[T][02],nTamNume) ,NiL},;
			 {"E1_PARCELA"		,PADR(aRegSE1[T][03],nTamParc) ,NiL},;
			 {"E1_TIPO"			,PADR(aRegSE1[T][04],nTamTipo) ,NiL},;
			 {"E1_CLIENTE"		,aRegSE1[T][05] 			   ,NiL},;
			 {"E1_LOJA"			,aRegSE1[T][06] 			   ,NiL},;
			 {"E1_NATUREZA"		,aRegSE1[T][20] 			   ,NiL},;
			 {"AUTMOTBX"		,aRegSE1[T][07] 			   ,NiL},;
			 {"AUTBANCO"		,PadR(aRegSE1[T][08],nTamBanc) ,Nil},;
			 {"AUTAGENCIA"		,PadR(aRegSE1[T][09],nTamAgec) ,Nil},;
			 {"AUTCONTA"		,PadR(aRegSE1[T][10],nTamCont) ,Nil},;
		 	 {"AUTDTBAIXA"		,aRegSE1[T][11] 			   ,NiL},;
			 {"AUTDTCREDITO"	,aRegSE1[T][12] 			   ,NiL},;
			 {"AUTHIST"			,aRegSE1[T][13] 			   ,NiL},; //"Conciliador SITEF"
			 {"AUTDESCONT"		,aRegSE1[T][14] 			   ,NiL},; //Valores de desconto
  	 		 {"AUTACRESC"		,aRegSE1[T][15] 			   ,NiL},; //Valores de acrescimo - deve estar cadastrado no titulo previamente
			 {"AUTDECRESC"		,aRegSE1[T][16] 			   ,NiL},; //Valore de decrescimo - deve estar cadastrado no titulo previamente
			 {"AUTMULTA"		,aRegSE1[T][17] 			   ,NiL},; //Valores de multa
			 {"AUTJUROS"		,aRegSE1[T][18] 			   ,NiL},; //Valores de Juros
			 {"AUTVALREC"		,aRegSE1[T][19] 			   ,NiL}}  //Valor recebido

//			 {"AUTCONTA"		,If(lPontoF,(PadR(areg[01][10],Len(SE8->E8_CONTA))),(PadR(StrTran(areg[01][10],"-",""),Len(SE8->E8_CONTA)))),Nil},;

						//Efetua baixa individual
aTitulosBX := aTitInd
lMsErroAuto := .f.
MSExecAuto({|x, y| FINA070(x, y)}, aTitulosBX, 3)
//Verifica se ExecAuto deu erro
If lMsErroAuto
   MostraErro()
   cRet := "N�o baixado-Verifique"
Else
   cRet := "Baixado  com  sucesso"
Endif
aTitInd :={}
Next T


CONOUTR("Pagamentos por Pay-Pal FINA910E - A910GrvArqp - FIM GRAVANDO ARQUIVO - " + DToC(Date()) + " - Hora: " + TIME())

Return(cRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910GrvLog�Autor  �Rafael Rosa da Silva� Data �  08/12/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao que efetua a gravacao dos Logs						  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �															  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function A910GrvLog(aLog)

	Local cType := "Arquivos LOG"+ "(*.CSV) |*.CSV|"			//"Arquivos LOG"	### "(*.log) |*.log|"
	Local cDir	:= cGetFile(cType ,"Selecione o diretorio para grava��o do LOG",0,,.F.,GETF_LOCALHARD + GETF_NETWORKDRIVE + GETF_RETDIRECTORY)			//
	Local nHdl	:= 0                           //Handle do arquivo
	Local cDados:= ""                          //Descri��o da Linha
	Local nI	:= 0                           //Variavel contadora de log
	Local cLin	:= ""                          //Variavel da linha do log
	Local cEOL	:= CHR(13)+CHR(10)            //Final de Linha
    Local nFator := If(Len(alog) > 2000, 1000, 1)

	//Incluo o nome do arquivo no caminho ja selecionado pelo usuario
	cDir := Upper(Alltrim(cDir)) + "LOG_FINA910_" + dTos(dDataBase) + StrTran(Time(),":","") + ".CSV"

	If (nHdl := FCreate(cDir)) == -1
	    MsgInfo("O arquivo de nome " + cDir + " nao pode ser executado! Verifique os parametros.")			//	###
	    Return
	EndIf

	cDados	:= "Linha da Ocorrencia;Tipo da Ocorrencia;Descricao da Ocorrencia;Nr. T�tulo; AUTORI ;ZAK_AUTORI; TAXA; ZAK_TAXA;VL PGTO;ZAK_VLLIQ"
	cLin	:= Space(Len(cDados)) + cEOL
	cLin	:= Stuff(cLin,01,Len(cDados),cDados)

	If FWrite(nHdl,cLin,Len(cLin)) != Len(cLin)
		If Aviso("Atencao","Ocorreu um erro na gravacao do arquivo. Continua?",{"Sim","N�o"}) == 2		//	### 	###
			FClose(nHdl)
			Return
		EndIf
	EndIf

	CONOUTR("Conciliador Concil - FINA910B - A910GrvLog - INICIO GRAVANDO LOG - " + DToC(Date()) + " - Hora: " + TIME())

	ProcRegua(Len(aLog)/nFator)

	For nI := 1 to Len(aLog)

		If nI%nFator == 0
			IncProc("Gravando os Log's..." + "(" + AllTrim(Str(nI)) + "/" + AllTrim(Str(Len(aLog))) + ")")			//
        EndIf
        //IF type(aLog[nI][9]) == 'N'
           cDados	:= aLog[nI][1] + ';' + aLog[nI][2] + ';' + aLog[nI][3] + ';' + aLog[nI][4] + ';' +  transform(aLog[nI][5],"@e 999999") + ';' +  transform(aLog[nI][6],"@e 999999") + ';' +  transform(aLog[nI][7],"@e 99999.99") + ';' + transform(aLog[nI][8],"@e 99999.99") + ';' + Transform(aLog[nI][9],"@e 99999.99") + ';' + Transform(aLog[nI][10],"@e 99999.99") //+';'
        //eLSE
        //   cDados	:= aLog[nI][1] + ';' + aLog[nI][2] + ';' + aLog[nI][3] + ';' + aLog[nI][4] + ';' + aLog[nI][5] + ';' + aLog[nI][6] + ';' + aLog[nI][7] + ';' + transform(aLog[nI][8],"@e 999.99") + ';' + aLog[nI][9] + ';' + Transform(aLog[nI][10],"@e 999.99") //+';'
        //eNDIF
		cLin	:= Space( Len(cDados) ) + cEOL
		cLin	:= Stuff(cLin,01,Len(cDados),cDados)

		If FWrite(nHdl,cLin,Len(cLin)) != Len(cLin)
			If Aviso(3026,"Ocorreu um erro na gravacao do arquivo. Continua?",{ "Sim","N�o"}) == 2		//	### 	###
				FClose(nHdl)
				Return
			EndIf
		EndIf
	Next nI

	FClose(nHdl)

	CONOUTR("Pagamento por Pay - Pall - FINA910E - A910GrvLogp - FIM GRAVANDO LOG - " + DToC(Date()) + " - Hora: " + TIME())

Return

