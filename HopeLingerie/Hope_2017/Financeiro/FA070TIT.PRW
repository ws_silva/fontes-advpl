#Include 'Protheus.ch'

User Function FA070TIT()	//Baixa a Receber Manual
	
	Local x
	local lRet := .T.
	_area := GetArea()
	
	dbSelectArea("SE1")
	SE1->(DbSetOrder(1)) //E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO
	Dbseek(xFilial("SE1")+Substring(CTITULO,1,3)+Substring(CTITULO,5,9)+Substring(CTITULO,15,3))
	
	For x := 1 to 5 

		_cVend	:= "SE1->E1_VEND"+AllTrim(Str(x))
		DbSelectArea("SE3")
		DbOrderNickName("SE3INAD")
		dbSeek(xFilial("SE3")+&_cVend+SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_PARCELA+"DEB.INAD. ")
		
		If Found()
		
			_cVendedor	:= SE3->E3_VEND	
			_cPercent	:= SE3->E3_PORC
			_cTipo		:= SE3->E3_TIPO
			_cPedido		:= SE3->E3_PEDIDO

			RecLock("SE3",.T.)
			SE3->E3_FILIAL		:= "    "
			SE3->E3_VEND		:= _cVendedor
			SE3->E3_NUM			:= SE1->E1_NUM
			SE3->E3_EMISSAO		:= dBaixa
			SE3->E3_SERIE		:= SE1->E1_PREFIXO
			SE3->E3_CODCLI		:= SE1->E1_CLIENTE
			SE3->E3_LOJA		:= SE1->E1_LOJA
			SE3->E3_BASE		:= nValRec
			SE3->E3_PORC		:= _cPercent
			SE3->E3_COMIS		:= (nValRec*_cPercent)/100
			SE3->E3_PREFIXO		:= SE1->E1_PREFIXO
			SE3->E3_PARCELA		:= SE1->E1_PARCELA
			SE3->E3_TIPO		:= _cTipo
			SE3->E3_BAIEMI		:= "E"
			SE3->E3_PEDIDO		:= _cPedido
			SE3->E3_ORIGEM		:= "F"
			SE3->E3_VENCTO		:= dBaixa
			SE3->E3_MOEDA		:= "01"
			SE3->E3_SDOC		:= "2"
			SE3->E3_XINADIM		:= "CRED.INAD."
			MsUnLock()

		End
	
	Next x
	
	RestArea(_area)
	
Return (lRet)

