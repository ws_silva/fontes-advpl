#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"
#include "fileio.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA007  �Autor  �Bruno Parreira      � Data �  02/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Exportacao de produtos B2B                                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA007(lAut) 

Local cQryPai	:= ""
Local cQryFilho	:= ""
Local cQryEstq	:= ""
Local cProdFil	:= ""
Local cPartNbr	:= ""
Local cCor		:= ""
Local cTam		:= ""
Local nPreco	:= 0
Local nPrecoDE	:= 0
Local nPrecoPOR	:= 0
Local cCodPai	:= ""
Local cDescPai	:= ""
Local cDescPrd	:= ""
Local cDescLvg	:= ""
Local cGrupo	:= ""
Local cSubGrp	:= ""
Local cCateg	:= ""
Local nI		:= 1
Local aCat		:= {}
Local nPos		:= 0
Local nx        := 0

Private cChvA1	:= ""
Private cChvA2	:= ""
Private cLog	:= ""
Private lMsErroAuto	:= .F.
Private cPerg	:= "HFATA007"

Private cArmazem  := ""
Private cMinEst   := ""
Private cOutlet   := ""
Private nMinimo   := 0
Private nMinEst   := 0//Val(cMinEst)
Private oWs       := NIL
Private oWsPrdPai := NIL
Private oWsPrdFil := NIL
Private oWsPrdCat := NIL
Private oWsEstq	  := NIL
Private oWSskuA	  := NIL
Private oWSskuB	  := NIL
Private cWsRet    := ""
Private aWsRet	  := {}

Private cHoraIni := Time()
Private cHoraFim := ""

Private cTabB2B	:= ""

Private cCodSub := ""
Private cCodOca := ""
Private cMaiorData := ""

Static oWsCat   	:= NIL
Static oWsFilPrd   	:= NIL
Static oWSTabPrc   	:= NIL
Static oWSPort   	:= NIL

Default lAut     := .F.

//---------- PREPARA��O DO AMBIENTE ----------//
lMenu := .f.
/*If Select("SX2") <> 0
	Conout("Menu .T.")
	lMenu := .t.
Else
	Conout("Menu .F.")
	Prepare Environment Empresa "01" Filial "0101"
Endif*/

If !lAut
	lMenu := .t.
Endif

cArmazem  := AllTrim(SuperGetMV("MV_XARMB2C",.F.,"'E0','E1'"))
cMinEst   := AllTrim(SuperGetMV("MV_XSLDMI2",.F.,"2"))
cOutlet   := AllTrim(SuperGetMV("MV_XOUTLET",.F.,"817073652"))
cOportu   := AllTrim(SuperGetMV("MV_XOPORTU",.F.,"2640399443"))

cTabB2B	:= AllTrim(SuperGetMV("MV_XTABB2B",.F.,"140"))
cCodSub := AllTrim(SuperGetMV("MV_XOCASI2",.F.,"2504210593")) //Codigo da Sub-Colecao Rakuten B2C
cCodOca := AllTrim(SuperGetMV("MV_XSUBCO2",.F.,"2714730602")) //Codigo da Ocasiao R-kuten B2C
cCnShow := AllTrim(SuperGetMV("MV_XCNSHOW",.F.,"005")) //Codigo do Canal Showroom

nMinimo   := Val(cMinEst)

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

MemoWrite("\log_rakuten\B2B\"+cDtTime+"_EXP-Produtos.txt","In�cio do processamento...")

nHandle := fopen("\log_rakuten\B2B\"+cDtTime+"_EXP-Produtos.txt",FO_READWRITE+FO_SHARED)

GravaLog("In�cio do processamento...")

oWsPrdPai := WSProduto():New()
oWsPrdFil := WSSKU():New()
oWsPrdCat := WSProdutoCategoria():New()
oWsEstq	  := WSEstoque():New()
oWsCat    := WSCategoria():New()
oWsFilPrd := WSFiltroProduto():New()
oWSTabPrc := WSTabelaPreco():New()
oWSPort   := WSPortifolio():New()

cChvA1 := ""//AllTrim(SuperGetMV("MV_XRA1B2C",.F.,"8B3CD696-D70A-4E59-8E2D-24A96ED8115E")) //Chave A1 para validar acesso ao webservice
cChvA2 := ""//AllTrim(SuperGetMV("MV_XRA2B2C",.F.,"2DD1CBF7-4B95-43BA-9F16-2C295E80E8CF")) //Chave A2 para validar acesso ao webservice

lProdPai  := .F.
lSku      := .T.
lTabPreco := .T.
If lMenu
	GravaLog("Executando via menu.")
	AjustaSX1(cPerg)
	If !Pergunte(cPerg)
		GravaLog("Processo cancelado pelo usu�rio.")
		Return
	Else
		//If Empty(mv_par01)
		//	MsgAlert("N�o foi digitado nenhum produto.","Aten��o")
		//	Return
		//Else
			If Len( AllTrim( mv_par01 ) ) <= 8
				lProdPai := .T.
			EndIf	
		//EndIf	
	EndIf
Else
	GravaLog("Executando exportacao automatica")

	mv_par01 := ""
	mv_par02 := 3
	mv_par03 := ""	
EndIf

nPrdPai := 0
nPrdFil := 0
aExcCat := {}
aCateg  := {}
lOutlet := .F.
lOportu := .F.
nContador := 0

If mv_par02 = 1
	lProdPai  := .F.
	lSku      := .T. 
	lTabPreco := .F.
ElseIf mv_par02 = 2
	lProdPai  := .F.
	lSku      := .F.
	lTabPreco := .T.
ElseIf mv_par02 = 3
	lProdPai  := .T.
	lSku      := .F.
	lTabPreco := .F.
Else
	lProdPai  := .T.
	lSku      := .T.
	lTabPreco := .T.
EndIf
	
//---------------------------------//
//--- Exporta��o do Produto Pai ---//
If lProdPai
	GravaLog("Executando Produto Pai")
	
	xFiltros(IF(lMenu,AllTrim(mv_par01),""), "PAI" )			
	TRBPAI->(dbGoTop())
	
	If TRBPAI->(!EOF())
		While TRBPAI->(!EOF()) 
			cCodPai	:= AllTrim(TRBPAI->B4_COD)
			cDescPai:= AllTrim(TRBPAI->B4_DESC)
			cDescPrd:= AllTrim(TRBPAI->DESCPT)+" -"
			cGrupo	:= AllTrim(TRBPAI->CATGRUPO)
			cCateg	:= AllTrim(TRBPAI->CATCATEGORIA)
			cSubGrp	:= AllTrim(TRBPAI->CATSUBGRUPO)
			cGrpOut := AllTrim(TRBPAI->GRPOUTLET)
			cCatOut := AllTrim(TRBPAI->CATOUTLET)
			nPreco	:= TRBPAI->PRCB2B
			
			ATUCAT(cCodPai)
			
			lInativ := SLDFILHOS(cCodPai)   // Saldo total dos produtos filhos

			cLavag := StrTran(TRBPAI->LAVAG,"|",", ")
			cTecid := AllTrim(TRBPAI->B4_YTECIDO)
			cCompo := AllTrim(TRBPAI->COMPO)+";"
			nDe    := 1
			nAte   := 0
			cDComp := ""

			//Composicao
			If !Empty(cCompo)
				nAte := At("-",cCompo)
				nCont := 0
				While nDe < Len(cCompo)
					cDComp += AllTrim(Str(Val(SubStr(cCompo,nAte+1,3))))+"% "+SubStr(cCompo,nDe,nAte-nDe)+", "
					nDe  := At(";",cCompo,nAte)+1
					nAte := At("-",cCompo,nDe)
					nCont++
					If nCont > 20
						Exit
					EndIf
				Enddo
			EndIf
			
			//Descricao do produto
			nDe    := 1
			nAte   := 0
			cDPrd  := ""
			If !Empty(cDescPrd)
				nAte   := At("-",cDescPrd,nDe+1)
				nCont := 0
				While nDe < Len(cDescPrd)
					cDPrd += "<br>"+SubStr(cDescPrd,nDe,nAte-nDe)
					nDe  := At("-",cDescPrd,nAte)
					nAte := At("-",cDescPrd,nDe+1)
					nCont++
					If nCont > 20
						Exit
					EndIf
				Enddo
			EndIf
			cCarac := "<strong>Modo de Lavagem: </strong>"+AllTrim(cLavag)
			cCarac += "<br><strong>Composi��o: </strong> "+AllTrim(cDComp)
			cCarac += "<br><strong>Tecido: </strong>"+AllTrim(cTecid)
			
			//lOutlet := VEROUTLET(cCodPai)
			lOportu := VEROPORTU(cCodPai)
			
			cMaiorData := MAIORDATA(cCodPai)
			
			aCateg := {AllTrim(TRBPAI->GRUPO),AllTrim(TRBPAI->CATGRUPO),AllTrim(TRBPAI->CATEGORIA),AllTrim(TRBPAI->CATCATEGORIA),AllTrim(TRBPAI->SUBGRUPO),AllTrim(TRBPAI->CATSUBGRUPO),IF(lOportu,cOportu,""),AllTrim(TRBPAI->GRPOUTLET),AllTrim(TRBPAI->CATOUTLET)}
			
			cNomeSub := AllTrim(TRBPAI->NOMESUB)
			
			//For nx := 1 to 1 //2	
				//If nx = 2
				//	cCodPai	:= "OUT_"+cCodPai
				//EndIf
				
				//nStatus := 0
			If lInativ
				nStatus := 2
			Else
				nStatus := 1
			EndIf
			
			lPrdNovo := .T.
			//nLojaCodigo,cCodIntPrd,cCodIntFab,cNomeProduto,nProdutoStatus,nTipoProduto,cA1,cA2
			If oWsPrdPai:Listar(1,cCodPai,"","",1,"1",cChvA1,cChvA2)
				cWsRet := oWsPrdPai:oWSListarResult:cDescricao
				GravaLog("Resultado: oWsPrdPai:Listar: "+cWsRet)	
				If "SUCESSO" $ UPPER(cWsRet)
					lPrdNovo := .F.
				Else
					GravaLog("Produto Novo: "+cCodPai)
				EndIf
			EndIf	
			
				//If TRBPAI->PRCPOR > 0 .And. TRBPAI->PRCDE >= TRB->PRCPOR
					//If !lInativ
					
			lContinua := .F.
			//nLojaCodigo,cCodIntPrd,cCodIntFor,cNomeProduto,cTituloProduto,cSubTituloProduto,cDescricaoProduto,cCaracteristicaProduto,cTexto1Produto,cTexto2Produto,cTexto3Produto,cTexto4Produto,cTexto5Produto,cTexto6Produto,cTexto7Produto,cTexto8Produto,cTexto9Produto,cTexto10Produto,nNumero1Produto,nNumero2Produto,nNumero3Produto,nNumero4Produto,nNumero5Produto,nNumero6Produto,nNumero7Produto,nNumero8Produto,nNumero9Produto,nNumero10Produto,cCodIntEnq,cModeloProduto,nPesoProduto,nPesoEmbalagemProduto,nAlturaProduto,nAlturaEmbalagemProduto,nLarguraProduto,nLarguraEmbalagemProduto,nProfPrd,nProfEmb,nEntregaProduto,nQuantidadeMaximaPorVenda,nStatusProduto,cTipoProduto,nPresente,nPrecoCheioProduto,nPrecoPor,nPersonalizacaoExtra,cPersonalizacaoLabel,cISBN,cEAN13,cYouTubeCode,cA1,cA2,oWS
			If oWsPrdPai:Salvar(1,cCodPai,"0001",cDescPai,cDescPai,"",cDPrd,cCarac,cMaiorData,"","","","","","","","","",0,0,0,0,0,0,0,0,0,0,"","",0.5,0.5,0,0,0,0,0,0,0,999,nStatus,"1",2,nPreco,nPreco,2,"2","","","",cChvA1,cChvA2)
			//If oWsPrdPai:Salvar(0,cCodPai,"0001",cDescPai,cDescPai,"",cDPrd,cCarac,"","","","","","","","","","",0,0,0,0,0,0,0,0,0,0,"","",0.5,0,0,0,0,0,0,0,0,999,nStatus,"1",2,nPreco,nPreco,2,"2","","","",cChvA1,cChvA2)
			//If oWsPrdPai:Salvar(1,"002481A0","0001","TESTE","TESTE","TESTE","TESTE","TESTE","","","","","","","","","","",0,0,0,0,0,0,0,0,0,0,"","",0.5,0,0,0,0,0,0,0,0,999,1,"1",2,nPreco,nPreco,2,"2","","","",cChvA1,cChvA2)  	
				cWsRet := oWsPrdPai:oWSSalvarResult:cDescricao
				GravaLog("Resultado: oWsPrdPai:Salvar: "+cCodPai + " - Status: "+AllTrim(Str(nStatus))+" Pre�o: "+AllTrim(str(nPreco))+" - "+cWsRet)

				If "SUCESSO" $ UPPER(cWsRet) 
					//xDatExp("PAI",Right(cCodPai,15))
				//EndIf
					lContinua := .T.
				Else
					If oWsPrdPai:AlterarStatus(1,cCodPai,nStatus,cChvA1,cChvA2)  	
						cWsRet := oWsPrdPai:oWSAlterarStatusResult:cDescricao
						GravaLog("Resultado: oWsPrdPai:AlterarStatus: "+cCodPai + " - "+cWsRet)
						
						If "SUCESSO" $ UPPER(cWsRet)
							If oWsPrdPai:AlterarPreco(1,cCodPai,nPreco,nPreco,cChvA1,cChvA2)  	
								cWsRet := oWsPrdPai:oWSAlterarPrecoResult:cDescricao
								GravaLog("Resultado: oWsPrdPai:AlterarPreco: "+cCodPai + " - "+cWsRet)
								
								If "SUCESSO" $ UPPER(cWsRet)
									lContinua := .T.
								EndIf
							EndIf
						EndIf								
					EndIf
				EndIf
			Else
				GravaLog("Erro na funcao Salvar Produto Pai: "+GetWSCError())
				Loop	
			EndIf

			If lContinua
				//Sub-Colecao
				//GravaLog("Parametros Filtro B4_YNSUBCO: 1,"+cCodSub+","+cCodPai+","+AllTrim(TRBPAI->B4_YNSUBCO)+",1,"+cChvA1+","+cChvA2)
				If oWsFilPrd:Salvar(1,cCodSub,cCodPai,AllTrim(TRBPAI->B4_YNSUBCO),1,cChvA1,cChvA2)
					cWsRet := oWsFilPrd:oWSSalvarResult:cDescricao
					If "SUCESSO" $ UPPER(cWsRet) 
						GravaLog("SubCole��o Salva com sucesso. Produto: "+cCodPai+" Sub-Colecao: "+AllTrim(TRBPAI->B4_YNSUBCO))
					Else
						GravaLog("Problema na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" Sub-Colecao: "+AllTrim(TRBPAI->B4_YNSUBCO)+" - "+cWsRet)
					EndIf
				Else
					GravaLog("Erro na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" Sub-Colecao: "+AllTrim(TRBPAI->B4_YNSUBCO))
				EndIf
				
				nDe    := 1
				nAte   := 0
				aOcasi := {}
				cOcasi := AllTrim(TRBPAI->B4_YOCASIA)
				If !Empty(cOcasi)
					While nDe < Len(cOcasi)
						aAdd(aOcasi,SubStr(cOcasi,nDe,5))
						nDe += 6
					EndDo
				EndIf
				
				//Ocasiao
				If Len(aOcasi) > 0
					For yy := 1 to Len(aOcasi)
						DbSelectArea("ZAD")
						DbSetOrder(1)
						If DbSeek(xFilial("ZAD")+aOcasi[yy])
							//GravaLog("Parametros Filtro B4_YOCASIA "+AllTrim(Str(yy))+": 1,"+cCodOca+","+cCodPai+","+AllTrim(TRBPAI->B4_YOCASIA)+",1,"+cChvA1+","+cChvA2)
							If oWsFilPrd:Salvar(1,cCodOca,cCodPai,AllTrim(ZAD->ZAD_DESCRI),1,cChvA1,cChvA2)
								cWsRet := oWsFilPrd:oWSSalvarResult:cDescricao
								If "SUCESSO" $ UPPER(cWsRet)
									GravaLog("SubCole��o Salva com sucesso. Produto: "+cCodPai+" Ocasi�o: "+AllTrim(ZAD->ZAD_DESCRI))
								Else
									GravaLog("Problema na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" OCasi�o: "+AllTrim(ZAD->ZAD_DESCRI)+" - "+cWsRet)
								EndIf
							Else
								GravaLog("Erro na fun��o oWsFilPrd:Salvar. Produto: "+cCodPai+" Ocasi�o: "+AllTrim(ZAD->ZAD_DESCRI))
							EndIf
						EndIf
					Next
				EndIf
				
				//Grupo
				If oWsPrdCat:Salvar( 1 , cCodPai , cGrupo , cChvA1 , cChvA2 )
					cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
					GravaLog("Produto: "+cCodPai + " x Grupo: "+ cGrupo +" - "+cWsRet)
				Else
					GravaLog("Erro na funcao Salvar Produto x Grupo: "+GetWSCError())
					Loop				
				EndIf
				//Categoria
				If oWsPrdCat:Salvar( 1 , cCodPai , cCateg , cChvA1 , cChvA2 )
					cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
					GravaLog("Produto: "+cCodPai + " x Categoria: "+ cCateg +" - "+cWsRet)
				Else
					GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
					Loop
				EndIf
				//SubGrupo
				If lPrdNovo
					If !Empty(cNomeSub)
						cCodCatSub := AllTrim(GetMV("MV_XSEQCAT"))
						If oWsCat:Salvar(1,cCodCatSub,cNomeSub,1,cCateg, cChvA1 , cChvA2 )
							cWsRet := oWsCat:oWSSalvarResult:cDescricao
							GravaLog("Categoria (Subgrupo) Atu: "+ cCodCatSub +" - "+cWsRet)
							PutMV("MV_XSEQCAT",SOMA1(cCodCatSub))
						Else
							GravaLog("Erro na funcao Salvar Categoria (SubGrupo) Atu: "+GetWSCError())
							Loop
						EndIf
						
						If oWsPrdCat:Salvar(1 , cCodPai , cCodCatSub , cChvA1 , cChvA2 )
							cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
							GravaLog("Produto: "+cCodPai+" x Categoria (Subgrupo) Atu: "+ cCodCatSub +" - "+cWsRet)
						Else
							GravaLog("Erro na funcao Salvar Produto x Categoria (SubGrupo) Atu: "+GetWSCError())
							Loop
						EndIf
					EndIf
				EndIf
				
				/*If !Empty(cCateg)
					If oWsCat:ListarArvore( 0 , cCateg , 1 , cChvA1 , cChvA2 )
						cWsRet := oWsCat:oWSListarArvoreResult:cDescricao
						
						If "SUCESSO" $ UPPER(cWsRet)
							
							GravaLog(OWSCAT:OWSLISTARARVORERESULT:OWSLISTA:OWSCLSCATEGORIA[1]:OWSCATEGORIASFILHAS:OWSCLSCATEGORIA[1]:cNome)
							
							For nI := 1 to Len(OWSCAT:OWSLISTARARVORERESULT:OWSLISTA:OWSCLSCATEGORIA[1]:OWSCATEGORIASFILHAS:OWSCLSCATEGORIA)
								cNomeCat := OWSCAT:OWSLISTARARVORERESULT:OWSLISTA:OWSCLSCATEGORIA[1]:OWSCATEGORIASFILHAS:OWSCLSCATEGORIA[nI]:CNOME
								If AllTrim(NOACENTO(UPPER(cNomeSub))) = AllTrim(NOACENTO(UPPER(cNomeCat)))
									cCodCatSub := AllTrim(OWSCAT:OWSLISTARARVORERESULT:OWSLISTA:OWSCLSCATEGORIA[1]:OWSCATEGORIASFILHAS:OWSCLSCATEGORIA[nI]:CCATEGORIACODIGOINTERNO)
									If oWsPrdCat:Salvar( 0 , cCodPai , cCodCatSub , cChvA1 , cChvA2 )
										cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
										GravaLog("Produto: "+cCodPai + " x SubGrupo: "+ cCodCatSub +" - "+cWsRet)
									Else
										GravaLog("Erro na funcao Salvar Produto x SubGrupo: "+GetWSCError())
										Loop				
									EndIf
								EndIf
							Next nI
						EndIf
					Else
						GravaLog("Erro na funcao ListarArvore: "+GetWSCError())
						Loop
					EndIf
				EndIf*/
				//Outlet
				If lOutlet
					If oWsPrdCat:Salvar( 1 , cCodPai , cGrpOut , cChvA1 , cChvA2 )
						cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
						GravaLog("Produto: "+cCodPai + " x Outlet Grp: "+ cGrpOut +" - "+cWsRet)
					Else
						GravaLog("Erro na funcao Salvar Produto x Outlet Grp: "+GetWSCError())
						Loop				
					EndIf
					
					If oWsPrdCat:Salvar( 1 , cCodPai , cCatOut , cChvA1 , cChvA2 )
						cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
						GravaLog("Produto: "+cCodPai + " x Outlet Cat: "+ cCatOut +" - "+cWsRet)
					Else
						GravaLog("Erro na funcao Salvar Produto x Outlet Cat: "+GetWSCError())
						Loop				
					EndIf
					
					//SubGrupo
					If lPrdNovo
						If !Empty(cNomeSub)
							cCodCatSub := AllTrim(GetMV("MV_XSEQCAT"))
							If oWsCat:Salvar(1,cCodCatSub,cNomeSub,1,cCatOut, cChvA1 , cChvA2 )
								cWsRet := oWsCat:oWSSalvarResult:cDescricao
								GravaLog("Categoria (Subgrupo) Atu: "+ cCodCatSub +" - "+cWsRet)
								PutMV("MV_XSEQCAT",SOMA1(cCodCatSub))
							Else
								GravaLog("Erro na funcao Salvar Categoria (SubGrupo) Atu: "+GetWSCError())
								Loop
							EndIf
							
							If oWsPrdCat:Salvar(1 , cCodPai , cCodCatSub , cChvA1 , cChvA2 )
								cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
								GravaLog("Produto: "+cCodPai+" x Categoria (Subgrupo) Atu: "+ cCodCatSub +" - "+cWsRet)
							Else
								GravaLog("Erro na funcao Salvar Produto x Categoria (SubGrupo) Atu: "+GetWSCError())
								Loop
							EndIf
						EndIf
					EndIf
				EndIf

				If lOportu
					If oWsPrdCat:Salvar(1, cCodPai , cOportu , cChvA1 , cChvA2 )
						cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
						GravaLog("Produto: "+cCodPai + " x Oportunidade: "+ cOportu +" - "+cWsRet)
					Else
						GravaLog("Erro na funcao Salvar Produto x Categoria (Oportunidade): "+GetWSCError())
						Loop				
					EndIf
				EndIf
				
				//Verifica��o Produto x Categoria
				/*If oWsPrdCat:ListarCategoriasAtivasPorProduto(1 , cCodPai , cChvA1 , cChvA2 )
					cWsRet := oWsPrdCat:oWSListarCategoriasAtivasPorProdutoResult:cDescricao
					
					//Se produto possui relacionamento
					If "SUCESSO" $ UPPER(cWsRet)*/
						/*For nI := 1 to Len(OWSPRDCAT:OWSLISTARCATEGORIASATIVASPORPRODUTORESULT:OWSLISTA:OWSCLSCATEGORIA)
							cCodInt := OWSPRDCAT:OWSLISTARCATEGORIASATIVASPORPRODUTORESULT:OWSLISTA:OWSCLSCATEGORIA[nI]:CCATEGORIACODIGOINTERNO
							If !Empty(cCodInt)
								If !(AllTrim(cCodInt) $ (AllTrim(aCateg[2])+"/"+AllTrim(aCateg[4])+"/"+AllTrim(aCateg[6])+"/"+AllTrim(aCateg[7])))
									aAdd(aExcCat,cCodInt)
								EndIf
								AADD(aCat,cCodInt)
							EndIf
						Next nI*/
						
						/*If Len(aExcCat) > 0
							For nE := 1 to Len(aExcCat) 
								If oWsPrdCat:Excluir(1,cCodPai,AllTrim(aExcCat[nE]),cChvA1,cChvA2)
									cWsRet := oWsPrdCat:oWSExcluirResult:cDescricao
									If "SUCESSO" $ UPPER(cWsRet)
										GravaLog("Categoria "+AllTrim(aExcCat[nE])+" do produto "+cCodPai+" exclu�da com sucesso.")
									EndIf
								Else
									GravaLog("Erro na func�o excluir. Produto: "+cCodPai+" Categoria: "+AllTrim(aExcCat[nE])+". Erro: "+GetWSCError())
									Loop
								EndIf
							Next
						EndIf*/
						
						/*If ( nPos := aScan( aCat , cGrupo ) ) = 0
							//Grupo
							If oWsPrdCat:Salvar(1, cCodPai , cGrupo , cChvA1 , cChvA2 )
								cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
								GravaLog("Produto: "+cCodPai + " x Categoria (Grupo) Atu: "+ cGrupo +" - "+cWsRet)
							Else
								GravaLog("Erro na funcao Salvar Produto x Categoria (Grupo) Atu: "+GetWSCError())
								Loop				
							EndIf
						EndIf
						
						If ( nPos := aScan( aCat , cCateg ) ) = 0
							//Categoria
							If oWsPrdCat:Salvar(1, cCodPai , cCateg , cChvA1 , cChvA2 )
								cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
								GravaLog("Produto: "+cCodPai + " x Categoria (Categoria) Atu: "+ cCateg +" - "+cWsRet)
							Else
								GravaLog("Erro na funcao Salvar Produto x Categoria (Categoria) Atu: "+GetWSCError())
								Loop					
							EndIf
						EndIf
						
						If ( nPos := aScan( aCat , cSubGrp ) ) = 0
							//SubGrupo
							If oWsPrdCat:Salvar(1 , cCodPai , cSubGrp , cChvA1 , cChvA2 )
								cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
								GravaLog("Produto: "+cCodPai + " x Categoria (Subgrupo) Atu: "+ cSubGrp +" - "+cWsRet)
							Else
								GravaLog("Erro na funcao Salvar Produto x Categoria (SubGrupo) Atu: "+GetWSCError())
								Loop				
							EndIf
						EndIf*/
						
						/*If lOutlet
							If ( nPos := aScan( aCat , cOutlet ) ) = 0
								//SubGrupo
								If oWsPrdCat:Salvar(1, cCodPai , cOutlet , cChvA1 , cChvA2 )
									cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
									GravaLog("Produto: "+cCodPai + " x Outlet: "+ cOutlet +" - "+cWsRet)
								Else
									GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
									Loop				
								EndIf
							EndIf
						EndIf*/
						
						/*If lOportu
							If ( nPos := aScan( aCat , cOportu ) ) = 0
								//SubGrupo
								If oWsPrdCat:Salvar(1, cCodPai , cOportu , cChvA1 , cChvA2 )
									cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
									GravaLog("Produto: "+cCodPai + " x Oportunidade: "+ cOportu +" - "+cWsRet)
								Else
									GravaLog("Erro na funcao Salvar Produto x Categoria (Oportunidade): "+GetWSCError())
									Loop				
								EndIf
							EndIf
						EndIf
					//Se nao possui relacionamento com alguma categoria	
					Else
						//Grupo
						If oWsPrdCat:Salvar(1, cCodPai , cGrupo , cChvA1 , cChvA2 )
							cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
							GravaLog("Produto: "+cCodPai + " x Categoria (Grupo) Nov: "+ cGrupo +" - "+cWsRet)
						Else
							GravaLog("Erro na funcao Salvar Produto x Categoria (Grupo) Nov: "+GetWSCError())
							Loop				
						EndIf
						//Categoria
						If oWsPrdCat:Salvar(1, cCodPai , cCateg , cChvA1 , cChvA2 )
							cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
							GravaLog("Produto: "+cCodPai + " x Categoria (Categoria) Nov: "+ cCateg +" - "+cWsRet)
						Else
							GravaLog("Erro na funcao Salvar Produto x Categoria (Categoria) Nov: "+GetWSCError())
							Loop
						EndIf
						//SubGrupo
						If oWsPrdCat:Salvar(1 , cCodPai , cSubGrp , cChvA1 , cChvA2 )
							cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
							GravaLog("Produto: "+cCodPai + " x Categoria (SubGrupo) Nov: "+ cSubGrp +" - "+cWsRet)
						Else
							GravaLog("Erro na funcao Salvar Produto x Categoria (SubGrupo) Nov: "+GetWSCError())
							Loop
						EndIf /*
						/*Outlet
						If lOutlet
							If oWsPrdCat:Salvar(1, cCodPai , cOutlet , cChvA1 , cChvA2 )
								cWsRet := oWsPrdCat:oWSSalvarResult:cDescricao
								GravaLog("Produto: "+cCodPai + " x Outlet: "+ cSubGrp +" - "+cWsRet)
							Else
								GravaLog("Erro na funcao Salvar Produto x Categoria: "+GetWSCError())
								Loop				
							EndIf
						EndIf*/
					/*EndIf
					
				Else
					GravaLog("Erro na funcao Listar Categorias Ativas Por Produto: "+GetWSCError())
					Loop
				EndIf*/
			EndIf
			
			aExcCat := {}
			//Next

			/*nPrdPai++
			
			If nPrdPai > 200
				nContador++
				cTime   := Time()
				cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
				MemoWrite("\log_rakuten\B2B\"+cDtTime+"_EXP-Produtos "+AllTrim(Str(nContador))+".log",cLog)
				cLog := "_"
				nPrdPai := 0
			EndIf*/
			
			aCateg  := {}
			
			TRBPAI->(DbSkip())
		EndDo
	Else
		If lMenu
			MsgAlert('Produto PAI n�o encontrado.',"Aten��o")
			GravaLog('Produto PAI n�o encontrado.')
			lSku := .F.
		EndIf	
	EndIf	
EndIf

If Select("TRBPAI") > 0
	TRBPAI->(DbCloseArea())
EndIf	

//lSku := .F.
//---------------------------------//
//--- Exporta��o do Produto SKU ---//
If lSku
	GravaLog("Executando Produto Filho(SKU)")
	
	If !Empty(mv_par03)
		GravaLog("Canal: "+GetAdvFVal("ZA3","ZA3_DESCRI",xFilial("ZA3")+mv_par03,1,""))
	EndIf
	
	xFiltros( IF(lMenu,AllTrim( mv_par01 ),) , "SKU" )
	TRBSKU->(dbGoTop())
	If TRBSKU->(!Eof())
		While TRBSKU->(!Eof())
			If !Empty(TRBSKU->B1_YFORMAT)
				cProdFil	:= AllTrim(SubStr( TRBSKU->B1_YFORMAT , 1 , AT("_" , TRBSKU->B1_YFORMAT ) -1 ))
				cProdPrt	:= AllTrim(TRBSKU->B1_COD)
				cPartNbr	:= AllTrim(TRBSKU->B1_YFORMAT)
				//cCor		:= AllTrim(SubStr( TRBSKU->B1_YFORMAT , AT("_" , TRBSKU->B1_YFORMAT ) +1 , RAT("_" , TRBSKU->B1_YFORMAT ) - AT("_" , TRBSKU->B1_YFORMAT ) -1 ))
				cTam		:= AllTrim(SubStr( TRBSKU->B1_YFORMAT , RAT("_" , TRBSKU->B1_YFORMAT ) +1  ))
			Else
				cProdFil	:= AllTrim(SubStr(TRBSKU->B1_COD,1,8))
				cProdPrt	:= AllTrim(TRBSKU->B1_COD)
				cPartNbr	:= AllTrim(TRBSKU->B1_COD)
				//cCor		:= AllTrim(SubStr(TRBSKU->B1_COD,9,3))
				cTam		:= AllTrim(SubStr(TRBSKU->B1_COD,12,4))
			EndIf	
			nPrecoB2B	:= TRBSKU->PRCB2B
			dDtLibPrd   := StoD(TRBSKU->B1_YDATLIB)
			
			cCor := AllTrim(GetAdvFVal("SBV","BV_DESCRI",xFilial("SBV")+"COR"+AllTrim(SubStr(TRBSKU->B1_COD,9,3)),1,AllTrim(SubStr(TRBSKU->B1_COD,9,3))))
			
			GravaLog("Codigo Protheus: "+cProdPrt)
			
			//If nPrecoPor > 0 .And. nPrecoDe >= nPrecoPor

			oWSskuA	:= oWsPrdFil:oWSsku1
			oWSskuB	:= oWsPrdFil:oWSsku2
			oWSskuC	:= oWsPrdFil:oWSsku3
			oWSskuD	:= oWsPrdFil:oWSsku4
			oWSskuE	:= oWsPrdFil:oWSsku5
			AADD(oWSskuA:cstring, "COR")
			AADD(oWSskuA:cstring, cCor)
			AADD(oWSskuB:cstring, "TAMANHO")
			AADD(oWSskuB:cstring, cTam)
			AADD(oWSskuC:cstring, "")
			AADD(oWSskuC:cstring, "")
			AADD(oWSskuD:cstring, "")
			AADD(oWSskuD:cstring, "")
			AADD(oWSskuE:cstring, "")
			AADD(oWSskuE:cstring, "")

			nEstoque := TRBSKU->TOTAL //ESTOQUE(cProdPrt)
			
			/*nProds  := 1 
			
			For nx := 1 to nProds
				If nx = 2
					cPartNbr := "OUT_"+cPartNbr
					cProdPrt := "OUT_"+cProdPrt
					cProdFil := "OUT_"+cProdFil
				EndIf*/
				
				DbSelectArea("ZA3")
				DbSetOrder(1)
				If DbSeek(xFilial("ZA3")+cCnShow) //CANAL SHOW ROOM
					dDatLib := ZA3->ZA3_DTINI
				Else
					dDatLib := DDATABASE
				EndIf
				
				lMinEst := .F.
				
				If !Empty(TRBSKU->B1_YDATLIB)
					If StoD(TRBSKU->B1_YDATLIB) >= dDatLib .And. AllTrim(TRBSKU->B1_YSITUAC) = "PNV"
						nMinEst := -1
						lMinEst := .T.
						If AllTrim(TRBSKU->B1_YCICLO) <> '00001'
							nEstoque := 99999
						EndIf
					EndIf
				EndIf
				
				If !lMinEst .And. Alltrim(TRBSKU->B1_YCICLO) = '00001' .And. AllTrim(TRBSKU->B1_YSITUAC) = "PV"
					nMinEst := -1
					lMinEst := .T.
				EndIf
				
				If !lMinEst
					nMinEst := nMinimo
				EndIf
				
				If nEstoque > nMinEst
					nStatus := 1
				Else
					nStatus := 2
				EndIf
				
				If nPrecoB2B > 0
					//GravaLog("Parametros Salvar SKU: 1,"+cProdFil+","+cPartNbr+","+AllTrim(Str(nPrecoB2B))+","+cCor+","+cTam+",,,,"+nStatus+","+TRBSKU->B1_YDATLIB)
					If oWsPrdFil:Salvar(1,cProdFil,cPartNbr,nPrecoB2B,oWSskuA,oWSskuB,oWSskuC,oWSskuD,oWSskuE,nStatus,TRBSKU->B1_YDATLIB,cChvA1,cChvA2)
						cWsRet := oWsPrdFil:oWSSalvarResult:cDescricao
						//GravaLog("Parametros oWsPrdFil:Salvar: 0,"+cProdFil+","+cPartNbr+","+AllTrim(Str(nPrecoPor))+","+cCor+","+cTam)
						GravaLog("Resultado oWsPrdFil:Salvar: Pre�o Por: "+AllTrim(Str(nPrecoB2B))+" - "+cPartNbr +" - "+cWsRet)
						
						lContinua := .F.
						If "SUCESSO" $ UPPER(cWsRet)
							//Atualiza Estoque do SKU
							lContinua := .T.
						Else
							If oWsPrdFil:AlterarStatus(1,cPartNbr,nStatus,cChvA1,cChvA2)	
								cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
								GravaLog("Resultado: oWsPrdFil:AlterarStatus: Produto: "+cPartNbr+" - "+cWsRet)
								
								If "SUCESSO" $ UPPER(cWsRet)
									If oWsPrdFil:AlterarPreco(1,cPartNbr,nPrecoB2B,cChvA1,cChvA2)	
										cWsRet := oWsPrdFil:oWSAlterarPrecoResult:cDescricao
										GravaLog("Resultado: oWsPrdFil:AlterarPreco: Produto: "+cPartNbr+" - "+cWsRet)
									
										If "SUCESSO" $ UPPER(cWsRet)
											lContinua := .T.
										EndIf
									EndIf
								EndIf
							EndIf
						EndIf
						
						//If lContinua
							/*If oWsEstq:Salvar(1, cPartNbr, cPartNbr, nEstoque, nMinEst, 3,cChvA1,cChvA2) //Atualiza estoque	
								cWsRet := oWsEstq:oWSSalvarResult:cDescricao
								//GravaLog("Parametros oWsEstq:Salvar: 0,"+cPartNbr+","+cPartNbr+","+AllTrim(Str(TRBEST->TOTAL_E0E1)))		
								GravaLog("Resultado: oWsEstq:Salvar: Estoque: "+AllTrim(Str(nEstoque))+" - "+cPartNbr+" - "+cWsRet)
								
								If "SUCESSO" $ UPPER(cWsRet)
									//xDatExp("ESTQ",Right(cProdPrt,15))
									DbSelectArea("ZZX")
									DbSetOrder(2)
									If DbSeek(xFilial("ZZX")+"B2B"+cProdPrt)
										RecLock("ZZX",.F.)
										ZZX->ZZX_QTDE := nEstoque
										ZZX->ZZX_DATA := DDATABASE
										ZZX->ZZX_HORA := Time()
										MsUnlock()
									Else
										RecLock("ZZX",.T.)
										ZZX->ZZX_COD    := cProdPrt
										ZZX->ZZX_QTDE   := nEstoque
										ZZX->ZZX_DATA   := DDATABASE
										ZZX->ZZX_HORA   := Time()
										ZZX->ZZX_ORIGEM := "B2B"
										MsUnlock()
									EndIf
								EndIf
								
							Else
								GravaLog("Erro na funcao Salvar Estoque: "+GetWSCError())
								Loop
							EndIf*/
							
							/*aCanais := {}
							
							If !Empty(mv_par03)
								DbSelectArea("ZA3")
								DbSetOrder(1)
								If DbSeek(xFilial("ZA3")+mv_par03)
									If !Empty(ZA3->ZA3_CODTAB) .And. !Empty(ZA3->ZA3_PORTIF)
										aAdd(aCanais,{AllTrim(ZA3->ZA3_CODIGO),AllTrim(ZA3->ZA3_PORTIF),AllTrim(ZA3->ZA3_CODTAB),ZA3->ZA3_DTINI,ZA3->ZA3_DTFIM})
									Else
										GravaLog("Canal selecionado n�o possui tabela de pre�o ou portif�lio amarrado.")
									EndIf
								EndIf
							Else
								DbSelectArea("ZA3")
								DbSetOrder(1)
								DbGoTop()
								While ZA3->(!EOF())
									If !Empty(ZA3->ZA3_CODTAB) .And. !Empty(ZA3->ZA3_PORTIF)
										aAdd(aCanais,{AllTrim(ZA3->ZA3_CODIGO),AllTrim(ZA3->ZA3_PORTIF),AllTrim(ZA3->ZA3_CODTAB),ZA3->ZA3_DTINI,ZA3->ZA3_DTFIM})
									EndIf
									ZA3->(DbSkip())
								EndDo
							EndIf	
							
							//aCanais := {{"004","0","90UN-4"},{"009","1","150K"}}
							//B2B-Varejo,     90UN-5
							//B2B-Franquia,   150K
							//B2B-HopeStore,  150K
							//SHOW-ROOM,      7890
							For cc := 1 to Len(aCanais)
								cTabela := "" 
								DbSelectArea("DA0")
								DbSetOrder(4)
								If DbSeek(xFilial("DA0")+aCanais[cc][3])
									cTabela := DA0->DA0_CODTAB
								Else
									DbSelectArea("DA0")
									DbSetOrder(1)
									If DbSeek(xFilial("DA0")+aCanais[cc][3])
										cTabela := DA0->DA0_CODTAB
									EndIf
								EndIf
								
								If !Empty(cTabela)
									DbSelectArea("DA1")
									DbSetOrder(1)
									If DbSeek(xFilial("DA1")+cTabela+cProdPrt)
										//GravaLog("Parametros SalvarItem Tab: (1,"+aCanais[cc][3]+","+cProdFil+","+cPartNbr+","+AllTrim(Str(DA1->DA1_PRCVEN))+",3)")
										If oWsTabPrc:SalvarItem(1,aCanais[cc][3],cProdFil,cPartNbr,DA1->DA1_PRCVEN,DA1->DA1_PRCVEN,1,cChvA1,cChvA2)
											cWsRet := oWsTabPrc:oWSSalvarItemResult:cDescricao
											//****GravaLog("Resultado: oWsTabPrc:SalvarItem: Tabela: "+aCanais[cc][3]+" - "+cPartNbr+" - "+cWsRet)
											If "SUCESSO" $ UPPER(cWsRet)
												//GravaLog("Parametros Portifolio SalvarItem: (1,"+aCanais[cc][2]+","+cProdFil+","+cPartNbr+",3)")
												If dDtLibPrd >= aCanais[cc][4] .And. dDtLibPrd <= aCanais[cc][5]
													If oWsPort:SalvarItem(1,aCanais[cc][2],cProdFil,cPartNbr,1,cChvA1,cChvA2)
														cWsRet := oWsPort:oWSSalvarItemResult:cDescricao
														//****GravaLog("Resultado: oWsPort:SalvarItem: Portif�lio: "+AllTrim(aCanais[cc][2])+" - "+cPartNbr+" - "+cWsRet)
														//If "SUCESSO" $ UPPER(cWsRet)
														//	Gravalog("Portif�lio salvo com sucesso. Portif�lio: "+aCanais[cc][2]+" - "+cPartNbr)
														//Else
														//	Gravalog("Problema ao salvar portif�lio. Portif�lio: "+aCanais[cc][2]+" - "+cPartNbr)
														//EndIf
													Else
														GravaLog("Erro na funcao SalvarItem do portif�lio: "+GetWSCError())
													EndIf
												Else
													GravaLog("Produto: "+cProdFil+" Portifolio: "+aCanais[cc][2]+": Data de liberacao do produto nao est� no intervalo de liberacao do canal (ZA3).")
												EndIf
											EndIf
										Else
											GravaLog("Erro na funcao SalvarItem da Tabela de pre�o: "+GetWSCError())
										EndIf
									Else
										GravaLog("Parametros Tab ExcluirItem: (1,"+aCanais[cc][3]+","+cProdFil+","+cPartNbr+")")
										If oWsTabPrc:ExcluirItem(1,aCanais[cc][3],cProdFil,cPartNbr,cChvA1,cChvA2)
											cWsRet := oWsTabPrc:oWSExcluirItemResult:cDescricao
											GravaLog("Resultado: oWsTabPrc:ExcluirItem: Tabela: "+aCanais[cc][3]+" - "+cPartNbr+" - "+cWsRet)
											//If "SUCESSO" $ UPPER(cWsRet)
												GravaLog("Parametros ExcluirItem Portifolio: (1,"+aCanais[cc][2]+","+cProdFil+","+cPartNbr+")")
												If oWsPort:ExcluirItem(1,aCanais[cc][2],cProdFil,cPartNbr,cChvA1,cChvA2)
													cWsRet := oWsPort:oWSExcluirItemResult:cDescricao
													GravaLog("Resultado: oWsPort:ExcluirItem: Portif�lio: "+AllTrim(aCanais[cc][2])+" - "+cPartNbr+" - "+cWsRet)
													//If "SUCESSO" $ UPPER(cWsRet)
													//	Gravalog("Portif�lio exclu�do com sucesso. Portif�lio: "+aCanais[cc][2]+" - "+cPartNbr)
													//Else
													//	Gravalog("Problema ao excluir portif�lio. Portif�lio: "+aCanais[cc][2]+" - "+cPartNbr)
													//EndIf
												Else
													GravaLog("Erro na funcao ExcluirItem do portif�lio: "+GetWSCError())
												EndIf
											//Else
											//	GravaLog("Produto n�o encontrado para exclusao na Tabela: "+aCanais[cc][3]+" - "+cPartNbr)
											//EndIf
										Else
											GravaLog("Erro na funcao ExcluirItem da Tabela de pre�o: "+GetWSCError())
										EndIf
									EndIf
								Else
									GravaLog("Tabela de pre�o "+aCanais[cc][3]+" n�o encontrada.")								
								EndIf
							Next*/
						//EndIf
					Else
						GravaLog("Erro na funcao Salvar: "+GetWSCError())
						Loop		
					EndIf
				Else
					If oWsPrdFil:Salvar(1,cPartNbr,cChvA1,cChvA2)
						cWsRet := oWsPrdFil:oWSExcluirResult:cDescricao
						GravaLog("Resultado oWsPrdFil:Excluir: Produto: "+cPartNbr +" - "+cWsRet)
					Else
						GravaLog("Erro na fun��o Excluir do produto: "+cPartNbr)
					EndIf
				EndIf	
				/*If TRBEST->TOTAL_E0E1 > 0 //Ativo o produto caso o saldo seja maior que 0
					GravaLog("Atualiza��o de estoque. Produto: "+cPartNbr)
					oWsPrdFil:AlterarStatus(1 , cPartNbr , 1 ,cChvA1,cChvA2)
					cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
					GravaLog("Parametros Ativa Produto oWsPrdFil:AlterarStatus: 0,"+cPartNbr+",1")
					GravaLog("Resultado Ativa Produto: oWsPrdFil:AlterarStatus: " + cPartNbr + " - " + cWsRet)	
					
					If "SUCESSO" $ UPPER(cWsRet)
						xDatExp("SKU",Right(cProdPrt,15))					
					EndIf
				EndIf/*
			//--- Altera��o de Produto	
			//Else
				//Verifica estoque e inativa o SKU caso esteja zerado
				
				//If oWsEstq:Retornar(1 , cPartNbr , cPartNbr ,cChvA1,cChvA2)  	
				//	aWsRet := oWsEstq:oWSRetornarResult:oWSLista	
					/*If aWsRet:OWSCLSESTOQUE[1]:NQTDE <= 0					
						oWsPrdFil:AlterarStatus(1 , cPartNbr , 2 ,cChvA1,cChvA2)
						cWsRet:oWsPrdFil:oWSAlterarStatusResult:cDescricao
						GravaLog("Resultado Produto SKU inativado: " + cProdPrt + " - " + cWsRet)
						
						If "Status da SKU Alterado com Sucesso" $ cWsRet
							xDatExp("SKU",Right(cProdPrt,15))					
						EndIf					
						
					//Else*/
				/*	If TRBEST->TOTAL_E0E1 > 0 .And. aWsRet:OWSCLSESTOQUE[1]:NQTDE <> TRBEST->TOTAL_E0E1
						If oWsEstq:Salvar(1, cPartNbr, cPartNbr, TRBEST->TOTAL_E0E1, 5, 3,cChvA1,cChvA2) //Atualiza estoque
							cWsRet := oWsEstq:oWSSalvarResult:cDescricao
							GravaLog("Parametros oWsEstq:Salvar: 0,"+cPartNbr+","+cPartNbr+","+AllTrim(Str(TRBEST->TOTAL_E0E1)))		
							GravaLog("Resultado: oWsEstq:Salvar: "+cPartNbr+" - "+cWsRet)
							
							If "SUCESSO" $ UPPER(cWsRet)
								xDatExp("ESTQ",Right(cProdPrt,15))
							EndIf			
										
						Else
							GravaLog("Erro na funcao Salvar Estoque: "+GetWSCError())
							Loop
						EndIf				
					EndIf
									
				Else
					GravaLog("Erro na funcao Retornar: "+GetWSCError())
					Loop		
				EndIf
				*/
				//Verifica o pre�o e atualiza caso necess�rio
				/*If oWsPrdFil:Listar(1 , cPartNbr , cPartNbr , 1 ,cChvA1,cChvA2)
					cWsRet := oWsPrdFil:oWsListarResult:cDescricao
					GravaLog("Parametros oWsPrdFil:Listar 0,"+cPartNbr+","+cPartNbr+",1")		
					GravaLog("Resultado: oWsPrdFil:Listar "+cPartNbr+" - "+cWsRet)
					
					If "SUCESSO" $ UPPER(cWsRet)
						aWsRet := oWsPrdFil:oWSListarResult:oWSLista
						GravaLog("Pre�o Atual: "+AllTrim(Str(aWsRet:OWSCLSPRODUTOCARACTERISTICA[1]:NPRECOPOR))+" Preco Por: "+ Alltrim(Str(nPrecoPor)))
						If aWsRet:OWSCLSPRODUTOCARACTERISTICA[1]:NPRECOPOR <> nPrecoPor 
							If oWsPrdFil:AlterarPreco(1 , cPartNbr , nPrecoPor ,cChvA1,cChvA2)
								cWsRet := oWsPrdFil:oWSAlterarPrecoResult:cDescricao
								GravaLog("Resultado: oWsPrdFil:AlterarPreco: " + cProdPrt + " - " + cWsRet)
								
								If "SUCESSO" $ UPPER(cWsRet)
									xDatExp("SKU",Right(cProdPrt,15))					
								EndIf
										
							Else
								GravaLog("Erro na funcao AlterarPreco: "+GetWSCError())
								Loop
							EndIf
						EndIf
					EndIf	
				Else
					GravaLog("Erro na funcao Retornar: "+GetWSCError())
					Loop
				EndIf*/
				
				/*If TRBEST->TOTAL_E0E1 <= nMinEst
					GravaLog("Estoque zerado. Inativando Produto: "+cPartNbr)
					INATIVA(cPartNbr)
				EndIf*/	
			//Next
			/*Else
				If nPrecoPor = 0
					GravaLog("Pre�o POR zerado para o produto: "+cCodPai)
				EndIf
				If nPrecoPor > nPrecoDe
					GravaLog("Pre�o POR maior que pre�o DE para o produto: "+cCodPai)
				EndIf
			EndIf*/
			
			/*nPrdFil++
			
			If nPrdFil > 200
				nContador++
				cTime   := Time()
				cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
				MemoWrite("\log_rakuten\B2B\"+cDtTime+"_EXP-Produtos "+AllTrim(Str(nContador))+".log",cLog)
				cLog := "_"
				nPrdFil := 0
			EndIf*/

			TRBSKU->(dbSkip())
		
		EndDo
	Else
		If lMenu
			MsgAlert("Produto SKU n�o encontrado.","Aten��o")
			GravaLog("Produto SKU n�o encontrado.")
		EndIf	
	EndIf
EndIf

If Select("TRBSKU") > 0
	TRBSKU->(DbCloseArea())
EndIf

If lTabPreco
	
	GravaLog("Iniciando Exporta��o da Tabela de Pre�o...")
	
	DbSelectArea("ZA3")
	DbSetOrder(1)
	DbGoTop()
	While ZA3->(!EOF())
	
		If !Empty(ZA3->ZA3_CODTAB) .And. !Empty(ZA3->ZA3_PORTIF)
			
			TABPRECO(ZA3->ZA3_CODIGO,mv_par01)
			
			DbSelectArea("TRBTAB")
			DbGoTop()
			
			If !EOF()
				While TRBTAB->(!EOF())
					If !Empty(TRBTAB->B1_YFORMAT)
						cProdFil	:= AllTrim(SubStr( TRBTAB->B1_YFORMAT , 1 , AT("_" , TRBTAB->B1_YFORMAT ) -1 ))
						cProdPrt	:= AllTrim(TRBTAB->B1_COD)
						cPartNbr	:= AllTrim(TRBTAB->B1_YFORMAT)
					Else
						cProdFil	:= AllTrim(SubStr(TRBTAB->B1_COD,1,8))
						cProdPrt	:= AllTrim(TRBTAB->B1_COD)
						cPartNbr	:= AllTrim(TRBTAB->B1_COD)
					EndIf
					
					dDtLibPrd := StoD(TRBTAB->B1_YDATLIB)
					dDtIni    := StoD(TRBTAB->ZA3_DTINI)
					dDtFim    := StoD(TRBTAB->ZA3_DTFIM)
					If 	TRBTAB->SITUACAO = 'I'
						If oWsTabPrc:SalvarItem(1,TRBTAB->ZA3_CODTAB,cProdFil,cPartNbr,TRBTAB->PRECO,TRBTAB->PRECO,1,cChvA1,cChvA2)
							cWsRet := oWsTabPrc:oWSSalvarItemResult:cDescricao
							GravaLog("TabPreco:Salvar: Produto: "+cPartNbr+" Tabela: "+TRBTAB->ZA3_CODTAB+" Pre�o: "+AllTrim(Str(TRBTAB->PRECO))+" Resultado: "+cWsRet)
							If "SUCESSO" $ UPPER(cWsRet)
								If dDtLibPrd >= dDtIni .And. dDtLibPrd <= dDtFim
									If oWsPort:SalvarItem(1,TRBTAB->ZA3_PORTIF,cProdFil,cPartNbr,1,cChvA1,cChvA2)
										cWsRet := oWsPort:oWSSalvarItemResult:cDescricao
										GravaLog("Portifoli:Salvar: Produto: "+cPartNbr+" Portifolio: "+TRBTAB->ZA3_PORTIF+" Resultado: "+cWsRet)
									Else
										GravaLog("Erro na funcao SalvarItem do portif�lio: "+GetWSCError())
									EndIf
								Else
									GravaLog("Produto: "+cProdFil+" Portifolio: "+TRBTAB->ZA3_PORTIF+": Data de liberacao do produto nao est� no intervalo de liberacao do canal (ZA3).")
								EndIf
								TcSqlExec(TRBTAB->QUERY)
							EndIf
						Else
							GravaLog("Erro na funcao SalvarItem da Tabela de pre�o: "+GetWSCError())
						EndIf
					Else
						If oWsTabPrc:ExcluirItem(1,TRBTAB->ZA3_CODTAB,cProdFil,cPartNbr,cChvA1,cChvA2)
							cWsRet := oWsTabPrc:oWSExcluirItemResult:cDescricao
							GravaLog("TabPreco:Excluir: Produto: "+cPartNbr+" Tabela: "+TRBTAB->ZA3_CODTAB+" Resultado: "+cWsRet)
							//If "SUCESSO" $ UPPER(cWsRet)
								If oWsPort:ExcluirItem(1,TRBTAB->ZA3_PORTIF,cProdFil,cPartNbr,cChvA1,cChvA2)
									cWsRet := oWsPort:oWSExcluirItemResult:cDescricao
									GravaLog("Portifoli:Excluir: Produto: "+cPartNbr+" Portifolio: "+TRBTAB->ZA3_PORTIF+" Resultado: "+cWsRet)
								Else
									GravaLog("Erro na funcao ExcluirItem do portif�lio: "+GetWSCError())
								EndIf
							//Else
							//	GravaLog("Produto n�o encontrado para exclusao na Tabela: "+aCanais[cc][3]+" - "+cPartNbr)
							//EndIf
							TcSqlExec(TRBTAB->QUERY)
						Else
							GravaLog("Erro na funcao ExcluirItem da Tabela de pre�o: "+GetWSCError())
						EndIf
					EndIf
		
					TRBTAB->(DbSkip())
				EndDo
			Else
				GravaLog("EOF")
			EndIf
			
			If Select("TRBTAB") > 0
				TRBTAB->(DbCloseArea())
			EndIf
		EndIf
		
		ZA3->(DbSkip())
	EndDo
	
	/*aCanais := {}
								
	If !Empty(mv_par03)
		DbSelectArea("ZA3")
		DbSetOrder(1)
		If DbSeek(xFilial("ZA3")+mv_par03)
			If !Empty(ZA3->ZA3_CODTAB) .And. !Empty(ZA3->ZA3_PORTIF)
				aAdd(aCanais,{AllTrim(ZA3->ZA3_CODIGO),AllTrim(ZA3->ZA3_PORTIF),AllTrim(ZA3->ZA3_CODTAB),ZA3->ZA3_DTINI,ZA3->ZA3_DTFIM})
			Else
				GravaLog("Canal selecionado n�o possui tabela de pre�o ou portif�lio amarrado.")
			EndIf
		EndIf
	Else
		DbSelectArea("ZA3")
		DbSetOrder(1)
		DbGoTop()
		While ZA3->(!EOF())
			If !Empty(ZA3->ZA3_CODTAB) .And. !Empty(ZA3->ZA3_PORTIF)
				aAdd(aCanais,{AllTrim(ZA3->ZA3_CODIGO),AllTrim(ZA3->ZA3_PORTIF),AllTrim(ZA3->ZA3_CODTAB),ZA3->ZA3_DTINI,ZA3->ZA3_DTFIM})
			EndIf
			ZA3->(DbSkip())
		EndDo
	EndIf	*/

EndIf

GravaLog("Fim do processamento.")

//nContador++
//MemoWrite("\log_rakuten\B2B\"+cDtTime+"_EXP-Produtos "+Alltrim(Str(nContador))+".log",cLog)

//DbCloseAll()

fclose(nHandle) 

If lMenu
	cHoraFim := Time()
	MsgInfo("Exporta��o de Produtos finalizada. "+cHoraIni+" - "+cHoraFim,"Aten��o")
Else
	Reset Environment
Endif

Return

Static Function TABPRECO(cCodCanal,cproduto)
Local cQuery  := ""
Local cTabB2B := SuperGetMV("MV_XTABB2B",.F.,"140")

cQuery := " SELECT 'DELETE B2BTABPRC WHERE CANAL = '''+ZA3_CODIGO+''' AND SKU = '''+B1_COD+'''; INSERT INTO B2BTABPRC (CANAL, SKU, SITUACAO, PRECO) VALUES ('''+ZA3_CODIGO+''','''+B1_COD+''','''+SITUACAO+''','+CAST(PRECO AS VARCHAR)+')' AS QUERY,* FROM ( "
cQuery += CRLF + "SELECT ZA3_CODIGO,ZA3_PORTIF,ZA3_DESCRI,ZA3_CODTAB,ZA3_DTINI,ZA3_DTFIM,B1_COD,B1_YFORMAT,B1_YDATLIB, "
cQuery += CRLF + "CASE WHEN DA1A.DA1_PRCVEN IS NULL AND DA1B.DA1_PRCVEN IS NOT NULL THEN 'I' "
cQuery += CRLF + "	 WHEN DA1B.DA1_PRCVEN IS NULL AND DA1A.DA1_PRCVEN IS NOT NULL THEN 'I' "
cQuery += CRLF + "	 ELSE 'E' END AS SITUACAO, "
cQuery += CRLF + "CASE WHEN DA1A.DA1_PRCVEN IS NULL AND DA1B.DA1_PRCVEN IS NOT NULL THEN DA1B.DA1_PRCVEN " 
cQuery += CRLF + "	 WHEN DA1B.DA1_PRCVEN IS NULL AND DA1A.DA1_PRCVEN IS NOT NULL THEN DA1A.DA1_PRCVEN " 
cQuery += CRLF + "	 ELSE 0 END AS PRECO "
cQuery += CRLF + "FROM "+RetSqlName("SB1")+" SB1 " 
cQuery += CRLF + "left join "+RetSqlName("ZA3")+" ZA3 on ZA3.D_E_L_E_T_ = '' and ZA3_CODTAB <> '' and ZA3_CODIGO = '"+cCodCanal+"' "
cQuery += CRLF + "LEFT join "+RetSqlName("DA0")+" DA0A on DA0A.D_E_L_E_T_ = '' and DA0A.DA0_XTBMIL = ZA3_CODTAB "
cQuery += CRLF + "LEFT join "+RetSqlName("DA0")+" DA0B on DA0B.D_E_L_E_T_ = '' and DA0B.DA0_CODTAB = ZA3_CODTAB "
cQuery += CRLF + "LEFT join "+RetSqlName("DA1")+" DA1A on DA1A.D_E_L_E_T_ = '' and DA0A.DA0_CODTAB = DA1A.DA1_CODTAB AND DA1A.DA1_CODPRO = B1_COD "
cQuery += CRLF + "LEFT join "+RetSqlName("DA1")+" DA1B on DA1B.D_E_L_E_T_ = '' and DA0B.DA0_CODTAB = DA1B.DA1_CODTAB AND DA1B.DA1_CODPRO = B1_COD "
cQuery += CRLF + "WHERE 	SB1.B1_FILIAL = '    '  "
		If !Empty(cProduto)
			If Len(alltrim(cProduto)) <= 8
				cQuery += CRLF + " AND SB1.B1_COD LIKE '"+alltrim(cProduto)+"%' "
			Else
				cQuery += CRLF + " AND SB1.B1_COD = '"+alltrim(cProduto)+"' "
			EndIf
		//Else
			//cWhere := "SB1.B1_MSEXP = '' "
		EndIf

cQuery += CRLF + "		AND SB1.D_E_L_E_T_ = ''  "
cQuery += CRLF + "		AND SB1.B1_YDWEB2B = 'S'  "
cQuery += CRLF + "		AND SB1.B1_YCOLECA IN	( SELECT ZAA.ZAA_CODIGO "  
cQuery += CRLF + "								  FROM "+RetSqlName("ZAA")+" ZAA " 
cQuery += CRLF + "								  WHERE ZAA.ZAA_FILIAL = '    '  "
cQuery += CRLF + "								  AND ZAA.D_E_L_E_T_ = ''  "
cQuery += CRLF + "								  AND ZAA.ZAA_DWEB2B = 'S'  "
cQuery += CRLF + "								)  "
cQuery += CRLF + "		AND SB1.B1_YSITUAC IN	( SELECT ZA8.ZA8_CODIGO "  
cQuery += CRLF + "								  FROM "+RetSqlName("ZA8")+" ZA8 " 
cQuery += CRLF + "								  WHERE ZA8.ZA8_FILIAL = '    '  "
cQuery += CRLF + "								  AND ZA8.D_E_L_E_T_ = ''  "
cQuery += CRLF + "								  AND ZA8.ZA8_DWEB2B = 'T'  "
cQuery += CRLF + "								)  "
cQuery += CRLF + "		AND SB1.B1_COD IN 		( SELECT DA1.DA1_CODPRO " 
cQuery += CRLF + "								  FROM "+RetSqlName("DA1")+" DA1 " 
cQuery += CRLF + "								  WHERE DA1.DA1_FILIAL = '    '  "
cQuery += CRLF + "								  AND DA1.D_E_L_E_T_ = ''  "
cQuery += CRLF + "								  AND DA1.DA1_CODTAB IN ('"+cTabB2B+"')	) ) Z "
cQuery += CRLF + "WHERE  "
cQuery += CRLF + "((SELECT PRECO FROM B2BTABPRC B2B WHERE ltrim(rtrim(CANAL)) = ZA3_CODIGO AND ltrim(rtrim(SKU)) = B1_COD AND Z.SITUACAO = ltrim(rtrim(B2B.SITUACAO))) IS NULL OR " 
cQuery += CRLF + "(SELECT PRECO FROM B2BTABPRC B2B WHERE ltrim(rtrim(CANAL)) = ZA3_CODIGO AND ltrim(rtrim(SKU)) = B1_COD AND Z.SITUACAO = ltrim(rtrim(B2B.SITUACAO))) <> PRECO) "
//cQuery += CRLF + "and B1_COD LIKE '00I24830%' "
cQuery += CRLF + "order by ZA3_CODIGO,ZA3_CODTAB,B1_COD "

MemoWrite("HFATA007_TABELA_PRECO.txt",cQuery)

If Select("TRBTAB") > 0
	TRBTAB->(DbCloseArea())
EndIf	
	
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRBTAB", .F., .T.)

DbSelectArea("TRBTAB")
DbGoTop()

Return

Static Function MAIORDATA(cRef)
Local cRet := ""
Local cQuery := ""

cQuery := "select MAX(B1_YDATLIB) AS MAIORDATA  "
cQuery += CRLF + "from "+RetSqlName("SB1")+" SB1 (nolock) "
cQuery += CRLF + "where SUBSTRING(B1_COD,1,8) = '"+cRef+"' "
cQuery += CRLF + "and D_E_L_E_T_ = '' "

MemoWrite("HFATA007_MAIORDATA.txt",cQuery)
	
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"DAT", .F., .T.)

DbSelectArea("DAT")

If DAT->(!EOF())
	cRet := DAT->MAIORDATA
EndIf

DAT->(DbCloseArea())

Return cRet

Static Function VEROPORTU(cRef)
Local cQuery := ""
Local lRet   := .F.
Local cSitOport := AllTrim(SuperGetMV("MV_XSITOPO",.F.,"'NPVO'"))

cQuery := "select B1_COD "
cQuery += CRLF + "from "+RetSqlName("SB1")+" SB1 (nolock) "
cQuery += CRLF + "where SUBSTRING(B1_COD,1,8) = '"+cRef+"' "
cQuery += CRLF + "and B1_YSITUAC IN ("+cSitOport+") "
cQuery += CRLF + "and D_E_L_E_T_ = '' "

MemoWrite("HFATA007_OPORTU.txt",cQuery)
	
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"OPO", .F., .T.)

DbSelectArea("OPO")

If OPO->(!EOF())
	lRet := .T.
EndIf

OPO->(DbCloseArea())

Return lRet

/*Static Function VEROUTLET(cRef)
Local cQuery := ""
Local lRet   := .F.
Local cTabB2B	:= SuperGetMV("MV_XTABB2B",.F.,"140")

cQuery := "select * from ( "
cQuery += CRLF + "select ISNULL(DA11.DA1_PRCVEN,0) AS PRCDE,ISNULL(DA12.DA1_PRCVEN,0) AS PRCPOR "
cQuery += CRLF + "from "+RetSqlName("DA1")+" DA11 "
cQuery += CRLF + "inner join "+RetSqlName("DA1")+" DA12 "
cQuery += CRLF + "on DA12.DA1_CODTAB = '"+cTabB2B+"' "
cQuery += CRLF + "and DA11.DA1_CODPRO = DA12.DA1_CODPRO "
cQuery += CRLF + "and DA12.D_E_L_E_T_ = '' "
cQuery += CRLF + "where DA11.DA1_CODTAB = '"+cTabDE+"' and DA11.D_E_L_E_T_ = '' and DA11.DA1_CODPRO like '"+cRef+"%' "
cQuery += CRLF + "group by DA11.DA1_PRCVEN,DA12.DA1_PRCVEN ) Z "
cQuery += CRLF + "where PRCDE > PRCPOR "

MemoWrite("HFATA007_OUTLET.txt",cQuery)
	
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"OUT", .F., .T.)

DbSelectArea("OUT")

If OUT->(!EOF())
	lRet := .T.
EndIf

OUT->(DbCloseArea())

Return lRet*/

Static Function INATIVA(cPartNbr)

oWsPrdFil:AlterarStatus(1 , cPartNbr , 2 ,cChvA1,cChvA2)
cWsRet := oWsPrdFil:oWSAlterarStatusResult:cDescricao
//GravaLog("Parametros Inativa Produto oWsPrdFil:AlterarStatus: 1,"+cPartNbr+",2")
GravaLog("Resultado Inativa Produto: oWsPrdFil:AlterarStatus: " + cPartNbr + " - " + cWsRet)	

If "SUCESSO" $ UPPER(cWsRet)
	xDatExp("SKU",Right(cProdPrt,15))					
EndIf

Return

Static Function ATUCAT(cProduto)
Local cquery := ""

For xx := 1 to 2
	cQuery := "SELECT * FROM ("
	//If xx = 1
	cQuery += CRLF + "SELECT B4_GRUPO AS GRUPO,ISNULL(BM_YCTRA2,'') AS CATGRUPO,BM_DESC, "
	cQuery += CRLF + "B4_YCATEGO AS CATEGORIA,ISNULL(ZAC_YCTRA2,'') AS CATCATEGORIA,ZAC_DESCRI,B4_YSUBGRP AS SUBGRUPO,ISNULL(ZAG_YCTRA2,'') AS CATSUBGRUPO,ZAG_DESCRI "	
	/*ElseIf xx = 2
		cQuery += CRLF + "SELECT B4_YCATEGO AS CATEGORIA,ISNULL(ZAC_YCTRA2,'') AS CATCATEGORIA "
	Else
		cQuery += CRLF + "SELECT B4_YSUBGRP AS SUBGRUPO,ISNULL(ZAG_YCTRA2,'') AS CATSUBGRUPO "
	EndIf*/
	cQuery += CRLF + "FROM "+RetSqlName("SB4")+" SB4 (nolock) "
	cQuery += CRLF + "LEFT JOIN "+RetSqlName("SBM")+" SBM (nolock) "
	cQuery += CRLF + "ON		SBM.BM_GRUPO = SB4.B4_GRUPO "
	cQuery += CRLF + "		AND SBM.BM_FILIAL = '"+xFilial("SBM")+"' "
	cQuery += CRLF + "		AND SBM.D_E_L_E_T_ = '' "
			
	cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAC")+" ZAC (nolock) "
	cQuery += CRLF + "ON		ZAC.ZAC_CODIGO = SB4.B4_YCATEGO "
	cQuery += CRLF + "		AND ZAC.ZAC_FILIAL = '"+xFilial("ZAC")+"' "
	cQuery += CRLF + "		AND ZAC.D_E_L_E_T_ = '' "
	
	cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAG")+" ZAG (nolock) "
	cQuery += CRLF + "ON		ZAG.ZAG_CODIGO = SB4.B4_YSUBGRP "
	cQuery += CRLF + "		AND ZAG.ZAG_CATEGO = SB4.B4_YCATEGO "
	cQuery += CRLF + "		AND ZAG.ZAG_FILIAL = '"+xFilial("ZAG")+"' "
	cQuery += CRLF + "		AND ZAG.D_E_L_E_T_ = '' "
			
	cQuery += CRLF + "WHERE	SB4.B4_FILIAL = '"+xFilial("SB4")+"' "
	cQuery += CRLF + "        AND SB4.D_E_L_E_T_ = '' "
	If !Empty(cProduto)
		If Len(AllTrim(cProduto)) <= 8
			cQuery += CRLF + " AND SB4.B4_COD like '"+AllTrim(cProduto)+"%' "
		Else
			cQuery += CRLF + " AND SB4.B4_COD = '"+AllTrim(cProduto)+"' "
		EndIf
	EndIf 
	cQuery += CRLF + "        AND SB4.B4_DWEB2B = 'S' "
	cQuery += CRLF + "AND 	SB4.B4_YCOLECA IN   ( SELECT ZAA.ZAA_CODIGO " 
	cQuery += CRLF + "                                      FROM "+RetSqlName("ZAA")+" ZAA (nolock) "
	cQuery += CRLF + "                                      WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
	cQuery += CRLF + "                                      AND ZAA.D_E_L_E_T_ = '' "
	cQuery += CRLF + "                                      AND ZAA.ZAA_DWEB2B = 'S' ) ) Z "
	If xx = 1
		cQuery += CRLF + " WHERE  CATGRUPO = ''"
		//cQuery += CRLF + " GROUP BY B4_GRUPO "
	ElseIf xx = 2
		cQuery += CRLF + "WHERE  CATCATEGORIA = ''"
		//cQuery += CRLF + " GROUP BY B4_YCATEGO "
	Else
		cQuery += CRLF + "WHERE  CATSUBGRUPO = ''"
		//cQuery += CRLF + " GROUP BY B4_YSUBGRP "
	EndIf
	cQuery += CRLF + " GROUP BY GRUPO,CATGRUPO,BM_DESC,CATEGORIA,CATCATEGORIA,ZAC_DESCRI,SUBGRUPO,CATSUBGRUPO,ZAG_DESCRI "
	//cQuery += CRLF + " order by B4_COD"
	
	MemoWrite("HFATA007_ATUCAT.txt",cQuery)
	
	cQuery := ChangeQuery(cQuery)
	
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRBCAT", .F., .T.)

	DbSelectArea("TRBCAT")
	
	If TRBCAT->(!EOF())
		While TRBCAT->(!EOF())
			If xx = 1
				cCodCat := AllTrim(TRBCAT->GRUPO)
				cNomCat := AllTrim(TRBCAT->BM_DESC)
				If !Empty(cCodCat)
					If oWsCat:Salvar(1,cCodCat,cNomCat,1,,cChvA1,cChvA2)
						cWsRet := oWsCat:oWSSalvarResult:cDescricao
						GravaLog("Categoria GRUPO: "+cCodCat+" - "+cNomCat+". Resultado: "+cWsRet)
						If "SUCESSO" $ UPPER(cWsRet)
							DbSelectArea("SBM")
							DbSetOrder(1)
							If DbSeek(xFilial("SBM")+TRBCAT->GRUPO)
								RecLock("SBM",.F.)
								SBM->BM_YCTRA2 := TRBCAT->GRUPO
								MsUnlock()
							EndIf
						EndIf
					Else
						GravaLog("Erro na funcao Salvar Grupo: "+GetWSCError())
					EndIf
				Else
					GravaLog("N�o h� grupo amarrado para o produto: "+cProduto)
				EndIf
			ElseIf xx = 2
				cCodCat := AllTrim(TRBCAT->CATEGORIA)
				cNomCat := AllTrim(TRBCAT->ZAC_DESCRI)
				If !Empty(cCodCat)
					If oWsCat:Salvar(1,cCodCat,cNomCat,1,TRBCAT->CATGRUPO,cChvA1,cChvA2)
						cWsRet := oWsCat:oWSSalvarResult:cDescricao
						GravaLog("Categoria CATEGORIA: "+cCodCat+" - "+cNomCat+". Resultado: "+cWsRet)
						If "SUCESSO" $ UPPER(cWsRet)
							DbSelectArea("ZAC")
							DbSetOrder(1)
							If DbSeek(xFilial("ZAC")+TRBCAT->CATEGORIA)
								RecLock("ZAC",.F.)
								ZAC->ZAC_YCTRA2 := TRBCAT->CATEGORIA
								MsUnlock()
							EndIf
						EndIf
					Else
						GravaLog("Erro na funcao Salvar Categoria: "+GetWSCError())
					EndIf
				Else
					GravaLog("N�o h� categoria amarrado para o produto: "+cProduto)
				EndIf
			Else
				cCodCat := AllTrim(TRBCAT->SUBGRUPO)
				cNomCat := AllTrim(TRBCAT->ZAG_DESCRI)
				If !Empty(cCodCat)
					If oWsCat:Salvar(1,cCodCat,cNomCat,1,TRBCAT->CATCATEGORIA,cChvA1,cChvA2)
						cWsRet := oWsCat:oWSSalvarResult:cDescricao
						GravaLog("Categoria SUBGRUPO: "+cCodCat+" - "+cNomCat+". Resultado: "+cWsRet)
						If "SUCESSO" $ UPPER(cWsRet)
							DbSelectArea("ZAG")
							DbSetOrder(1)
							If DbSeek(xFilial("ZAG")+TRBCAT->SUBGRUPO)
								RecLock("ZAG",.F.)
								ZAG->ZAG_YCTRA2 := TRBCAT->SUBGRUPO
								MsUnlock()
							EndIf
						EndIf
					Else
						GravaLog("Erro na funcao Salvar Categoria: "+GetWSCError())
					EndIf
				Else
					GravaLog("N�o h� subgrupo amarrado para o produto: "+cProduto)
				EndIf
			EndIf
			TRBCAT->(DbSkip())
		EndDo
	EndIf

	TRBCAT->(DbCloseArea())
Next


Return

//---------------------------//
//-------- Log --------------//
Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cDtHora+": HFATA007 - "+cMsg)
FWRITE(nHandle,CRLF+cDtHora+": HFATA007 - "+cMsg)
//cLog += CRLF+cDtHora+": HFATA007 - "+cMsg

Return

//---------------------------------//
//--- Regras Exporta��o Estoque ---//
Static Function xFiltros(cProduto,cTab)

Local cQuery	:= ""//GetNextAlias()
Local cWhere	:= ""
Local cTabB2B	:= SuperGetMV("MV_XTABB2B",.F.,"140")
Local cPrdStop	:= SuperGetMV("MV_XPRSTOP",.F.,"")
Default cProduto := ""

Do Case

	//--- Regras Exporta��o Produto Pai e amarra��o Produto x Categoria ---//
	Case cTab == "PAI"		      
		cQuery := "SELECT B4_COD,B4_DESC,ISNULL(CONVERT(VARCHAR(2047), B4_YDESCPT),'') AS DESCPT, B4_YTECIDO,B4_YOCASIA,B4_YNSUBCO, " 
		cQuery += CRLF + "ISNULL((select MAX(DA1_PRCVEN) from "+RetSqlName("DA1")+" DA1 (nolock) where DA1.D_E_L_E_T_ = '' and DA1_CODTAB = '"+cTabB2B+"' AND DA1_CODPRO like SUBSTRING(B4_COD,1,8)+'%'),0) AS PRCB2B,"
		
		cQuery += CRLF + "ISNULL((select TOP 1 ISNULL(CONVERT(VARCHAR(2047), ZD_DESCCP2),'') from "+RetSqlName("SZD")+" SZD1 (nolock) "
		cQuery += CRLF + "where ZD_PRODUTO = B4_COD and SZD1.D_E_L_E_T_ = '' and ISNULL(CONVERT(VARCHAR(2047), ZD_DESCCP2),'') <> '' "
		cQuery += CRLF + "group by ZD_PRODUTO,ISNULL(CONVERT(VARCHAR(2047), ZD_DESCCP2),'')),'') AS COMPO, "
		
		cQuery += CRLF + "ISNULL((select TOP 1 ZD_DESCLAV from "+RetSqlName("SZD")+" SZD2 (nolock) where ZD_PRODUTO = B4_COD and SZD2.D_E_L_E_T_ = '' and ZD_DESCLAV <> '' group by ZD_PRODUTO,ZD_DESCLAV),'') AS LAVAG, "
		cQuery += CRLF + "B4_GRUPO AS GRUPO,ISNULL(BM_YCTRA2,'') AS CATGRUPO,B4_YCATEGO AS CATEGORIA,ISNULL(ZAC_YCTRA2,'') AS CATCATEGORIA,B4_YSUBGRP AS SUBGRUPO,ISNULL(ZAG_YCTRA2,'') AS CATSUBGRUPO, "
		cQuery += CRLF + "ZAG_DESCRI AS NOMESUB, BM_XOUTB2B AS GRPOUTLET,ZAC_OUTB2B AS CATOUTLET "
		
		cQuery += CRLF + "FROM "+RetSqlName("SB4")+" SB4 (nolock) "
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("SBM")+" SBM (nolock) "
		cQuery += CRLF + "ON		SBM.BM_GRUPO = SB4.B4_GRUPO "
		cQuery += CRLF + "		AND SBM.BM_FILIAL = '"+xFilial("SBM")+"' "
		cQuery += CRLF + "		AND SBM.D_E_L_E_T_ = '' "
				
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAC")+" ZAC (nolock) "
		cQuery += CRLF + "ON		ZAC.ZAC_CODIGO = SB4.B4_YCATEGO "
		cQuery += CRLF + "		AND ZAC.ZAC_FILIAL = '"+xFilial("ZAC")+"' "
		cQuery += CRLF + "		AND ZAC.D_E_L_E_T_ = '' "
		
		cQuery += CRLF + "LEFT JOIN "+RetSqlName("ZAG")+" ZAG (nolock) "
		cQuery += CRLF + "ON		ZAG.ZAG_CODIGO = SB4.B4_YSUBGRP
		cQuery += CRLF + "		AND ZAG.ZAG_FILIAL = '"+xFilial("ZAG")+"' "
		cQuery += CRLF + "		AND ZAG.D_E_L_E_T_ = '' "
				
		cQuery += CRLF + "WHERE	SB4.B4_FILIAL = '"+xFilial("SB4")+"' "
		cQuery += CRLF + "        AND SB4.D_E_L_E_T_ = '' "
		If !Empty(cProduto)
			cQuery += CRLF + " AND SB4.B4_COD = '"+cProduto+"' "
		EndIf 
		cQuery += CRLF + "        AND SB4.B4_DWEB2B = 'S' "
		cQuery += CRLF + "AND 	SB4.B4_YCOLECA IN   ( SELECT ZAA.ZAA_CODIGO " 
		cQuery += CRLF + "                                      FROM "+RetSqlName("ZAA")+" ZAA (nolock) "
		cQuery += CRLF + "                                      WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
		cQuery += CRLF + "                                      AND ZAA.D_E_L_E_T_ = '' "
		cQuery += CRLF + "                                      AND ZAA.ZAA_DWEB2B = 'S' )"         			              
	    cQuery += CRLF + " order by B4_COD"
	    
	    MemoWrite("HFATA007_PAI.txt",cQuery)
	    
	    cQuery := ChangeQuery(cQuery)

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRBPAI", .F., .T.)                               

	//--- Regras Exporta��o Produto SKU ---//
	Case cTab == "SKU"
		cQuery := "SELECT * FROM ( "
		cQuery += CRLF + "SELECT B1_COD, B1_YFORMAT,B1_YSITUAC,B1_YDATLIB,B1_YCICLO, "
		cQuery += CRLF + "ISNULL((SELECT DA1_PRCVEN FROM "+RetSqlName("DA1")+" DA11 (nolock) WHERE DA11.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA11.D_E_L_E_T_ = '' AND DA11.DA1_CODTAB = '"+cTabB2B+"' AND DA11.DA1_CODPRO = SB1.B1_COD),0) AS PRCB2B, "
		//cQuery += CRLF + "ISNULL((SELECT DA1_PRCVEN FROM "+RetSqlName("DA1")+" DA12 WHERE DA12.DA1_FILIAL = '"+xFilial("DA1")+"' AND DA12.D_E_L_E_T_ = '' AND DA12.DA1_CODTAB = '"+cTabPOR+"' AND DA12.DA1_CODPRO = SB1.B1_COD),0) AS PRCPOR, "
		cQuery += CRLF + "ISNULL((SELECT SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) " 
		cQuery += CRLF + "FROM SB2010 SB2 (nolock) " 
		cQuery += CRLF + "WHERE SB2.B2_FILIAL = '"+xFilial("SB2")+"' " 
      	cQuery += CRLF + "AND SB2.B2_COD = B1_COD "
		cQuery += CRLF + "AND SB2.D_E_L_E_T_ = '' "
		cQuery += CRLF + "AND SB2.B2_LOCAL IN ("+cArmazem+") " 
		cQuery += CRLF + "GROUP BY SB2.B2_COD ),0) AS TOTAL"
		cQuery += CRLF + "FROM "+RetSqlName("SB1")+" SB1 (nolock) "
		cQuery += CRLF + "WHERE 	SB1.B1_FILIAL = '"+xFilial("SB1")+"' "
		cQuery += CRLF + "		AND SB1.D_E_L_E_T_ = '' "
		If !Empty(cProduto)
			If Len(cProduto) <= 8
				cQuery += CRLF + " AND SB1.B1_COD LIKE '"+cProduto+"%' "
			Else
				cQuery += CRLF + " AND SB1.B1_COD = '"+cProduto+"' "
			EndIf
		//Else
			//cWhere := "SB1.B1_MSEXP = '' "
		EndIf
		cQuery += CRLF + "		AND SB1.B1_YDWEB2B = 'S' "
		cQuery += CRLF + "		AND SB1.B1_COD > '"+cPrdStop+"' "
		cQuery += CRLF + "		AND SB1.B1_YCOLECA IN	( SELECT ZAA.ZAA_CODIGO  " 
		cQuery += CRLF + "								  FROM "+RetSqlName("ZAA")+" ZAA (nolock) "
		cQuery += CRLF + "								  WHERE ZAA.ZAA_FILIAL = '"+xFilial("ZAA")+"' "
		cQuery += CRLF + "								  AND ZAA.D_E_L_E_T_ = '' "
		cQuery += CRLF + "								  AND ZAA.ZAA_DWEB2B = 'S' "
		cQuery += CRLF + "								) "
		cQuery += CRLF + "		AND SB1.B1_YSITUAC IN	( SELECT ZA8.ZA8_CODIGO  "
		cQuery += CRLF + "								  FROM "+RetSqlName("ZA8")+" ZA8 (nolock) "
		cQuery += CRLF + "								  WHERE ZA8.ZA8_FILIAL = '"+xFilial("ZA8")+"' "
		cQuery += CRLF + "								  AND ZA8.D_E_L_E_T_ = '' "
		cQuery += CRLF + "								  AND ZA8.ZA8_DWEB2B = 'T' "
		cQuery += CRLF + "								) "
		cQuery += CRLF + "		AND SB1.B1_COD IN 		( SELECT DA1.DA1_CODPRO "
		cQuery += CRLF + "								  FROM "+RetSqlName("DA1")+" DA1 (nolock) "
		cQuery += CRLF + "								  WHERE DA1.DA1_FILIAL = '"+xFilial("DA1")+"' "
		cQuery += CRLF + "								  AND DA1.D_E_L_E_T_ = '' "
		cQuery += CRLF + "								  AND DA1.DA1_CODTAB IN ('"+cTabB2B+"')	) ) Z"
//		cQuery += CRLF + " and SB1.B1_COD LIKE '00I24830%' "
		
		//cQuery += CRLF + " WHERE PRCPOR > 0 AND PRCDE >= PRCPOR "
		cQuery += CRLF + " order by B1_COD"

		MemoWrite("HFATA007_SKU.txt",cQuery)

		cQuery := ChangeQuery(cQuery)

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRBSKU", .F., .T.)
EndCase

Return //cQuery

Static Function ESTOQUE(cProduto)
Local cQuery := ""
Local nRet   := 0

cQuery := "SELECT B2_COD,SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) as TOTAL "
cQuery += CRLF + "FROM "+RetSqlName("SB2")+" SB2 (nolock) "
cQuery += CRLF + "WHERE 	SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
cQuery += CRLF + "      AND SB2.B2_COD = '"+cProduto+"' "
cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "
cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
cQuery += CRLF + "GROUP BY SB2.B2_COD "

MemoWrite("HFATA007_ESTQ.txt",cQuery)

cQuery := ChangeQuery(cQuery)

If Select("EST")
	EST->(DbCloseArea())
EndIf

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"EST", .F., .T.)

DbSelectArea("EST")

If EST->(!EOF())
	nRet := EST->TOTAL
EndIf

EST->(DbCloseArea())

Return nRet

Static Function SLDFILHOS(cPai)
Local lRet := .T.
Local cQuery := ""

cQuery := "SELECT B2_COD,SUM(SB2.B2_QATU)-SUM(SB2.B2_QEMP)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV) as TOTAL_E0E1 "
cQuery += CRLF + "FROM "+RetSqlName("SB2")+" SB2 (nolock) "
cQuery += CRLF + "WHERE 	SB2.B2_FILIAL = '"+xFilial("SB2")+"' "
cQuery += CRLF + "		AND SB2.B2_COD like '"+AllTrim(cPai)+"%' "
cQuery += CRLF + "		AND SB2.D_E_L_E_T_ = '' "
cQuery += CRLF + "		AND SB2.B2_LOCAL IN ("+cArmazem+") "
cQuery += CRLF + "GROUP BY SB2.B2_COD

MemoWrite("HFATA007_SLD.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"SLD", .F., .T.)

DbSelectArea("SLD")

If SLD->(!EOF())
	While SLD->(!EOF())
		If SLD->TOTAL_E0E1 > nMinEst 
			lRet := .F.
			Exit
		EndIf
		SLD->(DbSkip())
	EndDo	
EndIf

SLD->(DbCloseArea())

Return lRet

//---------------------------------//
//--- Regras Exporta��o Estoque ---//
Static Function xDatExp(cTab,cFil)
Local aAreaSB1	:= SB1->(GetArea())
Local aAreaSB2	:= SB2->(GetArea())
Local aAreaSB4	:= SB4->(GetArea())

Do Case
	Case cTab == "PAI"
		DbSelectArea('SB4')
		DbSetOrder(1)
		If DbSeek(xFilial('SB4')+cFil)
			RecLock('SB4',.F.)
			SB4->B4_MSEXP := DTOS(DDATABASE)
			MsUnlock()
		EndIf
	
	Case cTab == "SKU"
		DbSelectArea('SB1')
		DbSetOrder(1)
		If DbSeek(xFilial('SB1')+cFil)
			RecLock('SB1',.F.)
			SB1->B1_MSEXP := DTOS(DDATABASE)
			MsUnlock()
		EndIf
		
	Case cTab == "ESTQ"
		DbSelectArea('SB2')
		DbSetOrder(1)
		If DbSeek(xFilial('SB2')+cFil)
			While xFilial('SB2') == SB2->B2_FILIAL .AND. SB2->B2_COD == cFil  
				RecLock('SB2',.F.)
				SB2->B2_MSEXP := DTOS(DDATABASE)
				MsUnlock()
				SB2->(dbSkip())
			EndDO
		EndIf
EndCase

RestArea(aAreaSB1)
RestArea(aAreaSB2)
RestArea(aAreaSB4)
Return

//----- Via Menu ----//
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

  
PutSx1(cPerg,"01","Produto?","Produto?","Produto?","Mv_ch1","C",15,0,0,"G","","","","N","Mv_par01",""   ,""   ,""   ,"",""   ,""   ,""   ,"","","","","","","","","",{"Atualiza qual Produto? (Vazio atualiza todos)",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)