#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBSRV.CH"

/* ===============================================================================
WSDL Location    http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/estoque.asmx?wsdl
Gerado em        08/03/17 14:19:38
Observa��es      C�digo-Fonte gerado por ADVPL WSDL Client 1.120703
                 Altera��es neste arquivo podem causar funcionamento incorreto
                 e ser�o perdidas caso o c�digo-fonte seja gerado novamente.
=============================================================================== */

User Function _INLHPZM ; Return  // "dummy" function - Internal Use 

/* -------------------------------------------------------------------------------
WSDL Service WSEstoque
------------------------------------------------------------------------------- */

WSCLIENT WSEstoque

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD teste
	WSMETHOD testeSalvar
	WSMETHOD AlterarEstoqueLote
	WSMETHOD Salvar
	WSMETHOD Retornar

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   cA1                       AS string
	WSDATA   cA2                       AS string
	WSDATA   oWS                       AS SCHEMA
	WSDATA   ctesteResult              AS string
	WSDATA   oWSlistProducts           AS Estoque_ArrayOfEstoqueStruct
	WSDATA   ctesteSalvarResult        AS string
	WSDATA   oWSlote                   AS Estoque_EstoqueProdutoLote
	WSDATA   oWSAlterarEstoqueLoteResult AS Estoque_clsRetornoOfEstoqueProdutoLoteResult
	WSDATA   nLojaCodigo               AS int
	WSDATA   cCodIntPrd     AS string
	WSDATA   cPartNumber               AS string
	WSDATA   nQtdEstoque               AS int
	WSDATA   nQtdMinima                AS int
	WSDATA   nTipoAlteracao            AS int
	WSDATA   oWSSalvarResult           AS Estoque_clsRetornoOfclsEstoque
	WSDATA   oWSRetornarResult         AS Estoque_clsRetornoOfclsEstoque

ENDWSCLIENT

WSMETHOD NEW WSCLIENT WSEstoque
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20170624 NG] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT WSEstoque
	::oWS                := NIL 
	::oWSlistProducts    := Estoque_ARRAYOFESTOQUESTRUCT():New()
	::oWSlote            := Estoque_ESTOQUEPRODUTOLOTE():New()
	::oWSAlterarEstoqueLoteResult := Estoque_CLSRETORNOOFESTOQUEPRODUTOLOTERESULT():New()
	::oWSSalvarResult    := Estoque_CLSRETORNOOFCLSESTOQUE():New()
	::oWSRetornarResult  := Estoque_CLSRETORNOOFCLSESTOQUE():New()
Return

WSMETHOD RESET WSCLIENT WSEstoque
	::cA1                := NIL 
	::cA2                := NIL 
	::oWS                := NIL 
	::ctesteResult       := NIL 
	::oWSlistProducts    := NIL 
	::ctesteSalvarResult := NIL 
	::oWSlote            := NIL 
	::oWSAlterarEstoqueLoteResult := NIL 
	::nLojaCodigo        := NIL 
	::cCodIntPrd := NIL 
	::cPartNumber        := NIL 
	::nQtdEstoque        := NIL 
	::nQtdMinima         := NIL 
	::nTipoAlteracao     := NIL 
	::oWSSalvarResult    := NIL 
	::oWSRetornarResult  := NIL 
	::Init()
Return

WSMETHOD CLONE WSCLIENT WSEstoque
Local oClone := WSEstoque():New()
	oClone:_URL          := ::_URL 
	oClone:cA1           := ::cA1
	oClone:cA2           := ::cA2
	oClone:ctesteResult  := ::ctesteResult
	oClone:oWSlistProducts :=  IIF(::oWSlistProducts = NIL , NIL ,::oWSlistProducts:Clone() )
	oClone:ctesteSalvarResult := ::ctesteSalvarResult
	oClone:oWSlote       :=  IIF(::oWSlote = NIL , NIL ,::oWSlote:Clone() )
	oClone:oWSAlterarEstoqueLoteResult :=  IIF(::oWSAlterarEstoqueLoteResult = NIL , NIL ,::oWSAlterarEstoqueLoteResult:Clone() )
	oClone:nLojaCodigo   := ::nLojaCodigo
	oClone:cCodIntPrd := ::cCodIntPrd
	oClone:cPartNumber   := ::cPartNumber
	oClone:nQtdEstoque   := ::nQtdEstoque
	oClone:nQtdMinima    := ::nQtdMinima
	oClone:nTipoAlteracao := ::nTipoAlteracao
	oClone:oWSSalvarResult :=  IIF(::oWSSalvarResult = NIL , NIL ,::oWSSalvarResult:Clone() )
	oClone:oWSRetornarResult :=  IIF(::oWSRetornarResult = NIL , NIL ,::oWSRetornarResult:Clone() )
Return oClone

// WSDL Method teste of Service WSEstoque

WSMETHOD teste WSSEND cA1,cA2,oWS WSRECEIVE ctesteResult WSCLIENT WSEstoque
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<teste xmlns="http://www.ikeda.com.br">'
cSoap += "</teste>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/teste",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/estoque.asmx")

::Init()
::ctesteResult       :=  WSAdvValue( oXmlRet,"_TESTERESPONSE:_TESTERESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method testeSalvar of Service WSEstoque

WSMETHOD testeSalvar WSSEND oWSlistProducts,cA1,cA2,oWS WSRECEIVE ctesteSalvarResult WSCLIENT WSEstoque
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<testeSalvar xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("listProducts", ::oWSlistProducts, oWSlistProducts , "ArrayOfEstoqueStruct", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</testeSalvar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/testeSalvar",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/estoque.asmx")

::Init()
::ctesteSalvarResult :=  WSAdvValue( oXmlRet,"_TESTESALVARRESPONSE:_TESTESALVARRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method AlterarEstoqueLote of Service WSEstoque

WSMETHOD AlterarEstoqueLote WSSEND oWSlote,cA1,cA2,oWS WSRECEIVE oWSAlterarEstoqueLoteResult WSCLIENT WSEstoque
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<AlterarEstoqueLote xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("lote", ::oWSlote, oWSlote , "EstoqueProdutoLote", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</AlterarEstoqueLote>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/AlterarEstoqueLote",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/estoque.asmx")

::Init()
::oWSAlterarEstoqueLoteResult:SoapRecv( WSAdvValue( oXmlRet,"_ALTERARESTOQUELOTERESPONSE:_ALTERARESTOQUELOTERESULT","clsRetornoOfEstoqueProdutoLoteResult",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method Salvar of Service WSEstoque

WSMETHOD Salvar WSSEND nLojaCodigo,cCodIntPrd,cPartNumber,nQtdEstoque,nQtdMinima,nTipoAlteracao,cA1,cA2,oWS WSRECEIVE oWSSalvarResult WSCLIENT WSEstoque
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Salvar xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoProdutoInterno", ::cCodIntPrd, cCodIntPrd , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("PartNumber", ::cPartNumber, cPartNumber , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("QtdEstoque", ::nQtdEstoque, nQtdEstoque , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("QtdMinima", ::nQtdMinima, nQtdMinima , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("TipoAlteracao", ::nTipoAlteracao, nTipoAlteracao , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</Salvar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Salvar",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/estoque.asmx")

::Init()
::oWSSalvarResult:SoapRecv( WSAdvValue( oXmlRet,"_SALVARRESPONSE:_SALVARRESULT","clsRetornoOfclsEstoque",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method Retornar of Service WSEstoque

WSMETHOD Retornar WSSEND nLojaCodigo,cCodIntPrd,cPartNumber,cA1,cA2,oWS WSRECEIVE oWSRetornarResult WSCLIENT WSEstoque
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.,.F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Retornar xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("CodigoProdutoInterno", ::cCodIntPrd, cCodIntPrd , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += WSSoapValue("PartNumber", ::cPartNumber, cPartNumber , "string", .F. , .F., 0 , NIL, .F.,.F.) 
cSoap += "</Retornar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Retornar",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://homologacao.ikeda.com.br/HopeB2B/ikcwebservice/estoque.asmx")

::Init()
::oWSRetornarResult:SoapRecv( WSAdvValue( oXmlRet,"_RETORNARRESPONSE:_RETORNARRESULT","clsRetornoOfclsEstoque",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure ArrayOfEstoqueStruct

WSSTRUCT Estoque_ArrayOfEstoqueStruct
	WSDATA   oWSEstoqueStruct          AS Estoque_EstoqueStruct OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_ArrayOfEstoqueStruct
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_ArrayOfEstoqueStruct
	::oWSEstoqueStruct     := {} // Array Of  Estoque_ESTOQUESTRUCT():New()
Return

WSMETHOD CLONE WSCLIENT Estoque_ArrayOfEstoqueStruct
	Local oClone := Estoque_ArrayOfEstoqueStruct():NEW()
	oClone:oWSEstoqueStruct := NIL
	If ::oWSEstoqueStruct <> NIL 
		oClone:oWSEstoqueStruct := {}
		aEval( ::oWSEstoqueStruct , { |x| aadd( oClone:oWSEstoqueStruct , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPSEND WSCLIENT Estoque_ArrayOfEstoqueStruct
	Local cSoap := ""
	aEval( ::oWSEstoqueStruct , {|x| cSoap := cSoap  +  WSSoapValue("EstoqueStruct", x , x , "EstoqueStruct", .F. , .F., 0 , NIL, .F.,.F.)  } ) 
Return cSoap

// WSDL Data Structure EstoqueProdutoLote

WSSTRUCT Estoque_EstoqueProdutoLote
	WSDATA   oWSProdutos               AS Estoque_ArrayOfEstoqueProduto OPTIONAL
	WSDATA   nLojaCodigo               AS int
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_EstoqueProdutoLote
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_EstoqueProdutoLote
Return

WSMETHOD CLONE WSCLIENT Estoque_EstoqueProdutoLote
	Local oClone := Estoque_EstoqueProdutoLote():NEW()
	oClone:oWSProdutos          := IIF(::oWSProdutos = NIL , NIL , ::oWSProdutos:Clone() )
	oClone:nLojaCodigo          := ::nLojaCodigo
Return oClone

WSMETHOD SOAPSEND WSCLIENT Estoque_EstoqueProdutoLote
	Local cSoap := ""
	cSoap += WSSoapValue("Produtos", ::oWSProdutos, ::oWSProdutos , "ArrayOfEstoqueProduto", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, ::nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure clsRetornoOfEstoqueProdutoLoteResult

WSSTRUCT Estoque_clsRetornoOfEstoqueProdutoLoteResult
	WSDATA   cAcao                     AS string OPTIONAL
	WSDATA   cData                     AS dateTime
	WSDATA   nCodigo                   AS int
	WSDATA   cDescricao                AS string OPTIONAL
	WSDATA   oWSLista                  AS Estoque_ArrayOfEstoqueProdutoLoteResult OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_clsRetornoOfEstoqueProdutoLoteResult
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_clsRetornoOfEstoqueProdutoLoteResult
Return

WSMETHOD CLONE WSCLIENT Estoque_clsRetornoOfEstoqueProdutoLoteResult
	Local oClone := Estoque_clsRetornoOfEstoqueProdutoLoteResult():NEW()
	oClone:cAcao                := ::cAcao
	oClone:cData                := ::cData
	oClone:nCodigo              := ::nCodigo
	oClone:cDescricao           := ::cDescricao
	oClone:oWSLista             := IIF(::oWSLista = NIL , NIL , ::oWSLista:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_clsRetornoOfEstoqueProdutoLoteResult
	Local oNode5
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cAcao              :=  WSAdvValue( oResponse,"_ACAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cData              :=  WSAdvValue( oResponse,"_DATA","dateTime",NIL,"Property cData as s:dateTime on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::nCodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,"Property nCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cDescricao         :=  WSAdvValue( oResponse,"_DESCRICAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode5 :=  WSAdvValue( oResponse,"_LISTA","ArrayOfEstoqueProdutoLoteResult",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode5 != NIL
		::oWSLista := Estoque_ArrayOfEstoqueProdutoLoteResult():New()
		::oWSLista:SoapRecv(oNode5)
	EndIf
Return

// WSDL Data Structure clsRetornoOfclsEstoque

WSSTRUCT Estoque_clsRetornoOfclsEstoque
	WSDATA   cAcao                     AS string OPTIONAL
	WSDATA   cData                     AS dateTime
	WSDATA   nCodigo                   AS int
	WSDATA   cDescricao                AS string OPTIONAL
	WSDATA   oWSLista                  AS Estoque_ArrayOfClsEstoque OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_clsRetornoOfclsEstoque
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_clsRetornoOfclsEstoque
Return

WSMETHOD CLONE WSCLIENT Estoque_clsRetornoOfclsEstoque
	Local oClone := Estoque_clsRetornoOfclsEstoque():NEW()
	oClone:cAcao                := ::cAcao
	oClone:cData                := ::cData
	oClone:nCodigo              := ::nCodigo
	oClone:cDescricao           := ::cDescricao
	oClone:oWSLista             := IIF(::oWSLista = NIL , NIL , ::oWSLista:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_clsRetornoOfclsEstoque
	Local oNode5
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cAcao              :=  WSAdvValue( oResponse,"_ACAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cData              :=  WSAdvValue( oResponse,"_DATA","dateTime",NIL,"Property cData as s:dateTime on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::nCodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,"Property nCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cDescricao         :=  WSAdvValue( oResponse,"_DESCRICAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode5 :=  WSAdvValue( oResponse,"_LISTA","ArrayOfClsEstoque",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode5 != NIL
		::oWSLista := Estoque_ArrayOfClsEstoque():New()
		::oWSLista:SoapRecv(oNode5)
	EndIf
Return

// WSDL Data Structure EstoqueStruct

WSSTRUCT Estoque_EstoqueStruct
	WSDATA   cPartnumber               AS string OPTIONAL
	WSDATA   cQuantidade               AS string OPTIONAL
	WSDATA   cQuantidadeMinima         AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_EstoqueStruct
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_EstoqueStruct
Return

WSMETHOD CLONE WSCLIENT Estoque_EstoqueStruct
	Local oClone := Estoque_EstoqueStruct():NEW()
	oClone:cPartnumber          := ::cPartnumber
	oClone:cQuantidade          := ::cQuantidade
	oClone:cQuantidadeMinima    := ::cQuantidadeMinima
Return oClone

WSMETHOD SOAPSEND WSCLIENT Estoque_EstoqueStruct
	Local cSoap := ""
	cSoap += WSSoapValue("Partnumber", ::cPartnumber, ::cPartnumber , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("Quantidade", ::cQuantidade, ::cQuantidade , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("QuantidadeMinima", ::cQuantidadeMinima, ::cQuantidadeMinima , "string", .F. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure ArrayOfEstoqueProduto

WSSTRUCT Estoque_ArrayOfEstoqueProduto
	WSDATA   oWSEstoqueProduto         AS Estoque_EstoqueProduto OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_ArrayOfEstoqueProduto
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_ArrayOfEstoqueProduto
	::oWSEstoqueProduto    := {} // Array Of  Estoque_ESTOQUEPRODUTO():New()
Return

WSMETHOD CLONE WSCLIENT Estoque_ArrayOfEstoqueProduto
	Local oClone := Estoque_ArrayOfEstoqueProduto():NEW()
	oClone:oWSEstoqueProduto := NIL
	If ::oWSEstoqueProduto <> NIL 
		oClone:oWSEstoqueProduto := {}
		aEval( ::oWSEstoqueProduto , { |x| aadd( oClone:oWSEstoqueProduto , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPSEND WSCLIENT Estoque_ArrayOfEstoqueProduto
	Local cSoap := ""
	aEval( ::oWSEstoqueProduto , {|x| cSoap := cSoap  +  WSSoapValue("EstoqueProduto", x , x , "EstoqueProduto", .F. , .F., 0 , NIL, .F.,.F.)  } ) 
Return cSoap

// WSDL Data Structure ArrayOfEstoqueProdutoLoteResult

WSSTRUCT Estoque_ArrayOfEstoqueProdutoLoteResult
	WSDATA   oWSEstoqueProdutoLoteResult AS Estoque_EstoqueProdutoLoteResult OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_ArrayOfEstoqueProdutoLoteResult
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_ArrayOfEstoqueProdutoLoteResult
	::oWSEstoqueProdutoLoteResult := {} // Array Of  Estoque_ESTOQUEPRODUTOLOTERESULT():New()
Return

WSMETHOD CLONE WSCLIENT Estoque_ArrayOfEstoqueProdutoLoteResult
	Local oClone := Estoque_ArrayOfEstoqueProdutoLoteResult():NEW()
	oClone:oWSEstoqueProdutoLoteResult := NIL
	If ::oWSEstoqueProdutoLoteResult <> NIL 
		oClone:oWSEstoqueProdutoLoteResult := {}
		aEval( ::oWSEstoqueProdutoLoteResult , { |x| aadd( oClone:oWSEstoqueProdutoLoteResult , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_ArrayOfEstoqueProdutoLoteResult
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_ESTOQUEPRODUTOLOTERESULT","EstoqueProdutoLoteResult",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSEstoqueProdutoLoteResult , Estoque_EstoqueProdutoLoteResult():New() )
			::oWSEstoqueProdutoLoteResult[len(::oWSEstoqueProdutoLoteResult)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure ArrayOfClsEstoque

WSSTRUCT Estoque_ArrayOfClsEstoque
	WSDATA   oWSclsEstoque             AS Estoque_clsEstoque OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_ArrayOfClsEstoque
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_ArrayOfClsEstoque
	::oWSclsEstoque        := {} // Array Of  Estoque_CLSESTOQUE():New()
Return

WSMETHOD CLONE WSCLIENT Estoque_ArrayOfClsEstoque
	Local oClone := Estoque_ArrayOfClsEstoque():NEW()
	oClone:oWSclsEstoque := NIL
	If ::oWSclsEstoque <> NIL 
		oClone:oWSclsEstoque := {}
		aEval( ::oWSclsEstoque , { |x| aadd( oClone:oWSclsEstoque , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_ArrayOfClsEstoque
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_CLSESTOQUE","clsEstoque",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSclsEstoque , Estoque_clsEstoque():New() )
			::oWSclsEstoque[len(::oWSclsEstoque)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure EstoqueProduto

WSSTRUCT Estoque_EstoqueProduto
	WSDATA   cCodigoInternoProduto     AS string OPTIONAL
	WSDATA   cPartNumber               AS string OPTIONAL
	WSDATA   nQtdEstoque               AS int
	WSDATA   nQtdEstoqueMin            AS int
	WSDATA   nStatus                   AS int
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_EstoqueProduto
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_EstoqueProduto
Return

WSMETHOD CLONE WSCLIENT Estoque_EstoqueProduto
	Local oClone := Estoque_EstoqueProduto():NEW()
	oClone:cCodigoInternoProduto := ::cCodigoInternoProduto
	oClone:cPartNumber          := ::cPartNumber
	oClone:nQtdEstoque          := ::nQtdEstoque
	oClone:nQtdEstoqueMin       := ::nQtdEstoqueMin
	oClone:nStatus              := ::nStatus
Return oClone

WSMETHOD SOAPSEND WSCLIENT Estoque_EstoqueProduto
	Local cSoap := ""
	cSoap += WSSoapValue("CodigoInternoProduto", ::cCodigoInternoProduto, ::cCodigoInternoProduto , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("PartNumber", ::cPartNumber, ::cPartNumber , "string", .F. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("QtdEstoque", ::nQtdEstoque, ::nQtdEstoque , "int", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("QtdEstoqueMin", ::nQtdEstoqueMin, ::nQtdEstoqueMin , "int", .T. , .F., 0 , NIL, .F.,.F.) 
	cSoap += WSSoapValue("Status", ::nStatus, ::nStatus , "int", .T. , .F., 0 , NIL, .F.,.F.) 
Return cSoap

// WSDL Data Structure EstoqueProdutoLoteResult

WSSTRUCT Estoque_EstoqueProdutoLoteResult
	WSDATA   oWSProdutos               AS Estoque_ArrayOfEstoqueProdutoResult OPTIONAL
	WSDATA   nLojaCodigo               AS int
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_EstoqueProdutoLoteResult
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_EstoqueProdutoLoteResult
Return

WSMETHOD CLONE WSCLIENT Estoque_EstoqueProdutoLoteResult
	Local oClone := Estoque_EstoqueProdutoLoteResult():NEW()
	oClone:oWSProdutos          := IIF(::oWSProdutos = NIL , NIL , ::oWSProdutos:Clone() )
	oClone:nLojaCodigo          := ::nLojaCodigo
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_EstoqueProdutoLoteResult
	Local oNode1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNode1 :=  WSAdvValue( oResponse,"_PRODUTOS","ArrayOfEstoqueProdutoResult",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode1 != NIL
		::oWSProdutos := Estoque_ArrayOfEstoqueProdutoResult():New()
		::oWSProdutos:SoapRecv(oNode1)
	EndIf
	::nLojaCodigo        :=  WSAdvValue( oResponse,"_LOJACODIGO","int",NIL,"Property nLojaCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
Return

// WSDL Data Structure clsEstoque

WSSTRUCT Estoque_clsEstoque
	WSDATA   nLojaCodigo               AS int
	WSDATA   nQtde                     AS int
	WSDATA   nQtdeMinimo               AS int
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_clsEstoque
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_clsEstoque
Return

WSMETHOD CLONE WSCLIENT Estoque_clsEstoque
	Local oClone := Estoque_clsEstoque():NEW()
	oClone:nLojaCodigo          := ::nLojaCodigo
	oClone:nQtde                := ::nQtde
	oClone:nQtdeMinimo          := ::nQtdeMinimo
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_clsEstoque
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::nLojaCodigo        :=  WSAdvValue( oResponse,"_LOJACODIGO","int",NIL,"Property nLojaCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::nQtde              :=  WSAdvValue( oResponse,"_QTDE","int",NIL,"Property nQtde as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::nQtdeMinimo        :=  WSAdvValue( oResponse,"_QTDEMINIMO","int",NIL,"Property nQtdeMinimo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
Return

// WSDL Data Structure ArrayOfEstoqueProdutoResult

WSSTRUCT Estoque_ArrayOfEstoqueProdutoResult
	WSDATA   oWSEstoqueProdutoResult   AS Estoque_EstoqueProdutoResult OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_ArrayOfEstoqueProdutoResult
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_ArrayOfEstoqueProdutoResult
	::oWSEstoqueProdutoResult := {} // Array Of  Estoque_ESTOQUEPRODUTORESULT():New()
Return

WSMETHOD CLONE WSCLIENT Estoque_ArrayOfEstoqueProdutoResult
	Local oClone := Estoque_ArrayOfEstoqueProdutoResult():NEW()
	oClone:oWSEstoqueProdutoResult := NIL
	If ::oWSEstoqueProdutoResult <> NIL 
		oClone:oWSEstoqueProdutoResult := {}
		aEval( ::oWSEstoqueProdutoResult , { |x| aadd( oClone:oWSEstoqueProdutoResult , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_ArrayOfEstoqueProdutoResult
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_ESTOQUEPRODUTORESULT","EstoqueProdutoResult",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSEstoqueProdutoResult , Estoque_EstoqueProdutoResult():New() )
			::oWSEstoqueProdutoResult[len(::oWSEstoqueProdutoResult)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure EstoqueProdutoResult

WSSTRUCT Estoque_EstoqueProdutoResult
	WSDATA   cCodigoInternoProduto     AS string OPTIONAL
	WSDATA   cPartNumber               AS string OPTIONAL
	WSDATA   nCodigoRetorno            AS int
	WSDATA   cMensagem                 AS string OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT Estoque_EstoqueProdutoResult
	::Init()
Return Self

WSMETHOD INIT WSCLIENT Estoque_EstoqueProdutoResult
Return

WSMETHOD CLONE WSCLIENT Estoque_EstoqueProdutoResult
	Local oClone := Estoque_EstoqueProdutoResult():NEW()
	oClone:cCodigoInternoProduto := ::cCodigoInternoProduto
	oClone:cPartNumber          := ::cPartNumber
	oClone:nCodigoRetorno       := ::nCodigoRetorno
	oClone:cMensagem            := ::cMensagem
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT Estoque_EstoqueProdutoResult
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cCodigoInternoProduto :=  WSAdvValue( oResponse,"_CODIGOINTERNOPRODUTO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cPartNumber        :=  WSAdvValue( oResponse,"_PARTNUMBER","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::nCodigoRetorno     :=  WSAdvValue( oResponse,"_CODIGORETORNO","int",NIL,"Property nCodigoRetorno as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cMensagem          :=  WSAdvValue( oResponse,"_MENSAGEM","string",NIL,NIL,NIL,"S",NIL,NIL) 
Return