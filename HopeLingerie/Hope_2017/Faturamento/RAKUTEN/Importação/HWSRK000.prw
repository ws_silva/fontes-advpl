#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HWSRK000  �Autor  �Bruno Parreira      � Data �  20/12/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Identifica ambiente logado para definir qual funcao de      ���
���          �importacao de pedidos da RAKUTEN utilizar.                  ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HWSRK000()
Local cAmbiente

cAmbiente := UPPER(AllTrim(GetEnvServer()))

Do Case
	Case "B2C" $ cAmbiente
		U_HWSRK001()
	Case "B2B" $ cAmbiente .Or. "BRUNO" $ cAmbiente
		U_HWSRK002()
	Otherwise
		MsgAlert("N�o � poss�vel efetuar importa��o de pedidos nesse ambiente.","Aviso")	
End Case

Return