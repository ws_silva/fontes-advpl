#include "topconn.ch"
#INCLUDE "PROTHEUS.CH"

/*�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATR003  �Autor  Weskley Silva      � Data �  04/10/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio espelho do pedido                        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

user function HFATR010()            
Local cPerg     := "HFATR010"
Private cDesc1 	:= "Este relatorio tem o objetivo imprimir o espelho do pedido."
Private cDesc2 	:= " "
Private cDesc3 	:= " "
Private cString	:= "SZJ"
Private Tamanho	:= "P"
Private cbCont,cabec1,cabec2,cbtxt
Private aOrd	:= {}//{"Endere�o"}
Private aReturn := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
Private nomeprog:= "HFATR003",nLastKey := 0
Private li		:= 80, limite:=132, lRodape:=.F.
Private wnrel  	:= "HFATR003"
Private titulo 	:= "Espelho do Pedido"
Private cSerIni := ""
Private cSerFin := ""
Private nLin 	:= 100
Private nCol 	:= 60
Private nPula 	:= 60
Private	nfim    := 2400
Private imprp	:= .F.
Private a_cols 	:= {50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000}

Private	oFont14	 := TFont():New("Arial",,14,,.f.,,,,,.f.)
Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
Private oFont09  := TFont():New("Arial",,09,,.f.,,,,,.f.)
Private oFont09n := TFont():New("Arial",,09,,.t.,,,,,.f.)
Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
Private oFont12  := TFont():New("Arial",,12,,.f.,,,,,.f.)
Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)

Private lMarcar  := .F.
Private cTipos   := ""

Private nPagina  := 0

cbtxt   := SPACE(10)
cbcont  := 0
Li      := 80
m_pag   := 1
cabec2  := ""
cabec1  := ""

AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
	Return
EndIf

cMvPar06 := AllTrim(mv_par06) 

If !Empty(cMvPar06)
	For nx := 1 to len(cMvPar06) step 2
		cAux := SubStr(cMvPar06,nx,2)
		If cAux <> '**'
			cTipos += "'0"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

MontaTela()

Return

Static Function MontaTela()
Local cQuery := ""
Local _astru := {}

cQuery := " SELECT C5_NUM, C5_CLIENT, C5_LOJACLI, A1_NOME, SUM(C6_QTDVEN) AS C6_QTDVEN, C5_EMISSAO "
cQuery += CRLF + "FROM "+RetSqlName("SC5")+" SC5 "
cQuery += CRLF + "INNER JOIN "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "ON A1_COD = C5_CLIENT "
cQuery += CRLF + "AND A1_LOJA = C5_LOJACLI "
cQuery += CRLF + "AND SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "INNER JOIN "+RetSqlName("SC6")+" SC6 "
cQuery += CRLF + "ON C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA "
cQuery += CRLF + "AND SC6.D_E_L_E_T_ = '' "
If !Empty(cTipos)
	cQuery += CRLF + "AND C5_TPPED IN ("+cTipos+") "
EndIf
cQuery += CRLF + "WHERE SC5.D_E_L_E_T_ = '' "
cQuery += CRLF + "AND C5_NUM between '"+mv_par01+"' and '"+mv_par02+"' "
cQuery += CRLF + "AND C5_EMISSAO   between '"+DtoS(mv_par04)+"' and '"+DtoS(mv_par05)+"' "

cQuery += CRLF + "GROUP BY C5_NUM,C5_CLIENT,C5_LOJACLI,A1_NOME,C5_EMISSAO  "
cQuery += CRLF + "ORDER BY C5_NUM "

MemoWrite("HFATR003_MontaTela.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"    ,"C",2,0})
AADD(_astru,{"C5_NUM" ,TAMSX3("C5_NUM" )[3],TAMSX3("C5_NUM" )[1],TAMSX3("C5_NUM" )[2]})
AADD(_astru,{"C5_CLIENT"  ,TAMSX3("C5_CLIENT"  )[3],TAMSX3("C5_CLIENT"  )[1],TAMSX3("C5_CLIENT"  )[2]})
AADD(_astru,{"C5_LOJACLI" ,TAMSX3("C5_LOJACLI" )[3],TAMSX3("C5_LOJACLI" )[1],TAMSX3("C5_LOJACLI" )[2]})
AADD(_astru,{"A1_NOME",TAMSX3("A1_NOME")[3],TAMSX3("A1_NOME")[1],TAMSX3("A1_NOME")[2]})
AADD(_astru,{"C6_QTDVEN"   ,TAMSX3("C6_QTDVEN"   )[3],TAMSX3("C6_QTDVEN"   )[1],TAMSX3("C6_QTDVEN"   )[2]})
AADD(_astru,{"C5_EMISSAO"   ,TAMSX3("C5_EMISSAO"   )[3],TAMSX3("C5_EMISSAO"   )[1],TAMSX3("C5_EMISSAO"   )[2]})

cArqTrab  := CriaTrab(_astru,.T.)
dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )
IndRegua("TRB",cArqTrab,"C5_NUM+C5_CLIENT+C5_LOJACLI")
TRB->(dbClearIndex())
TRB->(dbSetIndex(cArqTrab + OrdBagExt()))

//Atribui a tabela temporaria ao alias TRB
Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		
		DbSelectArea("TRB")
		RecLock("TRB",.T.)
		TRB->C5_NUM      := TMP->C5_NUM
		TRB->C5_CLIENT   := TMP->C5_CLIENT
		TRB->C5_LOJACLI  := TMP->C5_LOJACLI
		TRB->A1_NOME 	 := TMP->A1_NOME
		TRB->C6_QTDVEN  	 := TMP->C6_QTDVEN
		TRB->C5_EMISSAO  := StoD(TMP->C5_EMISSAO)
		MsUnlock()
		
		TMP->(DbSkip())
	EndDo
Else
	MsgAlert("Nenhum registro foi encontrado.","Aten��o")
	lRet := .F.
	LimpaTab()
	Return
EndIf

//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Pedido"     ,"C5_NUM" ,TAMSX3("C5_NUM" )[3],TAMSX3("C5_NUM" )[1],TAMSX3("C5_NUM" )[2],X3Picture("C5_NUM" )},;	
{"Cliente"    ,"C5_CLIENT"  ,TAMSX3("C5_CLIENT"  )[3],TAMSX3("C5_CLIENT"  )[1],TAMSX3("C5_CLIENT"  )[2],X3Picture("C5_CLIENT"  )},;
{"Loja"       ,"C5_LOJACLI" ,TAMSX3("C5_LOJACLI" )[3],TAMSX3("C5_LOJACLI" )[1],TAMSX3("C5_LOJACLI" )[2],X3Picture("C5_LOJACLI" )},;
{"Nome"       ,"A1_NOME",TAMSX3("A1_NOME")[3],TAMSX3("A1_NOME")[1],TAMSX3("A1_NOME")[2],X3Picture("A1_NOME")},;
{"Quantidade" ,"C6_QTDVEN"   ,TAMSX3("C6_QTDVEN"   )[3],TAMSX3("C6_QTDVEN"   )[1],TAMSX3("C6_QTDVEN"   )[2],X3Picture("C6_QTDVEN"   )},;
{"Dt.Emissao" ,"C5_EMISSAO"   ,TAMSX3("C5_EMISSAO"   )[3],TAMSX3("C5_EMISSAO"   )[1],TAMSX3("C5_EMISSAO"   )[2],X3Picture("C5_EMISSAO"   )}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.F.)
oMark:SetDescription('Espelho do Pedido')
oMark:SetFieldMark('MK_OK')
                              
//oMark:AddButton("Gerar Pre-Faturamento","MsgRun('Gerando pr�-faturamento...','DAP',{|| U_HFAT02PROX(oMark:Mark())})",,2,0)
//oMark:AddButton("Imprimir e Liberar","U_HFTR002REL(oMark,1)",,2,0)
oMark:AddButton("Imprimir","U_HFTR003REL(oMark,2)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HFTR03INV(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || oMark:AllMark()})

oMark:SetTemporary()

//Ativando a janela
oMark:Activate()

LimpaTab()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFTR002REL�Autor  �Weskley Silva      � Data � 04/10/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Funcao para controlar a transicao de uma tela para outra.   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

User Function HFTR003REL(oMark,nOpc) 
Local aAreaTRB := GetArea()
Local aPed     := {}
Local cPerg    := "HFATR010"

If !MsgYesNo("Confirma a impress�o?","Confirma��o")
	RestArea(aAreaTRB)
	lContinua := .F.
	Return
EndIf

aPed := GeraArray(oMark)

wnrel:=SetPrint(cString,wnrel,cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

oPrn := TMSPrinter():New()
oPrn:SetPortrait()

Processa({||ImpRel(aPed,oMark)},Titulo,"Imprimindo, aguarde...")

RestArea(aAreaTRB)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GeraArray �Autor  �Weskley Silva      � Data � 04/10/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta array com pedidos para pre-faturamento.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function GeraArray(oMark) 
Local lMark    := .F.
Local aArray   := {}

TRB->(DbGoTop())
While TRB->(!Eof())
    If !Empty(TRB->MK_OK)  	
		lMark := .T.
		aAdd(aArray,{TRB->C5_NUM,TRB->C5_CLIENT})
    Endif
    TRB->(DbSkip())
EndDo

If !lMark
	MsgAlert("Nenhum item foi selecionado.","Aten��o")
EndIf

Return aArray

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �LimpaTab  �Autor  �Weskley Silva      � Data � 04/10/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Limpa os arquivos e alias teporarios que estao abertos.     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LimpaTab()

//Limpar o arquivo tempor�rio
If !Empty(cArqTrab)
    Ferase(cArqTrab+GetDBExtension())
    Ferase(cArqTrab+OrdBagExt())
    cArqTrab := ""
    TRB->(DbCloseArea())
Endif

TMP->(DbCloseArea())

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFTR02INV �Autor  �Weskley Silva      � Data � 04/10/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Marca ou desmarca todos os itens da FWMarkBrowse.           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFTR03INV(cMarca)
Local aAreaTRB  := TRB->(GetArea())

lMarcar := !lMarcar 
 
dbSelectArea("TRB")
TRB->(dbGoTop())
While !TRB->(Eof())
    RecLock("TRB",.F.)
    TRB->MK_OK := IIf(lMarcar,cMarca,'  ')
    MsUnlock()
    TRB->(dbSkip())
EndDo

RestArea( aAreaTRB )

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � ImpRel   � Autor � Weskley Silva        � Data � 04/10/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HFATR003	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpRel(aPedido,oMark)
Local cQuery  := ""
Local cAmzPic := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0"))
Local nx      := 0
Local cPedido := ""

If Len(aPedido) > 0
	For nx := 1 to Len(aPedido)
		cPedido += "'"+aPedido[nx][1]+"',"
	Next
	
	cPedido := SubStr(cPedido,1,Len(cPedido)-1)
EndIf

cQuery := "SELECT C6_PRODUTO,B1_DESC,B1_UM,SUM(C6_QTDVEN) as C6_QTDVEN, "
cQuery += CRLF + "C5_PRAZO,C5_POLCOM,C5_TPPED,C5_CONDPAG,C5_XMICRDE,C5_XPEDRAK,C5_XPEDWEB,C5_CLIENTE,C5_LOJACLI,C5_TRANSP,C5_XINSTRU,C5_XPEDMIL,C5_XPEDVIN,C5_XPRZVIN,C5_FECENT,C5_FRETE,C5_EMISSAO, "
cQuery += CRLF + "C6_PRCVEN,C6_VALOR, C5_DESPESA,C5_NUM, "
cQuery += CRLF + "A1_NOME,A1_END,A1_EST,A1_MUN,A1_BAIRRO,A1_CEP, C6_XITPRES "
cQuery += CRLF + "FROM "+RetSqlName("SC5")+" SC5 "
cQuery += CRLF + "inner join "+RetSqlName("SC6")+" SC6 "
cQuery += CRLF + "on C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA "
cQuery += CRLF + "and SC6.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "on A1_COD = C5_CLIENTE "
cQuery += CRLF + "and A1_LOJA = C5_LOJACLI "
cQuery += CRLF + "and SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 "
cQuery += CRLF + "on B1_COD = C6_PRODUTO "
cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where SC5.D_E_L_E_T_ = '' "
cQuery += CRLF + "and C5_NUM IN ("+cPedido+") "
cQuery += CRLF + "GROUP BY C6_PRODUTO,B1_DESC,B1_UM, "
cQuery += CRLF + "C5_PRAZO,C5_POLCOM,C5_TPPED,C5_CONDPAG,C5_XMICRDE,C5_XPEDRAK,C5_XPEDWEB,C5_CLIENTE,C5_LOJACLI,C5_TRANSP,C5_XINSTRU,C5_XPEDMIL,C5_XPEDVIN,C5_XPRZVIN,C5_FECENT,C5_FRETE,C5_EMISSAO, "
cQuery += CRLF + "C6_PRCVEN,C6_VALOR, C5_DESPESA,C5_NUM, "
cQuery += CRLF + "A1_NOME,A1_END,A1_EST,A1_MUN,A1_BAIRRO,A1_CEP, C6_XITPRES "
cQuery += CRLF + "order by C5_NUM "

memowrite("HFATR003.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"QRY", .F., .T.)

DbSelectArea("QRY")
DbGoTop()
n_cont := 0

If QRY->(!EOF())
	While QRY->(!EOF())
		n_cont++
		QRY->(DbSkip())
	EndDo
	DbGoTop()
	ProcRegua(n_cont)
	NPAG := 1
Else
	MsgAlert("N�o h� dados a serem impressos.","Aten��o")
	Return	
EndIf

nTotQtd := 0
nTotVlr := 0
cNumPF  := ""
aColItens := {080,1500,1700,1900,2150}

lEst := .T.

While QRY->(!Eof())
	
	IncProc()
	
	If cNumPF <> QRY->C5_NUM .Or. Li > 25 //.or. nlin > 30
		If cNumPF <> QRY->C5_NUM
			nPagina := 1
		Else
			If Li > 25
				nPagina++
			EndIf		
		EndIf
		CabecSep(nPagina)
		Li := 0
		lEst := .T.
	EndIf
	
	cProduto := QRY->C6_PRODUTO+ " - " +SubStr(QRY->B1_DESC,1,45)
	nQuant   := QRY->C6_QTDVEN
	nPrcVen  := QRY->C6_PRCVEN
	nPrcTot  := QRY->C6_QTDVEN*QRY->C6_PRCVEN//QRY->C6_VALOR
	
	oPrn:Say(nLin,aColItens[1],cProduto,oFont09,100)
	oPrn:Say(nLin,aColItens[2],Transform(nQuant,"@E 9,999,999")+" "+QRY->B1_UM,oFont09,100)
	oPrn:Say(nLin,aColItens[3],Transform(nPrcVen,"@E 9,999,999.99"),oFont09,100)
	oPrn:Say(nLin,aColItens[4],Transform(nPrcTot,"@E 9,999,999.99"),oFont09,100)
	
	//oPrn:Box(nLin,2360,nLin+40,nfim)
	
	nLin += nPula
	Li++

	If Li > 25 //.or. nlin > 30
		nPagina++	
		CabecSep(nPagina)
		Li := 0
		lEst := .T.
	EndIf
	
	cNumPF  := QRY->C5_NUM
	nTotQtd += QRY->C6_QTDVEN
	nTotVlr += nPrcTot
	
	QRY->(DbSkip())
	
	If cNumPF <> QRY->C5_NUM //.Or. Li > 30
	
		If Li > 25 //.or. nlin > 30
			nPagina++
			CabecSep(nPagina)
			Li := 0
			lEst := .T.
		EndIf
	
		oPrn:Say(nLin,aColItens[3]-500,"Total:",oFont09n,100)
		oPrn:Say(nLin,aColItens[3]-150,Transform(nTotQtd,"@E 9,999,999"),oFont09n,100)
		oPrn:Say(nLin,aColItens[5]-250,Transform(nTotVlr,"@E 9,999,999.99"),oFont09n,100)
		oPrn:Box(nLin-10,50,nLin+50,nfim)
		nTotQtd := 0
		nTotVlr := 0
	EndIf
	
EndDo

MS_FLUSH()

If	( aReturn[5] == 1 ) //1-Disco, 2-Impressora
	oPrn:Preview()
Else
	oPrn:Setup()
	oPrn:Print()
EndIf

QRY->(DbCloseArea())
oMark:DeActivate()

Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CabecSep  � Autor � Weskley Silva        � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Imprime o cabecalho do relatorio                           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HFATR002	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CabecSep(nPag)
Local aArea := GetArea() 
nLin	:= 100
nCol	:= 60
nPula	:= 60
nEsp    := 350
nPosTit := 60

aColCab := {80,850,1600,2050}

aTit    := {"Produto","Qtde","Vrl. Unit","Vlr. Total"}
aColTit := AClone(aColItens)

oPrn:EndPage()

oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
oPrn:Say(nLin,aColCab[4]+150,"P�g.: "+AllTrim(Str(nPag)),oFont09,100)
nLin += nPula

oPrn:Say(nLin,aColCab[4]-50,dtoc(date())+" - "+Time(),oFont10,100)

nLin += nPula*2

oPrn:Say(nLin,aColCab[1],"Pedido Protheus:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_NUM,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Tipo Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,GetAdvFVal("SZ6","Z6_DESC",xFilial("SZ6")+QRY->C5_POLCOM+QRY->C5_PRAZO,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[2],"Ped Vinc./Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,IIF(!Empty(QRY->C5_XPEDVIN),(QRY->C5_XPEDVIN + " / " + AllTrim(Str(QRY->C5_XPRZVIN))),""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cond. Pagamento:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,GetAdvFVal("SE4","E4_DESCRI",xFilial("SE4")+QRY->C5_CONDPAG,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[3],"Tipo Pedido:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,GetAdvFVal("SZ4","Z4_DESC",xFilial("SZ4")+QRY->C5_POLCOM+QRY->C5_TPPED,1,""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cliente:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_CLIENTE+"/"+QRY->C5_LOJACLI+" - "+SubStr(QRY->A1_NOME,1,35),oFont10,100)
oPrn:Say(nLin,aColCab[3],"CEP:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,Transform(QRY->A1_CEP,"@R 99999-999"),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Endere�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->A1_END,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Data Emiss�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,DtoC(StoD(QRY->C5_EMISSAO)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cidade:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->A1_MUN,oFont10,100)
oPrn:Say(nLin,aColCab[2]+100,"UF:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,QRY->A1_EST,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Bairro:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,QRY->A1_BAIRRO,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Macro Regi�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_XMICRDE,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Frete:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(QRY->C5_FRETE,"@E 9,999,999.99"),oFont10,100)
oPrn:Say(nLin,aColCab[2],"Data Entrega:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,DtoC(StoD(QRY->C5_FECENT)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Transportador:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_TRANSP+" - "+GetAdvFVal("SA4","A4_NOME",xFilial("SA4")+QRY->C5_TRANSP,1,""),oFont10,100)
nLin += nPula
MSBAR('CODE128',5.8,15.0,QRY->C5_NUM,oPrn,.F.,,.T.,0.040,1.1,,,,.F.)
nLin += nPula
oPrn:Say(nLin-30,aColCab[4]-150,QRY->C5_NUM,oFont14n,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Observa��o1:",oFont10n,100)
cAux := ""
cObs := AllTrim(QRY->C5_XINSTRU)
If Len(cObs) > 50
	nDe  := 1
	nAte := 50
	While nDe < Len(cObs)
		cAux := SubStr(cObs,nDe,nAte)
		oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10n,100)
		nLin += nPula
		nDe  := nDe+50
		nAte := nAte+50 
	EndDo
Else
	oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10n,100)
	nLin += nPula	
EndIf
oPrn:Say(nLin,aColCab[1],"Observa��o2:",oFont10n,100)
cAux := ""
cObs := AllTrim(GetAdvFVal("SC5","C5_XOBSINT",xFilial("SC5")+QRY->C5_NUM,1,""))
If Len(cObs) > 50
	nDe  := 1
	nAte := 50
	While nDe < Len(cObs)
		cAux := SubStr(cObs,nDe,nAte)
		oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10n,100)
		nLin += nPula
		nDe  := nDe+50
		nAte := nAte+50 
	EndDo
Else
	oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10n,100)
	nLin += nPula	
EndIf		
nLin += nPula

oPrn:Box(nLin-10,50,nLin+50,nfim)

oPrn:Say(nLin,aColTit[1],aTit[1],oFont10n,100)
oPrn:Say(nLin,aColTit[2],aTit[2],oFont10n,100)
oPrn:Say(nLin,aColTit[3],aTit[3],oFont10n,100)
oPrn:Say(nLin,aColTit[4],aTit[4],oFont10n,100)

nLin += nPula
imprp	:= .T.

RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Weskley Silva      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Pedido de            ","","","Mv_ch1","C",6,0,0,"G","",   "","","N","Mv_par01",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o pedido de.",""},{""},{""},"")
PutSx1(cPerg,"02","Pedido ate           ","","","Mv_ch2","C",6,0,0,"G","",   "","","N","Mv_par02",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o pedido ate.",""},{""},{""},"")
PutSx1(cPerg,"03","Tipo dos Pedidos     ","","","Mv_ch6","C",99,0,0,"G","U_HF002TP()","","","N","Mv_par06","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os tipos de pedido.",""},{""},{""},"")
PutSx1(cPerg,"04","Data de              ","","","Mv_ch7","D",8,0,0,"G","",   "","","N","Mv_par07",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione a Data de.",""},{""},{""},"")
PutSx1(cPerg,"05","Data ate             ","","","Mv_ch8","D",8,0,0,"G","",   "","","N","Mv_par08",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione a Data ate.",""},{""},{""},"")


RestArea( aAreaSX1 )
RestArea( aAreaAtu )

return()
