#include "rwmake.ch"
#Include "PROTHEUS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA003  �Autor  �Bruno Parreira      � Data �  23/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Esta rotina atualiza os dados da expedicao da nota fiscal  ���
���          � ex: peso liquido, volume, transportadora..                 ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE - www.actualtrend.com.br                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA003()

	cRotina	:= "HFATA003"
	cOpcao		:= "ALTERAR"
	nOpcE		:= 2
	nOpcG		:= 3
	cCadastro	:= "Saida de Produtos na Expedicao"

	aRotina := {	{ "Pesquisa"		,"AxPesqui"			,0, 1},;
		{ "Visualiza"		,'U_HFT003EXP(.F.)'	,0, 2},;
		{ "Expedi��o NF"	,'U_HFT003EXP(.T.)'	,0, 4}}

	MBrowse(01,01,30,70,"SF2")

Return


/*
//���������������������������������������������������������������������������Ŀ
//�Rotina utilizada para alterar os dados da expedicao da nota fiscal de saida�
//�����������������������������������������������������������������������������
*/

User Function HFT003EXP(lExp)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oGet7
	Local oGet8
	Local oMultiGe1
	Local oMultiGe2
	Local oComboBo1
	Local oComboBo2
	Local oComboBo3
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8
	Local oSay9
	Local oSay10
	Local oSay11
	Local oSay12
	Local oSay13
	Local oSay14

	Local   _aAlias        		:= Alias()
	Private _nFilial         	:= SF2->F2_FILIAL
	Private _nFiscal         	:= SF2->F2_DOC
	Private _nSerie         	:= SF2->F2_SERIE
	Private _nCliente			:= SF2->F2_CLIENTE
	Private _nLoja				:= SF2->F2_LOJA
	Private _cTransportadora 	:= SF2->F2_TRANSP		//TRANSPORTADORA
	Private _cRedespacho		:= SF2->F2_REDESP 		//TRANSPORTADORA REDESPACHO
	Private _cData           	:= SF2->F2_EMISSAO		//F2_XDTEXP
	Private d_dtExp				:= DDATABASE			//SF2->F2_XDTEXP
	Private c_horas				:= SF2->F2_HORA
//	Private _xTamdt          	:= PesqPict("SF2","F2_EMISSAO",8)
	Private nPesoBruto			:= SF2->F2_PBRUTO		//SF2->F2_PBRUTO
	Private nPesoLiq			:= SF2->F2_PLIQUI		//SF2->F2_PLIQUI
	Private cVolume				:= SF2->F2_VOLUME1
	Private cCombo1				:= SF2->F2_ESPECI1
	Private aCombo1				:= {"CAIXA","PALLET","SACO","UNIDADE","CAIXA PAPELAO","BAU METAL"}
	Private cCombo2 			:= SE1->E1_XTPPAG
	Private aCombo2				:= {}
	Private cCombo3				:= SF2->F2_TPFRETE
	Private aCombo3				:= {"CIF","FOB","Por conta terceiros","Sem frete"}	//C=CIF;F=FOB;T=Por conta terceiros;S=Sem frete
	Private mMsgfis				:= FwCutOff(AllTrim(SF2->F2_MENNOTA), .T.)
	Private mXMenNt2			:= FwCutOff(AllTrim(SF2->F2_XMENNT2), .T.)
	Private _cQtdNF				:= QtdNF(_nFilial,_nFiscal,_nSerie)
	
	Static oDlg
	
	/// Altera��o referenta ao combo2 para pegar dados da tabela sx5  - daniel Pereira em 22/01/2018
	DbSelectArea("SX5")
	SX5->(DbSetOrder(1))
	SX5->(Dbseek('    ' + '24'))  //tabela de forma de pagamento
	while SX5->(!eof()) .and. SX5->X5_TABELA == '24'
		aadd(aCombo2,AllTrim(SX5->X5_DESCRI))
		SX5->(DbsKIP())
	Enddo
	If Len(aCombo2) == 0
		aCombo2 := {"BOLETO E-MAIL","BOLETO IMPRESSO","DEPOSITO BANCARIO","BOLETO BANCO","BOLETO E-COMMERCE","A VISTA","CARTAO","MOSTRUARIO"}
	Endif
    
	If lExp==.T. .or. TYPE("cRotina")=="U"
		_qry := " SELECT TOP 1 C5_NUM, C5_XPEDCLI "
		_qry += " FROM "+RetSqlName("SC6")+" SC6 (NOLOCK) INNER JOIN "+RetSqlName("SC5")+" SC5 (NOLOCK) ON C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM "
		_qry += " WHERE SC6.D_E_L_E_T_<>'*' AND C6_FILIAL='"+_nFilial+"' AND C6_NOTA='"+_nFiscal+"' AND C6_SERIE='"+_nSerie+"' "
	 
		If Select("TMPSC6") > 0
			TMPSC6->(DbCloseArea())
		EndIf
	
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC6",.T.,.T.)
		DbSelectArea("TMPSC6")
		DbGoTop()

		cCodCli		:= AllTrim(_nCliente)+"-"+AllTrim(_nLoja)
		cNumPed		:= TMPSC6->C5_NUM
		cNumPCli	:= TMPSC6->C5_XPEDCLI

		DbSelectArea("SC5")
		DbSetOrder(1)
		DbSeek(xfilial("SC5")+TMPSC6->C5_NUM)
	
		If nPesoBruto = 0
			nPesoBruto		:= SC5->C5_PBRUTO		//SF2->F2_PBRUTO
		End
		If nPesoLiq = 0
			nPesoLiq		:= SC5->C5_PESOL		//SF2->F2_PLIQUI
		End
		If cVolume = 0
			cVolume			:= SC5->C5_VOLUME1
		End
		If AllTrim(cCombo1) == ""
			cCombo1			:= AllTrim(SC5->C5_ESPECI1)
		End
	
		If AllTrim(cNumPCli) <> ''
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+cNumPed+" - PED. CLIENTE: "+cNumPCli+" - "+cValToChar(_cQtdNF)+" P�S."
			mmsgfis	:= SubStr(mmsgfis +" - "+ _txtPad + Space(254),1,254)
		Else
			_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
			mmsgfis	:= SubStr(mmsgfis +" - "+ _txtPad + Space(254),1,254)
		End
	End
	
	If cCombo3 == 'C'
		cCombo3	:= "CIF"
	ElseIf cCombo3 == 'F'
		cCombo3	:= "FOB"
	ElseIf cCombo3 == 'T'
		cCombo3	:= "Por conta terceiros"
	ElseIf cCombo3 == 'S'
		cCombo3	:= "Sem frete"
	Else
		cCombo3	:= "Sem frete"
	End

//���������������������������������������������������������������������������Ŀ
//� Criacao da Interface                                                      �
//�����������������������������������������������������������������������������

	DEFINE MSDIALOG oDlg TITLE "Expedi��o de Nota Fiscal e Mensagem para Nota" FROM 000, 000  TO 550, 500 COLORS 0, 16777215 PIXEL

	@ 008, 010 SAY oSay1 PROMPT "Nota Fiscal:   "+_nFiscal SIZE 064, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 008, 140 SAY oSay2 PROMPT "Total de Itens na NF:   "+cValToChar(_cQtdNF)+" pe�as" SIZE 098, 007 OF oDlg COLORS 0, 16777215 PIXEL

	@ 022, 010 SAY oSay3 PROMPT "Data de Sa�da:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 032, 010 MSGET oGet1 VAR d_dtExp SIZE 100, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 022, 140 SAY oSay4 PROMPT "Hora de Sa�da:" SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 032, 140 MSGET oGet2 VAR c_horas SIZE 100, 010 OF oDlg COLORS 0, 16777215 PIXEL

	@ 051, 010 SAY oSay5 PROMPT "Transportadora:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 061, 010 MSGET oGet3 VAR _cTransportadora SIZE 100, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 F3 "SA4" PIXEL
	@ 051, 140 SAY oSay6 PROMPT "Transportadora de re-despacho:" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 061, 140 MSGET oGet4 VAR _cRedespacho SIZE 100, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 F3 "SA4" PIXEL

	@ 080, 010 SAY oSay7 PROMPT "Peso Bruto:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 090, 010 MSGET oGet5 VAR nPesoBruto SIZE 100, 010 OF oDlg PICTURE "999999.9999" COLORS 0, 16777215 PIXEL
	@ 080, 140 SAY oSay8 PROMPT "Peso Liquido:" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 090, 140 MSGET oGet6 VAR nPesoLiq SIZE 100, 010 OF oDlg PICTURE "999999.9999" COLORS 0, 16777215 PIXEL

	@ 109, 010 SAY oSay9 PROMPT "Volume:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 119, 010 MSGET oGet7 VAR cVolume SIZE 100, 010 OF oDlg PICTURE "999999" COLORS 0, 16777215 PIXEL
	@ 109, 140 SAY oSay10 PROMPT "Especie:" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 119, 140 MSCOMBOBOX oComboBo1 VAR cCombo1 ITEMS aCombo1 SIZE 100, 012 OF oDlg COLORS 0, 16777215 PIXEL

	@ 138, 010 SAY oSay11 PROMPT "Forma de Pagamento:" SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 148, 010 MSCOMBOBOX oComboBo2 VAR cCombo2 ITEMS aCombo2 SIZE 100, 012 OF oDlg COLORS 0, 16777215 PIXEL
	@ 138, 140 SAY oSay12 PROMPT "Tipo Frete" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 148, 140 MSCOMBOBOX oComboBo3 VAR cCombo3 ITEMS aCombo3 SIZE 100, 012 OF oDlg COLORS 0, 16777215 PIXEL

	@ 167, 010 SAY oSay13 PROMPT "Mensagem complementar para Nota:" SIZE 100, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 177, 010 MSGET oGet8 VAR mmsgfis SIZE 230, 010 OF oDlg PICTURE "@" COLORS 0, 16777215 PIXEL

	@ 196, 010 SAY oSay14 PROMPT "Mensagem complementar para Nota 2:" SIZE 100, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 206, 010 GET oMultiGe2 VAR mXMenNt2 OF oDlg MULTILINE SIZE 230, 044 COLORS 0, 16777215 HSCROLL PIXEL
	
	If lExp==.T. .or. TYPE("cRotina")=="U"
		@ 256, 160 BUTTON oButton2 PROMPT "Salvar" 		SIZE 037, 012 OF oDlg ACTION SALVAR() PIXEL
		@ 256, 204 BUTTON oButton1 PROMPT "Cancelar" 	SIZE 037, 012 OF oDlg ACTION CLOSE(oDlg) PIXEL
	Else
		@ 256, 204 BUTTON oButton1 PROMPT "Fechar" 		SIZE 037, 012 OF oDlg ACTION CLOSE(oDlg) PIXEL
	EndIf

	ACTIVATE MSDIALOG oDlg CENTERED

Return()

                
/*
//���������������������������������������������������������������������������Ŀ
//�Atualiza dados de expedicao da NF.                                         �
//�����������������������������������������������������������������������������
*/

Static Function SALVAR()

	Local _Alias          := Alias()

	DbSelectarea("SF2")
	DbSetOrder(1)
	DbSeek(xFilial("SF2")+_nFiscal+_nSerie+_nCliente+_nLoja)
		
	If _nSerie+_nFiscal == SF2->(F2_SERIE+F2_DOC)
		RecLock("SF2",.F.)
		SF2->F2_TRANSP 		:= _cTransportadora
		SF2->F2_REDESP		:= _cRedespacho
		SF2->F2_EMISSAO 	:= _cData
		SF2->F2_PBRUTO		:= nPesoBruto
		SF2->F2_PLIQUI		:= nPesoLiq
		SF2->F2_VOLUME1		:= cVolume
		SF2->F2_ESPECI1		:= cCombo1
		SF2->F2_HORA		:= c_horas
		SF2->F2_MENNOTA		:= mmsgfis
		SF2->F2_XMENNT2		:= mXMenNt2
		SF2->F2_TPFRETE		:= SubStr(cCombo3,1,1)
		MSUNLOCK()
	End

	DbSelectArea(_Alias)
	
	DbSelectarea("SC5")
	DbSetOrder(1)
	If DbSeek(xfilial("SC5")+cNumPed)
		If SC5->C5_NUM==cNumPed
			RecLock("SC5",.F.)
			SC5->C5_MENNOTA		:= mmsgfis
			SC5->C5_XMENNT2		:= mXMenNt2
			MSUNLOCK()
		End
	End
	
	DbSelectarea("SE1")
	DbSetOrder(2)
	DbSeek(xfilial("SE1")+_nCliente+_nLoja+_nSerie+_nFiscal)

	While !EOF() .AND. _nSerie+_nFiscal == SE1->E1_PREFIXO+SE1->E1_NUM

		If _nSerie+_nFiscal == SE1->E1_PREFIXO+SE1->E1_NUM
			RecLock("SE1",.F.)
			SE1->E1_XTPPAG	:= cCombo2
			MSUNLOCK()
		End
	
		DbSkip()
	
	End

	DbSelectArea(_Alias)

	CLOSE(oDlg)
	
	U_HPREL029(_nFiscal,_nSerie,_nCliente,_nLoja)

Return()


Static Function QtdNF(nFilial,nFiscal,nSerie)

	_qry := " SELECT SUM(D2_QUANT) AS QTDNF FROM "+RetSQLName("SD2")+" SD2 (NOLOCK) "
	_qry += " WHERE D_E_L_E_T_<>'*' AND D2_FILIAL='"+nFilial+"' AND D2_DOC='"+nFiscal+"' AND D2_SERIE='"+nSerie+"' "
	
	If Select("TMPSFT") > 0
		TMPSFT->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSFT",.T.,.T.)
	DbSelectArea("TMPSFT")
	DbGoTop()
	
	cQtdNF	:= TMPSFT->QTDNF

Return(cQtdNF)