#include "protheus.ch"
#include "rwmake.ch"

#Define CRLF  CHR(13)+CHR(10)

// #########################################################################################
// Projeto: HOPE
// Modulo : Faturamento
// ---------+-------------------+-----------------------------------------------------------
// Data     | Daniel R. Melo    | PEDVINC
// ---------+-------------------+-----------------------------------------------------------
// 01/07/16 | Actual Trend      | Filtro para pedido Vinculado
// ---------+-------------------+-----------------------------------------------------------

//------------------------------------------------------------------------------------------

user function PedVinc()

Local oSay1
Local oFiltro
Local oButton1
Local oButton2
Local oDlgSc5									// Tela
Local nPosLbx		:= 0						// Posicao do List
Local cFil			:= Space(15)
Local lRet			:= .F.						// Retorno da funcao
Private oLbx1  
Private aItems	:= {}						// Array com os itens
Private _nVez		:= 0

CursorWait()

xPesqSc5()

CursorArrow()
	     
If Len(aItems) <= 0
	Alert("N�o existem pedidos para vincular")
   Return(lRet)
Endif	
	
DEFINE MSDIALOG oDlgSc5 FROM  50,003 TO 350,500 TITLE "Busca Pedidos de Venda" PIXEL  

	@ 005,005 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER "  Numero  ","  Dt Emiss�o  ","  Cliente  " SIZE 242,120 OF oDlgSc5 PIXEL NOSCROLL 
		oLbx1:SetArray(aItems)
		oLbx1:bLine:={||{aItems[oLbx1:nAt,1],aItems[oLbx1:nAt,2],aItems[oLbx1:nAt,3]}}
		oLbx1:BlDblClick := {||(lRet:= .T.,VAR_IXB:= Alltrim(aItems[oLbx1:nAt,1]), oDlgSc5:End())}
		oLbx1:Refresh()

	@ 132, 005 SAY oSay1 PROMPT "Filtro" SIZE 055, 007 OF oDlgSc5 COLORS 0, 16777215 PIXEL
	@ 130, 025 MSGET oFiltro VAR CFIL SIZE 100, 010 OF oDlgSc5 VALID xPesqSc5(cFil) COLORS 0, 16777215 PIXEL

	@ 130, 157 BUTTON oButton1 PROMPT "Selecionar" SIZE 040, 012 OF oDlgSc5 ACTION (lRet:= .T.,VAR_IXB:= Alltrim(aItems[oLbx1:nAt,1]),oDlgSc5:End()) PIXEL
	@ 130, 207 BUTTON oButton2 PROMPT "Cancelar"   SIZE 040, 012 OF oDlgSc5 ACTION (lRet:= .F.,oDlgSc5:End()) PIXEL	

ACTIVATE MSDIALOG oDlgSc5 CENTERED

Return(lRet) 

Static Function xPesqSc5(_Filtro)

	aItems := {} 
	
	_qry := "SELECT C5_NUM AS NUMERO, C5_EMISSAO AS DTEMISS, C5_CLIENTE AS CLIENTE " + CRLF
	_qry += "FROM "+RetSqlName("SC5")+" SC5 WHERE D_E_L_E_T_ <> '*' AND " + CRLF
	_qry += "		C5_FILIAL = '"+xFilial("SC5")+"' AND C5_NOTA = '' AND C5_NUM <> '"+M->C5_NUM+"' "  + CRLF
	_qry += "		AND C5_CLIENTE = '"+M->C5_CLIENTE+"' AND C5_LOJACLI = '"+M->C5_LOJACLI+"' " + CRLF

	If alltrim(_Filtro) <> ""
		_qry += "		AND C5_NUM LIKE '%"+alltrim(_Filtro)+"%' " + CRLF
	Endif

	_qry += " ORDER BY 1 "  + CRLF
		
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	
	DbSelectArea("XXX")
	DbGoTop()

	If !EOF()
		While !Eof() 
			Aadd(aItems,{SPACE(2)+XXX->NUMERO, SPACE(2)+XXX->DTEMISS, SPACE(2)+XXX->CLIENTE})
			DbSkip()
		End
	Else
		Aadd(aItems,{SPACE(2)+XXX->NUMERO, SPACE(2)+XXX->DTEMISS, SPACE(2)+XXX->CLIENTE})
	Endif       
	DbSelectArea("XXX")
	DbCloseArea()

If alltrim(_Filtro) <> "" .or. _nVez > 0
	_nVez	+= 1
	oLbx1:aArray := aItems
	oLbx1:Refresh()
Endif

Return