#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL030 
Performance de atendimento por SKU
@author Weskley Silva
@since 07/03/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/'

user function HPREL030()

Local aRet := {}
Local aPerg := {}
Local CMVPAR07
Private cTipos := ""
Private oReport
Private cPergCont	:= 'HPREL030' 
	

//AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif


If !Empty(Mv_Par07)
	For nx := 1 to len(Mv_Par07) step 2
		cAux := SubStr(Mv_Par07,nx,2)
		If cAux <> '**'
		if nx == len(Mv_Par07)
			cTipos += "'0"+cAux+"'"
		else
			cTipos += "'0"+cAux+"',"
		endif	
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)
	

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

return 

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 08 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'PFF', 'PERFORMANCE DE ATENDIMENTO POR SKU', , {|oReport| ReportPrint( oReport ), 'PERFORMANCE DE ATENDIMENTO POR SKU' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PERFORMANCE DE ATENDIMENTO POR SKU', { 'PFF', 'SZR','SZJ','SA1','SA2','SC5','SZ1'})
			
	TRCell():New( oSection1, 'EMISSAO'			    ,'PFF', 		'EMISSAO',							"@!"                        ,15)
	TRCell():New( oSection1, 'PEDIDO'			    ,'PFF', 		'PEDIDO',		  					"@!"				        ,10)
	TRCell():New( oSection1, 'PRODUTO'		 	    ,'PFF', 		'PRODUTO',		          			"@!"						,25)
	TRCell():New( oSection1, 'REFERENCIA' 			,'PFF', 		'REFERENCIA',				       	"@!"						,20)
	TRCell():New( oSection1, 'COR'					,'PFF', 		'COR',	       				        "@!"		                ,35)
	TRCell():New( oSection1, 'TAMANHO' 				,'PFF', 		'TAMANHO',			 				"@!"				        ,20)
	TRCell():New( oSection1, 'COLECAO'  			,'PFF', 		'COLECAO',		    			    "@!"				        ,35)
	TRCell():New( oSection1, 'SUBCOLECAO' 			,'PFF', 	    'SUBCOLECAO',                  		"@!"		    	        ,35)
	TRCell():New( oSection1, 'SITUACAO_PROD'		,'PFF', 		'SITUACAO_PROD',      	   			"@!"						,20)
	TRCell():New( oSection1, 'QT_PEDIDO'	   	    ,'PFF', 		'QT_PEDIDO',      	   			    "@E 999,99"					,10)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'		,'PFF', 		'QTDE_ENTREGUE',			   		"@E 999,99"					,10)
	TRCell():New( oSection1, 'QTDE_PERDA'			,'PFF', 		'QTDE_PERDA',				   		"@E 999,99"					,10)
	TRCell():New( oSection1, 'SALDO'				,'PFF', 		'SALDO',			   				"@E 999,99"					,10)
	TRCell():New( oSection1, 'VALOR_TOTAL'			,'PFF', 		'VALOR_TOTAL',						"@E 999,99"					,10)
	TRCell():New( oSection1, 'CLIENTE'				,'PFF', 		'CLIENTE',		   					"@!"						,30)
	TRCell():New( oSection1, 'TIPO_PEDIDO'			,'PFF', 		'TIPO_PEDIDO',			   			"@!"						,20)
	TRCell():New( oSection1, 'COND_PGTO'			,'PFF', 		'COND_PGTO',		   				"@!"						,20)
	TRCell():New( oSection1, 'POLITICA'		        ,'PFF', 		'POLITICA',							"@!"						,20)
	

	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 08 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("PFF") > 0
		PFF->(dbCloseArea())
	Endif
	
    cQuery := " SELECT R.EMISSAO AS EMISSAO, "
    cQuery += " R.PEDIDO, "
    cQuery += " R.PRODUTO, "
    cQuery += " R.REFERENCIA, "
    cQuery += " R.COR, "
    cQuery += " R.TAMANHO, "
    cQuery += " R.COLECAO, "
    cQuery += " R.SUBCOLECAO, "
    cQuery += " R.SITUACAO_PROD, "
    cQuery += " R.QT_PEDIDO, "
    cQuery += " R.QTDE_ENTREGUE, "
    cQuery += " R.QTDE_PERDA, "
    cQuery += " ((R.QT_PEDIDO - R.QTDE_ENTREGUE)- R.QTDE_PERDA) AS SALDO, "
    cQuery += " R.VALOR_TOTAL, "
    cQuery += " R.CLIENTE, "
    cQuery += " R.TIPO_PEDIDO, "
    cQuery += " R.COND_PGTO, "
    cQuery += " R.POLITICA "
    cQuery += " FROM "
    cQuery += " (SELECT CONVERT(CHAR,CAST(C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQuery += " C5_NUM AS PEDIDO, "
    cQuery += " C6.C6_PRODUTO AS PRODUTO, "
    cQuery += " SUBSTRING(C6.C6_PRODUTO,1,8) AS REFERENCIA, "
    cQuery += " ISNULL(BV_DESCRI,'-') AS COR, "
    cQuery += " CASE "
    cQuery += " WHEN RIGHT(C6.C6_PRODUTO,4) LIKE '000%' THEN REPLACE(RIGHT(C6.C6_PRODUTO,4),'000','') "
    cQuery += " WHEN RIGHT(C6.C6_PRODUTO,4) LIKE '00%' THEN REPLACE(RIGHT(C6.C6_PRODUTO,4),'00','') "
    cQuery += " WHEN RIGHT(C6.C6_PRODUTO,4) LIKE '0%' THEN SUBSTRING(RIGHT(C6.C6_PRODUTO,4),2,4) "
    cQuery += " ELSE REPLACE(RIGHT(C6.C6_PRODUTO,4),'00','') "
    cQuery += " END TAMANHO, "
    cQuery += " ISNULL(ZAA_DESCRI,'-') AS COLECAO, "
    cQuery += " ISNULL(ZAH_DESCRI,'-')AS SUBCOLECAO, "
    cQuery += " B1_YSITUAC AS SITUACAO_PROD, "
    cQuery += " C6.C6_QTDVEN AS QT_PEDIDO, "
	cQuery += " C6.C6_QTDENT AS QTDE_ENTREGUE, "
	cQuery += " SC6.C6_QTDVEN AS QTDE_PERDA, "
    cQuery += " SUM(C6.C6_VALOR) AS VALOR_TOTAL, "
    cQuery += " A1_NOME AS CLIENTE, "
    cQuery += " A1_COD, "
    cQuery += " C5_CONDPAG + ' - ' + E4_DESCRI AS COND_PGTO, "
    cQuery += " C5_POLCOM + ' - ' + Z2_DESC AS POLITICA, "
    cQuery += " Z1_CODIGO + ' - ' + Z1_DESC AS TIPO_PEDIDO "
    cQuery += " FROM SC6010 (NOLOCK) C6 "
    cQuery += " LEFT JOIN SC6010 SC6 (NOLOCK) ON C6.C6_NUM = SC6.C6_NUM "
	cQuery += " AND C6.C6_PRODUTO = SC6.C6_PRODUTO AND C6.C6_CLI = SC6.C6_CLI AND C6.C6_FILIAL = SC6.C6_FILIAL "
	cQuery += "	AND SC6.C6_BLQ = 'R' AND SC6.D_E_L_E_T_ = '' "
    cQuery += " INNER JOIN SC5010 (NOLOCK) C5 ON (C5.C5_NUM = C6.C6_NUM AND C5.C5_FILIAL = C6.C6_FILIAL AND C5.C5_CLIENTE = C6.C6_CLI "
    cQuery += " AND C5.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN SA1010 (NOLOCK) A1 ON (A1.A1_COD = C5.C5_CLIENTE "
    cQuery += " AND A1.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN SZ1010 (NOLOCK) Z1 ON (Z1_CODIGO = C5_TPPED "
    cQuery += " AND Z1.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN SD2010 (NOLOCK) D2 ON (C6.C6_NUM = D2.D2_PEDIDO AND C6.C6_PRODUTO = D2.D2_COD AND C6.C6_CLI = D2.D2_CLIENTE "
	cQuery += " AND C6.C6_TES = D2.D2_TES AND D2.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN SF2010 (NOLOCK) F2 ON (F2.F2_CLIENT = D2.D2_CLIENTE "
    cQuery += " AND F2.F2_LOJA = D2.D2_LOJA AND F2.F2_DOC = D2.D2_DOC "
    cQuery += " AND F2.D_E_L_E_T_ = '') "
    cQuery += " RIGHT JOIN SZ2010 (NOLOCK) Z2 ON (Z2_CODIGO = C5_POLCOM "
    cQuery += " AND Z2.D_E_L_E_T_ = '') "
    cQuery += " RIGHT JOIN SE4010 (NOLOCK) E4 ON (E4.E4_CODIGO = C5_CONDPAG "
    cQuery += " AND E4.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN SB1010 (NOLOCK) B1 ON (B1_COD = C6.C6_PRODUTO "
    cQuery += " AND B1.D_E_L_E_T_ <> '*') "
    cQuery += " LEFT JOIN SBV010 (NOLOCK) BV ON SUBSTRING(B1_COD,9,3) = BV_CHAVE "
    cQuery += " AND BV_TABELA = 'COR' "
    cQuery += " AND BV.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN ZAA010 (NOLOCK) ZAA ON B1_YCOLECA = ZAA_CODIGO "
    cQuery += " AND ZAA.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN ZAH010 (NOLOCK) ON B1_YSUBCOL = ZAH_CODIGO "
    cQuery += " AND ZAH010.D_E_L_E_T_ = '' "
    cQuery += " WHERE C6.D_E_L_E_T_ = '' "
    cQuery += " AND C5_POLCOM NOT IN ('099','018','344','352','353','350') "
   
    If !Empty(cTipos)
    	cQuery += " AND C5_TPPED IN ("+cTipos+") "
	EndIf
  
   if !Empty(mv_par03)
		cQuery += " AND C6.C6_PRODUTO BETWEEN '"+Alltrim(mv_par03)+"' AND '"+Alltrim(mv_par04)+"'   "
	endif
	if !Empty(mv_par05)
		cQuery += " AND A1_COD BETWEEN '"+Alltrim(mv_par05)+"' AND '"+Alltrim(mv_par06)+"'  "
	endif
  
	cQuery += " AND F2.F2_DAUTNFE BETWEEN '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"' "
    cQuery += " GROUP BY C5_NUM, " 
	cQuery += "	C5.C5_EMISSAO, "
    cQuery += " C6.C6_PRODUTO, "
    cQuery += " BV_DESCRI, "
    cQuery += " ZAA_DESCRI, "
    cQuery += " ZAH_DESCRI, "
    cQuery += " B1_YSITUAC, "
    cQuery += " C6.C6_QTDVEN, "
    cQuery += " C6.C6_QTDENT, "
    cQuery += " A1_NOME, "
    cQuery += " C5_CONDPAG + ' - ' + E4_DESCRI, "
    cQuery += " C5_POLCOM + ' - ' + Z2_DESC, "
    cQuery += " Z1_CODIGO + ' - ' + Z1_DESC, "
	cQuery += " C6.C6_NOTA, "
	cQuery += " SC6.C6_QTDVEN, "
    cQuery += " A1_COD) R "
	
	TCQUERY cQuery NEW ALIAS PFF

	While PFF->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("EMISSAO"):SetValue(PFF->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")

		oSection1:Cell("PEDIDO"):SetValue(PFF->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(PFF->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(PFF->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COR"):SetValue(PFF->COR)
		oSection1:Cell("COR"):SetAlign("LEFT")
			
		oSection1:Cell("TAMANHO"):SetValue(PFF->TAMANHO)
		oSection1:Cell("TAMANHO"):SetAlign("LEFT")
				
		oSection1:Cell("COLECAO"):SetValue(PFF->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
				
		oSection1:Cell("SUBCOLECAO"):SetValue(PFF->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("SITUACAO_PROD"):SetValue(PFF->SITUACAO_PROD)
		oSection1:Cell("SITUACAO_PROD"):SetAlign("LEFT")
		
		oSection1:Cell("QT_PEDIDO"):SetValue(PFF->QT_PEDIDO)
		oSection1:Cell("QT_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_ENTREGUE"):SetValue(PFF->QTDE_ENTREGUE)
		oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PERDA"):SetValue(PFF->QTDE_PERDA)
		oSection1:Cell("QTDE_PERDA"):SetAlign("LEFT")
						
		oSection1:Cell("SALDO"):SetValue(PFF->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_TOTAL"):SetValue(PFF->VALOR_TOTAL)
		oSection1:Cell("VALOR_TOTAL"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE"):SetValue(PFF->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(PFF->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("COND_PGTO"):SetValue(PFF->COND_PGTO)
		oSection1:Cell("COND_PGTO"):SetAlign("LEFT")
		
		oSection1:Cell("POLITICA"):SetValue(PFF->POLITICA)
		oSection1:Cell("POLITICA"):SetAlign("LEFT")
		
		
		oSection1:PrintLine()
		
		PFF->(DBSKIP()) 
	enddo
	PFF->(DBCLOSEAREA())
Return( Nil )
