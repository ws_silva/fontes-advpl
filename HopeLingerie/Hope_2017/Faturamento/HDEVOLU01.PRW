#include "protheus.ch"
#include "topconn.ch"
#include "tbiconn.ch"
#include "shell.ch"
#Include "RwMake.ch"
#include "rwmake.ch"
#include "ap5mail.ch"
#include "xmlxfun.ch"       
#include "TOTVS.CH"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �HPFINDEV01 � Autor � Rafael Rosa da Silva � Data �05/08/2009���
�������������������������������������������������������������������������Ĵ��
���Locacao   � CSA              �Contato � 								  ���
�������������������������������������������������������������������������Ĵ��
���Descricao �AXCADASTRO na tabela de Conciliacao Concil				  ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���              �  /  /  �      �                                        ���
���              �  /  /  �      �                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

User Function HDEVOLU01()

Local aLegenda 		:= {{"ZAS_STATUS == '1'","BR_VERMELHO"  },;
						{"ZAS_STATUS == '2'","BR_VERDE" 	},;
						{"ZAS_STATUS == '3'","BR_AMARELO"	},;
						{"ZAS_STATUS == '4'","BR_AZUL"		},;
						{"ZAS_STATUS == '5'","BR_PRETO"  	}}   //Antecipado

Private cCadastro 	:= "Devolu��o Efetuadas "
Private cString 	:= "ZAS"
Private aRotina 	:= {{"Pesquisar"	 ,"AxPesqui"   ,0,1},;
             			{"Atualiza BASE" ,"U_FINDEV1A" ,0,3},;
             			{"Alterar"		 ,"A9199LTERA" ,0,4},;
             			{"Rel. Devolu��o","U_FINDER1A" ,0,4},;
             			{"Legenda"		 ,"U_HPDEVLEG" ,0,5}}


dbSelectArea(cString)
dbSetOrder(1)

mBrowse(6,1,22,75,cString,,,,,,aLegenda)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �910ALEGEND�Autor  �Rafael Rosa da Silva� Data �  08/05/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Rotina que monsta a Legenda de status do Conciliador TEF	  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HPDEVLEG()

BrwLegenda("Conciliacao Concil","Legenda",{{"BR_VERMELHO","N�o Processado"	},;
										{"BR_VERDE"	     ,"Em Processo      "},;
										{"BR_AMARELO"	 ,"Divergente"		},;
										{"BR_AZUL"		 ,"FINALIZADO"      },;
								        {"BR_PRETO"		 ,"OMNICHANNEL"		}})
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PRJ001  �Autor  �ADVPL BIALE - FAB. SOFTWARE� Data �  05/27/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao de Modelo de Dados.                                  ���
���          �Onde � definido a estrutura de dados                        ���
���          �Regra	 de Negocio.                                           ���
�������������������������������������������������������������������������͹��
���Uso       � Aula de MVC                                                ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function ModelDef()
Local oStruct	:=	FWFormStruct(1,"ZAS") //Retorna a Estrutura do Alias passado como Parametro (1=Model,2=View)
Local oModel

//Instancia do Objeto de Modelo de Dados
oModel	:=	MpFormModel():New('HPFINDEV01',/*Pre-Validacao*/,/*Pos-Validacao*/,/*Commit*/,/*Cancel*/)

//Adiciona um modelo de Formulario de Cadastro Similar � Enchoice ou Msmget
oModel:AddFields('ID_FLD_FINDEV01', /*cOwner*/, oStruct, /*bPreValidacao*/, /*bPosValidacao*/, /*bCarga*/ )

//Adiciona Descricao do Modelo de Dados
oModel:SetDescription( 'Modelo de Dados de DEVOLU��O de Cart�o Cr�dito' )

//Adiciona Descricao do Componente do Modelo de Dados
oModel:GetModel( 'ID_FLD_FINDEV01' ):SetDescription( 'Formulario de Dados para DEVOLU��O de Cart�o Cr�dito' )


Return(oModel)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PRJ001  �Autor  �ADVPL BIALE - FAB. SOFTWARE� Data �  05/27/12   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao de Visualizacao.                                     ���
���          �Onde � definido a visualizacao da Regra de Negocio.         ���
�������������������������������������������������������������������������͹��
���Uso       � Aula de MVC                                                ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function ViewDef()
Local oStruct	:=	FWFormStruct(2,"ZAS") 	//Retorna a Estrutura do Alias passado
                                            // como Parametro (1=Model,2=View)
Local oModel	:=	FwLoadModel('FINDEV01')	//Retorna o Objeto do Modelo de Dados
Local oView		:=	FwFormView():New()      //Instancia do Objeto de Visualiza��o

//Define o Modelo sobre qual a Visualizacao sera utilizada
oView:SetModel(oModel)

//Vincula o Objeto visual de Cadastro com o modelo
oView:AddField( 'ID_VIEW_FINDEV01', oStruct, 'ID_FLD_FINDEV01')

//Define o Preenchimento da Janela
oView:CreateHorizontalBox( 'ID_HBOX' , 100 )


// Relaciona o ID da View com o "box" para exibicao
oView:SetOwnerView( 'ID_VIEW_FINDEV01', 'ID_HBOX' )

Return(oView)






User function FINDEV1A()
Local aArea := GetArea()
If MsgYesNo("Confirma a atualiza��o da base? ")
	Processa({|| HPFINDEV1A()})
Endif
RestArea(aArea)
Return

Static Function HPFINDEV1A()
Local aArea  := Getarea()
Local aRegs  := {}
Local cPerg	 := "HPFINDEV1A"
Local cMensa := "" 
Local cPedido:= "" 
Local dData  := ctod(space(08))
Local cQuery := ''
AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
    Return
EndIf
      
cQuery := " SELECT E1_PREFIXO AS PREFIXO , E1_EMISSAO AS DATA ,E1_NUM AS NOTA, 'D1_NFORI' AS NFREF, 'F2_EMISSAO' AS DTANFRE, 'D2_PEDIDO'  AS PEDIDO, 'C5_EMISSAO' AS DTAPDO, 'ZAK_NSUCAR' AS NSUCAR, " 
cQuery += " 'ZAK_AUTCAR' AS AUTCAR,E1_CLIENTE AS CODCLI, E1_LOJA    AS LOJA,  E1_NOMCLI  AS NOMCLI ,'A1_CGC'     AS CPFCLI,'E4_DESCRI ' AS [TPPGTO],'F1_VALBRUT' AS VLNDEV, " 
cQuery += " 'F2_VALBRUT' AS VLNREF, SPACE(10)  AS TP ,SPACE(10)  AS ORIENT,SPACE(10)  AS NFTROCA,SPACE(10)  AS MOTIVO,SPACE(10)  AS CODMIL,'F1_SERIE' AS SERIE "
cQuery += " FROM SE1010 E1 WITH (NOLOCK)  "
cQuery += " WHERE E1_TIPO = 'NCC' AND E1_EMISSAO  BETWEEN '"+DtoS(MV_PAR05)+"' AND '"+DtoS(MV_PAR06)+"' AND E1_CLIENTE  BETWEEN '"+MV_PAR01+"' "  
cQuery += "      AND '"+MV_PAR02+"' AND E1_NUM BETWEEN '"+MV_PAR03+"'  AND '"+MV_PAR04+"' AND E1.D_E_L_E_T_ = '' "


IF Select("XTRB") > 0
   XTRB->(dbCloseArea())
Endif      
TCQUERY cQuery NEW ALIAS xTRB 
XTRB->(DBGOTOP())
While XTRB->(!EOF())  
  DbSelectArea('SA1')
  SA1->(Dbgotop())
  SA1->(DbSetOrder(1)) 
  IF SA1->(DbSeek(xFilial("SA1")+XTRB->CODCLI+xTRB->LOJA,.T.))
  cCodMil := SA1->A1_XCOD //Alltrim(Posicione("SA1",1,xFilial("SA1")+Substr(xTRB->CODCLI,1,6)+xTRB->LOJA,"A1_XCOD"))
  cCgc    := SA1->A1_CGC  //Alltrim(Posicione("SA1",1,xFilial("SA1")+Substr(xTRB->CODCLI,1,6)+xTRB->LOJA,"A1_CGC"))    
  ELSE
  cCodMil := "N/Encontrato"
  cCgc    := ""
  ENDIF
  dbSelectArea("ZAS")
  ZAS->(DbSetOrder(1))
  ZAS->(Dbgotop())
  If ZAS->(DbSeek(xFilial("ZAS")+padr(xTRB->NOTA,9),.t. ))
     ZAS->(Reclock("ZAS",.F.))
     ZAS->ZAS_FILIAL := xFilial("ZAS")       
  ELSE
     ZAS->(Reclock("ZAS",.T.))
  Endif
  
  ZAS->ZAS_DATA   := CTOD(SUBSTR(xTRB->DATA,7,2)+'/'+SUBSTR(xTRB->DATA,5,2)+'/'+SUBSTR(xTRB->DATA,3,2))
  ZAS->ZAS_NOTAFE := xTRB->NOTA
  
    cQry  := "SELECT D1_PEDIDO,D1_NFORI,D1_SERIORI FROM SD1010 D1 WITH (NOLOCK)  WHERE  D1_DOC = '"+xTRB->NOTA+"' AND D1_FORNECE = '"+XTRB->CODCLI+"'  AND D1.D_E_L_E_T_ = '' "
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
     ZAS->ZAS_NTREFE := YTRB->D1_NFORI
     cPedido         := YTRB->D1_PEDIDO
     XD1_NFORI       := YTRB->D1_NFORI
     XD1_SERIE       := YTRB->D1_SERIORI
   
  Else
     cPedido         := ""
     XD1_NFORI       := ""
     XD1_SERIE       := ""
  Endif     
  cQry  := "SELECT D2_PEDIDO FROM SD2010 D2 WITH (NOLOCK)  WHERE  D2_DOC = '"+XD1_NFORI+"' AND D2_SERIE = '"+XD1_SERIE+"' AND D2_CLIENTE =  '"+XTRB->CODCLI+"' AND  D2_LOJA = '"+XTRB->LOJA+"'   AND D2.D_E_L_E_T_ = '' "  
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
     cPedido  := YTRB->D2_PEDIDO
  Endif      
  
  cQry  := "SELECT F2_EMISSAO, F2_SERIE, F2_VALBRUT FROM SF2010 F2 WITH (NOLOCK)  WHERE  F2_DOC = '"+XD1_NFORI+"' AND F2_SERIE = '"+XD1_SERIE+"' AND F2_CLIENTE =  '"+XTRB->CODCLI+"' AND  F2_LOJA = '"+XTRB->LOJA+"'   AND F2.D_E_L_E_T_ = '' "  
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
     ZAS->ZAS_EMISAO := CTOD(SUBSTR(YTRB->F2_EMISSAO,7,2)+'/'+SUBSTR(YTRB->F2_EMISSAO,5,2)+'/'+SUBSTR(YTRB->F2_EMISSAO,3,2)) 
     nVLNFRE := YTRB->F2_VALBRUT
     cSerie  := YTRB->F2_SERIE 
  Else 
     ZAS->ZAS_EMISAO := ctod(space(8))
     nVLNFRE         := 0
     cSerie          := "  " 
  Endif   
  cQry  := "SELECT C5_MENNOTA, C5_EMISSAO FROM SC5010 WHERE C5_NUM = '"+alltrim(cPedido)+"' AND D_E_L_E_T_ = ''"
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
       cMensa := YTRB->C5_MENNOTA
     nPos:= at('/',cMensa)
     cPedido := substr(cMensa,nPos-17,17 ) //  : IKE-25411520002 /
     dData   := CTOD(SUBSTR(YTRB->C5_EMISSAO,7,2)+'/'+SUBSTR(YTRB->C5_EMISSAO,5,2)+'/'+SUBSTR(YTRB->C5_EMISSAO,3,2))
  Endif     
  ZAS->ZAS_CODPED := cPedido //xTRB->PEDIDO
  ZAS->ZAS_DTAPED := dData // CTOD(SUBSTR(xTRB->DTAPDO,7,2)+'/'+SUBSTR(xTRB->DTAPDO,5,2)+'/'+SUBSTR(xTRB->DTAPDO,3,2))
  ZAS->ZAS_CODCLI := xTRB->CODCLI
  ZAS->ZAS_LOJCLI := xTRB->LOJA
  ZAS->ZAS_NOMECL := xTRB->NOMCLI
  ZAS->ZAS_CPF    := cCgc
  dbSelectArea("ZAK")
  cQry  := "SELECT ZAK_TPPAGO, ZAK_NSUCAR, ZAK_AUTCAR  FROM ZAK010 AK WITH (NOLOCK) WHERE  SUBSTRING(ZAK_NUM,1,9) = '"+XD1_NFORI+"' AND ZAK_CODCLI = '"+xTRB->CODCLI+"'  AND AK.D_E_L_E_T_ = '' "
  IF Select("YTRB") > 0        
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
       ZAS->ZAS_TPPAGT := YTRB->ZAK_TPPAGO 
       ZAS->ZAS_NSUCAR := YTRB->ZAK_NSUCAR 
       ZAS->ZAS_AUTCAR := YTRB->ZAK_AUTCAR
  Else
     ZAS->ZAS_TPPAGT := " "
     ZAS->ZAS_NSUCAR := " " 
     ZAS->ZAS_AUTCAR := " "
  Endif     
  cQry  := "SELECT F1_VALBRUT FROM SF1010 F1 WITH (NOLOCK)  WHERE  F1_DOC = '"+xTRB->NOTA+"' AND F1_FORNECE = '"+XTRB->CODCLI+"'  AND F1.D_E_L_E_T_ = '' "
  IF Select("YTRB") > 0
     YTRB->(dbCloseArea())
  Endif      
  TCQUERY cQry NEW ALIAS YTRB 
  YTRB->(DBGOTOP())
  If YTRB->(!EOF())
     ZAS->ZAS_VLNFDE := YTRB->F1_VALBRUT
  Else
     ZAS->ZAS_VLNFDE :=  0
  Endif    
  ZAS->ZAS_VLNFRE :=  nVLNFRE   // F2_VALBRUT   xTRB->VLNREF
  ZAS->ZAS_TP     := xTRB->TP
  ZAS->ZAS_ORIENT := xTRB->ORIENT
  ZAS->ZAS_NFVTRO := xTRB->NFTROCA
  ZAS->ZAS_MOTIVO := xTRB->MOTIVO
  ZAS->ZAS_SERIEN := cSerie   //SF2->F2_SERIE
  ZAS->ZAS_STATUS := '1'
  ZAS->ZAS_CLIMIL := cCodMil
  ZAS->(MsUnlock())
  dbselectarea("XTRB")
  XTRB->(dbSKIP())
ENDDO
RestArea(Aarea)
Return

/*/
���������������������������������������������������������������������������������
�����������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 ���
�����������������������������������������������������������������������������Ĵ��
���Descri��o � Verifica/cria SX1 a partir de matriz parverificacao          ���
�����������������������������������������������������������������������������Ĵ��
���Uso       � Especifico para Clientes Microsiga                             ���
������������������������������������������������������������������������������ٱ�
���������������������������������������������������������������������������������
����������������������������������������������������������������������������������
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

//	Aadd(aPergs,{"Ate Cliente","","","mv_ch8","C",6,0,0,"G","","MV_PAR08","","","","ZZZZZZ","","","","","","","","","","","","","","","","","","","","","SE1","","","",""})
	Aadd(aPergs,{"Cliente de ?  ","","","mv_ch1","C",6,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Cliente At�?  ","","","mv_ch2","C",6,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"T�tulo Inicial","","","mv_ch3","C",9,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"T�tulo Final  ","","","mv_ch4","C",9,0,0,"G","","mv_par04","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data de?      ","","","mv_ch5","D",8,0,0,"G","","mv_par05","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data ate?     ","","","mv_ch6","D",8,0,0,"G","","mv_par06","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next