#Include "PROTHEUS.CH"
#include "TbiConn.ch"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MT410TOK  �Autor  �Bruno Parreira      � Data �  23/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Valida pedido de venda.                                     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

User Function MT410TOK()
Local lRet      := .T.  // Conteudo de retorno
Local nOpc      := PARAMIXB[1]		// Opcao de manutencao
Local aRecTiAdt := PARAMIXB[2]		// Array com registros de adiantamento
Local nx        := 0
Local _aAreaOLD := GetArea()                            
Local a_SC9		:= SC9->(GetArea())
Local a_SC6		:= SC6->(GetArea())
Local a_SC5		:= SC5->(GetArea()) 
Local nPosAmz   := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_LOCAL'}) 
Local cAmzReFat := AllTrim(SuperGetMV("MV_XAMZREF",.F.,"XX")) //Armazem padrao Re-Faturamento
Local _xAltPed  := ""
Local _xobs	  	:= "N" 
Local nPosPrd   := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRODUTO'})
Local nPosItem  := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_ITEM'})
Local nPosQtd   := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_QTDVEN'})

lVer := .F.

lConf:= .F.
If nOpc == 4  .and. SC5->C5_TIPO = "N"
	DbSelectArea("SZJ")
	DbSetOrder(2) 
	If DbSeek(xFilial("SZJ")+SC5->C5_NUM)
		While SZJ->(!EOF()) .And. SZJ->ZJ_PEDIDO = SC5->C5_NUM
			If Empty(SZJ->ZJ_DOC)
				IF SZJ->ZJ_CONF = "S"
					lConf := .T.
				Endif
				lVer := .T.
			EndIf	
			SZJ->(DbSkip())
		EndDo
		If lConf
			MsgAlert("Pedido j� efevitado! N�o poder� ser alterado!","A T E N � � O")
			lRet := .F.
		Else
			If lVer
				DbSelectArea("SZJ")
				DbSetOrder(2)
				DbGoTop() 
				DbSeek(xFilial("SZJ")+SC5->C5_NUM)
					
				If M->C5_CLIENTE <> SZJ->ZJ_CLIENTE
					MsgAlert("Cliente foi alterado! Pedido n�o poder� ser saldo!","A T E N � � O")
					lRet := .F.
				Else
					For nX:= 1 to len(acols)
						cGrade	 := acols[nX][35]
						If cGrade == 'S'
	//						If acols[nX][len(acols[nX])] = .F.
							For _y := 1 to len(oGrade:aColsGrade[nX])
								cProduto  := alltrim(acols[nX][2])
								cCor      := SubStr(OGRADE:ACOLSGRADE[nX][_y][1],2,3)
								aTamanho  := u_bTam1(cProduto,cCor)
								For _z := 2 to len(oGrade:aColsGrade[nX][_y])
									If oGrade:aColsGrade[nX][_y][_z][1] > 0
										DbSelectArea("SZJ")
										DbSetOrder(2)
										DbSeek(xfilial("SZJ")+SC5->C5_NUM+aCols[nX,nPosItem]+cProduto+cCor+aTamanho[_z-1])
	
										If acols[nX][len(aHeader)+1] = .F.
											If Found()
												If SZJ->ZJ_QTDLIB > oGrade:aColsGrade[nX][_y][_z-1][1] 
										   			MsgAlert("O Produto "+cProduto+cCor+aTamanho[_z-1]+" sofreu altera��o. O Pedido n�o poder� ser salvo!","Aten��o") 
										   			lRet := .F.
										   		Endif
											Endif
										Else
											If Found()
									   			MsgAlert("O Produto "+cProduto+cCor+aTamanho[_z-1]+" sofreu altera��o. O Pedido n�o poder� ser salvo!","Aten��o") 
									   			lRet := .F.
											Endif
										Endif
									Endif
								Next _z
							Next _y
	//					End
						Else
							DbSelectArea("SZJ")
							DbSetOrder(2)
							DbSeek(xfilial("SZJ")+SC5->C5_NUM+aCols[nX,nPosItem]+aCols[nX,nPosPrd])
							
							If acols[nX][len(aHeader)+1] = .F.
								If Found() 
									If SZJ->ZJ_QTDLIB > aCols[nX,nPosQtd]
								   		MsgAlert("O Produto "+aCols[nX,nPosPrd]+" sofreu altera��o. O Pedido n�o poder� ser salvo!","Aten��o") 
								   		lRet := .F.
								   	Endif
							   	Endif
						   	Else
						   		If Found()
							   		MsgAlert("O Produto "+aCols[nX,nPosPrd]+" sofreu altera��o. O Pedido n�o poder� ser salvo!","Aten��o") 
							   		lRet := .F.
						   		Endif
							Endif
						EndIf
					Next nx
				Endif
			Endif
		EndIf				
	EndIf
EndIf


If nOpc == 3 .Or. nOpc == 4  //2-Visualizacao, 3-Inclusao, 4-Alteracao
	If M->C5_TIPO = "N"
		If M->C5_XREFAT = 'S'
			For nx := 1 To Len(aCols)
			   	If aCols[nX][nPosAmz] <> cAmzReFat
			   		MsgAlert("Pedido de Re-Faturamento devem ter o armaz�m "+cAmzReFat,"Aten��o") 
			   		lRet := .F.
				EndIf
			Next
		EndIf
	EndIf
EndIf

If nOpc == 4

	//Grava informa��es do pedido anterior a altera��o 
	DbSelectArea("SC5")
	DbSetOrder(1)
	DbSeek(xfilial("SC5")+M->C5_NUM)
	
	If AllTrim(SC5->C5_XOBSINT) <> AllTrim(M->C5_XOBSINT)
		_xobs	  := "S"
	End
	 
	_xAltPed  := SC5->C5_CLIENTE+'/'+SC5->C5_POLCOM+'/'+SC5->C5_CONDPAG+'/'+SC5->C5_TABELA+'/'+SC5->C5_TPPED+'/'+_xobs
	
	//RecLock("SC5",.F.)
	//Replace C5_XALTPED	with _xAltPed
	//MsUnLock()
	
	_qry1 := "UPDATE SC5010 SET C5_XALTPED='"+_xAltPed+"' WHERE D_E_L_E_T_ <> '*' AND C5_FILIAL ='"+xfilial("SC5")+"' AND C5_NUM ='"+M->C5_NUM+"' "
	TcSqlExec(_qry1)
	
EndIf

//DbCloseArea()

RestArea(a_SC9)
RestArea(a_SC6)
RestArea(a_SC5)
RestArea(_aAreaOLD)

Return(lRet)

user function buscaCond()

Local oDlgSu9														// Tela
Local nPosLbx  := 0                                                 // Posicao do List
Local nPos     := 0                                                 // Posicao no array
Local cAssunto := ""                                                // Descricao do Assunto
Local lRet     := .F.                                               // Retorno da funcao
Local cFil	   := Space(15)
Local oFiltro
Private oLbx1  
Private aItems   := {}                                                // Array com os itens

CursorWait()

u_XCond()

CursorArrow()
	     
If Len(aItems) <= 0
	Alert("Condi��o n�o encontrada")
   Return(lRet)
Endif	
	
DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Condi��o de Pagamento" PIXEL  

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
			"Descri��o","C�digo";	//"Descricao"
			SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo" 
	oLbx1:SetArray(aItems)
    oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
					 aItems[oLbx1:nAt,2]}}

    oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
    //@ 88, 005 MSGET oTipo VAR CFIL SIZE 035, 010 OF oDlgSu9 VALID u_XCond() COLORS 0, 16777215 PIXEL
	@ 88, 005 SAY oSay43 PROMPT "Filtro" SIZE 055, 007 OF oDlgSu9 COLORS 0, 16777215 PIXEL
    @ 88, 025 MSGET oFiltro VAR CFIL SIZE 100, 010 OF oDlgSu9 VALID u_XCond(cFil) COLORS 0, 16777215 PIXEL
    DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
    DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

ACTIVATE MSDIALOG oDlgSu9 CENTERED

If lRet
   DbSelectarea("SE4")
   DbSetorder(1)
   DbSeek(xFilial("SE4")+aItems[nPos][2])
Endif

Return(lRet) 

User Function XCond(_Filtro)

	aItems := {}
	If __cUserId $ GetMV("MV_XUSRCOM")
		_qry := "Select E4_CODIGO as CODIGO, E4_DESCRI as DESCRICAO from "+RetSqlName("SE4")+" SE4 "
		_qry += "where SE4.D_E_L_E_T_ = '' "
		If alltrim(_filtro) <> ""
			_qry += "and (E4_CODIGO like '%"+alltrim(_filtro)+"%' or E4_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif
		_qry += "Order By E4_CODIGO " 
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Else
		_qry := "Select E4_CODIGO as CODIGO, E4_DESCRI as DESCRICAO from "+RetSqlName("SE4")+" SE4 "
		_qry += "inner join "+RetSqlName("SZ3")+" SZ3 on SZ3.D_E_L_E_T_ = '' and Z3_COND = E4_CODIGO "
		_qry += "where SE4.D_E_L_E_T_ = '' and Z3_POLITIC = '"+M->C5_POLCOM+"' "
		If alltrim(_filtro) <> ""
			_qry += "and (E4_CODIGO like '%"+alltrim(_filtro)+"%' or E4_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif
		_qry += "Order By E4_CODIGO " 
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Endif
	
	DbSelectArea("XXX")
	DbGoTop()
	
	If !EOF()
		While !Eof() 
			Aadd(aItems,{XXX->DESCRICAO,XXX->CODIGO})
			DbSkip()
		End
	Else
		Aadd(aItems,{"   ","   "})
	Endif       
	DbSelectArea("XXX")
	DbCloseArea()

If alltrim(_filtro) <> ""
	oLbx1:aArray := aItems
	oLbx1:Refresh()
Endif

Return

user function buscaTab()

Local oDlgSu9														// Tela
Local nPosLbx  := 0                                                 // Posicao do List
Local nPos     := 0                                                 // Posicao no array
Local cAssunto := ""                                                // Descricao do Assunto
Local cFil	   := Space(15)
Local lRet     := .F.                                               // Retorno da funcao
Private oLbx1  
Private aItems   := {}                                                // Array com os itens

CursorWait()

u_xtab()

CursorArrow()
	     
If Len(aItems) <= 0
	Alert("Tabela n�o encontrada")
   Return(lRet)
Endif	
	
DEFINE MSDIALOG oDlgSu9 FROM  50,003 TO 260,500 TITLE "Tabela de Pre�os" PIXEL  

	@ 03,10 LISTBOX oLbx1 VAR nPosLbx FIELDS HEADER ;
			"Descri��o","C�digo";	//"Descricao"
			SIZE 233,80 OF oDlgSu9 PIXEL NOSCROLL //"Descricao","Ocorrencia","Prazo" 
	oLbx1:SetArray(aItems)
    oLbx1:bLine:={||{aItems[oLbx1:nAt,1],;
					 aItems[oLbx1:nAt,2]}}

    oLbx1:BlDblClick := {||(lRet:= .T.,nPos:= oLbx1:nAt, oDlgSu9:End())}
	oLbx1:Refresh()

	_xxcod := ""
//    DEFINE SBUTTON FROM 88,140 TYPE 15 ENABLE OF oDlgSu9 ACTION (u_cadsB1("V",aItems[oLbx1:nAt][2]))
	@ 88, 005 SAY oSay43 PROMPT "Filtro" SIZE 055, 007 OF oDlgSu9 COLORS 0, 16777215 PIXEL
    @ 88, 025 MSGET oFiltro VAR CFIL SIZE 100, 010 OF oDlgSu9 VALID u_XTab(cFil) COLORS 0, 16777215 PIXEL

    DEFINE SBUTTON FROM 88,175 TYPE 1 ENABLE OF oDlgSu9 ACTION (lRet:= .T.,nPos := oLbx1:nAt,oDlgSu9:End())
    DEFINE SBUTTON FROM 88,210 TYPE 2 ENABLE OF oDlgSu9 ACTION (lRet:= .F.,oDlgSu9:End())

ACTIVATE MSDIALOG oDlgSu9 CENTERED

If lRet
   DbSelectarea("DA0")
   DbSetorder(1)
   DbSeek(xFilial("DA0")+aItems[nPos][2])
Endif

Return(lRet) 

User Function xTab(_Filtro)

	If alltrim(M->C5_POLCOM)<>""
		cPolitica	:= M->C5_POLCOM
	Else
		cPolitica	:= SC5->C5_POLCOM
	End
	
	aItems := {} 
	If __cUserId $ GetMV("MV_XUSRCOM")
		_qry := "Select DA0_CODTAB as CODIGO, DA0_DESCRI as DESCRICAO from "+RetSqlName("DA0")+" DA0 "
		_qry += "where DA0.D_E_L_E_T_ = '' "
		If alltrim(_filtro) <> ""
			_qry += "and (DA0_CODTAB like '%"+alltrim(_filtro)+"%' or DA0_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif

		_qry += "Order By DA0_CODTAB " 
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Else
		_qry := "Select DA0_CODTAB as CODIGO, DA0_DESCRI as DESCRICAO from "+RetSqlName("DA0")+" DA0 " 
		_qry += "inner join "+RetSqlName("SZ5")+" SZ5 on SZ5.D_E_L_E_T_ = '' and Z5_TABELA = DA0_CODTAB "
		_qry += "where DA0.D_E_L_E_T_ = '' and Z5_POLITIC = '"+cPolitica+"' " 
		If alltrim(_filtro) <> ""
			_qry += "and (DA0_CODTAB like '%"+alltrim(_filtro)+"%' or DA0_DESCRI like '%"+alltrim(_filtro)+"%') "
		Endif

		_qry += "Order By DA0_CODTAB "
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"XXX",.F.,.T.)
	Endif
	
	DbSelectArea("XXX")
	DbGoTop()

	If !EOF()
		While !Eof() 
			Aadd(aItems,{XXX->DESCRICAO,XXX->CODIGO})
			DbSkip()
		End
	Else
		Aadd(aItems,{XXX->DESCRICAO,XXX->CODIGO})
	Endif       
	DbSelectArea("XXX")
	DbCloseArea()

If alltrim(_filtro) <> ""
	oLbx1:aArray := aItems
	oLbx1:Refresh()
Endif

Return


User Function bTam1(cProduto,cCor) //Fun��o Busca Tamanho

	Local nX, _i
	Local aTamanhos := {}
	Local aAlterFields := {}

	If Select("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf

	cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM "+RetSQLName("SBV")+" SBV "
	cQuery  += "WHERE D_E_L_E_T_ = '' "
	cQuery  += "	AND BV_TABELA = (SELECT B4_COLUNA FROM "+RetSQLName("SB4")+" SB4 WHERE D_E_L_E_T_<>'*' AND B4_COD='"+cProduto+"') "
	cQuery  += "ORDER BY R_E_C_N_O_"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
	  
	DbSelectArea("TMPSBV")

	cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" SZD WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_COR ='"+cCor+"' AND ZD_PRODUTO='"+cProduto+"'"

	If Select("TMPSZD") > 0
		TMPSZD->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)

	DbSelectArea("TMPSZD")

	While !EOF()
	
		For _i := 1  to 200
		
			DbSelectArea("SX3")
			_campo		:= "ZH_TAM"+StrZero(_i,3)
			cZdVerif	:= 0
	
			TMPSZD->(DbGoTop())
	
			While TMPSZD->(!EOF())
//				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					cZdVerif += 1
					
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo)) .and. cZdVerif==1
						Aadd(aTamanhos, {alltrim(TMPSBV->BV_CHAVE),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","",""}[1])
					Endif
//				Endif
		 		
				TMPSZD->(DbSkip())
				
			EndDo
	  	
			DbSelectArea("TMPSBV")
			
			DbSkip()
			
		Next _i
		
	EndDo

	DbClosearea()
	
Return (aTamanhos)