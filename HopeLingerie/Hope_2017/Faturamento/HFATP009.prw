#include "xmlxfun.ch"

#include "fileio.ch"

     

User Function HFATP009()



	Local aPergs    := {}

	Local lConv      := .F.

	        

	Private cArq       := ""

	Private cArqMacro  := "XLS2DBF.XLA"

	Private cTemp      := GetTempPath() //pega caminho do temp do client

	Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema

	Private aResps     := {}

	Private aArquivos  := {}

 

	aAdd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.F.,""})

	aAdd(aPergs,{1,"Tabela: ",Space(TamSX3("DA0_CODTAB")[1]),"",'.T.',"DA0","",0,.F.})

	aAdd(aPergs,{1,"Descricao: ",Space(TamSX3("DA0_DESCRI")[1]),"",'.T.',"","",0,.F.})

	aAdd(aPergs,{1,"Data Inicial:",Ctod(Space(8)),"","","","",50,.F.}) // Tipo data

	aAdd(aPergs,{1,"Data Final:",Ctod(Space(8)),"","","","",50,.F.}) // Tipo data

		

	If ParamBox(aPergs,"Parametros", @aResps)



		aAdd(aArquivos, AllTrim(aResps[1]))

		cArq := AllTrim(aResps[1])



		DbSelectArea("DA0")

		DbSetOrder(1)

		DbSeek(xfilial("DA0")+alltrim(aResps[2]))

 		  

		If AllTrim(DA0->DA0_TABVIR) == ""



//			MsAguarde( {|| ConOut("Comecou conversao do arquivo "+cArq+ " - "+Time()),;

//            			   lConv := convArqs(aArquivos) }, "Convertendo arquivos", "Convertendo arquivos" )

//			If lConv

			Processa({|| ProcMovs()})

//        	EndIf 

		Else

			Alert("A tabela selecionada e uma Tabela Virtual e nao podera ser atualizada por esta rotina!")

		EndIf



	Endif



Return

                         

Static Function ProcMovs()



	Local cError		:= ""

	Local cWarning	:= ""

	Local nFile		:= 0

	Local aLinhas		:= {}

	Local cFile		:= AllTrim(aResps[1])

	Local nI			:= 0

	Local cLinha  	:= ""

	Local nLintit		:= 1

	Local nLin 		:= 0

	Local nTotLin 	:= 0

	Local aDados  	:= {}

	Local nHandle 	:= 0

	Local cEOL	    	:= +Chr(13)+Chr(10)

	Local cTabVirt	:=""



	nFile := fOpen(cFile, FO_READ + FO_DENYWRITE)

   

	If nFile == -1

		If !Empty(cFile)

			MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")

		EndIf

		Return

	EndIf



	nHandle := Ft_Fuse(cFile)

	Ft_FGoTop()

	nLinTot := FT_FLastRec()-1

	ProcRegua(nLinTot)



	While nLinTit > 0 .AND. !Ft_FEof()

		Ft_FSkip()

		nLinTit--

	EndDo



	Do While !Ft_FEof()

		IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))

		nLin++

		cLinha := Ft_FReadLn()

		If Empty(AllTrim(StrTran(cLinha,',','')))

			Ft_FSkip()

			Loop

		EndIf

		cLinha := StrTran(cLinha,'"',"'")

		cLinha := '{"'+cLinha+','+alltrim(str(nLin+1))+'"}'

		cLinha := StrTran(cLinha,';',',')

		cLinha := StrTran(cLinha,',','","')

		aAdd(aDados, &cLinha)

   

		FT_FSkip()

	EndDo



	FT_FUse()

   

	ProcRegua(Len(aDados))

	

	Begin Transaction

				

		If !Found()

			Reclock("DA0",.T.)

			Replace DA0_FILIAL		with xfilial("DA0")

			Replace DA0_CODTAB		with alltrim(aResps[2])

			Replace DA0_DESCRI		with alltrim(aResps[3])

			Replace DA0_DATDE			with aResps[4]

			Replace DA0_DATATE		with aResps[5]

			Replace DA0_ATIVO			with "1"

			Replace DA0_HORADE		with "00:00"

			Replace DA0_HORATE		with "23:59"

			Replace DA0_TPHORA		with "1"

			MsUnLock()

		Endif



		_qry1 := "SELECT DA0_CODTAB AS DA0CODTAB, DA0_DATDE AS DA0DATDE, DA0_FATOR AS DA0FATOR "+cEOL

		_qry1 += "FROM "+RetSqlName("DA0")+" DA0 WHERE D_E_L_E_T_<>'*' AND DA0_TABVIR= '"+alltrim(aResps[2])+"' "+cEOL



		If Select("TMPDA0") > 0

			TMPDA0->(DbCloseArea())

		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry1),"TMPDA0",.T.,.T.)

		DbSelectArea("TMPDA0")

		TMPDA0->(DbGoTop())



		While !TMPDA0->(Eof())

			

			_qry2 := "UPDATE "+RetSQLName("DA1")+" SET D_E_L_E_T_='*', R_E_C_D_E_L_=R_E_C_N_O_ WHERE D_E_L_E_T_<>'*' AND DA1_CODTAB='"+TMPDA0->DA0CODTAB+"' "+cEOL

			TcSqlExec(_qry2)



			cTabVirt += TMPDA0->DA0CODTAB +"/"

			

			TMPDA0->(DbSkip())

			

		EndDo

        	

		For nI := 1 To Len(aDados)



			IncProc("Gravando a Linha "+AllTrim(strzero(nI,6))+" de "+AllTrim(strzero(Len(aDados),6)))

			

			DbSelectArea("SB4")

			DbSetOrder(1)

			DbSeek(xfilial("SB4")+alltrim(aDados[nI,3]))

  		  		

			DbSelectArea("SBV")

			DbSetOrder(2)

			DbSeek(xfilial("SBV")+SB4->B4_COLUNA+alltrim(aDados[nI,7]))

  		  		

			If !Found()

				DbSelectArea("SBV")

				DbSetOrder(1)

				DbSeek(xfilial("SBV")+SB4->B4_COLUNA+alltrim(aDados[nI,7]))

			Endif

  		  		

			DbSelectArea("SB1")

			DbSetOrder(1)

			DbSeek(xfilial("SB1")+alltrim(aDados[nI,3])+alltrim(aDados[nI,5])+alltrim(aDados[nI,7]))



			If Found()

				DbSelectArea("DA1")

				DbSetOrder(1)

				DbSeek(xfilial("DA1")+alltrim(aResps[2])+alltrim(aDados[nI,3])+alltrim(aDados[nI,5])+alltrim(aDados[nI,7]))

				

				If !Found()

					RecLock("DA1",.T.)

					Replace DA1_FILIAL			with xfilial("DA1")

					Replace DA1_CODTAB			with aResps[2]

					Replace DA1_ITEM				with strzero(nI,6)

					Replace DA1_CODPRO			with alltrim(aDados[nI,3])+alltrim(aDados[nI,5])+alltrim(aDados[nI,7])

					Replace DA1_PRCVEN			with val(alltrim(aDados[nI,8]))

					Replace DA1_ATIVO				with "1"

					Replace DA1_MOEDA				with 1

					Replace DA1_TPOPER			with "4"

					Replace DA1_DATVIG			with ddatabase

					Replace DA1_QTDLOT			with 999999            

					Replace DA1_INDLOT 			with '000000000999999.99'

					MsUnLock()

				Else

					RecLock("DA1",.F.)

					Replace DA1_PRCVEN			with val(alltrim(aDados[nI,8]))

					MsUnLock()

				Endif

			Else

				cError += "Produto ["+alltrim(aDados[nI,3])+alltrim(aDados[nI,5])+alltrim(aDados[nI,7])+"] nao encontrado ! - Linha ["+alltrim(aDados[nI,9])+"]"+Chr(13)+Chr(10)

			Endif

			

			TMPDA0->(DbGoTop())

			

			While !TMPDA0->(Eof())

		

				DbSelectArea("DA1")

				DbSetOrder(1)

				DbSeek(xfilial("DA1")+TMPDA0->DA0CODTAB+alltrim(aDados[nI,3])+alltrim(aDados[nI,5])+alltrim(aDados[nI,7]))

					

				If !Found()

					RecLock("DA1",.T.)

					Replace DA1_FILIAL			with xfilial("DA1")

					Replace DA1_CODTAB			with TMPDA0->DA0CODTAB

					Replace DA1_ITEM				with strzero(nI,6)

					Replace DA1_CODPRO			with alltrim(aDados[nI,3])+alltrim(aDados[nI,5])+alltrim(aDados[nI,7])

					Replace DA1_PRCVEN			with VAL(aDados[nI,8]) * TMPDA0->DA0FATOR

					Replace DA1_ATIVO				with "1"

					Replace DA1_MOEDA				with 1

					Replace DA1_TPOPER			with "4"

					Replace DA1_DATVIG			with STOD(TMPDA0->DA0DATDE)

					Replace DA1_QTDLOT			with 999999            

					Replace DA1_INDLOT 			with '000000000999999.99'

					MsUnLock()

				Else

					RecLock("DA1",.F.)

					Replace DA1_PRCVEN			with VAL(aDados[nI,8]) * TMPDA0->DA0FATOR

					MsUnLock()

				Endif

	

				TMPDA0->(DbSkip())

			

			EndDo

						

		Next nI

		

	End Transaction



	If !Empty(cError)

		EECView(cError)

	EndIf

	

	If cTabVirt==""

		Alert("Tabela "+aResps[2]+", atualizada com sucesso!")

	Else

		Alert("Tabela Principal "+aResps[2]+" e Tabelas Virtuais "+Substring(cTabVirt,1,(len(cTabVirt)-1))+", atualizadas com sucesso!")

	End

	

Return