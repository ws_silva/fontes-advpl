#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function HEXECIMP()
Local cQuery := ""

cQuery := "SELECT * FROM SC5010 where D_E_L_E_T_ = '*' and C5_XINTEGR = 'S' order by C5_XPEDMIL"

cQuery := ChangeQuery(cQuery)

If Select("TMPSC5") > 0 
	TMPSC5->(DbCloseArea())
Endif

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSC5", .F., .T.)

DbSelectArea("TMPSC5")
DbGoTop()
While !EOF()
		
	aCabec := {}

   	aadd(aCabec,{"C5_FILIAL"	, TMPSC5->C5_FILIAL	 		,Nil})
   	aadd(aCabec,{"C5_NUM"		, TMPSC5->C5_NUM	 		,Nil})
   	aadd(aCabec,{"C5_TIPO"		, TMPSC5->C5_TIPO	 		,Nil})
   	aadd(aCabec,{"C5_EMISSAO"	, STOD(TMPSC5->C5_EMISSAO) 	,Nil})
   	aadd(aCabec,{"C5_FECENT"	, STOD(TMPSC5->C5_FECENT)	,Nil})
   	aadd(aCabec,{"C5_TABELA"	, TMPSC5->C5_TABELA	 		,Nil})
   	aadd(aCabec,{"C5_CLIENTE"	, TMPSC5->C5_CLIENTE 		,Nil})
   	aadd(aCabec,{"C5_LOJACLI"	, TMPSC5->C5_LOJACLI 		,Nil})
   	aadd(aCabec,{"C5_CLIENT"	, TMPSC5->C5_CLIENT  		,Nil})
   	aadd(aCabec,{"C5_LOJAENT"	, TMPSC5->C5_LOJAENT 		,Nil})
   	aadd(aCabec,{"C5_XCANAL"	, TMPSC5->C5_XCANAL  		,Nil})
   	aadd(aCabec,{"C5_XCANALD"	, TMPSC5->C5_XCANALD 		,Nil})
   	aadd(aCabec,{"C5_XEMP"		, TMPSC5->C5_XEMP    		,Nil})
   	aadd(aCabec,{"C5_XDESEMP"	, TMPSC5->C5_XDESEMP 		,Nil})
   	aadd(aCabec,{"C5_XGRUPO"	, TMPSC5->C5_XGRUPO  		,Nil})
   	aadd(aCabec,{"C5_XGRUPON"	, TMPSC5->C5_XGRUPON 		,Nil})
   	aadd(aCabec,{"C5_XCODDIV"	, TMPSC5->C5_XCODDIV 		,Nil})
   	aadd(aCabec,{"C5_XNONDIV"	, TMPSC5->C5_XNONDIV 		,Nil})
   	aadd(aCabec,{"C5_XMICRRE"	, TMPSC5->C5_XMICRRE 		,Nil})
   	aadd(aCabec,{"C5_XMICRDE"	, TMPSC5->C5_XMICRDE 		,Nil})
   	aadd(aCabec,{"C5_XCODREG"	, TMPSC5->C5_XCODREG 		,Nil})
   	aadd(aCabec,{"C5_XDESREG"	, TMPSC5->C5_XDESREG 		,Nil})
   	aadd(aCabec,{"C5_TRANSP"	, TMPSC5->C5_TRANSP  		,Nil})
   	aadd(aCabec,{"C5_CONDPAG"	, TMPSC5->C5_CONDPAG 		,Nil})
   	aadd(aCabec,{"C5_XDESCOD"	, TMPSC5->C5_XDESCOD 		,Nil})
   	aadd(aCabec,{"C5_VEND1"		, TMPSC5->C5_VEND1   		,Nil})
   	aadd(aCabec,{"C5_COMIS1"	, TMPSC5->C5_COMIS1  		,Nil})
   	aadd(aCabec,{"C5_NOMVEND"	, TMPSC5->C5_NOMVEND 		,Nil})
   	aadd(aCabec,{"C5_VEND2"		, TMPSC5->C5_VEND2   		,Nil})
   	aadd(aCabec,{"C5_COMIS2"	, TMPSC5->C5_COMIS2  		,Nil})
   	aadd(aCabec,{"C5_NOMGERE"	, TMPSC5->C5_NOMGERE 		,Nil})
   	aadd(aCabec,{"C5_POLCOM"	, TMPSC5->C5_POLCOM  		,Nil})
   	aadd(aCabec,{"C5_PRAZO"		, TMPSC5->C5_PRAZO   		,Nil})
   	aadd(aCabec,{"C5_TPPED"		, TMPSC5->C5_TPPED   		,Nil})
   	aadd(aCabec,{"C5_XPEDRAK"	, TMPSC5->C5_XPEDRAK 		,Nil})
   	aadd(aCabec,{"C5_XPEDWEB"	, TMPSC5->C5_XPEDWEB 		,Nil})
   	aadd(aCabec,{"C5_XPEDMIL"	, TMPSC5->C5_XPEDMIL 		,Nil})
   	aadd(aCabec,{"C5_XINTEGR"	, TMPSC5->C5_XINTEGR 		,Nil})

	cQuery := "SELECT * FROM SC6010 where C6_NUM = '"+TMPSC5->C5_NUM+"' and D_E_L_E_T_ = '*' and C6_XINTEGR='S' "
	
	cQuery := ChangeQuery(cQuery)

	If Select("TMPITEM") > 0 
		TMPITEM->(DbCloseArea())
	Endif

	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPITEM", .F., .T.)
	
	aItens := {}
	
	DbSelectArea("TMPITEM")
	TMPITEM->(DbGoTop())		    
	While !EOF()		
		aLinha := {}

		DbSelectArea("DA1")
		DbSetOrder(1)
		DbSeek(xfilial("DA1")+TMPSC5->C5_CONDPAG+TMPITEM->C6_PRODUTO)
		
		If !Found() .or. TMPITEM->C6_PRUNIT <> TMPSC5->C5_TABELA
			aCabec[6,2] := ""
			_prunit := TMPITEM->C6_PRUNIT
		Else
			_prunit := TMPITEM->C6_PRUNIT
		Endif
		
		aadd(aLinha,{"C6_FILIAL"	,TMPITEM->C6_FILIAL	,Nil})			
		aadd(aLinha,{"C6_ITEM"		,TMPITEM->C6_ITEM	,Nil})			
		aadd(aLinha,{"C6_PRODUTO"	,TMPITEM->C6_PRODUTO,Nil})			
		aadd(aLinha,{"C6_DESCRI"	,TMPITEM->C6_DESCRI	,Nil})					
		aadd(aLinha,{"C6_QTDVEN"	,TMPITEM->C6_QTDVEN	,Nil})			
		aadd(aLinha,{"C6_PRCVEN"	,TMPITEM->C6_PRCVEN	,Nil})			
		aadd(aLinha,{"C6_PRUNIT"	,_prunit			,Nil})			
		aadd(aLinha,{"C6_TES"		,TMPITEM->C6_TES	,Nil})
		aadd(aLinha,{"C6_LOCAL"		,TMPITEM->C6_LOCAL	,Nil})
		aadd(aLinha,{"C6_QTDENT"	,TMPITEM->C6_QTDENT	,Nil})			
		aadd(aLinha,{"C6_NOTA"		,TMPITEM->C6_NOTA	,Nil})					
		aadd(aItens,aLinha)
		
		TMPITEM->(DbSkip())
	End
	
	TMPITEM->(DbCloseArea())
		
	BEGIN TRANSACTION
	
	lMsErroAuto := .F.
	
	If Len(aCabec) > 0 .and. Len(aItens) > 0
		MATA410(aCabec,aItens,3)

		If lMsErroAuto
			MostraErro("\log_import\",TMPSC5->C5_NUM+"_PED.log")
			//MostraErro()
		Else
			_qry1 := "Update SC5010 set C5_XINTEGR = 'X' where C5_NUM = '"+TMPSC5->C5_NUM+"' and D_E_L_E_T_ = '*' and  C5_XINTEGR='S'"
			TcSqlExec(_qry1)
			_qry2 := "Update SC6010 set C6_XINTEGR = 'X' where C6_NUM = '"+TMPSC5->C5_NUM+"' and D_E_L_E_T_ = '*' and  C6_XINTEGR='S'"
			TcSqlExec(_qry2)
			//MsgInfo("Pedido "+TMPSC5->C5_NUM+" gerado com sucesso.","Aviso")
		EndIf
	Endif
	
	END TRANSACTION

	TMPSC5->(DbSkip())	
EndDo

TMPSC5->(DbCloseArea())

Return
