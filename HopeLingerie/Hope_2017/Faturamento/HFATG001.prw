#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � HFATG001 �Autor  � Actual Trend       � Data � 13/02/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � Tela para selecao de Instrucoes do Pedido                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HFATG001()

Local aArea 			:= GetArea()
Local cTitulox		:= ""
Local cTabela			:= "Z4"                     
Local lRet			:= .T.
Local aBox			:= {}
Local nTam			:= 2
Local MvParDef		:= ""
Local cTipos			:= ""
Local cTiposDesc		:= ""
local cAux2			:= ""
local nx

Public cProcs			:= ""
Public cProcsDesc	:= ""

MvParRet	:= Alltrim(C5_XCDINST)   // Carrega Nome da Variavel do Get em Questao
MvParRet2	:= Alltrim(C5_XINSTRU) 	// Carrega Nome da Variavel do Get em Questao

dbSelectArea("SX5")
DbSetOrder(1)
DbGoTop()
cTitulox := "INSTRUCOES DO PEDIDO"

While !Eof()
	If Alltrim(SX5->X5_TABELA) == cTabela
		Aadd(aBox,Alltrim(SX5->X5_CHAVE) + " - " + Alltrim(SX5->X5_DESCRI))
		MvParDef += SubStr(SX5->X5_CHAVE,1,2)
   End
   dbSkip()
EndDo

IF f_Opcoes(	@MvParRet		,;    //Variavel de Retorno
 				cTitulox		,;    //Titulo da Coluna com as opcoes
 				@aBox			,;    //Opcoes de Escolha (Array de Opcoes)
 				@MvParDef		,;    //String de Opcoes para Retorno
 				NIL				,;    //Nao Utilizado
 				NIL				,;    //Nao Utilizado
 				.F.				,;    //Se a Selecao sera de apenas 1 Elemento por vez
 				nTam			,;    //Tamanho da Chave
 				Len(aBox)		,;    //No maximo de elementos na variavel de retorno
 				.T.				,;    //Inclui Botoes para Selecao de Multiplos Itens
 				.F.				,;    //Se as opcoes serao montadas a partir de ComboBox de Campo ( X3_CBOX )
 				NIL				,;    //Qual o Campo para a Montagem do aOpcoes
 				.F.				,;    //Nao Permite a Ordenacao
 				.F.				,;    //Nao Permite a Pesquisa    
 				.F.				,;    //Forca o Retorno Como Array
 			  )     					//Consulta F3    
End

RestArea(aArea)

If !Empty(MvParRet) .and. AT("**",MvParRet)<>0
	For nx := 1 to len(MvParRet) step 2
		cAux2 := SubStr(MvParRet,nx,2)
		If cAux2 <> '**'
			cTipos		+= cAux2+","
			cTiposDesc	+= Alltrim(POSICIONE("SX5",1,xFilial("SX5")+'Z4'+cAux2,"X5_DESCRI"))+"|"
		EndIf 
	Next
	cProcs		:= SubStr(cTipos,1,Len(cTipos)-1)
	cProcsDesc	:= SubStr(cTiposDesc,1,Len(cTiposDesc)-1)
Else
	cProcs		:= MvParRet
	cProcsDesc	:= MvParRet2
EndIf

M->C5_XCDINST	:= cProcs

Return (cProcsDesc)