#Include 'Protheus.ch'
#Include 'FWMVCDEF.ch'
#Include 'RestFul.CH'
#include 'Totvs.ch'

User Function HAPICLI()
Return

Class CONSULTACLIENTE
	
	Data cCodigo	As String
	Data cLoja	  	As String
	Data cPk        As String

	Method New(cCodigo,cLoja,cPk) Constructor
	 
EndClass

Method New(cCodCli, cLojCli, cPk) Class CONSULTACLIENTE
	::cCodigo := cCodCli
	::cLoja   := cLojCli
	::cPk     := cPk
Return(Self)

Class ERRORCLI
	
	Data cErro	As String
	
	Method New(cErro) Constructor
	 
EndClass

Method New(cCodErro) Class ERRORCLI
	::cErro := cCodErro
Return(Self)

WSRESTFUL CLIENTE DESCRIPTION "Servico REST cadastro de clientes Protheus"
	WSDATA CPF As String
	WSDATA CODLOJA As String
	
	WSMETHOD POST DESCRIPTION "Inclui cliente no Protheus" WSSYNTAX "/CLIENTE/"
	WSMETHOD PUT DESCRIPTION "Altera cadastro de cliente no Protheus" WSSYNTAX "/CLIENTE/{CODIGO+LOJA}"
	WSMETHOD GET DESCRIPTION "Retorna o codigo e loja do cliente" WSSYNTAX "/CLIENTE/{CPF}"
END WSRESTFUL

WSMETHOD GET WSRECEIVE CPF WSSERVICE CLIENTE
Local cCPF     := Self:CPF
Local aArea    := GetArea()
Local oObjCli  := Nil
Local oObjErro := Nil
Local cJson    := ""

::SetContentType("application/json")

If !Empty(cCPF)
	DbSelectArea("SA1")
	SA1->( DbSetOrder(3) )
	If SA1->( DbSeek( xFilial("SA1") + cCPF ) )
	    oObjCli := CONSULTACLIENTE():New(SA1->A1_COD,SA1->A1_LOJA,Encode64(xFilial("SA1")+SA1->A1_COD+SA1->A1_LOJA))

	    cJson := FWJsonSerialize(oObjCli)

		::SetResponse(cJson)
	Else
		oObjErro := ERRORCLI():New("Cliente nao encontrado")
		cJson := FWJsonSerialize(oObjErro)
		::SetResponse(cJson)
	EndIf
Else
	oObjErro := ERRORCLI():New("Favor informar o CFP do cliente para consulta")
	cJson := FWJsonSerialize(oObjErro)
	::SetResponse(cJson)
EndIf

RestArea(aArea)
Return(.T.)

WSMETHOD POST WSSERVICE CLIENTE
Local cJson  := ::GetContent()
Local oParse := Nil
Local aClientes := {}
Local cRet := ""
Local nReg := 0
Local cCodCli := ""
Local aCli := {}
Local cCPF := ""
Local aLog := {}

Local cDtTime := DTOS(DDATABASE) + SubStr(Time(),1,2) + SubStr(Time(),4,2) + SubStr(Time(),7,2)

::SetContentType("application/json")

If Empty(cJson)
	SetRestFault(400,'JSON VAZIO.')
	Return .F.
EndIf
	
If FwJsonDeserialize(cJson,@oParse)
	nReg := Len(oParse:models[1]:fields)
	
	If nReg > 0 
		For nx := 1 to nReg
			aAdd(aClientes,{oParse:models[1]:fields[nx]:id,oParse:models[1]:fields[nx]:value})
			If AllTrim(oParse:models[1]:fields[nx]:id) = "A1_CGC"
				cCPF := AllTrim(oParse:models[1]:fields[nx]:value)
			EndIf
		Next
		
		DbSelectArea("SA1")
		DbSetOrder(3)
		If DbSeek(xFilial("SA1")+cCPF)
			SetRestFault(400,'CPF/CNPJ JA CADASTRADO. FAVOR CONSULTAR O CPF PARA RECEBER O CODIGO DE CLIENTE.')
    		Return .F.
		EndIf
	Else
		SetRestFault(400,'CAMPOS NAO INFORMADOS NO JSON.')
    	Return .F.
	EndIf
	
	If Len(aClientes) = 0 
		SetRestFault(400,'NAO A CAMPOS A SEREM INSERIDOS.')
    	Return .F.
	EndIf
	
	cCodCli:= GETSX8NUM("SA1","A1_COD")

	DbSelectArea("SA1")
	DbSetOrder(1)
	DbSeek(xfilial("SA1")+cCodCli)
		
	While Found()
		CONFIRMSX8()	
		cCodCli:= GETSX8NUM("SA1","A1_COD")
		
		DbSeek(xfilial("SA1")+cCodCli)
	EndDo

	aAdd(aCli,{"A1_FILIAL",xFilial("SA1"),}) 
	aAdd(aCli,{"A1_COD"   ,cCodCli       ,}) 
	aAdd(aCli,{"A1_LOJA"  ,"0001"        ,}) 
	
	cCodMun := ""
	cEstado := ""
	cPais   := ""
	cNature := ""
	cConta  := ""
	For ny := 1 to Len(aClientes)
		cCampo := AllTrim(aClientes[ny][1])
		cValor := UPPER(NoAcento(AnsiToOem(AllTrim(aClientes[ny][2])))) 
		aAdd(aCli,{cCampo,cValor,})
		
		If cCampo = "A1_EST"
			cEstado := cValor
		EndIf
		
		If cCampo = "A1_COD_MUN"
			cCodMun := cValor
		EndIf
		
		If cCampo = "A1_PAIS"
			cPais := cValor
		EndIf
		
		If cCampo = "A1_NATUREZ"
			cNature := cValor
		EndIf
		
		If cCampo = "A1_CONTA"
			cConta := cValor
		EndIf
	Next
	
	DbSelectArea("CC2")
	DbSetOrder(1)
	If !DbSeek(xFilial("CC2")+cEstado+cCodMun)
		SetRestFault(400,'ERRO NO CAMPO A1_COD_MUN. CODIGO DE MUNICIPIO NAO ENCONTRADO.')
    	Return .F.
	EndIf
	
	DbSelectArea("SYA")
	DbSetOrder(1)
	If !DbSeek(xFilial("SYA")+cPais)
		SetRestFault(400,'ERRO NO CAMPO A1_PAI. CODIGO DE PAIS NAO ENCONTRADO.')
    	Return .F.
	EndIf
	
	DbSelectArea("SED")
	DbSetOrder(1)
	If !DbSeek(xFilial("SED")+cNature)
		SetRestFault(400,'ERRO NO CAMPO A1_NATUREZ. CODIGO DA NATUREZA NAO ENCONTRADO.')
    	Return .F.
	EndIf
	
	DbSelectArea("CT1")
	DbSetOrder(1)
	If !DbSeek(xFilial("CT1")+cConta)
		SetRestFault(400,'ERRO NO CAMPO A1_CONTA. CODIGO DA CONTA CONTABIL NAO ENCONTRADO.')
    	Return .F.
	EndIf
	
	lMsErroAuto := .F.
	 
	MATA030(aCli,3)
	 
	If lMsErroAuto
		MostraErro("\Log\API\",cDtTime+"_ERRO_EXECAUTO.log")
		aLog := GetAutoGRLog()
	   	SetRestFault(400,'ERRO NA INCLUSAO DO CLIENTE.')
    	Return .F.
	Else
		cRet := '{"Mensagem":"Cliente inserido com sucesso","Codigo":"'+cCodCli+'","Loja":"0001"}'
	
    	::SetResponse(cRet) 
	EndIf
Else
    SetRestFault(400,'ERRO AO DECODIFICAR O JSON.')
    Return .F.
EndIf 	

Return .T.

WSMETHOD PUT WSRECEIVE CODLOJA WSSERVICE CLIENTE
Local cCodLoja := Self:CODLOJA
Local cJson    := ::GetContent()
Local oParse   := Nil
Local aClientes := {}
Local cRet := ""
Local nReg := 0

//If Len(::aURLParms) == 0
//   SetRestFault(400,"CODIGO+LOJA SAO OBRIGATORIOS.")
//   Return .F.
//EndIf

::SetContentType("application/json")
   
If Empty(cJson)
	SetRestFault(400,'JSON VAZIO.')
	Return .F.
EndIf

If FwJsonDeserialize(cJson,@oParse)
	nReg := Len(oParse:models[1]:fields)
	
	If nReg > 0 
		For nx := 1 to nReg
			aAdd(aClientes,{oParse:models[1]:fields[nx]:id,oParse:models[1]:fields[nx]:value})
		Next
	Else
		SetRestFault(400,'CAMPOS NAO INFORMADOS NO JSON.')
    	Return .F.
	EndIf
	
	If Len(aClientes) = 0 
		SetRestFault(400,'NAO A CAMPOS A SEREM INSERIDOS.')
    	Return .F.
	EndIf

	DbSelectArea("SA1")
	DbSetOrder(1)
	If DbSeek(xFilial("SA1")+cCodLoja)
		RecLock("SA1",.F.)
			For ny := 1 to Len(aClientes)
				If !(AllTrim(aClientes[ny][1]) $ "A1_COD/A1_LOJA/A1_CGC/A1_TIPO/A1_PESSOA")
					CAMPO := "SA1->"+AllTrim(aClientes[ny][1])

					&CAMPO := UPPER(NoAcento(AnsiToOem(AllTrim(aClientes[ny][2]))))
				EndIf
			Next
		MsUnlock()
		
		cRet := '{"Mensagem":"Cliente alterado com sucesso"}'
	
		::SetResponse(cRet)
		
	Else
		SetRestFault(400,'NAO FOI ENCONTRADO CLIENTE COM ESSE CODIGO.')
    	Return .F.
	EndIf
Else
	SetRestFault(400,'ERRO AO DECODIFICAR O JSON.')
    Return .F.
EndIf

Return .T.

/*Exemplo de JSON para inclusao via POST
{"id":"MATA030","operation":3,"models":[
	{"id":"MATA030_SA1","modeltype":"FIELDS","fields":[
		{"id":"A1_TIPO","value":"F"},
		{"id":"A1_PESSOA","value":"F"},
		{"id":"A1_NOME","value":"Melissa Queiroz"},
		{"id":"A1_NREDUZ","value":"Melissa Queiroz"},
		{"id":"A1_CGC","value":"32434237835"},
		{"id":"A1_INSCR","value":"ISENTO"},
		{"id":"A1_END","value":"Rua Itanha�m, 514 apartamento 83"},
		{"id":"A1_BAIRRO","value":"Vila Prudente"},
		{"id":"A1_EST","value":"SP"},
		{"id":"A1_CEP","value":"03137020"},
		{"id":"A1_REGIAO","value":"007"},
		{"id":"A1_COD_MUN","value":"50308"},
		{"id":"A1_MUN","value":"S�o Paulo"},
		{"id":"A1_NATUREZ","value":"11116001"},
		{"id":"A1_PAIS","value":"105"},
		{"id":"A1_CONTA","value":"101020201501"},
		{"id":"A1_EMAIL","value":"melqueiroz@gmail.com"}]
	}]
}*/