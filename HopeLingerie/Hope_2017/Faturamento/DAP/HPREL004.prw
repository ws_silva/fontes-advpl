#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} RF013 
Relatório Viip - Ecommerce 
@author Weskley Silva
@since 11/08/2017
@version 2.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL004()

	Private oReport
	Private cPergCont	:= 'HPREL004' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'ECOM', 'RELATORIO VIPP ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELATORIO VIPP' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO VIPP', { 'ECOM', 'SC5','SA1','SF2','CC2'})
			
	TRCell():New( oSection1, 'NOME'		    	    ,'ECOM', 		'NOME',								"@!"                        ,40)
	TRCell():New( oSection1, 'ENDE'      	        ,'ECOM', 		'ENDE',		    					"@!"				        ,80)
	TRCell():New( oSection1, 'NUMERO'          		,'ECOM', 		'NUMERO',              				"@!"						,06)
	TRCell():New( oSection1, 'COMPLEMENTO'         	,'ECOM', 		'COMPLEMENTO',              		"@!"						,15)
	TRCell():New( oSection1, 'BAIRRO' 				,'ECOM', 		'BAIRRO' ,	        				"@!"		                ,20)
	TRCell():New( oSection1, 'MUNICIPIO'	       	,'ECOM', 		'MUNICIPIO',          				"@!"		                ,20)
	TRCell():New( oSection1, 'ESTADO' 				,'ECOM', 		'ESTADO' ,	    					"@!"		                ,15)
	TRCell():New( oSection1, 'CEP'	        		,'ECOM', 		'CEP' ,		        				"@!"		                ,10)
	TRCell():New( oSection1, 'TELEFONE'			    ,'ECOM', 		'TELEFONE',			   				"@!"						,15)
	TRCell():New( oSection1, 'FINANCEIRO'			,'ECOM', 		'FINANCEIRO' ,	    				"@!"	                    ,20)
	TRCell():New( oSection1, 'REGISTRO'				,'ECOM', 		'REGISTRO' ,	  				  	"@!"		                ,20)
	TRCell():New( oSection1, 'PESO'					,'ECOM', 		'PESO',						    	"@!"		                ,03)
	TRCell():New( oSection1, 'ALTURA'				,'ECOM', 		'ALTURA' ,			    			"@!"	                    ,08)
	TRCell():New( oSection1, 'LARGURA'		        ,'ECOM', 		'LARGURA',				   			"@!"						,15)
	TRCell():New( oSection1, 'COMPRIMENTO'		    ,'ECOM', 		'COMPRIMENTO',			   			"@!"						,15)
	TRCell():New( oSection1, 'CUBICO'		        ,'ECOM', 		'CUBICO',				   			"@!"						,15)
    TRCell():New( oSection1, 'NF'			        ,'ECOM', 		'NF',					   			"@!"						,15)
    TRCell():New( oSection1, 'SERIE'		        ,'ECOM', 		'SERIE',				   			"@!"						,15)
	TRCell():New( oSection1, 'VALOR'		        ,'ECOM', 		'VALOR',				   			"@E 999.999,99"				,15)
	TRCell():New( oSection1, 'VALOR_A_COBRAR'		,'ECOM', 		'VALOR_A_COBRAR',				   	"@E 999.999,99"				,15)
	TRCell():New( oSection1, 'ADICIONAIS'			,'ECOM', 		'ADICIONAIS',					   	"@!"						,15)
    TRCell():New( oSection1, 'CONTRATO'				,'ECOM', 		'CONTRATO',						   	"@!"						,15)
	TRCell():New( oSection1, 'ADMINISTRATIVO'		,'ECOM', 		'ADMINISTRATIVO',				   	"@!"						,15)
	TRCell():New( oSection1, 'CARTAO'				,'ECOM', 		'CARTAO',				   			"@!"						,15)
	TRCell():New( oSection1, 'EMAIL'				,'ECOM', 		'EMAIL',				   			"@!"						,15)
	TRCell():New( oSection1, 'OBS1'					,'ECOM', 		'OBS1',				   				"@!"						,15)
	TRCell():New( oSection1, 'OBS2'					,'ECOM', 		'OBS2',				   				"@!"						,15)
	TRCell():New( oSection1, 'OBS3'					,'ECOM', 		'OBS3',				   				"@!"						,15)
	TRCell():New( oSection1, 'DESC_OBJETO'		    ,'ECOM', 		'DESC_OBJETO',		   				"@!"						,15)
	TRCell():New( oSection1, 'ID_VOLUME'			,'ECOM', 		'ID_VOLUME',		   				"@!"						,15)
	TRCell():New( oSection1, 'QTDE_VOLUME'			,'ECOM', 		'QTDE_VOLUME',		   				"@!"						,15)
	TRCell():New( oSection1, 'COD_CLIENTE_VISUAL'	,'ECOM', 		'COD_CLIENTE_VISUAL',  				"@!"						,15)
	TRCell():New( oSection1, 'CELULAR'			    ,'ECOM', 		'CELULAR',			   				"@!"						,15)
	
								

Return( oReport )



//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("ECOM") > 0
		ECOM->(dbCloseArea())
	Endif
      
	cQuery := " SELECT " 
	cQuery += " SA1010.A1_NOME AS NOME, " 
	cQuery += " SA1010.A1_ENDENT AS ENDE, " 
	cQuery += " '' AS NUMERO, " 
	cQuery += " SA1010.A1_COMPLEM AS COMPLEMENTO , "  
	cQuery += " SA1010.A1_BAIRROE AS BAIRRO, " 
	cQuery += " SA1010.A1_MUNE AS MUNICIPIO, " 
	cQuery += " SA1010.A1_ESTE AS ESTADO, " 
	cQuery += " SA1010.A1_CEPE AS CEP, " 
	cQuery += " SA1010.A1_DDD+SA1010.A1_TELE AS TELEFONE, "
	cQuery += " '' AS FINANCEIRO, "
	cQuery += " '' AS REGISTRO, " 
	cQuery += " F2_PBRUTO AS PESO, " 
	cQuery += " '' AS ALTURA, " 
	cQuery += " '' AS LARGURA, " 
	cQuery += " '' AS COMPRIMENTO, " 
	cQuery += " '' AS CUBICO, " 
	cQuery += " SC5010.C5_NOTA AS NF, " 
	cQuery += " SC5010.C5_SERIE AS SERIE, " 
	cQuery += " CAST(SF2010.F2_VALBRUT  AS NUMERIC(15,2)) AS VALOR, " 
	cQuery += " '' AS VALOR_A_COBRAR, " 
	cQuery += " '' AS ADICIONAIS, " 
	cQuery += " '9912246333' AS CONTRATO, " 
	cQuery += " '' AS ADMINISTRATIVO, " 
	cQuery += " CC2_XCPOST AS CARTAO, " 
	cQuery += " SA1010.A1_EMAIL AS EMAIL, " 
	cQuery += " ISNULL(CASE " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'IKE%'  THEN REPLACE(SC5010.C5_XPEDWEB, 'IKE', '') " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'NU%'  THEN REPLACE(SC5010.C5_XPEDWEB, 'NU', '') " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'TNU%'  THEN REPLACE(SC5010.C5_XPEDWEB, 'TNU', '') " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'TNU2%'  THEN REPLACE(SC5010.C5_XPEDWEB, 'TNU2', '') " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'D%'  THEN REPLACE(SC5010.C5_XPEDWEB, 'D', '') " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'T%'  THEN REPLACE(SC5010.C5_XPEDWEB, 'T', '') " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'T2%' THEN REPLACE(SC5010.C5_XPEDWEB, 'T2', '') " 
    cQuery += " WHEN SC5010.C5_XPEDWEB LIKE 'R%'  THEN REPLACE(SC5010.C5_XPEDWEB, 'R', '') " 
    cQuery += "  END,'') AS OBS1, 
	cQuery += " SC5010.C5_XPEDRAK AS OBS2, " 
	cQuery += " 'E-COMMERCE' AS OBS3, " 
	cQuery += " '' AS DESC_OBJETO, " 
	cQuery += " '' AS ID_VOLUME, " 
	cQuery += " F2_VOLUME1 AS QTDE_VOLUME, " 
	cQuery += " '' AS COD_CLIENTE_VISUAL, "
	cQuery += " '' AS CELULAR " 
 	cQuery += " FROM "+RetSqlName("SC5")+" SC5010 " 
	cQuery += " JOIN "+RetSqlName("SA1")+" SA1010 ON SA1010.A1_COD=SC5010.C5_CLIENT AND SA1010.A1_LOJA = SC5010.C5_LOJACLI AND SA1010.D_E_L_E_T_ = '' " 
	cQuery += " JOIN "+RetSqlName("SF2")+" SF2010 ON SF2010.F2_DOC=SC5010.C5_NOTA AND SF2010.F2_SERIE=SC5010.C5_SERIE AND SC5010.D_E_L_E_T_ = '' " 
	cQuery += " JOIN "+RetSqlName("CC2")+" CC2010 ON CC2010.CC2_CODMUN = SA1010.A1_COD_MUN AND CC2010.CC2_EST = SA1010.A1_EST AND CC2010.D_E_L_E_T_ = ''  " 
	cQuery += " WHERE SF2010.F2_EMISSAO  BETWEEN '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"' " 
	if Empty(mv_par03) .or. Empty(mv_par04)
		cQuery += " AND SF2010.F2_DOC <> '' "
	else
		cQuery += " AND SF2010.F2_DOC BETWEEN '"+ mv_par03 +"' AND '"+ mv_par04 +"' " 
	endif
	cQuery += " AND SF2010.D_E_L_E_T_ = '' " 

	TCQUERY cQuery NEW ALIAS ECOM

	While ECOM->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("NOME"):SetValue(ECOM->NOME)
		oSection1:Cell("NOME"):SetAlign("LEFT")

		oSection1:Cell("ENDE"):SetValue(ECOM->ENDE)
		oSection1:Cell("ENDE"):SetAlign("LEFT")
		
		oSection1:Cell("NUMERO"):SetValue(ECOM->NUMERO)
		oSection1:Cell("NUMERO"):SetAlign("LEFT")
		
		oSection1:Cell("COMPLEMENTO"):SetValue(ECOM->COMPLEMENTO)
		oSection1:Cell("COMPLEMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("BAIRRO"):SetValue(ECOM->BAIRRO)
		oSection1:Cell("BAIRRO"):SetAlign("LEFT")
			
		oSection1:Cell("MUNICIPIO"):SetValue(ECOM->MUNICIPIO)
		oSection1:Cell("MUNICIPIO"):SetAlign("LEFT")
				
		oSection1:Cell("ESTADO"):SetValue(ECOM->ESTADO)
		oSection1:Cell("ESTADO"):SetAlign("LEFT")
				
		oSection1:Cell("CEP"):SetValue(ECOM->CEP)
		oSection1:Cell("CEP"):SetAlign("LEFT")
		
		oSection1:Cell("TELEFONE"):SetValue(ECOM->TELEFONE)
		oSection1:Cell("TELEFONE"):SetAlign("LEFT")
						
		oSection1:Cell("FINANCEIRO"):SetValue(ECOM->FINANCEIRO)
		oSection1:Cell("FINANCEIRO"):SetAlign("LEFT")
				
		oSection1:Cell("REGISTRO"):SetValue(ECOM->REGISTRO)
		oSection1:Cell("REGISTRO"):SetAlign("LEFT")
				
		oSection1:Cell("PESO"):SetValue(ECOM->PESO)
		oSection1:Cell("PESO"):SetAlign("LEFT")
		
		oSection1:Cell("ALTURA"):SetValue(ECOM->ALTURA)
		oSection1:Cell("ALTURA"):SetAlign("LEFT")
		
		oSection1:Cell("COMPRIMENTO"):SetValue(ECOM->COMPRIMENTO)
		oSection1:Cell("COMPRIMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("CUBICO"):SetValue(ECOM->CUBICO)
		oSection1:Cell("CUBICO"):SetAlign("LEFT")
		
		oSection1:Cell("NF"):SetValue(ECOM->NF)
		oSection1:Cell("NF"):SetAlign("LEFT")
		
		oSection1:Cell("SERIE"):SetValue(ECOM->SERIE)
		oSection1:Cell("SERIE"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR"):SetValue(ECOM->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_A_COBRAR"):SetValue(ECOM->VALOR_A_COBRAR)
		oSection1:Cell("VALOR_A_COBRAR"):SetAlign("LEFT")
		
		oSection1:Cell("ADICIONAIS"):SetValue(ECOM->ADICIONAIS)
		oSection1:Cell("ADICIONAIS"):SetAlign("LEFT")
		
		oSection1:Cell("CONTRATO"):SetValue(ECOM->CONTRATO)
		oSection1:Cell("CONTRATO"):SetAlign("LEFT")
		
		oSection1:Cell("ADMINISTRATIVO"):SetValue(ECOM->ADMINISTRATIVO)
		oSection1:Cell("ADMINISTRATIVO"):SetAlign("LEFT")
		
		oSection1:Cell("CARTAO"):SetValue(ECOM->CARTAO)
		oSection1:Cell("CARTAO"):SetAlign("LEFT")
		
		oSection1:Cell("EMAIL"):SetValue(ECOM->EMAIL)
		oSection1:Cell("EMAIL"):SetAlign("LEFT")
		
		oSection1:Cell("OBS1"):SetValue(ECOM->OBS1)
		oSection1:Cell("OBS1"):SetAlign("LEFT")
		
		oSection1:Cell("OBS2"):SetValue(ECOM->OBS2)
		oSection1:Cell("OBS2"):SetAlign("LEFT")
		
		oSection1:Cell("OBS3"):SetValue(ECOM->OBS3)
		oSection1:Cell("OBS3"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_OBJETO"):SetValue(ECOM->DESC_OBJETO)
		oSection1:Cell("DESC_OBJETO"):SetAlign("LEFT")
		
		oSection1:Cell("ID_VOLUME"):SetValue(ECOM->ID_VOLUME)
		oSection1:Cell("ID_VOLUME"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_VOLUME"):SetValue(ECOM->QTDE_VOLUME)
		oSection1:Cell("QTDE_VOLUME"):SetAlign("LEFT")
		
		oSection1:Cell("COD_CLIENTE_VISUAL"):SetValue(ECOM->COD_CLIENTE_VISUAL)
		oSection1:Cell("COD_CLIENTE_VISUAL"):SetAlign("LEFT")
		
		oSection1:Cell("CELULAR"):SetValue(ECOM->CELULAR)
		oSection1:Cell("CELULAR"):SetAlign("LEFT")
				
		oSection1:PrintLine()
		
		ECOM->(DBSKIP()) 
	enddo
	ECOM->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Dt Inicial"		        ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt Final"			    ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Nota Inicial"		    ,""		,""		,"mv_ch3","C",09,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Nota Final"			    ,""		,""		,"mv_ch4","C",09,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Cartao"			        ,""		,""		,"mv_ch5","C",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return