#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function MS520VLD()
Local aArea   := GetArea()
Local lRet    := .T.
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cQuery  := ""

cQuery := "select * from "+RetSqlName("SZJ")+" SZJ "
cQuery += CRLF + " where ZJ_DOC = '"+SF2->F2_DOC+"' and ZJ_SERIE='"+SF2->F2_SERIE+"' and SZJ.D_E_L_E_T_ = '' "
cQuery += CRLF + " order by ZJ_DOC,ZJ_SERIE,ZJ_PRODUTO "

MemoWrite("MS520VLD.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

DbSelectArea("TRB")

If TRB->(!EOF())
	While TRB->(!EOF())
		DbSelectArea("SZJ")
		DbSetOrder(1)
		If DbSeek(xFilial("SZJ")+TRB->ZJ_NUMPF+TRB->ZJ_ITEM)
			RecLock("SZJ",.F.)
			SZJ->ZJ_CONF  := ""
			SZJ->ZJ_DOC   := ""
			SZJ->ZJ_SERIE := ""
			MsUnlock()
		EndIf
		
		DbSelectArea("SZP")
		DbSetOrder(1)
		If DbSeek(xFilial("SZP")+TRB->ZJ_NUMPF)
			While SZP->(!EOF()) .and. SZP->ZP_NUMPF = TRB->ZJ_NUMPF
				RecLock("SZP",.F.)
				SZP->ZP_QTDLIB := 0
				MsUnlock()
				SZP->(DbSkip())
			EndDo
		EndIf
				
		DbSelectArea("SB2")
		DbSetOrder(2)
		If DbSeek(xFilial("SB2")+cAmzPik+TRB->ZJ_PRODUTO)
			nQtdXRes := SB2->B2_XRESERV + TRB->ZJ_QTDLIB
			RecLock("SB2",.F.)
			SB2->B2_XRESERV := nQtdXRes
			MsUnlock()
		EndIf
		
		TRB->(DbSkip())
	EndDo
EndIf

RestArea(aArea)

Return lRet