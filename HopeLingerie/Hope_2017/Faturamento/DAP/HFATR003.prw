#Include "Protheus.ch"
#Include "topconn.ch"

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATR003  � Autor � Bruno Parreira     � Data �  16/06/17   ���
�������������������������������������������������������������������������͹��
���Descricao � Relatorio de analise do pre-faturamento gerado.            ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

User Function HFATR003(cLoteDAP)
Local cDesc1       := "Este programa tem como objetivo imprimir relatorio "
Local cDesc2       := "de acordo com os parametros informados pelo usuario."
Local cDesc3       := "Rela��o de Pr�-Faturamentos gerados"
Local cPict        := ""
Local titulo       := "Rela��o dos pr�-faturamentos gerados para o Lote DAP: "
Local nLin         := 80                                                                                                                                                              
					 //0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
					 //012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
Local Cabec1       := "                           QTDE              VALOR             QTDE            VALOR           %                                                                                 "
                     //  1         2       3       4       5        6                                      7              8                  9                   10       11    12    13
Local Cabec2       := "PRE-FAT      PEDIDO        PEDIDO            PEDIDO            PRE-FAT         PRE-FAT         ATENDIDO     CLIENTE                                                            UF"                     
Local imprime      := .T.
Local aOrd         := {}
Local lPerg        := .T.
					//  1   2   3   4   5   6   7   8   9  10  11  12  13  14   
Private aCol       := {000,013,026,043,062,077,097,108,175}
Private lEnd       := .F.
Private lAbortPrint:= .F.
Private CbTxt      := ""
Private limite     := 220
Private tamanho    := "G"
Private nomeprog   := "HFATR003"
Private nTipo      := 15
Private aReturn    := { "Zebrado", 1, "Administracao", 1, 2, 1, "", 1}
Private nLastKey   := 0
Private cbtxt      := Space(10)
Private cbcont     := 00
Private CONTFL     := 01
Private m_pag      := 01
Private wnrel      := "HFATR003"
Private cPerg      := "HFATR003"
Private cString    := "SZJ"
Private cArqTEMP

If Empty(cLoteDap)
	AjustaSX1(cPerg)
	If !Pergunte(cPerg)
		Return
	EndIf
Else
	mv_par01 := cLoteDap	
EndIf

titulo       := "Rela��o dos pr�-faturamentos gerados para o Lote DAP: "+mv_par01

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho,,.F.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

nTipo := If(aReturn[4]==1,15,18)

RptStatus({|| RunReport(Cabec1,Cabec2,Titulo,nLin) },Titulo)

//Roda(cbcont,cbtxt,tamanho)		
	
//���������������������������������������������������������������������Ŀ
//� Finaliza a execucao do relatorio...                                 �
//�����������������������������������������������������������������������

SET DEVICE TO SCREEN

//���������������������������������������������������������������������Ŀ
//� Se impressao em disco, chama o gerenciador de impressao...          �
//�����������������������������������������������������������������������

If aReturn[5]==1
	DbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
Endif

MS_FLUSH()

If Select("TEMP1") > 0                                                                                                                                               	
	TEMP1->(DbCloseArea())
 
	/*If MsgYesNo("Deseja gerar planilha excel ?")
	
		cExcel :=  AllTrim(GetTempPath())+"LOTEDAP_"+StrTran(Dtoc(dDatabase),"/","")+".XLS"
		//cExcel := "D:\LOTEDAP_"+StrTran(Dtoc(dDatabase),"/","")+".XLS"

		fErase(cExcel)
		__CopyFile(cArqTEMP+".DBF",cExcel)
		fErase(cArqTEMP+".*")
		
		oExcelApp:= MsExcel():New()                        	
		oExcelApp:WorkBooks:Open(cExcel)
		oExcelApp:SetVisible(.T.)                                                                          
		lExcel := .T.
		//Return
	EndIf*/  
EndIf

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �RUNREPORT � Autor � Bruno Parreira     � Data �  16/06/17   ���
�������������������������������������������������������������������������͹��
���Descricao � Busca registros no banco e gera relatorio                  ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)
Local cQuery   := ""
Local nTotReg  := 0
Local nContar  := 0
Local nTotQPed := 0
Local nTotVPed := 0
Local nTotQPF  := 0
Local nTotVPF  := 0
Local nCont    := 0
Local nQtdeC6  := 0
Local nVlrC6   := 0

cQuery :=        " select ZJ_LOTDAP,ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,A1_EST,  "
cQuery += CRLF + " SUM(ZJ_QTDLIB) AS ZJ_QTDLIB, SUM(C6_QTDVEN) AS C6_QTDVEN,SUM(C6_VALOR) AS VALORPED, "
cQuery += CRLF + " CASE WHEN SUM(ZJ_QTDLIB) = SUM(C6_QTDVEN) THEN SUM(C6_VALOR) ELSE ROUND(SUM(VALORPF),2) END AS VALORPF from ( "
cQuery += CRLF + "    select ZJ_LOTDAP,ZJ_NUMPF,ZJ_PEDIDO,ZJ_ITEM,ZJ_PRODUTO,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,A1_EST, "
cQuery += CRLF + " 	  ZJ_QTDLIB,C6_QTDVEN,C6_PRCVEN,C6_VALOR,(ZJ_QTDLIB*C6_PRCVEN) AS VALORPF "
cQuery += CRLF + "    FROM "+RetSqlName("SZJ")+" SZJ "
cQuery += CRLF + "       INNER JOIN "+RetSqlName("SC6")+" SC6 "
cQuery += CRLF + "        on C6_NUM = ZJ_PEDIDO "								
cQuery += CRLF + "        and C6_ITEM = ZJ_ITEM "
cQuery += CRLF + "        and C6_PRODUTO = ZJ_PRODUTO "
cQuery += CRLF + "        and SC6.D_E_L_E_T_ = '' "
cQuery += CRLF + "       INNER JOIN "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "        on A1_COD = ZJ_CLIENTE "						  
cQuery += CRLF + "        and A1_LOJA = ZJ_LOJA "
cQuery += CRLF + "        and SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "    where SZJ.D_E_L_E_T_ = '' "
cQuery += CRLF + "    and ZJ_LOTDAP = '"+mv_par01+"' ) A "
cQuery += CRLF + " group by ZJ_LOTDAP,ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,A1_EST  "
cQuery += CRLF + " order by ZJ_LOTDAP,ZJ_PEDIDO "

MemoWrite("HFATR003.TXT",cQuery)

cQuery  := ChangeQuery(cQuery)

DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),"TRB",.F.,.T.)


// ************************ Monta Estrutura do Temporario Excel
_aStruct := {} 
aAdd(_aStruct,{"NUMPF"	    ,"C",08,0})	// Pre-Faturamento
aAdd(_aStruct,{"PEDIDO"	    ,"C",06,0})	// Pedido
aAdd(_aStruct,{"QTDEPEDIDO" ,"N",11,0})	// Qtde Pedido
aAdd(_aStruct,{"VLRPEDIDO"  ,"N",13,2})	// Valor Pedido
aAdd(_aStruct,{"QTDEPREFAT" ,"N",11,0})	// Qtde Pre-Faturamento
aAdd(_aStruct,{"VLRPREFAT"	,"N",13,2})	// Valor Pre-Faturamento
aAdd(_aStruct,{"PERCATEND"	,"N",05,1})	// % Atendido
aAdd(_aStruct,{"CLIENTE"	,"C",99,0})	// Cliente
aAdd(_aStruct,{"ESTADO"  	,"C",02,0})	// Estado

cArqTEMP := CriaTrab(_aStruct)   

dbUseArea( .T.,,cArqTemp,"TEMP1",.F.,.F. )          //__LocalDriver

DbSelectArea("TRB")
TRB->(DbEval({ || nTotReg++ },,{|| !Eof()}))
TRB->(DbGoTop())

If TRB->(!EOF())
	ProcRegua(nTotReg)
	While TRB->(!EOF())
		IncProc("Gerando relatorio, regs : "+StrZero(++nContar,6)+" de "+StrZero(nTotReg,6))
		
		If lAbortPrint
			@ nLin,000 PSAY "*** CANCELADO PELO OPERADOR ***"
			Exit
		Endif
		
		If nLin > 64
			Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
			nLin := 9
		Endif 

		DbSelectArea("SC6")
		DbSetOrder(1)
		If DbSeek(xFilial("SC6")+TRB->ZJ_PEDIDO)
			While SC6->(!EOF()) .And. SC6->C6_NUM = TRB->ZJ_PEDIDO
				nQtdeC6 += SC6->C6_QTDVEN
				nVlrC6  += SC6->C6_VALOR
				SC6->(DbSkip())
			EndDo
		EndIf
		nPercTot := (TRB->ZJ_QTDLIB / nQtdeC6) * 100  

		@ nLin, aCol[1]  PSay TRB->ZJ_NUMPF
		@ nLin, aCol[2]  PSay TRB->ZJ_PEDIDO
		@ nLin, aCol[3]  PSay Transform(nQtdeC6,"@E 999,999")
		@ nLin, aCol[4]  PSay Transform(nVlrC6,"@E 9,999,999.99")
		@ nLin, aCol[5]  PSay Transform(TRB->ZJ_QTDLIB,"@E 999,999")
		@ nLin, aCol[6]  PSay Transform(TRB->VALORPF,"@E 9,999,999.99")
		@ nLin, aCol[7]  PSay Transform(nPercTot,"@E 999.9")+"%"
		@ nLin, aCol[8]  PSay TRB->ZJ_CLIENTE+"/"+TRB->ZJ_LOJA+" - "+SubStr(TRB->A1_NOME,1,50)
		@ nLin, aCol[9]  PSay TRB->A1_EST
		                                                                                    
		RecLock("TEMP1",.T.)
		TEMP1->NUMPF	  := TRB->ZJ_NUMPF
		TEMP1->PEDIDO	  := TRB->ZJ_PEDIDO
		TEMP1->QTDEPEDIDO := nQtdeC6
		TEMP1->VLRPEDIDO  := nVlrC6
		TEMP1->QTDEPREFAT := TRB->ZJ_QTDLIB
		TEMP1->VLRPREFAT  := TRB->VALORPF
		TEMP1->PERCATEND  := nPercTot
		TEMP1->CLIENTE 	  := TRB->ZJ_CLIENTE+"/"+TRB->ZJ_LOJA+" - "+TRB->A1_NOME
		TEMP1->ESTADO 	  := TRB->A1_EST
		MsUnlock()
		
		nLin++
		nTotQPed += nQtdeC6
		nTotVPed += nVlrC6
		nTotQPF  += TRB->ZJ_QTDLIB
		nTotVPF  += TRB->VALORPF
		nCont++
		
		nQtdeC6 := 0
		nVlrC6  := 0
		
		TRB->(DbSkip())
	EndDo

	@ nLin,000 PSay __PrtThinLine()
	nLin++
	@ nLin, aCol[1]  PSay "Qtd. Pedidos: "+AllTrim(Str(nCont))
	@ nLin, aCol[3]  PSay Transform(nTotQPed,"@E 999,999")
	@ nLin, aCol[4]  PSay Transform(nTotVPed,"@E 9,999,999.99")
	@ nLin, aCol[5]  PSay Transform(nTotQPF,"@E 999,999")
	@ nLin, aCol[6]  PSay Transform(nTotVPF,"@E 9,999,999.99")
	@ nLin, aCol[7]  PSay Transform((nTotQPF/nTotQPed)*100,"@E 999")+"%"
		
EndIf

TRB->(DbCloseArea())                                  

Return

/*���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Funcao    �AjustaSX1 � Autor � Bruno Parreira     � Data �  16/06/17   ���
�������������������������������������������������������������������������͹��
���Descricao � Cria parametros da rotina                                  ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(	cPerg, "01","Lote DAP ? ","","","Mv_ch1",TAMSX3("ZJ_LOTDAP")[3],TAMSX3("ZJ_LOTDAP")[1],TAMSX3("ZJ_LOTDAP")[2],0,"G","","","","N","Mv_par01","","","","","","","", "","","", "","","","","","",{"Digite o lote DAP.",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)