#Include "RwMake.ch"
#Include "Protheus.ch"
#Include "Topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA006  �Autor  �Bruno Parreira      � Data �  20/06/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Tela de controle do DAP.                                    ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATA006()
Local cPerg := "HFATA006"   
local nX
Local nI
Local aCombo := {"Lote DAP","Pre-Faturamento","Codigo Cliente","Nome Cliente","Pedido","Nota Fiscal","Pedido Millenium"}
Local cCombo
Private oDlg
Private oGetDados
Private cPesqChav := Space(50)
Private oPesqChav 
Private nOpc      := 0
Private nStr      := 0 
Private nPos      := 0    
Private aColAux   := {}
Private lRefresh  := .T.
Private aHeader   := {}
Private aCols     := {} 
Private aFields   := {}    
Private cArqTMP   
Private oButton1
Private oButton2
Private oButton3
Private oButton4 
Private oButton5 
Private oButton6  
Private oButton7  
Private oButton8  
Private nCol := 0
Private cLegenda := ""
Private nTamanho := 0
Private cTipos   := ""
Private nTotQtd := 0
Private nTotVlr := 0

AJUSTASX1(cPerg)

If !Pergunte(cPerg)
	Return
EndIf

cMvPar06 := AllTrim(mv_par06) 

If !Empty(cMvPar06)
	For nx := 1 to len(cMvPar06) step 2
		cAux := SubStr(cMvPar06,nx,2)
		If cAux <> '**'
			cTipos += "'0"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

Processa( {|| MontaArray() }, "Aguarde...", "Carregando campos...",.F.)

aFields := {"LEGENDA","ZJ_LOTDAP","ZJ_NUMPF","ZJ_QTDLIB","ZJ_QTDSEP","ZJ_CLIENTE","ZJ_LOJA","A1_NOME","ZJ_DATA","C5_NUM","C5_EMISSAO","C5_FECENT","ZJ_DOC","ZJ_SERIE","F2_EMISSAO","C5_XPEDMIL","D3_USUARIO"}

aAlterFields := {}

Aadd(aHeader, {" ","ZJ_LOTDAP","@BMP", 2, 0,".F.","","C","","V"})

DbSelectArea("SX3")
SX3->(DbSetOrder(2))
For nX := 2 to Len(aFields)
	If SX3->(DbSeek(aFields[nX]))
		nTamanho := SX3->X3_TAMANHO
		Do Case     
			Case aFields[nX] = "ZJ_QTDLIB"
				cCampo := "Qtde Pr�-Fat"
			Case aFields[nX] = "ZJ_QTDSEP"
				cCampo := "Qtde Conf."
			Case aFields[nX] = "ZJ_CLIENTE"
				cCampo := "Cliente"
			Case aFields[nX] = "ZJ_DATA"
				cCampo := "Dt Pre-Fat"
			Case aFields[nX] = "C5_PEDIDO"
				cCampo := "Pedido"
			Case aFields[nX] = "ZJ_DOC"
				cCampo := "Nota Fiscal"
			Case aFields[nX] = "F2_EMISSAO"
				cCampo := "Dt Emis NF"
			Case aFields[nX] = "D3_USUARIO"
				cCampo := "Ult. Conferente"
			Otherwise 
	    		cCampo := Trim(X3Titulo())       
		EndCase
		Aadd(aHeader,{cCampo,;
		              SX3->X3_CAMPO,;                      
		              SX3->X3_PICTURE,;                      
		              nTamanho,;                      
		              SX3->X3_DECIMAL,;                      
		              SX3->X3_VALID,;                      
		              "",;                      
		              SX3->X3_TIPO,;                      
		              "",;                      
		              "" })
	EndIf
Next nX

Aadd(aCols,Array(Len(aHeader)+1))

For nI := 1 To Len(aHeader)
	If aHeader[nI][2] = "ZJ_LOTDAP"
		aCols[1][nI] := "000001"
	EndIf
    aCols[1][nI] := CriaVar(aHeader[nI][2])
Next nI

aCols[1][Len(aHeader)+1] := .F.

DEFINE MSDIALOG oDlg TITLE "Controle DAP" FROM 00,00 TO 540,1300 PIXEL  
          
@ 007, 005 Say "Pesquisa por:" Size 050,010 COLOR CLR_BLACK PIXEL OF oDlg
@ 005, 045 ComboBox cCombo Items aCombo         SIZE 080, 010 PIXEL OF oDlg 	                                                                                                                                    
@ 005, 130 MSGET  oPesqChv  Var cPesqChav       SIZE 120, 010 COLOR CLR_BLACK Picture "@!" PIXEL OF oDlg
@ 005, 255 BUTTON oButton1  PROMPT "Pesquisar"  SIZE 040, 012 OF oDlg ACTION PESQUISA(cCombo)  PIXEL MESSAGE "Pesquisar"     
@ 252, 005 BUTTON oButton3  PROMPT "Legenda"    SIZE 040, 015 OF oDlg ACTION LEGENDAS() PIXEL MESSAGE "Legenda"
//@ 252, 050 BUTTON oButton2  PROMPT "Exp. Excel" SIZE 040, 015 OF oDlg ACTION MsgInfo("Exportar Excel") PIXEL MESSAGE "Exportar para Excel"
//@ 252, 095 BUTTON oButton4  PROMPT "Botao 4"    SIZE 040, 015 OF oDlg ACTION MsgInfo("Botao 4") PIXEL MESSAGE "Bot�o 4"
//@ 252, 140 BUTTON oButton5  PROMPT "Botao 5"    SIZE 040, 015 OF oDlg ACTION APCPREM10(@oGetDados:nAt) PIXEL MESSAGE "Consulta Remessa para Oficina"
//@ 252, 185 BUTTON oButton6  PROMPT "Ret. Ofic." SIZE 040, 015 OF oDlg ACTION APCPRET10(@oGetDados:nAt) PIXEL MESSAGE "Consulta Retorno de Oficina"
//@ 252, 230 BUTTON oButton7  PROMPT "Observa��o" SIZE 040, 015 OF oDlg ACTION APCPOBS10(@oGetDados:nAt) PIXEL MESSAGE "Consulta Observa��es do Peddido de Venda"
//@ 252, 275 BUTTON oButton7  PROMPT "NF Sa�da"   SIZE 040, 015 OF oDlg ACTION APCPNFS10(@oGetDados:nAt) PIXEL MESSAGE "Consulta Nota Fiscal de Sa�da"
@ 254, 200 Say "Total Qtde:" Size 050,010 COLOR CLR_BLACK PIXEL OF oDlg 	                                                                                                                                    
@ 252, 250 MSGET  oTotQtd  Var nTotQtd          SIZE 60, 010 COLOR CLR_BLACK Picture "@!" WHEN .F. PIXEL OF oDlg
//@ 254, 370 Say "Total Valor:" Size 050,010 COLOR CLR_BLACK PIXEL OF oDlg 	                                                                                                                                    
//@ 252, 490 MSGET  oTotVlr  Var nTotVlr          SIZE 120, 010 COLOR CLR_BLACK Picture "@!" PIXEL OF oDlg
@ 252, 605 BUTTON oButton8  PROMPT "Sair"       SIZE 040, 015 OF oDlg ACTION (Iif(SAIRTELA(),oDlg:End(),)) PIXEL MESSAGE "Sair"


oGetDados := MsNewGetDados():New(022,001,248,653,GD_UPDATE,"AllwaysTrue","AllwaysTrue","",aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue",oDlg, aHeader, aColAux)

oGetDados:aCols=aColAux 

N := oGetDados:nAt       

oPesqChv:SetFocus() // forca o foco nesta variavel

oGetDados:oBrowse:bHeaderClick := {|oGetDados,nCol| Ordena(nCol)} 

ACTIVATE MSDIALOG oDlg CENTERED 

//TMP->(DbCloseArea())
TRB->(DbCloseArea())

//EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �PESQUISA  �Autor  �Bruno Parreira      � Data �  27/06/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Realiza pesquisa no aCols da MsNewGetDados.                 ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/        

Static Function PESQUISA(cCombo)     
Local aArea := GetArea()
Local nColuna := 0       

//{"Lote DAP","Pedido","Codigo Cliente","Nome Cliente","Nota Fiscal"}
//{"LEGENDA","ZJ_LOTDAP","ZJ_NUMPF","ZJ_QTDLIB","ZJ_QTDSEP","ZJ_CLIENTE","ZJ_LOJA","A1_NOME","ZJ_DATA","C5_NUM","C5_EMISSAO","C5_FECENT","ZJ_DOC","ZJ_SERIE","F2_EMISSAO"}
Do Case
	Case cCombo = "Lote DAP"
		nColuna := 2
	Case cCombo = "Pre-Faturamento"
		nColuna := 3
	Case cCombo = "Codigo Cliente"
		nColuna := 6
	Case cCombo = "Nome Cliente"
		nColuna := 8
	Case cCombo = "Pedido"
		nColuna := 10	
	Case cCombo = "Nota Fiscal"
		nColuna := 13	
EndCase
      
If !Empty(AllTrim(cPesqChav))                                      
	nStr := Len(AllTrim(cPesqChav))
	nPos := Ascan(oGetDados:aCols,{|x| SubStr(x[nColuna],1,nStr) = UPPER(AllTrim(cPesqChav))})  //X[nColuna] - Coluna do aCols
	If nPos > 0
		n:= nPos
  		oGetDados:OBROWSE:NAT := nPos
    	oGetDados:OBROWSE:Refresh()
     	oGetDados:Refresh()
  	Endif
EndIf	  	
             
RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �ORDENA    �Autor  �Bruno Parreira      � Data �  27/06/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Ordenar aCols na MsNewGetDados.                             ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������          
�����������������������������������������������������������������������������
*/

Static Function ORDENA(nCol) 

If nCol = 4 .Or. nCol = 5  						// Campos Numericos
	aColAux := aSort(aColAux,,,{|x,y| AllTrim(Str(x[nCol]))+x[2]+x[3] < AllTrim(Str(y[nCol]))+y[2]+y[3]})
ElseIf nCol = 9 .Or. nCol = 11 .Or. nCol = 12 	//Campos Data
 	aColAux := aSort(aColAux,,,{|x,y| DtoS(x[nCol])+x[2]+x[3] < DtoS(y[nCol])+y[2]+y[3]})
Else  											//Restante
 	aColAux := aSort(aColAux,,,{|x,y| x[nCol]+x[2]+x[3] < y[nCol]+y[2]+y[3]})
EndIf 
oGetDados:setArray(aColAux) 
oGetDados:oBrowse:Refresh() 

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �SAIRTELA  �Autor  �Bruno Parreira      � Data �  27/06/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Pergunta para confirmacao de saida da tela de controle do   ���
���          �DAP.                                                        ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/    

Static Function SAIRTELA()
Local lRet := .F.

If MsgYesNo("Deseja realmente sair?","Controle do DAP")
 	lRet := .T.
EndIf

Return lRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �LEGENDAS  �Autor  �Bruno Parreira      � Data �  27/06/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Legenda da tela de controle do DAP.                         ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LEGENDAS()

Local aCores := {}
                                                                   
//BR_AMARELO,BR_AZUL,BR_BRANCO,BR_CINZA,BR_LARANJA,BR_MARROM,BR_VERDE,BR_VERMELHO,BR_PINK,BR_PRETO,BR_VIOLETA

aAdd(aCores,{"BR_VERDE"   ,"DAP Gerado"})
aAdd(aCores,{"BR_AZUL"    ,"Liberado para Separa��o"})
aAdd(aCores,{"BR_AMARELO" ,"Em Confer�ncia"})
aAdd(aCores,{"BR_LARANJA" ,"Conferido"})
aAdd(aCores,{"BR_VERMELHO","Faturado"})

BrwLegenda("Controle do DAP","Legenda",aCores)

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MontaArray�Autor  �Bruno Parreira      � Data �  27/06/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Monta a tela de controle do DAP.                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function MontaArray()
Local cQuery   := ""   

Private oDlg                                   
Private oGroup1             
Private oButton1
Private oButton2                               
Private oTitulo
Private cTitulo  := "Controle DAP" 
Private oFont1   := TFont():New("Times New Roman",,020,,.T.,,,,,.F.,.F.)        

// ************************ Monta Estrutura do Temporario Excel
/*_aStruct := {} 
aAdd(_aStruct,{"FILIAL"    ,"C",15                      ,0})	
aAdd(_aStruct,{"EMISSAO_PV","D",TAMSX3("C5_EMISSAO")[1],TAMSX3("C5_EMISSAO")[2]})	
aAdd(_aStruct,{"NUMERO_PV" ,"C",TAMSX3("C5_NUM"    )[1],TAMSX3("C5_NUM"    )[2]})	
aAdd(_aStruct,{"ITEM_PV"   ,"C",TAMSX3("C6_ITEM"   )[1],TAMSX3("C6_ITEM"   )[2]})	
aAdd(_aStruct,{"DT_ENT_PV" ,"D",TAMSX3("C6_ENTREG" )[1],TAMSX3("C6_ENTREG" )[2]})	
aAdd(_aStruct,{"COD_CLIENT","C",TAMSX3("A1_COD"    )[1],TAMSX3("A1_COD"    )[2]})	
aAdd(_aStruct,{"LOJA"      ,"C",TAMSX3("A1_LOJA"   )[1],TAMSX3("A1_LOJA"   )[2]})	
aAdd(_aStruct,{"NOME"      ,"C",TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2]})	
aAdd(_aStruct,{"PED_CLIENT","C",TAMSX3("C6_PEDCLI" )[1],TAMSX3("C6_PEDCLI" )[2]})	
aAdd(_aStruct,{"PRODUTO"   ,"C",TAMSX3("C6_PRODUTO")[1],TAMSX3("C6_PRODUTO")[2]})	
aAdd(_aStruct,{"DESCRICAO" ,"C",TAMSX3("B1_DESC"   )[1],TAMSX3("B1_DESC"   )[2]})	
aAdd(_aStruct,{"QTDE_VENDA","N",TAMSX3("C6_QTDVEN" )[1],TAMSX3("C6_QTDVEN" )[2]})	
aAdd(_aStruct,{"NUM_OP"    ,"C",TAMSX3("C6_XNUMOP" )[1],TAMSX3("C6_XNUMOP" )[2]})	
aAdd(_aStruct,{"QTDE_OP"   ,"N",TAMSX3("C2_QUANT"  )[1],TAMSX3("C2_QUANT"  )[2]})	
aAdd(_aStruct,{"EMISSAO_OP","D",TAMSX3("C2_EMISSAO")[1],TAMSX3("C2_EMISSAO")[2]})	
aAdd(_aStruct,{"PRV_ENT_OP","D",TAMSX3("C2_DATPRF" )[1],TAMSX3("C2_DATPRF" )[2]})	
aAdd(_aStruct,{"ULT_OPERAC","C",TAMSX3("H6_OPERAC" )[1],TAMSX3("H6_OPERAC" )[2]})	
aAdd(_aStruct,{"OPERACAO"  ,"C",TAMSX3("G2_DESCRI" )[1],TAMSX3("G2_DESCRI" )[2]})	
aAdd(_aStruct,{"COD_OFIC"  ,"C",TAMSX3("A2_COD"    )[1],TAMSX3("A2_COD"    )[2]})	
aAdd(_aStruct,{"LOJA_OFIC" ,"C",TAMSX3("A2_LOJA"   )[1],TAMSX3("A2_LOJA"   )[2]})	
aAdd(_aStruct,{"OFICINA"   ,"C",TAMSX3("A2_NOME"   )[1],TAMSX3("A2_NOME"   )[2]})	
aAdd(_aStruct,{"NF_ENV_OFI","C",12                     ,0                       })
aAdd(_aStruct,{"PRV_RET_OF","D",TAMSX3("H6_XDPRVOF")[1],TAMSX3("H6_XDPRVOF")[2]})
aAdd(_aStruct,{"QTD_ENV_OF","N",TAMSX3("H6_QTDPROD")[1],TAMSX3("H6_QTDPROD")[2]})
aAdd(_aStruct,{"NF_RET_OFI","C",12                     ,0                       })
aAdd(_aStruct,{"QTD_RET_OF","N",TAMSX3("H6_QTDPRO2")[1],TAMSX3("H6_QTDPRO2")[2]})
aAdd(_aStruct,{"QTD_PRODUZ","N",TAMSX3("C2_QUJE"   )[1],TAMSX3("C2_QUJE"   )[2]})	
aAdd(_aStruct,{"NF_FATURAD","C",12                     ,0                       })
aAdd(_aStruct,{"EMISSAO_NF","D",TAMSX3("D2_EMISSAO")[1],TAMSX3("D2_EMISSAO")[2]})	
aAdd(_aStruct,{"PRC_VENDA" ,"N",TAMSX3("C6_PRCVEN" )[1],TAMSX3("C6_PRCVEN" )[2]})	
aAdd(_aStruct,{"TOTAL"     ,"N",TAMSX3("C6_VALOR"  )[1],TAMSX3("C6_VALOR"  )[2]})	

cArqTMP	:= CriaTrab(_aStruct)   

dbUseArea( .T.,, cArqTmp, "TMP", if(.F. .OR. .F., !.F., NIL), .F. )          //__LocalDriver*/

/*Aadd(aBox,"Todos")
Aadd(aBox,"DAP Gerado")
Aadd(aBox,"Liberado para Confer�ncia")
Aadd(aBox,"Em Confer�ncia")
Aadd(aBox,"Conferido")
Aadd(aBox,"Faturado")*/
cQuery :=        "select * from ( "
cQuery += CRLF + "Select ZJ_LOTDAP,ZJ_NUMPF,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,ZJ_DATA,ZJ_CONF,ZJ_DOC,ZJ_SERIE,C5_NUM,C5_EMISSAO,C5_FECENT,C5_XPEDMIL,SUM(ZJ_QTDLIB) AS ZJ_QTDLIB,SUM(ZJ_QTDSEP) AS ZJ_QTDSEP, "
cQuery += CRLF + "(select TOP 1 ZR_USR from SZR010 SZR where ZR_NUMPF = ZJ_NUMPF and D_E_L_E_T_ = '' order by ZR_DATA,ZR_HORA DESC) AS ZR_USR "
cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ "
cQuery += CRLF + "inner join "+RetSqlName("SC5")+" SC5 "
cQuery += CRLF + "on C5_NUM = ZJ_PEDIDO "
cQuery += CRLF + "and SC5.D_E_L_E_T_ = '' "
cQuery += CRLF + "and C5_NUM between '"+mv_par01+"' and '"+mv_par02+"' "
cQuery += CRLF + "and C5_FECENT between '"+DtoS(mv_par03)+"' and '"+DtoS(mv_par04)+"' "
cQuery += CRLF + "and C5_CLIENTE between '"+mv_par07+"' and '"+mv_par08+"' "
cQuery += CRLF + "and C5_LOJACLI between '"+mv_par09+"' and '"+mv_par10+"' "
cQuery += CRLF + "and C5_EMISSAO between '"+DtoS(mv_par13)+"' and '"+DtoS(mv_par14)+"' "
If !Empty(cTipos)
	cQuery += CRLF + "and C5_TPPED IN ("+cTipos+") "
EndIf
cQuery += CRLF + "inner join "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "on A1_COD = ZJ_CLIENTE "
cQuery += CRLF + "and A1_LOJA = ZJ_LOJA "
cQuery += CRLF + "and SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where SZJ.D_E_L_E_T_ = ''  "
cQuery += CRLF + "and ZJ_LOTDAP between '"+mv_par11+"' and '"+mv_par12+"' "
cQuery += CRLF + "group by ZJ_LOTDAP,ZJ_NUMPF,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,ZJ_DATA,ZJ_CONF,ZJ_DOC,ZJ_SERIE,C5_NUM,C5_EMISSAO,C5_FECENT,C5_XPEDMIL ) Z "
If mv_par05 = '02'
	cQuery += CRLF + "where ZJ_DOC = '' and ZJ_CONF = '' and ZJ_QTDSEP = 0 " //Incluir campo Lib. Separacao
ElseIf mv_par05 = '03'
	cQuery += CRLF + "where ZJ_DOC = '' and ZJ_CONF = 'L' and ZJ_QTDSEP = 0 " //Incluir campo Lib. Separacao
ElseIf mv_par05 = '04'
	cQuery += CRLF + "where ZJ_DOC = '' and ZJ_CONF = 'L' and ZJ_QTDSEP > 0 "
ElseIf mv_par05 = '05'	
	cQuery += CRLF + "where ZJ_DOC = '' and ZJ_CONF = 'S' and ZJ_QTDSEP > 0 "
ElseIf mv_par05 = '06'	
	cQuery += CRLF + "where ZJ_DOC <> '' and ZJ_CONF = 'S' and ZJ_QTDSEP > 0 "			
EndIf
cQuery += CRLF + "order by ZJ_LOTDAP,ZJ_NUMPF " 
      
MemoWrite("HFATA006.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)  

DbSelectArea("TRB")

nCountReg := 0
DbEval({|| nCountReg++})
TRB->(DbGoTop())
ProcRegua(nCountReg)

If TRB->(!EOF())
	While TRB->(!EOF()) 
		IncProc()
		
		Do Case                         
			Case !Empty(TRB->ZJ_DOC)							//Faturado
				cLegenda := "BR_VERMELHO"
			Case TRB->ZJ_CONF = "S" .And. Empty(TRB->ZJ_DOC) 	//Conferido
				cLegenda := "BR_LARANJA"
			Case TRB->ZJ_QTDSEP > 0 .And. TRB->ZJ_CONF = "L"	//Em Conferencia
				cLegenda := "BR_AMARELO"
			Case TRB->ZJ_QTDSEP = 0	.And. TRB->ZJ_CONF = "L"	//Liberado para Conferencia  //Verificar e criar campo para este apontamento.
				cLegenda := "BR_AZUL"
			Case Empty(TRB->ZJ_DOC)								//DAP Gerado
				cLegenda := "BR_VERDE"
			/*Case !Empty(TRB->C2_NUM) .And. Empty(TRB->H6_OPERAC)
				cLegenda := "BR_AZUL"
			Case cOperac = "CORTE"
				cLegenda := "BR_AMARELO"
			Case cOperac = "SILK"
				cLegenda := "BR_CINZA"	
			Case cOperac = "ACABAMENTO CORTE" .Or. cOperac = "ACABAMENTO DE CORTE"
				cLegenda := "BR_LARANJA"
			Case cOperac = "COSTURA"
				cLegenda := "BR_MARROM"
			Case cOperac = "RECEBIMENTO" .Or. cOperac = "CONTAGEM"
				cLegenda := "BR_PRETO"
			Case cOperac = "REVISAO"
				cLegenda := "BR_BRANCO"*/
			Otherwise
				cLegenda := ""	 
		EndCase
		
		//aFields := {"LEGENDA","ZJ_LOTDAP","ZJ_NUMPF","ZJ_QTDLIB","ZJ_QTDSEP","ZJ_CLIENTE","ZJ_LOJA","A1_NOME","ZJ_DATA","C5_NUM","C5_EMISSAO"}
	    
	    cLot := TRB->ZJ_LOTDAP			//Lote DAP
	    cPF  := TRB->ZJ_NUMPF			//Pre-faturamento
	    nQtL := TRB->ZJ_QTDLIB			//Qtd Liberada
	    nQtS := TRB->ZJ_QTDSEP			//Qtd Conferida
	    cCli := TRB->ZJ_CLIENTE			//Cliente
	    cLoj := TRB->ZJ_LOJA			//Loja
	    cNom := TRB->A1_NOME			//Nome
	    dDat := StoD(TRB->ZJ_DATA)		//Data Pre-Faturamento
	    cPed := TRB->C5_NUM				//Pedido
	    dEmi := StoD(TRB->C5_EMISSAO)	//Dt Emissao Pedido
	    dEnt := StoD(TRB->C5_FECENT)	//Dt Entrega Pedido
	    cDoc := TRB->ZJ_DOC				//Nota Fiscal
	    cSer := TRB->ZJ_SERIE			//Serie
	    dNF  := GetAdvFVal("SF2","F2_EMISSAO",xFilial("SF2")+TRB->ZJ_DOC+TRB->ZJ_SERIE+TRB->ZJ_CLIENTE+TRB->ZJ_LOJA,1,"")	//Dt Emissao NF
	    cMil := TRB->C5_XPEDMIL
	    cUsr := UsrRetName(TRB->ZR_USR)
	    
	    aAdd(aColAux,{cLegenda,cLot,cPF,nQtL,nQtS,cCli,cLoj,cNom,dDat,cPed,dEmi,dEnt,cDoc,cSer,dNF,cMil,cUsr,.F.})
	    
	    nTotQtd += nQtL
	    
	    //aAdd(aColAux,{cPedido,cItem,.F.})
	    
	    /*RecLock("TMP",.T.)
	    TMP->FILIAL     := cFilOrg
 		TMP->EMISSAO_PV := CtoD(cDtEmis)
 		TMP->NUMERO_PV  := cPedido
 		TMP->ITEM_PV    := cItem
 		TMP->DT_ENT_PV  := CtoD(cDtEntr)
 		TMP->COD_CLIENT := cClient
 		TMP->LOJA       := cLoja
 		TMP->NOME       := cNome
 		TMP->PED_CLIENT := cPedCli
 		TMP->PRODUTO    := cProd
 		TMP->DESCRICAO  := cDesc
 		TMP->QTDE_VENDA := nQtdVen
 		TMP->NUM_OP     := cNumOP
 		TMP->QTDE_OP    := nQtdOP
 		TMP->EMISSAO_OP := CtoD(cDtEmOP)
 		TMP->PRV_ENT_OP := CtoD(cDtEtOP)
 		TMP->ULT_OPERAC := cUltOpr
 		TMP->OPERACAO   := cOperac
 		TMP->COD_OFIC   := cCodFor
 		TMP->LOJA_OFIC  := cLojFor
 		TMP->OFICINA    := cNomFor
 		TMP->NF_ENV_OFI := cNfOfic
 		TMP->PRV_RET_OF := CtoD(cNfDtOf)
 		TMP->QTD_ENV_OF := nQtdEnv
 		TMP->NF_RET_OFI := cNfRetOf
 		TMP->QTD_RET_OF := nQtdRec      
 		TMP->QTD_PRODUZ := nQtdAOP 
 		TMP->NF_FATURAD := cNfFim  
 		TMP->EMISSAO_NF := CtoD(cEmisNF)
 		TMP->PRC_VENDA  := nPrcVen
 		TMP->TOTAL      := nTotal
 		MsUnlock()*/
		
		TRB->(DbSkip())	
    EndDo                
    
Else
	MsgAlert("Os par�metros n�o retornaram resultado.")    
	Return
EndIf    

aColsOri := aClone(aColAux)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

//PutSx1(cPerg,"01","Status dos Pedidos   ","","","Mv_ch1","N"                   ,                     1,                     0,2,"C",           "",   "","","N","Mv_par01",   "DAP Gerado","","","","Lib. Separa��o","","","Conferindo","","","Conferido","","","Faturado","","","","","","","","","",{"Selecione o status do pedido.",""},{""},{""},"")
PutSx1(cPerg,"01","Pedido de            ","","","Mv_ch1",TAMSX3("C6_NUM"    )[3],TAMSX3("C6_NUM"    )[1],TAMSX3("C6_NUM"    )[2],0,"G",           "","SC5","","N","Mv_par01",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o pedido incial.",""},{""},{""},"")
PutSx1(cPerg,"02","Pedido ate           ","","","Mv_ch2",TAMSX3("C6_NUM"    )[3],TAMSX3("C6_NUM"    )[1],TAMSX3("C6_NUM"    )[2],0,"G",           "","SC5","","N","Mv_par02",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o pedido final.",""},{""},{""},"")
PutSx1(cPerg,"03","Data Entrega de      ","","","Mv_ch3",TAMSX3("C6_ENTREG" )[3],TAMSX3("C6_ENTREG" )[1],TAMSX3("C6_ENTREG" )[2],0,"G",           "",   "","","N","Mv_par03",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Data de entrega inicial.",""},{""},{""},"")
PutSx1(cPerg,"04","Data Entrega ate     ","","","Mv_ch4",TAMSX3("C6_ENTREG" )[3],TAMSX3("C6_ENTREG" )[1],TAMSX3("C6_ENTREG" )[2],0,"G",           "",   "","","N","Mv_par04",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Data de entrega final.",""},{""},{""},"")
PutSx1(cPerg,"05","Status dos Pedidos   ","","","Mv_ch5","C"                   ,                    20,                     0,2,"G","U_HF006ST()",   "","","N","Mv_par05",               "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o status do pedido.",""},{""},{""},"")
PutSx1(cPerg,"06","Tipo dos Pedidos     ","","","Mv_ch6","C"                   ,                    99,                     0,0,"G","U_HF002TP()",   "","","N","Mv_par06",               "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione os tipos de pedido.",""},{""},{""},"")
PutSx1(cPerg,"07","Cliente de           ","","","Mv_ch7",TAMSX3("C6_CLI"    )[3],TAMSX3("C6_CLI"    )[1],TAMSX3("C6_CLI"    )[2],0,"G",           "","SA1","","N","Mv_par07",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o cliente incial.",""},{""},{""},"")
PutSx1(cPerg,"08","Cliente ate          ","","","Mv_ch8",TAMSX3("C6_CLI"    )[3],TAMSX3("C6_CLI"    )[1],TAMSX3("C6_CLI"    )[2],0,"G",           "","SA1","","N","Mv_par08",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o cliente final.",""},{""},{""},"")
PutSx1(cPerg,"09","Loja de              ","","","Mv_ch9",TAMSX3("C6_LOJA"   )[3],TAMSX3("C6_LOJA"   )[1],TAMSX3("C6_LOJA"   )[2],0,"G",           "",   "","","N","Mv_par09",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o loja incial.",""},{""},{""},"")
PutSx1(cPerg,"10","Loja ate             ","","","Mv_chA",TAMSX3("C6_LOJA"   )[3],TAMSX3("C6_LOJA"   )[1],TAMSX3("C6_LOJA"   )[2],0,"G",           "",   "","","N","Mv_par10",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o loja final.",""},{""},{""},"")
PutSx1(cPerg,"11","Lote DAP de          ","","","Mv_chB",TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2],0,"G",           "",   "","","N","Mv_par11",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o lote DAP incial.",""},{""},{""},"")
PutSx1(cPerg,"12","Lote DAP ate         ","","","Mv_chC",TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2],0,"G",           "",   "","","N","Mv_par12",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Informe o lote DAP final.",""},{""},{""},"")
PutSx1(cPerg,"13","Data Emissao de      ","","","Mv_chD",TAMSX3("C5_EMISSAO")[3],TAMSX3("C5_EMISSAO")[1],TAMSX3("C5_EMISSAO")[2],0,"G",           "",   "","","N","Mv_par13",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Data de emissao incial.",""},{""},{""},"")
PutSx1(cPerg,"14","Data Emissao ate     ","","","Mv_chE",TAMSX3("C5_EMISSAO")[3],TAMSX3("C5_EMISSAO")[1],TAMSX3("C5_EMISSAO")[2],0,"G",           "",   "","","N","Mv_par14",             "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Data de emissao final.",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HF006ST   �Autor  �Bruno Parreira      � Data �  11/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria tela de selecao de tipos de pedidos.                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HF006ST(l1Elem,lTipoRet)

Local MvPar
Local aArea  := GetArea()
Local cTitulo := ""          
Local lRet  := .T.
Local aBox  := {}                      
Local nTam  := 2
Local MvParDef := ""

MvPar:=&(Alltrim(ReadVar())) // Carrega Nome da Variavel do Get em Questao
mvRet:=Alltrim(ReadVar())    // Iguala Nome da Variavel ao Nome variavel de Retorno

cTitulo := "Status do Pedido"
Aadd(aBox,"Todos")
Aadd(aBox,"DAP Gerado")
Aadd(aBox,"Liberado para Confer�ncia")
Aadd(aBox,"Em Confer�ncia")
Aadd(aBox,"Conferido")
Aadd(aBox,"Faturado")
MvParDef := "010203040506"
/*While !Eof()
   Aadd(aBox,SZ1->Z1_CODIGO + " - " + Alltrim(SZ1->Z1_DESC))
   MvParDef += SubStr(SZ1->Z1_CODIGO,2,2)
   dbSkip()
EndDo*/

If f_Opcoes( @MvPar,;  // uVarRet
      cTitulo,;  // cTitulo
      @aBox,;   // aOpcoes
      MvParDef,;  // cOpcoes
      ,;    // nLin1
      ,;    // nCol1
      .T.,;    // l1Elem
      nTam,;    // nTam
      Len(aBox),; // nElemRet
      .F.,;    // lMultSelect
      ,;    // lComboBox
      ,;    // cCampo
      ,;    // lNotOrdena
      ,;    // NotPesq
      ,;   // ForceRetArr
       )    // F3
       
  	&MvRet := mvpar                                                                          // Devolve Resultado
EndIF

RestArea(aArea)

Return lRet   