#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL006 
Relatório de Necessidade  
@author Weskley Silva
@since 31/10/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL006()

	Private oReport

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 31 de Outubro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'MRP', 'RELATORIO NECESSIDADE ', , {|oReport| ReportPrint( oReport ), 'RELATORIO NECESSIDADE' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO NECESSIDADE', { 'MRP', 'CZJ','CZK','SB4'})
			
	TRCell():New( oSection1, 'MRP'		    	    ,'MRP', 		'MRP',								"@!"                        ,10)
	TRCell():New( oSection1, 'CODIGO'      	        ,'MRP', 		'CODIGO',	    					"@!"				        ,15)
	TRCell():New( oSection1, 'SKU'          		,'MRP', 		'SKU',              				"@!"						,20)
	TRCell():New( oSection1, 'DESCRICAO'         	,'MRP', 		'DESCRICAO',	              		"@!"						,25)
	TRCell():New( oSection1, 'PERIODO' 				,'MRP', 		'PERIODO' ,	        				"@!"		                ,04)
	TRCell():New( oSection1, 'PREVISAO'		       	,'MRP', 		'PREVISAO',          				"@E 999.999,99"		        ,15)
	TRCell():New( oSection1, 'ENTRADA' 				,'MRP', 		'ENTRADA' ,	    					"@E 999.999,99"		        ,15)
	TRCell():New( oSection1, 'SAIDA'	        	,'MRP', 		'SAIDA' ,		       				"@E 999.999,99"	            ,15)
	TRCell():New( oSection1, 'ESTOQUE'			    ,'MRP', 		'ESTOQUE',			   				"@E 999.999,99"				,15)
	TRCell():New( oSection1, 'NECESSIDADE'			,'MRP', 		'NECESSIDADE' ,	    				"@E 999.999,99"	            ,15)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 31 de Outubro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("MRP") > 0
		MRP->(dbCloseArea())
	Endif
      
	cQuery := " SELECT CZJ_NRMRP AS MRP, LEFT(CZJ_PROD,8) AS CODIGO, CZJ_PROD AS SKU, B4_DESC AS DESCRICAO, CZK_PERMRP AS PERIODO, " 
	cQuery += " CZK_QTSLES AS PREVISAO, CZK_QTENTR AS ENTRADA, CZK_QTSAID AS SAIDA, CZK_QTSEST AS ESTOQUE, CZK_QTNECE AS NECESSIDADE " 
	cQuery += " FROM "+RetSqlName("CZJ")+" CZJ (NOLOCK) " 
	cQuery += " INNER JOIN "+RetSqlName("CZK")+" CZK (NOLOCK) ON CZK.D_E_L_E_T_ = '' AND CZK_NRMRP = CZJ_NRMRP AND CZK_RGCZJ = CZJ.R_E_C_N_O_ " 
	cQuery += " INNER JOIN "+RetSqlName("SB4")+" SB4 (NOLOCK) ON SB4.D_E_L_E_T_ = '' AND B4_COD = LEFT(CZJ_PROD,8) " 
	cQuery += " WHERE CZJ.D_E_L_E_T_ = '' " 
	
	TCQUERY cQuery NEW ALIAS MRP

	While MRP->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("MRP"):SetValue(MRP->MRP)
		oSection1:Cell("MRP"):SetAlign("LEFT")

		oSection1:Cell("CODIGO"):SetValue(MRP->CODIGO)
		oSection1:Cell("CODIGO"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(MRP->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(MRP->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("PERIODO"):SetValue(MRP->PERIODO)
		oSection1:Cell("PERIODO"):SetAlign("LEFT")
			
		oSection1:Cell("PREVISAO"):SetValue(MRP->PREVISAO)
		oSection1:Cell("PREVISAO"):SetAlign("LEFT")
				
		oSection1:Cell("ENTRADA"):SetValue(MRP->ENTRADA)
		oSection1:Cell("ENTRADA"):SetAlign("LEFT")
				
		oSection1:Cell("SAIDA"):SetValue(MRP->SAIDA)
		oSection1:Cell("SAIDA"):SetAlign("LEFT")
		
		oSection1:Cell("ESTOQUE"):SetValue(MRP->ESTOQUE)
		oSection1:Cell("ESTOQUE"):SetAlign("LEFT")
						
		oSection1:Cell("NECESSIDADE"):SetValue(MRP->NECESSIDADE)
		oSection1:Cell("NECESSIDADE"):SetAlign("LEFT")
								
		oSection1:PrintLine()
		
		MRP->(DBSKIP()) 
	enddo
	MRP->(DBCLOSEAREA())
Return( Nil )
