#include "topconn.ch"
#INCLUDE "PROTHEUS.CH"
#include "rwmake.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATR002  �Autor  �Bruno Parreira      � Data �  13/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de pick-list de separacao.                        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATR002()                    

//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
Local cPerg     := "HFATR002"

Private cDesc1 	:= "Este relatorio tem o objetivo de informar para os usu�rios do"
Private cDesc2 	:= "estoque, quais itens dever�o ser separados para os"
Private cDesc3 	:= "pr�-faturamentos liberados."
Private cString	:= "SZJ"
Private tamanho	:= "P"
Private cbCont,cabec1,cabec2,cbtxt
Private aOrd	:= {}//{"Endere�o"}
Private aReturn := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
Private nomeprog:= "HFATR002",nLastKey := 0
Private li		:= 80, limite:=132, lRodape:=.F.
Private wnrel  	:= "HFATR002"
Private titulo 	:= "Mapa de Separa��o"
Private cSerIni := ""
Private cSerFin := ""
Private nLin 	:= 100
Private nCol 	:= 60
Private nPula 	:= 60
Private	nfim    := 2400
Private imprp	:= .F.
Private a_cols 	:= {50, 100, 150, 200, 250, 300, 350, 400, 450, 500, 550, 600, 650, 700, 750, 800, 850, 900, 950, 1000}

Private	oFont14	 := TFont():New("Arial",,14,,.f.,,,,,.f.)
Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
Private oFont09  := TFont():New("Arial",,09,,.f.,,,,,.f.)
Private oFont09n := TFont():New("Arial",,09,,.t.,,,,,.f.)
Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
Private oFont12  := TFont():New("Arial",,12,,.f.,,,,,.f.)
Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)

Private lMarcar  := .F.
Private cTipos   := ""

Private nPagina  := 0

cbtxt   := SPACE(10)
cbcont  := 0
Li      := 80
m_pag   := 1
cabec2  := ""
cabec1  := ""

AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
	Return
EndIf

cMvPar06 := AllTrim(mv_par06) 

If !Empty(cMvPar06)
	For nx := 1 to len(cMvPar06) step 2
		cAux := SubStr(cMvPar06,nx,2)
		If cAux <> '**'
			cTipos += "'0"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

MontaTela()

Return

Static Function MontaTela()
Local cQuery := ""
Local _astru := {}

cQuery := "select ZJ_LOTDAP,ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,ZJ_DATA,A1_NOME,SUM(ZJ_QTDLIB) as ZJ_QTDLIB "
cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ "
cQuery += CRLF + "inner join "+RetSqlName("SA1")+" SA1 "
cQuery += CRLF + "on A1_COD = ZJ_CLIENTE "
cQuery += CRLF + "and A1_LOJA = ZJ_LOJA "
cQuery += CRLF + "and SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SC5")+" SC5 "
cQuery += CRLF + "on C5_NUM = ZJ_PEDIDO "
cQuery += CRLF + "and SC5.D_E_L_E_T_ = '' "
If !Empty(cTipos)
	cQuery += CRLF + "and C5_TPPED IN ("+cTipos+") "
EndIf
cQuery += CRLF + "where SZJ.D_E_L_E_T_ = '' "
cQuery += CRLF + "and ZJ_PEDIDO between '"+mv_par01+"' and '"+mv_par02+"' "
cQuery += CRLF + "and ZJ_LOTDAP between '"+mv_par04+"' and '"+mv_par05+"' "
cQuery += CRLF + "and ZJ_DATA   between '"+DtoS(mv_par07)+"' and '"+DtoS(mv_par08)+"' "
If mv_par03 = 2
	cQuery += CRLF + "and ZJ_QTDSEP = 0 "
EndIf
If mv_par09 = 1
	cQuery += CRLF + "and ZJ_CONF IN ('L','S') "
Else
	cQuery += CRLF + "and ZJ_CONF = '' "
EndIf
cQuery += CRLF + "group by ZJ_LOTDAP,ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,ZJ_DATA,A1_NOME "
cQuery += CRLF + "order by ZJ_LOTDAP,ZJ_NUMPF "

MemoWrite("HFATR002_MontaTela.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"    ,"C",2,0})
AADD(_astru,{"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2]})
AADD(_astru,{"ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2]})
AADD(_astru,{"ZJ_PEDIDO" ,TAMSX3("ZJ_PEDIDO" )[3],TAMSX3("ZJ_PEDIDO" )[1],TAMSX3("ZJ_PEDIDO" )[2]})
AADD(_astru,{"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2]})
AADD(_astru,{"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2]})
AADD(_astru,{"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2]})
AADD(_astru,{"ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2]})
AADD(_astru,{"ZJ_DATA"   ,TAMSX3("ZJ_DATA"   )[3],TAMSX3("ZJ_DATA"   )[1],TAMSX3("ZJ_DATA"   )[2]})

cArqTrab  := CriaTrab(_astru,.T.)
dbUseArea( .T.,, cArqTrab, "TRB", .F., .F. )
IndRegua("TRB",cArqTrab,"ZJ_LOTDAP+ZJ_NUMPF")
TRB->(dbClearIndex())
TRB->(dbSetIndex(cArqTrab + OrdBagExt()))

//Atribui a tabela temporaria ao alias TRB
Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		
		DbSelectArea("TRB")
		RecLock("TRB",.T.)
		TRB->ZJ_LOTDAP  := TMP->ZJ_LOTDAP
		TRB->ZJ_NUMPF   := TMP->ZJ_NUMPF
		TRB->ZJ_PEDIDO  := TMP->ZJ_PEDIDO
		TRB->ZJ_CLIENTE := TMP->ZJ_CLIENTE
		TRB->ZJ_LOJA    := TMP->ZJ_LOJA
		TRB->A1_NOME    := TMP->A1_NOME
		TRB->ZJ_QTDLIB  := TMP->ZJ_QTDLIB
		TRB->ZJ_DATA    := StoD(TMP->ZJ_DATA)
		MsUnlock()
		
		TMP->(DbSkip())
	EndDo
Else
	MsgAlert("Nenhum registro foi encontrado.","Aten��o")
	lRet := .F.
	LimpaTab()
	Return
EndIf

//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('TRB')        

//define as colunas para o browse
aColunas := {;
{"Lote DAP"   ,"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2],X3Picture("ZJ_LOTDAP" )},;	
{"Num Pre-Fat","ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2],X3Picture("ZJ_NUMPF"  )},;
{"Pedido"     ,"ZJ_PEDIDO" ,TAMSX3("ZJ_PEDIDO" )[3],TAMSX3("ZJ_PEDIDO" )[1],TAMSX3("ZJ_PEDIDO" )[2],X3Picture("ZJ_PEDIDO" )},;
{"Cliente"    ,"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2],X3Picture("ZJ_CLIENTE")},;
{"Loja"       ,"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2],X3Picture("ZJ_LOJA"   )},;
{"Nome"       ,"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2],X3Picture("A1_NOME"   )},;
{"Qtd Pre-Fat","ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2],X3Picture("ZJ_QTDLIB" )},;
{"Dt. Pre-Fat","ZJ_DATA"   ,TAMSX3("ZJ_DATA"   )[3],TAMSX3("ZJ_DATA"   )[1],TAMSX3("ZJ_DATA"   )[2],X3Picture("ZJ_DATA"   )}}

//seta as colunas para o browse
oMark:SetFields(aColunas)
    
//Setando sem�foro, descri��o e campo de mark
oMark:SetSemaphore(.F.)
oMark:SetDescription('Mapa de Separa��o')
oMark:SetFieldMark('MK_OK')
                              
//oMark:AddButton("Gerar Pre-Faturamento","MsgRun('Gerando pr�-faturamento...','DAP',{|| U_HFAT02PROX(oMark:Mark())})",,2,0)
oMark:AddButton("Imprimir e Liberar","U_HFTR002REL(oMark,1)",,2,0)
oMark:AddButton("Apenas Imprimir","U_HFTR002REL(oMark,2)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HFTR02INV(oMark:Mark())",,2,0)
     
//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || oMark:AllMark()})

oMark:SetTemporary()

//Ativando a janela
oMark:Activate()

LimpaTab()

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFTR002REL�Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Funcao para controlar a transicao de uma tela para outra.   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/ 

User Function HFTR002REL(oMark,nOpc) 
Local aAreaTRB := GetArea()
Local aPed     := {}
Local cPerg    := "HFATR002"

If !MsgYesNo("Confirma a impress�o dos pr�-faturamentos selecionados?","Confirma��o")
	RestArea(aAreaTRB)
	lContinua := .F.
	Return
EndIf

aPed := GeraArray(oMark)

wnrel:=SetPrint(cString,wnrel,cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

oPrn := TMSPrinter():New()
oPrn:SetPortrait()

If nOpc = 1
	Processa({||LiberaPF(aPed)},Titulo,"Liberando para confer�ncia, aguarde...")
EndIf

Processa({||ImpRel(aPed,oMark)},Titulo,"Imprimindo, aguarde...")

RestArea(aAreaTRB)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GeraArray �Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta array com pedidos para pre-faturamento.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LiberaPF(aPedido) 
Local nx := 0 

ProcRegua(Len(aPedido))

For nx := 1 to Len(aPedido)
	IncProc()
	DbSelectArea("SZJ")
	DbSetOrder(1)
	If Dbseek(xFilial("SZJ")+aPedido[nx][2])
		If Empty(SZJ->ZJ_CONF)
			While !EOF() .and. SZJ->ZJ_NUMPF = aPedido[nx][2]
				RecLock("SZJ",.F.)
				SZJ->ZJ_CONF = "L"
				MsUnlock()
				DbSkip()
			End
			
			//ATUSTATUS(aPedido[nx][1])
			
		Else
			MsgInfo("Pedido "+SZJ->ZJ_NUMPF+" j� liberado!","Aviso")
		Endif
	EndIf
Next

Return

Static Function ATUSTATUS(cPedSta)
Local cQuery  := ""
Local cPedRak := ""
Local cLog    := ""
Local oWsPedido

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

oWsPedido := WSPedido():New()

cLog += CRLF + "In�cio do processamento..."

DbSelectArea("SC5")
DbSetOrder(1)
If DbSeek(xFilial("SC5")+cPedSta)
	If Empty(SC5->C5_XSTATUS) .And. SC5->C5_ORIGEM = "B2B"
		cPedRak := AllTrim(SC5->C5_XPEDRAK)
		If !Empty(cPedRak) 
			cLog += CRLF + "Processando pedido Rakuten: "+cPedRak
			
			cLog += CRLF + "Par�metros: 0,"+cPedRak+",,134,,,0,"+cChvA1+","+cChvA2+")"
			
			If oWsPedido:AlterarStatus(0,Val(cPedRak),"",134,"","",0,"",cChvA1,cChvA2)
				cWsRet := oWsPedido:oWSAlterarStatusResult:cDescricao
				
				cLog += CRLF + "Retorno da Funcao AlterarStatus: "+cWsRet
				If "SUCESSO" $ UPPER(cWsRet)
					RecLock("SC5",.F.)
					SC5->C5_XSTATUS := "A" //Status Alterado
					MsUnlock()
					cLog += CRLF + "Status atualizado com sucesso para o pedido Protheus: "+cPedSta
				EndIf
			Else
				cLog += CRLF + "Erro na Funcao AlterarStatus: "+GetWSCError()
			EndIf
		Else
			cLog += CRLF + "Campo com o pedido Rakuten (C5_XPEDRAK) vazio. Pedido Protheus: "+cPedSta
		EndIf
	EndIf
EndIf

cLog += CRLF + "T�rmino do processamento..."

MemoWrite("\log_rakuten\B2B\"+cDtTime+"_ATU-STATUS-PED-"+cPedSta+".txt",cLog)	
	
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �GeraArray �Autor  �Bruno Parreira      � Data � 08/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Monta array com pedidos para pre-faturamento.               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function GeraArray(oMark) 
Local lMark    := .F.
Local aArray   := {}

TRB->(DbGoTop())
While TRB->(!Eof())
    If !Empty(TRB->MK_OK)  	
		lMark := .T.
		aAdd(aArray,{TRB->ZJ_PEDIDO,TRB->ZJ_NUMPF})
    Endif
    TRB->(DbSkip())
EndDo

If !lMark
	MsgAlert("Nenhum item foi selecionado.","Aten��o")
EndIf

Return aArray

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �LimpaTab  �Autor  �Bruno Parreira      � Data � 14/03/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Limpa os arquivos e alias teporarios que estao abertos.     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function LimpaTab()

//Limpar o arquivo tempor�rio
If !Empty(cArqTrab)
    Ferase(cArqTrab+GetDBExtension())
    Ferase(cArqTrab+OrdBagExt())
    cArqTrab := ""
    TRB->(DbCloseArea())
Endif

TMP->(DbCloseArea())

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFTR02INV �Autor  �Bruno Parreira      � Data � 06/01/2017  ���
�������������������������������������������������������������������������͹��
���Desc      �Marca ou desmarca todos os itens da FWMarkBrowse.           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFTR02INV(cMarca)
Local aAreaTRB  := TRB->(GetArea())

lMarcar := !lMarcar 
 
dbSelectArea("TRB")
TRB->(dbGoTop())
While !TRB->(Eof())
    RecLock("TRB",.F.)
    TRB->MK_OK := IIf(lMarcar,cMarca,'  ')
    MsUnlock()
    TRB->(dbSkip())
EndDo

RestArea( aAreaTRB )

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � ImpRel   � Autor � Bruno Parreira        � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HFATR002	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpRel(aPedido,oMark)
Local cQuery  := ""
Local cAmzPic := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0"))
Local nx      := 0
Local cPedido := ""

If Len(aPedido) > 0
	For nx := 1 to Len(aPedido)
		cPedido += "'"+aPedido[nx][1]+"',"
	Next
	
	cPedido := SubStr(cPedido,1,Len(cPedido)-1)
EndIf

// AND ZJ_ITEMGRD = C6_ITEMGRD

cQuery := "select ZJ_LOTDAP, ZJ_NUMPF, ZJ_PEDIDO, ZJ_ITEM, ZJ_PRODUTO, B1_DESC, B1_UM, ZJ_LOCALIZ, BE_XCODEST, BE_XSEQUEN, "+CRLF 
cQuery += "		SUM(ZJ_QTDLIB) as ZJ_QTDLIB, C5_PRAZO, C5_POLCOM, C5_TPPED, C5_CONDPAG, C5_XMICRDE, C5_XPEDRAK, C5_XPEDWEB, "+CRLF
cQuery += "		C5_CLIENTE, C5_LOJACLI, C5_TRANSP, C5_XINSTRU, C5_XPEDMIL, C5_XPEDVIN, C5_XPRZVIN, C5_FECENT, C5_FRETE, C5_EMISSAO, "+CRLF
cQuery += "		C6_PRCVEN, C6_VALOR, C5_DESPESA, A1_NOME, A1_END, A1_EST, A1_MUN, A1_BAIRRO, A1_CEP, C6_XITPRES, "+CRLF
cQuery += "		LTRIM(RTRIM(REPLACE(REPLACE(REPLACE(C6_XPRESEN, CHAR(10), ''), CHAR(13), ' '), CHAR(9), ''))) AS C6_XPRESEN "+CRLF
cQuery += "from "+RetSqlName("SZJ")+" SZJ WITH (NOLOCK) "+CRLF
cQuery += "		inner join "+RetSqlName("SC5")+" SC5 WITH (NOLOCK) on C5_NUM = ZJ_PEDIDO and SC5.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SC6")+" SC6 WITH (NOLOCK) on C6_NUM = ZJ_PEDIDO and ZJ_PRODUTO=C6_PRODUTO "+CRLF 
cQuery += "							and C6_ITEM = ZJ_ITEM  and SC6.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SA1")+" SA1 WITH (NOLOCK) on A1_COD = C5_CLIENTE and A1_LOJA = C5_LOJACLI and SA1.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) on B1_COD = ZJ_PRODUTO and SB1.D_E_L_E_T_ = '' "+CRLF
cQuery += "		inner join "+RetSqlName("SBE")+" SBE WITH (NOLOCK) on BE_LOCAL = '"+cAmzPic+"' and BE_LOCALIZ = ZJ_LOCALIZ and SBE.D_E_L_E_T_ = '' "+CRLF
cQuery += "where SZJ.D_E_L_E_T_ = '' "+CRLF
cQuery += "		and ZJ_PEDIDO IN ("+cPedido+") "+CRLF
cQuery += "		and ZJ_LOTDAP between '"+mv_par04+"' and '"+mv_par05+"' "+CRLF
If mv_par03 = 2
	cQuery += "		and ZJ_QTDSEP = 0 "+CRLF
EndIf
//If mv_par09 = 1
//	cQuery += "		and ZJ_CONF IN ('L','S') "+CRLF
//Else
//	cQuery += "		and ZJ_CONF = '' "+CRLF
//EndIf
cQuery += "group by ZJ_LOTDAP, ZJ_NUMPF, ZJ_PEDIDO, ZJ_ITEM, ZJ_PRODUTO, B1_DESC, B1_UM, ZJ_LOCALIZ, BE_XCODEST, BE_XSEQUEN, "+CRLF
cQuery += "			C5_PRAZO, C5_POLCOM, C5_TPPED, C5_CONDPAG, C5_XMICRDE, C5_XPEDRAK, C5_XPEDWEB, C5_CLIENTE, C5_LOJACLI, C5_TRANSP, "+CRLF
cQuery += "			C5_XINSTRU, C5_XPEDMIL, C5_XPEDVIN, C5_XPRZVIN, C5_FECENT, C5_FRETE, C5_EMISSAO, C6_PRCVEN, C6_VALOR, C5_DESPESA, "+CRLF
cQuery += "			A1_NOME, A1_END, A1_EST, A1_MUN, A1_BAIRRO, A1_CEP, C6_XITPRES, C6_XPRESEN "+CRLF
cQuery += "order by ZJ_LOTDAP, ZJ_NUMPF, BE_XCODEST, BE_XSEQUEN, ZJ_LOCALIZ, ZJ_PEDIDO, ZJ_ITEM "+CRLF

memowrite("HFATR002.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"QRY", .F., .T.)

DbSelectArea("QRY")
DbGoTop()
n_cont := 0

If QRY->(!EOF())
	While QRY->(!EOF())
		n_cont++
		QRY->(DbSkip())
	EndDo
	DbGoTop()
	ProcRegua(n_cont)
	NPAG := 1
Else
	MsgAlert("N�o h� dados a serem impressos.","Aten��o")
	Return	
EndIf

nTotQtd := 0
nTotVlr := 0
cNumPF  := ""
cCodEst := ""
aColItens := {080,300,1500,1700,1900,2150}

lEst := .T.

While QRY->(!Eof())
	
	IncProc()
	
	If cNumPF <> QRY->ZJ_NUMPF .Or. Li > 25 //.or. nlin > 30
		If cNumPF <> QRY->ZJ_NUMPF
			nPagina := 1
		Else
			If Li > 25
				nPagina++
			EndIf		
		EndIf
		CabecSep(nPagina)
		Li := 0
		lEst := .T.
	EndIf
	
	If cCodEst <> QRY->BE_XCODEST .Or. lEst
		oPrn:Say(nLin,aColItens[1],"Esta��o: "+QRY->BE_XCODEST,oFont09n,100)
		oPrn:Box(nLin-10,50,nLin+50,nfim)
		nLin += nPula
		lEst := .F.
	EndIf

	cEnderec := QRY->ZJ_LOCALIZ
	cProduto := QRY->ZJ_PRODUTO+" - "+SubStr(QRY->B1_DESC,1,45)
	nQuant   := QRY->ZJ_QTDLIB
	nPrcVen  := QRY->C6_PRCVEN
	nPrcTot  := QRY->ZJ_QTDLIB*QRY->C6_PRCVEN//QRY->C6_VALOR
	nPeso    := 0
	
	oPrn:Say(nLin,aColItens[1],cEnderec,oFont09,100)
	oPrn:Say(nLin,aColItens[2],cProduto,oFont09,100)
	oPrn:Say(nLin,aColItens[3],Transform(nQuant,"@E 9,999,999")+" "+QRY->B1_UM,oFont09,100)
	oPrn:Say(nLin,aColItens[4],Transform(nPrcVen,"@E 9,999,999.99"),oFont09,100)
	oPrn:Say(nLin,aColItens[5],Transform(nPrcTot,"@E 9,999,999.99"),oFont09,100)
	oPrn:Say(nLin,aColItens[6],Iif(alltrim(QRY->C6_XITPRES)="S","Presente"," "),oFont09,100)
	
	oPrn:Box(nLin,2360,nLin+40,nfim)
	
	nLin += nPula
	Li++

	If Li > 25 //.or. nlin > 30
		nPagina++
		CabecSep(nPagina)
		Li := 0
		lEst := .T.
	EndIf
	
	cNumPF  := QRY->ZJ_NUMPF
	cCodEst := QRY->BE_XCODEST
	nTotQtd += QRY->ZJ_QTDLIB
	nTotVlr += nPrcTot//QRY->C6_VALOR
	
	QRY->(DbSkip())
	
	If cNumPF <> QRY->ZJ_NUMPF //.Or. Li > 30
	
		If Li > 25 //.or. nlin > 30
			nPagina++
			CabecSep(nPagina)
			Li := 0
			lEst := .T.
		EndIf
	
		oPrn:Say(nLin,aColItens[3]-150,"Total:",oFont09n,100)
		oPrn:Say(nLin,aColItens[3],Transform(nTotQtd,"@E 9,999,999"),oFont09n,100)
		oPrn:Say(nLin,aColItens[5],Transform(nTotVlr,"@E 9,999,999.99"),oFont09n,100)
		oPrn:Box(nLin-10,50,nLin+50,nfim)
		nTotQtd := 0
		nTotVlr := 0
	EndIf
	
	/*
	If nLin > nPula*50
		roda(cbcont,cbtxt,tamanho)
		NPAG++
	EndIf*/
	
EndDo

/*
If len(a_dados) > 0
	IMPRIMET()
EndIf
*/
/*If Li != 80
	roda(cbcont,cbtxt,tamanho)
EndIf*/

MS_FLUSH()

If	( aReturn[5] == 1 ) //1-Disco, 2-Impressora
	oPrn:Preview()
Else
	oPrn:Setup()
	oPrn:Print()
EndIf

QRY->(DbCloseArea())
oMark:DeActivate()

Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    �CabecSep  � Autor � Bruno Parreira        � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Imprime o cabecalho do relatorio                           ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HFATR002	                                                  ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function CabecSep(nPag)
Local aArea := GetArea() 
nLin	:= 100
nCol	:= 60
nPula	:= 60
nEsp    := 350
nPosTit := 60

aColCab := {80,850,1600,2050}

aTit    := {"Endere�o","Produto","Qtde","Vrl. Unit","Vlr. Total","Presente"}
aColTit := AClone(aColItens)

oPrn:EndPage()

oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
oPrn:Say(nLin,aColCab[4]+150,"P�g.: "+AllTrim(Str(nPag)),oFont09,100)
nLin += nPula

oPrn:Say(nLin,aColCab[4]-50,dtoc(date())+" - "+Time(),oFont10,100)

nLin += nPula*2

oPrn:Say(nLin,aColCab[1],"Pr�-Faturamento:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->ZJ_NUMPF,oFont10,100)
oPrn:Say(nLin,aColCab[2],"Pedido RAKUTEN:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,AllTrim(If(!Empty(QRY->C5_XPEDWEB),QRY->C5_XPEDWEB,QRY->C5_XPEDRAK))+" - "+AllTrim(QRY->C5_XPEDMIL),oFont10,100)
oPrn:Say(nLin,aColCab[3],"Pedido Protheus:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,QRY->ZJ_PEDIDO,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Lote DAP:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->ZJ_LOTDAP,oFont10,100)
oPrn:Say(nLin,aColCab[2],"Tipo Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,GetAdvFVal("SZ6","Z6_DESC",xFilial("SZ6")+QRY->C5_POLCOM+QRY->C5_PRAZO,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[3],"Ped Vinc./Prazo:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,IIF(!Empty(QRY->C5_XPEDVIN),(QRY->C5_XPEDVIN + " / " + AllTrim(Str(QRY->C5_XPRZVIN))),""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cond. Pagamento:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,GetAdvFVal("SE4","E4_DESCRI",xFilial("SE4")+QRY->C5_CONDPAG,1,""),oFont10,100)
oPrn:Say(nLin,aColCab[3],"Tipo Pedido:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,GetAdvFVal("SZ4","Z4_DESC",xFilial("SZ4")+QRY->C5_POLCOM+QRY->C5_TPPED,1,""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cliente:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_CLIENTE+"/"+QRY->C5_LOJACLI+" - "+SubStr(QRY->A1_NOME,1,35),oFont10,100)
oPrn:Say(nLin,aColCab[3],"CEP:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,Transform(QRY->A1_CEP,"@R 99999-999"),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Endere�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->A1_END,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Data Emiss�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,DtoC(StoD(QRY->C5_EMISSAO)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Cidade:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->A1_MUN,oFont10,100)
oPrn:Say(nLin,aColCab[2],"UF:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,QRY->A1_EST,oFont10,100)
oPrn:Say(nLin,aColCab[3],"Bairro:",oFont10n,100)
oPrn:Say(nLin,aColCab[3]+nEsp,QRY->A1_BAIRRO,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Macro Regi�o:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_XMICRDE,oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Frete:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(QRY->C5_FRETE,"@E 9,999,999.99"),oFont10,100)
oPrn:Say(nLin,aColCab[2],"Data Entrega:",oFont10n,100)
oPrn:Say(nLin,aColCab[2]+nEsp,DtoC(StoD(QRY->C5_FECENT)),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Transportador:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,QRY->C5_TRANSP+" - "+GetAdvFVal("SA4","A4_NOME",xFilial("SA4")+QRY->C5_TRANSP,1,""),oFont10,100)
nLin += nPula
oPrn:Say(nLin,aColCab[1],"Presente:",oFont10n,100)
oPrn:Say(nLin,aColCab[1]+nEsp,Transform(QRY->C5_DESPESA,"@E 9,999,999.99"),oFont10,100)

MSBAR('CODE128',5.8,15.0,QRY->ZJ_NUMPF,oPrn,.F.,,.T.,0.040,1.1,,,,.F.)
nLin += nPula
oPrn:Say(nLin-30,aColCab[4]-150,QRY->ZJ_NUMPF,oFont14n,100)
nLin += nPula

nLimite := 130

cObs := AllTrim(QRY->C5_XINSTRU)
If !Empty(cObs)
	oPrn:Say(nLin,aColCab[1],"Instru��es:",oFont10n,100)
	cAux := ""
	If Len(cObs) > nLimite
		nDe  := 1
		nAte := nLimite
		While nDe < Len(cObs)
			cAux := SubStr(cObs,nDe,nAte)
			oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
			nLin += nPula
			nDe  := nDe+nLimite
			nAte := nAte+nLimite
		EndDo
	Else
		oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
		nLin += nPula	
	EndIf
EndIf

cObs := AllTrim(GetAdvFVal("SC5","C5_XOBSINT",xFilial("SC5")+QRY->ZJ_PEDIDO,1,""))
If !Empty(cObs)
	oPrn:Say(nLin,aColCab[1],"Observa��o:",oFont10n,100)
	cAux := ""
	If Len(cObs) > nLimite
		nDe  := 1
		nAte := nLimite
		While nDe < Len(cObs)
			cAux := SubStr(cObs,nDe,nAte)
			oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
			nLin += nPula
			nDe  := nDe+nLimite
			nAte := nAte+nLimite
		EndDo
	Else
		oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
		nLin += nPula	
	EndIf		
EndIf

DbSelectArea("SC6")
DbSetOrder(1)
If DbSeek(xFilial("SC6")+QRY->ZJ_PEDIDO)
	lPre := .T.
	While SC6->(!EOF()) .And. SC6->C6_NUM = QRY->ZJ_PEDIDO
		If !Empty(SC6->C6_XPRESEN)
			cObs := SC6->C6_PRODUTO+" - "+AllTrim(SC6->C6_XPRESEN)
			Memowrite("HFATR002_cObs.txt",cObs)
			If lPre
				oPrn:Say(nLin,aColCab[1],"Msg. Presente:",oFont10n,100)
				lPre := .F.
			EndIf
			cAux := ""
			If Len(cObs) > nLimite
				nDe  := 1
				nAte := nLimite
				While nDe < Len(cObs)
					cAux := SubStr(cObs,nDe,nAte)
					If nDe = 1
						Memowrite("HFATR002_cAux.txt",cAux)
					EndIf
					oPrn:Say(nLin,aColCab[1]+nEsp,cAux,oFont10,100)
					nLin += nPula
					nDe  := nDe+nLimite
					nAte := nAte+nLimite
				EndDo
			Else
				oPrn:Say(nLin,aColCab[1]+nEsp,cObs,oFont10,100)
				nLin += nPula	
			EndIf		
		EndIf
		SC6->(DbSkip())
	EndDo
EndIf
nLin += nPula

oPrn:Box(nLin-10,50,nLin+50,nfim)

oPrn:Say(nLin,aColTit[1],aTit[1],oFont10n,100)
oPrn:Say(nLin,aColTit[2],aTit[2],oFont10n,100)
oPrn:Say(nLin,aColTit[3]+20,aTit[3],oFont10n,100)
oPrn:Say(nLin,aColTit[4],aTit[4],oFont10n,100)
oPrn:Say(nLin,aColTit[5],aTit[5],oFont10n,100)
oPrn:Say(nLin,aColTit[6],aTit[6],oFont10n,100)

nLin += nPula
imprp	:= .T.

RestArea(aArea)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Pedido de            ","","","Mv_ch1","C",6,0,0,"G","",   "","","N","Mv_par01",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o pedido de.",""},{""},{""},"")
PutSx1(cPerg,"02","Pedido ate           ","","","Mv_ch2","C",6,0,0,"G","",   "","","N","Mv_par02",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o pedido ate.",""},{""},{""},"")
PutSx1(cPerg,"03","Imprime j� separados?","","","Mv_ch3","N",1,0,2,"C","",   "","","N","Mv_par03",       "Sim","","","",      "Nao","","",     "","","","","","","","","","","","","","","","",{"Imprime itens j� separados?",""},{""},{""},"")
PutSx1(cPerg,"04","Lote DAP de          ","","","Mv_ch4","C",6,0,0,"G","",   "","","N","Mv_par04",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o Lote DAP de.",""},{""},{""},"")
PutSx1(cPerg,"05","Lote DAP ate         ","","","Mv_ch5","C",6,0,0,"G","",   "","","N","Mv_par05",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione o Lote DAP ate.",""},{""},{""},"")
PutSx1(cPerg,"06","Tipo dos Pedidos     ","","","Mv_ch6","C",99,0,0,"G","U_HF002TP()","","","N","Mv_par06","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os tipos de pedido.",""},{""},{""},"")
PutSx1(cPerg,"07","Data de              ","","","Mv_ch7","D",8,0,0,"G","",   "","","N","Mv_par07",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione a Data de.",""},{""},{""},"")
PutSx1(cPerg,"08","Data ate             ","","","Mv_ch8","D",8,0,0,"G","",   "","","N","Mv_par08",          "","","","",         "","","",     "","","","","","","","","","","","","","","","",{"Selecione a Data ate.",""},{""},{""},"")
PutSx1(cPerg,"09","Somente Liberados ?  ","","","Mv_ch9","N",1,0,2,"C","",   "","","N","Mv_par09",       "Sim","","","",      "Nao","","",     "","","","","","","","","","","","","","","","",{"Imprime itens j� liberados?",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)