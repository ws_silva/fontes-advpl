
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTA004  �Autor  �Bruno Parreira      � Data �  23/04/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Abastecimento preventivo.                                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HESTA004()
Local cPerg   := "HESTA004"
Private cLog  := ""
Private cCodAbast := ""

AjustaSX1(cPerg)

If !Pergunte(cPerg)
	Return
EndIf

If !MsgYesNo("Confirma o abastecimento preventivo para a esta��o "+mv_par01+"?","Confirma��o")
	Return
EndIf

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

cLog += "Processo de abastecimento preventivo para esta��o "+mv_par01+" iniciado."

Processa( {|| ABASTECE() }, "Aguarde...", "Processando...",.F.)

MemoWrite("\log_abast\"+cDtTime+"_ABAST"+cCodAbast+".log",cLog)

Return

Static Function ABASTECE()
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cAmzPul := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao
Local nReg := 0

DbSelectArea("SBE")
DbOrderNickName("LOCEST")
If DbSeek(xFilial("SBE")+cAmzPik+mv_par01)
	While SBE->(!EOF()) .And. SBE->BE_LOCAL+SBE->BE_XCODEST = cAmzPik+mv_par01
		If !Empty(SBE->BE_CODPRO)
			nReg++
		EndIf
		SBE->(DbSkip())
	EndDo	
EndIf

ProcRegua(nReg)

DbSelectArea("SBE")
DbOrderNickName("LOCEST")
DbGoTop()
If DbSeek(xFilial("SBE")+cAmzPik+mv_par01)
	cParAbast := GetMV("MV_XCODABA")
	cCodAbast := "A"+cParAbast
	cItem := "01"
	While SBE->(!EOF()) .And. SBE->BE_LOCAL+SBE->BE_XCODEST = cAmzPik+mv_par01
		If !Empty(SBE->BE_CODPRO)
			nFator  := SBE->BE_XFATOR     //Fator de conversao (quantas pecas cabem no recipiente)
			nQtdRec := SBE->BE_XQTDREC    //Quantidade de recipeientes que cabem no endere�o
			
			nSldPik := HESTSALDO(SBE->BE_CODPRO,cAmzPik) //Saldo do produto no picking
			
			nQtdAtu := nSldPik / nFator  //Quantidade de recipientes atualmente
			
			If nQtdAtu > int(nQtdAtu)
				nQtdAtu := int(nQtdAtu) + 1
			Else
				nQtdAtu := int(nQtdAtu)
			EndIf
			
			If nQtdAtu < nQtdRec
				nQtdFalt := nQtdRec - nQtdAtu   //Quantidade de recipientes que estao faltando no endere�o do picking
				nSldPos  := nQtdAtu * nFator    //Quantidade possivel de pecas com os recipientes atuais
				nSldFalt := nQtdFalt * nFator   //Quantidade de pecas faltantes para transferir para o picking
				
				nSldPul  := HESTSALDO(SBE->BE_CODPRO,cAmzPul)  //Saldo de pecas no pulmao
					
				If nSldPul <= 0
					IncProc()
					SBE->(DbSkip())
					Loop
				EndIf

				U_HTRANSF(SBE->BE_CODPRO,nSldFalt,cAmzPul,cAmzPik,cCodAbast,cItem,cCodAbast) //Faz a transferencia da quantidade faltante do pulmao para o picking
				cItem := SOMA1(cItem,2)
			EndIf
		EndIf
		IncProc()
		SBE->(DbSkip())
	EndDo
	cPrxAbast := SOMA1(cParAbast,5) 
	PutMV("MV_XCODABA",cPrxAbast)
	
	MsgInfo("Abastecimento realizado com sucesso!","Aviso")
Else
	MsgAlert("Esta��o n�o encontrada.","Aten��o")	
EndIf 

Return

Static Function HESTSALDO(cProduto,cArmazem) 
Local nRet := 0

DbSelectArea("SB2")
DbSetOrder(1)
If DbSeek(xFilial("SB2")+cProduto+cArmazem)
	nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()	
EndIf

Return nRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Codigo da Estacao","","","Mv_ch1",TAMSX3("BE_XCODEST")[3],TAMSX3("BE_XCODEST")[1],TAMSX3("BE_XCODEST")[2],0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"Codigo da estacao.",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)