#include "protheus.ch"
#include "topconn.ch"
#include "tbiconn.ch"
#include "shell.ch"
#Include "RwMake.ch"
#include "rwmake.ch"
#include "ap5mail.ch"
#include "xmlxfun.ch"       
#include "TOTVS.CH"
/*
Le arquivo CSV (planilha com dados de titulos a pagar
Conforme seguencia abaixo:
PREFIXO	NUMERO PARCELA TIPO	NATUREZA PORTADOR FORNECEDOR LOJA NOME_FORNECEDOR EMISSAO VENCIMENTO VALOR CODIGO_DE_BARRAS	HISTORICO
PREFIXO	NUMERO DOC	PARCELA	TIPO	NATUREZA	PORTADOR	AGENCIA	CODIGO CLIENTE	LOJA	NOME	EMISSAO	VENCIENTO	VALOR	CENTRO CUSTO	EMISSAO	HISTORICO

1	             2	       3	      4	      5      	6	          7	            8	     9	10	       11	   12      	13	      14        	15	     16	           17	        18	       19	   20	      21	       22	      23	      24	      25	   26	       27	       28	          29	           30	  31	   32	      33	        34	       35	    36	       37	        38	        39	       40	        41	        42	        43	       44	      45	         46
E1_PREFIXO	N_DOCUMENTO	CHECK NF	CONTA	E1_NUM	E1_PARCELA	CODIGO_PROTHEUS	E1_NATUREZA	CPF	CNPJ	E1_LOJA	BLABAL	E1_EMISSAO	E1_VENCTO	E1_VENCREA	E1_VALOR	E1_NUMBCO	E1_HIST	E1_STUACA	E1_SALDO	E1_DESCONT	E1_MULTA	E1_JUROS	CUSTAS	E1_NUMNOTA	E1_VALLIQ	E1_IDCNAB	CODIGO_MILLENIUM	E1_XCOD	E1_XNSU	E1_XAUTNSU	E1_XCANAL	E1_XCANALD	E1_XEMP	E1_XDESEMP	E1_XGRUPO	E1_XGRUPON	E1_XCODDIV	E1_XNONDIV	E1_XMICRRE	E1_XMICRDE	E1_XCODREG	E1_XDESREG	E1_XFORREC	TIPO_GERADOR	EVENTO


*/
USER FUNCTION IMPZAX()  //U_IMPSE1()
IF MsgYesno("Confirma importa��o das Devolu��es ? " )
	Processa({|| u_IMPZAXA()})
Endif
Return

USER FUNCTION IMPZAXA()
Local aSE1      := {}
Local aCrateio  := {}
Local aErros    := {}
Local lSim      := .T.
Local nlin      := 0
Local aRet      := {} //
Local cTexto    := ""
Local cxTexto   := ""
Local NPARC     := 0
PRIVATE lMsHelpAuto := .F.
PRIVATE lMsErroAuto := .F.


nVisibilidade := Nil													// Exibe todos os diret�rios do servidor e do cliente
//		nVisibilidade := GETF_ONLYSERVER										// Somente exibe o diret�rio do servidor
//		nVisibilidade := GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE	// Somente exibe o diret�rio do cliente

cArquivo := cGetFile("Todos os Arquivos ()*.*)     | *.* | ","Selecione um arquivo",0,,.T.,nVisibilidade, .T.)
nHandle := FT_FUse(cArquivo)
if nHandle = -1
	MsgStop(" Problemas na abertura do Arquivo")
	return
endif
nLinTot := FT_FLastRec()
Procregua(nLinTot)
FT_FGoTop()
While !FT_FEOF()
	cLine := FT_FReadLn()
	if Len(alltrim(cLine))== 0
		FT_FSKIP()
		Loop
	Endif
	nRecno := FT_FRecno()
	cLinha := StrTran(cLine,'"',"'")
	cLinha := '{"'+cLinha+'"}'
	//adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
	cLinha := StrTran(cLinha,';','","')
	aAdd(aRet, &cLinha)
	nlin++
	IncProc("Processando  Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)) )
	If nlin >= 2
	if Len(Aret[1]) >= 39
		dbSelectArea("ZAX")
		ZAX->(DbSetOrder(1))  //ZAX_FILIAL, ZAX_NFENTR, ZAX_SERIE, ZAX_CODCLI, ZAX_LOJCLI, R_E_C_N_O_, D_E_L_E_T_
		ZAX->(Dbgotop())
		cNum := Alltrim(aRet[1][20])
		cNum := strzero(val(cNum),9)
		If ZAX->(DbSeek(xFilial("ZAX")+padr(cNum,9),.t. ))
			ZAX->(Reclock("ZAX",.F.))
			ZAX->ZAX_FILIAL := xFilial("ZAX")
		ELSE
			ZAX->(Reclock("ZAX",.T.))
		Endif
		cValor := ''
		npos1  := At('$',Alltrim(aRet[1][13]))  //Alltrim(aRet[1][13])
		cValor := substr(Alltrim(aRet[1][13]),npos1+1,15)
		npos3  := At('.',Alltrim(cValor))
		if npos3 <> 0
			cValor := substr(Alltrim(cValor),1,npos3-1)+substr(Alltrim(cValor),npos3+1,15)
		Endif
		npos2  := At(',',Alltrim(cValor))
		cValor := substr(Alltrim(cValor),1,npos2-1)+substr(Alltrim(cValor),npos2+1,5)
		if npos2 == 0
			nValor := val(cValor)
		Else
			nValor := val(cValor)/100
		Endif
		ZAX->ZAX_CLINFE := Alltrim(aRet[1][02])
		ZAX->ZAX_DTAREG := ctod(Alltrim(aRet[1][03]))
		ZAX->ZAX_ASSIST := Alltrim(aRet[1][04])
		
		ZAX->ZAX_CODCLI := Alltrim(aRet[1][20])
		ZAX->ZAX_LOJCLI := Alltrim(aRet[1][20])
		ZAX->ZAX_UF     := Alltrim(aRet[1][07])
		
		ZAX->ZAX_MREGIA := Alltrim(aRet[1][08])
		ZAX->ZAX_REPRES := Alltrim(aRet[1][09])
		ZAX->ZAX_NFENTR := cNum  //Alltrim(aRet[1][20])
		ZAX->ZAX_DTEMIS := ctod(Alltrim(aRet[1][21]))
		ZAX->ZAX_VOLUME := val(Alltrim(aRet[1][12]))
		ZAX->ZAX_VALOR  := nvalor //Alltrim(aRet[1][13])
		ZAX->ZAX_TPDEVO := Alltrim(aRet[1][14])
		ZAX->ZAX_MOTIVO := Alltrim(aRet[1][15])
		ZAX->ZAX_AUTORI := Alltrim(aRet[1][16])
		ZAX->ZAX_TRANSP := Alltrim(aRet[1][17])
		ZAX->ZAX_FRETE  := Alltrim(aRet[1][18])
		ZAX->ZAX_FLUXOD := Alltrim(aRet[1][19])
		ZAX->ZAX_NFRETO := Alltrim(aRet[1][10])
		ZAX->ZAX_EMRETO := ctod(Alltrim(aRet[1][11]))
		ZAX->ZAX_DIATRA := val(Alltrim(aRet[1][22]))
		ZAX->ZAX_QUALID := Alltrim(aRet[1][23])
		ZAX->ZAX_QTDPEC := val(Alltrim(aRet[1][24]))
		ZAX->ZAX_CONFER := val(Alltrim(aRet[1][25]))
		ZAX->ZAX_1      := val(Alltrim(aRet[1][26]))
		ZAX->ZAX_CONSER := val(Alltrim(aRet[1][27]))
		ZAX->ZAX_CONSE2 := val(Alltrim(aRet[1][28]))
		ZAX->ZAX_CONSE3 := val(Alltrim(aRet[1][29]))
		ZAX->ZAX_FALTA  := val(Alltrim(aRet[1][30]))
		ZAX->ZAX_SOBRA  := Val(Alltrim(aRet[1][31]))
		//ZAX->ZAX_PCUSAD := Alltrim(aRet[1][20])
		//ZAX->ZAX_DTAREV := Alltrim(aRet[1][20])
		//ZAX->ZAX_DIFERE := Alltrim(aRet[1][20])
		ZAX->ZAX_DTRECE := ctod(Alltrim(aRet[1][33]))
		//ZAX->ZAX_DTAQPA := Alltrim(aRet[1][20])
		ZAX->ZAX_ACAOCO := Alltrim(aRet[1][32])
		ZAX->ZAX_POSTAG := Alltrim(aRet[1][34])
		ZAX->ZAX_CODOBJ := Alltrim(aRet[1][35])
		ZAX->ZAX_PEDIDO := Alltrim(aRet[1][36])
		ZAX->ZAX_NFDVCO := Alltrim(aRet[1][37])
		ZAX->ZAX_DTALIB := ctod(Alltrim(aRet[1][38]))
		ZAX->ZAX_OBSERV := Alltrim(aRet[1][39])+' '+ Alltrim(aRet[1][39])
		//ZAX->ZAX_SERIE  := Alltrim(aRet[1][20])
		ZAX->ZAX_STATUS := '01'
	    ZAX->(MsUnlock())
		cQry  := " SELECT F1_DOC,F1_SERIE,F1_FORNECE,F1_LOJA,F1_EMISSAO FROM SF1010 (NOLOCK) WHERE F1_DOC = '"+padr(cNum,9)+"' AND F1_TIPO = 'D' AND D_E_L_E_T_ = '' "
		IF Select("YTRB") > 0
			YTRB->(dbCloseArea())
		Endif
		TCQUERY cQry NEW ALIAS YTRB
		YTRB->(DBGOTOP())
		If YTRB->(!EOF())
			cQry := " SELECT * FROM SD1010 WHERE
			cQry += " D1_DOC = '"+YTRB->F1_DOC+"' AND D1_SERIE = '"+YTRB->F1_SERIE+"' AND D1_FORNECE = '"+YTRB->F1_FORNECE+"' AND D1_LOJA = '"+YTRB->F1_LOJA+"'  AND D_E_L_E_T_ = '' "
			IF Select("ITRB") > 0
				ITRB->(dbCloseArea())
			Endif
			TCQUERY cQry NEW ALIAS ITRB
			ITRB->(DBGOTOP())
			If ITRB->(!EOF())
				dbSelectArea("ZAZ")
				ZAZ->(DbSetOrder(1))  //ZAZ_FILIAL, ZAZ_NFENTR, ZAZ_SERIE, ZAZ_CODCLI, ZAZ_LOJCLI,ZAZ_ITEM,  R_E_C_N_O_, D_E_L_E_T_
				while ITRB->(!EOF())
					ZAZ->(Dbgotop())
					If ZAZ->(DbSeek(xFilial("ZAZ")+padr(ITRB->D1_DOC,9)+ITRB->D1_SERIE+ITRB->D1_FORNECE+ITRB->D1_LOJA+ITRB->D1_ITEM,.t. ))
						ZAZ->(Reclock("ZAZ",.F.))
						ZAZ->ZAZ_FILIAL := xFilial("ZAZ")
					ELSE
						ZAZ->(Reclock("ZAZ",.T.))
					Endif
					ZAZ->ZAZ_ITEM    := substr(ITRB->D1_ITEM,2,3)
					ZAZ->ZAZ_SKU     := ITRB->D1_COD
					ZAZ->ZAZ_NFENTR  := ITRB->D1_DOC
					ZAZ->ZAZ_SERIE   := ITRB->D1_SERIE
					ZAZ->ZAZ_QUANT   := ITRB->D1_QUANT
					ZAZ->ZAZ_VLUNIT  := ITRB->D1_VUNIT
					ZAZ->ZAZ_VLTOT   := ITRB->D1_TOTAL
					ZAZ->ZAZ_CODCLI  := ITRB->D1_FORNECE
					ZAZ->ZAZ_LOJCLI  := ITRB->D1_LOJA
					CODCOR := SUBSTR(ALLTRIM(ITRB->D1_COD),9,3)
					CODTAM := SUBSTR(ALLTRIM(ITRB->D1_COD),12,4)
					ZAZ->ZAZ_DESCR   := Posicione("SB1",1,xFilial("SB1")+padr(ITRB->D1_DOC,9),"B1_DESC")
					ZAZ->ZAZ_COR     := Posicione("SBV",1,xFilial("SBV")+'COR'+CODCOR,"BV_DESCRI")
					ZAZ->ZAZ_TAMANH  := Posicione("SBV",1,xFilial("SBV")+'TAM'+CODTAM,"BV_DESCRI")
					ZAZ->ZAZ_REFER   := SUBSTR(ALLTRIM(ITRB->D1_COD),1,8)
					ZAZ->(MsUnlock())
					dbselectArea("ITRB")
					ITRB->(dbskip())
				Enddo
			endif
		 Endif	
	Endif
	Endif
	// Pula para pr�xima linha
	FT_FSKIP()
	aRet      := {}
Enddo
cxTexto:=""
for i := 1 to len(aErros)
	cxTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
Next i
aRet      := {}
lSim      := .T.
cTexto    := cxTexto
If len(aErros) >0
	cTexto := "Log de Importa��o, Dados n�o Importados. "+CHR(13)+CHR(10)
	for i := 1 to len(aErros)
		cTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
	Next i
	__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)
Endif
if len(alltrim(cTexto)) == 0
	MsgInfo("Fim da Importa��o -  com sucesso        "+CHR(13)+CHR(10)+cTexto)
	
Else
	MsgInfo("Fim da Importa��o -  Verifique erros   "+CHR(13)+CHR(10)+cTexto)
Endif
Return()


static FUNCTION NoAcento(cString)
Local cChar  := ""
Local nX     := 0
Local nY     := 0
Local cVogal := "aeiouAEIOU"
Local cAgudo := "�����"+"�����"
Local cCircu := "�����"+"�����"
Local cTrema := "�����"+"�����"
Local cCrase := "�����"+"�����"
Local cTio   := "����"
Local cCecid := "��"
Local cMaior := "&lt;"
Local cMenor := "&gt;"

For nX:= 1 To Len(cString)
	cChar:=SubStr(cString, nX, 1)
	IF cChar$cAgudo+cCircu+cTrema+cCecid+cTio+cCrase
		nY:= At(cChar,cAgudo)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cCircu)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cTrema)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cCrase)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr(cVogal,nY,1))
		EndIf
		nY:= At(cChar,cTio)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr("aoAO",nY,1))
		EndIf
		nY:= At(cChar,cCecid)
		If nY > 0
			cString := StrTran(cString,cChar,SubStr("cC",nY,1))
		EndIf
	Endif
Next

If cMaior$ cString
	cString := strTran( cString, cMaior, "" )
EndIf
If cMenor$ cString
	cString := strTran( cString, cMenor, "" )
EndIf

cString := StrTran( cString, CRLF, " " )

For nX:=1 To Len(cString)
	cChar:=SubStr(cString, nX, 1)
	If (Asc(cChar) < 32 .Or. Asc(cChar) > 123) .and. !cChar $ '|'
		cString:=StrTran(cString,cChar,".")
	Endif
Next nX
Return cString

