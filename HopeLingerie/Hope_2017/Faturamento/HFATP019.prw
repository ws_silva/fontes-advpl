#Include "PROTHEUS.CH"

User Function HFATP019()

	local cVldAlt := ".T."
	local cVldExc := ".T."
	local cAlias
	
	cAlias := "ZAN"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Vinculo Pedidos x Politica Comercial"
	aRotina := {;
		{ "Pesquisar" , "AxPesqui", 0, 1},;
		{ "Visualizar", "u_VenxPol(4)"	, 0, 2},;
		{ "Alterar"   , "u_VenxPol(2)"	, 0, 4},;
		{ "Excluir"   , "u_VenxPol(3)"	, 0, 5},;
		{ "Incluir"   , "u_VenxPol(1)"	, 0, 3};
		}

	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)
	
return


User Function VenxPol(_tp)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oSay1
	Local oSay2
	Private oFolder1
	Private cGet1			:= Space(6)
	Private cGet2			:= Space(40)

	Static oDlg

	If _tp <> 1
		cGet1 := ZAN->ZAN_REPRES
		cGet2 := ZAN->ZAN_NOMREP
	Endif

	DEFINE MSDIALOG oDlg TITLE "Vinculo Pedidos x Politica Comercial" FROM 000, 000  TO 500, 900 COLORS 0, 16777215 PIXEL

	@ 012, 015 SAY oSay1 PROMPT "Codigo:" SIZE 040, 008 OF oDlg COLORS 0, 16777215 PIXEL
	If _tp <> 1
		@ 010, 050 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 F3 "SA31" READONLY PIXEL
	Else
		@ 010, 050 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg VALID U_VldVxP(1,cGet1) COLORS 0, 16777215 F3 "SA31" PIXEL
	Endif
	@ 012, 145 SAY oSay1 PROMPT "Representante:" SIZE 035, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 190 MSGET oGet2 VAR cGet2 SIZE 220, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

	fMSNewGe1(_tp)
	If _tp == 4
		@ 233, 388 BUTTON oButton2 PROMPT "Cancelar" 		SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL
	Else
		@ 233, 388 BUTTON oButton1 PROMPT "Salvar" 			SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
		@ 233, 344 BUTTON oButton2 PROMPT "Cancelar" 		SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL
	Endif

	ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSNewGe1(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZAN_CODPOL","ZAN_NOMPOL"}
	Local aAlterFields := {"ZAN_CODPOL"}
	Private aHeader1 := {}
	Private aCols1 := {}

	Static oMSNewGe1

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek("ZAN_CODPOL")) .AND. aFields[nX]=="ZAN_CODPOL"
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldVxP(2,M->ZAN_CODPOL)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("ZAN")
		DbSetOrder(1)
		//DbSeek(xfilial("ZAN")+ZAN->ZAN_REPRES+ZAN_CODPOL)
		DbSeek(xfilial("ZAN")+ZAN->ZAN_REPRES)

		If Found()
			While !EOF() .and. ZAN->ZAN_REPRES == cGet1
				Aadd(aCols1,{ZAN->ZAN_CODPOL,ZAN->ZAN_NOMPOL,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols1, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols1, aFieldFill)
	Endif
  
	oMSNewGe1 := MsNewGetDados():New( 40, 010, 225, 443, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", , aHeader1, aCols1)

Return


Static Function Save(_tp)

	Local I
	
	DbSelectArea("ZAN")
	DbSetOrder(1)
	DbSeek(xfilial("ZAN")+cGet1)

	If _tp == 1
		For i := 1 to Len(OMSNEWGE1:ACOLS)
			If OMSNEWGE1:ACOLS[i,len(OMSNEWGE1:aheader)+1] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
				RecLock("ZAN",.T.)
				Replace ZAN_FILIAL			with xfilial("ZAN")
				Replace ZAN_REPRES			with cGet1
				Replace ZAN_NOMREP			with cGet2
				Replace ZAN_CODPOL			with OMSNEWGE1:ACOLS[i,1]
				Replace ZAN_NOMPOL			with OMSNEWGE1:ACOLS[i,2]
				MsUnLock()
			Endif
		Next

	ElseIf _tp == 2
		For i := 1 to Len(OMSNEWGE1:ACOLS)
			If OMSNEWGE1:ACOLS[i,len(OMSNEWGE1:aheader)+1] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
				DbSelectArea("ZAN")
				DbSetOrder(1)
				DbSeek(xfilial("ZAN")+cGet1+OMSNEWGE1:ACOLS[i,1])
				If !Found()
					RecLock("ZAN",.T.)
					Replace ZAN_FILIAL			with xfilial("ZAN")
					Replace ZAN_REPRES			with cGet1
					Replace ZAN_NOMREP			with cGet2
					Replace ZAN_CODPOL			with OMSNEWGE1:ACOLS[i,1]
					Replace ZAN_NOMPOL			with OMSNEWGE1:ACOLS[i,2]
					MsUnLock()
				Endif
			Else
				DbSelectArea("ZAN")
				DbSetOrder(1)
				DbSeek(xfilial("ZAN")+cGet1+OMSNEWGE1:ACOLS[i,1])
				If Found()
					RecLock("ZAN",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next

	ElseIf _tp == 3
		DbSelectArea("ZAN")
		DbSetOrder(1)
		DbSeek(xfilial("ZAN")+cGet1)
	
		While !EOF() .and. ZAN->ZAN_REPRES == cGet1
			RecLock("ZAN",.F.)
			DbDelete()
			MsUnlock()
			DbSkip()
		End
	Endif

Return


User Function VldVxP(_nItem,_Cod)

	Local lRet := .T.
	Local _i

	If _nItem == 1
		If Alltrim(cGet1)==""
			lRet := .F.
			Return(lRet)
		Else
			If Select("TMP") > 0
				TMP->(DbCloseArea())
			EndIf
		 	
			cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("ZAN")
			cQuery  += " WHERE D_E_L_E_T_<>'*' AND ZAN_REPRES='"+_Cod+"'"
			
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
			
			If TMP->COUNT > 0
				Alert("O Repsesentante digitado, j� existe no cadastro.")
				lRet := .F.
				TMP->(DbCloseArea())
				Return(lRet)
			End
			
			TMP->(DbCloseArea())

		End
		
		If lRet
			cGet2	:= POSICIONE("SA3",1,xFilial("SA3")+Alltrim(cGet1),"A3_NOME")
		End

	ElseIf _nItem == 2
  
		For _i:=1 to len(oMSNewGe1:aCols)
			If alltrim(_Cod) == alltrim(oMSNewGe1:aCols[_i,1]) .and. _i <> oMSNewGe1:nAT
				Alert("A Politica j� foi incluida, selecione outra Politica.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(_Cod) == ""
				Alert("N�o poder� incluir uma linha em branco.")
				lRet := .F.
				Return(lRet)
			Else
				If Select("TMP") > 0
					TMP->(DbCloseArea())
				EndIf
		 	
				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SZ2")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND Z2_CODIGO='"+_Cod+"'"
			
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
			
				If TMP->COUNT==0 .and. _Cod<>""
					Alert("A Politica digitada n�o existe no cadastro.")
					lRet := .F.
					TMP->(DbCloseArea())
					Return(lRet)
				End
			
				TMP->(DbCloseArea())
			Endif
		Next

	End

Return(lRet)