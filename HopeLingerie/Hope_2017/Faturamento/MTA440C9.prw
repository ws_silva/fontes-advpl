/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MTA440C9 �Autor  �Daniel R. Melo      � Data �  12/06/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Ponto de entrada durante a liberacao do pedido de venda     ���
���          �Utilizado para atualizar o campo C9_XBLQ                    ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE.                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function MTA440C9()

	Local aArea := GetArea()
	
	DbSelectArea('SC9')
	DbSetOrder(1) // SC9_FILIAL, C9_PEDIDO, C9_ITEM, C9_SEQUEN, C9_PRODUTO, R_E_C_N_O_, D_E_L_E_T_
	If DbSeek(xFilial('SC9')+SC5->C5_NUM)

		While SC9->(!EOF()) .and. SC9->C9_PEDIDO = SC5->C5_NUM 
			
			If ALLTRIM(SC5->C5_ORIGEM)='MANUAL' .AND. (SC5->C5_TIPO$'B/D' .OR. SC5->C5_TPPED $ GETMV("MV_HTPPED") .OR. SC5->C5_POLCOM $ GETMV("MV_HPOLCOM"))
				
				RecLock('SC9', .F.)
					SC9->C9_XBLQ   := "L"
				MsUnLock()
				
				If SC6->C6_ENTREG == Ctod("  /  /    ")
					RecLock('SC9', .F.)
						SC9->C9_DATENT := SC5->C5_FECENT
					MsUnLock()
				End
			
			End
			
			SC9->(DbSkip())

		EndDo

	EndIf

	RestArea(aArea)

Return .T.