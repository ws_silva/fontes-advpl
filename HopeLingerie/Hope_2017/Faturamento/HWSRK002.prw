#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HWSRK001  �Autor  �Bruno Parreira      � Data �  01/12/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa utilizado para importar os pedidos de venda B2B    ���
���          �da RAKUTEN                                                  ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HWSRK002()
Local aWsRet   := {}
Local cWsRet     := ""

Private oWs      := NIL
Private cLog     := ""
Private cPerg  := "HWSRK002"
Private cTime    := ""
Private cDtTime  := ""
Private cPlB2BV  := ""
Private cTpB2BV  := ""
Private cPlB2BF  := ""
Private cTpB2BF  := ""
Private cPlB2BH  := ""
Private cTpB2BH  := ""
Private cPlB2BL  := ""
Private cTpB2BL  := ""
Private cPlB2B1  := ""
Private cTpB2B1  := ""
Private cAmzPk   := ""
Private cTpOper  := ""
Private cChvA1   := ""
Private cChvA2   := ""
Private nErro    := 0
Private nGerados := 0

lMenu := .f.
If Select("SX2") <> 0
	lMenu := .t.
Else
	Prepare Environment Empresa "01" Filial "0101"
Endif

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

GravaLog("In�cio do processamento...") 

oWsClient := WSPedido():New()

//cChvA1  := AllTrim(SuperGetMV("MV_XRA1B2C",.F.,"8B3CD696-D70A-4E59-8E2D-24A96ED8115E")) //Chave A1 para validar acesso ao webservice
//cChvA2  := AllTrim(SuperGetMV("MV_XRA2B2C",.F.,"2DD1CBF7-4B95-43BA-9F16-2C295E80E8CF")) //Chave A2 para validar acesso ao webservice
//cCnB2BV := AllTrim(SuperGetMV("MV_XRCN2BV",.F.,"007")) //Canal de vendas dos clientes do B2B Varejo
cPlB2BV := AllTrim(SuperGetMV("MV_XRPL2BV",.F.,"001")) //Politica comercial dos pedidos B2B Varejo
cTpB2BV := AllTrim(SuperGetMV("MV_XRTP2BV",.F.,"032")) //Tipo de Pedido dos pedidos B2B Varejo
cPlB2BF := AllTrim(SuperGetMV("MV_XRPL2BF",.F.,"050")) //Politica comercial dos pedidos B2B Franquia
cTpB2BF := AllTrim(SuperGetMV("MV_XRTP2BF",.F.,"030")) //Tipo de Pedido dos pedidos B2B Franquia
cPlB2BH := AllTrim(SuperGetMV("MV_XRPL2BH",.F.,"050")) //Politica comercial dos pedidos B2B Hope Store
cTpB2BH := AllTrim(SuperGetMV("MV_XRTP2BH",.F.,"033")) //Tipo de Pedido dos pedidos B2B Hope Store
cPlB2BL := AllTrim(SuperGetMV("MV_XRPL2BL",.F.,"053")) //Politica comercial dos pedidos B2B Loja Propria
cTpB2BL := AllTrim(SuperGetMV("MV_XRTP2BL",.F.,"047")) //Tipo de Pedido dos pedidos B2B Loja Propria
cPlB2B1 := AllTrim(SuperGetMV("MV_XRPL2B1",.F.,"054")) //Politica comercial dos pedidos B2B Hope 1.0 
cTpB2B1 := AllTrim(SuperGetMV("MV_XRTP2B1",.F.,"023")) //Tipo de Pedido dos pedidos B2B Hope 1.0
cAmzPk  := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking

cTabVar := SuperGetMV("MV_XTABVAR",.F.,"90UN-5")
cTabFra := SuperGetMV("MV_XTABFRA",.F.,"150K")
cTabHps := SuperGetMV("MV_XTABHPS",.F.,"311B")
cTabLoj := SuperGetMV("MV_XTABLOJ",.F.,"150K")
cTabHp1 := SuperGetMV("MV_XTABHP1",.F.,"150K")

cPrzImed := AllTrim(SuperGetMV("MV_XPRZIME",.F.,"002")) //Codigo Prazo Imediato

//cNtB2C := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCnB2BV,1,"11116001")

cTpOper := AllTrim(SuperGetMV("MV_XRTOV2C",.F.,"01")) //Tipo de Operacao de venda para pedidos B2C

If !lMenu
	GravaLog("Executando funcao ListarNovos automatica")
	If oWsClient:ListarNovos(0,7,,cChvA1,cChvA2)  // Codigo Loja, Status Pedido	
		cWsRet := oWsClient:oWSListarNovosResult:cDescricao
		If "SUCESSO" $ UPPER(cWsRet)	
			aWsRet := aClone(oWsClient:oWSListarNovosResult:oWSLista:oWSClsPedido)
			IMPORTA(aWsRet)
		Else
			GravaLog("Problema nos parametros da fun��o ListarNovos: "+cWsRet)
		EndIf		
	Else
		GravaLog("Erro na funcao ListarNovos: "+GetWSCError())
		Return		
	EndIf
Else
	GravaLog("Executando via menu.")
	AjustaSX1(cPerg)
	If Pergunte(cPerg)
		If mv_par01 = 1
			GravaLog("Executando Listar via menu.")
			If oWsClient:Listar(0,Val(mv_par02),7,,cChvA1,cChvA2)  // Codigo Loja, Codigo Pedido, Status Pedido	
				cWsRet := oWsClient:oWSListarResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)	
					aWsRet := aClone(oWsClient:oWSListarResult:oWSLista:oWSClsPedido)
					Processa( {|| IMPORTA(aWsRet,.F.) }, "Aguarde...", "Processando...",.F.)
				Else
					GravaLog("Problema nos parametros da fun��o Listar: "+cWsRet)
				EndIf		
			Else
				MsgAlert("Erro Listar: "+GetWSCError(),"Erro")
				GravaLog("Erro na funcao Listar via menu: "+GetWSCError())
				Return		
			EndIf
		ElseIf mv_par01 = 2
			GravaLog("Executando ListarNovos via menu.")
			If oWsClient:ListarNovos(0,7,,cChvA1,cChvA2)  // Codigo Loja, Status Pedido	
				cWsRet := oWsClient:oWSListarNovosResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)	
					aWsRet := aClone(oWsClient:oWSListarNovosResult:oWSLista:oWSClsPedido)
					Processa( {|| IMPORTA(aWsRet,.F.) }, "Aguarde...", "Processando...",.F.)
				Else
					GravaLog("Problema nos parametros da fun��o ListarNovos: "+cWsRet)
				EndIf		
			Else
				MsgAlert("Erro ListarNovos: "+GetWSCError(),"Erro")
				GravaLog("Erro na funcao ListarNovos via menu: "+GetWSCError())
				Return		
			EndIf
		Else
			GravaLog("Executando Listar SZT via menu.")
			Processa( {|| IMPORTASZT() }, "Aguarde...", "Processando...",.F.)		
		EndIf
		
		If lMenu
			If mv_par01 <> 1
				If nGerados > 0
					MsgInfo("Foram importados "+AllTrim(Str(nGerados))+" pedidos com sucesso.","Aviso")
					GravaLog("Foram importados "+AllTrim(Str(nGerados))+" pedidos com sucesso.","Aviso")
				Else
					MsgInfo("N�o foram encontrados pedidos aptos a importar.","Aviso")
					GravaLog("N�o foram encontrados pedidos aptos a importar.","Aviso")
				EndIf	
			EndIf
		Endif	
	Else
		GravaLog("Processo cancelado pelo usu�rio.")
	EndIf			
Endif

GravaLog("Fim do processamento.")
		
MemoWrite("\log_rakuten\B2B\"+cDtTime+"_IMP.log",cLog)

If !lMenu
	Reset Environment
EndIf	

Return

Static Function IMPORTASZT()
Local nCont := 0
Local nPed  := 0

DbSelectArea("SZT")
DbSetOrder(2)
If DbSeek(xFilial("SZT")+"B2B")
	While SZT->(!EOF()) .And. SZT->ZT_ORIGEM = "B2B"
		If Empty(SZT->ZT_STATUS)
			nCont++
		EndIf
		SZT->(DbSkip())
	EndDo	
EndIf		

If nCont = 0
	GravaLog("N�o h� itens a serem improtados na SZT.")
	Return
EndIf

ProcRegua(nCont)

DbSelectArea("SZT")
DbSetOrder(2)
If DbSeek(xFilial("SZT")+"B2B")
	While SZT->(!EOF()) .And. SZT->ZT_ORIGEM = "B2B"
		If Empty(SZT->ZT_STATUS)
			IncProc()
			DbSelectArea("SC5")
			DbOrderNickName("XPEDRAK")
			If DbSeek(xFilial("SC5")+SZT->ZT_PEDIDO)
				GravaLog("Pedido Rakuten: "+SZT->ZT_PEDIDO+" j� importado no protheus. Alterando status...") 	
				RecLock("SZT",.F.)
				SZT->ZT_STATUS = "X"
				MsUnlock()
				SZT->(DbSkip())
				Loop				
			EndIf	
			//BEGIN TRANSACTION
			nPed++
			If oWsClient:Listar(0,Val(SZT->ZT_PEDIDO),7,,cChvA1,cChvA2)  // Codigo Loja, Codigo Pedido, Status Pedido
				cWsRet := oWsClient:oWSListarResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)
					aWsRet := aClone(oWsClient:oWSListarResult:oWSLista:oWSClsPedido)
					IMPORTA(aWsRet,.T.,SZT->ZT_EMISSAO)
				Else
					GravaLog("Problema nos parametros da fun��o Listar: "+cWsRet)
				EndIf			
			Else
				GravaLog("Pedido "+SZT->ZT_PEDIDO+".Erro na funcao Listar SZT via menu: "+GetWSCError())		
			EndIf
			//END TRANSACTION
			If nPed > 20
				cTime   := Time()
				cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
				MemoWrite("\log_rakuten\B2B\"+cDtTime+"_IMP.log",cLog)
				cLog := "_"
				nPed := 0
			EndIf		
		EndIf
		SZT->(DbSkip())
	EndDo
EndIf

Return

Static Function IMPORTA(aWsRet,lSZT,dEmissao)
Local nx,ny    := 0
Local cNroPed  := ""
Local cCGC     := ""
Local cCodCli  := ""
Local cLojCli  := ""
Local cNovCli  := ""
Local cTipPes  := ""
Local cNomCli  := ""
Local nPosP    := 0
Local cProd    := ""
Local cRef     := ""
Local cCor     := ""
Local cTam     := ""
Local cPedRak  := ""
Local cCanal   := ""
Local cNature  := ""
Local cPolitc  := ""
Local cTipPed  := ""
Local cCndPag  := ""
Local cCdEnd   := ""
Local cCdMun   := ""
Local cCdEnt   := ""
Local cCdHop   := ""
Local cCdGer   := ""
Local cCdTip   := ""
Local cCdGrp   := ""
Local cCdDiv   := ""
Local cCdReg   := ""
Local cCdMac   := ""
Local cNtB2C   := ""
Local cNmCan   := ""
Local cNmTip   := ""
Local cNmGrp   := ""
Local cNmDiv   := ""
Local cNmReg   := ""
Local cNmMac   := ""
Local cEndere  := ""
Local cComple  := ""
Local cBairro  := ""
Local cEstado  := ""
Local cCEP     := ""
Local cMunici  := ""
Local cEmail   := ""
Local cDDDTel  := ""
Local cNumTel  := ""
Local cDDDCel  := ""
Local cNumCel  := ""
Local cTES     := ""
Local cCodPed  := ""
Local nParce   := 0
Local cCdPrz   := ""
Private lMsErroAuto := .F.

If Len(aWsRet) > 0
	If lMenu .And. !lSZT
		ProcRegua(Len(aWsRet))
	EndIf
	
	For nx := 1 to Len(aWsRet)
		If lMenu .And. !lSZT
			IncProc()
		EndIf
			
		cPedRak := StrZero(aWsRet[nx]:nPedidoCodigo,6)
		
		GravaLog("Iniciando a importa��o do pedido: "+cPedRak)
		
		DbSelectArea("SC5")
		DbOrderNickName("XPEDRAK")
		If DbSeek(xFilial("SC5")+cPedRak)
			GravaLog("Pedido "+cPedRak+" j� importado no protheus")
			If lMenu
				If mv_par01 = 1
					If !MsgYesNo("Pedido "+cPedRak+" j� importado. Importar novamente?","Aviso")
						Exit
					EndIf
				ElseIf mv_par01 = 3
					DbSelectArea("SZT")
					DbSetOrder(1)
					If DbSeek(xFilial("SZT")+cPedRak)
						RecLock("SZT",.F.)
						SZT->ZT_STATUS = "X"
						MsUnlock()
					EndIf
					Loop
					//ValidPed(cPedRak)
				Else
					//ValidPed(cPedRak)
					Loop		
				EndIf
			Else
				//ValidPed(cPedRak)
				Loop
			EndIf
		EndIf
		
		Do Case
			Case AllTrim(aWsRet[nx]:oWSPessoa:Value) = 'PessoaF�sica'
				cTipPes := "F"
				cCGC    := aWsRet[nx]:cCPF
			Case AllTrim(aWsRet[nx]:oWSPessoa:Value) = 'PessoaJur�dica'
				cTipPes := "J"
				cCGC    := aWsRet[nx]:cCNPJ
			Otherwise
				GravaLog("Tipo de cliente diferente de F ou J. Pedido: "+cPedRak)
				Loop	 
		EndCase
		
		/*Do Case
			Case !Empty(aWsRet[nx]:cCPF)
				cTipPes := "F"
				cCGC    := AllTrim(aWsRet[nx]:cCPF)
			Case !Empty(aWsRet[nx]:cCNPJ)
				cTipPes := "J"
				cCGC    := AllTrim(aWsRet[nx]:cCNPJ)
			Otherwise
				GravaLog("Tipo de cliente diferente de F ou J. Pedido: "+cPedRak)
				Loop
		EndCase*/

		DbSelectArea("SA1")
		DbSetOrder(3)
		If DbSeek(xFilial("SA1")+cCGC)
			cCodCli := ""
			cLojCli := ""
			
			If SA1->A1_MSBLQL = "1" //Inativo
				While SA1->(!EOF()) .And. SA1->A1_CGC = cCGC
					If SA1->A1_MSBLQL <> "1"
						cCodCli := SA1->A1_COD
						cLojCli := SA1->A1_LOJA
						Exit
					EndIf
					SA1->(DbSkip())
				EndDo
			EndIf
			
			If Empty(cCodCli)
				GravaLog("Cliente ativo n�o encontrado. Pedido: "+cPedRak)
				Loop
			EndIf
			
			cNature := ""
			
			If !Empty(SA1->A1_NATUREZ)
				cNature := SA1->A1_NATUREZ
			EndIf
			
			cCanal := AllTrim(SA1->A1_XCANAL)
			nParce := aWsRet[nx]:nParceiroCodigo
			cTabela := ""
			cTab    := ""

			Do Case
				Case nParce = 29 //Varejo
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11113001")
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabVar,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabVar,4,"") +" - "+cTabVar+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabVar,4,"") 
					cCodPed := "B2BV-"
					cPolitc := cPlB2BV
					cTipPed := cTpB2BV
				Case nParce = 21 //Franquia
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111001")
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabFra,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabFra,4,"") +" - "+cTabFra+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabFra,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2BF
					cTipPed := cTpB2BF
				Case nParce = 23 //Hope Store
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111001")
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHps,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHps,4,"") +" - "+cTabHps+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabHps,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2BH
					cTipPed := cTpB2BH
				Case nParce = 24 //Loja Propria
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111401")
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabLoj,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabLoj,4,"") +" - "+cTabLoj+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabLoj,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2BL
					cTipPed := cTpB2BL
				Case nParce = 22 //Hope 1.0
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111001")
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHp1,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHp1,4,"") +" - "+cTabHp1+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabHp1,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2B1
					cTipPed := cTpB2B1				
				Otherwise
					GravaLog("Canal "+cCanal+" nao esta apto a ser importado. Pedido: "+cPedRak)
					Loop
			End Case
		Else
			GravaLog("Cliente n�o encontrado. Pedido: "+cPedRak)
			Loop
		EndIf
		
		If Empty(cNature)
			GravaLog("Natureza n�o encontrada. Cliente: "+cCodCli+" Canal:"+cCanal+" Pedido: "+cPedRak)
			Loop
		EndIf
		
		cNroPed := GetMV("MV_XNMPVRK")
		
		DbSelectArea("SE4")
		DbOrderNickName("XCONDRAK")
		If DbSeek(xFilial("SE4")+SubStr(AllTrim(Str(aWsRet[nx]:nPagamento))+SPACE(3),1,3)+SubStr(AllTrim(Str(aWsRet[nx]:nQtdeParcelas))+SPACE(3),1,2))
			cCndPag := SE4->E4_CODIGO
		Else
			cCndPag := ""
			GravaLog("Condicao de pagamento nao encontrada. Pedido: "+cPedRak)
			Loop	
		EndIf
		
		DbSelectArea("SZ6")
		DbSetOrder(4)
		If DbSeek(xFilial("SZ6")+cPolitc+AllTrim(Str(aWsRet[nx]:nPrazoEntrega)))
			cCdPrz  := SZ6->Z6_PRAZO
			cDCdPrz := SZ6->Z6_DESC
			If aWsRet[nx]:nPrazoEntrega = 15 // PROGRAMADO
				cDataEnt := SubStr(aWsRet[nx]:cDataEntrega,1,4)+SubStr(aWsRet[nx]:cDataEntrega,6,2)+SubStr(aWsRet[nx]:cDataEntrega,9,2)
			Else
				If lSZT
					cDataEnt := DtoS(dEmissao)
				Else
					cDataEnt := DtoS(DDATABASE)
				EndIf		
			EndIf
		Else
			cCdPrz   := ""
			If lSZT
				cDataEnt := DtoS(dEmissao)
			Else
				cDataEnt := DtoS(DDATABASE)
			EndIf
			cDCdPrz  := ""	
		EndIf  
		
		nPercDesc := Round(((aWsRet[nx]:nDesconto / aWsRet[nx]:nValorSubTotal) * 100),2)
		nDesconto := aWsRet[nx]:nDesconto
		
		aCabec := {}
		cDataEmi := SubStr(aWsRet[nx]:cData,1,4)+SubStr(aWsRet[nx]:cData,6,2)+SubStr(aWsRet[nx]:cData,9,2)
		
		DbSelectArea("SA1")
		DbSetOrder(1)
		DbSeek(xFilial("SA1")+cCodCli+cLojCli)
		
	   	aadd(aCabec,{"C5_NUM"    ,cNroPed,Nil})
		aadd(aCabec,{"C5_TIPO"   ,"N",Nil})
		aadd(aCabec,{"C5_CLIENTE",cCodCli,Nil})		
		aadd(aCabec,{"C5_LOJACLI",cLojCli,Nil})		
		aadd(aCabec,{"C5_LOJAENT",cLojCli,Nil})
		aadd(aCabec,{"C5_POLCOM" ,cPolitc,Nil})
		aadd(aCabec,{"C5_TPPED"  ,cTipPed,Nil})
		aadd(aCabec,{"C5_CONDPAG",cCndPag,Nil})
		aadd(aCabec,{"C5_NATUREZ",cNature,Nil})
		aadd(aCabec,{"C5_XPEDRAK",StrZero(aWsRet[nx]:nPedidoCodigo,6),Nil})
		aadd(aCabec,{"C5_XPEDWEB",cCodPed+aWsRet[nx]:cPedidoCodigoCliente,Nil})
		aadd(aCabec,{"C5_FRETE"  ,aWsRet[nx]:nValorFreteCobrado,Nil})
		//aadd(aCabec,{"C5_DESCONT",aWsRet[nx]:nDesconto,Nil})
		aadd(aCabec,{"C5_DESC1"  ,nPercDesc,Nil})
		aadd(aCabec,{"C5_XCREDRK",aWsRet[nx]:nValorVale,Nil})
		aadd(aCabec,{"C5_XNSU"   ,aWsRet[nx]:oWsCartao:cCartID,Nil})
		aadd(aCabec,{"C5_XAUTCAR",aWsRet[nx]:oWsCartao:cCarAutorizacaoCodigo,Nil})
		If lSZT
			aadd(aCabec,{"C5_EMISSAO",dEmissao,Nil})
		Else 
			aadd(aCabec,{"C5_EMISSAO",StoD(cDataEmi),Nil})
		EndIf	
		aadd(aCabec,{"C5_FECENT" ,StoD(cDataEnt),Nil})
		
		aadd(aCabec,{"C5_XCANAL" ,SA1->A1_XCANAL,Nil})
		aadd(aCabec,{"C5_XCANALD",SA1->A1_XCANALD,Nil})
		aadd(aCabec,{"C5_XCODREG",SA1->A1_XCODREG,Nil})
		aadd(aCabec,{"C5_XDESREG",SA1->A1_XDESREG,Nil})
		aadd(aCabec,{"C5_XMICRRE",SA1->A1_XMICRRE,Nil})
		aadd(aCabec,{"C5_XMICRDE",SA1->A1_XMICRDE,Nil})
		
		//aadd(aCabec,{"C5_PRAZO"  ,cCdPrz,Nil})
		aadd(aCabec,{"C5_XTABRAK",cTabela,Nil})
		aadd(aCabec,{"C5_ORIGEM","B2B",Nil})
		
		If !Empty(cTab)
			aadd(aCabec,{"C5_TABELA",cTab,Nil})
		EndIf
		
		aItens := {}

		lContinua := .T.
		cItem     := "01"
		//cDataEnt  := SubStr(aWsRet[nx]:cDataEntrega,1,4)+SubStr(aWsRet[nx]:cDataEntrega,6,2)+SubStr(aWsRet[nx]:cDataEntrega,9,2)
		
		aWsItens := aClone(aWsRet[nx]:oWsItens:oWsItem)
		
		For ny := 1 to len(aWsItens)
			cProd := aWsItens[ny]:cItemPartNumber   //Codigo do produto que vem da RAKUTEN
			nPosP := Len(cProd)
			cProd := AllTrim(If(SubStr(cProd,1,4)='OUT_',SubStr(cProd,5),cProd)) //Retira o OUT_ caso seja um produto de promocao
			aWsItens[ny]:cItemPartNumber := cProd
		Next
		
		ASORT(aWsItens, , , { |x,y| x[cItemPartNumber] < y[cItemPartNumber] } )
		
		For ny := 1 to Len(aWsItens)
		
			//Formato do codigo do produto vindo da RAKUTEN. Ex.: OUT_12345678_PRT_P  -> PROMOCAO_REFERENCIA_COR_TAMANHO
			
			cProd := aWsItens[ny]:cItemPartNumber   //Codigo do produto que vem da RAKUTEN
			nPosP := Len(cProd)
			cProd := AllTrim(If(SubStr(cProd,1,4)='OUT_',SubStr(cProd,5),cProd)) //Retira o OUT_ caso seja um produto de promocao
	
			DbSelectArea("SB1")
			DbSetOrder(1)
			If !DbSeek(xFilial("SB1")+cProd)
		    	DbSelectArea("SB1")
		    	DbOrderNickName("YFORMAT")
				If !DbSeek(xFilial("SB1")+cProd)
					GravaLog("Produto: "+cProd+" n�o encontrado. Pedido: "+cPedRak)
					lContinua := .F.
					Exit
				EndIf
	    	EndIf
	    	
	    	cTES  := MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,)
	    	If Empty(cTes)
	    		GravaLog("TES nao encontrada para o produto: "+SB1->B1_COD+". Cliente Protheus: "+cCodCli+"/"+cLojCli+" Tp Operacao: "+cTpOper+". Pedido: "+cPedRak)
				lContinua := .F.
				Exit
	    	EndIf
	    	
	    	nPrUnit := aWsItens[ny]:nItemValorFinal
	    	nPrVend := aWsItens[ny]:nItemValorFinal
	    	
			If !Empty(cTab)
				DbSelectArea("DA1")
				DbSetOrder(1)
				If DbSeek(xFilial("DA1")+cTab+SB1->B1_COD)
					nPrUnit := DA1->DA1_PRCVEN
				EndIf
			EndIf
			
			If nPercDesc > 0
				nPrVend := Round(aWsItens[ny]:nItemValorFinal - ((nPercDesc/100)*aWsItens[ny]:nItemValorFinal),6)
			EndIf
			
			aLinha := {}
			aadd(aLinha,{"C6_ITEM",cItem,Nil})			
			aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD,Nil})			
			aadd(aLinha,{"C6_QTDVEN",aWsItens[ny]:nItemQtde,Nil})			
			aadd(aLinha,{"C6_PRCVEN",nPrVend,Nil})			
			aadd(aLinha,{"C6_PRUNIT",nPrUnit,Nil})			
			aadd(aLinha,{"C6_VALOR",ROUND(aWsItens[ny]:nItemQtde*nPrVend,2),Nil})
			aadd(aLinha,{"C6_OPER" ,cTpOper      ,Nil})			
			aadd(aLinha,{"C6_TES",cTES,Nil})
			aadd(aLinha,{"C6_NUM",cNroPed,Nil})
			aadd(aLinha,{"C6_LOCAL",cAmzPk,Nil})
			aadd(aLinha,{"C6_ENTREG",StoD(cDataEnt),Nil})
			aadd(aLinha,{"C6_GRADE","S",Nil})
			aadd(aLinha,{"C6_ITEMGRD",strzero(val(cItem),3),Nil})
			aadd(aItens,aLinha)
			
			cItem := SOMA1(cItem)
		Next
	
		If !lContinua
			GravaLog("Erro na importa��o dos itens do pedido: "+cPedRak)
			Loop		
		EndIf

		For _i := 1 to len(aItens)
			If _i <> 1
				If SubStr(aitens[_i][2][2],1,11) = SubStr(aitens[_i-1][2][2],1,11)
					aItens[_i][1][2] := aItens[_i-1][1][2]
					aItens[_i][9][2] := SOMA1(aItens[_i-1][9][2])
				Else
					aItens[_i][1][2] := SOMA1(aItens[_i-1][1][2])
					aItens[_i][9][2] := "001"
				Endif
			Endif
		Next		
		
		BEGIN TRANSACTION
	
		MATA410(aCabec,aItens,3)
			
		If lMsErroAuto
			GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedRak)
			If lMenu
				MostraErro()
			Else
				MostraErro("\log_rakuten\B2B\",cDtTime+"_PED.log")
			EndIf	
			nErro++
			lMsErroAuto := .T.
		Else
			If lMenu
				If mv_par01 == 1
					MsgInfo("Pedido importado com sucesso. N� Protheus: "+cNroPed,"Aviso")
				EndIf
			EndIf
			If !Empty(cCdPrz)
				DbSelectArea("SC5")
				DbSetOrder(1)
				If DbSeek(xFilial("SC5")+cNroPed)
					RecLock("SC5",.F.)
					SC5->C5_PRAZO   := cCdPrz  //Gravo o Prazo depois pois da erro no execauto
					SC5->C5_XDPRAZO := cDCdPrz
					MsUnlock()
					/*If nDesconto > 0
						AjustaDesc(cNroPed,nDesconto)
					EndIf*/	
				EndIf
			EndIf
			
			GravaLog("Pedido Protheus n� "+cNroPed+" foi gerado com sucesso. Pedido: "+cPedRak) 
			nGerados++
			cNroPed := SOMA1(SubStr(cNroPed,2,5))
			PutMV("MV_XNMPVRK","R"+cNroPed)
			
			DbSelectArea("SZT")
			DbSetOrder(1)
			If DbSeek(xFilial("SZT")+cPedRak)
				RecLock("SZT",.F.)
				SZT->ZT_STATUS = "X"
				MsUnlock()
			EndIf
			
			//ValidPed(cPedRak)
		EndIf
		
		END TRANSACTION	
	Next
EndIf

Return

Static Function AjustaDesc(cPedVen,nDescont)
Local aArea  := GetArea()
Local cQuery := ""

cQuery := "select SUM(C6_VALOR) AS VLRVEN,SUM(C6_QTDVEN*C6_PRUNIT) AS VLRTAB "
cQuery += CRLF + "from "+RetSqlName("SC6")+" SC6 "
cQuery += CRLF + "where D_E_L_E_T_ = '' "
cQuery += CRLF + "and C6_NUM = '"+cPedVen+"' "
cQuery += CRLF + "group by C6_NUM " 

MemoWrite("HWSRK002_AjustaDesc.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

DbSelectArea("TMP")

If TMP->(!EOF())
	nDescAtu := TMP->VLRTAB - TMP->VLRVEN
	nDif     := nDescont - nDescAtu
	If nDif > 0
		DbSelectArea("SC6")
		DbSetOrder(1)
		If DbSeek(xFilial("SC6")+cPedVen)
			nValItem := SC6->C6_VALOR - nDif
			nValUnit := nValItem / SC6->C6_QTDVEN
			RecLock("SC6",.F.)
			SC6->C6_PRCVEN := nValUnit
			SC6->C6_VALOR  := nValItem
			MsUnlock()
		EndIf
	EndIf
EndIf

RestArea(aArea)

Return

/*
Static Function ValidPed(cPedido)

If oWsClient:Validar(0,Val(cPedido),'?',cChvA1,cChvA2)
	GravaLog("Executado comando Validar para o pedido "+cPedido)
Else
	GravaLog("Erro na funcao Validar: "+GetWSCError())
EndIf

Return
*/
Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cMsg)
cLog += CRLF+cDtHora+": HWSRK002 - "+cMsg

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Funcao:"        ,"Funcao:"        ,"Funcao:"        ,"mv_ch1","N",1,0,2,"C","","","","" ,"mv_par01","Listar","Listar","Listar","","ListarNovos","ListarNovos","ListarNovos","Listar SZT","Listar SZT","Listar SZT","","","","","","",{"Informe a funcao para importacao",""},{""},{""},"")  
PutSx1(cPerg,"02","Pedido RAKUTEN:","Pedido Rakuten:","Pedido Rakuten:","Mv_ch2","C",6,0,0,"G","","","","N","Mv_par02",""   ,""   ,""   ,"",""   ,""   ,""   ,"","","","","","","","","",{"Informe o numero do pedido RAKUTEN",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)