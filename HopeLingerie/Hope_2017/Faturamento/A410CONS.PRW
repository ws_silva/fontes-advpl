#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

User Function A410CONS()

	aBotao := {}
	AAdd( aBotao, { "PRECO", { || U_HITENS() }, "Itens" } )
	AAdd( aBotao, { "PRECO", { || U_HIMPED() }, "Privalia" } )
	AAdd( aBotao, { "PRECO", { || U_HIMPBN() }, "Beneficiamento" } )
	AAdd( aBotao, { "PRECO", { || U_HEXCPED()}, "Excluir" } )

Return( abotao)

User Function HEXCPED()

	Local _i
	Local nPosPrd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRODUTO'})

//PutSx1("EXCPE","01","Ref De? ","Aprovador ? ","Aprovador ? ","Mv_ch1",TAMSX3("B4_COD")[3],TAMSX3("B4_COD")[1],TAMSX3("B4_COD")[2],0,"G","","SB4","","N","mv_par01","","","","","","","","","","","","","","","","",{"Informe a Refer�ncia.",""},{""},{""},"")
//PutSx1("EXCPE","02","Ref Ate? ","Aprovador ? ","Aprovador ?","Mv_ch2",TAMSX3("B4_COD")[3],TAMSX3("B4_COD")[1],TAMSX3("B4_COD")[2],0,"G","","SB4","","N","mv_par02","","","","","","","","","","","","","","","","",{"Informe a Refer�ncia.",""},{""},{""},"")
	Pergunte("EXCPED",.T.)

	For _i := 1 to len(aCols)
		If SubStr(aCols[_i,nPosPrd],1,8) >= alltrim(MV_PAR01) .and. SubStr(aCols[_i,nPosPrd],1,8) <= alltrim(MV_PAR02)
			aCols[_i,len(aheader)+1] := .T.
		Endif
	Next

Return


User Function HITENS
	Local _i
	Local nPosQtd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_QTDVEN'})
	Local nPosPven    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRCVEN'})
	Local nPosPUni    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRUNIT'})

	_qtdit := 0
	_ptotal:= 0
	_pvend := 0

	For _i := 1 to len(acols)
		If ACOLS[_i,len(AHEADER)+1] == .F.
			_qtdit += aCols[_i,nPosQtd]
			_ptotal+= aCols[_i,nPosQtd]*aCols[_i,nPosPUni]
			_pvend += aCols[_i,nPosQtd]*aCols[_i,nPosPven]
		Endif
	Next

	Alert("Quantidade de Itens: "+alltrim(str(_qtdit))+chr(10)+"% Desconto:"+str(ROUND(100-((_pvend*100)/_ptotal),2)))

Return

User Function HIMPED()

	Local nx
	Private oProcesso 	:= Nil

	oProcesso := MsNewProcess():New({|lEnd| HPCPPTXTA() },"Lendo Arquivo(s)...")//Funcao de leitura do arquivo que transforma o conteudo lido em Array
	oProcesso:Activate()

Return

Static Function HPCPPTXTA()

	Local alinha:= {}

	oProcesso:SetRegua1( 2 )
	alinha:= HPCPPTXTB() //Leitura do arquivo(TXT)

	If cErro1<>'1'
		If Len(alinha) > 0
			oProcesso:IncRegua1("Gravado Informacoes: ")
//			aSort(alinha,,, { |x,y| y[1]+y[2] > x[1]+x[2]} )
			HPCPPTXTC(@alinha)
		Else
			MsgSTop("O Arquivo informado esta inv�lido !","TXT")
			Return nil
		EndIf
	EndIf

return nil

Static Function HPCPPTXTB()

	Local cTypeTXT	:=	"Arquivos TXT|*.TXT|Todos os Arquivos|*.*"
	Local nHdlLog 	:= 0
	Local alinha	:= {}
	Local nLast		:= 0
	Local nlinha	:= 0
	Local aTemp		:= {}
	Local nPos		:= 0
	Local cErro		:= ""
	Local dDateImp	:= ""
	Public cErro1	:= ""

	cTXTFile := cGetFile("Arquivos TXT|*.TXT",OemToAnsi("Selecione o arquivo "),0,"C:\",,GETF_LOCALHARD+GETF_NETWORKDRIVE)

	If Empty(cTXTFile)
		Return alinha
	EndIf

	oProcesso:IncRegua1("Lendo Arquivo: "+cTXTFile ) // Atualiza a barra de processamento

	If File(cTXTFile)
		nHdlLog := FOpen(cTXTFile, 2) //Abertura do arquivo
		If nHdlLog == -1 //verifica se abriu com sucesso
			cErro += "N�o foi pross�vel abrir o Arquivo selecionado ["+Alltrim(cTXTFile)+"] !"+ Chr(13) + Chr(10)
			Return nil
		Else
			FT_FUSE(cTXTFile)
			FT_FGOTOP()
			nLast := FT_FLastRec()		// Retorna o numero de linhas do arquivo
			oProcesso:SetRegua2(nLast) //Define o limite da regua

			While !FT_FEOF()

				cLineCpos := AllTrim(FT_FREADLN())
				oProcesso:IncRegua2("Lendo "+alltrim(str(nlinha))+" de "+ alltrim(str( nLast )))

				/* Tratamento para limpar sujeiras */
				cLine := StrTran(cLineCpos,";"," ;")
				aTemp := StrTokArr(cLine,";")

				DbSelectArea("SB1")
				SB1->(DbSetOrder(5))
				SB1->(DbSeek(xfilial("SB1")+Substr(cLineCpos,1,13)))
				if SB1->(!eof())
					Aadd(alinha, {Substr(cLineCpos,1,13),;							//N_ORDEM
					SubStr(cLineCpos,14,len(cLineCpos)-13),;
						SB1->B1_COD;							//FASE PROTHEUS
					})
					nlinha++
				ELse
					If !MsgYesNo( 'C�digo de barra '+Substr(cLineCpos,1,13)+' n�o encontrado! Continua? ')
						Exit //Return nil
					Endif
   		           //Help( ,, 'Help',, 'C�digo de barra '+Substr(cLineCpos,1,13)+' n�o encontrado!',1,0)
				Endif
				FT_FSKIP()
			Enddo
		EndIf
	Else
		cErro += "Arquivo selecionado ["+Alltrim(cTXTFile)+"] n�o encontrado !"+Chr(13)+Chr(10)
		Return nil
	EndIf

	If !Empty(cErro)
		EECView(cErro)
	EndIf

Return alinha

Static Function HPCPPTXTC(alinha)

	Local nx := 0
	Local ny := 0
	Local _cItem := "  "
	Local nPosPrd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRODUTO'})
	Local nPosQtd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_QTDVEN'})
	Local nPosDes    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_DESCRI'})
	Local nPosPrc    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRCVEN'})
	Local nPosPUn    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRUNIT'})
	Local nPosVal    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_VALOR'})
	Local nPosOpe    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_OPER'})
	Local nPosTES    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_TES'})
	Local nPosUM     := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_UM'})
	Local nPosDtEnt  := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_ENTREG'})

	ASORT(aLinha, , , { |x,y| x[3] < y[3] } )

	For ny:=1 to len(alinha)

		If Empty(aCols[len(acols),nPosPrd])
			ADEL(aCols,1)
			ASIZE(aCols,Len(aCols)-1)
		EndIf

		AADD(aCols,Array(Len(aHeader)+1))

		For nx:=1 to Len(aHeader)
			If IsHeadRec(Alltrim(aHeader[nx,2]))
				aCols[Len(aCols)][nx] := 0
			ElseIf IsHeadAlias(Alltrim(aHeader[nx,2]))
				aCols[Len(aCols)][nx] := "SC6"
			Else
				aCols[Len(aCols)][nx] := CriaVar(Alltrim(aHeader[nx,2]),.F.)
			Endif
		Next nx
		aCOLS[Len(aCols)][Len(aHeader)+1] := .F.

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+aLinha[ny,3])

		_cItem := Soma1(_cItem,TamSX3("C6_ITEM")[1])

//	aCols[len(aCols),1] := strzero(len(acols),2)
		aCols[len(aCols),1] := _cItem
		aCols[len(acols),nPosPrd] := SB1->B1_COD
		aCols[len(acols),nPosDes] := SB1->B1_DESC
		aCols[len(acols),nPosUM]  := SB1->B1_UM
		aCols[len(acols),nPosQtd] := val(aLinha[ny,2])

		DbSelectArea("SZ1")
		DbSetOrder(1)
		DbSeek(xfilial("SZ1")+M->C5_TPPED)

		_oper := SZ1->Z1_TPOPER

		If alltrim(_oper) = ""
			_oper := "01"
		Endif

		DbSelectArea("DA1")
		DbSetOrder(1)
		DbSeek(xfilial("DA1")+M->C5_TABELA+SB1->B1_COD)

		cTES  := MaTesInt(2,_oper,M->C5_CLIENTE,M->C5_LOJAENT,"C",SB1->B1_COD,)

		aCols[len(acols),nPosPrc] 	:= DA1->DA1_PRCVEN
		aCols[len(acols),nPosPUn] 	:= DA1->DA1_PRCVEN
		aCols[len(acols),nPosVal] 	:= Round(DA1->DA1_PRCVEN*val(aLinha[ny,2]),2)
		aCols[len(acols),nPosOpe] 	:= _oper
		aCols[len(acols),nPosTES] 	:= cTES
		aCols[len(acols),nPosDtEnt] := M->C5_FECENT

//		If ExistTrigger("C6_PRODUTO")
		RunTrigger(2,Len(aCols),,"C6_PRODUTO")
//		EndIf
//		If ExistTrigger("C6_QTDVEN")
		RunTrigger(2,Len(aCols),,"C6_QTDVEN")
//		EndIf

	Next ny
	
	_nPven		:= 0
	For yn := 1 to len(acols)
		
		_nPven		:= aCols[yn,nPosPUn]
		_nPVdesc	:= Round(_nPven*(1-(M->C5_DESC1/100)),2)
		
		aCols[yn,nPosPrc] 	:= _nPVdesc
		aCols[yn,nPosVal] 	:= NoRound((aCols[yn,nPosQtd]*round(_nPVdesc,2)),2)
		oGetDad:Refresh()
		
	Next ny

Return()

User Function HIMPBN

	Local nx
	Private oProcesso 	:= Nil

	oProcesso := MsNewProcess():New({|lEnd| HPCPPTXTW() },"Lendo Arquivo(s)...")//Funcao de leitura do arquivo que transforma o conteudo lido em Array
	oProcesso:Activate()

Return

Static Function HPCPPTXTW()

	Local alinha:= {}

	oProcesso:SetRegua1( 2 )
	alinha:= HPCPPTXTZ() //Leitura do arquivo(TXT)

	If cErro1<>'1'
		If Len(alinha) > 0
			oProcesso:IncRegua1("Gravado Informacoes: ")
//			aSort(alinha,,, { |x,y| y[1]+y[2] > x[1]+x[2]} )
			HPCPPTXTY(@alinha)
		Else
			MsgSTop("O Arquivo informado esta inv�lido !","TXT")
			Return nil
		EndIf
	EndIf

return nil

Static Function HPCPPTXTZ()

	Local cTypeTXT	:=	"Arquivos TXT|*.TXT|Todos os Arquivos|*.*"
	Local nHdlLog 	:= 0
	Local alinha	:= {}
	Local nLast		:= 0
	Local nlinha	:= 0
	Local aTemp		:= {}
	Local nPos		:= 0
	Local cErro		:= ""
	Local dDateImp	:= ""
	Public cErro1	:= ""

	cTXTFile := cGetFile("Arquivos TXT|*.TXT",OemToAnsi("Selecione o arquivo "),0,"C:\",,GETF_LOCALHARD+GETF_NETWORKDRIVE)

	If Empty(cTXTFile)
		Return alinha
	EndIf

	oProcesso:IncRegua1("Lendo Arquivo: "+cTXTFile ) // Atualiza a barra de processamento

	If File(cTXTFile)
		nHdlLog := FOpen(cTXTFile, 2) //Abertura do arquivo
		If nHdlLog == -1 //verifica se abriu com sucesso
			cErro += "N�o foi pross�vel abrir o Arquivo selecionado ["+Alltrim(cTXTFile)+"] !"+ Chr(13) + Chr(10)
			Return nil
		Else
			FT_FUSE(cTXTFile)
			FT_FGOTOP()
			nLast := FT_FLastRec()		// Retorna o numero de linhas do arquivo
			oProcesso:SetRegua2(nLast) //Define o limite da regua

			While !FT_FEOF()

				cLineCpos := AllTrim(FT_FREADLN())
				oProcesso:IncRegua2("Lendo "+alltrim(str(nlinha))+" de "+ alltrim(str( nLast )))

				/* Tratamento para limpar sujeiras */
				cLine := StrTran(cLineCpos,";"," ;")
				aTemp := StrTokArr(cLine,";")
				Aadd(alinha, {Substr(cLineCpos,1,15),;				//N_ORDEM
				SubStr(cLineCpos,16,9),;							//FASE PROTHEUS
				SubStr(cLineCpos,25,8);								//FASE PROTHEUS
				})
				nlinha++
				FT_FSKIP()
			Enddo
		EndIf
	Else
		cErro += "Arquivo selecionado ["+Alltrim(cTXTFile)+"] n�o encontrado !"+Chr(13)+Chr(10)
		Return nil
	EndIf

	If !Empty(cErro)
		EECView(cErro)
	EndIf

Return alinha

Static Function HPCPPTXTY(alinha)

	Local nx := 0
	Local ny := 0
	Local _cItem := "  "
	Local nPosPrd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRODUTO'})
	Local nPosQtd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_QTDVEN'})
	Local nPosDes    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_DESCRI'})
	Local nPosUM     := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_UM'})
	Local nPosLoc    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_LOCAL'})
	Local nPosPrc    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_PRCVEN'})
	Local nPosVal    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_VALOR'})
	Local nPosTES    := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_TES'})
	Local nPosDtEnt  := aScan(aHeader,{|x| AllTrim(x[2]) == 'C6_ENTREG'})

	For ny:=1 to len(alinha)

		If Empty(aCols[len(acols),nPosPrd])
			ADEL(aCols,1)
			ASIZE(aCols,Len(aCols)-1)
		EndIf

		AADD(aCols,Array(Len(aHeader)+1))

		For nx:=1 to Len(aHeader)
			If IsHeadRec(Alltrim(aHeader[nx,2]))
				aCols[Len(aCols)][nx] := 0
			ElseIf IsHeadAlias(Alltrim(aHeader[nx,2]))
				aCols[Len(aCols)][nx] := "SC6"
			Else
				aCols[Len(aCols)][nx] := CriaVar(Alltrim(aHeader[nx,2]),.F.)
			Endif
		Next nx
		aCOLS[Len(aCols)][Len(aHeader)+1] := .F.

		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+aLinha[ny,1])

		_cItem := Soma1(_cItem,TamSX3("C6_ITEM")[1])

//	aCols[len(aCols),1] := strzero(ny,2)
		aCols[len(aCols),1] := _cItem
		aCols[len(acols),nPosPrd] := SB1->B1_COD
		aCols[len(acols),nPosDes] := SB1->B1_DESC
		aCols[len(acols),nPosUM] := SB1->B1_UM
		aCols[len(acols),nPosQtd] := val(aLinha[ny,2])


		If ExistTrigger("C6_PRODUTO")
			RunTrigger(2,Len(aCols),,"C6_PRODUTO")
		EndIf
		If ExistTrigger("C6_QTDVEN")
			RunTrigger(2,Len(aCols),,"C6_QTDVEN")
		EndIf
		aCols[len(acols),nPosPrc] 	:= Val(aLinha[ny,3])
		aCols[len(acols),nPosVal] 	:= round(Val(aLinha[ny,3])*Val(aLinha[ny,2]),2)
		aCols[len(acols),nPosLoc] 	:= "AP"
		aCols[len(acols),nPosTES] 	:= "760"
		aCols[len(acols),nPosDtEnt] := M->C5_FECENT

	Next ny

Return()

User Function HATUALI(Gat,cTabela) //Fun��o chamada por gatilho

	If Altera
		_nvalped := 0
		_nvaldes := 0
		

		For nX:= 1 to len(acols)
			cGrade	 := acols[nX][35]
			If cGrade == 'S'
				_xMultGrade	:= AllTrim(GetMV("MV_GRDMULT"))  //Verifica o parametro de MultiGrade
				If _xMultGrade $ 'MATA410'
					_nPven1  := 0
					//_nPven2  := 0
					nPVdesc2 := 0
					If acols[nX][len(acols[nX])] = .F.
						nPven	 := acols[nX][25]//Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+acols[nX][2],"DA1_PRCVEN")
						For _y := 1 to len(oGrade:aColsGrade[nX])
							cProduto  := alltrim(acols[nX][2])
							cCor      := SubStr(OGRADE:ACOLSGRADE[nX][_y][1],2,3)
							aTamanho  := u_bTamanho(cProduto,cCor)
							For _z := 2 to len(oGrade:aColsGrade[nX][_y])
								If oGrade:aColsGrade[nX][_y][_z][1] > 0
									_nPven	 := Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+cProduto+cCor+Right("0000"+aTamanho[_Z-1],4),"DA1_PRCVEN")
									_nPven1	 += Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+cProduto+cCor+Right("0000"+aTamanho[_Z-1],4),"DA1_PRCVEN")
									//_nPven2  += _nPven * oGrade:aColsGrade[nX][_y][_z][1])
									nPVdesc2  += Round((_nPven * oGrade:aColsGrade[nX][_y][_z][1])*(1-(M->C5_DESC1/100)),2)
								Endif
							Next _z
						Next _y
						nPVdesc			:= Round(PegaDesc(_nPven1),2)	//Round(_nPven1*(1-(M->C5_DESC1/100)),2)
						nPVdesc6		:= Round(PegaDesc(_nPven1),6)	//Round(_nPven1*(1-(M->C5_DESC1/100)),6)
						//nPVdesc2		:= Round(PegaDesc(_nPven2),2)	//Round(_nPven2*(1-(M->C5_DESC1/100)),2)
						acols[nX][25]	:= _nPven1    									//Pre�o sem desconto
						acols[nX][6]	:= nPVdesc6										//Pre�o unit�rio
						acols[nX][7]	:= NoRound(nPVdesc2,2)							//Valor Total
						_nValped += nPVdesc2
						_nvaldes += _nPven1-nPVdesc// (nPven-nPVdesc)*acols[nX][5]
						oGetDad:Refresh()
					End
				Else
					If acols[nX][len(acols[nX])] = .F.
						nPven	 := acols[nX][25]//Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+acols[nX][2],"DA1_PRCVEN")
						For _y := 1 to len(oGrade:aColsGrade[nX])
							If oGrade:aColsGrade[nX][_y][2][1] > 0
								nPven	 := Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+alltrim(acols[nX][2])+SubStr(OGRADE:ACOLSGRADE[nX][_y][1],2,3),"DA1_PRCVEN")
							Endif
						Next
						nPVdesc			:= Round(PegaDesc(nPven),2)		//Round(nPven*(1-(M->C5_DESC1/100)),2)
						acols[nX][25]	:= nPven
						acols[nX][6]	:= nPVdesc
						acols[nX][7]	:= NoRound((acols[nX][5]*round(nPVdesc,2)),2)
						_nValped += acols[nX][5]*nPven
						_nvaldes += (nPven-acols[nX][6])*acols[nX][5]// (nPven-nPVdesc)*acols[nX][5]
						oGetDad:Refresh()
					End
				End
			Else
				nPven	 		:= Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+alltrim(acols[nX][2]),"DA1_PRCVEN")
				nPVdesc  		:= Round(PegaDesc(nPven),2)		//Round(nPven*(1-(M->C5_DESC1/100)),2)
				nPVdesc6 		:= Round(PegaDesc(nPven),6)		//Round(nPven*(1-(M->C5_DESC1/100)),6)
				acols[nX][25]	:= nPven
				acols[nX][6]	:= nPVdesc6
				acols[nX][7]	:= NoRound((acols[nX][5]*round(nPVdesc,2)),2)
				_nValped += acols[nX][5]*nPven
				_nvaldes += (nPven-acols[nX][6])*acols[nX][5]// (nPven-nPVdesc)*acols[nX][5]
				oGetDad:Refresh()
			EndIf
		Next nx

		nDescAtu := Round(PegaDesc(_nValped),2)		//Round(_nValped * (M->C5_DESC1/100),2)
		nDif     := Round(nDescAtu - _nvaldes,2)

		If nDif <> 0
			If cGrade <> 'S'
				For nX := 1 to len(aCols)
					If nDif / aCols[nX][5] = 0 .and. nDif <> 0
						nValItem := acols[1][7] - nDif
						nValUnit := Round((nValItem / acols[1][5]),2)
						acols[1][6] := nValUnit
						acols[1][7] := noround((acols[1][5]*round(nValUnit,2)),2)
						nDif := 0
					Endif
				Next
				ndif1 := ndif
				If nDif <> 0
					_x := 0
					_dif := 99999
					For nX := 1 to len(aCols)
						nValItem := acols[nX][7] - nDif
						nValUnit := Round((nValItem / acols[nX][5]),2)
						If _dif <> 0
							If nDif > 0
								If nValUnit - acols[nX][6] <> 0 .and. abs(ndif - ((nValUnit*acols[nX][5]) - acols[nX][7])) < abs(_dif)
									_dif := ndif - ((nValUnit*acols[nX][5]) - acols[nX][7])
									_x := nX
								Endif
							Else
								If nValUnit - acols[nX][6] <> 0 .and. abs(ndif - (acols[nX][7] - (nValUnit*acols[nX][5]))) < abs(_dif)
									_dif := ndif - (acols[nX][7] - (nValUnit*acols[nX][5]))
									_x := nX
								Endif
							Endif
						Endif
					Next
					IF _x > 0
						nValItem := acols[_x][7] - nDif
						nValUnit := Round((nValItem / acols[_x][5]),2)
						nDif := nDif - (acols[_x][7] - noround((acols[_x][5]*round(nValUnit,2)),2))
						acols[_x][6] := nValUnit
						acols[_x][7] := noround((acols[_x][5]*round(nValUnit,2)),2)
					//nDif := nDif - nDif1
					Endif
				Endif
	
				ndif1 := ndif
				If nDif <> 0
					_x := 0
					_dif := 99999
					For nX := 1 to len(aCols)
						nValItem := acols[nX][7] - nDif
						nValUnit := Round((nValItem / acols[nX][5]),2)
						If _dif <> 0
							If nDif > 0
								If nValUnit - acols[nX][6] <> 0 .and. abs(ndif - ((nValUnit*acols[nX][5]) - acols[nX][7])) < abs(_dif)
									_dif := ndif - ((nValUnit*acols[nX][5]) - acols[nX][7])
									_x := nX
								Endif
							Else
								If nValUnit - acols[nX][6] <> 0 .and. abs(ndif - (acols[nX][7] - (nValUnit*acols[nX][5]))) < abs(_dif)
									_dif := ndif - (acols[nX][7] - (nValUnit*acols[nX][5]))
									_x := nX
								Endif
							Endif
						Endif
					Next
					IF _x > 0
						nValItem := acols[_x][7] - nDif
						nValUnit := Round((nValItem / acols[_x][5]),2)
						nDif := nDif - (acols[_x][7] - noround((acols[_x][5]*round(nValUnit,2)),2))
						acols[_x][6] := nValUnit
						acols[_x][7] := noround((acols[_x][5]*round(nValUnit,2)),2)
	//				nDif := nDif - nDif1
					Endif
				Endif
	
				If nDif <> 0
					nValItem := acols[1][7] - nDif
					nValUnit := Round((nValItem / acols[1][5]),2)
					acols[1][6] := nValUnit
					acols[1][7] := noround((acols[1][5]*round(nValUnit,2)),2)
				Endif
				oGetDad:Refresh()
			Endif
		EndIf

		GETDREFRESH()
//		SetFocus(oGetDad:oBrowse:hWnd) // Atualizacao por linha //Desabilitado para n�o descer para a SC6 - Erro de TES
		oGetDad:Refresh()
		A410LinOk(oGetDad)

	EndIf

	If Gat == "TAB"
		lRet	:= cTabela
	Elseif Gat == "COND"
		lRet	:= M->C5_CONDPAG
	End

Return(lRet)

User Function HATUALI2(cProduto,cGrade,cItem) //Fun��o chamada por gatilho

	If cGrade == 'S'
		_xMultGrade	:= AllTrim(GetMV("MV_GRDMULT"))  //Verifica o parametro de MultiGrade
		If _xMultGrade $ 'MATA410'
			_nPven1  := 0
			//_nPven2  := 0
			nPVdesc2 := 0
			If acols[n][len(acols[n])] = .F.
				_nPven	 := acols[n][25]
				For _y := 1 to len(oGrade:aColsGrade[n])
					cProduto  := alltrim(acols[n][2])
					cCor      := SubStr(OGRADE:ACOLSGRADE[n][_y][1],2,3)
					aTamanho  := u_bTamanho(cProduto,cCor)
					For _x := 2 to len(oGrade:aColsGrade[n][_y])-1
						If oGrade:aColsGrade[n][_y][_x][1] > 0
							_nPven	 := Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+cProduto+cCor+Right("0000"+aTamanho[_Z-1],4),"DA1_PRCVEN")
							_nPven1	 += Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+cProduto+cCor+Right("0000"+aTamanho[_Z-1],4),"DA1_PRCVEN")
							//_nPven2  += _nPven * oGrade:aColsGrade[n][_y][_x][1]
							nPVdesc2  += Round((_nPven * oGrade:aColsGrade[n][_y][_x][1])*(1-(M->C5_DESC1/100)),2)
						Endif
					Next _x
				Next _y
				nPVdesc 		:= Round(PegaDesc(_nPven1),2)		//Round(_nPven1*(1-(M->C5_DESC1/100)),2)
				//nPVdesc2 		:= Round(PegaDesc(_nPven2),2)		//Round(_nPven2*(1-(M->C5_DESC1/100)),2)
				acols[n][25]	:= _nPven1    									//Pre�o sem desconto
				acols[n][6]		:= nPVdesc										//Pre�o unit�rio
				acols[n][7]		:= NoRound(nPVdesc2,2)	//Valor Total
				_nValped 		+= nPVdesc2
				_nvaldes 		+= _nPven1-nPVdesc// (nPven-nPVdesc)*acols[nX][5]
				oGetDad:Refresh()
			End
		Else
			If acols[n][len(acols[n])] = .F.
				nPven	 := acols[n][25]
				For _y := 1 to len(oGrade:aColsGrade[n])
					For _x := 2 to len(oGrade:aColsGrade[n][_y])-1
						If oGrade:aColsGrade[n][_y][_x][1] > 0
							nPven	 := Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+alltrim(cProduto)+SubStr(OGRADE:ACOLSGRADE[n][_y][1],2,3),"DA1_PRCVEN")
						Endif
					Next _x
				Next _y
				nPVdesc 		:= Round(PegaDesc(nPven),2)		//Round(nPven*(1-(M->C5_DESC1/100)),2)
				acols[n][25]	:= nPven
				acols[n][6]		:= nPVdesc
				acols[n][7]		:= NoRound((acols[n][5]*round(nPVdesc,2)),2)
				_nValped		:= nPven
				oGetDad:Refresh()
			End
		End
	Else
		nPven	 		:= Posicione("DA1",1,xFilial("DA1")+M->C5_TABELA+cPRODUTO,"DA1_PRCVEN")
		nPVdesc  		:= Round(PegaDesc(nPven),2)		//Round(nPven*(1-(M->C5_DESC1/100)),2)
		nPVdesc6 		:= Round(PegaDesc(nPven),6)		//Round(nPven*(1-(M->C5_DESC1/100)),6)
		acols[n][25]	:= nPven
		acols[n][6]		:= nPVdesc6
		acols[n][7]		:= NoRound((acols[n][5]*round(nPVdesc,2)),2)
		_nValped		:= nPven
		oGetDad:Refresh()
	EndIf

Return(_nValped)


User Function bTamanho(cProduto,cCor) //Fun��o Busca Tamanho

	Local nX, _i
	Local aTamanhos := {}
	Local aAlterFields := {}

	If Select("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf

	cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM "+RetSQLName("SBV")+" SBV "
	cQuery  += "WHERE D_E_L_E_T_ = '' "
	cQuery  += "	AND BV_TABELA = (SELECT B4_COLUNA FROM "+RetSQLName("SB4")+" SB4 WHERE D_E_L_E_T_<>'*' AND B4_COD='"+cProduto+"') "
	cQuery  += "ORDER BY R_E_C_N_O_"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
	  
	DbSelectArea("TMPSBV")

	cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" SZD WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_COR ='"+cCor+"' AND ZD_PRODUTO='"+cProduto+"'"

	If Select("TMPSZD") > 0
		TMPSZD->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)

	DbSelectArea("TMPSZD")

	While !EOF()
	
		For _i := 1  to 200
		
			DbSelectArea("SX3")
			_campo		:= "ZH_TAM"+StrZero(_i,3)
			cZdVerif	:= 0
	
			TMPSZD->(DbGoTop())
	
			While TMPSZD->(!EOF())
//				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					cZdVerif += 1
					
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo)) .and. cZdVerif==1
						Aadd(aTamanhos, {alltrim(TMPSBV->BV_DESCRI),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","",""}[1])
					Endif
//				Endif
		 		
				TMPSZD->(DbSkip())
				
			EndDo
	  	
			DbSelectArea("TMPSBV")
			
			DbSkip()
			
		Next _i
		
	EndDo

	DbClosearea()
	
Return (aTamanhos)


Static Function PegaDesc(nValor)

	_area	:= GetArea()
	_valor	:= nValor

	If M->C5_DESC1 <> 0
		_valor := _valor*(1-(M->C5_DESC1/100))
	Endif
	If M->C5_DESC2 <> 0
		_valor := _valor*(1-(M->C5_DESC2/100))
	Endif
	If M->C5_DESC3 <> 0
		_valor := _valor*(1-(M->C5_DESC3/100))
	Endif
	If M->C5_DESC4 <> 0
		_valor := _valor*(1-(M->C5_DESC4/100))
	Endif
	If M->C5_DESC5 <> 0
		_valor := _valor*(1-(M->C5_DESC5/100))
	Endif

	RestArea(_area)

Return(_valor)