#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

#Define CRLF  CHR(13)+CHR(10)

// #########################################################################################
// Projeto: HOPE
// Modulo : FaturamentoDANIEL
// ---------+-------------------+-----------------------------------------------------------
// Data     | Daniel R. Melo    | 
// ---------+-------------------+-----------------------------------------------------------
// 01/07/16 | Actual Trend      | Job para atualiza��o de Status de Pedidos
// ---------+-------------------+-----------------------------------------------------------

//------------------------------------------------------------------------------------------

user function HFATP017(cTabela)

//Bloqueio para protutos que n�o produzem
If cTabela == 'SC2'
	cSituac := Posicione("SB1",1,xFilial("SB1")+M->C2_PRODUTO,"B1_YSITUAC")
	If cSituac $ 'NPV |NPVO|NPNV|PDSV'
		If Alltrim(C2_SEQMRP)=''
			Alert(cSituac+" - PROD INDISPONIVEL")
			lRet := .F.
		End
	Else
		lRet := .T.
	End
End

//Bloqueio para protutos que n�o Vendem
If cTabela == 'SC6'
	If SB1->B1_YSITUAC $ 'PNV |NPNV|PDSV'
		If Alltrim(M->C5_ORIGEM)=='MANUAL'
			Alert(SB1->B1_YSITUAC+" - PRODUTO INDISPONIVEL PARA A VENDA")
			lRet := .F.
		End
	Else
		lRet := .T.
	End
End

Return (lRet)