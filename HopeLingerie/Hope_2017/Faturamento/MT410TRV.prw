#include 'protheus.ch'
#include 'parmtype.ch'

user function MT410TRV()

Local cCliForn := ParamIXB[1] // Codigo do cliente/fornecedor 
Local cLoja := ParamIXB[2] // Loja 
Local cTipo := ParamIXB[3] // C=Cliente(SA1) - F=Fornecedor(SA2) 
Local aRet := Array(4) 
Local lTravaSA1 := .F. // (T/F)  Liga/Desliga trava da tabela SA1 
Local lTravaSA2 := .F. // Desliga trava da tabela SA2 
Local lTravaSB2 := .F. // Desliga trava da tabela SB2 

Local aRet[1] := lTravaSA1 
Local aRet[2] := lTravaSA2 
Local aRet[3] := lTravaSB2 


Return(aRet[3])
	