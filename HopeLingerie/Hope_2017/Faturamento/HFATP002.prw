#Include "PROTHEUS.CH"

User Function HFATP002()
                        
	local cVldAlt		:= ".T."
	local cVldExc		:= ".T."
	local _vend 		:= ""
	local _xvend 		:= ""
	local cExprFilTop	:= ""
	local cAlias
	
	cAlias := "SZ2"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)

	private cCadastro := "Cadastro de Politica Comercial"

	DbSelectArea("SA3")
	DbSetOrder(7)
	DbSeek(xfilial("SA3")+__cUserID)
	
	If Found()
		_vend := SA3->A3_COD
		_xvend += "'"+_vend+"',"

		DbSelectArea('ZA4')
		ZA4->(DbSetOrder(1))
		ZA4->(DbGoTop())
		ZA4->(DbSeek(xFilial("ZA4")))
		While ZA4->(!Eof())
			If ZA4->ZA4_GEREN = _vend
				_xvend += "'"+ZA4->ZA4_REPRE+"',"
			End
			ZA4->(DbSkip())
			
		EndDo

	Else
		_xvend := ""
	Endif

	If AllTrim(_xvend) == ""
		aRotina := {	{ "Pesquisar" , "AxPesqui"	 , 0, 1},;
						{ "Visualizar", "u_PolCom(4)", 0, 2},;
						{ "Incluir"   , "u_PolCom(1)", 0, 3},;
						{ "Alterar"   , "u_PolCom(2)", 0, 4},;
						{ "Excluir"   , "u_PolCom(3)", 0, 5}}
	Else
		aRotina := {	{ "Pesquisar" , "AxPesqui"	 , 0, 1},;
						{ "Visualizar", "u_PolCom(4)", 0, 2}}
	End

	IF AllTrim(_xvend) <> ""
		cExprFilTop += " Z2_CODIGO IN ( "
		cExprFilTop += " SELECT ZAN_CODPOL FROM "+RetSqlName("ZAN")+" WHERE D_E_L_E_T_<>'*' "
		cExprFilTop += " AND ZAN_REPRES IN ("+ SubStr(_xvend,1,len(alltrim(_xvend))-1) + "))"
	End

	mBrowse( 6, 1, 22, 75, cAlias, NIL , NIL , NIL , NIL , NIL , NIL , NIL , NIL , NIL , NIL , NIL , NIL , NIL , cExprFilTop )
	
return


User Function PolCom(_tp)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Private oFolder1
	Private cGet1 := Space(3)
	Private cGet2 := Space(40)
	Private cGet3 := 0
	Private cGet4 := 0

	Static oDlg

	If _tp <> 1
		cGet1 := SZ2->Z2_CODIGO
		cGet2 := SZ2->Z2_DESC
		cGet3 := SZ2->Z2_VRMIN
		cGet4 := SZ2->Z2_VRMINPA
	Endif
	
	DEFINE MSDIALOG oDlg TITLE "Pol�tica Comercial" FROM 000, 000  TO 500, 865 COLORS 0, 16777215 PIXEL

	@ 011, 015 SAY oSay1 PROMPT "C�digo" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	If _tp <> 1
		@ 011, 049 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	Else
		@ 011, 049 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 PIXEL
	Endif
	@ 012, 130 SAY oSay2 PROMPT "Descri��o" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 166 MSGET oGet2 VAR cGet2 Picture "@!" SIZE 259, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 033, 014 SAY oSay3 PROMPT "Vr. M�nimo Pedido" SIZE 052, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 076 MSGET oGet3 VAR cGet3 Picture "@E 999,999,999.99" SIZE 073, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 031, 193 SAY oSay4 PROMPT "Vr. M�nimo Parcela" SIZE 056, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 031, 269 MSGET oGet4 VAR cGet4 Picture "@E 999,999,999.99" SIZE 071, 010 OF oDlg COLORS 0, 16777215 PIXEL
	@ 049, 004 FOLDER oFolder1 SIZE 426, 172 OF oDlg ITEMS "Tp.Pedido","Tab.Pre�o","Cond.Pagto","Prazo" COLORS 0, 16777215 PIXEL
	fMSNewGe1(_tp)
	fMSNewGe2(_tp)
	fMSNewGe3(_tp)
	fMSNewGe4(_tp)
	If _tp == 1 .or. _tp == 2 .or. _tp == 3 
		@ 233, 344 BUTTON oButton1 PROMPT "Confirma" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
	End
	@ 233, 388 BUTTON oButton2 PROMPT "Cancela" SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL

	ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSNewGe1(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"Z4_TPPED","Z4_DESC"}
	Local aAlterFields := {"Z4_TPPED"}
	Private aHeader1 := {}
	Private aCols1 := {}

	Static oMSNewGe1

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZ4")
		DbSetOrder(1)
		DbSeek(xfilial("SZ4")+SZ2->Z2_CODIGO)

		If Found()
			While !EOF() .and. SZ4->Z4_POLITIC == SZ2->Z2_CODIGO
				Aadd(aCols1,{SZ4->Z4_TPPED,SZ4->Z4_DESC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols1, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols1, aFieldFill)
	Endif
  
	oMSNewGe1 := MsNewGetDados():New( 003, 003, 161, 421, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[1], aHeader1, aCols1)

Return

//------------------------------------------------ 
Static Function fMSNewGe2(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"Z5_TABELA","Z5_DESC"}
	Local aAlterFields := {"Z5_TABELA"}
	Private aHeader2 := {}
	Private aCols2 := {}
	Static oMSNewGe2

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader2, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZ5")
		DbSetOrder(1)
		DbSeek(xfilial("SZ5")+SZ2->Z2_CODIGO)
  	
		IF Found()
			While !EOF() .and. SZ5->Z5_POLITIC == SZ2->Z2_CODIGO
				Aadd(aCols2,{SZ5->Z5_TABELA,SZ5->Z5_DESC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols2, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols2, aFieldFill)
	Endif

	oMSNewGe2 := MsNewGetDados():New( 003, 003, 161, 421, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[2], aHeader2, aCols2)

Return
//------------------------------------------------ 
Static Function fMSNewGe3(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"Z3_COND","Z3_DESC"}
	Local aAlterFields := {"Z3_COND"}
	Private aHeader3 := {}
	Private aCols3 := {}
	Static oMSNewGe3

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader3, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZ3")
		DbSetOrder(1)
		DbSeek(xfilial("SZ3")+SZ2->Z2_CODIGO)
  	
		If Found()
			While !EOF() .and. SZ3->Z3_POLITIC == SZ2->Z2_CODIGO
				Aadd(aCols3,{SZ3->Z3_COND,SZ3->Z3_DESC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols3, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols3, aFieldFill)
	Endif
  
	oMSNewGe3 := MsNewGetDados():New( 003, 003, 161, 421, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[3], aHeader3, aCols3)

Return

//------------------------------------------------ 
Static Function fMSNewGe4(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"Z6_PRAZO","Z6_DESC"}
	Local aAlterFields := {"Z6_PRAZO"}
	Private aHeader4 := {}
	Private aCols4 := {}
	Static oMSNewGe4

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader4, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZ6")
		DbSetOrder(1)
		DbSeek(xfilial("SZ6")+SZ2->Z2_CODIGO)
  	
		If Found()
			While !EOF() .and. SZ6->Z6_POLITIC == SZ2->Z2_CODIGO
				Aadd(aCols4,{SZ6->Z6_PRAZO,SZ6->Z6_DESC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols4, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols4, aFieldFill)
	Endif
  
	oMSNewGe4 := MsNewGetDados():New( 003, 003, 161, 421, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[4], aHeader4, aCols4)

Return

Static Function Save(_tp)

	Local I

	DbSelectArea("SZ2")
	DbSetOrder(1)
	DbSeek(xfilial("SZ2")+cGet1)

	If !Found()
		RecLock("SZ2",.T.)
		Replace Z2_FILIAL	with xfilial("SZ2")
		Replace Z2_CODIGO	with cGet1
	Else
		RecLock("SZ2",.F.)
	Endif
	Replace Z2_DESC		with cGet2
	Replace Z2_VRMIN	with cGet3
	Replace Z2_VRMINPA	with cGet4
	MsUnLock()

	If _tp == 1
		For i := 1 to Len(OMSNEWGE1:ACOLS)
			If OMSNEWGE1:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
				RecLock("SZ4",.T.)
				Replace Z4_FILIAL		with xfilial("SZ4")
				Replace Z4_POLITIC		with cGet1
				Replace Z4_TPPED		with OMSNEWGE1:ACOLS[i,1]
				Replace Z4_DESC			with OMSNEWGE1:ACOLS[i,2]
				MsUnLock()
			Endif
		Next

		For i := 1 to Len(OMSNEWGE2:ACOLS)
			If OMSNEWGE2:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE2:ACOLS[i,1]) <> ""
				RecLock("SZ5",.T.)
				Replace Z5_FILIAL		with xfilial("SZ5")
				Replace Z5_POLITIC		with cGet1
				Replace Z5_TABELA		with OMSNEWGE2:ACOLS[i,1]
				Replace Z5_DESC			with OMSNEWGE2:ACOLS[i,2]
				MsUnLock()
			Endif
		Next

		For i := 1 to Len(OMSNEWGE3:ACOLS)
			If OMSNEWGE3:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE3:ACOLS[i,1]) <> ""
				RecLock("SZ3",.T.)
				Replace Z3_FILIAL		with xfilial("SZ3")
				Replace Z3_POLITIC		with cGet1
				Replace Z3_COND			with OMSNEWGE3:ACOLS[i,1]
				Replace Z3_DESC			with OMSNEWGE3:ACOLS[i,2]
				MsUnLock()
			Endif
		Next
	
		For i := 1 to Len(OMSNEWGE4:ACOLS)
			If OMSNEWGE4:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE4:ACOLS[i,1]) <> ""
				RecLock("SZ6",.T.)
				Replace Z6_FILIAL		with xfilial("SZ4")
				Replace Z6_POLITIC		with cGet1
				Replace Z6_PRAZO		with OMSNEWGE4:ACOLS[i,1]
				Replace Z6_DESC			with OMSNEWGE4:ACOLS[i,2]
				MsUnLock()
			Endif
		Next
	ElseIf _tp == 2
		For i := 1 to Len(OMSNEWGE1:ACOLS)
			If OMSNEWGE1:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
				DbSelectArea("SZ4")
				DbSetOrder(1)
				DbSeek(xfilial("SZ4")+cGet1+OMSNEWGE1:ACOLS[i,1])
			
				If !Found()
					RecLock("SZ4",.T.)
					Replace Z4_FILIAL		with xfilial("SZ4")
					Replace Z4_POLITIC		with cGet1
					Replace Z4_TPPED		with OMSNEWGE1:ACOLS[i,1]
					Replace Z4_DESC			with OMSNEWGE1:ACOLS[i,2]
					MsUnLock()
				Endif
			Else
				DbSelectArea("SZ4")
				DbSetOrder(1)
				DbSeek(xfilial("SZ4")+cGet1+OMSNEWGE1:ACOLS[i,1])
			
				If Found()
					RecLock("SZ4",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next

		For i := 1 to Len(OMSNEWGE2:ACOLS)
			If OMSNEWGE2:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE2:ACOLS[i,1]) <> ""
				DbSelectArea("SZ5")
				DbSetOrder(1)
				DbSeek(xfilial("SZ5")+cGet1+OMSNEWGE2:ACOLS[i,1])
			
				If !Found()
					RecLock("SZ5",.T.)
					Replace Z5_FILIAL		with xfilial("SZ5")
					Replace Z5_POLITIC		with cGet1
					Replace Z5_TABELA		with OMSNEWGE2:ACOLS[i,1]
					Replace Z5_DESC			with OMSNEWGE2:ACOLS[i,2]
					MsUnLock()
				Endif
			Else
				DbSelectArea("SZ5")
				DbSetOrder(1)
				DbSeek(xfilial("SZ5")+cGet1+OMSNEWGE2:ACOLS[i,1])
			
				If Found()
					RecLock("SZ5",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next

		For i := 1 to Len(OMSNEWGE3:ACOLS)
			If OMSNEWGE3:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE3:ACOLS[i,1]) <> ""
				DbSelectArea("SZ3")
				DbSetOrder(1)
				DbSeek(xfilial("SZ3")+cGet1+OMSNEWGE3:ACOLS[i,1])
			
				If !Found()
					RecLock("SZ3",.T.)
					Replace Z3_FILIAL		with xfilial("SZ3")
					Replace Z3_POLITIC		with cGet1
					Replace Z3_COND			with OMSNEWGE3:ACOLS[i,1]
					Replace Z3_DESC			with OMSNEWGE3:ACOLS[i,2]
					MsUnLock()
				Endif
			Else
				DbSelectArea("SZ3")
				DbSetOrder(1)
				DbSeek(xfilial("SZ3")+cGet1+OMSNEWGE3:ACOLS[i,1])
			
				If Found()
					RecLock("SZ3",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next
	
		For i := 1 to Len(OMSNEWGE4:ACOLS)
			If OMSNEWGE4:ACOLS[i,3] == .F. .and. alltrim(OMSNEWGE4:ACOLS[i,1]) <> ""
				DbSelectArea("SZ6")
				DbSetOrder(1)
				DbSeek(xfilial("SZ6")+cGet1+OMSNEWGE4:ACOLS[i,1])
			
				If !Found()
					RecLock("SZ6",.T.)
					Replace Z6_FILIAL		with xfilial("SZ6")
					Replace Z6_POLITIC		with cGet1
					Replace Z6_PRAZO		with OMSNEWGE4:ACOLS[i,1]
					Replace Z6_DESC			with OMSNEWGE4:ACOLS[i,2]
					MsUnLock()
				Endif
			Else
				DbSelectArea("SZ6")
				DbSetOrder(1)
				DbSeek(xfilial("SZ6")+cGet1+OMSNEWGE4:ACOLS[i,1])
			
				If Found()
					RecLock("SZ6",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next
	ElseIf _tp == 3
		DbSelectArea("SZ2")
		DbSetOrder(1)
		DbSeek(xfilial("SZ2")+cGet1)

		If Found()
			RecLock("SZ2",.F.)
			DbDelete()
			MsUnLock()
		Endif
	
		DbSelectArea("SZ3")
		DbSetOrder(1)
		DbSeek(xfilial("SZ3")+cGet1)
	
		While !EOF() .and. SZ3->Z3_POLITIC == cGet1
			RecLock("SZ3",.F.)
			DbDelete()
			MsUnlock()
			DbSkip()
		End

		DbSelectArea("SZ4")
		DbSetOrder(1)
		DbSeek(xfilial("SZ4")+cGet1)
	
		While !EOF() .and. SZ4->Z4_POLITIC == cGet1
			RecLock("SZ4",.F.)
			DbDelete()
			MsUnlock()
			DbSkip()
		End

		DbSelectArea("SZ5")
		DbSetOrder(1)
		DbSeek(xfilial("SZ5")+cGet1)
	
		While !EOF() .and. SZ5->Z5_POLITIC == cGet1
			RecLock("SZ5",.F.)
			DbDelete()
			MsUnlock()
			DbSkip()
		End

		DbSelectArea("SZ6")
		DbSetOrder(1)
		DbSeek(xfilial("SZ6")+cGet1)
	
		While !EOF() .and. SZ6->Z6_POLITIC == cGet1
			RecLock("SZ6",.F.)
			DbDelete()
			MsUnlock()
			DbSkip()
		End

	Endif

Return