#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include "TOTVS.CH"

#define MB_ICONHAND                 16
#define MB_ICONEXCLAMATION          48

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATP018   � Autor � Daniel R. Melo    � Data �  10/07/1027 ���
�������������������������������������������������������������������������͹��
���Descricao � Cadastro de Gerentes.                                      ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE LINGERIE;                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function HFATP018()
	
	Local aRotAdic :={}
	Local cVldAlt	:= ".F." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
	Local cVldExc	:= ".F." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.
	Local aButtons	:= {}
	Private cPerg   := "DA0"
	Private cString := cPerg
	Private cEOL		:= +Chr(13)+Chr(10)

	dbSelectArea("DA0")
	dbSetOrder(1)

	Pergunte(cPerg,.F.)
	SetKey(123,{|| Pergunte(cPerg,.T.)}) // Seta a tecla F12 para acionamento dos parametros

	aadd(aRotAdic,{ "Ajusta Pre�o","U_HFATP032()", 0 , 6 })
	aadd(aRotAdic,{ "Copiar Tabela de Pre�o","U_CopTabPr()", 0 , 7 })

	AxCadastro(cString,"Tabela de Pre�os"		,cVldExc,"U_cAltDA0()",aRotAdic, , , , , , , aButtons, , )

	Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros

Return

User Function cAltDA0()

	Local cUsers	:= ""

	cUsers		:= AllTrim(GetMv("MV_HTABPR1"))
//	cUsers		+= "000088"    //Codigo Daniel Melo
		
	If __cUserId $ cUsers
			
		If Inclui
			
			If AllTrim(M->DA0_TABVIR)<>''
										
				cQuery1  := " EXEC CRIA_DA1 '"+M->DA0_TABVIR+"','"+M->DA0_CODTAB+"' "+cEOL 
				TcSqlExec(cQuery1)
				
				cQuery2  := "UPDATE DA1010 SET DA1_DATVIG='"+dToS(dDataBase)+"', DA1_PRCVEN=DA1_PRCVEN*"+AllTrim(str(M->DA0_FATOR))+", "+cEOL
				cQuery2  += "		DA1_USERGI='', DA1_USERGA='' "+cEOL 
				cQuery2  += "WHERE D_E_L_E_T_<>'*' AND DA1_CODTAB='"+M->DA0_CODTAB+"' " 
				TcSqlExec(cQuery2) 

				MessageBox("A Tabela Virtual "+M->DA0_CODTAB+" foi inserida com base na Tebela "+M->DA0_TABVIR+".","Inclus�o Efetuada com Sucesso",MB_ICONEXCLAMATION)

			Else

				U_CopTabPr(M->DA0_CODTAB)

			End

		ElseIf Altera
	
			cQuery1  := "UPDATE "+RetSQLName("DA1")+" SET DA1_DATVIG='"+dToS(M->DA0_DATDE)+"' "+cEOL 
			cQuery1  += "WHERE D_E_L_E_T_<>'*' AND DA1_CODTAB='"+M->DA0_CODTAB+"' "+cEOL
			TcSqlExec(cQuery1) 
	
		End
		
	End

Return .T.

User Function CopTabPr(GetTab)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oGet3
	Local oSay1
	Local oSay2
	Local oSay3
	Local cUsers		:= ""
	Private cGetTabNew 	:= Iif(Alltrim(GetTab)<>'',GetTab,DA0->DA0_CODTAB)
	Private cGetTabOld 	:= Space(3)
	Private cGetFator	:= 0
	Static oDlg

	cUsers		:= AllTrim(GetMv("MV_HTABPR1"))
//	cUsers		+= "000088"    //Codigo Daniel Melo

	If __cUserId $ cUsers

		cQuery  := "SELECT COUNT(DA1_ITEM) AS COUNTTABNEW FROM "+RetSQLName("DA1")+" AS DA1 WITH (NOLOCK) "+cEOL
		cQuery  += "WHERE D_E_L_E_T_<>'*' AND DA1_FILIAL='"+xFilial("DA1")+"' AND DA1_CODTAB='"+cGetTabNew+"' "+cEOL
	
		If Select("TMPDA1N") > 0
			TMPDA1N->(DbCloseArea())
		EndIf
	
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPDA1N",.T.,.T.)
	
		DbSelectArea("TMPDA1N")
		DbGoTop()
		
		If TMPDA1N->COUNTTABNEW == 0

			DEFINE MSDIALOG oDlg TITLE "Replica Tabela" FROM 000, 000  TO 170, 290 COLORS 0, 16777215 PIXEL
		
			@ 009, 007 SAY oSay1 PROMPT "Para a Tabela:" 	SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL
			@ 007, 063 MSGET oGet1 VAR cGetTabNew 			SIZE 072, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
			@ 027, 007 SAY oSay2 PROMPT "Copia da Tabela:" 	SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL
			@ 025, 063 MSGET oGet2 VAR cGetTabOld 			SIZE 072, 010 OF oDlg COLORS 0, 16777215 F3 "DA0" PIXEL
			@ 045, 007 SAY oSay3 PROMPT "Fator para Copia:"	SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL
			@ 043, 063 MSGET oGet3 VAR cGetFator 			SIZE 072, 010 OF oDlg PICTURE "@E 9.9999" COLORS 0, 16777215 PIXEL
			 
			@ 065, 097 BUTTON oButton1 PROMPT "Replicar" 	SIZE 037, 012 ACTION (CopTab1(1)) OF oDlg PIXEL
			@ 065, 050 BUTTON oButton2 PROMPT "Cancelar" 	SIZE 037, 012 ACTION (CopTab1(2)) OF oDlg PIXEL
		
			ACTIVATE MSDIALOG oDlg CENTERED
			
		Else
			MessageBox("A Tabela "+cGetTabNew+" j� tem registros e n�o pode ser aplicada a replica da Tabela","C�pia Negada",MB_ICONHAND)
		End
	
		DbCloseArea()
	Else
		MessageBox("Usu�rio sem acesso a rotina","C�pia Negada",MB_ICONHAND)
	End

Return

Static Function CopTab1(xopc, cCount)

	If xopc == 1
		If cGetTabNew <> cGetTabOld
			
			oDlg:End()
			
			cQuery  := "SELECT COUNT(DA1_ITEM) AS COUNTTABOLD FROM "+RetSQLName("DA1")+" AS DA1 WITH (NOLOCK) "+cEOL
			cQuery  += "WHERE D_E_L_E_T_<>'*' AND DA1_FILIAL='"+xFilial("DA1")+"' AND DA1_CODTAB='"+cGetTabOld+"' "+cEOL
	
			If Select("TMPDA1O") > 0
				TMPDA1O->(DbCloseArea())
			EndIf
		
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPDA1O",.T.,.T.)
		
			DbSelectArea("TMPDA1O")
			DbGoTop()
			
			oProcesso := MsNewProcess():New({|lEnd| CopTab2(TMPDA1O->COUNTTABOLD) },"Copiando Registros ...")
			oProcesso:Activate()
			
		Else
			
			MessageBox("A Tabela a ser copiada deve ser diferente da Tabela "+cGetTabNew,"C�pia Negada",MB_ICONHAND)

		End
	ElseIf xopc == 2
		oDlg:End()
	End

Return


Static Function CopTab2(cCount)

	cQuery1  := " EXEC CRIA_DA1 '"+cGetTabOld+"','"+cGetTabNew+"' "+cEOL
	TcSqlExec(cQuery1)
	
	cQuery2  := " UPDATE DA1010 SET DA1_DATVIG='"+dToS(dDataBase)+"', DA1_USERGI='', DA1_USERGA='' "+cEOL
	If cGetFator <> 0
		cQuery2  += "		, DA1_PRCVEN=DA1_PRCVEN*"+AllTrim(str(cGetFator))+" "+cEOL
	End
	cQuery2  += " WHERE D_E_L_E_T_<>'*' AND DA1_CODTAB='"+cGetTabNew+"' " 
	TcSqlExec(cQuery2) 

	MessageBox("A Tabela "+cGetTabOld+" Foi replicada para a Tebela "+cGetTabNew,"C�pia Efetuada",MB_ICONEXCLAMATION)
	
Return