#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL011 
Status Pr�-Faturamento
@author Weskley Silva
@since 07/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL011()

Private oReport
Private cPergCont	:= 'HPREL011' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'STA', 'STATUS PR�-FATURAMENTO ', , {|oReport| ReportPrint( oReport ), 'STATUS PR�-FATURAMENTO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'STATUS PR�-FATURAMENTO', { 'STA', 'SZR','SZJ','SA1','SA2','SC5','SZ1'})
			
	TRCell():New( oSection1, 'PEDIDO'	    	    ,'STA', 		'PEDIDO',							"@!"                        ,10)
	TRCell():New( oSection1, 'TIPO_PED'	        	,'STA', 		'TIPO_PED',   						"@!"				        ,35)
	TRCell():New( oSection1, 'COD_CLIENTE' 		    ,'STA', 		'COD_CLIENTE',              		"@!"						,06)
	TRCell():New( oSection1, 'CLIENTE' 		        ,'STA', 		'CLIENTE',              			"@!"						,35)
	TRCell():New( oSection1, 'DAP'         			,'STA', 		'DAP',			           			"@!"						,10)
	TRCell():New( oSection1, 'DATA_PRE'				,'STA', 		'DATA_PRE',       					"@!"		                ,15)
	TRCell():New( oSection1, 'NUM_PRE_FATURAMENTO'	,'STA', 		'NUM_PRE_FATURAMENTO',	   			"@!"						,15)
	TRCell():New( oSection1, 'QTDE_PRE'      		,'STA', 		'QTDE_PRE',      					"@E 999,99"				    ,15)
	TRCell():New( oSection1, 'QTDE_CONF' 			,'STA', 	    'QTDE_CONF',     					"@E 999,99"		    	    ,15)
	TRCell():New( oSection1, 'A_CONFERIR' 			,'STA', 	    'A_CONFERIR',     					"@E 999,99"		    	    ,15)
	TRCell():New( oSection1, 'VALOR'				,'STA', 		'VALOR',   							"@E 999,999.99"				,15)
	TRCell():New( oSection1, 'SITUACAO'				,'STA', 		'SITUACAO',			   				"@!"						,15)
	TRCell():New( oSection1, 'STATUS'				,'STA', 		'STATUS',				   			"@!"						,15)
	TRCell():New( oSection1, 'NOTA_FISCAL'			,'STA', 		'NOTA_FISCAL',			   			"@!"						,15)
	TRCell():New( oSection1, 'DATA_FAT'				,'STA', 		'DATA_FAT',		   					"@!"						,15)

	
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("STA") > 0
		STA->(dbCloseArea())
	Endif
      
	cQuery := " SELECT R.ZJ_PEDIDO AS PEDIDO, R.A1_COD AS COD_CLIENTE,R.Z1_DESC AS TIPO_PED, R.A1_NOME AS CLIENTE, R.ZJ_LOTDAP AS DAP, R.ZJ_DATA AS DATA_PRE, R.ZJ_NUMPF AS NUM_PRE_FATURAMENTO, R.QTDE_PRE, R.QTDE_CONF,R.QTDE_PRE - R.QTDE_CONF AS A_CONFERIR, R.VALOR, R.ZJ_CONF AS SITUACAO, " 

	cQuery += "  CASE  WHEN R.ZJ_DOC <> '' AND R.ZJ_CONF = 'S'  THEN 'Faturado'  " 

	cQuery += " ELSE CASE WHEN R.ZJ_DOC = '' AND R.ZJ_CONF = 'S' THEN 'Conferido' "  

	cQuery += " ELSE CASE WHEN R.QTDE_CONF > 0 AND R.ZJ_CONF = 'L' THEN 'Em Conferencia'  " 

	cQuery += " ELSE CASE WHEN R.QTDE_CONF = 0  AND R.ZJ_CONF = 'L' THEN 'Liberado para Conferencia' " 

	cQuery += " ELSE CASE WHEN R.ZJ_CONF = '' THEN 'Dap Gerado' " 

	cQuery += " ELSE 'Sem Status' END END END END END AS STATUS, " 

	cQuery += " R.ZJ_DOC AS NOTA_FISCAL, R.EMISSAO_NF AS DATA_FAT " 

	cQuery += " FROM " 

	cQuery += " (SELECT ZJ_PEDIDO, Z1_DESC, A1.A1_COD ,A1.A1_NOME, ZJ_LOTDAP, ZJ_DATA, ZJ_NUMPF, SUM(ZJ_QTDLIB) AS QTDE_PRE, SUM(ZJ_QTDSEP) AS QTDE_CONF, SUM(ZJ_QTDLIB * C6_PRUNIT) AS VALOR, ZJ_CONF, " 


	cQuery += " ZJ_DOC, ISNULL(F2_EMISSAO,'') AS EMISSAO_NF " 

	cQuery += " FROM SZJ010 (NOLOCK) AS ZJ " 

	cQuery += " INNER JOIN SA1010 (NOLOCK) AS A1 ON (A1_COD = ZJ_CLIENTE AND A1.D_E_L_E_T_ <> '*') " 

	cQuery += " LEFT JOIN SF2010 (NOLOCK) AS F2 ON (F2.F2_FILIAL = ZJ_FILIAL AND F2_DOC = ZJ_DOC AND F2.D_E_L_E_T_ <> '*') " 

	cQuery += " INNER JOIN SC6010 (NOLOCK) AS C6 ON (C6_FILIAL = ZJ_FILIAL AND C6_NUM = ZJ_PEDIDO AND C6_PRODUTO = ZJ_PRODUTO AND C6.D_E_L_E_T_ <> '*') " 

	cQuery += " INNER JOIN SC5010 (NOLOCK) AS C5 ON (C5_FILIAL = ZJ_FILIAL AND C5_NUM = ZJ_PEDIDO AND C5.D_E_L_E_T_ <> '*') "

	cQuery += " LEFT JOIN SZ1010 (NOLOCK)AS Z1 ON (Z1_CODIGO = C5_TPPED AND Z1.D_E_L_E_T_ <> '*') "

	cQuery += " WHERE ZJ.D_E_L_E_T_ <> '*' AND ZJ_DATA BETWEEN '"+ DTOS(mv_par05) +"'  AND '"+ DTOS(mv_par06) + "'  "
	
	IF !EMPTY(mv_par02)
	
	cQuery += " AND ZJ_NUMPF BETWEEN  '"+mv_par02+"' AND '"+mv_par03+"' "
	
	endif
	
	
	  if !EMPTY(mv_par01)
       	cQuery += " AND ZJ_LOTDAP = '"+Alltrim(mv_par01)+"'  "
    ENDIF
    
    IF !EMPTY(mv_par04)
    	cQuery += " AND ZJ_PEDIDO = '"+Alltrim(mv_par04)+"' "
    ENDIF
    
    if Upper(mv_par07) == "S"
    
    	cQuery += " AND ZJ_DOC <> ''  "

    elseif Upper(mv_par07) == "N"
    	   	
    	cQuery += " AND ZJ_DOC = '' "
    Endif
	
	
	cQuery += " GROUP BY ZJ_PEDIDO, Z1_DESC,A1_NOME, ZJ_LOTDAP, ZJ_DATA, ZJ_NUMPF, ZJ_CONF, ZJ_DOC, F2_EMISSAO, ZJ_CONF, A1.A1_COD  "

	cQuery += " ) R "
	    
       	
	TCQUERY cQuery NEW ALIAS STA

	While STA->(!EOF())
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("PEDIDO"):SetValue(STA->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")

		oSection1:Cell("TIPO_PED"):SetValue(STA->TIPO_PED)
		oSection1:Cell("TIPO_PED"):SetAlign("LEFT")
		
		oSection1:Cell("COD_CLIENTE"):SetValue(STA->COD_CLIENTE)
		oSection1:Cell("COD_CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE"):SetValue(STA->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
				
		oSection1:Cell("DAP"):SetValue(STA->DAP)
		oSection1:Cell("DAP"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_PRE"):SetValue(STA->DATA_PRE)
		oSection1:Cell("DATA_PRE"):SetAlign("LEFT")
		
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetValue(STA->NUM_PRE_FATURAMENTO)
		oSection1:Cell("NUM_PRE_FATURAMENTO"):SetAlign("LEFT")
		
		
		oSection1:Cell("QTDE_PRE"):SetValue(STA->QTDE_PRE)
		oSection1:Cell("QTDE_PRE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_CONF"):SetValue(STA->QTDE_CONF)
		oSection1:Cell("QTDE_CONF"):SetAlign("LEFT")
		
		
		oSection1:Cell("A_CONFERIR"):SetValue(STA->A_CONFERIR)
		oSection1:Cell("A_CONFERIR"):SetAlign("LEFT")		
		
		oSection1:Cell("VALOR"):SetValue(STA->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")
		
		
		oSection1:Cell("SITUACAO"):SetValue(STA->SITUACAO)
		oSection1:Cell("SITUACAO"):SetAlign("LEFT")
		

		oSection1:Cell("STATUS"):SetValue(STA->STATUS)
		oSection1:Cell("STATUS"):SetAlign("LEFT")
		
		oSection1:Cell("NOTA_FISCAL"):SetValue(STA->NOTA_FISCAL)
		oSection1:Cell("NOTA_FISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_FAT"):SetValue(STA->DATA_FAT)
		oSection1:Cell("DATA_FAT"):SetAlign("LEFT")
		
											
		oSection1:PrintLine()
		
		STA->(DBSKIP()) 
	enddo
	STA->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 07 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Lote Dap ?"		        		,""		,""		,"mv_ch1","C",06,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Pr�-Fatuamento de ?"            ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Pr�-Fatuamento ate ?"		    ,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Pedido ?"		                ,""		,""		,"mv_ch4","C",06,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data Pre-Faturamento de ?"		,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Data Pre-Faturamento at� ?"		,""		,""		,"mv_ch6","D",08,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "07","Faturado ?"						,""		,""		,"mv_ch7","C",02,0,1,"G",""	,""	,"","","mv_par07"," ","","","","","","","","","","","","","","","")
Return