#include "Protheus.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CADHOPE   �Autor  �Fgarcia             � Data �  10/05/16   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina para importa��o de clientes e forcenedor            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Gaveteiro    CADHOPE                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HCADA002()

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbGoTop()
	
	While !EOF()
		
		_texto := SB1->B1_DESC
		
		RecLock("SB1",.F.)
			Replace B1_DESC with U__BlNJET(Alltrim(_texto))
		MsUnLock()		
		
		DbSelectArea("SB1")
		DbSkip()
	End

	DbSelectArea("SED")
	DbSetOrder(1)
	DbGoTop()
	
	While !EOF()
		
		_texto := SED->ED_DESCRIC
		
		RecLock("SED",.F.)
			Replace ED_DESCRIC with U__BlNJET(Alltrim(_texto))
		MsUnLock()		
		
		DbSelectArea("SED")
		DbSkip()
	End

	Alert("Terminou!")
Return
