#include "Protheus.ch"
#include "TbiConn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CADHOPE   �Autor  �Fgarcia             � Data �  10/05/16   ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina para importa��o de clientes e forcenedor            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � Gaveteiro    CADHOPE                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HCADA001()

Local cExtFiles 	:= ""
Local aFiles    	:= {}
Local cArquivo		:= ""
Local cArquivo2		:= ""
Local aDados		:= {}
Local __y			:= 0
Private cPath 	  	:= "" 


//RPCSetType(3)

//PREPARE ENVIRONMENT EMPRESA "01" FILIAL "0101"  TABLES "SA1","SX5","SX6","SX2","SA2"
// Parametro origem dos arquivos a ser importados
cPath 	  	:= AllTrim(If(Right(GetMV("MV_PTHOPE"),1) == "\", GetMV("MV_PTHOPE"), GetMV("MV_PTHOPE") + "\"))
cExtFiles 	:= AllTrim(GetMV("MV_ARQHOPE")) // *.csv
aFiles    	:= Directory(cPath + cExtFiles, "D")




For __y := 1 to Len(aFiles)
	
	cArquivo := aFiles[__y,1]
	alert("1")
	If Upper(left(cArquivo,8))$'CLIENTES'
		M010SA1(cArquivo)
	Elseif Upper(left(cArquivo,8))$'FORNECED'
		Alert("2")
		M010SA2(cArquivo)
	Endif
	
Next __y


//RESET ENVIRONMENT

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �M010SA1          �Fgarcia             � Data �  10/05/16    ���
�������������������������������������������������������������������������͹��
���Desc.     �  IMPORTA��O DE CLIENTE 					                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/


Static Function M010SA1(cArquivo)
Local aCampos 		:=  {}				//Array com os nomes dos campos vindos da web.
Local aIgnorados    :=  {}				//Campos que devem ser ignorados na rotina automatica.
Local aCabSA1 		:=  {}				//Array com os campos da rotina automatica.
Local nArq			:=  0
Local aArray 		:=  {}
local NERRO         :=  ""
local cQuery        :=  ""
Local Y

If File(cPath+cArquivo)
	nArq := FOpen(cPath+cArquivo)
Endif


If nArq <> 0
	
	
	
	If nArq > 0
		cTexto := ""
		Do while .T.
			cCarac := FReadStr(nArq,1)
			If cCarac == ""
				Exit
			Endif
			If cCarac == Chr(13)
				If !Empty(cTexto)
					aDad := Str2Array(Alltrim(cTexto),";")
					cTexto := ""
					aadd(aArray,aDad)
				Endif
			Else
				cTexto += cCarac
			Endif
			If FError() <> 0
				Exit
			Endif
		Enddo
		If !Empty(cTexto)
			aDad := Str2Array(Alltrim(cTexto),";")
			cTexto := ""
			aadd(aArray,aDad)
		Endif
		
		FClose(nArq)
	Endif
	
	For y:=2  to len (aArray)
		
		
		aResult := aArray[y]
		
		If Len(aResult) >= 59
			
			
			iF EMPTY (aResult[6])
				aCli:= 	U__BlNJE1(Alltrim(aResult[7]))
			Else
				XCG1:= 		U__BlNJE1(strtran(Alltrim(aResult[6]),".",""))
				aCli:= 	SUBSTR(XCG1,2,15)
				
			endif
			
			
			lNovo := .F.
			
			
			SA1->(DbSetOrder(3))
//			If SA1->(Dbseek(xFilial("SA1")+aCli))
//				lNovo := .F.
//			Else
				lNovo := .T.
//			Endif
			
			If lNovo
				RecLock("SA1",.T.)
				cCod 		:= GetSx8Num("SA1","A1_COD")
				
				A1_COD		:= cCod
				A1_LOJA		:= "0001"
				A1_FILIAL 	:= XFILIAL("SA1")
				
				ConfirmSx8()
				
				
				A1_XCOD :=  SUBSTR(Alltrim(aResult[1]),2,10)
				A1_XCLI	:= Alltrim(aResult[2])
				A1_XGER := Alltrim(aResult[3])
				A1_NOME :=  U__BlNJET(Alltrim(UPPER(aResult[4])))
				iF EMPTY (aResult[5])
					A1_NREDUZ:=  U__BlNJET(Alltrim(UPPER(aResult[4])))
				else
					A1_NREDUZ:=  U__BlNJET(Alltrim(UPPER(aResult[5])))
				Endif
				A1_XEMP := aResult[9]
				iF SUBSTR(Alltrim(aResult[10]),1,1)='F'
					A1_MSBLQL ='2'
				ELSE
					A1_MSBLQL='1'
				Endif
				A1_EMAIL	:= aResult[11]
				A1_XDESEMP := aResult[12]
				A1_END:= aResult[13]
				A1_XCODEND:= aResult[14]
				A1_CEP:= U__BlNJE1(Alltrim(aResult[15]))
				A1_MUN := U__BlNJET(Alltrim(UPPER(aResult[16])))
				A1_EST := aResult[17]
				IF ALLTRIM(aResult[17])  <> 'EX'
					A1_PAIS		:=	"105"
					A1_CODPAIS	:=	"01058"
					iF EMPTY (aResult[6])
						A1_CGC		:= 	U__BlNJE1(strtran(Alltrim(aResult[7]),".",""))
						A1_PESSOA	:= 'F'
						A1_TIPO		:= "F"
					Else
						XCG1:= 		U__BlNJE1(Alltrim(aResult[6]))
						A1_CGC		:= 	SUBSTR(strtran(XCG1,".",""),2,15)
						A1_PESSOA	:= 'J'
						A1_TIPO		:= "S"
					endif
					A1_INSCR   := U__BlNJE1(Alltrim(aResult[8]))
				ELSE
					A1_PAIS		:=	"999"
					A1_CODPAIS	:=	"99999"
					A1_PESSOA	:= 'X'
					A1_CGC		:= ""
					A1_TIPO		:= "F"
					A1_INSCR   	:= ""
				ENDIF
				A1_DDD:= aResult[18]
				A1_TEL:= U__BlNJE1(Alltrim(aResult[19]))
				A1_ENDCOB:= aResult[21]
				A1_XCODENC:= aResult[22]
				A1_CEPC:= U__BlNJE1(Alltrim(aResult[23]))
				A1_MUNC:= U__BlNJET(Alltrim(UPPER(aResult[24])))
				A1_ESTC:= U__BlNJET(Alltrim(aResult[25]))
				A1_DDDC:= U__BlNJET(Alltrim(aResult[26]))
				A1_TELC:= U__BlNJE1(Alltrim(aResult[27]))
				A1_ENDENT:= U__BlNJET(Alltrim(UPPER(aResult[29])))
				A1_XCODENE:= U__BlNJET(Alltrim(aResult[30]))
				A1_CEPE:= U__BlNJE1(Alltrim(aResult[31]))
				A1_MUNE:= U__BlNJET(Alltrim(UPPER(aResult[32])))
				A1_ESTE:= U__BlNJET(Alltrim(aResult[33]))
				A1_DDDE:= U__BlNJET(Alltrim(aResult[34]))
				A1_TELE:= U__BlNJE1(Alltrim(aResult[35]))
				A1_DTNASC :=ctod(aResult[37])
				A1_CONTATO :=U__BlNJET(Alltrim(aResult[38]))
				A1_SUFRAMA:=U__BlNJET(Alltrim(aResult[39]))
				IF SUBSTR(Alltrim(aResult[40]),1,1)='T'
					A1_XBLQVEN :='S'
				Else
					A1_XBLQVEN:='N'
				Endif
				IF SUBSTR(Alltrim(aResult[41]),1,1)='T'
					A1_XBLQPED :='S'
				Else
					A1_XBLQPED:='N'
				Endif
				A1_XHISTBQ := alltrim(aResult[42])+CHR(13)+CHR(10)
				A1_XGRUPO:=U__BlNJET(Alltrim(aResult[43]))
				A1_XGRUPON:=U__BlNJET(Alltrim(aResult[44]))
				A1_XCANAL:=U__BlNJET(Alltrim(aResult[45]))
				A1_XCANALD:=U__BlNJET(Alltrim(aResult[46]))
				A1_LC:=VAL(aResult[47])
				A1_XCODREG:=U__BlNJET(Alltrim(aResult[48]))
				A1_XDESREG:=U__BlNJET(Alltrim(aResult[49]))
				A1_XDTFUND:=ctod(aResult[50])
				A1_XCODDIV:=U__BlNJET(Alltrim(aResult[51]))
				A1_XNONDIV:=U__BlNJET(Alltrim(aResult[52]))
				A1_XMICRRE:=U__BlNJET(Alltrim(aResult[53]))
				A1_XMICRDE:=U__BlNJET(Alltrim(aResult[54]))
				A1_COD_MUN:=strzero(val(U__BlNJE1(Alltrim(aResult[55]))),5)
				A1_COD_MUC:=strzero(val(U__BlNJE1(Alltrim(aResult[56]))),5)
				A1_CODMUNE:=strzero(val(U__BlNJE1(Alltrim(aResult[57]))),5)
				A1_XOBS:= alltrim(aResult[58])+CHR(13)+CHR(10)
				A1_XOBSF:= alltrim(aResult[59])+CHR(13)+CHR(10)
				A1_BAIRRO:=U__BlNJET(Alltrim(UPPER(aResult[20])))
				A1_BAIRROC:=U__BlNJET(Alltrim(UPPER(aResult[28])))
				A1_BAIRROE:=U__BlNJET(Alltrim(UPPER(aResult[36])))
				SA1->(MsUnLock())
				
			Else
				
				RecLock("SA1",.F.)
			A1_XCOD :=  SUBSTR(Alltrim(aResult[1]),2,10)
				A1_XCLI	:= Alltrim(aResult[2])
				A1_XGER := Alltrim(aResult[3])
				A1_NOME :=  U__BlNJET(Alltrim(UPPER(aResult[4])))
				iF EMPTY (aResult[5])
					A1_NREDUZ:=  U__BlNJET(Alltrim(UPPER(aResult[4])))
				else
					A1_NREDUZ:=  U__BlNJET(Alltrim(UPPER(aResult[5])))
				Endif
				iF EMPTY (aResult[6])
					A1_CGC		:= 	U__BlNJE1(strtran(Alltrim(aResult[7]),".",""))
					A1_PESSOA	:= 'F'
					A1_TIPO		:= "F"
				Else
					XCG1:= 		U__BlNJE1(Alltrim(aResult[6]))
					A1_CGC		:= 	SUBSTR(strtran(XCG1,".",""),2,15)
					A1_PESSOA	:= 'J'
					A1_TIPO		:= "S"
				endif
				A1_INSCR   := U__BlNJE1(Alltrim(aResult[8]))
				A1_XEMP := aResult[9]
				iF SUBSTR(Alltrim(aResult[10]),1,1)='F'
					A1_MSBLQL ='2'
				ELSE
					A1_MSBLQL='1'
				Endif
				A1_EMAIL	:= aResult[11]
				A1_XDESEMP := aResult[12]
				A1_END:= aResult[13]
				A1_XCODEND:= aResult[14]
				A1_CEP:= U__BlNJE1(Alltrim(aResult[15]))
				A1_MUN := U__BlNJET(Alltrim(UPPER(aResult[16])))
				A1_EST := aResult[17]
				IF ALLTRIM(aResult[17])  <> 'EX'
					A1_PAIS		:=	"105"
					A1_CODPAIS	:=	"01058"
				ELSE
					A1_PAIS		:=	"999"
					A1_CODPAIS	:=	"99999"
					A1_PESSOA	:= 'X'
				ENDIF
				A1_DDD:= aResult[18]
				A1_TEL:= U__BlNJE1(Alltrim(aResult[19]))
				A1_ENDCOB:= aResult[21]
				A1_XCODENC:= aResult[22]
				A1_CEPC:= U__BlNJE1(Alltrim(aResult[23]))
				A1_MUNC:= U__BlNJET(Alltrim(UPPER(aResult[24])))
				A1_ESTC:= U__BlNJET(Alltrim(aResult[25]))
				A1_DDDC:= U__BlNJET(Alltrim(aResult[26]))
				A1_TELC:= U__BlNJE1(Alltrim(aResult[27]))
				A1_ENDENT:= U__BlNJET(Alltrim(UPPER(aResult[29])))
				A1_XCODENE:= U__BlNJET(Alltrim(aResult[30]))
				A1_CEPE:= U__BlNJE1(Alltrim(aResult[31]))
				A1_MUNE:= U__BlNJET(Alltrim(UPPER(aResult[32])))
				A1_ESTE:= U__BlNJET(Alltrim(aResult[33]))
				A1_DDDE:= U__BlNJET(Alltrim(aResult[34]))
				A1_TELE:= U__BlNJE1(Alltrim(aResult[35]))
				A1_DTNASC :=ctod(aResult[37])
				A1_CONTATO :=U__BlNJET(Alltrim(aResult[38]))
				A1_SUFRAMA:=U__BlNJET(Alltrim(aResult[39]))
				IF SUBSTR(Alltrim(aResult[40]),1,1)='T'
					A1_XBLQVEN :='S'
				Else
					A1_XBLQVEN:='N'
				Endif
				IF SUBSTR(Alltrim(aResult[41]),1,1)='T'
					A1_XBLQPED :='S'
				Else
					A1_XBLQPED:='N'
				Endif
				A1_XHISTBQ := alltrim(aResult[42])+CHR(13)+CHR(10)
				A1_XGRUPO:=U__BlNJET(Alltrim(aResult[43]))
				A1_XGRUPON:=U__BlNJET(Alltrim(aResult[44]))
				A1_XCANAL:=U__BlNJET(Alltrim(aResult[45]))
				A1_XCANALD:=U__BlNJET(Alltrim(aResult[46]))
				A1_LC:=VAL(aResult[47])
				A1_XCODREG:=U__BlNJET(Alltrim(aResult[48]))
				A1_XDESREG:=U__BlNJET(Alltrim(aResult[49]))
				A1_XDTFUND:=ctod(aResult[50])
				A1_XCODDIV:=U__BlNJET(Alltrim(aResult[51]))
				A1_XNONDIV:=U__BlNJET(Alltrim(aResult[52]))
				A1_XMICRRE:=U__BlNJET(Alltrim(aResult[53]))
				A1_XMICRDE:=U__BlNJET(Alltrim(aResult[54]))
				A1_COD_MUN:=strzero(val(U__BlNJE1(Alltrim(aResult[55]))),5)
				A1_COD_MUC:=strzero(val(U__BlNJE1(Alltrim(aResult[56]))),5)
				A1_CODMUNE:=strzero(val(U__BlNJE1(Alltrim(aResult[57]))),5)
				A1_XOBS:= alltrim(aResult[58])+CHR(13)+CHR(10)
				A1_XOBSF:= alltrim(aResult[59])+CHR(13)+CHR(10)
				A1_BAIRRO:=U__BlNJET(Alltrim(UPPER(aResult[20])))
				A1_BAIRROC:=U__BlNJET(Alltrim(UPPER(aResult[28])))
				A1_BAIRROE:=U__BlNJET(Alltrim(UPPER(aResult[36])))
				SA1->(MsUnLock())
			Endif
		ENDIF
	next
	
	
		cQuery := " UPDATE "+RetSqlName("SA1")+" SET A1_END = A1_ENDCOB,A1_CEP=A1_CEPC,A1_MUN=A1_MUNC,A1_EST=A1_ESTC,A1_DDD=A1_DDDC,A1_TEL=A1_TELC,A1_COD_MUN=A1_COD_MUC,A1_BAIRRO=A1_BAIRROC
		cQuery += " WHERE A1_EST =''
	NERRO := TCSQLEXEC(CQUERY)
	                                 
	
		cQuery := " UPDATE "+RetSqlName("SA1")+" SET A1_END=A1_ENDENT,A1_CEP=A1_CEPE,A1_MUN=A1_MUNE,A1_EST=A1_ESTE,A1_DDD=A1_DDDE,A1_TEL=A1_TELE,A1_COD_MUN=A1_CODMUNE,A1_BAIRRO=A1_BAIRROE
		cQuery += " WHERE A1_EST =''
	NERRO := TCSQLEXEC(CQUERY)	

       cQuery := " UPDATE "+RetSqlName("SA1")+" SET  A1_CONTA =ZA3_CONTA,A1_NATUREZ=ZA3_NATURE  
cQuery += " FROM SA1010, ZA3010  WHERE A1_XCANAL=ZA3_CODIGO  AND A1_CONTA=''
NERRO := TCSQLEXEC(CQUERY)	



// remove o arquivo da pasta de processamento
M010MOVARQ(cPath, cArquivo)
	
Endif

Return

//Fornecedor


Static Function M010SA2(cArquivo)
Local aCampos 		:=  {}				//Array com os nomes dos campos vindos da web.
Local aIgnorados    :=  {}				//Campos que devem ser ignorados na rotina automatica.
Local aCabSA1 		:=  {}				//Array com os campos da rotina automatica.
Local nArq			:=  0
Local aArray 		:=  {}
Local y

If File(cPath+cArquivo)
	nArq := FOpen(cPath+cArquivo)
Endif

alert(str(narq))
If nArq <> 0
	
	
	
	If nArq > 0
		cTexto := ""
		Do while .T.
			cCarac := FReadStr(nArq,1)
			If cCarac == ""
				Exit
			Endif
			If cCarac == Chr(13)
				If !Empty(cTexto)
					aDad := Str2Array(Alltrim(cTexto),";")
					cTexto := ""
					aadd(aArray,aDad)
				Endif
			Else
				cTexto += cCarac
			Endif
			If FError() <> 0
				Exit
			Endif
		Enddo
		If !Empty(cTexto)
			aDad := Str2Array(Alltrim(cTexto),";")
			cTexto := ""
			aadd(aArray,aDad)
		Endif
		
		FClose(nArq)
	Endif
	
	For y:=2  to len (aArray)
		
		aResult := aArray[y]
		
		If Len(aResult) >= 40
			
			
			iF EMPTY (aResult[4])
				aCli:= 	U__BlNJE1(Alltrim(aResult[5]))
			Else
				XCG1:= 		strtran(U__BlNJE1(Alltrim(aResult[4])),".","")
				aCli:= 	SUBSTR(XCG1,2,15)
				
			endif
			
			
			lNovo := .F.
			
			
//			SA2->(DbSetOrder(3))
//			If SA2->(Dbseek(xFilial("SA2")+aCli))
//				lNovo := .F.
//			Else
				lNovo := .T.
//			Endif
			
			If lNovo
				RecLock("SA2",.T.)
				cCod 		:= GetSx8Num("SA2","A2_COD")
				
				A2_COD		:= cCod
				A2_LOJA		:= "0001"
				A2_FILIAL 	:= XFILIAL("SA2")
				
				ConfirmSx8()
				A2_XCOD :=  SUBSTR(Alltrim(aResult[1]),2,10)
				A2_NOME :=   U__BlNJET(Alltrim(UPPER(aResult[2])))
				iF EMPTY (aResult[3])
					A2_NREDUZ:=   U__BlNJET(Alltrim(UPPER(aResult[2])))
				else
					A2_NREDUZ:=   U__BlNJET(Alltrim(UPPER(aResult[3])))
				Endif
				
				A2_XEMP 	:=U__BlNJET(Alltrim(aResult[7]))
				A2_EMAIL	:=aResult[8]
				A2_XDESEMP  :=	U__BlNJET(Alltrim(aResult[9]))
				A2_END      := U__BlNJET(Alltrim(UPPER(aResult[10])))
				A2_XCODEND	:=U__BlNJET(Alltrim(aResult[11]))
				A2_CEP      :=U__BlNJE1(Alltrim(aResult[12]))
				A2_MUN		:= U__BlNJET(Alltrim(UPPER(aResult[13])))
				A2_EST      :=U__BlNJET(Alltrim(aResult[14]))
				IF aResult[13]  <> 'EX'
					A2_PAIS		:=	"105"
					A2_CODPAIS	:=	"01058"
					iF EMPTY (aResult[4])
						A2_CGC		:= 	strtran(U__BlNJE1(Alltrim(aResult[5])),".","")
						A2_TIPO	:= 'F'
					Else
						XCG1:= U__BlNJE1(Alltrim(aResult[4]))
						A2_CGC		:= 	SUBSTR(strtran(XCG1,".",""),2,15)
						A2_TIPO	:= 'J'
					endif
					A2_INSCR	:=	U__BlNJE1(Alltrim(aResult[6]))

				ELSE
					A2_PAIS		:=	"99999"
					A2_CODPAIS	:=	"99999"
					A2_TIPO	:= 'X'
					A2_CGC		:= 	""
					A2_TIPO	:= 'J'
					A2_INSCR	:=	""
					
				ENDIF
				A2_DDD      :=U__BlNJE1(Alltrim(aResult[15]))
				A2_TEL 		:=U__BlNJE1(Alltrim(aResult[16]))
				A2_ENDCOB   := U__BlNJET(Alltrim(UPPER(aResult[18])))
				A2_XCODENC	:=U__BlNJET(Alltrim(aResult[19]))
				A2_CEPC		:=U__BlNJE1(Alltrim(aResult[20]))
				A2_MUNC   := U__BlNJET(Alltrim(UPPER(aResult[21])))
				A2_ESTC   :=U__BlNJET(Alltrim(aResult[22]))
				A2_DDDC   :=U__BlNJE1(Alltrim(aResult[23]))
				A2_TELC   :=U__BlNJE1(Alltrim(aResult[24]))
				A2_ENDENT := U__BlNJET(Alltrim(UPPER(aResult[26])))
				A2_XCODENE:=U__BlNJE1(Alltrim(aResult[27]))
				A2_CEPE		:=U__BlNJE1(Alltrim(aResult[28]))
				A2_ESTE:=U__BlNJET(Alltrim(aResult[30]))
				A2_MUNE:= U__BlNJET(Alltrim(UPPER(aResult[29])))
				A2_DDDE:=U__BlNJE1(Alltrim(aResult[31]))
				A2_TELE:=U__BlNJE1(Alltrim(aResult[32]))
				A2_DTNASC:=ctod(aResult[34])
				A2_CONTATO:= U__BlNJET(Alltrim(UPPER(aResult[35])))
				A2_SUFRAMA:=U__BlNJE1(Alltrim(aResult[36]))
//				A2_XGRUPO :=U__BlNJET(Alltrim(aResult[34]))
//				A2_XGRUPON :=U__BlNJET(Alltrim(aResult[35]))
//				A2_INSCR1:=U__BlNJE1(Alltrim(aResult[36]))
				A2_COD_MUN:=strzero(val(U__BlNJE1(Alltrim(aResult[37]))),5)
				A2_COD_MUC:=strzero(val(U__BlNJE1(Alltrim(aResult[38]))),5)
				A2_COD_MUE:=strzero(val(U__BlNJE1(Alltrim(aResult[39]))),5)
				A2_BAIRRO:= U__BlNJET(Alltrim(UPPER(aResult[17])))
				A2_BAIRRC:= U__BlNJET(Alltrim(UPPER(aResult[25])))
				A2_BAIRRE:= U__BlNJET(Alltrim(UPPER(aResult[33])))
				if aResult[40] ='T'
					A2_XOFICIN:='S'
				Else
					A2_XOFICIN:='N'
				Endif
				SA2->(MsUnLock())      
				
			Else
				
				RecLock("SA2",.F.)
				A2_XCOD :=  SUBSTR(Alltrim(aResult[1]),2,10)
				A2_NOME :=   U__BlNJET(Alltrim(UPPER(aResult[2])))
				iF EMPTY (aResult[3])
					A2_NREDUZ:=   U__BlNJET(Alltrim(UPPER(aResult[2])))
				else
					A2_NREDUZ:=   U__BlNJET(Alltrim(UPPER(aResult[3])))
				Endif
				
				A2_XEMP 	:=U__BlNJET(Alltrim(aResult[7]))
				A2_EMAIL	:=aResult[8]
				A2_XDESEMP  :=	U__BlNJET(Alltrim(aResult[9]))
				A2_END      := U__BlNJET(Alltrim(UPPER(aResult[10])))
				A2_XCODEND	:=U__BlNJET(Alltrim(aResult[11]))
				A2_CEP      :=U__BlNJE1(Alltrim(aResult[12]))
				A2_MUN		:= U__BlNJET(Alltrim(UPPER(aResult[13])))
				A2_EST      :=U__BlNJET(Alltrim(aResult[14]))
				IF aResult[13]  <> 'EX'
					A2_PAIS		:=	"105"
					A2_CODPAIS	:=	"01058"
					iF EMPTY (aResult[4])
						A2_CGC		:= 	strtran(U__BlNJE1(Alltrim(aResult[5])),".","")
						A2_TIPO	:= 'F'
					Else
						XCG1:= U__BlNJE1(Alltrim(aResult[4]))
						A2_CGC		:= 	SUBSTR(strtran(XCG1,".",""),2,15)
						A2_TIPO	:= 'J'
					endif
					A2_INSCR	:=	U__BlNJE1(Alltrim(aResult[6]))
				ELSE
					A2_PAIS		:=	"99999"
					A2_CODPAIS	:=	"99999"
					A2_TIPO		:= 'X'
					A2_CGC		:= 	""
					A2_TIPO		:= 'J'
					A2_INSCR	:=	""
				ENDIF
				A2_DDD      :=U__BlNJE1(Alltrim(aResult[15]))
				A2_TEL 		:=U__BlNJE1(Alltrim(aResult[16]))
				A2_ENDCOB   := U__BlNJET(Alltrim(UPPER(aResult[18])))
				A2_XCODENC	:=U__BlNJET(Alltrim(aResult[19]))
				A2_CEPC		:=U__BlNJE1(Alltrim(aResult[20]))
				A2_MUNC   := U__BlNJET(Alltrim(UPPER(aResult[21])))
				A2_ESTC   :=U__BlNJET(Alltrim(aResult[22]))
				A2_DDDC   :=U__BlNJE1(Alltrim(aResult[23]))
				A2_TELC   :=U__BlNJE1(Alltrim(aResult[24]))
				A2_ENDENT := U__BlNJET(Alltrim(UPPER(aResult[26])))
				A2_XCODENE:=U__BlNJE1(Alltrim(aResult[27]))
				A2_CEPE		:=U__BlNJE1(Alltrim(aResult[28]))
				A2_ESTE:=U__BlNJET(Alltrim(aResult[30]))
				A2_MUNE:= U__BlNJET(Alltrim(UPPER(aResult[29])))
				A2_DDDE:=U__BlNJE1(Alltrim(aResult[31]))
				A2_TELE:=U__BlNJE1(Alltrim(aResult[32]))
				A2_DTNASC:=ctod(aResult[34])
				A2_CONTATO:= U__BlNJET(Alltrim(UPPER(aResult[35])))
				A2_SUFRAMA:=U__BlNJE1(Alltrim(aResult[36]))
//				A2_XGRUPO :=U__BlNJET(Alltrim(aResult[34]))
//				A2_XGRUPON :=U__BlNJET(Alltrim(aResult[35]))
//				A2_INSCR1:=U__BlNJE1(Alltrim(aResult[36]))
				A2_COD_MUN:=strzero(val(U__BlNJE1(Alltrim(aResult[37]))),5)
				A2_COD_MUC:=strzero(val(U__BlNJE1(Alltrim(aResult[38]))),5)
				A2_COD_MUE:=strzero(val(U__BlNJE1(Alltrim(aResult[39]))),5)
				A2_BAIRRO:= U__BlNJET(Alltrim(UPPER(aResult[17])))
				A2_BAIRRC:= U__BlNJET(Alltrim(UPPER(aResult[25])))
				A2_BAIRRE:= U__BlNJET(Alltrim(UPPER(aResult[33])))
				if aResult[40] ='T'
					A2_XOFICIN:='S'
				Else
					A2_XOFICIN:='N'
				Endif
				SA2->(MsUnLock())
				
			Endif
		ENDIF
	next
	
	
	// remove o arquivo da pasta de processamento
M010MOVARQ(cPath, cArquivo)
	
Endif

Return





// Cadastro de clientes



Static Function Str2Array(cString, cDelim, cStr)

Local aReturn := {}
Local cAux    := cString
Local nPos    := 0
Local nPosAux := 0
Local nX      := 0
Local nI      := 0

Default cDelim := "|"
Default cStr   := ""

While At(cDelim, cAux) > 0
	nPos := At(cDelim, cAux)
	AAdd(aReturn, SubStr(cAux, 1, nPos-1))
	cAux := SubStr(cAux, nPos+1)
End
AAdd(aReturn, cAux)

If !Empty(cStr)
	For nI := 1 To Len(aReturn)
		aReturn[nI] := StrTran(aReturn[nI], cStr, " ")
		aReturn[nI] := AllTrim(aReturn[nI])
	Next nI
EndIf

Return(aReturn)

Static Function M010MOVARQ(cPath, cArqProc,cArqProc2)
Default cArqProc2 := ""

//��������������������������������������������������������������������������
//� Apos ler o arquivo, move para a pasta Lidos.							�
//��������������������������������������������������������������������������
If File(cPath + cArqProc)
	__CopyFile(cPath + cArqProc, cPath + "\Lidos\" + cArqProc)
	FErase(cPath + cArqProc)
EndIf

If File(cPath + cArqProc2)
	__CopyFile(cPath + cArqProc2, cPath + "\Lidos\" + cArqProc2)
	FErase(cPath + cArqProc2)
EndIf


Return ()




User Function _BlNJET(cTexto)   


	// acento agudo
	cTexto := StrTran( cTexto, "�", "a" )
	cTexto := StrTran( cTexto, "�", "e" )
	cTexto := StrTran( cTexto, "�", "i" )
	cTexto := StrTran( cTexto, "�", "o" )
	cTexto := StrTran( cTexto, "�", "u" )
	cTexto := StrTran( cTexto, "�", "A" )
	cTexto := StrTran( cTexto, "�", "E" )
	cTexto := StrTran( cTexto, "�", "I" )
	cTexto := StrTran( cTexto, "�", "O" )
	cTexto := StrTran( cTexto, "�", "U" )
	
	// acento circunflexo
	cTexto := StrTran( cTexto, "�", "a" )
	cTexto := StrTran( cTexto, "�", "e" )
	cTexto := StrTran( cTexto, "�", "i" )
	cTexto := StrTran( cTexto, "�", "o" )
	cTexto := StrTran( cTexto, "�", "u" )
	cTexto := StrTran( cTexto, "�", "A" )
	cTexto := StrTran( cTexto, "�", "E" )
	cTexto := StrTran( cTexto, "�", "I" )
	cTexto := StrTran( cTexto, "�", "O" )
	cTexto := StrTran( cTexto, "�", "U" )
	
	// til
	cTexto := StrTran( cTexto, "�", "a" )
	cTexto := StrTran( cTexto, "�", "o" )
	cTexto := StrTran( cTexto, "�", "n" )
	cTexto := StrTran( cTexto, "�", "A" )
	cTexto := StrTran( cTexto, "�", "O" )
	cTexto := StrTran( cTexto, "�", "N" )
	
	// ce-cedilha
	cTexto := StrTran( cTexto, "�", "c" )
	cTexto := StrTran( cTexto, "�", "C" )
	
	// trema
	cTexto := StrTran( cTexto, "�", "a" )
	cTexto := StrTran( cTexto, "�", "e" )
	cTexto := StrTran( cTexto, "�", "i" )
	cTexto := StrTran( cTexto, "�", "o" )
	cTexto := StrTran( cTexto, "�", "u" )
	cTexto := StrTran( cTexto, "�", "A" )
	cTexto := StrTran( cTexto, "�", "E" )
	cTexto := StrTran( cTexto, "�", "I" )
	cTexto := StrTran( cTexto, "�", "O" )
	cTexto := StrTran( cTexto, "�", "U" )
	
	// "a" e "o" 3� e 3� Terceira e Terceiro
	cTexto := StrTran( cTexto, "�", " " )
	cTexto := StrTran( cTexto, "�", " " )
	  
	// crase
	cTexto := StrTran( cTexto, "�", "a" )
	cTexto := StrTran( cTexto, "�", "e" )
	cTexto := StrTran( cTexto, "�", "i" )
	cTexto := StrTran( cTexto, "�", "o" )
	cTexto := StrTran( cTexto, "�", "u" )
	cTexto := StrTran( cTexto, "�", "A" )
	cTexto := StrTran( cTexto, "�", "E" )
	cTexto := StrTran( cTexto, "�", "I" )
	cTexto := StrTran( cTexto, "�", "O" )
	cTexto := StrTran( cTexto, "�", "U" )

	// especiais
	cTexto := StrTran( cTexto, "-", " " )
	cTexto := StrTran( cTexto, "!", " " )
	cTexto := StrTran( cTexto, "||", " " )
	cTexto := StrTran( cTexto, "|", " " )
	cTexto := StrTran( cTexto, "'", " " )
	cTexto := StrTran( cTexto, "@", " " )
	cTexto := StrTran( cTexto, "#", " " )
	cTexto := StrTran( cTexto, "$", " " )
	cTexto := StrTran( cTexto, "%", " " )
	cTexto := StrTran( cTexto, "�", " " )
	cTexto := StrTran( cTexto, "&", "e" )
	cTexto := StrTran( cTexto, "*", " " )
	cTexto := StrTran( cTexto, "(", " " )
	cTexto := StrTran( cTexto, ")", " " )
	cTexto := StrTran( cTexto, "+", " " )
	cTexto := StrTran( cTexto, "=", " " )
	cTexto := StrTran( cTexto, ",", " " )
	cTexto := StrTran( cTexto, ";", " " )
	cTexto := StrTran( cTexto, ":", " " )
	cTexto := StrTran( cTexto, "?", " " )
	cTexto := StrTran( cTexto, "/", " " )
	cTexto := StrTran( cTexto, "\", " " )
	cTexto := StrTran( cTexto, "<", " " )
	cTexto := StrTran( cTexto, ">", " " )
//	cTexto := StrTran( cTexto, ".", " " )


//UPPER

Return(cTexto)                        


User Function _BlNJE1(_cTexto)
_cTexto:=StrTran(_cTexto,"'","")
_cTexto:=StrTran(_cTexto,"�","")
_cTexto:=StrTran(_cTexto,"�","")
_cTexto:=StrTran(_cTexto,'"',"")
_cTexto:=StrTran(_cTexto,"!","")
_cTexto:=StrTran(_cTexto,"#","")
_cTexto:=StrTran(_cTexto,"$","")
_cTexto:=StrTran(_cTexto,"%","")
_cTexto:=StrTran(_cTexto,"*","")
_cTexto:=StrTran(_cTexto,"=","")
_cTexto:=StrTran(_cTexto,"+","")
_cTexto:=StrTran(_cTexto,"{","")
_cTexto:=StrTran(_cTexto,"}","")
_cTexto:=StrTran(_cTexto,"<","")
_cTexto:=StrTran(_cTexto,">","")
_cTexto:=StrTran(_cTexto,"-","")
_cTexto:=StrTran(_cTexto,"(","")
_cTexto:=StrTran(_cTexto,")","")
_cTexto:=StrTran(_cTexto,"/","")
_cTexto:=StrTran(_cTexto,"�","")

Return(_cTexto)

                              