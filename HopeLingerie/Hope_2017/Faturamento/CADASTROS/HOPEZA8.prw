#INCLUDE "rwmake.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HOPEZA8   � Autor � Roberto Le�o       � Data �  10/11/16   ���
�������������������������������������������������������������������������͹��
���Descricao � Cadastro de Situa��o do Produto.                           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE LINGERIE                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function HOPEZA8()

Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������

Private cPerg   := "ZA8"
Private cString := "ZA8"

dbSelectArea("ZA8")
dbSetOrder(1)

cPerg   := "ZA8"

Pergunte(cPerg,.F.)
SetKey(123,{|| Pergunte(cPerg,.T.)}) // Seta a tecla F12 para acionamento dos parametros

AxCadastro(cString,"Cadastro de Situa��o do Produto",cVldExc,cVldAlt)

Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros


Return
