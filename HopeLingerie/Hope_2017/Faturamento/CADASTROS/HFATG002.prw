// #########################################################################################
// Projeto:
// Modulo :
// Fonte  : HFATG002
// ---------+-------------------+-----------------------------------------------------------
// Date     | Author             | Description
// ---------+-------------------+-----------------------------------------------------------
// 05/04/18 | TOTVS | Developer Studio | Gerado pelo Assistente de C�digo
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

//------------------------------------------------------------------------------------------
/*/{Protheus.doc} novo


@author    TOTVS | Developer Studio - Gerado pelo Assistente de C�digo
@version   1.xx
@since     5/04/2018
/*/
//------------------------------------------------------------------------------------------
user function HFATG002()

_cCli:= GETSXENUM("SA1","A1_COD")

DbSelectArea("SA1")
DbSetOrder(1)
DbSeek(xfilial("SA1")+_cCli)

While Found()
	CONFIRMSX8() 
	_cCli:= GETSXENUM("SA1","A1_COD")
	DbSeek(xfilial("SA1")+_cCli)
Enddo
		
return(_cCli)
