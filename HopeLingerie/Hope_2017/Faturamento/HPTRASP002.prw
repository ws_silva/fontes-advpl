#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH" 
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'       
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"

user function HPTRASP002()
  

	Local aArea		:= GetArea()
	Local aCoors	:= FwGetDialogSize( oMainWnd )
	Local oFWLayer,oPanelTop1,oView
	Local cOk      := "B" 
	Local aFiltros := {}
	Local bBlockClique
	Local cPerg := "HPTRASP002"
	Private aGrid := {}
	Private oBrowse, oMarkBrow
	Private aNF	:= {}
	Static nPedido := 1
	Static nNF     := 2
	Static nCodCli := 3
	Static nNomClia := 4
	Static nLojaCli := 5
	Static nUF  := 6
	Static nMun := 7
	Static nVol := 8
	Static nPeso := 9
	Static nOk := 10
	Static nTransp := 11
	Static nRedes := 12
	Static nSerie := 13

	SetPrvt("oDlg1","oBrw1")	 
	
	Private oDlg
		
	aNF  := RF23iTEM(aNF)
	
	oDlg := MSDialog():New( aCoors[1],aCoors[2],aCoors[3],aCoors[4],"Romaneio de Transporte",,,.F.,,,,,,.T.,,,.T. )

	oFWLayer	:= FWLayer():New()
	oFWLayer:Init( oDlg, .F., .T. )		
	oFWLayer:AddLine( 'TOP1', 50, .F. )
	
	oPanelTop1 := oFWLayer:getLinePanel( 'TOP1' )
	
	oBrowse	    := FWFormBrowse():New()
	oMarkBrow	:= oBrowse:FWBrowse()
	oMarkBrow:SetDataArray()
	oMarkBrow:SetArray(@aNF)
	oMarkBrow:SetDescription('Romaneio')
	oMarkBrow:SetOwner( oDlg )
	oMarkBrow:SetProfileID("4")
	oMarkBrow:SetMenuDef("")
	oMarkBrow:SetDoubleClick({|| nil })	
	bBlockClique	:= {|| U_CLICK23() }
	
		
	//oMarkBrow:AddButton("Imprimir",{||HOPEREL1()},,,)
	oMarkBrow:AddButton("Filtro",{||Filtro()},,,)
	oMarkBrow:AddButton("Marca/Desmarca Todos",{||MarcarTodos()},,,)
	oMarkBrow:AdButton("Excluir",{||EXCLUI()})
	//oMarkBrow:AddButton("Transportadora",{||YTRANSP(aNF,oMarkBrow,1)},,,)
	
	oBrowse:SetUseFilter()

	Aadd( aFiltros, { "aNF[oBrowse:At()]["+cvaltochar(nRedes)+"]", GetSx3Cache("F2_REDESP","X3_TITULO"),"C",GetSx3Cache("F2_REDESP","X3_TAMANHO"),0,GetSx3Cache("F2_REDESP","X3_PICTURE")} )
	Aadd( aFiltros, { "aNF[oBrowse:At()]["+cvaltochar(nTransp)+"]", GetSx3Cache("F2_TRANSP","X3_TITULO"),"C",GetSx3Cache("F2_TRANSP","X3_TAMANHO"),0,GetSx3Cache("F2_TRANSP","X3_PICTURE")} )
	
	oBrowse:SetFieldFilter(aFiltros)
	
	oMarkBrow:AddMarkColumns(;
	{||If(aNF[oMarkBrow:At()][nOk]=="","LBNO","LBTICK")};	//Imagem da marca
	,bBlockClique;
	,{|| MarcarTodos()})


	//Pedido	
	oMarkBrow:SetColumns({{;
	"Pedido",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aNF),aNF[oMarkBrow:At()][nPedido],"")},;  		   // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("D2_PEDIDO","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("D2_PEDIDO","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	// Nota Fiscal
	oMarkBrow:SetColumns({{;
	"Numero",;  					   			                        // T�tulo da coluna
	{|| If(!Empty(aNF),aNF[oMarkBrow:At()][nNF],"")},;  				// Code-Block de carga dos dados
	"C",;                                                        	    // Tipo de dados
	"@!",;                       			                            // M�scara
	1,;					   				                                // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("F2_DOC","X3_TAMANHO"),;                       	        // Tamanho
	GetSx3Cache("F2_DOC","X3_DECIMAL"),;                       	        // Decimal
	.F.,;                                       			            // Indica se permite a edi��o
	{||.T.},;                                   			            // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                            // Indica se exibe imagem
	bBlockClique,;                     			                        // Code-Block de execu��o do duplo clique
	NIL,;                                       			            // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			            // Code-Block de execu��o do clique no header
	.F.,;                                       			            // Indica se a coluna est� deletada
	.T.,;                     				   				            // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                                // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	// NOME CLIENTE
	oMarkBrow:SetColumns({{;
	"Cliente",;  					   			                        // T�tulo da coluna
	{|| If(!Empty(aNF),aNF[oMarkBrow:At()][nNomClia],"")},;             // Code-Block de carga dos dados
	"C",;                                                        	    // Tipo de dados
	"@!",;                       			                            // M�scara
	1,;					   				                                // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_NOME","X3_TAMANHO"),;                       	    // Tamanho
	GetSx3Cache("A1_NOME","X3_DECIMAL"),;                       	    // Decimal
	.F.,;                                       			            // Indica se permite a edi��o
	{||.T.},;                                   			            // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                            // Indica se exibe imagem
	bBlockClique,;                     			                        // Code-Block de execu��o do duplo clique
	NIL,;                                       			            // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			            // Code-Block de execu��o do clique no header
	.F.,;                                       			            // Indica se a coluna est� deletada
	.T.,;                     				   				            // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                                // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	
		// Estado
	oMarkBrow:SetColumns({{;
	"UF",;  					   			                            // T�tulo da coluna
	{|| If(!Empty(aNF),aNF[oMarkBrow:At()][nUF] ,"")},;                 // Code-Block de carga dos dados
	"C",;                                                        	    // Tipo de dados
	"@!",;                       			                            // M�scara
	1,;					   				                                // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_EST","X3_TAMANHO"),;                       	        // Tamanho
	GetSx3Cache("A1_EST","X3_DECIMAL"),;                       	        // Decimal
	.F.,;                                       			            // Indica se permite a edi��o
	{||.T.},;                                   			            // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                            // Indica se exibe imagem
	bBlockClique,;                     			                        // Code-Block de execu��o do duplo clique
	NIL,;                                       			            // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			            // Code-Block de execu��o do clique no header
	.F.,;                                       			            // Indica se a coluna est� deletada
	.T.,;                     				   				            // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                                // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	
	// Municipio do cliente
	oMarkBrow:SetColumns({{;
	"Municipio",;  					   			                       // T�tulo da coluna
	{|| If(!Empty(aNF),posicione("SA1",1,xFilial("SA1") + aNF[oMarkBrow:At()][nCodCli] + aNF[oMarkBrow:At()][nLojaCli],"A1_MUN"),"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A1_MUN","X3_TAMANHO"),;                       	       // Tamanho
	GetSx3Cache("A1_MUN","X3_DECIMAL"),;                       	       // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
		
	//Volume
	oMarkBrow:SetColumns({{;
	"Volume",;  					   			                        // T�tulo da coluna
	{|| If(!Empty(aNF),POSICIONE("SF2",1,XFILIAL("SF2") + aNF[oMarkBrow:At()][nNF] + aNF[oMarkBrow:At()][nSerie],"F2_VOLUME1"),"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("F2_VOLUME1","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("F2_VOLUME1","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	//Peso
	oMarkBrow:SetColumns({{;
	"Peso",; 					   			               // T�tulo da coluna
	{|| If(!Empty(aNF),POSICIONE("SF2",1,XFILIAL("SF2") + aNF[oMarkBrow:At()][nNF] + aNF[oMarkBrow:At()][nSerie],"F2_PLIQUI" ),"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@E 999,999.9999",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("F2_PLIQUI","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("F2_PLIQUI","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	//Transportadora
	oMarkBrow:SetColumns({{;
	"Transportadora",;  					   			               // T�tulo da coluna
	{|| If(!Empty(aNF),U_YDESTRA(aNF[oMarkBrow:At()][nTransp]) ,"")},;  // Code-Block de carga dos dados
	"C",;                                                        	   // Tipo de dados
	"@!",;                       			                           // M�scara
	1,;					   				                               // Alinhamento (0=Centralizado, 1=Esquerda ou 2=Direita)
	GetSx3Cache("A4_NOME","X3_TAMANHO"),;                       	   // Tamanho
	GetSx3Cache("A4_NOME","X3_DECIMAL"),;                       	   // Decimal
	.F.,;                                       			           // Indica se permite a edi��o
	{||.T.},;                                   			           // Code-Block de valida��o da coluna ap�s a edi��o
	NIL,;                       		 	                           // Indica se exibe imagem
	bBlockClique,;                     			                       // Code-Block de execu��o do duplo clique
	NIL,;                                       			           // Vari�vel a ser utilizada na edi��o (ReadVar)
	{||.T.},;                                   			           // Code-Block de execu��o do clique no header
	.F.,;                                       			           // Indica se a coluna est� deletada
	.T.,;                     				   				           // Indica se a coluna ser� exibida nos detalhes do Browse
	}};		                			                               // Op��es de carga dos dados (Ex: 1=Sim, 2=N�o)
	)
	
	oMarkBrow:Activate()
	oDlg:Activate(,,,.T.)
	RestArea(aArea)
	
	aNF := {}
	
Return

Static Function RF23ITEM(aNF,cRedesp,cTrasnp)

	Local cPerg := "HPTRASP002"
	
	cQuery := " SELECT D2_PEDIDO AS PEDIDO,F2_DOC AS NF,F2_SERIE AS SERIE,F2_CLIENTE AS COD_CLI,SUBSTRING(A1_NOME,1,38) AS CLIENTE, F2_LOJA AS LOJA, " 
	cQuery += " CASE WHEN (A1_ESTE ='' OR A1_ESTE = NULL OR LTRIM(RTRIM(A1_ESTE)) = 'VAZIO' OR LTRIM(RTRIM(A1_ESTE)) = 'VA') THEN A1_EST ELSE A1_ESTE END AS UF, "  
	cQuery += " CASE WHEN (A1_MUNE = '' OR A1_MUNE = NULL OR LTRIM(RTRIM(A1_MUNE)) = 'VAZIO') THEN A1_MUN ELSE A1_MUNE END AS MUNICIPIO, "
	cQuery += " F2_VOLUME1 AS VOL,F2_PLIQUI AS PESO,F2_TRANSP,F2_REDESP, "
	cQuery += " (SELECT A4_NOME FROM "+RetSqlName("SA4")+" SA4 (NOLOCK) WHERE A4_COD = F2_TRANSP AND D_E_L_E_T_ = '') AS TRANSPORTADORA, "
	cQuery += " (SELECT A4_NOME FROM "+RetSqlName("SA4")+" SA4 (NOLOCK) WHERE A4_COD = F2_REDESP AND D_E_L_E_T_ = '') AS REDESPACHO "
	cQuery += " FROM "+RetSqlName("SF2")+" SF2 (NOLOCK) JOIN  "+RetSqlName("SA1")+" SA1 (NOLOCK) ON(F2_LOJA = A1_LOJA AND F2_CLIENTE = A1_COD) "
	cQuery += " JOIN "+RetSqlName("SD2")+" SD2 (NOLOCK) ON(D2_FILIAL = F2_FILIAL AND D2_DOC = F2_DOC) "          
	cQuery += " WHERE  F2_VOLUME1 >=1  
	IF EMPTY(cRedesp) .AND. EMPTY(cTrasnp)
		cQuery += " AND F2_REDESP <> ''  AND F2_TRANSP <> '' "
	ELSEIF !EMPTY(cRedesp) .AND. EMPTY(cTrasnp)
		cQuery += " AND F2_REDESP = '"+cRedesp+"' "
	ELSEIF !EMPTY(cTrasnp) .AND. EMPTY(cRedesp)
		cQuery += " AND F2_TRANSP = '"+cTrasnp+"' "
	ELSE
	    cQuery += " AND F2_REDESP = '"+cRedesp+"'  AND F2_TRANSP = '"+cTrasnp+"' "
	ENDIF
	cQuery += " AND SF2.D_E_L_E_T_ ='' AND SD2.D_E_L_E_T_ ='' AND SA1.D_E_L_E_T_ ='' AND F2_YROMANE <> '' "
	cQuery += " GROUP BY F2_DOC,F2_FILIAL,F2_CLIENTE,A1_NOME,A1_ESTE,A1_EST,A1_MUNE, A1_MUN, "
	cQuery += " F2_VOLUME1,F2_PLIQUI,F2_EMISSAO,D2_PEDIDO,F2_LOJA,F2_TRANSP,F2_REDESP,F2_SERIE " 
	
	TcQuery cQuery New Alias T01

	WHILE !T01->(EOF())
		aadd(aNF,	{ALLTRIM(T01->PEDIDO),;
		ALLTRIM(T01->NF),;
		ALLTRIM(T01->COD_CLI),;
		ALLTRIM(T01->CLIENTE),;
		ALLTRIM(T01->LOJA),;
		ALLTRIM(T01->UF),;
		ALLTRIM(T01->MUNICIPIO),;
		ALLTRIM(T01->VOL),;		
		ALLTRIM(T01->PESO),;
		"",;
		ALLTRIM(T01->F2_TRANSP),;
		ALLTRIM(T01->F2_REDESP),;
		ALLTRIM(T01->SERIE)})

		T01->(dbSkip())
	ENDDO 
	
	T01-> (dbclosearea())
	
Return aNF

Static Function HOPEREL1()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\TEMP\"				
	Local oFont1 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont2 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont3 	:= TFont():New( "Arial",,16,,.T.,,,,,  .F. )
	Local cFilePrint := ""
	Local nLin   := 100
	Local nTmLin := 10
	Local nSalto := 10
	Local cLinha :=''
	
	U_HPREL()
	aNF := {}
	RF23ITEM(aNF)
	oMarkBrow:SetArray(@aNF)
	oBrowse:refresh()
	oMarkBrow:refresh()			
	
Return

User Function HPREL()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\TEMP\"					
	Local cFilePrint := "romaneio.pdf"
	Local xArea   := GetArea()	
	Local cLogo   := "\system\logohope.png"
	Local cLinha  := ""
	Private cPerg := "ROMA"
	Private oPrinter
	Private oFont1 	:= TFont():New( "Arial",,11,,.T.,,,,,  .F. )
	Private oFont2 	:= TFont():New( "Arial",,10,,.F.,,,,,  .F. )
	Private oFont3 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Private nQuantNf   := 0
	Private nQuantVol  := 0
	Private nQuantPeca := 0
	Private nPesoLi := 0
	Private nValNF := 0
	Private nCont := 0
	Private nLin   := 25
	Private nTmLin := 15
	Private nSalto := 20
		
	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()
	oPrinter:StartPage()	

	ProcRegua(0)

	oPrinter:Say ( nLin, 200, "ROMANEIO DE ENTREGA POR TRANSPORTADORA",  oFont3 )
	oPrinter:SayBitmap( nLin-010, 050, cLogo, 100, 058)
	nLin := nLin + nTmLin
	
		For x:=1 to len(aNF)
		
		If aNF[x][nOk] == "1"
			cLinha += "'"+aNF[x][nPedido]+aNF[x][nNF]+aNF[x][nCodCli]+aNF[x][nLojaCli]+"',"
		endif

	Next x
	
	cLinha := Substring(cLinha,1,len(cLinha)-1)
		
	cNum := GETMV("HP_SEQROMA")
	
	_qry1 := " Update SF2010 SET F2_YROMANE = '"+Alltrim(cNum)+"' FROM SF2010 JOIN SD2010 ON D2_FILIAL = F2_FILIAL AND D2_CLIENTE = F2_CLIENT AND D2_LOJA = F2_LOJA AND D2_DOC = F2_DOC  "
	_qry1 += " WHERE D2_PEDIDO+F2_DOC+F2_CLIENTE+F2_LOJA IN (" +cLinha+ ") "
	
	TcSqlExec(_qry1)
			

	cNewpar := SOMA1(cNum)
	
	PUTMV("HP_SEQROMA",Alltrim(cNewpar))

	if Select("ROM") > 0
		ROM->(dbCloseArea())
	endif

	cQuery := " SELECT D2_PEDIDO AS PEDIDO,F2_DOC AS NF,F2_CLIENTE AS COD_CLI,SUBSTRING(A1_NOME,1,38) AS CLIENTE,F2_YROMANE AS ROMANEIO,  " 
	cQuery += " CASE WHEN (A1_ESTE ='' OR A1_ESTE = NULL OR LTRIM(RTRIM(A1_ESTE)) = 'VAZIO' OR LTRIM(RTRIM(A1_ESTE)) = 'VA') THEN A1_EST ELSE A1_ESTE END AS UF, "  
	cQuery += " CASE WHEN (A1_MUNE = '' OR A1_MUNE = NULL OR LTRIM(RTRIM(A1_MUNE)) = 'VAZIO') THEN A1_MUN ELSE A1_MUNE END AS MUNICIPIO, "
	cQuery += " F2_VOLUME1 AS VOL,F2_PLIQUI AS PESO,F2_VALBRUT AS VALOR_NF ,SUM(D2_QUANT) AS QTD_PECA, "
	cQuery += " (SELECT A4_NOME FROM "+RetSqlName("SA4")+" SA4010 WHERE A4_COD = F2_TRANSP AND D_E_L_E_T_ = '') AS TRANSPORTADORA, "
	cQuery += " (SELECT A4_NOME FROM "+RetSqlName("SA4")+" SA4010 WHERE A4_COD = F2_REDESP AND D_E_L_E_T_ = '') AS REDESPACHO "
	cQuery += " FROM "+RetSqlName("SF2")+ " SF2010 JOIN  "+RetSqlName("SA1")+" SA1010 ON(F2_LOJA = A1_LOJA AND F2_CLIENTE = A1_COD) "
	cQuery += " JOIN "+RetSqlName("SD2")+ " SD2010 ON(D2_FILIAL = F2_FILIAL AND D2_DOC = F2_DOC) "          
	cQuery += " WHERE  F2_VOLUME1 >=1  AND D2_PEDIDO+F2_DOC+F2_CLIENTE+F2_LOJA IN (" +cLinha+ ")  " 
	cQuery += " AND SF2010.D_E_L_E_T_ ='' AND SD2010.D_E_L_E_T_ ='' AND SA1010.D_E_L_E_T_ ='' AND F2_YROMANE = '' "
	cQuery += " GROUP BY F2_DOC,F2_FILIAL,F2_CLIENTE,A1_NOME,A1_ESTE,A1_EST,A1_MUNE, A1_MUN, "
	cQuery += " F2_VOLUME1,F2_PLIQUI,F2_EMISSAO,D2_PEDIDO, F2_REDESP, F2_TRANSP, F2_VALBRUT,F2_YROMANE " 
	
	TcQuery cQuery New Alias ROM	 

	cTrans := ROM->TRANSPORTADORA
	cRedes := ROM->REDESPACHO
	cRoma :=  ROM->ROMANEIO


	oPrinter:Say ( nLin, 200, "Transp: ", oFont3 )
	oPrinter:Say ( nLin, 250, cTrans, oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Remetente:  ", oFont3 )
	oPrinter:Say ( nLin, 260, " HOPE DO NORDESTE LTDA ", oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Endere�o:  ", oFont3 )
	oPrinter:Say ( nLin, 260, "RUA ARGEU BRAGA HERBSTER,610 ", oFont2 )
	nLin := nLin + nTmLin
	
	oPrinter:Say ( nLin, 200, "Cidade: ", oFont3 )
	oPrinter:Say ( nLin, 250, "MARANGUAPE-CE ", oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Romaneio: ", oFont3 )
	oPrinter:Say ( nLin, 250, cRoma, oFont2 )
	nLin := nLin + nTmLin 

	nLin := nLin + 25
	oPrinter:Say ( nLin,  20, "NF",       	oFont1 )
	oPrinter:Say ( nLin, 100, "CLIENTE",  	oFont1 )
	oPrinter:Say ( nLin, 300, "CIDADE",     oFont1 )
	oPrinter:Say ( nLin, 400, "UF",   		oFont1 )
	oPrinter:Say ( nLin, 450, "PESO",      	oFont1 )
	oPrinter:Say ( nLin, 500, "VOLUME", 	oFont1 )
	oPrinter:Say ( nLin, 550, "TOTAL", 		oFont1 )


	While !ROM->(EOF())

		IF nLin >= 750
			nLin   := 25
			oPrinter:EndPage()
			oPrinter:StartPage()
		ENDIF

		oPrinter:Say ( nLin+10,   20, ROM->NF ,                     oFont2 )
		oPrinter:Say ( nLin+10,  100, ROM->CLIENTE,                 oFont2 )
		oPrinter:Say ( nLin+10,  300, ROM->MUNICIPIO,              oFont2 )
		oPrinter:Say ( nLin+10,  400, ROM->UF ,              		oFont2 )
		oPrinter:Say ( nLin+10,  450, cValToChar(ROM->PESO),  		oFont2 )
		oPrinter:Say ( nLin+10,  500, cValToChar(ROM->VOL) ,     	oFont2 )
		oPrinter:Say ( nLin+10,  550, cValToChar(ROM->VALOR_NF),    oFont2 )
			
		
		nValNF     += ROM->VALOR_NF
		nQuantVol  += ROM->VOL
		nPesoLi    += ROM->PESO
		nQuantPeca += ROM->QTD_PECA

		ROM->(dbSkip())
		nLin := nLin + 10
		nCont++
		
	ENDDO
	
	nLin := nLin + 30
	FunRdp()
	
	T01->(DbCloseArea())
	ROM->(DbCloseArea())

	oPrinter:EndPage()

	oPrinter:Preview()
	oPrinter:SetViewPDF ( .T. )
	Restarea(xArea)

	HOPEREL2()	
return

Static Function FunRdp()

	Local cLinha1 := ""

	For x:=1 to len(aNF)
		
		If aNF[x][nOk] == "1"
			cLinha1 += "'"+aNF[x][nNF]+"',"
		endif

	Next x
	
	cLinha1 := Substring(cLinha1,1,len(cLinha1)-1)
	
	cQuery1 := " SELECT SUM(CAST(T.VOL AS int)) AS VOL ,T.DESCR AS DESCR FROM ( "
	cQuery1 += " SELECT "
	cQuery1 += " MAX(ZR_VOLUME) AS VOL,ZQ_DESCRIC AS DESCR,D2_DOC "
	cQuery1 += " FROM "+RetSqlName("SZJ")+" (NOLOCK) JOIN "+RetSqlName("SD2")+ " SD2010 (NOLOCK) ON D2_DOC = ZJ_DOC AND D2_CLIENTE = ZJ_CLIENTE AND D2_SERIE = ZJ_SERIE "
	cQuery1 += " JOIN "+RetSqlName("SZR")+" (NOLOCK) ON SZR010.ZR_NUMPF = SZJ010.ZJ_NUMPF AND SZR010.D_E_L_E_T_ = '' "
	cQuery1 += " JOIN "+RetSqlName("SZQ")+" (NOLOCK) ON (ZR_TPVOL = ZQ_CODIGO) "
	cQuery1 += " WHERE SD2010.D_E_L_E_T_ = '' AND SZJ010.D_E_L_E_T_ = '' AND D2_DOC IN ("+cLinha1+")  "
	cQuery1 += " GROUP BY ZQ_DESCRIC,D2_DOC "
	cQuery1 += " )T GROUP BY T.DESCR"
	
	TcQuery cQuery1 New Alias T01
	 
	cdescri := T01->DESCR
	cQuant :=  T01->VOL
	
	nLin := nLin + 10
	
    IF nLin >= 750
		oPrinter:EndPage()
	ENDIF
	
	oPrinter:Say ( nLin, 20, "Descri��o",  oFont3 )
	oPrinter:Say ( nLin, 500,"Qtd Volumes", oFont3 )
	nLin := nLin + nTmLin
	
	While !T01->(EOF()) 
	
	oPrinter:Say ( nLin, 20, cdescri,  oFont2 )	
	oPrinter:Say ( nLin, 500, cQuant,  oFont2 )
	nLin := nLin + nTmLin
	
	T01->(dbSkip())
	nLin := nLin + 10
	ENDDO
	
	nLin := nLin + 100	
	oPrinter:Say ( nLin, 20, "QTD PE�A",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nQuantPeca, "@E 999,999,999"),20),  oFont2 )	
	
	nLin := nLin + nTmLin	
	oPrinter:Say ( nLin, 20, "VOLUMES",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nQuantVol, "@E 999,999,999"),20),  oFont2 )
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "N� NOTAS",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nCont, "@E 9999,999,999.99"),20),  oFont2 )	
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "TOTAL DE NOTAS",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nValNF, "@E 9999,999,999.99"),20),  oFont2 )			

	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "PESO BRUTO:",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nPesoLi, "@E 9999,999,999.99"),20),  oFont2 )				


	nLin := nLin + nTmLin + 10
	oPrinter:Say ( nLin, 20, "Declara��o: ",  oFont1 )
	nLin := nLin + nTmLin
	oPrinter:Say ( nLin, 20, "Recebemos para transporte aos seus destinos, as cargas constantes nas notas fiscais supra relacionadas. ",  oFont1 )
	
	nLin := nLin + nTmLin + 50
	oPrinter:Say ( nLin, 20, cTrans,  oFont3 )
	
	oPrinter:EndPage()
				
Return

Static Function HOPEREL2()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\TEMP\"				
	Local oFont1 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont2 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont3 	:= TFont():New( "Arial",,16,,.T.,,,,,  .F. )
	Local cFilePrint := ""
	Local nLin   := 100
	Local nTmLin := 10
	Local nSalto := 10
	Local cLinha :=''
	
	U_HPREL2()
	aNF := {}
	RF23ITEM(aNF)
	oMarkBrow:SetArray(@aNF)
	oBrowse:refresh()
	oMarkBrow:refresh()			
	
Return

User Function HPREL2()

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\TEMP\"					
	Local cFilePrint := "redespacho.pdf"
	Local xArea := GetArea()	
	Local cLogo := "\system\logohope.png"
	Local cLinha := ""
	Private cPerg		:= "ROMA"
	Private oPrinter
	Private oFont1 	:= TFont():New( "Arial",,11,,.T.,,,,,  .F. )
	Private oFont2 	:= TFont():New( "Arial",,10,,.F.,,,,,  .F. )
	Private oFont3 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Private nQuantNf := 0
	Private nQuantVol := 0
	Private nQuantPeca := 0
	Private nPesoLi := 0
	Private nValNF := 0
	Private nCont := 0
	Private nLin   := 25
	Private nTmLin := 15
	Private nSalto := 20
		
	oPrinter := FWMSPrinter():New(cFilePrint, IMP_PDF , lAdjustToLegacy,cLocal, lDisableSetup, .T., , , , , .F., )
	oPrinter:SetPortrait()
	oPrinter:StartPage()	

	ProcRegua(0)

	oPrinter:Say ( nLin, 200, "ROMANEIO DE ENTREGA POR TRANSPORTADORA",  oFont3 )
	oPrinter:SayBitmap( nLin-010, 050, cLogo, 100, 058)
	nLin := nLin + nTmLin
	
		For x:=1 to len(aNF)
		
		If aNF[x][nOk] == "1"
			cLinha += "'"+aNF[x][nPedido]+aNF[x][nNF]+aNF[x][nCodCli]+aNF[x][nLojaCli]+"',"
		endif

	Next x
	
	cLinha := Substring(cLinha,1,len(cLinha)-1)
	
	if Select("ROM") > 0
		ROM->(dbCloseArea())
	endif

	cQuery := " SELECT D2_PEDIDO AS PEDIDO,F2_DOC AS NF,F2_CLIENTE AS COD_CLI,SUBSTRING(A1_NOME,1,38) AS CLIENTE,F2_YROMANE AS ROMANEIO,  " 
	cQuery += " CASE WHEN (A1_ESTE ='' OR A1_ESTE = NULL OR LTRIM(RTRIM(A1_ESTE)) = 'VAZIO' OR LTRIM(RTRIM(A1_ESTE)) = 'VA') THEN A1_EST ELSE A1_ESTE END AS UF, "  
	cQuery += " CASE WHEN (A1_MUNE = '' OR A1_MUNE = NULL OR LTRIM(RTRIM(A1_MUNE)) = 'VAZIO') THEN A1_MUN ELSE A1_MUNE END AS MUNICIPIO, "
	cQuery += " F2_VOLUME1 AS VOL,F2_PLIQUI AS PESO,F2_VALBRUT AS VALOR_NF ,SUM(D2_QUANT) AS QTD_PECA, "
	cQuery += " (SELECT A4_NOME FROM "+RetSqlName("SA4")+" WHERE A4_COD = F2_TRANSP AND D_E_L_E_T_ = '') AS TRANSPORTADORA, "
	cQuery += " (SELECT A4_NOME FROM "+RetSqlName("SA4")+" WHERE A4_COD = F2_REDESP AND D_E_L_E_T_ = '') AS REDESPACHO "
	cQuery += " FROM "+RetSqlName("SF2")+" JOIN  "+RetSqlName("SA1")+" ON(F2_LOJA = A1_LOJA AND F2_CLIENTE = A1_COD) "
	cQuery += " JOIN "+RetSqlName("SD2")+" ON(D2_FILIAL = F2_FILIAL AND D2_DOC = F2_DOC) "          
	cQuery += " WHERE  F2_VOLUME1 >=1  AND D2_PEDIDO+F2_DOC+F2_CLIENTE+F2_LOJA IN (" +cLinha+ ")  " 
	cQuery += " AND SF2010.D_E_L_E_T_ ='' AND SD2010.D_E_L_E_T_ ='' AND SA1010.D_E_L_E_T_ ='' AND F2_YROMANE = '' "
	cQuery += " GROUP BY F2_DOC,F2_FILIAL,F2_CLIENTE,A1_NOME,A1_ESTE,A1_EST,A1_MUNE, A1_MUN, "
	cQuery += " F2_VOLUME1,F2_PLIQUI,F2_EMISSAO,D2_PEDIDO, F2_REDESP, F2_TRANSP, F2_VALBRUT,F2_YROMANE " 
	
	TcQuery cQuery New Alias ROM	 

	cTrans := ROM->TRANSPORTADORA
	cRedes := ROM->REDESPACHO
	cRoma :=  ROM->ROMANEIO

	oPrinter:Say ( nLin, 200, "Transp: ", oFont3 )
	oPrinter:Say ( nLin, 250, cTrans, oFont2 )
	nLin := nLin + nTmLin 

	oPrinter:Say ( nLin, 200, "Redespacho:  ", oFont3 )
	oPrinter:Say ( nLin, 280, cRedes, oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Romaneio:  ", oFont3 )
	oPrinter:Say ( nLin, 280, cRoma , oFont2 )
	nLin := nLin + nTmLin 
	
	nLin := nLin + 25
	oPrinter:Say ( nLin,  20, "NF",       	oFont1 )
	oPrinter:Say ( nLin, 100, "CLIENTE",  	oFont1 )
	oPrinter:Say ( nLin, 300, "CIDADE",     oFont1 )
	oPrinter:Say ( nLin, 400, "UF",   		oFont1 )
	oPrinter:Say ( nLin, 450, "PESO",      	oFont1 )
	oPrinter:Say ( nLin, 500, "VOLUME", 	oFont1 )
	oPrinter:Say ( nLin, 550, "TOTAL", 		oFont1 )

	While !ROM->(EOF())

		IF nLin >= 750
			nLin   := 25
			oPrinter:EndPage()
			oPrinter:StartPage()
		ENDIF

		oPrinter:Say ( nLin+10,   20, ROM->NF ,                     oFont2 )
		oPrinter:Say ( nLin+10,  100, ROM->CLIENTE,                 oFont2 )
		oPrinter:Say ( nLin+10,  300, ROM->MUNICIPIO ,              oFont2 )
		oPrinter:Say ( nLin+10,  400, ROM->UF ,              		oFont2 )
		oPrinter:Say ( nLin+10,  450, cValToChar(ROM->PESO),  		oFont2 )
		oPrinter:Say ( nLin+10,  500, cValToChar(ROM->VOL) ,     	oFont2 )
		oPrinter:Say ( nLin+10,  550, cValToChar(ROM->VALOR_NF),    oFont2 )
		

		nValNF     += ROM->VALOR_NF
		nQuantVol  += ROM->VOL
		nPesoLi    += ROM->PESO
		nQuantPeca += ROM->QTD_PECA

		ROM->(dbSkip())
		nLin := nLin + 10
		nCont++
		
	ENDDO
	nLin := nLin + 30
	FunRdp2()

	ROM->(DbCloseArea())

	oPrinter:EndPage()

	oPrinter:Preview()
	oPrinter:SetViewPDF ( .T. )
	Restarea(xArea)
return

Static Function FunRdp2()

	nLin := nLin + 10
	
    IF nLin >= 750
		oPrinter:EndPage()
	ENDIF
	
		
	oPrinter:Say ( nLin, 20, "QTD PE�A",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nQuantPeca, "@E 999,999,999"),20),  oFont2 )	
	
	nLin := nLin + nTmLin	
	oPrinter:Say ( nLin, 20, "VOLUMES",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nQuantVol, "@E 999,999,999"),20),  oFont2 )
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "N� NOTAS",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nCont, "@E 9999,999,999.99"),20),  oFont2 )	
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "TOTAL DE NOTAS",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nValNF, "@E 9999,999,999.99"),20),  oFont2 )			

	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "PESO BRUTO:",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nPesoLi, "@E 9999,999,999.99"),20),  oFont2 )				
		
	oPrinter:EndPage()
					
Return

Static function MarcarTodos()
	
	for y:=1 to len(aNF)
		IF aNF[y][nOk] == "" 
			aNF[y][nOk]:= "1"			
		else			
			aNF[y][nOk] := ""
		endif
	next y
	
	oBrowse:refresh()
	oMarkBrow:refresh()	
Return

Static Function Query(aNFo,bSld,bVisual)

	Local cLinha := "" 
	Local cQuery
	
	aGrid := {}		
	lRet := .T.
Return lRet

STATIC FUNCTION FILTRO()

	Local lOk	:= .F.
	Local cArea := GetArea()
	Local cTransportadora := Space(GetSx3Cache("A4_COD","X3_TAMANHO"))
	Local aParam := {}
	Local aRetParm	:= {}

	aAdd(aParam,{1,"Redespacho"		,Space(GetSx3Cache("A4_COD","X3_TAMANHO")),"",".T.","SA4",".T.",80,.F.})
	aAdd(aParam,{1,"Transportadora"	,Space(GetSx3Cache("A4_COD","X3_TAMANHO")),"",".T.","SA4",".T.",80,.F.})
	
	If ParamBox(aParam,"Filtro",@aRetParm,{||.T.},,,,,,"U_RF023",.F.,.F.)
		lOk	:= .T.
		cRedesp := aRetParm[1]
		cTrasnp := aRetParm[2]
	else 
		return
	EndIf

	aNF:= {}
	RF23ITEM(aNF,cRedesp,cTrasnp)
	oMarkBrow:SetArray(@aNF)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)
	
RETURN

STATIC FUNCTION EXCLUI()

	Local lOk	:= .F.
	Local cArea := GetArea()
	

	aNF:= {}
	RF23ITEM(aNF,cRedesp,cTrasnp)
	oMarkBrow:SetArray(@aNF)
	oMarkBrow:refresh()
	oBrowse:refresh()
	oMarkBrow:goto(1,.T.)
	
RETURN  

USER FUNCTION CLICK23()

	IF aNF[oMarkBrow:At()][nOk] == ""
		aNF[oMarkBrow:At()][nOk]:= "1"
	ELSE 
		aNF[oMarkBrow:At()][nOk] := ""   
	ENDIF
RETURN

// FUN��O PARA ATUALIZAR AS TRANSPORTADORAS

STATIC FUNCTION YTRANSP(aNF,oMarkBrow)

	Local lOk	:= .F.
	Local cArea := GetArea()
	Local cTransportadora	:= Space(GetSx3Cache("A4_COD","X3_TAMANHO"))
	Local aParam := {}
	Local aRetParm	:= {}
	Local aTransp := {}
	Local cModal := ""
	Local cTab := ""

	aAdd(aParam,{1,"Transportadora"		,cTransportadora	,GetSx3Cache("A4_COD","X3_PICTURE"),"existCpo('SA4')","SA4",".T.",80,.F.})

	If ParamBox(aParam,"Escolha da transportadora",@aRetParm,{||.T.},,,,,,"U_HPTRASP002",.T.,.T.)
		lOk	:= .T.
	EndIf

	if lOk

		For x:=1 to len(aNF)
			If aNF[x][nOk] == "1"
				
				DBSELECTAREA("SF2")
				DBSETORDER(2)
				DBSEEK(xFilial("SF2") + aNF[x][nCodCli] + aNF[x][nLojaCli] + aNF[x][nNF])

				If Found()
					RECLOCK("SF2", .F.)

					SF2->F2_TRANSP := aRetParm[1]
					
					MSUNLOCK()

				EndIf

				cQuery := " SELECT D2_FILIAL,D2_PEDIDO , D2_DOC,D2_CLIENTE,D2_LOJA "
				cQuery += " FROM "+RETSQLNAME("SD2")+" SD2 "
				cQuery += " WHERE D_E_L_E_T_=''  "
				cQuery += " AND  D2_FILIAL = '"+xFilial("SD2")+"' "
				cQuery += " AND  D2_DOC = '"+aNF[x][nNF]+"' "
				cQuery += " AND  D2_CLIENTE = '"+aNF[x][nCodCli]+"' "
				cQuery += " AND  D2_LOJA   = '"+aNF[x][nLojaCli]+"' "
			
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"T08",.F.,.T.)

				IF !T08->(EOF())

					//  PEDIDO 1
					dbSelectArea("SC5")
					dbSetOrder(1)// SC5_FILIAL + SC5_NUM + SC5_CLIENTE + SC5_LOJA
					dbSeek(T08->D2_FILIAL + T08->D2_PEDIDO + T08->D2_CLIENTE + T08->D2_LOJA  ) // Busca exata

					IF FOUND() // Avalia o retorno da pesquisa realizada
						RECLOCK("SC5", .F.)

						C5_TRANSP := aRetParm[1]

						MSUNLOCK()// Destrava o registro
					ENDIF

				ENDIF

				T08->(DBCLOSEAREA())
				
				//STATUS DA GRID
				aNF[x][nTransp] := aRetParm[1]
				MsgInfo("Transportadora alterada com sucesso","HOPE")
			endif
		NEXT

		aRetParm :={}  //limpando array
		aParam   :={}  //limpando array

		RestArea( cArea )
	endif
	YDESMARCA()
	oMarkBrow:refresh()

RETURN

STATIC FUNCTION YDESMARCA()

	FOR nAx := 1 to len(aNF)
		aNF[nAx][nOK] := ""
	NEXT

RETURN

USER FUNCTION YDESTRA(cCod)
	Local cDesc := ""

	cQuery := " select A4_NOME "
	cQuery += " FROM " + RETSQLNAME("SA4")+"  "
	cQuery += " WHERE D_E_L_E_T_ =''  "
	cQuery += " AND   A4_FILIAL  = '"+xFilial("SA4")+"' "
	cQuery += " AND   A4_COD = '"+cCod+"' "
	TCQuery cquery new alias T05

	IF !T05->(eof())
		cDesc:= ALLTRIM(T05->A4_NOME)
	ENDIF
	T05->(dbclosearea())

Return cDesc
