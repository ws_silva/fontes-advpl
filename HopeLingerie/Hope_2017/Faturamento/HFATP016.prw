#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#INCLUDE "TBICONN.CH"

// #########################################################################################
// Projeto: HOPE
// Modulo : FaturamentoDANIEL
// ---------+-------------------+-----------------------------------------------------------
// Data     | Daniel R. Melo    | 
// ---------+-------------------+-----------------------------------------------------------
// 01/07/16 | Actual Trend      | Job para atualização de Status de Pedidos
// ---------+-------------------+-----------------------------------------------------------
// #########################################################################################


User function HFATP016()

	Prepare Environment Empresa "01" Filial "0101"
	
	Processa({|| JOBPED1A() },"Processando...", "Ajustando Status dos Produtos ...")
	u_HPCPP016()
	Reset Environment

Return

Static function JOBPED1A()

/*
	DbSelectArea("SB1")
	DbSetOrder(1)
	ProcRegua(RecCount())

	While !EOF()
		IncProc()
		If AllTrim(B1_YSITUAC) == "PNV"
			If  B1_YDATLIB < dDatabase .and. B1_YDATLIB <> Ctod("  /  /    ")
				If B1_YDATINA < dDatabase .or. B1_YDATINA == Ctod("  /  /    ")
					RecLock("SB1",.F.)
					SB1->B1_YSITUAC := "PV"
					MsUnlock()
				EndIf
			EndIf
		ElseIf AllTrim(B1_YSITUAC) == "PV" .or. AllTrim(B1_YSITUAC) == "PVFO"
			If B1_YDATINA < dDatabase .and. B1_YDATINA <> Ctod("  /  /    ")
				RecLock("SB1",.F.)
				SB1->B1_YSITUAC := "NPV"
				MsUnlock()
			EndIf
		EndIf
	
		DbSkip()

	EndDo
*/

CQUERY := "UPDATE "+RETSQLNAME("SB1")+" SET B1_YSITUAC = 'NPV' "
CQUERY += "   FROM "+RETSQLNAME("SB1")+"  B1 "
CQUERY += "LEFT JOIN  "+RETSQLNAME("SZL")+" ZL ON  ZL_PRODUTO = SUBSTRING(B1_COD,1,8) AND ZL.D_E_L_E_T_ = '' AND ZL_COR = SUBSTRING(B1_COD,9,3) AND ZL_TAM = SUBSTRING(B1_COD,12,4) "
CQUERY += "WHERE B1.D_E_L_E_T_<>'*' " 
CQUERY += "       AND B1_YSITUAC IN ('PV','PVFO') "
//CQUERY += "       AND B1_YDATLIB < '20180111' AND B1_YDATLIB <> '' "
//CQUERY += "	   AND (ZL_DTENCER < '"+dtos(dDatabase)+"' OR ZL_DTENCER <> '') "
CQUERY += "	   AND (ZL_DTENCER < '"+dtos(dDatabase)+"' and ZL_DTENCER <> '') "
TCSQLEXEC(CQUERY)

CQUERY := "UPDATE "+RETSQLNAME("SB1")+" SET B1_YSITUAC = 'PV' "
CQUERY += "   FROM "+RETSQLNAME("SB1")+"  B1 "
CQUERY += "LEFT JOIN  "+RETSQLNAME("SZL")+" ZL ON  ZL_PRODUTO = SUBSTRING(B1_COD,1,8) AND ZL.D_E_L_E_T_ = '' AND ZL_COR = SUBSTRING(B1_COD,9,3) AND ZL_TAM = SUBSTRING(B1_COD,12,4) "
CQUERY += "WHERE B1.D_E_L_E_T_<>'*' " 
CQUERY += "       AND B1_YSITUAC = 'PNV' "
CQUERY += "       AND B1_YDATLIB < '"+dtos(dDatabase)+"' AND B1_YDATLIB <> '' "
//CQUERY += "	   AND (ZL_DTENCER < '20180811' OR ZL_DTENCER ='') "

TCSQLEXEC(CQUERY)

Return