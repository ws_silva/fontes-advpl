#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

User Function HFATP014()                        

	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	local cAlias

	DbSelectArea("SZY")
	DbSetOrder(1)
	DbSeek(xfilial("SZY")+__CUSERID)

	If 	__CUSERID = SZY->ZY_CODUSU
			
		cAlias := "ZZ3"
		chkFile(cAlias)
		dbSelectArea(cAlias)
		dbSetOrder(1)
		private cCadastro := "Libera��o de Comercial"
		aRotina := {;
			{ "Pesquisar" , "AxPesqui"   , 0, 1},;
			{ "Visualizar", "u_PolCo3(4)", 0, 2},;
			{ "Liberar"   , "u_PolCo3(2)", 0, 4},;
			{ "Estornar"  , "u_PolCo3(3)", 0, 5};
			}
	
		dbSelectArea(cAlias)
		mBrowse( 6, 1, 22, 75, cAlias)
	
	Else
		Alert ("Usu�rio sem acesso a Rotina!")
	End
	
return

User Function PolCo3(_tp)


Local oButton1
Local oButton2
Local oGet1
Local oGet2
Local oSay1
Local oSay2
Private oFolder1
Private cGet1 := Space(6)
Private cGet2 := Space(1)

Static oDlg

If _tp <> 1
	cGet1 := ZZ3->ZZ3_PEDIDO
	cGet2 := ZZ3->ZZ3_DATA
Endif
  DEFINE MSDIALOG oDlg TITLE "Libera��o de Pedidos" FROM 000, 000  TO 500, 900 COLORS 0, 16777215 PIXEL

    @ 012, 015 SAY oSay1 PROMPT "Pedido" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 011, 049 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    @ 012, 130 SAY oSay2 PROMPT "Data" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 011, 166 MSGET oGet2 VAR cGet2 SIZE 259, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    fMSNewGe1(_tp)
    If _tp == 2
    	@ 233, 388 BUTTON oButton1 PROMPT "Liberar" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
    ElseIf _tp == 3
    	@ 233, 388 BUTTON oButton1 PROMPT "Estornar" SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
    End
    @ 233, 344 BUTTON oButton2 PROMPT "Cancelar" SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL

  ACTIVATE MSDIALOG oDlg CENTERED

Return

//------------------------------------------------ 
Static Function fMSNewGe1(_tp)
//------------------------------------------------ 
Local nX
Local aFieldFill := {}
Local aFields := {"ZZ1_BLQ","ZZ1_DESCBL","ZZ1_DTLIB","ZZ1_USRLIB"}
Local aAlterFields := {}
Private aHeader1 := {}
Private aCols1 := {}

Static oMSNewGe1

  	// Define field properties
  	DbSelectArea("SX3")
  	SX3->(DbSetOrder(2))
  	For nX := 1 to Len(aFields)
    	If SX3->(DbSeek(aFields[nX]))
      		Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
                       SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

	DbSelectArea("ZZ1")
	DbSetOrder(1)
	DbSeek(xfilial("ZZ1")+cGet1)

	While !EOF() .and. ZZ1->ZZ1_PEDIDO == cGet1
		Aadd(aCols1,{ZZ1->ZZ1_BLQ,ZZ1->ZZ1_DESCBL,ZZ1->ZZ1_DTLIB,ZZ1->ZZ1_USRLIB,.F.})  
  		DbSkip()
  	End
  
	oMSNewGe1 := MsNewGetDados():New( 030, 010, 225, 443, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", , aHeader1, aCols1)

Return


Static Function Save(_tp)

Local I
Local cEOL		:= +Chr(13)+Chr(10)

If _tp == 2
	For i := 1 to Len(OMSNEWGE1:ACOLS)
		DbSelectArea("ZZ1")
		DbSetOrder(1)
		DbSeek(xfilial("ZZ1")+cGet1+OMSNEWGE1:ACOLS[i,1])
		
		//"TA"	"T�tulos em Atraso"							//Titulos em Atraso (em Aberto Vecidos)
		If OMSNEWGE1:ACOLS[i,1]="TA" .and. SZY->ZY_TITATRA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"PA"	"Pago em Atrazo"								//Titulos em Atraso (Pagos com Atrazo)
		If OMSNEWGE1:ACOLS[i,1]="PA" .and. SZY->ZY_TITATRP="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"CI"	"Cliente Inativo"								//Cliente Inativo
		If OMSNEWGE1:ACOLS[i,1]="CI" .and. SZY->ZY_CLIINAT="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"CN"	"Cliente Novo"								//Cliente Novo
		If OMSNEWGE1:ACOLS[i,1]="CN" .and. SZY->ZY_CLINOVO="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"CP"	"Cliente Incompleto"							//Dados Incompletos
		If OMSNEWGE1:ACOLS[i,1]="CP" .and. SZY->ZY_DINCOMP="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"OB"	"Observacao Cliente"							//Observa��o do Pedido
		If OMSNEWGE1:ACOLS[i,1]="OB" .and. SZY->ZY_OSBPED="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End
		
		//"CH"	"Cheque Devolvido"							//Cheque Devolvido
		If OMSNEWGE1:ACOLS[i,1]="CH" .and. SZY->ZY_CHEDEV="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"PI"	"Produto Inativo"								//Produto inativo
		If OMSNEWGE1:ACOLS[i,1]="PI" .and. SZY->ZY_PRODINA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"LM"	"Limite Excedido"								//Limite Ultrapassado
		If OMSNEWGE1:ACOLS[i,1] $ "LM|QL" .and. SZY->ZY_LMULTRA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"LM"	"Limite Excedido"								//Limite Ultrapassado
		If OMSNEWGE1:ACOLS[i,1] $ "LM|QL" .and. SZY->ZY_LMULTRA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End	
		
		//"ND"	"Notas de Debito"								//Nota Debito
		If OMSNEWGE1:ACOLS[i,1]="ND" .and. SZY->ZY_NOTADEB="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End
		
		//"PR"	"T�tulos Protesto"							//Titulos em Protesto
		If OMSNEWGE1:ACOLS[i,1]="PR" .and. SZY->ZY_TITPROT="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End
		
		//"PE"	"Perdas (Conta a Receber)"					//Conta Perdas
		If OMSNEWGE1:ACOLS[i,1]="PE" .and. SZY->ZY_CTPERDA="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"VP"	"Valor Minimo Pedido"						//Valor do Pedido
		If OMSNEWGE1:ACOLS[i,1]="VP" .and. SZY->ZY_VALPED="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End
		
		//"BC"	"Bloqueio por Cliente"						//Cliente
		If OMSNEWGE1:ACOLS[i,1]="BC" .and. SZY->ZY_CLIENTE="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End
						
		//"PG"	"Bloqueio por Tabela Cond. Pagamento"		//Condi��o de Pagamento
		If OMSNEWGE1:ACOLS[i,1]="PG" .and. SZY->ZY_CONDPAG="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End

		//"PC"	"Bloqueio por Tabela de Pre�os"				//SZY-> 	//Tabela de Pre�o
		If OMSNEWGE1:ACOLS[i,1]="PC" .and. SZY->ZY_TBPRECO="S" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB			with ddatabase
				Replace ZZ1_USRLIB		with cUsername
			MsUnLock()
		End
		
		//"PD"	"Bloqueio por Tipo de Pedido"				//SZZ
		If OMSNEWGE1:ACOLS[i,1]="PD" .and. AllTrim(OMSNEWGE1:ACOLS[i,4])=""
			DbSelectArea("SZZ")
			DbSetOrder(1)
			DbSeek(xfilial("ZZ1")+__CUSERID)
			
			_qry := " SELECT COUNT(C5_TPPED) AS CONT FROM "+RetSqlName("ZZ1")+" ZZ1 "+cEOL
			_qry += " 		INNER JOIN "+RetSqlName("SC5")+" SC5 ON SC5.D_E_L_E_T_<>'*' AND C5_NUM=ZZ1_PEDIDO "+cEOL
			_qry += " 		INNER JOIN "+RetSqlName("SZZ")+" SZZ ON SZZ.D_E_L_E_T_<>'*' AND ZZ_USUARIO='"+__CUSERID+"' AND ZZ_TPPED=C5_TPPED "+cEOL
			_qry += " WHERE ZZ1.D_E_L_E_T_<>'*' "+cEOL
			_qry += " 		AND ZZ1_PEDIDO='126827' "+cEOL
			_qry += " 	AND ZZ1_BLQ='PD' "+cEOL
	
	
			If Select("TMPZZ1") > 0 
				TMPZZ1->(DbCloseArea()) 
			EndIf 
	
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPZZ1",.T.,.T.)
			DbSelectArea("TMPZZ1")
			DbGoTop()
			If TMPZZ1->CONT > 0
				RecLock("ZZ1",.F.)
					Replace ZZ1_DTLIB			with ddatabase
					Replace ZZ1_USRLIB		with cUsername
				MsUnLock()
			End
			
			TMPZZ1->(DbCloseArea())

		End

	Next

	DbSelectArea("ZZ1")
	DbSetOrder(1)
	DbSeek(xfilial("ZZ1")+cGet1)

	_blq := "L"
	
	While !EOF() .and. ZZ1->ZZ1_PEDIDO = cGet1 .and. _blq = "L"
		If alltrim(ZZ1->ZZ1_USRLIB) = ""
			_blq := "B"
		Endif
		DbSelectArea("ZZ1")
		Dbskip()
	End

	If _blq = "L"
		DbSelectArea("ZZ3")
		DbSetOrder(1)
		DbSeek(xfilial("ZZ3")+cGet1)
		
		RecLock("ZZ3",.F.)
			Replace ZZ3_STATUS with _blq
		MsUnLock()

		DbSelectArea("SC5")
		DbSetOrder(1)
		DbSeek(xfilial("SC5")+cGet1)
		
		RecLock("SC5",.F.)
			Replace C5_XBLQ with _blq
		MsUnLock()
	Endif

ElseIf _tp == 3
	For i := 1 to Len(OMSNEWGE1:ACOLS)
		DbSelectArea("ZZ1")
		DbSetOrder(1)
		DbSeek(xfilial("ZZ1")+cGet1+OMSNEWGE1:ACOLS[i,1])

		If OMSNEWGE1:ACOLS[i,4]=SUBSTRING(cUsername,1,10)
			RecLock("ZZ1",.F.)
				Replace ZZ1_DTLIB 	with ctod("  /  /  ")
				Replace ZZ1_USRLIB	with ""
			MsUnLock()
		End

	Next

	DbSelectArea("ZZ3")
	DbSetOrder(1)
	DbSeek(xfilial("ZZ3")+cGet1)
		
	RecLock("ZZ3",.F.)
		Replace ZZ3_STATUS with "B"
	MsUnLock()
		
	DbSelectArea("SC5")
	DbSetOrder(1)
	DbSeek(xfilial("SC5")+cGet1)
		
	RecLock("SC5",.F.)
		Replace C5_XBLQ with "B"
	MsUnLock()

Endif

Return