#include "rwmake.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � SF2460I  �Autor  �Daniel R. Melo      � Data �  23/03/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Ponto de Entrada para chamar a fun��o que atualiza os dados���
���Desc.     � da expedicao da nota fiscal                                ���
���          � ex: peso liquido, volume, transportadora..                 ���
�������������������������������������������������������������������������͹��
���Uso       � Especifico HOPE - www.actualtrend.com.br                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function SF2460I()

	Local aArea    := GetArea()
	Local nxTaxa   := 0
	Local _XVLTXCC := 0 //Round((SE1->E1_VALOR*nxTaxa)/100,2)
	Local _XVLRLIQ := 0 //SE1->E1_VALOR-Round((SE1->E1_VALOR*nxTaxa)/100,2)

//Grava o NSU no registro SE1

	_desc := 0

	DbSelectArea("SA1")
	DbSetOrder(1)
	If DbSeek(xfilial("SA1")+SF2->F2_CLIENTE+SF2->F2_LOJA)

		If SA1->A1_XTPDE01 = "F"
			_desc += SA1->A1_XDESC01*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE02 = "F"
			_desc += SA1->A1_XDESC02*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE03 = "F"
			_desc += SA1->A1_XDESC03*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE04 = "F"
			_desc += SA1->A1_XDESC04*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE05 = "F"
			_desc += SA1->A1_XDESC05*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE06 = "F"
			_desc += SA1->A1_XDESC06*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE07 = "F"
			_desc += SA1->A1_XDESC07*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE08 = "F"
			_desc += SA1->A1_XDESC08*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE09 = "F"
			_desc += SA1->A1_XDESC09*(1-(_desc/100))
		Endif
		If SA1->A1_XTPDE10 = "F"
			_desc += SA1->A1_XDESC10*(1-(_desc/100))
		Endif
	EndIf  

	DbSelectarea("SE1")
	DbSetOrder(1)
	DbSeek(xfilial("SE1")+SF2->(F2_SERIE+F2_DOC))

	While !EOF() .and. SF2->F2_SERIE == SE1->E1_PREFIXO .and. SF2->F2_DOC == SE1->E1_NUM

		If SF2->(F2_SERIE+F2_DOC) == SE1->(E1_PREFIXO+E1_NUM)
			nxTaxa := Posicione("SE4",1,xFilial("SE4")+SF2->F2_COND,"E4_XTXCC")
			IF nxTaxa == 0
				_XVLTXCC  := 0 //Round((SE1->E1_VALOR*nxTaxa)/100,2)
				_XVLRLIQ  := 0 //SE1->E1_VALOR-Round((SE1->E1_VALOR*nxTaxa)/100,2)
			else
				_XVLTXCC  := Round(((SE1->E1_VALOR-SC5->C5_XCREDRK)*nxTaxa)/100,2)
				_XVLRLIQ  := SE1->E1_VALOR-Round(((SE1->E1_VALOR-SC5->C5_XCREDRK)*nxTaxa)/100,2)
			Endif
			RecLock("SE1",.F.)
			SE1->E1_XNSU	:= SC5->C5_XNSU
			SE1->E1_XAUTNSU	:= SC5->C5_XAUTCAR
			SE1->E1_XPEDWEB	:= SC5->C5_XPEDWEB
			SE1->E1_XPEDRAK	:= SC5->C5_XPEDRAK
			SE1->E1_XFORREC	:= SC5->C5_XDESCOD
			SE1->E1_XCARTID	:= SC5->C5_XCARTID
			SE1->E1_DESCFIN := _DESC
			SE1->E1_XTXCC   := nxTaxa
			SE1->E1_DECRESC := _XVLTXCC
			SE1->E1_SDDECRE := _XVLTXCC 
			SE1->E1_VLTXCC  := _XVLTXCC
			SE1->E1_VLRLIQ  := _XVLRLIQ
			SE1->E1_XCODDIV := SA1->A1_XCODDIV
			SE1->E1_XNONDIV := SA1->A1_XNONDIV
			SE1->E1_XCANAL  := SA1->A1_XCANAL
			SE1->E1_XCANALD := SA1->A1_XCANALD
			SE1->E1_XCODREG := SA1->A1_XCODREG
			SE1->E1_XDESREG := SA1->A1_XDESREG
			MSUNLOCK()
		Endif

		DbSkip()

	EndDo

/* // escolheram n�o fazer isso - Eduardo
If alltrim(SC5->C5_XNSU) <> ""
	DbSelectarea("SE1")
	DbSetOrder(1)
	DbGoTop()
	DbSeek(xfilial("SE1")+SF2->(F2_SERIE+F2_DOC))
	_banco := ""

	While !EOF() .and. SF2->F2_SERIE == SE1->E1_PREFIXO .and. SF2->F2_DOC == SE1->E1_NUM

		If SF2->(F2_SERIE+F2_DOC) == SE1->(E1_PREFIXO+E1_NUM)
			aBaixa := {}

			DbSelectArea("SE4")
			DbSetOrder(1)
			DbSeek(xfilial("SE4")+SF2->F2_COND)

			_banco := SE4->E4_BANCO

			IF alltrim(_banco) <> ""
				DbSelectArea("SA6")
				DbSetOrder(1)
				DbSeek(xfilial("SA6")+_banco)

				_desconto := SE1->E1_SALDO * (SE4->E4_XTXCC/100)
				_dtcred   := SE1->E1_VENCTO+SA6->A6_DIASCOB

				aBaixa := {{"E1_PREFIXO"  ,SE1->E1_PREFIXO        ,Nil    },;
				           {"E1_NUM"      ,SE1->E1_NUM            ,Nil    },;
				           {"E1_PARCELA"  ,SE1->E1_PARCELA		  ,Nil	  },;
				           {"E1_TIPO"     ,SE1->E1_TIPO           ,Nil    },;
				           {"AUTMOTBX"    ,"NOR"                  ,Nil    },;
				           {"AUTBANCO"    ,SA6->A6_COD            ,Nil    },;
				           {"AUTAGENCIA"  ,SA6->A6_AGENCIA        ,Nil    },;
				           {"AUTCONTA"    ,SA6->A6_NUMCON         ,Nil    },;
				           {"AUTDTBAIXA"  ,dDataBase              ,Nil    },;
				           {"AUTDTCREDITO",_dtcred                ,Nil    },;
				           {"AUTHIST"     ,"BAIXA CC"             ,Nil    },;
				           {"AUTDESCONT"  ,_desconto              ,Nil    },;
				           {"AUTVALREC"   ,SE1->E1_SALDO          ,Nil    }}

				MSExecAuto({|x,y| Fina070(x,y)},aBaixa,3)
			Endif
		Endif

		DbSelectArea("SE1")
		DbSkip()

	EndDo
Endif
*/

//Grava numero da nota no DAP que gerou a NF
/*DbSelectArea("SD2")
DbSetOrder(3)
If DbSeek(xFilial("SD2")+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA)
	While SD2->(!EOF()) .And. SD2->D2_DOC+SD2->D2_SERIE = SF2->F2_DOC+SF2->F2_SERIE
		DbSelectArea("SZJ")
		DbSetOrder(2)
		If DbSeek(xFilial("SZJ")+SD2->D2_PEDIDO+SD2->D2_ITEMPV)
			While SZJ->(!EOF()) .And. SZJ->ZJ_PEDIDO+SZJ->ZJ_ITEM = SD2->D2_PEDIDO+SD2->D2_ITEMPV
				If Empty(SZJ->ZJ_DOC) .And. SZJ->ZJ_CONF = "S"
					RecLock("SZJ",.F.)
					SZJ->ZJ_DOC   := SD2->D2_DOC
					SZJ->ZJ_SERIE := SD2->D2_SERIE
					MsUnlock()
				EndIf
				SZJ->(DbSkip())
			EndDo
		EndIf
		SD2->(DbSkip())
	EndDo
EndIf
*/

	_qry := "update "+RetSqlName("SZJ")+" set ZJ_DOC = D2_DOC, ZJ_SERIE = D2_SERIE from "+RetSqlName("SZJ")+" SZJ "
	_qry += "inner join "+RetSqlName("SD2")+" SD2 on SD2.D_E_L_E_T_ = '' and ZJ_PEDIDO = D2_PEDIDO and ZJ_ITEM = D2_ITEMPV "
	_qry += "where SZJ.D_E_L_E_T_ = '' and D2_DOC = '"+SF2->F2_DOC+"' and D2_SERIE = '"+SF2->F2_SERIE+"' and ZJ_CONF = 'S' and ZJ_DOC = '' "
	TcSqlExec(_qry)

//Chama a rotina para atualiizar os dados da expedi��o da NF.
//Alterado para o Ponto de Entrada M460FIM 
//	U_HFT003EXP(.T.)

	DbSelectArea("SD2")
	DbSetOrder(3)
	aRegSC5 := {}
	If DbSeek(xFilial("SD2")+SF2->F2_DOC+SF2->F2_SERIE+SF2->F2_CLIENTE+SF2->F2_LOJA)
		While SD2->(!EOF()) .And. SD2->D2_DOC+SD2->D2_SERIE = SF2->F2_DOC+SF2->F2_SERIE
			nPos := aScan( aRegSC5, {|x| alltrim(x) == alltrim(SD2->D2_PEDIDO)})
			IF nPos = 0
				aAdd(aRegSC5, SD2->D2_PEDIDO)
			Endif

			DbSelectArea("SD2")
			DbSkip()
		End
	Endif

	For _i := 1 to len(aRegSC5)
		u_ElimRes(aRegSC5[_i])
	Next

	RestArea(aArea)

Return NIL

User Function ElimRes(_ped)

	nVlrDep := 0
	Begin Transaction
			// Elimina residuo de itens e atualiza cabecalho do pedido.
		_qry := "Select sum((C6_QTDVEN-C6_QTDENT)*C6_PRUNIT) as SALDO from "+RetSqlName("SC6")+" SC6 where SC6.D_E_L_E_T_ = '' and C6_BLQ <> 'R' "
		_qry += "and C6_NUM = '"+_ped+"' "

		If Select("TMPSC6") > 0
			TMPSC6->(DbCloseArea())
		EndIf

		cQuery := ChangeQuery(_qry)
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC6",.T.,.T.)

		IF TMPSC6->SALDO < 300
			DbSelectArea("SC6")
			DbSetOrder(1)
			DbSeek(xfilial("SC6")+_ped)

			While !EOF() .and. SC6->C6_NUM = _ped
				If (SC6->C6_QTDVEN - SC6->C6_QTDENT) > 0 .and. SC6->C6_BLQ <> 'R'
					MaResDoFat(nil, .T., .F., @nVlrDep)
				Endif

				DbSelectArea("SC6")
				DbSkip()
			End
			DbSelectArea("SC5")
			DbSetOrder(1)
			DbSeek(xfilial("SC5")+_ped)
			MaLiberOk({ SC5->C5_NUM }, .T.)
		Endif

		DbSelectarea("TMPSC6")
		DbCloseArea()

	End Transaction
Return