// #########################################################################################
// Projeto: Hope
// Modulo : Faturamento
// Fonte  : hFATP001
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 09/10/16 | Actual Trend      | Cadastro de Tipo de Faturamento 
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

user function HFATP001
	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	
	local cAlias
	
	cAlias := "SZ1"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	axCadastro(cAlias, "Cadastro de Tipo de Faturamento", cVldExc, cVldAlt)
	
return
