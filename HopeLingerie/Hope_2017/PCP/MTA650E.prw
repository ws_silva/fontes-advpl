#Include 'Protheus.ch'

User Function MTA650E()

lRet := .T.

If SC2->C2_XRES == "L" .or. SC2->C2_XRES == "T"
	
	_op := SC2->C2_NUM + SC2->C2_ITEM + SC2->C2_SEQUEN + SC2->C2_ITEMGRD
	
	If SC2->C2_XRES == "T"
		If !MsgYesNo("Esta OP j� foi Transferida. A Tranfer�ncia j� foi estornada?","OP j� transferida")
			lRet := .F.
		Endif 
	Endif

	If lRet
		DbSelectArea("SD4")
		DbSetOrder(2)
		DbSeek(xfilial("SD4")+_op)
		_ok := .T.
		
		While !EOF() .and. SubStr(SD4->D4_OP,1,14) == SubStr(_op,1,14)
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+SD4->D4_COD)
			
			If !(SB1->B1_TIPO $ ("MI|PI")) .and. SubStr(SD4->D4_COD,1,1) <> "B"
				DbSelectArea("SB2")
				DbSetOrder(1)
				DbSeek(xfilial("SB2")+SD4->D4_COD+SB1->B1_LOCPAD)
				
				If Found()
					RecLock("SB2",.F.)
						Replace B2_XRES	with B2_XRES - SD4->D4_XRES
					MsUnLock()			
					
					RecLock("SD4",.F.)
						Replace D4_XRES with 0
					MsUnLock()
				Else
					_ok := .F.
				Endif
			Endif
		
			DbSelectArea("SD4")
			DbSkip()
		End
		
		If _ok
			_qry := "update "+RetSqlName("SC2")+" set C2_XRES = '' where D_E_L_E_T_ = '' and C2_NUM = '"+SubStr(_op,1,6)+"' and C2_ITEM = '"+SubStr(_op,7,2)+"' and C2_SEQUEN = '"+SubStr(_op,9,3)+"' and C2_ITEMGRD = '"+SubStr(_op,12,3)+"' "
			TcSqlExec(_qry)
			lRet := .T.
		Else
			lRet := .F.
			Alert("Ocorreram erros no estorno da Libera��o!")
		Endif
	Endif		
Endif
 
Return(lRet)
