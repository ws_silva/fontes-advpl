
#include "xmlxfun.ch"        
#include "fileio.ch"
     
User Function HPCPP004
        Local aPergs    := {}
        Local lConv      := .F.
        
        Private cArq       := ""
		Private cArqMacro  := "XLS2DBF.XLA"
		Private cTemp      := GetTempPath() //pega caminho do temp do client
		Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
        Private aResps     := {}    
        Private aArquivos  := {}    
 
        aAdd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.F.,""})            
		aAdd(aPergs,{1,"Documento: ",Space(TamSX3("C4_DOC")[1]),"",'.T.',"",'.T.',20,.F.})   
		aAdd(aPergs,{2,"Tipo",Space(12), {"Previs�o", "Plano Mestre"}, 50,'.T.',.T.})  
		aAdd(aPergs,{1,"Ano: ",Space(4),"",'.T.',"",'.T.',20,.F.})   
		
        If ParamBox(aPergs,"Parametros", @aResps)

	      	aAdd(aArquivos, AllTrim(aResps[1]))
			cArq := AllTrim(aResps[1]) 
//			MsAguarde( {|| ConOut("Come�ou convers�o do arquivo "+cArq+ " - "+_time),;
//            			   lConv := convArqs(aArquivos) }, "Convertendo arquivos", "Convertendo arquivos" )
//			If lConv
        		Processa({|| ProcMovs()})
//        	EndIf 
        Endif           
Return
                         
Static Function ProcMovs()
        Local cError    	 := ""
        Local cWarning      := ""
        Local nFile         := 0
        Local aLinhas   	 := {}                          
        Local cFile     	 := AllTrim(aResps[1])
        Local nI            := 0
		Local cLinha  := ""
		Local nLintit    := 1 
		Local nLin 		 := 0
		Local nTotLin := 0
		Local aDados  := {}
		Local nHandle := 0

 		DbSelectArea("SC4")
 		DbSetOrder(1)
 		DbSeek(xfilial("SC4")+aResps[2])
 		  
 		If Found()
			MsgAlert("Documento ja existe! Importacao cancelada", "Atencao!")
			Return
 		Endif
   
    	nFile := fOpen(cFile, FO_READ + FO_DENYWRITE)
   
        If nFile == -1
        	If !Empty(cFile)
            	MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
            EndIf
            Return
        EndIf          

		  nHandle := Ft_Fuse(cFile)
		  Ft_FGoTop()                                                         
		  nLinTot := FT_FLastRec()-1
		  ProcRegua(nLinTot)

		  While nLinTit > 0 .AND. !Ft_FEof()
			   Ft_FSkip()
			   nLinTit--
		  EndDo

		  Do While !Ft_FEof()
			   IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
			   nLin++
			   cLinha := Ft_FReadLn()
			   If Empty(AllTrim(StrTran(cLinha,',','')))
			      Ft_FSkip()
			      Loop
			   EndIf
			   cLinha := StrTran(cLinha,'"',"'")
			   cLinha := '{"'+cLinha+'"}'
			   cLinha := StrTran(cLinha,';',',') 
			   cLinha := StrTran(cLinha,',','","')
			   aAdd(aDados, &cLinha)
   
			   FT_FSkip()
		  EndDo

		  FT_FUse()             
       	_time := Time()
        Begin Transaction    
  		  For nI := 1 To Len(aDados)
  		  		DbSelectArea("SB1")
  		  		DbSetOrder(1)
  		  		DbSeek(xfilial("SB1")+alltrim(aDados[nI,1]))
  		  		If Found()
  		  			If aResps[3] = "Previs�o"
	  		  			IF val(aDados[nI,11]) > 0
			  		  		RecLock("SC4",.T.)
  					  			Replace C4_FILIAL		with xfilial("SC4")
  					  			Replace C4_DOC			with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1]
  		  						Replace C4_LOCAL		with SB1->B1_LOCPAD
  		  						Replace C4_QUANT		with val(aDados[nI,11])
  		  						Replace C4_DATA			with ctod("01/01/"+aResps[4])
  		  						Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
			  		  		MsUnLock()
			  		  	Endif

						If val(aDados[nI,12]) > 0
	  				  		RecLock("SC4",.T.)
  					  			Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1] 
  		  						Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  		  					Replace C4_QUANT			with val(aDados[nI,12])
		  		  				Replace C4_DATA			with ctod("01/02/"+aResps[4])
  		  						Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,13]) > 0
	  			  			RecLock("SC4",.T.)
  			  					Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
	  		  					Replace C4_PRODUTO		with aDados[nI,1]
  			  					Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  			  				Replace C4_QUANT			with val(aDados[nI,13])
  				  				Replace C4_DATA			with ctod("01/03/"+aResps[4])
  		  						Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,14]) > 0
		  			  		RecLock("SC4",.T.)
  				  				Replace C4_FILIAL			with xfilial("SC4")
  				  				Replace C4_DOC				with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1]
  		  						Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  		  					Replace C4_QUANT			with val(aDados[nI,14])
  			  					Replace C4_DATA			with ctod("01/04/"+aResps[4])
  		  						Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
		  			  		MsUnLock()
		  				Endif

						If val(aDados[nI,15]) > 0
	  				  		RecLock("SC4",.T.)
  			  					Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1]
  		  						Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
		  		  				Replace C4_QUANT			with val(aDados[nI,15])
  				  				Replace C4_DATA			with ctod("01/05/"+aResps[4])
  		  						Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,16]) > 0
	  			  			RecLock("SC4",.T.)
  			  					Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
	  		  					Replace C4_PRODUTO		with aDados[nI,1]
  			  					Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  			  				Replace C4_QUANT			with val(aDados[nI,16])
  				  				Replace C4_DATA			with ctod("01/06/"+aResps[4])
  		  						Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif
	  				
						If val(aDados[nI,17]) > 0
		  			  		RecLock("SC4",.T.)
  				  				Replace C4_FILIAL			with xfilial("SC4")
  				  				Replace C4_DOC				with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1]
  		  						Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  		  					Replace C4_QUANT			with val(aDados[nI,17])
  			  					Replace C4_DATA			with ctod("01/07/"+aResps[4])
  			  					Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
		  			  		MsUnLock()
		  				Endif

						If val(aDados[nI,18]) > 0
	  				  		RecLock("SC4",.T.)
  			  					Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1]
  		  						Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
		  		  				Replace C4_QUANT			with val(aDados[nI,18])
  				  				Replace C4_DATA			with ctod("01/08/"+aResps[4])
  				  				Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,19]) > 0
	  			  			RecLock("SC4",.T.)
  			  					Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
	  		  					Replace C4_PRODUTO		with aDados[nI,1]
  			  					Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  			  				Replace C4_QUANT			with val(aDados[nI,19])
  				  				Replace C4_DATA			with ctod("01/09/"+aResps[4])
  				  				Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,20]) > 0
		  			  		RecLock("SC4",.T.)
  				  				Replace C4_FILIAL			with xfilial("SC4")
  				  				Replace C4_DOC				with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1]
  		  						Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  		  					Replace C4_QUANT			with val(aDados[nI,20])
  			  					Replace C4_DATA			with ctod("01/10/"+aResps[4])
  			  					Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
		  			  		MsUnLock()
		  				Endif

						If val(aDados[nI,21]) > 0
	  				  		RecLock("SC4",.T.)
  			  					Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
  		  						Replace C4_PRODUTO		with aDados[nI,1]
  		  						Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
		  		  				Replace C4_QUANT			with val(aDados[nI,21])
  				  				Replace C4_DATA			with ctod("01/11/"+aResps[4])
  				  				Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,22]) > 0
	  			  			RecLock("SC4",.T.)
  			  					Replace C4_FILIAL			with xfilial("SC4")
  			  					Replace C4_DOC				with aResps[2]
	  		  					Replace C4_PRODUTO		with aDados[nI,1]
  			  					Replace C4_LOCAL			with SB1->B1_LOCPAD  		  					
	  			  				Replace C4_QUANT			with val(aDados[nI,22])
  				  				Replace C4_DATA			with ctod("01/12/"+aResps[4])
  				  				Replace C4_OBS			with "IMPORTSOP "+ dtoc(ddatabase) + _time
	  				  		MsUnLock()
	  					Endif
					Else
	  		  			IF val(aDados[nI,11]) > 0
			  		  		RecLock("SHC",.T.)
  					  			Replace HC_FILIAL			with xfilial("SHC")
  					  			Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1]
  		  						Replace HC_QUANT			with val(aDados[nI,11])
  		  						Replace HC_DATA			with ctod("01/01/"+aResps[4])
			  		  		MsUnLock()
			  		  	Endif

						If val(aDados[nI,12]) > 0
	  				  		RecLock("SHC",.T.)
  					  			Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1] 
	  		  					Replace HC_QUANT			with val(aDados[nI,12])
		  		  				Replace HC_DATA			with ctod("01/02/"+aResps[4])
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,13]) > 0
	  			  			RecLock("SHC",.T.)
  			  					Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
	  		  					Replace HC_PRODUTO		with aDados[nI,1]
	  			  				Replace HC_QUANT			with val(aDados[nI,13])
  				  				Replace HC_DATA			with ctod("01/03/"+aResps[4])
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,14]) > 0
		  			  		RecLock("SHC",.T.)
  				  				Replace HC_FILIAL			with xfilial("SHC")
  				  				Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1]
	  		  					Replace HC_QUANT			with val(aDados[nI,14])
  			  					Replace HC_DATA			with ctod("01/04/"+aResps[4])
		  			  		MsUnLock()
		  				Endif

						If val(aDados[nI,15]) > 0
	  				  		RecLock("SHC",.T.)
  			  					Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1]
		  		  				Replace HC_QUANT			with val(aDados[nI,15])
  				  				Replace HC_DATA			with ctod("01/05/"+aResps[4])
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,16]) > 0
	  			  			RecLock("SHC",.T.)
  			  					Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
	  		  					Replace HC_PRODUTO		with aDados[nI,1]
	  			  				Replace HC_QUANT			with val(aDados[nI,16])
  				  				Replace HC_DATA			with ctod("01/06/"+aResps[4])
	  				  		MsUnLock()
	  					Endif
	  				
						If val(aDados[nI,17]) > 0
		  			  		RecLock("SHC",.T.)
  				  				Replace HC_FILIAL			with xfilial("SHC")
  				  				Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1]
	  		  					Replace HC_QUANT			with val(aDados[nI,17])
  			  					Replace HC_DATA			with ctod("01/07/"+aResps[4])
		  			  		MsUnLock()
		  				Endif

						If val(aDados[nI,18]) > 0
	  				  		RecLock("SHC",.T.)
  			  					Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1]
		  		  				Replace HC_QUANT			with val(aDados[nI,18])
  				  				Replace HC_DATA			with ctod("01/08/"+aResps[4])
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,19]) > 0
	  			  			RecLock("SHC",.T.)
  			  					Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
	  		  					Replace HC_PRODUTO		with aDados[nI,1]
	  			  				Replace HC_QUANT			with val(aDados[nI,19])
  				  				Replace HC_DATA			with ctod("01/09/"+aResps[4])
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,20]) > 0
		  			  		RecLock("SHC",.T.)
  				  				Replace HC_FILIAL			with xfilial("SHC")
  				  				Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1]
	  		  					Replace HC_QUANT			with val(aDados[nI,20])
  			  					Replace HC_DATA			with ctod("01/10/"+aResps[4])
		  			  		MsUnLock()
		  				Endif

						If val(aDados[nI,21]) > 0
	  				  		RecLock("SHC",.T.)
  			  					Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
  		  						Replace HC_PRODUTO		with aDados[nI,1]
		  		  				Replace HC_QUANT			with val(aDados[nI,21])
  				  				Replace HC_DATA			with ctod("01/11/"+aResps[4])
	  				  		MsUnLock()
	  					Endif

						If val(aDados[nI,22]) > 0
	  			  			RecLock("SHC",.T.)
  			  					Replace HC_FILIAL			with xfilial("SHC")
  			  					Replace HC_DOC				with aResps[2]
	  		  					Replace HC_PRODUTO		with aDados[nI,1]
	  			  				Replace HC_QUANT			with val(aDados[nI,22])
  				  				Replace HC_DATA			with ctod("01/12/"+aResps[4])
	  				  		MsUnLock()
	  					Endif
					Endif				
				Endif
	     Next nI          
        End Transaction    
Return