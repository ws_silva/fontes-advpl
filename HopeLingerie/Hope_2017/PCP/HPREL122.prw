#INCLUDE 'TOTVS.CH'
#INCLUDE 'RWMAKE.CH'
#Include "Topconn.ch"
#Include 'Protheus.ch'
#INCLUDE "AP5MAIL.CH"
#INCLUDE 'FWMVCDEF.CH'

User Function HPREL122()
	
	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Data de"			,dDataBase,PesqPict("SC1", "C1_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) // Tipo data
	aAdd(aPerg,{1,"Data At�" 		,dDataBase,PesqPict("SC1", "C1_EMISSAO"),'.T.'	,""		,'.T.'	,50	,.F.}) // Tipo data
	aAdd(aPerg,{1,"Referencia" 		,Space(15),""							,""		,"SB1"	,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Armazem"			,Space(15),""							,""		,""	,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Tipo de Produto"	,Space(2),""							,""		,"02"	,""		,0	,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Grupo" 	     	,Space(4),""							,""		,"SBM"		,""		,0	,.F.}) // Tipo caractere

	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| GeraTrf(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
	
Return


STATIC fUNCTION GeraTrf(aRet)

	oExcel := FWMsExcelEx():New()
	cQuery := ""
	i := 0
		
	IF Select("TRF") > 0
		TRF->(dbCloseArea())
	Endif
      
    cQuery0 := "SELECT 'SD3' ORIGEM, '' FORNECEDOR, '' PEDIDO, SB1.B1_GRUPO CODGRP, '' VALORTT, SD3.D3_EMISSAO AS DATA_MOVIMENTO ,SD3.D3_DOC AS DOCUMENTO, SD3.D3_FILIAL AS FILIAL, SB4.B4_TIPO AS TIPO, SD3.D3_COD AS SKU, LEFT(SD3.D3_COD,8) AS REFERENCIA, SB4.B4_DESC AS DESCRICAO, SB1.B1_UM AS UNID_MEDIDA, SUBSTRING(SD3.D3_COD,9,3) AS COD_COR, SBV.BV_DESCRI AS DESC_COR, RIGHT(SD3.D3_COD,4) AS TAM, SD3.D3_QUANT AS QUANTIDADE, SBM.BM_DESC AS GRUPO, SD3.D3_LOCAL AS ARMAZEN, SD3.D3_LOCALIZ AS ENDERECO, SD3.D3_USUARIO AS USUARIO, SD3.D3_OBS AS OBS, SD3.D3_CF CF FROM SD3010 SD3 (NOLOCK) INNER JOIN SBV010 SBV (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SD3.D3_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' INNER JOIN SB4010 SB4 (NOLOCK) ON SB4.B4_COD=LEFT(SD3.D3_COD,8) AND SB4.D_E_L_E_T_='' INNER JOIN SB1010 SB1 (NOLOCK) ON SB1.B1_COD = SD3.D3_COD AND SB1.D_E_L_E_T_ = '' INNER JOIN SBM010 SBM (NOLOCK) ON SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_ = '' WHERE SD3.D_E_L_E_T_=''"
      
	if !Empty(aRet[1])
		cQuery0 += " AND SD3.D3_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"'  " 
	endif
	
	if !Empty(aRet[3]) 
		cQuery0 += " AND LEFT(SB1.B1_COD,8) = LEFT('"+Alltrim(aRet[3])+"',8)"
	endif	
	
	if !Empty(aRet[4]) 
		//cQuery0 += " AND SD3.D3_LOCAL = '"+Alltrim(aRet[4])+"' "
		
		cQuery0 += " AND SD3.D3_LOCAL IN ('"+ Replace(alltrim(aRet[4]),"/","','") +"')  " 
		
	endif
	
	if !Empty(aRet[5])
		cQuery0 += " AND SB1.B1_TIPO = '"+ Alltrim(aRet[5])+ "' "
	endif
	
	if !Empty(aRet[6])
		cQuery0 += " AND SB1.B1_GRUPO = '"+ Alltrim(aRet[6])+ "' "
	endif


    cQuery1 := "SELECT 'SD2' ORIGEM, '' FORNECEDOR ,'' PEDIDO, SB1.B1_GRUPO CODGRP, '' VALORTT,SD2.D2_EMISSAO AS DATA_MOVIMENTO, SD2.D2_DOC AS DOCUMENTO, SD2.D2_FILIAL AS FILIAL, SB4.B4_TIPO AS TIPO, SD2.D2_COD AS SKU, LEFT(SD2.D2_COD,8) AS REFERENCIA, SB4.B4_DESC AS DESCRICAO, SB1.B1_UM AS UNID_MEDIDA, SUBSTRING(SD2.D2_COD,9,3) AS COD_COR, SBV.BV_DESCRI AS DESC_COR, RIGHT(SD2.D2_COD,4) AS TAM, SD2.D2_QUANT AS QUANTIDADE, SBM.BM_DESC AS GRUPO, SD2.D2_LOCAL AS ARMAZEN, SD2.D2_LOCALIZ AS ENDERECO, '' AS USUARIO, '' AS OBS, SX5.X5_DESCRI CF FROM SD2010 SD2 (NOLOCK) INNER JOIN SBV010 SBV (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SD2.D2_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' INNER JOIN SB4010 SB4 (NOLOCK) ON SB4.B4_COD=LEFT(SD2.D2_COD,8) AND SB4.D_E_L_E_T_='' INNER JOIN SB1010 SB1 (NOLOCK) ON SB1.B1_COD = SD2.D2_COD AND SB1.D_E_L_E_T_ = '' INNER JOIN SBM010 SBM (NOLOCK) ON SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_ = '' INNER JOIN SX5010 SX5 (NOLOCK) ON SX5.X5_TABELA='13' AND X5_CHAVE=SD2.D2_CF AND SX5.D_E_L_E_T_='' WHERE SD2.D_E_L_E_T_=''"
      
	if !Empty(aRet[1])
		cQuery1 += " AND SD2.D2_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"'  " 
	endif
	
	if !Empty(aRet[3]) 
		cQuery1 += " AND LEFT(SB1.B1_COD,8) = LEFT('"+Alltrim(aRet[3])+"',8)"
	endif		
	
	if !Empty(aRet[4]) 
		//cQuery1 += " AND SD2.D2_LOCAL = '"+Alltrim(aRet[4])+"' "
		cQuery1 += " AND SD2.D2_LOCAL IN ('"+ Replace(alltrim(aRet[4]),"/","','") +"')  " 
		
	endif
	
	if !Empty(aRet[5])
		cQuery1 += " AND SB1.B1_TIPO = '"+ Alltrim(aRet[5])+ "' "
	endif
	
	if !Empty(aRet[6])
		cQuery1 += " AND SB1.B1_GRUPO = '"+ Alltrim(aRet[6])+ "' "
	endif


   cQuery3 := "SELECT 'SD1' ORIGEM, SA2.A2_NOME FORNECEDOR , D1_PEDIDO, SB1.B1_GRUPO CODGRP, SD1.D1_TOTAL VALORTT, SD1.D1_DTDIGIT AS DATA_MOVIMENTO, SD1.D1_DOC AS DOCUMENTO, SD1.D1_FILIAL AS FILIAL, SB4.B4_TIPO AS TIPO, SD1.D1_COD AS SKU, LEFT(SD1.D1_COD,8) AS REFERENCIA, SB4.B4_DESC AS DESCRICAO, SB1.B1_UM AS UNID_MEDIDA, SUBSTRING(SD1.D1_COD,9,3) AS COD_COR, SBV.BV_DESCRI AS DESC_COR, RIGHT(SD1.D1_COD,4) AS TAM, SD1.D1_QUANT AS QUANTIDADE, SBM.BM_DESC AS GRUPO, SD1.D1_LOCAL AS ARMAZEN, '' AS ENDERECO, '' AS USUARIO, '' AS OBS, SX5.X5_DESCRI CF FROM SD1010 SD1 (NOLOCK) INNER JOIN SBV010 SBV (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SD1.D1_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' INNER JOIN SB4010 SB4 (NOLOCK) ON SB4.B4_COD=LEFT(SD1.D1_COD,8) AND SB4.D_E_L_E_T_='' INNER JOIN SB1010 SB1 (NOLOCK) ON SB1.B1_COD = SD1.D1_COD AND SB1.D_E_L_E_T_ = '' INNER JOIN SBM010 SBM (NOLOCK) ON SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_ = '' INNER JOIN SX5010 SX5 (NOLOCK) ON SX5.X5_TABELA='13' AND X5_CHAVE=SD1.D1_CF AND SX5.D_E_L_E_T_='' INNER JOIN SA2010 SA2 (NOLOCK) ON SA2.A2_COD = SD1.D1_FORNECE  AND SA2.A2_LOJA = SD1.D1_LOJA AND SA2.D_E_L_E_T_='' WHERE SD1.D_E_L_E_T_=''"
      
	if !Empty(aRet[1])
		cQuery3 += " AND SD1.D1_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"'  " 
	endif
	
	if !Empty(aRet[3]) 
		cQuery3 += " AND LEFT(SB1.B1_COD,8) = LEFT('"+Alltrim(aRet[3])+"',8)"
	endif		
	
	if !Empty(aRet[4]) 
		//cQuery3 += " AND SD1.D1_LOCAL = '"+Alltrim(aRet[4])+"' "
		cQuery3 += " AND SD1.D1_LOCAL IN ('"+ Replace(alltrim(aRet[4]),"/","','") +"')  " 
	endif
	
	if !Empty(aRet[5])
		cQuery3 += " AND SB1.B1_TIPO = '"+ Alltrim(aRet[5])+ "' "
	endif
	
	if !Empty(aRet[6])
		cQuery3 += " AND SB1.B1_GRUPO = '"+ Alltrim(aRet[6])+ "' "
	endif

	cQuery := "SELECT * FROM (" + cQuery0 +" UNION ALL " + cQuery1 +" UNION ALL " + cQuery3 + ") T ORDER BY SKU, DATA_MOVIMENTO"

	memowrite("c:\temp\transferencia_armazem.sql", cQuery)


	TCQUERY cQuery NEW ALIAS TRF
	
	cRel := "Transfer�ncias"
	cRelTit := "Transfer�ncias entre armazens"
	
	oExcel:AddworkSheet(cRel)
	oExcel:AddTable(cRel,cRelTit)
	oExcel:AddColumn(cRel,cRelTit,"FILIAL",1)
	oExcel:AddColumn(cRel,cRelTit,"TIPO",1)
	oExcel:AddColumn(cRel,cRelTit,"COD_GRUPO",1)
	oExcel:AddColumn(cRel,cRelTit,"GRUPO",1)
	oExcel:AddColumn(cRel,cRelTit,"DATA_MOVIMENTO",2)
	oExcel:AddColumn(cRel,cRelTit,"DOCUMENTO",1)
	oExcel:AddColumn(cRel,cRelTit,"PEDIDO",1)	
	oExcel:AddColumn(cRel,cRelTit,"FORNECEDOR",1)
	oExcel:AddColumn(cRel,cRelTit,"SKU",1)
	oExcel:AddColumn(cRel,cRelTit,"REFERENCIA",1)
	oExcel:AddColumn(cRel,cRelTit,"COD_COR",1)
	oExcel:AddColumn(cRel,cRelTit,"DESC_COR",1)
	oExcel:AddColumn(cRel,cRelTit,"TAM",1)
	oExcel:AddColumn(cRel,cRelTit,"DESCRICAO",1)
	oExcel:AddColumn(cRel,cRelTit,"UNID_MEDIDA",1)
	oExcel:AddColumn(cRel,cRelTit,"QUANTIDADE",3)
	oExcel:AddColumn(cRel,cRelTit,"VALOR_TOTAL",3)
	oExcel:AddColumn(cRel,cRelTit,"ARMAZEN",1)
	oExcel:AddColumn(cRel,cRelTit,"ENDERECO",1)
	oExcel:AddColumn(cRel,cRelTit,"USUARIO",1)
	oExcel:AddColumn(cRel,cRelTit,"MOVIMENTO",1)
	oExcel:AddColumn(cRel,cRelTit,"STATUS",1)	
	oExcel:AddColumn(cRel,cRelTit,"OBS",1)	
	
	While TRF->(!EOF())		
		i++
		incproc()
		oExcel:AddRow(cRel,cRelTit,;
					{ TRF->FILIAL,;
					  TRF->TIPO,;
					  TRF->CODGRP,;
					  TRF->GRUPO,;
					  DTOC(STOD(TRF->DATA_MOVIMENTO)),;
					  STRZERO(VAL(TRF->DOCUMENTO),9),;
					  STRZERO(VAL(TRF->PEDIDO),6),;
					  TRF->FORNECEDOR,;
					  TRF->SKU,;
					  TRF->REFERENCIA,;
					  TRF->COD_COR,;
					  TRF->DESC_COR,;
					  TRF->TAM,;
					  TRF->DESCRICAO,;
					  TRF->UNID_MEDIDA,;
					  Transform(TRF->QUANTIDADE,"@E 9,999,999.9999"),;
					  Transform(TRF->VALORTT,"@E 9,999,999.9999"),;
					  TRF->ARMAZEN,;
					  TRF->ENDERECO,;
					  TRF->USUARIO,; 
					  IIF(TRF->ORIGEM == "SD3",'INTERNA - '+UPPER(SD3CF(TRF->CF)),TRF->CF),;
					  UPPER(__Status(TRF->ORIGEM)),;
					  TRF->OBS})
		TRF->(DBSKIP())
		
	End Do
	
	cFileName = "c:\temp\HPREL122_"+DTOS(DDATABASE)+"_"+REPLACE(TIME(),":","")+".xml"
	oExcel:Activate()
	oExcel:GetXMLFile(cFileName)
	
	oExcelApp := MsExcel():New()
	oExcelApp:WorkBooks:Open(cFileName) // Abre uma planilha
	oExcelApp:SetVisible(.T.)
	oExcelApp:Destroy()		
		
RETURN

Static Function SD3CF(cf)

		cf := alltrim(cf)
		
		DO CASE	
			CASE cf == "RE0"  
				cfRet = "Requisi��o Manual de Material Aprop. Direta"
			CASE cf == "RE1"  
				cfRet = "Requisi��o Automatica de Material Aprop. Direta"
			CASE cf == "RE2"  
				cfRet = "Requisi��o Automatica de Material Aprop. Indireta"
			CASE cf == "RE3"  
				cfRet = "Requisi��o Manual de Material Aprop. Indireta"
			CASE cf == "RE4"  
				cfRet = "Saida - Transfer�ncia em Geral"
			CASE cf == "RE5"  
				cfRet = "Requisi��o informando OP na nota fiscal de entrada."
			CASE cf == "RE6"  
				cfRet = "Requisi��o Manual de Material Valorizada"
			CASE cf == "RE7"  
				cfRet = "Saida - Desmontagem de Produto" 
			CASE cf == "DE0"  
				cfRet = "Devolu��o Manual de Material Aprop. Direta"
			CASE cf == "DE1"  
				cfRet = "Devolu��o Automatica de Material Aprop. Direta"
			CASE cf == "DE2"  
				cfRet = "Devolu��o Automatica de Material Aprop. Indireta"
			CASE cf == "DE3"  
				cfRet = "Devolu��o Manual de Material Aprop. Indireta"
			CASE cf == "DE4"  
				cfRet = "Entrada - Transfer�ncia em Geral"
			CASE cf == "DE5"  
				cfRet = "Devolu��o informando OP na nota fiscal"
			CASE cf == "DE6"  
				cfRet = "Devolu��o Manual de Material Valorizada."
			CASE cf == "DE7"  
				cfRet = "Entrada - Desmontagem de Produto" 
			
			CASE cf == "PR0"  
				cfRet = "Produ��o manual."
			CASE cf == "PR1"  
				cfRet = "Produ��o autom�tica."
			
			CASE cf == "ER0"  
				cfRet = "Estorno de produ��o manual." 
			CASE cf == "ER1"  
				cfRet = "Estorno de autom�tica."
		ENDCASE

Return cfRet

Static Function __Status(_cOri)

		_cOri := alltrim(_cOri)
		
		DO CASE	
			CASE _cOri == "SD1"  
				cStat = "Entrada"
			CASE _cOri == "SD2"  
				cStat = "Sa�da"
			CASE _cOri == "SD3"
				cStat = "Mov. Interna"
		ENDCASE

Return cStat