#INCLUDE 'TOTVS.CH'
#INCLUDE 'RWMAKE.CH'
#Include "Topconn.ch"
#Include 'Protheus.ch'
#INCLUDE "AP5MAIL.CH"
#INCLUDE 'FWMVCDEF.CH'


// Relat�rio de saldos por armazem
User Function HPREL018()

	Local aRet := {}
	Local aParamBox := {}

	Private cCadastro := "Saldo por Armazem"

	aAdd(aParamBox,{1,"Armazem",Space(15),"","","",""   ,0,.F.}) // Tipo caractere
	aAdd(aParamBox,{1,"Filial" ,Space(6) ,"","","SM0","",0,.F.}) // Tipo caractere

	If ParamBox(aParamBox,"Par�metros...",@aRet) 

	 	Processa({|| GeraArq(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
	
Return


sTATIC fUNCTION GeraArq(aRet)

	Local oExcel := FWMsExcelEx():New()
	Local cQuery := ""
		Local i := 0
		
		IF Select("SLD") > 0
			SLD->(dbCloseArea())
		Endif
	      
	    cQuery := " SELECT SB2.B2_FILIAL AS FILIAL, " 
		cQuery += " SB2.B2_COD AS SKU, "
		cQuery += " SB1.B1_DESC AS DESCRICAO, "
		cQuery += " LEFT(B2_COD,8) AS REFERENCIA, "
		cQuery += " B1_TIPO AS TIPO, "
		cQuery += " BM_GRUPO AS COD_GRUPO, "
		cQuery += " BM_DESC AS GRUPO, "
		cQuery += " B2_LOCAL AS ARMAZEN, "
		cQuery += " CAST(B2_QATU AS NUMERIC(15,4)) AS SALDO, "
		cQuery += " CAST(B2_RESERVA + B2_XRES AS NUMERIC(15,4)) AS RESERVA, "
		cQuery += " CAST(B2_QEMP AS NUMERIC(15,4)) AS EMPENHO, "
		cQuery += " CAST(B2_QEMPN AS NUMERIC(15,4)) AS EMPENHO_NF, "
		cQuery += " LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AS COD_COR,"
	    cQuery += " ISNULL(SBV.BV_DESCRI,'#NAO INFORMADA#')  AS DESC_COR," 
	    cQuery += " RIGHT(SB1.B1_COD,4)  AS TAM, "
	    cQuery += " B1_UM AS UNID_MED,"
		cQuery += " CAST(B2_QPEDVEN AS NUMERIC(15,4)) AS QTDE_PED_VENDA, "
		cQuery += " CAST(B2_SALPEDI AS NUMERIC(15,4)) AS QTDE_PREVISTA_ENTRAR, "
		cQuery += " CAST(B2_QNPT AS NUMERIC(15,4)) AS QTDE_PODER_TERCEIROS, "
		cQuery += " CAST(B2_QACLASS AS NUMERIC(15,4)) AS SALDO_A_ENDERECAR, "
		cQuery += " CAST(B2_QEMPPRE AS NUMERIC(15,4)) AS EMPENHO_PREVISTO, "
		cQuery += " CAST(B2_SALPPRE AS NUMERIC(15,4)) AS SALDO_PREVISTO_OP, "
		//SUM(SB2.B2_QATU)-SUM(SB2.B2_RESERVA)-SUM(SB2.B2_XRESERV)
		cQuery += " CAST(B2_QATU - (B2_RESERVA + B2_XRES) AS NUMERIC(15,4)) AS TOTAL " // 11/01/2018 - Solicitado pela Camila do PCP para abater do total as revervas 
		cQuery += " FROM "+RetSqlName('SB2')+" SB2 (NOLOCK) "
		cQuery += " JOIN "+RetSqlName('SB1')+" SB1 (NOLOCK) ON SB1.B1_COD=SB2.B2_COD AND SB1.D_E_L_E_T_='' "
		cQuery += " JOIN "+RetSqlName('SBM')+" SBM (NOLOCK) ON SBM.BM_GRUPO=B1_GRUPO AND SBM.D_E_L_E_T_='' "
		cQuery += " LEFT JOIN "+RetSqlName('SBV')+" SBV (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
		cQuery += " WHERE SB2.D_E_L_E_T_='' " //"AND B2_QATU > 0 " // 11/01/2018 - Solicitado pela Camila do PCP para passar a considerar os itens negativos
		cQuery += " AND SB1.B1_TIPO NOT IN ('MO','SV') " 
		
		if !Empty(aRet[1])
			cQuery += " AND B2_LOCAL IN ('"+ Replace(alltrim(aRet[1]),"/","','") +"')  " 
		else
			cQuery += " "
		endif
		
		if !Empty(aRet[2]) 
			cQuery += " AND B2_FILIAL = '"+Alltrim(aRet[2])+"' "
		else
			cQuery += " "
		endif
		
		TCQUERY cQuery NEW ALIAS SLD
			
		cRel := "Saldo"
		cRelTit := "Saldo em Estoque"
		oExcel:AddworkSheet(cRel)
		oExcel:AddTable(cRel,cRelTit)
		oExcel:AddColumn(cRel,cRelTit,"FILIAL",1)
		oExcel:AddColumn(cRel,cRelTit,"ARMAZEN",2)
		oExcel:AddColumn(cRel,cRelTit,"TIPO",3)
		oExcel:AddColumn(cRel,cRelTit,"COD_GRUPO",1)
		oExcel:AddColumn(cRel,cRelTit,"GRUPO",1)
		oExcel:AddColumn(cRel,cRelTit,"SKU",1)
		oExcel:AddColumn(cRel,cRelTit,"REFERENCIA",1)
		oExcel:AddColumn(cRel,cRelTit,"COD_COR",1)
		oExcel:AddColumn(cRel,cRelTit,"DESC_COR",1)
		oExcel:AddColumn(cRel,cRelTit,"TAM",1)
		oExcel:AddColumn(cRel,cRelTit,"DESCRICAO",1)
		oExcel:AddColumn(cRel,cRelTit,"UNID_MED",1)
		oExcel:AddColumn(cRel,cRelTit,"SALDO",1)
		oExcel:AddColumn(cRel,cRelTit,"EMPENHO",1)
		//oExcel:AddColumn(cRel,cRelTit,"TOTAL",1)
		oExcel:AddColumn(cRel,cRelTit,"SALDO_RESERVA_A_ENDERE�AR",1) // 11/01/2018 - Solicitado pela Camila do PCP para alterar a nomenclatura da coluna para evitar mal entendido nos processos de confer�ncia entre membros da equipe
		oExcel:AddColumn(cRel,cRelTit,"QTDE_PREVISTA_ENTRAR",1)
		oExcel:AddColumn(cRel,cRelTit,"QTDE_PED_VENDA",1)
		oExcel:AddColumn(cRel,cRelTit,"QTDE_PODER_TERCEIROS",1)
		oExcel:AddColumn(cRel,cRelTit,"EMPENHO_PREVISTO",1)
		oExcel:AddColumn(cRel,cRelTit,"EMPENHO_NF",1)
		oExcel:AddColumn(cRel,cRelTit,"RESERVA",1)
		oExcel:AddColumn(cRel,cRelTit,"SALDO_PREVISTO_OP",1)
		oExcel:AddColumn(cRel,cRelTit,"SALDO_A_ENDERECAR",1)	
		
		
		While SLD->(!EOF())
			
			i++
			incproc()
			oExcel:AddRow(cRel,cRelTit,{;
				SLD->FILIAL,;
				SLD->ARMAZEN,;
				SLD->TIPO,;
				SLD->COD_GRUPO,;
				SLD->GRUPO,;
				SLD->SKU,;
				SLD->REFERENCIA,;
				SLD->COD_COR,;
				SLD->DESC_COR,;
				SLD->TAM,;
				SLD->DESCRICAO,;
				SLD->UNID_MED,;
				Transform(SLD->SALDO,"@E 9,999,999.9999"),;
				Transform(SLD->EMPENHO,"@E 9,999,999.9999"),;
				Transform(SLD->TOTAL,"@E 9,999,999.9999"),;
				Transform(SLD->QTDE_PREVISTA_ENTRAR,"@E 9,999,999.9999"),;
				Transform(SLD->QTDE_PED_VENDA,"@E 9,999,999.9999"),;
				Transform(SLD->QTDE_PODER_TERCEIROS,"@E 9,999,999.9999"),;
				Transform(SLD->EMPENHO_PREVISTO,"@E 9,999,999.9999"),;
				Transform(SLD->EMPENHO_NF,"@E 9,999,999.9999"),;
				Transform(SLD->RESERVA,"@E 9,999,999.9999"),;
				Transform(SLD->SALDO_PREVISTO_OP,"@E 9,999,999.9999"),;
				Transform(SLD->SALDO_A_ENDERECAR,"@E 9,999,999.9999")})
			SLD->(DBSKIP()) 
			
		End Do
		
		
		cFileName = "c:\temp\HPREL018_"+DTOS(DDATABASE)+"_"+REPLACE(TIME(),":","")+".xml"
		oExcel:Activate()
		
		oExcel:GetXMLFile(cFileName)
		
		oExcelApp := MsExcel():New()
		oExcelApp:WorkBooks:Open(cFileName) // Abre uma planilha
		oExcelApp:SetVisible(.T.)
		oExcelApp:Destroy()		
		
		

RETURN

