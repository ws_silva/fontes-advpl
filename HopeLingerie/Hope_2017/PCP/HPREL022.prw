#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL019 
Saldo por Armazem
@author Weskley Silva
@since 14/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL022()
	
Private oReport
Private cPergCont	:= 'HPREL022' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 14 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'TRF', 'TRANSFERENCIA ENTRE ARMAZEM', , {|oReport| ReportPrint( oReport ), 'TRANSFERENCIA ENTRE ARMAZEM' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'TRANSFERENCIA ENTRE ARMAZEM', { 'TRF', 'SD3','SBV','SB4'})
			
	TRCell():New( oSection1, 'FILIAL'				,'TRF', 		'FILIAL',			  			    "@!"				        ,35)
	TRCell():New( oSection1, 'TIPO'					,'TRF', 		'TIPO',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'GRUPO'				,'TRF', 		'GRUPO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'DATA_MOVIMENTO'		,'TRF', 		'DATA_MOVIMENTO',				    "@!"                        ,10)
	TRCell():New( oSection1, 'DOCUMENTO'     		,'TRF', 		'DOCUMENTO',		  			    "@!"				        ,15)
	TRCell():New( oSection1, 'SKU'				    ,'TRF', 		'SKU',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'REFERENCIA'			,'TRF', 		'REFERENCIA',		  			    "@!"				        ,25)
	TRCell():New( oSection1, 'COD_COR'				,'TRF', 		'COD_COR',			  			    "@!"				        ,06)
	TRCell():New( oSection1, 'DESC_COR'				,'TRF', 		'DESC_COR',			  			    "@!"				        ,06)
	TRCell():New( oSection1, 'TAM'					,'TRF', 		'TAM',			     			    "@!"				        ,06)
	TRCell():New( oSection1, 'DESCRICAO'			,'TRF', 		'DESCRICAO',		  			    "@!"				        ,06)
	TRCell():New( oSection1, 'UNID_MEDIDA'			,'TRF', 		'UNID_MEDIDA',		  			    "@!"				        ,06)
	TRCell():New( oSection1, 'QUANTIDADE'			,'TRF', 		'QUANTIDADE',				  	    "@E 999.999,99"		        ,06)
	TRCell():New( oSection1, 'ARMAZEN_ORIGEM'		,'TRF', 		'ARMAZEN_ORIGEM',				    "@!"				        ,06)
	TRCell():New( oSection1, 'ENDERECO_ORIGEM' 		,'TRF', 		'ENDERECO_ORIGEM',					"@!"				        ,06)
	TRCell():New( oSection1, 'ARMAZEN_DESTINO' 		,'TRF', 		'ARMAZEN_DESTINO',					"@!"				        ,06)
	TRCell():New( oSection1, 'ENDERECO_DESTINO'		,'TRF', 		'ENDERECO_DESTINO',					"@!"				        ,06)
	TRCell():New( oSection1, 'USUARIO' 				,'TRF', 		'USUARIO',							"@!"				        ,06)
	TRCell():New( oSection1, 'OBS' 					,'TRF', 		'OBS',								"@!"				        ,06)


Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 14 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("TRF") > 0
		TRF->(dbCloseArea())
	Endif
      
    cQuery := " SELECT " 
    cQuery += " RIGHT(SD3.D3_EMISSAO,2)+'/'+SUBSTRING(SD3.D3_EMISSAO,5,2)+'/'+LEFT(SD3.D3_EMISSAO,4) AS DATA_MOVIMENTO, "
    cQuery += " SD30.D3_DOC AS DOCUMENTO, "
    cQuery += " SD3.D3_FILIAL AS FILIAL, " 
    cQuery += " SB4.B4_TIPO AS TIPO, "
    cQuery += " SD3.D3_COD AS SKU, "
    cQuery += " LEFT(SD3.D3_COD,8) AS REFERENCIA, " 
    cQuery += " SB4.B4_DESC AS DESCRICAO, "
    cQuery += " SB4.B4_UM AS UNID_MEDIDA, "
    cQuery += " SUBSTRING(SD3.D3_COD,9,3) AS COD_COR, " 
    cQuery += " SBV.BV_DESCRI AS DESC_COR, "
    cQuery += "	RIGHT(SD3.D3_COD,4)  AS TAM, " 
    cQuery += " SD3.D3_QUANT AS QUANTIDADE, " 
    cQuery += " SBM.BM_DESC AS GRUPO, "
    cQuery += " SD3.D3_LOCAL AS ARMAZEN_ORIGEM, "
    cQuery += " SD3.D3_LOCALIZ AS ENDERECO_ORIGEM , " 
    cQuery += " SD30.D3_LOCAL AS ARMAZEN_DESTINO , "
    cQuery += " SD30.D3_LOCALIZ AS ENDERECO_DESTINO, "
    cQuery += " SD3.D3_USUARIO AS USUARIO, "
    cQuery += " SD3.D3_OBS AS OBS "
    cQuery += " FROM "+RetSqlName('SD3')+" SD3 "
    cQuery += " INNER JOIN "+RetSqlName('SD3')+" SD30 ON SD30.D3_COD=SD3.D3_COD AND SD30.D3_DOC=SD3.D3_DOC AND SD30.D3_NUMSEQ=SD3.D3_NUMSEQ AND SD30.D_E_L_E_T_=''  "
    cQuery += " INNER JOIN "+RetSqlName('SBV')+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SD3.D3_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
    cQuery += " INNER JOIN "+RetSqlName('SB4')+" SB4 ON SB4.B4_COD=LEFT(SD3.D3_COD,8) AND SB4.D_E_L_E_T_='' "
    cQuery += " INNER JOIN "+RetSqlName('SB1')+" SB1 ON SB1.B1_COD = SD30.D3_COD AND SB1.D_E_L_E_T_ = ''  "
    cQuery += " INNER JOIN "+RetSqlName('SBM')+" SBM ON SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_ = '' "
    cQuery += " WHERE SD3.D_E_L_E_T_='' AND SD3.D3_CF IN ('RE4','RE6') AND SD30.D3_CF IN ('DE4','DE6') " 
    
      
	if !Empty(mv_par01)
		cQuery += " AND SD3.D3_EMISSAO BETWEEN '"+ DTOS(mv_par01) +"'  AND '"+ DTOS(mv_par02) +"'  " 
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par03) 
		cQuery += " AND SD3.D3_LOCAL = '"+Alltrim(mv_par03)+"' "
	else
		cQuery += " "
	endif
	
	if !Empty(mv_par04)
		cQuery += " AND SD30.D3_LOCAL = '"+Alltrim(mv_par04)+"' "
	else 
		cQuery += " "
	endif
	
	if !Empty(mv_par05)
		cQuery += " AND SB4.B4_TIPO = '"+ Alltrim(mv_par05)+ "' "
	else 
		cQuery += " "
	endif
	
	if !Empty(mv_par06)
		cQuery += " AND SD30.D3_DOC = '"+ Alltrim(mv_par06)+ "' "
	else 
		cQuery += " "
	endif
	
	TCQUERY cQuery NEW ALIAS TRF

	While TRF->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		
		oSection1:Cell("FILIAL"):SetValue(TRF->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(TRF->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(TRF->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_MOVIMENTO"):SetValue(TRF->DATA_MOVIMENTO)
		oSection1:Cell("DATA_MOVIMENTO"):SetAlign("LEFT")

		oSection1:Cell("DOCUMENTO"):SetValue(TRF->DOCUMENTO)
		oSection1:Cell("DOCUMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(TRF->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
				
		oSection1:Cell("REFERENCIA"):SetValue(TRF->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(TRF->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(TRF->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(TRF->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
			
		oSection1:Cell("DESCRICAO"):SetValue(TRF->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("UNID_MEDIDA"):SetValue(TRF->UNID_MEDIDA)
		oSection1:Cell("UNID_MEDIDA"):SetAlign("LEFT")
		
		oSection1:Cell("QUANTIDADE"):SetValue(TRF->QUANTIDADE)
		oSection1:Cell("QUANTIDADE"):SetAlign("LEFT")
		
		oSection1:Cell("ARMAZEN_ORIGEM"):SetValue(TRF->ARMAZEN_ORIGEM)
		oSection1:Cell("ARMAZEN_ORIGEM"):SetAlign("LEFT")
		
		oSection1:Cell("ENDERECO_ORIGEM"):SetValue(TRF->ENDERECO_ORIGEM)
		oSection1:Cell("ENDERECO_ORIGEM"):SetAlign("LEFT")
		
		oSection1:Cell("ARMAZEN_DESTINO"):SetValue(TRF->ARMAZEN_DESTINO)
		oSection1:Cell("ARMAZEN_DESTINO"):SetAlign("LEFT")
		
		oSection1:Cell("ENDERECO_DESTINO"):SetValue(TRF->ENDERECO_DESTINO)
		oSection1:Cell("ENDERECO_DESTINO"):SetAlign("LEFT")
		
		oSection1:Cell("USUARIO"):SetValue(TRF->USUARIO)
		oSection1:Cell("USUARIO"):SetAlign("LEFT")
		
		oSection1:Cell("OBS"):SetValue(TRF->OBS)
		oSection1:Cell("OBS"):SetAlign("LEFT")
		
		oSection1:PrintLine()
		
		TRF->(DBSKIP()) 
	enddo
	TRF->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 14 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Data de ?"          	 ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data At� ? "       		 ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Armazem Origem ? "	     ,""		,""	    ,"mv_ch3","C",02,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Armazem Destino ? "      ,""		,""		,"mv_ch4","C",02,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Tipo de Produto ? "  	 ,""		,""		,"mv_ch5","C",02,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Documento ? "      	 	 ,""		,""		,"mv_ch6","C",09,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
	
Return
