#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL026
Acompanhamento Plano Mestre
@author Weskley Silva
@since 14/12/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/


user function HPREL026()

	Private oReport
	Private cPergCont	:= 'HPREL026' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 14 de Dezembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'ACP', 'ACOMPANHAMENTO PLANO MESTRE', cPergCont, {|oReport| ReportPrint( oReport ), 'ACOMPANHAMENTO PLANO MESTRE' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'ACOMPANHAMENTO PLANO MESTRE ', { 'ACP', 'SHC','SB4','SBV','SC2'})
			
	TRCell():New( oSection1, 'DOC'		    	    ,'ACP', 		'DOC',								"@!"                        ,08)
	TRCell():New( oSection1, 'DATA_PM'     	        ,'ACP', 		'DATA_PM',	    					"@!"				        ,10)
	TRCell():New( oSection1, 'SKU'  	        	,'ACP', 		'SKU',	              				"@!"						,25)
	TRCell():New( oSection1, 'TIPO'  	        	,'ACP', 		'TIPO',	              				"@!"						,10)
	TRCell():New( oSection1, 'DATA_LIBERACAO'      	,'ACP', 		'DATA_LIBERACAO',	   				"@!"						,10)
	TRCell():New( oSection1, 'DESCRICAO'         	,'ACP', 		'DESCRICAO',	              		"@!"						,35)
	TRCell():New( oSection1, 'CICLO_PRODUCAO'		,'ACP', 		'CICLO_PRODUCAO' ,     				"@!"		                ,25)
	TRCell():New( oSection1, 'COLECAO'	 			,'ACP', 		'COLECAO',			       			"@!"		                ,25)
	TRCell():New( oSection1, 'SUBCOLECAO' 			,'ACP', 		'SUBCOLECAO',		       			"@!"		                ,25)
	TRCell():New( oSection1, 'COD_COLECAO' 			,'ACP', 		'COD_COLECAO' ,    					"@!"		                ,10)
	TRCell():New( oSection1, 'REFERENCIA'	   		,'ACP', 		'REFERENCIA' ,        				"@!"		                ,20)
	TRCell():New( oSection1, 'COD_COR'			    ,'ACP', 		'COD_COR',			   				"@!"						,10)
	TRCell():New( oSection1, 'DESC_COR'				,'ACP', 		'DESC_COR' ,	    				"@!"	                    ,20)
	TRCell():New( oSection1, 'TAM'					,'ACP', 		'TAM' ,			  				  	"@!"		                ,10)
	TRCell():New( oSection1, 'QUANT_PM'				,'ACP', 		'QUANT_PM',					    	"@E 999.99"             	,15)
	TRCell():New( oSection1, 'QTDE_FINALIZADA'		,'ACP', 		'QTDE_FINALIZADA',	    			"@E 999.99"	            	,15)
	TRCell():New( oSection1, 'QTD_PREVISTA'		    ,'ACP', 		'QTD_PREVISTA',		   				"@E 999.99"     			,15)
	TRCell():New( oSection1, 'QTD_PROCESSO'			,'ACP', 		'QTD_PROCESSO',		    			"@E 999.99"	            	,15)
	TRCell():New( oSection1, 'A_PROGRAMAR'			,'ACP', 		'A_PROGRAMAR',	    				"@E 999.99"	            	,15)
	TRCell():New( oSection1, 'A_PRODUZIR'			,'ACP', 		'A_PRODUZIR',	    				"@E 999.99"	            	,15)									

Return( oReport )



//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 14 de Dezembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("ACP") > 0
		ACP->(dbCloseArea())
	Endif
     
     cQuery := " SELECT T.DOC, " 
	 cQuery += " T.DATA_PM, " 
	 cQuery += " T.SKU, " 
	 cQuery += " T.TIPO, "
	 cQuery += " T.DATA_LIBERACAO, "
	 cQuery += " T.DESCRICAO, "
	 cQuery += " T.CICLO_PRODUCAO, "
	 cQuery += " T.COLECAO, "
	 cQuery += " T.B4_YCOLECA AS COD_COLECAO,T.SUBCOLECAO,  "
	 cQuery += " T.REFERENCIA, "
	 cQuery += " T.COD_COR, "
	 cQuery += " T.DESC_COR, "
	 cQuery += " T.TAM, "
	 cQuery += " ISNULL(SUM(T.QUANT_PM),0) AS QUANT_PM , "
	 cQuery += " ISNULL(SUM(T.QTDE_FINALIZADA),0) AS QTDE_FINALIZADA, "
	 cQuery += " ISNULL(SUM(T.QTD_PREVISTA),0) AS QTD_PREVISTAS , "
	 cQuery += " ISNULL(SUM(T.QTD_PROCESSO),0) AS QTD_PROCESSO, "
	 cQuery += " ISNULL(SUM(T.QUANT_PM - (T.QTDE_FINALIZADA + T.QTD_PROCESSO)),0) AS A_PROGRAMAR, "
	 cQuery += " ISNULL(SUM((T.QUANT_PM - T.QTDE_FINALIZADA)),0) AS A_PRODUZIR "

	 cQuery += " FROM ( "
 
	 cQuery += " SELECT " 
	 cQuery += " SH.HC_DOC AS DOC, " 
	 cQuery += " RIGHT(HC_DATA,2)+'/'+SUBSTRING(HC_DATA,5,2)+'/'+LEFT(HC_DATA,4) AS DATA_PM , " 
	 cQuery += " SH.HC_PRODUTO AS SKU, "
	 cQuery += " B1.B1_TIPO AS TIPO,  "
	 cQuery += " RIGHT(B1.B1_YDATLIB,2)+'/'+SUBSTRING(B1.B1_YDATLIB,5,2)+'/'+LEFT(B1.B1_YDATLIB,4) AS DATA_LIBERACAO, " 
	 cQuery += " B4.B4_DESC AS DESCRICAO, "
	 cQuery += " B4.B4_YNCICLO AS CICLO_PRODUCAO, "
	 cQuery += " B4.B4_YNCOLEC AS COLECAO, "
	 cQuery += " B4.B4_YCOLECA, "
	 cQuery += " B1.B1_YDESSCO AS SUBCOLECAO,"
	 cQuery += " B4.B4_COD AS REFERENCIA, " 
	 cQuery += " SUBSTRING(HC_PRODUTO,9,3) AS COD_COR, " 
	 cQuery += " SBV.BV_DESCRI AS DESC_COR, "
	 cQuery += " RIGHT(HC_PRODUTO,4)  AS TAM, "
	 cQuery += " SH.HC_QUANT AS QUANT_PM,  "
	 cQuery += " ISNULL(SUM(SC.C2_QUJE),0) AS QTDE_FINALIZADA, "
	 cQuery += " '' AS QTD_PREVISTA, "
	 cQuery += " ISNULL(SUM(SC.C2_QUANT- SC.C2_QUJE- SC.C2_PERDA),0)  AS QTD_PROCESSO "
	 cQuery += " FROM "+RetSqlName("SHC")+" SH "
	 cQuery += " JOIN "+RetSqlName("SB4")+" B4 ON B4.B4_COD=LEFT(HC_PRODUTO,8) "
	 cQuery += " JOIN "+RetSqlName("SB1")+" B1 ON B1.B1_COD=HC_PRODUTO " 
	 cQuery += " JOIN "+RetSqlName("SBV")+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(HC_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
	 cQuery += " LEFT JOIN "+RetSqlName("SC2")+" SC ON SC.C2_PRODUTO=SH.HC_PRODUTO  AND SC.D_E_L_E_T_=''  AND SC.C2_XRES <> '' "
	 cQuery += " WHERE " 
	 cQuery += " SH.D_E_L_E_T_=''  "
	 cQuery += " AND B4.D_E_L_E_T_='' " 
	 cQuery += " AND B1.D_E_L_E_T_='' "
 
	 cQuery += " GROUP BY HC_DOC, "
     cQuery += " SH.HC_DATA,  "
     cQuery += " SH.HC_PRODUTO, " 
     cQuery += " B4.B4_DESC, "
     cQuery += " B4.B4_YNCICLO, "
     cQuery += " B4.B4_YNCOLEC, "
     cQuery += " B4.B4_YCOLECA,B1.B1_YDESSCO, "
     cQuery += " B4.B4_COD, "
     cQuery += " SBV.BV_DESCRI, "
     cQuery += " SH.HC_QUANT, "
     cQuery += " B1.B1_YDATLIB, " 
     cQuery += " B1.B1_TIPO "
 

     cQuery += " UNION " 

     cQuery += " SELECT  "
     cQuery += " SH.HC_DOC AS DOC, " 
     cQuery += " RIGHT(HC_DATA,2)+'/'+SUBSTRING(HC_DATA,5,2)+'/'+LEFT(HC_DATA,4) AS DATA_PM , " 
     cQuery += " SH.HC_PRODUTO AS SKU, "
     cQuery += " B1.B1_TIPO AS TIPO, "
     cQuery += " RIGHT(B1.B1_YDATLIB,2)+'/'+SUBSTRING(B1.B1_YDATLIB,5,2)+'/'+LEFT(B1.B1_YDATLIB,4) AS DATA_LIBERACAO, "
     cQuery += " B4.B4_DESC AS DESCRICAO, "
     cQuery += " B4.B4_YNCICLO AS CICLO_PRODUCAO, "
     cQuery += " B4.B4_YNCOLEC AS COLECAO, "
     cQuery += " B4.B4_YCOLECA, "
     cQuery += " B1.B1_YDESSCO AS SUBCOLECAO, "
     cQuery += " B4.B4_COD AS REFERENCIA, " 
     cQuery += " SUBSTRING(HC_PRODUTO,9,3) AS COD_COR, " 
     cQuery += " SBV.BV_DESCRI AS DESC_COR,  "
     cQuery += " RIGHT(HC_PRODUTO,4)  AS TAM, "
     cQuery += " '' AS QUANT_PM,  " 
     cQuery += " '' AS QTDE_FINALIZADA, "
     cQuery += " ISNULL(SUM(SC.C2_QUANT- SC.C2_QUJE- SC.C2_PERDA),0) AS QTD_PREVISTA, "
     cQuery += " '' AS QTD_FIRMES "
     cQuery += " FROM "+RetSqlName("SHC")+" SH "
     cQuery += " JOIN "+RetSqlName("SB4")+" B4 ON B4.B4_COD=LEFT(HC_PRODUTO,8) "
     cQuery += " JOIN "+RetSqlName("SB1")+" B1 ON B1.B1_COD=HC_PRODUTO "
     cQuery += " JOIN "+RetSqlName("SBV")+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(HC_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
     cQuery += " LEFT JOIN "+RetSqlName("SC2")+" SC ON SC.C2_PRODUTO = HC_PRODUTO  AND SC.D_E_L_E_T_='' AND SC.C2_TPOP = 'P' AND SC.C2_XRES = '' "
     cQuery += " WHERE "
     cQuery += " SH.D_E_L_E_T_='' " 
     cQuery += " AND B4.D_E_L_E_T_='' " 
     cQuery += " AND B1.D_E_L_E_T_='' "
 
     cQuery += " GROUP BY HC_DOC, "
     cQuery += " SH.HC_DATA,  "
     cQuery += " SH.HC_PRODUTO, " 
     cQuery += " B4.B4_DESC, "
     cQuery += " B4.B4_YNCICLO, "
     cQuery += " B4.B4_YNCOLEC, "
     cQuery += " B4.B4_YCOLECA,B1.B1_YDESSCO, "
     cQuery += " B4.B4_COD, "
     cQuery += " SBV.BV_DESCRI, "
     cQuery += " SH.HC_QUANT, "
     cQuery += " B1.B1_YDATLIB, " 
     cQuery += " B1.B1_TIPO  "
 
     cQuery += " ) T "

     
     cQuery += " WHERE T.DOC BETWEEN '" + Alltrim(mv_par01) + "'  AND  '" + Alltrim(mv_par02) + "' "

     cQuery += " GROUP BY T.DOC,T.DATA_PM,T.SKU,T.TIPO,T.DATA_LIBERACAO,T.DESCRICAO,T.CICLO_PRODUCAO,T.COLECAO,T.SUBCOLECAO,T.B4_YCOLECA,T.REFERENCIA,T.COD_COR, "
     cQuery += " T.DESC_COR,T.TAM  " 

	 TCQUERY cQuery NEW ALIAS ACP

	While ACP->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("DOC"):SetValue(ACP->DOC)
		oSection1:Cell("DOC"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_PM"):SetValue(ACP->DATA_PM)
		oSection1:Cell("DATA_PM"):SetAlign("LEFT")
			
		oSection1:Cell("SKU"):SetValue(ACP->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(ACP->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_LIBERACAO"):SetValue(ACP->DATA_LIBERACAO)
		oSection1:Cell("DATA_LIBERACAO"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(ACP->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO_PRODUCAO"):SetValue(ACP->CICLO_PRODUCAO)
		oSection1:Cell("CICLO_PRODUCAO"):SetAlign("LEFT")
		
		oSection1:Cell("COLECAO"):SetValue(ACP->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBCOLECAO"):SetValue(ACP->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
	    oSection1:Cell("COD_COLECAO"):SetValue(ACP->COD_COLECAO)
		oSection1:Cell("COD_COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(ACP->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(ACP->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(ACP->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(ACP->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
					
		oSection1:Cell("QUANT_PM"):SetValue(ACP->QUANT_PM)
		oSection1:Cell("QUANT_PM"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_FINALIZADA"):SetValue(ACP->QTDE_FINALIZADA)
		oSection1:Cell("QTDE_FINALIZADA"):SetAlign("LEFT")
	
		oSection1:Cell("QTD_PREVISTA"):SetValue(ACP->QTD_PREVISTA)
		oSection1:Cell("QTD_PREVISTA"):SetAlign("LEFT")
	
		oSection1:Cell("QTD_PROCESSO"):SetValue(ACP->QTD_PROCESSO)
		oSection1:Cell("QTD_PROCESSO"):SetAlign("LEFT")
		
		oSection1:Cell("A_PROGRAMAR"):SetValue(ACP->A_PROGRAMAR)
		oSection1:Cell("A_PROGRAMAR"):SetAlign("LEFT")
		
		oSection1:Cell("A_PRODUZIR"):SetValue(ACP->A_PRODUZIR)
		oSection1:Cell("A_PRODUZIR"):SetAlign("LEFT")	
			
		oSection1:PrintLine()
		
		ACP->(DBSKIP()) 
	enddo
	ACP->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 14 de Dezembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Documento de ? "		        ,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Documento at� ?"			    ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
return