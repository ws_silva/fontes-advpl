#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL010 
Finalizado por Oficina
@author Weskley Silva
@since 08/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL014()

Private oReport
Private cPergCont	:= 'HPREL014' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 08 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'OFC', 'FINALIZADO POR OFICINA', , {|oReport| ReportPrint( oReport ), 'FINALIZADO POR OFICINA' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'FINALIZADO POR OFICINA', { 'OFC', 'SZR','SZJ','SA1','SA2','SC5','SZ1'})
			
	TRCell():New( oSection1, 'FILIAL'			    ,'OFC', 		'FILIAL',							"@!"                        ,10)
	TRCell():New( oSection1, 'ARMAZEN'			    ,'OFC', 		'ARMAZEN',		  					"@!"				        ,04)
	TRCell():New( oSection1, 'OP'		 	        ,'OFC', 		'OP',		          				"@!"						,10)
	TRCell():New( oSection1, 'SKU' 			    	,'OFC', 		'SKU',				       			"@!"						,15)
	TRCell():New( oSection1, 'DESCRICAO'			,'OFC', 		'DESCRICAO',	       				"@!"		                ,35)
	TRCell():New( oSection1, 'CICLO' 				,'OFC', 		'CICLO',			 				"@!"				        ,20)
	TRCell():New( oSection1, 'COLECAO'  			,'OFC', 		'COLECAO',		    			    "@!"				        ,35)
	TRCell():New( oSection1, 'SUBCOLECAO' 			,'OFC', 	    'SUBCOLECAO',                  		"@!"		    	        ,35)
	TRCell():New( oSection1, 'GRUPO'	   			,'OFC', 		'GRUPO',      	   					"@!"						,20)
	TRCell():New( oSection1, 'TIPO'	   			    ,'OFC', 		'TIPO',      	   					"@!"						,20)
	TRCell():New( oSection1, 'PRODUTO'			    ,'OFC', 		'PRODUTO',			   				"@!"						,15)
	TRCell():New( oSection1, 'COD_COR'				,'OFC', 		'COD_COR',				   			"@!"						,10)
	TRCell():New( oSection1, 'DESC_COR'				,'OFC', 		'DESC_COR',			   				"@!"						,20)
	TRCell():New( oSection1, 'TAM'					,'OFC', 		'TAM',								"@!"						,06)
	TRCell():New( oSection1, 'COD_FORN'				,'OFC', 		'COD_FORN',		   					"@!"						,10)
	TRCell():New( oSection1, 'NOME'					,'OFC', 		'NOME',			   					"@!"						,40)
	TRCell():New( oSection1, 'CELULA'				,'OFC', 		'CELULA',		   					"@!"						,20)
	TRCell():New( oSection1, 'DATA_APONTAMENTO'		,'OFC', 		'DATA_APONTAMENTO',					"@!"						,10)
	TRCell():New( oSection1, 'QTD_PROD'				,'OFC', 		'QTD_PROD',		   					"@E 999,99"					,16)
	TRCell():New( oSection1, 'QTD_PERDA'			,'OFC', 		'QTD_PERDA',	   					"@E 999,99"					,06)

	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 08 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("OFC") > 0
		OFC->(dbCloseArea())
	Endif
      
    cQuery := " SELECT " 
    cQuery += " SC2.C2_FILIAL AS FILIAL, "  
    cQuery += " SH6.H6_LOCAL AS ARMAZEN, "
    cQuery += " SC2.C2_NUM AS OP, "
    cQuery += " SC2.C2_PRODUTO AS SKU, " 
    cQuery += " SB4.B4_DESC AS DESCRICAO, " 
    cQuery += " SB4.B4_YNCICLO AS CICLO, "
    cQuery += " SB4.B4_YNCOLEC AS COLECAO, "
    cQuery += " SB4.B4_YNSUBCO AS SUBCOLECAO, "
    cQuery += " SB4.B4_YNTPCOM AS GRUPO, "
	cQuery += " '' AS QTD_PERDA , "
    cQuery += " SB1.B1_TIPO AS TIPO, "
    cQuery += " LEFT(SC2.C2_PRODUTO,8) AS PRODUTO, "
    cQuery += " SUBSTRING(SC2.C2_PRODUTO,9,3) AS COD_COR, "
    cQuery += " SBV.BV_DESCRI AS DESC_COR, "
    cQuery += " RIGHT(SC2.C2_PRODUTO,4)  AS TAM, "
    cQuery += " ISNULL(SC2.C2_YFORNEC,'-') AS COD_FORN, "
    cQuery += " ISNULL(SA2.A2_NOME,'-') AS NOME, "
    cQuery += " SC2.C2_RECURSO AS CELULA, "
    cQuery += " RIGHT(SH6.H6_DTAPONT,2)+'/'+SUBSTRING(SH6.H6_DTAPONT,5,2)+'/'+LEFT(SH6.H6_DTAPONT,4) AS DATA_APONTAMENTO, "
    cQuery += " SUM(SH6.H6_QTDPROD) AS QTD_PROD, "
    cQuery += " ISNULL((SELECT SUM(H6_QTDPROD) FROM "+RetSqlName('SH6')+" SH6 (NOLOCK) WHERE SC2.C2_NUM+SC2.C2_ITEM+SC2.C2_SEQUEN+SC2.C2_ITEMGRD=SH6.H6_OP AND SC2.C2_PRODUTO=SH6.H6_PRODUTO AND SC2.C2_LOCAL = SH6.H6_LOCAL AND SH6.H6_LOCAL IN ('QL','QB')),0) AS QTD_QUALIDADE "
    cQuery += " FROM "+RetSqlName('SH6')+"  SH6 (NOLOCK) " 
    cQuery += " INNER JOIN "+RetSqlName('SC2')+" SC2 (NOLOCK) ON SC2.C2_NUM+SC2.C2_ITEM+SC2.C2_SEQUEN+SC2.C2_ITEMGRD=SH6.H6_OP AND SC2.C2_PRODUTO=SH6.H6_PRODUTO AND SC2.C2_FILIAL = SH6.H6_FILIAL AND SC2.D_E_L_E_T_ = '' AND SC2.D_E_L_E_T_='' "
    cQuery += " INNER JOIN "+RetSqlName('SB4')+" SB4 (NOLOCK) ON SB4.B4_COD=LEFT(SH6.H6_PRODUTO,8) AND SB4.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName('SBV')+" SBV (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC2.C2_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
    cQuery += " LEFT JOIN  "+RetSqlName('SA2')+" SA2 (NOLOCK) ON SA2.A2_COD=SC2.C2_YFORNEC AND SA2.A2_LOJA = SC2.C2_YLOJAFO AND SA2.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN  "+RetSqlName('SB1')+" SB1 (NOLOCK) ON B1_COD = SH6.H6_PRODUTO AND SB1.D_E_L_E_T_ = '' "
    cQuery += " WHERE SH6.H6_OPERAC IN ('40','70','25') AND SH6.D_E_L_E_T_='' "
   
    IF !EMPTY(mv_par01) .AND. !EMPTY(mv_par02)
    	cQuery += "	AND SH6.H6_DTAPONT BETWEEN '"+ DTOS(mv_par01)+"' AND '"+ DTOS(mv_par02)+"'   "  
	Endif
 
   	cQuery += " GROUP BY "
   	cQuery += " SC2.C2_FILIAL, "
   	cQuery += " SH6.H6_LOCAL, "
   	cQuery += " SC2.C2_NUM, "
   	cQuery += " SC2.C2_PRODUTO,SC2.C2_ITEM,SC2.C2_SEQUEN,SC2.C2_ITEMGRD, "
   	cQuery += " SB4.B4_DESC, "
   	cQuery += " SB4.B4_YNCICLO, "
   	cQuery += " SB4.B4_YNCOLEC, "
   	cQuery += " SB4.B4_YNSUBCO, "
   	cQuery += " SB1.B1_TIPO, "
   	cQuery += " SC2.C2_PRODUTO, SC2.C2_LOCAL, "
   	cQuery += " SBV.BV_DESCRI, "
   	cQuery += " SC2.C2_YFORNEC, "
   	cQuery += " SC2.C2_RECURSO, "
   	cQuery += " SA2.A2_NOME, "
   	cQuery += " SH6.H6_DTAPONT, "
   	cQuery += " SB4.B4_YNTPCOM "
   	 

	cQuery += " UNION "

	cQuery += " SELECT "
    cQuery += " SC2.C2_FILIAL AS FILIAL, "
    cQuery += " SH6.H6_LOCAL AS ARMAZEN, "
    cQuery += " SC2.C2_NUM AS OP, "
    cQuery += " SC2.C2_PRODUTO AS SKU, "
    cQuery += " SB4.B4_DESC AS DESCRICAO, "
    cQuery += " SB4.B4_YNCICLO AS CICLO, "
    cQuery += " SB4.B4_YNCOLEC AS COLECAO, "
    cQuery += " SB4.B4_YNSUBCO AS SUBCOLECAO, "
    cQuery += " SB4.B4_YNTPCOM AS GRUPO, "
    cQuery += " SUM(H6_QTDPERD)  AS QTD_PERDA, "
    cQuery += " SB1.B1_TIPO AS TIPO, "
    cQuery += " LEFT(SC2.C2_PRODUTO,8) AS PRODUTO, "
    cQuery += " SUBSTRING(SC2.C2_PRODUTO,9,3) AS COD_COR, "
    cQuery += " SBV.BV_DESCRI AS DESC_COR, "
    cQuery += " RIGHT(SC2.C2_PRODUTO,4)  AS TAM, "
    cQuery += " ISNULL(SC2.C2_YFORNEC,'-') AS COD_FORN, "
    cQuery += " ISNULL(SA2.A2_NOME,'-') AS NOME, "
    cQuery += " SC2.C2_RECURSO AS CELULA, "
    cQuery += " RIGHT(SH6.H6_DTAPONT,2)+'/'+SUBSTRING(SH6.H6_DTAPONT,5,2)+'/'+LEFT(SH6.H6_DTAPONT,4) AS DATA_APONTAMENTO, "
    cQuery += " SUM(SH6.H6_QTDPROD) AS QTD_PROD, "
    cQuery += " ISNULL((SELECT SUM(H6_QTDPROD) FROM "+RetSqlName('SH6')+" SH6 (NOLOCK) WHERE SC2.C2_NUM+SC2.C2_ITEM+SC2.C2_SEQUEN+SC2.C2_ITEMGRD=SH6.H6_OP AND SC2.C2_PRODUTO=SH6.H6_PRODUTO AND SC2.C2_LOCAL = SH6.H6_LOCAL AND SH6.H6_LOCAL IN ('QL','QB')),0) AS QTD_QUALIDADE " 
    cQuery += "  FROM "+RetSqlName('SH6')+"  SH6 (NOLOCK) "
    cQuery += " INNER JOIN "+RetSqlName('SC2')+" SC2 (NOLOCK) ON SC2.C2_NUM+SC2.C2_ITEM+SC2.C2_SEQUEN+SC2.C2_ITEMGRD=SH6.H6_OP AND SC2.C2_PRODUTO=SH6.H6_PRODUTO AND SC2.C2_FILIAL = SH6.H6_FILIAL AND SC2.D_E_L_E_T_ = '' AND SC2.D_E_L_E_T_='' "
    cQuery += " INNER JOIN "+RetSqlName('SB4')+" SB4 (NOLOCK) ON SB4.B4_COD=LEFT(SH6.H6_PRODUTO,8) AND SB4.D_E_L_E_T_='' "
	cQuery += " INNER JOIN "+RetSqlName('SBV')+" SBV (NOLOCK) ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC2.C2_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_=''  "
    cQuery += " LEFT JOIN  "+RetSqlName('SA2')+" SA2 (NOLOCK) ON SA2.A2_COD=SC2.C2_YFORNEC AND SA2.A2_LOJA = SC2.C2_YLOJAFO AND SA2.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN  "+RetSqlName('SB1')+" SB1 (NOLOCK) ON B1_COD = SH6.H6_PRODUTO AND SB1.D_E_L_E_T_ = '' "
    cQuery += " WHERE SH6.H6_OPERAC IN ('30') AND SH6.D_E_L_E_T_='' AND  SB1.B1_TIPO IN ('PF','PI','KT') "
	cQuery += " AND H6_QTDPERD > 0 "
   
   IF !EMPTY(mv_par01) .AND. !EMPTY(mv_par02)
    cQuery += " AND SH6.H6_DTAPONT BETWEEN '"+ DTOS(mv_par01)+"' AND '"+ DTOS(mv_par02)+"'  "
    Endif
 
   cQuery += " GROUP BY  "
   cQuery += " SC2.C2_FILIAL, "
   cQuery += " SH6.H6_LOCAL,"
   cQuery += " SC2.C2_NUM, "
   cQuery += " SC2.C2_PRODUTO,SC2.C2_ITEM,SC2.C2_SEQUEN,SC2.C2_ITEMGRD, "
   cQuery += " SB4.B4_DESC, "
   cQuery += " SB4.B4_YNCICLO, "
   cQuery += " SB4.B4_YNCOLEC, "
   cQuery += " SB4.B4_YNSUBCO, "
   cQuery += " SB1.B1_TIPO, "
   cQuery += " SC2.C2_PRODUTO, SC2.C2_LOCAL, "
   cQuery += " SBV.BV_DESCRI,  "
   cQuery += " SC2.C2_YFORNEC, " 
   cQuery += " SC2.C2_RECURSO, "
   cQuery += " SA2.A2_NOME, " 
   cQuery += " SH6.H6_DTAPONT, "
   cQuery += " SB4.B4_YNTPCOM  " 

	
	TCQUERY cQuery NEW ALIAS OFC

	While OFC->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(OFC->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")

		oSection1:Cell("ARMAZEN"):SetValue(OFC->ARMAZEN)
		oSection1:Cell("ARMAZEN"):SetAlign("LEFT")
		
		oSection1:Cell("OP"):SetValue(OFC->OP)
		oSection1:Cell("OP"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(OFC->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(OFC->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
			
		oSection1:Cell("CICLO"):SetValue(OFC->CICLO)
		oSection1:Cell("CICLO"):SetAlign("LEFT")
				
		oSection1:Cell("COLECAO"):SetValue(OFC->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
				
		oSection1:Cell("SUBCOLECAO"):SetValue(OFC->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(OFC->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(OFC->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(OFC->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(OFC->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
						
		oSection1:Cell("DESC_COR"):SetValue(OFC->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(OFC->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("COD_FORN"):SetValue(OFC->COD_FORN)
		oSection1:Cell("COD_FORN"):SetAlign("LEFT")
		
		oSection1:Cell("NOME"):SetValue(OFC->NOME)
		oSection1:Cell("NOME"):SetAlign("LEFT")
		
		oSection1:Cell("CELULA"):SetValue(OFC->CELULA)
		oSection1:Cell("CELULA"):SetAlign("LEFT")
		
		oSection1:Cell("DATA_APONTAMENTO"):SetValue(OFC->DATA_APONTAMENTO)
		oSection1:Cell("DATA_APONTAMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_PROD"):SetValue(OFC->QTD_PROD)
		oSection1:Cell("QTD_PROD"):SetAlign("LEFT")
		
		oSection1:Cell("QTD_PERDA"):SetValue(OFC->QTD_PERDA)
		oSection1:Cell("QTD_PERDA"):SetAlign("LEFT")
		
		
											
		oSection1:PrintLine()
		
		OFC->(DBSKIP()) 
	enddo
	OFC->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 08 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Data Apontamento de ?"            ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data Apontamento ate ?"		   	  ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	
Return