#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL024 
Operacoes X ReferenciaPai
@author Weskley Silva
@since 29/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL024()
	
Private oReport
Private cPergCont	:= 'HPREL024' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 29 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'OPR', 'OPERACOES X REFERENCIA PAI', , {|oReport| ReportPrint( oReport ), 'OPERACOES X REFERENCIA PAI' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'OPERACOES X REFERENCIA PAI', { 'OPR', 'SD4','SC2','SB1','SBV'})
			
	TRCell():New( oSection1, 'PRODUTO_PAI'			,'OPR', 		'PRODUTO_PAI',					    "@!"                        ,15)
	TRCell():New( oSection1, 'TIPO_PRODUTO_PAI'		,'OPR', 		'TIPO_PRODUTO_PAI',	  			    "@!"				        ,15)
	TRCell():New( oSection1, 'CICLO'				,'OPR', 		'CICLO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'COLECAO'				,'OPR', 		'COLECAO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'SUBCOLECAO'		    ,'OPR', 		'SUBCOLECAO',		  			    "@!"				        ,15)
	TRCell():New( oSection1, 'PRODUTO_OPERACAO' 	,'OPR', 		'PRODUTO_OPERACAO',	  			    "@!"				        ,25)
	TRCell():New( oSection1, 'OPERACAO'				,'OPR', 		'OPERACAO',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DESC_OPERACAO'		,'OPR', 		'DESC_OPERACAO',	  			    "@!"				        ,35)
	TRCell():New( oSection1, 'LOTE_PADRAO'			,'OPR', 		'LOTE_PADRAO',  	  			    "@!"				        ,06)
	TRCell():New( oSection1, 'TEMPO_PADRAO'			,'OPR', 		'TEMPO_PADRAO',		  			    "@!"				        ,06)
	TRCell():New( oSection1, 'MAO_DE_OBRA'			,'OPR', 		'MAO_DE_OBRA',				  	    "@!"				        ,06)
			
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 29 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("OPR") > 0
		OPR->(dbCloseArea())
	Endif
      
    cQuery := " SELECT " 
    cQuery += " LEFT(VE.CODIGO,8) AS PRODUTO_PAI, "  	
    cQuery += " SB4.B4_TIPO AS TIPO_PRODUTO_PAI, "
    cQuery += " SB4.B4_YNCICLO AS CICLO, "
    cQuery += " SB4.B4_YNCOLEC AS COLECAO, "
    cQuery += " SB4.B4_YNSUBCO AS SUBCOLECAO, "
    cQuery += " SG2.G2_REFGRD AS PRODUTO_OPERACAO, "
    cQuery += " SG2.G2_OPERAC AS OPERACAO, "
    cQuery += " SG2.G2_DESCRI AS DESC_OPERACAO, "
    cQuery += " SG2.G2_LOTEPAD AS LOTE_PADRAO, "
    cQuery += " SG2.G2_TEMPAD AS TEMPO_PADRAO, "
    cQuery += " SG2.G2_MAOOBRA AS MAO_DE_OBRA "
 
    cQuery += " FROM V_ESTRUTURA VE "
    cQuery += " INNER JOIN "+RetSqlName('SG2')+" SG2 ON SG2.G2_REFGRD=LEFT(VE.COD_PAI,8) "
    cQuery += " INNER JOIN "+RetSqlName('SB4')+" SB4 ON SG2.G2_REFGRD=SB4.B4_COD "

    cQuery += " WHERE SG2.D_E_L_E_T_='' AND SB4.D_E_L_E_T_='' "
    
    if !EMPTY(mv_par01)
    	cQuery += " AND LEFT(VE.CODIGO,8) = '"+Alltrim(mv_par01)+"' "
    ENDIF
    
    IF !EMPTY(mv_par02)
    	cQuery += " AND SG2.G2_OPERAC = '"+Alltrim(mv_par02)+"' "
    ENDIF
    
    
    cQuery += " GROUP BY  "
    cQuery += " LEFT(VE.CODIGO,8), "
    cQuery += " SB4.B4_TIPO, "
    cQuery += " SG2.G2_REFGRD, "
    cQuery += " SB4.B4_YNCICLO, "
    cQuery += " SB4.B4_YNCOLEC, "
    cQuery += " SB4.B4_YNSUBCO, "
    cQuery += " SG2.G2_OPERAC, "
    cQuery += " SG2.G2_DESCRI, "
    cQuery += " SG2.G2_LOTEPAD, "
    cQuery += " SG2.G2_TEMPAD, "
    cQuery += " SG2.G2_MAOOBRA " 
    
 	
	TCQUERY cQuery NEW ALIAS OPR

	While OPR->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("PRODUTO_PAI"):SetValue(OPR->PRODUTO_PAI)
		oSection1:Cell("PRODUTO_PAI"):SetAlign("LEFT")

		oSection1:Cell("TIPO_PRODUTO_PAI"):SetValue(OPR->TIPO_PRODUTO_PAI)
		oSection1:Cell("TIPO_PRODUTO_PAI"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO"):SetValue(OPR->CICLO)
		oSection1:Cell("CICLO"):SetAlign("LEFT")
		
		oSection1:Cell("COLECAO"):SetValue(OPR->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("SUBCOLECAO"):SetValue(OPR->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
				
		oSection1:Cell("PRODUTO_OPERACAO"):SetValue(OPR->PRODUTO_OPERACAO)
		oSection1:Cell("PRODUTO_OPERACAO"):SetAlign("LEFT")
		
		oSection1:Cell("OPERACAO"):SetValue(OPR->OPERACAO)
		oSection1:Cell("OPERACAO"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_OPERACAO"):SetValue(OPR->DESC_OPERACAO)
		oSection1:Cell("DESC_OPERACAO"):SetAlign("LEFT")
		
		oSection1:Cell("LOTE_PADRAO"):SetValue(OPR->LOTE_PADRAO)
		oSection1:Cell("LOTE_PADRAO"):SetAlign("LEFT")
		
		oSection1:Cell("TEMPO_PADRAO"):SetValue(OPR->TEMPO_PADRAO)
		oSection1:Cell("TEMPO_PADRAO"):SetAlign("LEFT")
		
		oSection1:Cell("MAO_DE_OBRA"):SetValue(OPR->MAO_DE_OBRA)
		oSection1:Cell("MAO_DE_OBRA"):SetAlign("LEFT")
		
		oSection1:PrintLine()
		
		OPR->(DBSKIP()) 
	enddo
	OPR->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 29 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Referencia ?"           ,""		,""		,"mv_ch1","C",15,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Operacao ? "	        ,""		,""		,"mv_ch2","C",02,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
		
Return
