#include "rwmake.ch"

User Function EMP650()

Local _i

_area := GetArea()
_CLOCAL:= "PADRAO" //ALLTRIM(UPPER(Getmv("MV_HENDPAD")))
_cAmzEmp := GETMV("MV_XLOCPRO")

nPosPrd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'G1_COMP'})
nPosEnd    := aScan(aHeader,{|x| AllTrim(x[2]) == 'DC_LOCALIZ'})
nPosLoc    := aScan(aHeader,{|x| AllTrim(x[2]) == 'D4_LOCAL'})

For _i := 1 to len(aCols)
	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+aCols[_i,nPosPrd])
	
	//If SB1->B1_TIPO <> "MB"
		aCols[_i,nPosLoc] := _cAmzEmp
	//Endif

	If SB1->B1_XEMP = "N"
		aCols[_i,len(aHeader)+1] := .T.
	Endif
Next


RestArea(_area)	
Return()