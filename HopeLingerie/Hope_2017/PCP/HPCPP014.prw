#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include 'totvs.ch'

User Function HPCPP014

	Private aCposBrw
	Private cArqTMP
	
	U_FILTRO()
     
Return


User Function VISOP(_op)

	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_op)

	cErro := ""	
	While !EOF() .and. SC2->C2_NUM = _op
		If alltrim(SC2->C2_SEQPAI) = ""
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(Xfilial("SB1")+SC2->C2_PRODUTO)
			cErro += "Prod: "+SC2->C2_PRODUTO+" - "+alltrim(SB1->B1_DESC)+" - Qtd: "+alltrim(str(SC2->C2_QUANT))+ Chr(13) + Chr(10)
		Endif
		
		DbSelectarea("SC2")
		DbSkip()
	End

	EECView(cErro)

Return

User Function CHKEST(_op)
	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_op)
	
	If Found()
		If alltrim(SC2->C2_XRES) <> ""
			Alert("OP j� Liberada!")
			Return
		Endif
	Endif
	
	_qry := "SELECT * FROM ( " 
	_qry += "SELECT C2_NUM,left(C2_PRODUTO,11) as C2_PRODUTO, D4_COD, B1_DESC, SUM(D4_QUANT) AS EMPENHO, (B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES) AS EMP_RES, B2_QATU, B2_XRES,B2_QACLASS, B2_RESERVA FROM "+RetSqlName("SD4")+" SD4 " 
	_qry += "JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "
	
	_qry += "JOIN "+RetSqlName("SC2")+" SC2 ON SC2.D_E_L_E_T_ = '' AND C2_NUM = LEFT(D4_OP,6) AND C2_ITEM = '01' AND C2_SEQUEN = '001' AND C2_ITEMGRD = SUBSTRING(D4_OP,12,3) "
	_qry += "LEFT JOIN "+RetSqlName("SB2")+" SB2 ON SB2.D_E_L_E_T_ = '' AND B2_COD = D4_COD AND B2_LOCAL = B1_LOCPAD AND B2_FILIAL = '"+xfilial("SB2")+"'  AND B2_LOCAL = 'A1' " 
	_qry += "WHERE SD4.D_E_L_E_T_ = '' AND D4_FILIAL = '"+xFilial("SD4")+"'  "
	_qry += "AND LEFT(D4_OP,6) = '"+_op+"' "
	_qry += "AND D4_COD NOT LIKE 'B%' AND B1_TIPO NOT IN ('MI','PI') " 
	_qry += "AND B1_GRUPO not in ('MP05','MP16','MP17','MP91') "
	_qry += "GROUP BY C2_NUM,left(C2_PRODUTO,11),D4_COD, B1_DESC, B2_QATU, B2_XRES, B2_RESERVA,B2_QACLASS) AS M " 
	_qry += "WHERE EMPENHO > (B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES) "

    If Select("TMPSD4") > 0
    	TMPSD4->(dbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSD4",.T.,.T.)
	DbSelectArea("TMPSD4")
	DbGoTop()
	
	
	If !EOF()
		cErro := ""
		While !EOF()
			DbSelectArea("SB1")
			DbSetOrder(1)
			DbSeek(xfilial("SB1")+TMPSD4->D4_COD)
			
			cErro += " Produto com estoque insuficiente "+TMPSD4->D4_COD+" - "+alltrim(SB1->B1_DESC)+"!"+ Chr(13) + Chr(10)
			cErro += " Do produto "+TMPSD4->C2_PRODUTO+" - Quantidade solicitada: "+alltrim(str(TMPSD4->EMPENHO)) + Chr(13) + Chr(10)
			cErro += " Quantidade em Estoque: "+alltrim(str(TMPSD4->(B2_QATU - B2_QACLASS - B2_RESERVA - B2_XRES))) + Chr(13) + Chr(10) + Chr(13) + Chr(10)
			DbSelectArea("TMPSD4")
			DbSkip()
		End
		EECView(cErro)
		If Select("TMPSD4") > 0
			TMPSD4->(dbCloseArea())
    	EndIf
	Else
		If MsgYesNo("Estoque suficiente. Deseja liberar a OP?","Libera��o de OP")
		    If Select("TMPSD4") > 0
		    	TMPSD4->(dbCloseArea())
    		EndIf
    		_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'L' where C2_NUM = '"+_op+"' "
    		TcSqlExec(_qry)
    		
    		_qry := "update "+RetSqlName("SB2")+" set B2_XRES = B2_XRES + "
    		_qry += "(Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 where D4_XRES = 0 and SD4.D_E_L_E_T_ = '' and "
    		_qry += "B2_COD = D4_COD and D4_FILIAL = '"+xfilial("SD4")+"' and left(D4_OP,6) = '"+_op+"') "
    		_qry += "from "+RetSqlName("SB2")+" SB2 where SB2.D_E_L_E_T_ = '' and B2_FILIAL = '"+xfilial("SB2")+"' and "
    		_qry += "B2_COD in (Select D4_COD from "+RetSqlName("SD4")+" SD4 where D4_FILIAL = '"+xfilial("SD4")+"' and SD4.D_E_L_E_T_ = '' and "
    		_qry += "left(D4_OP,6) = '"+_op+"' group by D4_COD) and B2_LOCAL = 'A1' and "
    		_qry += "(Select Sum(D4_QUANT) from "+RetSqlName("SD4")+" SD4 where D4_FILIAL = '"+xfilial("SD4")+"' and SD4.D_E_L_E_T_ = '' and "
    		_qry += "B2_COD = D4_COD and D4_XRES = 0 and left(D4_OP,6) = '"+_op+"') is not null " 


			TcSqlExec(_qry)

			_qry := "update "+RetSqlName("SD4")+" set D4_XDTLIB = '"+dtos(ddatabase)+"', D4_XRES = D4_QTDEORI, "
			_qry += "D4_XOPP = left(D4_OP,6), D4_XTIPO = (Select B1_TIPO from "+RetSqlName("SB1")+" SB1 where SB1.D_E_L_E_T_ = '' and B1_COD = D4_COD) "
			_qry += "where D4_FILIAL = '"+xfilial("SD4")+"' and D_E_L_E_T_ = '' and "
			_qry += "left(D4_OP,6) = '"+_op+"' and D4_XDTLIB = '' "
			TcSqlExec(_qry)
			
			_qry := "update "+RetSqlName("ZAM")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where ZAM_OP = '"+_op+"' "
			TcSqlExec(_qry)
			
			
			_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XDTRANS = '"+dtos(ddatabase)+"' WHERE C2_NUM = '"+_op+"' AND C2_FILIAL = '"+xfilial("SC2")+"' AND D_E_L_E_T_ = '' "
			TcSqlExec(_qry)
			
			
			MsgInfo("Ordem de Produ��o liberada!","Aviso")
		Endif
	Endif

Return

Static Function MONTATRAB(cResp)
		
	_QRY := "DELETE "+RetSqlName("ZAM")
	TcSqlExec(_QRY)

	_qry := "Select C2_NUM, Left(C2_PRODUTO,8) as PRODUTO, B4_DESC, Substring(C2_PRODUTO,9,3) as COR, BV_DESCRI from "+RetSqlName("SC2")+" SC2 (NOLOCK) "
	_qry += "left join "+RetSqlName("SB4")+" SB4 on SB4.D_E_L_E_T_ = '' and B4_COD = left(C2_PRODUTO,8) "
	_qry += "left join "+RetSqlName("SBV")+" SBV on SBV.D_E_L_E_T_ = '' and BV_TABELA = B4_LINHA AND BV_CHAVE = SUBSTRING(C2_PRODUTO,9,3) "
	if cResp == "Liberadas e Transferidas"
	_qry += "where SC2.D_E_L_E_T_ = '' AND C2_TPOP = 'F'  AND C2_XRES <> '' "
	else
	_qry += "where SC2.D_E_L_E_T_ = '' and C2_DATRF = '' and C2_QUJE < C2_QUANT  and C2_XRES = '' and C2_SEQPAI = '' AND C2_TPOP = 'F' " 
	endif
	_qry += "group by C2_NUM, Left(C2_PRODUTO,8) , B4_DESC, Substring(C2_PRODUTO,9,3), BV_DESCRI "
	_qry += "order by C2_NUM "
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSC2",.T.,.T.)
	
	dbSelectArea("TMPSC2")
	aCposBrw := {}
	aEstrut  := {}
	If !EOF()
			
        While !TMPSC2->(Eof())
               
			RecLock("ZAM", .T.)
				ZAM->ZAM_OP      := TMPSC2->C2_NUM
                ZAM->ZAM_PRODUT  := TMPSC2->PRODUTO
                ZAM->ZAM_DESC    := TMPSC2->B4_DESC
                ZAM->ZAM_COR	 := TMPSC2->COR
                ZAM->ZAM_DESCCO	 := TMPSC2->BV_DESCRI
			ZAM->(msUnlock())

			TMPSC2->(DbSkip())
		Enddo

	EndIf

    If Select("TMPSC2") > 0
    	TMPSC2->(dbCloseArea())
    EndIf

 Return
 
 USER FUNCTION FILTRO()

	Local lOk	:= .F.
	Local aParam := {}
	Local aRetParm	:= {}
	Local cResp := ""
	Local aList := {"Liberadas e Transferidas","Pendente de Liberacao"}
	local cVldAlt	:= ".T." 
	local cVldExc	:= ".T." 

	aAdd(aParam,{2,"Filtro","Todas as OP",aList, 90,'.T.',.F.}) 
	
	
	If ParamBox(aParam,"Filtro",@aRetParm,{||.T.},,,,,,"U_HPCPP014",.F.,.F.)
		lOk	:= .T.
		cResp := aRetParm[1]
	else
		lOk := .F.
	endif	
		
	if lOk == .T. 
		MsgRun("Selecionando Registros, Aguarde...",,{|| MONTATRAB(cResp)})
		
		cCadastro := "Libera��o de Ordem de Produ��o"
		aRotina := {;
		{ "Pesquisar" 	, "AxPesqui"					, 0, 1},;
		{ "Checar"		, "u_CHKEST(ZAM->ZAM_OP)"		, 0, 2},;
		{ "Visualizar"	, "u_VISOP(ZAM->ZAM_OP)"		, 0, 2},;
		{ "Estorna"     ,  "u_ESTOP()"                  , 0, 2},;
		{ "Imprime OP" 	, "u_HRPCP001(ZAM->ZAM_OP)"		, 0, 3}}

		mBrowse( 6, 1,22,75,"ZAM",,,,,,)
		
	else 
		MsgAlert("Cancelado pelo usuario","HOPE")
		return .F.
	EndIf
	
RETURN 

User Function ESTOP()

	Pergunte("HPCP014",.T.)
	
	_op := MV_PAR01
	
	DbSelectArea("SC2")
	DbSetOrder(1)
	DbSeek(xfilial("SC2")+_op)

	If SC2->C2_XRES == "T"
		If !MsgYesNo("Esta OP j� foi Transferida. A Tranfer�ncia j� foi estornada?","OP j� transferida")
			Return()
		Endif 
	Endif

	DbSelectArea("SD4")
	DbSetOrder(2)
	DbSeek(xfilial("SD4")+_op)
	_ok := .T.
	
	While !EOF() .and. SubStr(SD4->D4_OP,1,6) == SubStr(_op,1,6)
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SD4->D4_COD)
		
		If !(SB1->B1_TIPO $ ("MI|PI")) .and. SubStr(SD4->D4_COD,1,1) <> "B" .AND. !(SB1->B1_GRUPO $ ('MP05|MP16|MP17|MP91'))
			DbSelectArea("SB2")
			DbSetOrder(1)
			DbSeek(xfilial("SB2")+SD4->D4_COD+SD4->D4_LOCAL)
			
			If Found()
				RecLock("SB2",.F.)
					Replace B2_XRES	with B2_XRES - SD4->D4_XRES
				MsUnLock()			
				
				RecLock("SD4",.F.)
					Replace D4_XRES with 0
				MsUnLock()
			Else
				_ok := .F.
			Endif
		Endif
	
		DbSelectArea("SD4")
		DbSkip()
	End
	
	If _ok
		_qry := "update "+RetSqlName("SC2")+" set C2_XRES = '' where D_E_L_E_T_ = '' and C2_NUM = '"+SubStr(_op,1,6)+"' "
		TcSqlExec(_qry)
		MsgBox("Estorno realizado com sucesso!","Aviso","INFO")
	Else
		Alert("N�o foi poss�vel realizar o estorno!")
	Endif		
Return