#Include 'Protheus.ch'

User Function A261TOK( )

Local lRet := .T. // 'Valida��es do Usuario
Local _erro := ""
Local nPosOP    := aScan(aHeader,{|x| AllTrim(x[2]) == 'D3_XOP'})

_area := GetArea()
For _i := 1 to len(aCols)
	If ACOLS[_i,len(AHEADER)+1] == .F. .and. aCols[_i,9] = "AP"
		If alltrim(aCols[_i,nPosOP]) <> ""
//			lret := .F.
//			_erro += "Obrigat�rio o preenchimento da Ordem de Produ��o! "+Chr(13)
//		Else
			If POSICIONE("SC2",1,xFilial("SC2")+alltrim(aCols[_i,nPosOP]),"C2_XRES") <> "L"
				lret := .F.
				_erro += "Ordem de Produ��o n�o est� liberada! "+chr(13)
			Else
				DbSelectArea("SB2")
				_areaB2 := GetArea()
				DbSetOrder(1)
				DbSeek(xfilial("SB2")+aCols[_i,1]+aCols[_i,4])
				If Found()
					If (B2_QATU - B2_QEMP - B2_RESERVA) < aCols[_i,16]
						lret := .F.
						_erro += "Saldo insuficiente para atender a OP! "+chr(13)
					Endif
				Else
					lret := .F.
					_erro += "Produto n�o tem saldo cadastrado! "+chr(13)
				Endif
				RestArea(_areaB2)
			Endif
		Else
			DbSelectArea("SB2")
			_areaB2 := GetArea()
			DbSetOrder(1)
			DbSeek(xfilial("SB2")+aCols[_i,1]+aCols[_i,4])
			If Found()
				If (B2_QATU - B2_QEMP - B2_RESERVA - B2_XRES) < aCols[_i,16]
					lret := .F.
					_erro += "Saldo insuficiente! "+chr(13)
				Endif
			Else
				lret := .F.
				_erro += "Produto n�o tem saldo cadastrado! "+chr(13)
			Endif
			RestArea(_areaB2)
		Endif
	Endif
Next

If !lRet
	Alert(_erro)
Else
	For _i := 1 to len(aCols)
		If ACOLS[_i,len(AHEADER)+1] == .F. .and. aCols[_i,9] = "AP"
			_qry := "update "+RetSqlName("SC2")+" set C2_XRES = 'T' where D_E_L_E_T_ = '' and C2_FILIAL = '"+xfilial("SC2")+"' and C2_NUM+C2_ITEM+C2_SEQUEN = '"+left(aCols[_i,nPosOP],11)+"' "
			TcSqlExec(_qry)
			
			// TODO WESKLEY SILVA -- Update para todo produto do tipo KIT ficar como Transferido
			_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XRES = 'T' FROM "+RetSqlName("SC2")+"  JOIN "+RetSqlName("SB1")+" ON C2_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' WHERE C2_XRES = 'L' AND SC2010.D_E_L_E_T_ = '' AND B1_TIPO = 'KT' AND C2_NUM = '"+left(aCols[_i,nPosOP],6)+"' "
			TcSqlExec(_qry)
			
			_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XRES = 'T' WHERE LEFT(C2_PRODUTO,4) IN ('MIEE','MIEF') AND C2_NUM = '"+left(aCols[_i,nPosOP],6)+"' AND D_E_L_E_T_ = '' AND C2_XRES = 'L' "
			TcSqlExec(_qry)
			
			//_qry := " UPDATE "+RetSqlName("SC2")+" SET C2_XRES = 'T' FROM "+RetSqlName("SG2")+" SG2 JOIN "+RetSqlName("SC2")+" SC2 ON (G2_REFGRD =  LEFT(C2_PRODUTO,8))  WHERE G2_OPERAC = '70' AND  SG2.D_E_L_E_T_ = '' AND C2_NUM = '"+left(aCols[_i,nPosOP],6)+"' AND SC2.D_E_L_E_T_ = '' "
			//TcSqlExec(_qry)

			DbSelectArea("SD4")
			_areaD4 := GetArea()
			DbSetOrder(1)
			DbSeek(xfilial("SD4")+aCols[_i,1]+left(aCols[_i,nPosOP],14))
				
			While !EOF() .and. SD4->D4_COD = aCols[_i,1] .and. left(SD4->D4_OP,14) = left(aCols[_i,nPosOP],14)
					
				RecLock("SD4",.F.)
					Replace D4_XQTRANS	with D4_QUANT
					Replace D4_XDTTRA 	with ddatabase
				MsUnLock()
				
				_qry := "Update "+RetSqlName("SB2")+" set B2_XRES = B2_XRES - "+str(SD4->D4_XRES)+" where B2_FILIAL = '"+xfilial("SB2")+"' and D_E_L_E_T_ = '' and B2_COD = '"+SD4->D4_COD+"' and B2_LOCAL = '"+aCols[_i,4]+"' "
				TcSqlExec(_qry)
				
				Dbskip()
			End
		Endif
	Next
Endif
RestArea(_area)
Return lRet