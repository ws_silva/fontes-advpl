#include "topconn.ch"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "FWPrintSetup.ch"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH" 
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'       
#INCLUDE "RPTDEF.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HRPCP001  �Autor  �Daniel R. Melo      � Data �  13/02/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de ordem de abastecimento.                        ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HRPCP001(_cOP)

//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
	Private cDesc1     := "Este programa tem como objetivo imprimir relatorio "
	Private cDesc2     := "de acordo com os parametros informados pelo usuario."
	Private cDesc3 	   := ""
	Private cString    := "SC2"
	Private cPerg      := PADR("HRPCP001",10)

	Private Tamanho  := "G"
	Private aOrd	   := {}
	Private aReturn  := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
	//Private li		   := 80, limite:=132, lRodape:=.F.
	Private wnrel    := "HRPCP001"
	Private titulo   := "ORDEM DE CORTE REQUISICAO DE MATERIA PRIMA"
	Private nLin 	   := 100
	Private nCol 	   := 60
	Private nPula    := 60
	Private nfim     := 3200
	Private imprp	   := .F.
	Private cOrdemServ := " "
	Private cObs := " "
	Private cProdPri := ""
	Private cProdCab := ""
	Private cCabec := ""
	Private cRolet := ""
	Private cAlca := ""

	Private oFont08  := TFont():New("Arial",,08,,.f.,,,,,.f.)
	//Private oFont08n := TFont():New("Arial",,08,,.t.,,,,,.f.)
	Private oFont09  := TFont():New("Arial",,09,,.f.,,,,,.f.)
	Private oFont09n := TFont():New("Arial",,09,,.t.,,,,,.f.)
	Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
	Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
	Private oFont11  := TFont():New("Arial",,11,,.f.,,,,,.f.)
	//Private oFont11n := TFont():New("Arial",,11,,.t.,,,,,.f.)
	//Private oFont12  := TFont():New("Arial",,12,,.f.,,,,,.f.)
	Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)
	//Private oFont14  := TFont():New("Arial",,14,,.f.,,,,,.f.)
	Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
	Private aParam := {}
	Private aRetParm := {}
	
	
		cOrdemServ	:= _cOP

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf

	cQuery := " SELECT TOP 1 C2_NUM AS NUM , C2_OBS AS OBS  FROM "+RetSqlName("SC2")+" SC2 WHERE C2_NUM = '"+cOrdemServ+"' AND C2_TPOP = 'F' AND C2_XRES <> '' AND D_E_L_E_T_ = '' "

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
	
	if EMPTY(TMP->NUM)
		MsgAlert("Ordem de Produ��o n�o liberada! Favor liberar.","HOPE")
		RETURN 
	ELSE	
	
	cObs := TMP->OBS
		
	aAdd(aParam,{1,"N� Paginas",2 ,"",".T.",,".T.",40,.F.})
	
	If ParamBox(aParam,"Filtro",@aRetParm,{||.T.},,,,,,"U_HPCPP014",.F.,.F.)
		lOk	:= .T.
		nResp := aRetParm[1]
	else
		lOk := .F.
	endif	
	
	wnrel:=SetPrint(cString,"HRPCP001",cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.T.,Tamanho)

	//If nLastKey == 27
	//Endif
			
	If nLastKey == 27
		Return
	Endif

	oPrn 	 := TMSPrinter():New()
	
	Processa({||ImpRel(cOrdemServ)}, titulo, "Imprimindo, aguarde...")
		
	If( aReturn[5] == 1 ) //1-Disco, 2-Impressora
		oPrn:Preview()
	Else
		oPrn:Setup()
		oPrn:Print({1,10},nResp)
	EndIf
	
	ENDIF
	MS_FLUSH() 
Return
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � ImpRel   � Autor �Daniel R. Melo         � Data � 07/02/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � HESTR001                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpRel(cOrdemServ)

	Local aArea := GetArea()
	Local _cOrdem		:= cOrdemServ
	Local cTipoMat	:= ""
	Local cProdPri := " "
	
	IncProc()

	For _y := 1 to 3
	
		nLin	     := 100
		nCol	     := 50
		nPula       := 50
		nPosTit     := 60
		
		
		if _y == 1
			cTipoMat	:= "AV"
		endif
		
		
		if _y == 2
			cTipoMat	:= "B"
		endif

		if _y == 3
			cTipoMat	:= "A"	
		endif
					
			
		
		if cTipoMat == "A"
		
		
		If Select("TMP2") > 0
			TMP2->(DbCloseArea())
		EndIf

		cQuery := " SELECT TOP 1 SD4.D4_PRODUTO AS PRODUTO " 
		cQuery += "	FROM "+RetSqlName("SD4")+" SD4 "  
		cQuery += " JOIN "+RetSqlName("SC2")+" SC2 WITH (NOLOCK) ON C2_FILIAL=D4_FILIAL AND D4_OP = C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD AND C2_PRODUTO = D4_PRODUTO AND SC2.D_E_L_E_T_ = '' " 
		cQuery += "	JOIN "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) ON SB1.B1_COD=SD4.D4_PRODUTO AND SB1.D_E_L_E_T_='' " 
		cQuery += " JOIN "+RetSqlName("SBM")+" SBM WITH (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' " 
		//cQuery += " JOIN "+RetSqlName("SB4")+" SB4 (NOLOCK) ON SB4.D_E_L_E_T_ = '' AND B4_COD = SUBSTRING(D4_COD,1,8)" 
		//cQuery += " JOIN "+RetSqlName("SBV")+" SBV1 (NOLOCK) ON SBV1.D_E_L_E_T_ = '' AND SBV1.BV_TABELA = B4_LINHA AND SBV1.BV_CHAVE = SUBSTRING(D4_COD,9,3) " 
		//cQuery += " JOIN "+RetSqlName("SBV")+" SBV2 (NOLOCK) ON SBV2.D_E_L_E_T_ = '' AND SBV2.BV_TABELA = B4_COLUNA AND SBV2.BV_CHAVE = SUBSTRING(D4_COD,12,4) " 
		cQuery += " WHERE SD4.D_E_L_E_T_='' " 
		cQuery += "	AND BM_GRUPO IN ("+ GETMV("HP_GRPOPAL") +") " 
		cQuery += "	AND C2_NUM = '"+AllTrim(_cOrdem)+"' " 
		cQuery += " GROUP BY SD4.D4_PRODUTO " 
		cQuery += "	HAVING SUM(SD4.D4_QUANT)>0 " 

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP2",.T.,.T.)
		DbSelectArea("TMP2")
		TMP2->(DbGoTop())
	
		cVeriAlca := TMP2->PRODUTO
		   
		IF Empty(cVeriAlca)
			Exit 
	    endif 
		TMP2->(DbCloseArea())	
			
		ENDIF
		
		
		/*if cTipoMat == 'B'
		
		If Select("TMP2") > 0
			TMP2->(DbCloseArea())
		EndIf
		
		
		cQuery := " SELECT SUM(C2_QUANT) AS QUANTIDADE , LEFT(C2_PRODUTO,8) AS PRODUTO FROM "+RetSqlName("SC2")+"  "
		cQuery += " JOIN SB1010 ON B1_COD = C2_PRODUTO AND SB1010.D_E_L_E_T_ = '' "
		cQuery += " WHERE C2_NUM = '"+AllTrim(_cOrdem)+"' AND B1_GRUPO IN ("+ GETMV("HP_GRPOPAL") +") "
		cQuery += " GROUP BY LEFT(C2_PRODUTO,8) "
		
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP2",.T.,.T.)
		DbSelectArea("TMP2")
		TMP2->(DbGoTop())
		
		cVeriAlca := TMP2->PRODUTO
		   
		//IF Empty(cVeriAlca)
		//	Exit 
		//endif 
		TMP2->(DbCloseArea())	
				
		endif
		*/
		
		
		oPrn:StartPage()
		
		oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)

		MSBAR('CODE128',1,23.8,cOrdemServ,oPrn,.F.,,.T.,0.042,1.7,,,,.F.)
	
		nLin += nPula
		oPrn:Box(nLin+20,50,nLin+23,nfim-450)

		nLin += nPula

		DbSelectArea("SC2")
		DbSetOrder(1)
		DbSeek(xfilial("SC2")+_cOrdem)

		If cTipoMat == 'B'
			oPrn:Say(nLin,080,"ORDEM DE CORTE N�:",oFont10,100)
			oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)
		
		elseif cTipoMat	== "A"
					
			oPrn:Say(nLin,080,"ORDEM DE AL�A N�:",oFont10,100)
			oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)
					
		ElseIF cTipoMat == "AV"
			oPrn:Say(nLin,080,"ORDEM DE AVIAMENTOS N�:",oFont10,100)
			oPrn:Say(nLin-5,600,AllTrim(_cOrdem),oFont12n,100)	
		Endif

		
		
		cObsOs		:= AllTrim(SubStr(SC2->C2_XBOS,1,100))
		Cabec1b	:= SubStr(SC2->C2_PRODUTO,1,8) + " - " + AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(SC2->C2_PRODUTO,1,8),"B4_DESC"))
				
		oPrn:Say(nLin,900,"PRODUTO:",oFont10,100)
		oPrn:Say(nLin,1130,Cabec1b,oFont10n,100)
		
		dData := Date()
		
		oPrn:Say(nLin,2250,"Data de Impress�o:",oFont10,100)
		oPrn:Say(nLin,2600,Dtos(dData),oFont10n,100)

		nLin += nPula
		oPrn:Box(nLin,50,nLin+3,nfim-450)
		nLin += nPula
		
		oPrn:Say(nLin,1300,"ORIENTACAO DA ORDEM:",oFont09n,100)
		oPrn:Say(nLin,2100,cObs,oFont10,100)
			
			
		nLin += nPula 
		
		If cTipoMat == 'B'
			
		If Select("TMP2") > 0
			TMP2->(DbCloseArea())
		EndIf
		
		cQuery := " SELECT TOP 1 C2_PRODUTO AS PRODUTO FROM "+RetSqlName("SC2")+" WITH (NOLOCK)  "
		cQuery += " JOIN "+RetSqlName("SB1")+" WITH (NOLOCK) ON B1_COD = C2_PRODUTO AND SB1010.D_E_L_E_T_ = '' "
		cQuery += " WHERE C2_NUM = '"+AllTrim(_cOrdem)+"' AND B1_GRUPO IN ("+ GETMV("HP_GRPOPCO") +") AND SC2010.D_E_L_E_T_ = '' AND LEFT(C2_PRODUTO,4) = 'MIRO'  "
		cQuery += " GROUP BY C2_PRODUTO "
		
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP2",.T.,.T.)
		DbSelectArea("TMP2")
		TMP2->(DbGoTop())	
		
		
		 cRolet := LEFT(TMP2->PRODUTO,4)
		
		TMP2->(DBCLOSEAREA())	
				
		If Select("TMP1") > 0
			TMP1->(DbCloseArea())
		EndIf

		
		cQuery := " SELECT SUM(C2_QUANT) AS QUANTIDADE , LEFT(C2_PRODUTO,8) AS PRODUTO FROM "+RetSqlName("SC2")+" WITH (NOLOCK)  "
		cQuery += " JOIN "+RetSqlName("SB1")+" WITH (NOLOCK)  ON B1_COD = C2_PRODUTO AND SB1010.D_E_L_E_T_ = '' "
		cQuery += " WHERE C2_NUM = '"+AllTrim(_cOrdem)+"' AND B1_GRUPO IN ("+ GETMV("HP_GRPOPCO") +") AND B1_DESC NOT LIKE 'ESPUMA%' AND SC2010.D_E_L_E_T_ = '' "
		cQuery += " GROUP BY LEFT(C2_PRODUTO,8) "
		
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP1",.T.,.T.)
		DbSelectArea("TMP1")
		TMP1->(DbGoTop())
				
		
		oPrn:Say(nLin,aColI1[1],"QUANTIDADES SOLICITADAS POR GRADE E COR",oFont09n,100)
			oPrn:Say(nLin,aColI1[3],"QUANTIDADES PRODUZIDAS",oFont09n,100)
			nLin += nPula
			oPrn:Box(nLin,50,nLin+3,nfim)
			nLin += nPula
			
			aTaman	:= VERTAM(_cOrdem)
			aCores	:= VERCOR(_cOrdem)
			
			aColI1x1 := aColI1[2]
			aColI1x2 := aColI1[4]
			
			For _i := 1 to len(aTaman)
				oPrn:Say(nLin,aColI1x1,AllTrim(aTaman[_i,3]),oFont09n,100)
				aColI1x1 += 120
				oPrn:Say(nLin,aColI1x2,AllTrim(aTaman[_i,3]),oFont09n,100)
				aColI1x2 += 120
			Next _i
			nLin += nPula
	
			For _i := 1 to Len(aCores)
				oPrn:Say(nLin,aColI1[1],aCores[_i,1] +"-"+ SubStr(aCores[_i,2],1,35),oFont09,100)
				oPrn:Say(nLin,aColI1[3],aCores[_i,1] +"-"+ SubStr(aCores[_i,2],1,35),oFont09,100)

				aColI1x1 := aColI1[2]
				aColI1x2 := aColI1[4]
				For _x := 1 to len(aTaman)
					If aTaman[_x,1] == aCores[_i,1]
						oPrn:Say(nLin,aColI1x1,Alltrim(STR(aTaman[_x,4])),oFont09,100)
						aColI1x1 += 120
						oPrn:Say(nLin,aColI1x2,Alltrim(STR(aTaman[_x,5])),oFont09,100)
						aColI1x2 += 120
					End
				next _x
				nLin += nPula
			next _i

			nLin += nPula + nPula
			
			//oPrn:Box(nLin-10,50,nLin+nPula+5,nfim)
			//oPrn:Say(nLin,080,cObsOs,oFont11,100)
			//nLin += nPula + nPula + nPula
			
		IF !Empty(cRolet)
			oPrn:Say(nLin,080,"ROLETE",oFont10,100)
			nLin += nPula + nPula + nPula
		endif
	
		oPrn:Say(nLin,080,"PRODUTO PRINCIPAL:",oFont10,100)
		nLin += nPula + nPula
		
		while !TMP1->(EOF())
			
		
		cProdPri := TMP1->PRODUTO	
		cQuant := TMP1->QUANTIDADE
		cCabecCort	:=  AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(TMP1->PRODUTO,1,8),"B4_DESC"))
				
		oPrn:Say(nLin-5,080,cProdPri,oFont12n,100)
		oPrn:Say(nLin-5,550,cCabecCort,oFont12n,100)
		oPrn:Say(nLin-5,3000,Transform(cQuant,"@E 999,999.99"),oFont12n,100)
		nLin += nPula + nPula	
							
		
		aMatPrim	:= ORDCORTE(cTipoMat,_cOrdem,cProdPri)
		
		aColItem := {050,450,1750,1900,2300,2600,2700}
			aColCab  := {"| CODIGO","| DESCRICAO MATERIA PRIMA","| TAM.","| CORES","| QTD. SOL.","| UN","| QTD. UTILIZADA"}
			For _i := 1 to len(aColCab)
				oPrn:Say(nLin,aColItem[_i],AllTrim(aColCab[_i]),oFont09,100)
			Next _i
			nLin += nPula
			oPrn:Box(nLin,50,nLin+3,nfim)
			nLin += nPula
			
			
			For _i := 1 to len(aMatPrim)
				oPrn:Say(nLin,aColItem[1],"| "+ Alltrim(aMatPrim[_i,1])								,oFont08,100)		//CODIGO
				oPrn:Say(nLin,aColItem[2],"| "+ SubStr(Alltrim(aMatPrim[_i,2]),1,80)				,oFont08,100)		//DESCRICAO
				oPrn:Say(nLin,aColItem[3],"| "+ Alltrim(aMatPrim[_i,4])								,oFont08,100)		//TAMANHO
				oPrn:Say(nLin,aColItem[4],"| "+ SubStr(Alltrim(aMatPrim[_i,6]),1,30)				,oFont08,100)		//COR
				oPrn:Say(nLin,aColItem[5],"| "+ Transform(aMatPrim[_i,8],"@E 999,999.99999")		,oFont08,100)		//QTD. SOLICITADA
				oPrn:Say(nLin,aColItem[6],"| "+ Alltrim(aMatPrim[_i,7])								,oFont08,100)		//UNIDADE
				oPrn:Say(nLin,aColItem[7],"|  "														,oFont08,100)		//QTD. UTILIZADA
					
				If nLin > 2200
					nLin	  := 100
					oPrn:EndPage()
					oPrn:StartPage()
					
					oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
					
					nLin += nPula + nPula
					oPrn:Box(nLin-30,50,nLin-30+3,nfim-530)
				
					MSBAR('CODE128',1,23.8,cOrdemServ,oPrn,.F.,,.T.,0.042,1.7,,,,.F.)

					oPrn:Box(nLin-10,50,nLin+nPula+5,nfim)
					oPrn:Say(nLin,080,cObsOs,oFont11,100)
					nLin += nPula + nPula
					
					If cTipoMat == 'B'
						oPrn:Say(nLin,080,"ORDEM DE CORTE N�:",oFont10,100)
						oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)
					elseif cTipoMat	== "A"
						oPrn:Say(nLin,080,"ORDEM DE AL�A N�:",oFont10,100)
						oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)						
					ElseIF cTipoMat == "AV"
					
						oPrn:Say(nLin,080,"ORDEM DE AVIAMENTOS N�:",oFont10,100)
						oPrn:Say(nLin-5,600,AllTrim(_cOrdem),oFont12n,100)	
					Endif
					
					Cabec1b	:= SubStr(SC2->C2_PRODUTO,1,8) + " - " + AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(SC2->C2_PRODUTO,1,8),"B4_DESC"))
					oPrn:Say(nLin,900,"PRODUTO:",oFont10,100)
					oPrn:Say(nLin,1130,Cabec1b,oFont10n,100)
			
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim-530)

					nLin += nPula + nPula
					For _x := 1 to len(aColCab)
						oPrn:Say(nLin,aColItem[_x],AllTrim(aColCab[_x]),oFont08,100)
					Next _x
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim)

				Endif

				nLin += nPula

			Next _i
			
			TMP1->(DbSkip())
								
		enddo
		
				
		If cTipoMat == 'B'
		
			If nLin > 1600
					nLin	  := 200
					oPrn:EndPage()
					oPrn:StartPage()
			
				nLin     := 1620
				aColTab  := {050,330,740,1150,1560,1970,2380,2790,3200}
				aColTCab := {"DETALHE","ENCAIXE","COMPRIMENTO","AREA","% QUEBRA","PESO","LARGURA","FOLHAS"}
				aColTLin := {"TECIDO","FUNDO","TECIDO","TECIDO"}

				oPrn:Say(nLin,aColTab[1],AllTrim(aColTCab[1]),oFont08,100)
				oPrn:Box(nLin+40,aColTab[2]-30,nLin+43,nfim)
				
				nLin += nPula+20
				For _x := 1 to len(aColTab)-2
					oPrn:Say(nLin,aColTab[_x+1]+100,aColTCab[_x+1],oFont08,100)
				Next _x
				nLin += 70
				For _z := 1 to len(aColTLin)
					oPrn:Say(nLin+50,aColTab[1],aColTLin[_z],oFont09,100)
					For _x := 1 to len(aColTab)-2
						oPrn:Box(nLin,aColTab[_x+1],nLin+120,aColTab[_x+2])
					Next _x
					nLin += 120
				Next _z
				
			ELSE
			
				nLin     := 1620
				aColTab  := {050,330,740,1150,1560,1970,2380,2790,3200}
				aColTCab := {"DETALHE","ENCAIXE","COMPRIMENTO","AREA","% QUEBRA","PESO","LARGURA","FOLHAS"}
				aColTLin := {"TECIDO","FUNDO","TECIDO","TECIDO"}

				oPrn:Say(nLin,aColTab[1],AllTrim(aColTCab[1]),oFont08,100)
				oPrn:Box(nLin+40,aColTab[2]-30,nLin+43,nfim)
				
				nLin += nPula+20
				For _x := 1 to len(aColTab)-2
					oPrn:Say(nLin,aColTab[_x+1]+100,aColTCab[_x+1],oFont08,100)
				Next _x
				nLin += 70
				For _z := 1 to len(aColTLin)
					oPrn:Say(nLin+50,aColTab[1],aColTLin[_z],oFont09,100)
					For _x := 1 to len(aColTab)-2
						oPrn:Box(nLin,aColTab[_x+1],nLin+120,aColTab[_x+2])
					Next _x
					nLin += 120
				Next _z
				ENDIF 							
			Endif
		
		TMP1->(DbCloseArea())
		
		elseif cTipoMat	== "A"
				
		cCabec := SubStr(SC2->C2_PRODUTO,1,8)
		
		
		If Select("TMP2") > 0
			TMP2->(DbCloseArea())
		EndIf

		
		cQuery := " SELECT SUM(C2_QUANT) AS QUANTIDADE, C2_PRODUTO AS PRODUTO FROM "+RetSqlName("SC2")+" WITH (NOLOCK) "
		cQuery += " JOIN "+RetSqlName("SB1")+" WITH (NOLOCK) ON B1_COD = C2_PRODUTO AND SB1010.D_E_L_E_T_ = '' "
		cQuery += " WHERE C2_NUM = '"+AllTrim(_cOrdem)+"' AND B1_GRUPO IN ("+ GETMV("HP_GRPOPAL") +") AND SC2010.D_E_L_E_T_ = '' "
		cQuery += " GROUP BY C2_PRODUTO "
		
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP2",.T.,.T.)
		DbSelectArea("TMP2")
		TMP2->(DbGoTop())	
		
		
				
		oPrn:Say(nLin,080,"PRODUTO PRINCIPAL:",oFont10,100)
		nLin += nPula + nPula
					
		
		while !TMP2->(EOF())
		
		If Select("TPP") > 0
			TPP->(DbCloseArea())
		EndIf
		
		//TODO VERIFICA O CONSUMO DO ROLETE EM UM PRODUTO PRINCIPAL
		cQuery1 := " SELECT SG1.G1_COD FROM "+RetSqlName("SG1")+" SG1 WITH (NOLOCK)  WHERE SG1.G1_COMP = '"+Alltrim(TMP2->PRODUTO)+"' AND LEFT(SG1.G1_COD,8) = '"+Alltrim(cCabec)+"' "
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TPP",.T.,.T.)
		DbSelectArea("TPP")
		TPP->(DbGoTop())
		//FIM	
			
			cProd1 :=  TPP->G1_COD				
			cProdPri := TMP2->PRODUTO	
			cQuant := TMP2->QUANTIDADE
			cCabecAlca	:=  AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(TMP2->PRODUTO,1,8),"B4_DESC"))
			
			
			IF !Empty(cProd1) 
				oPrn:Say(nLin-5,080,cProd1,oFont11,100)
				nLin += nPula + nPula
				oPrn:Say(nLin-5,080,cProdPri,oFont12n,100)
				oPrn:Say(nLin-5,550,cCabecAlca,oFont12n,100)
				oPrn:Say(nLin-5,3000,Transform(cQuant,"@E 999,999.99"),oFont12n,100)
				nLin += nPula + nPula
			else
				oPrn:Say(nLin-5,080,cProdPri,oFont12n,100)
				oPrn:Say(nLin-5,550,cCabecAlca,oFont12n,100)
				oPrn:Say(nLin-5,3000,Transform(cQuant,"@E 999,999.99"),oFont12n,100)
				nLin += nPula + nPula
			endif	
				
			aMatPrim := ORDALCA(cTipoMat,_cOrdem,cProdPri)
						 			 
			aColItem := {050,450,1750,1900,2300,2600,2700}
			aColCab  := {"| CODIGO","| DESCRICAO MATERIA PRIMA","| TAM.","| CORES","| QTD. SOL.","| UN","| QTD. UTILIZADA"}
			For _i := 1 to len(aColCab)
				oPrn:Say(nLin,aColItem[_i],AllTrim(aColCab[_i]),oFont09,100)
			Next _i
			nLin += nPula
			oPrn:Box(nLin,50,nLin+3,nfim)
			nLin += nPula
			
			
			For _i := 1 to len(aMatPrim)
				oPrn:Say(nLin,aColItem[1],"| "+ Alltrim(aMatPrim[_i,1])								,oFont08,100)		//CODIGO
				oPrn:Say(nLin,aColItem[2],"| "+ SubStr(Alltrim(aMatPrim[_i,2]),1,80)				,oFont08,100)		//DESCRICAO
				oPrn:Say(nLin,aColItem[3],"| "+ Alltrim(aMatPrim[_i,4])								,oFont08,100)		//TAMANHO
				oPrn:Say(nLin,aColItem[4],"| "+ SubStr(Alltrim(aMatPrim[_i,6]),1,30)				,oFont08,100)		//COR
				oPrn:Say(nLin,aColItem[5],"| "+ Transform(aMatPrim[_i,8],"@E 999,999.99999")		,oFont08,100)		//QTD. SOLICITADA
				oPrn:Say(nLin,aColItem[6],"| "+ Alltrim(aMatPrim[_i,7])								,oFont08,100)		//UNIDADE
				oPrn:Say(nLin,aColItem[7],"|  "														,oFont08,100)		//QTD. UTILIZADA
					
				If nLin > 2200
					nLin	  := 100
					oPrn:EndPage()
					oPrn:StartPage()
					
					oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
					
					nLin += nPula + nPula
					oPrn:Box(nLin-30,50,nLin-30+3,nfim-530)
				
					MSBAR('CODE128',1,23.8,cOrdemServ,oPrn,.F.,,.T.,0.042,1.7,,,,.F.)

					oPrn:Box(nLin-10,50,nLin+nPula+5,nfim)
					oPrn:Say(nLin,080,cObsOs,oFont11,100)
					nLin += nPula + nPula
					
					If cTipoMat == 'B'
						oPrn:Say(nLin,080,"ORDEM DE CORTE N�:",oFont10,100)
						oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)
					elseif cTipoMat	== "A"
						oPrn:Say(nLin,080,"ORDEM DE AL�A N�:",oFont10,100)
						oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)						
					ElseIF cTipoMat == "AV"					
						oPrn:Say(nLin,080,"ORDEM DE AVIAMENTOS N�:",oFont10,100)
						oPrn:Say(nLin-5,600,AllTrim(_cOrdem),oFont12n,100)	
					Endif
					
					Cabec1b	:= SubStr(SC2->C2_PRODUTO,1,8) + " - " + AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(SC2->C2_PRODUTO,1,8),"B4_DESC"))
					oPrn:Say(nLin,900,"PRODUTO:",oFont10,100)
					oPrn:Say(nLin,1130,Cabec1b,oFont10n,100)
			
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim-530)

					nLin += nPula + nPula
					For _x := 1 to len(aColCab)
						oPrn:Say(nLin,aColItem[_x],AllTrim(aColCab[_x]),oFont08,100)
					Next _x
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim)

				Endif

				nLin += nPula

			Next _i
			
			
			TMP2->(DbSkip())
			
		enddo
		TMP2->(DbCloseArea())
		
		EndIF
		
				
		nLin += nPula
		oPrn:Box(nLin,50,nLin+3,nfim-450)
		nLin += nPula
 		
 		if  cTipoMat == "AV"
 		
		aColI1      := {080,600,1580,2100}
		
		
		If Select("TMP2") > 0
			TMP2->(DbCloseArea())
		EndIf
		
		cQuery := " SELECT TOP 1 C2_PRODUTO AS PRODUTO FROM "+RetSqlName("SC2")+" WITH (NOLOCK)  "
		cQuery += " JOIN "+RetSqlName("SB1")+" WITH (NOLOCK) ON B1_COD = C2_PRODUTO AND SB1010.D_E_L_E_T_ = '' "
		cQuery += " WHERE C2_NUM = '"+AllTrim(_cOrdem)+"' AND B1_GRUPO IN ("+ GETMV("HP_GRPOPCO") +") AND SC2010.D_E_L_E_T_ = '' AND LEFT(C2_PRODUTO,4) = 'MIAL'  "
		cQuery += " GROUP BY C2_PRODUTO "
		
		
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP2",.T.,.T.)
		DbSelectArea("TMP2")
		TMP2->(DbGoTop())	
		
		
		 cAlca := LEFT(TMP2->PRODUTO,4)
		
		TMP2->(DBCLOSEAREA())	
		
			
			
		If !SC2->(Eof())
		
			oPrn:Say(nLin,aColI1[1],"QUANTIDADES SOLICITADAS POR GRADE E COR",oFont09n,100)
			oPrn:Say(nLin,aColI1[3],"QUANTIDADES PRODUZIDAS",oFont09n,100)
			nLin += nPula
			oPrn:Box(nLin,50,nLin+3,nfim)
			nLin += nPula
			
			aTaman	:= VERTAM(_cOrdem)
			aCores	:= VERCOR(_cOrdem)
			
			aColI1x1 := aColI1[2]
			aColI1x2 := aColI1[4]
			
			For _i := 1 to len(aTaman)
				oPrn:Say(nLin,aColI1x1,AllTrim(aTaman[_i,3]),oFont09n,100)
				aColI1x1 += 120
				oPrn:Say(nLin,aColI1x2,AllTrim(aTaman[_i,3]),oFont09n,100)
				aColI1x2 += 120
			Next _i
			nLin += nPula
	
			For _i := 1 to Len(aCores)
				oPrn:Say(nLin,aColI1[1],aCores[_i,1] +"-"+ SubStr(aCores[_i,2],1,35),oFont09,100)
				oPrn:Say(nLin,aColI1[3],aCores[_i,1] +"-"+ SubStr(aCores[_i,2],1,35),oFont09,100)

				aColI1x1 := aColI1[2]
				aColI1x2 := aColI1[4]
				For _x := 1 to len(aTaman)
					If aTaman[_x,1] == aCores[_i,1]
						oPrn:Say(nLin,aColI1x1,Alltrim(STR(aTaman[_x,4])),oFont09,100)
						aColI1x1 += 120
						oPrn:Say(nLin,aColI1x2,Alltrim(STR(aTaman[_x,5])),oFont09,100)
						aColI1x2 += 120
					End
				next _x
				nLin += nPula
			next _i

			nLin += nPula + nPula
			
			oPrn:Box(nLin-10,50,nLin+nPula+5,nfim)
			oPrn:Say(nLin,080,cObsOs,oFont11,100)
			nLin += nPula + nPula
				
				
			IF !Empty(cRolet)
				oPrn:Say(nLin,080,"ROLETE",oFont10,100)
				nLin += nPula + nPula + nPula
			endif
			
			
			IF !Empty(cAlca)
				oPrn:Say(nLin,080,"ROLETE",oFont10,100)
				nLin += nPula + nPula + nPula
			endif		
			
			aMatPrim	:= VERMTPR(cTipoMat,_cOrdem,cProdCab)
			
			
			aColItem := {050,450,1750,1900,2300,2600,2700}
			aColCab  := {"| CODIGO","| DESCRICAO MATERIA PRIMA","| TAM.","| CORES","| QTD. SOL.","| UN","| QTD. UTILIZADA"}
			For _i := 1 to len(aColCab)
				oPrn:Say(nLin,aColItem[_i],AllTrim(aColCab[_i]),oFont09,100)
			Next _i
			nLin += nPula
			oPrn:Box(nLin,50,nLin+3,nfim)
			nLin += nPula
			
			
			For _i := 1 to len(aMatPrim)
				oPrn:Say(nLin,aColItem[1],"| "+ Alltrim(aMatPrim[_i,1])								,oFont08,100)		//CODIGO
				oPrn:Say(nLin,aColItem[2],"| "+ SubStr(Alltrim(aMatPrim[_i,2]),1,80)				,oFont08,100)		//DESCRICAO
				oPrn:Say(nLin,aColItem[3],"| "+ Alltrim(aMatPrim[_i,4])								,oFont08,100)		//TAMANHO
				oPrn:Say(nLin,aColItem[4],"| "+ SubStr(Alltrim(aMatPrim[_i,6]),1,30)				,oFont08,100)		//COR
				oPrn:Say(nLin,aColItem[5],"| "+ Transform(aMatPrim[_i,8],"@E 999,999.99999")		,oFont08,100)		//QTD. SOLICITADA
				oPrn:Say(nLin,aColItem[6],"| "+ Alltrim(aMatPrim[_i,7])								,oFont08,100)		//UNIDADE
				oPrn:Say(nLin,aColItem[7],"|  "														,oFont08,100)		//QTD. UTILIZADA
					
				If nLin > 2200
					nLin	  := 100
					oPrn:EndPage()
					oPrn:StartPage()
					
					oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)
					
					nLin += nPula + nPula
					oPrn:Box(nLin-30,50,nLin-30+3,nfim-530)
				
					MSBAR('CODE128',1,23.8,cOrdemServ,oPrn,.F.,,.T.,0.042,1.7,,,,.F.)

					oPrn:Box(nLin-10,50,nLin+nPula+5,nfim)
					oPrn:Say(nLin,080,cObsOs,oFont11,100)
					nLin += nPula + nPula
					
					If cTipoMat == 'B'
						oPrn:Say(nLin,080,"ORDEM DE CORTE N�:",oFont10,100)
						oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)
					elseif cTipoMat	== "A"
						oPrn:Say(nLin,080,"ORDEM DE AL�A N�:",oFont10,100)
						oPrn:Say(nLin-5,550,AllTrim(_cOrdem),oFont12n,100)						
					ElseIF cTipoMat == "AV"
					
						oPrn:Say(nLin,080,"ORDEM DE AVIAMENTOS N�:",oFont10,100)
						oPrn:Say(nLin-5,600,AllTrim(_cOrdem),oFont12n,100)	
					Endif
					
					Cabec1b	:= SubStr(SC2->C2_PRODUTO,1,8) + " - " + AllTrim(POSICIONE("SB4",1,xFilial("SB4")+SubStr(SC2->C2_PRODUTO,1,8),"B4_DESC"))
					oPrn:Say(nLin,900,"PRODUTO:",oFont10,100)
					oPrn:Say(nLin,1130,Cabec1b,oFont10n,100)
			
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim-530)

					nLin += nPula + nPula
					For _x := 1 to len(aColCab)
						oPrn:Say(nLin,aColItem[_x],AllTrim(aColCab[_x]),oFont08,100)
					Next _x
					nLin += nPula
					oPrn:Box(nLin,50,nLin+3,nfim)

				Endif

				nLin += nPula

			Next _i
			
		endif
			
			
		ENDIF
		SC2->(dbCloseArea())
		
		oPrn:EndPage()
		
		nLin		:= 80
	

	Next _y
	
	nLin += nPula
	imprp	:= .T.

	RestArea(aArea)
			
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VERCOR   �Autor  �Daniel R. Melo      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para Grupos Utilizados no Relat�rio                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERCOR(_cOrdem)

	Local cEOL			:= +Chr(13)+Chr(10)
	Local cQuery
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	cQuery  := "SELECT SUBSTRING(C2_PRODUTO,9,3) AS COR, SBV1.BV_DESCRI AS DESCCOR "+cEOL
	cQuery  += "FROM "+RetSQLName("SC2")+" SC2 WITH (NOLOCK) "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SB4")+" SB4 WITH (NOLOCK)  ON SB4.D_E_L_E_T_<>'*' AND B4_COD=SUBSTRING(C2_PRODUTO,1,8) "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SBV")+" SBV1 WITH (NOLOCK) ON SBV1.D_E_L_E_T_<>'*' AND SBV1.BV_TABELA=B4_LINHA "+cEOL
	cQuery  += "				AND SBV1.BV_CHAVE=SUBSTRING(C2_PRODUTO,9,3) "+cEOL
	cQuery  += "WHERE SC2.D_E_L_E_T_<>'*' AND C2_FILIAL= '"+xFilial("SC2")+"' "+cEOL
	cQuery  += "	AND C2_SEQPAI= '' AND C2_NUM= '"+AllTrim(_cOrdem)+"' "+cEOL
	cQuery  += "GROUP BY SUBSTRING(C2_PRODUTO,9,3), SBV1.BV_DESCRI "+cEOL

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
		
	While TMP->(!EOF())
		Aadd(aFieldFill, TMP->COR)
		Aadd(aFieldFill, TMP->DESCCOR)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FQUERY1   �Autor  �Daniel R. Melo      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para Grupos Utilizados no Relat�rio                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERTAM(_cOrdem)

	Local cEOL			:= +Chr(13)+Chr(10)
	Local cQuery
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	cQuery  := "SELECT SUBSTRING(C2_PRODUTO,9,3) AS COR, SUBSTRING(C2_PRODUTO,12,4) AS TAM, SBV2.BV_DESCRI AS DESCTAM, "+cEOL
	cQuery  += "			C2_QUANT AS QUANT, C2_QUJE AS QENTREG "+cEOL
	cQuery  += "FROM "+RetSQLName("SC2")+" SC2 "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SB4")+" SB4 WITH (NOLOCK) ON SB4.D_E_L_E_T_<>'*' AND B4_COD=SUBSTRING(C2_PRODUTO,1,8) "+cEOL
	cQuery  += "	INNER JOIN "+RetSQLName("SBV")+" SBV2 WITH (NOLOCK) ON SBV2.D_E_L_E_T_<>'*' AND SBV2.BV_TABELA=B4_COLUNA AND "+cEOL
	cQuery  += "				SBV2.BV_CHAVE=SUBSTRING(C2_PRODUTO,12,4) "+cEOL
	cQuery  += "WHERE SC2.D_E_L_E_T_<>'*' AND C2_FILIAL= '"+xFilial("SC2")+"' "+cEOL
	cQuery  += "	AND C2_SEQPAI= '' AND C2_NUM= '"+AllTrim(_cOrdem)+"' "+cEOL
	cQuery  += "GROUP BY C2_PRODUTO, C2_ITEMGRD, SBV2.BV_DESCRI, C2_QUANT, C2_QUJE "+cEOL
	cQuery  += "ORDER BY C2_ITEMGRD "+cEOL
	
	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf
	
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
		
	While TMP->(!EOF())
		Aadd(aFieldFill, TMP->COR)
		Aadd(aFieldFill, TMP->TAM)
		Aadd(aFieldFill, TMP->DESCTAM)
		Aadd(aFieldFill, TMP->QUANT)
		Aadd(aFieldFill, TMP->QENTREG)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FQUERY1   �Autor  �Daniel R. Melo      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para Grupos Utilizados no Relat�rio                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERMTPR(cTipoMat,_cOrdem,cProdCab)

	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}
	
	
	If Select("TMP1") > 0
		TMP1->(DbCloseArea())
	EndIf
	
	
	If Select("TMP2") > 0
		TMP2->(DbCloseArea())
	EndIf
	
	// TODO  INICIO - QUERY INCLUIDA POR WESKLEY (23/10/2017)
	
	cQuery := " SELECT SD4.D4_COD AS CODIGO,SB1.B1_TIPO AS TIPO, B4_DESC AS DESCRIPRO ,SUBSTRING(D4_COD,12,4) AS TAMANHO, SUM(SD4.D4_QUANT) AS QUANT, ISNULL(SBV2.BV_DESCRI,'') AS DESCTAM, "
	cQuery += " SUBSTRING(D4_COD,9,3) AS COR, SBV1.BV_DESCRI AS DESCCOR, SB1.B1_UM AS UN "
	cQuery += " FROM SD4010 SD4 "
	cQuery += " JOIN "+RetSqlName("SC2")+" SC2 WITH (NOLOCK) ON C2_FILIAL=D4_FILIAL AND D4_PRODUTO=C2_PRODUTO AND D4_OP=C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD AND SC2.D_E_L_E_T_ = '' "
	cQuery += " JOIN "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) ON SB1.B1_COD=SD4.D4_COD AND SB1.D_E_L_E_T_='' "
	cQuery += " JOIN "+RetSqlName("SBM")+" SBM WITH (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' "
	cQuery += " JOIN "+RetSqlName("SB4")+" SB4 WITH (NOLOCK) ON SB4.D_E_L_E_T_<>'*'  AND B4_COD=SUBSTRING(D4_COD,1,8) "
	cQuery += " JOIN "+RetSqlName("SBV")+" SBV1 WITH (NOLOCK) ON SBV1.D_E_L_E_T_<>'*' AND SBV1.BV_TABELA=B4_LINHA AND SBV1.BV_CHAVE=SUBSTRING(D4_COD,9,3) "
	cQuery += " JOIN "+RetSqlName("SBV")+" SBV2 WITH (NOLOCK) ON SBV2.D_E_L_E_T_<>'*' AND SBV2.BV_TABELA=B4_COLUNA AND SBV2.BV_CHAVE=SUBSTRING(D4_COD,12,4) "
	cQuery += " JOIN "+RetSqlName("SB1")+" SB WITH (NOLOCK) ON  SB.B1_COD = SD4.D4_PRODUTO AND SB.D_E_L_E_T_ = '' " 
	cQuery += " JOIN "+RetSqlName("SBM")+" SBM1 WITH (NOLOCK) ON SBM1.BM_GRUPO=SB.B1_GRUPO AND SBM1.D_E_L_E_T_=''  "
	cQuery += " WHERE SD4.D_E_L_E_T_ = '' "
	cQuery += " AND C2_FILIAL= '" +xFilial("SC2") +"' "
	cQuery += " AND C2_NUM = '"+AllTrim(_cOrdem)+"' "
	
	//if cProdCab = 'KT' .OR. cProdCab = 'PI'
	//	cQuery += " AND SD4.D4_OP NOT LIKE '%00100%' " 
	//ELSE 
	//	cQuery += " AND SD4.D4_OP LIKE '%00100%' "
	//ENDIF
	
	cQuery += " AND SB1.B1_TIPO <> 'PI' "
	cQuery += " AND SB.B1_GRUPO NOT IN ("+ GETMV("HP_GRPOP") + ") AND SB1.B1_GRUPO NOT IN ("+ GETMV("HP_GRPOP") + ") "
	cQuery += " GROUP BY SD4.D4_COD,B4_DESC,D4_COD,SBV2.BV_DESCRI,SBV1.BV_DESCRI,SB1.B1_UM,SB1.B1_TIPO "
	cQuery += " HAVING SUM(SD4.D4_QUANT)>0 "
	
	// TODO FIM - QUERY INCLUIDA POR WESKLEY (23/10/2017)
	
	TCQUERY cQuery NEW ALIAS TMP2
	
		
	While TMP2->(!EOF())
		Aadd(aFieldFill, TMP2->CODIGO)
		Aadd(aFieldFill, TMP2->DESCRIPRO)
		Aadd(aFieldFill, TMP2->TAMANHO)
		Aadd(aFieldFill, TMP2->DESCTAM)
		Aadd(aFieldFill, TMP2->COR)
		Aadd(aFieldFill, TMP2->DESCCOR)
		Aadd(aFieldFill, TMP2->UN)
		Aadd(aFieldFill, TMP2->QUANT)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)


Static Function ORDCORTE(cTipoMat,_cOrdem,cProdPri)

	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	If Select("TMP3") > 0
		TMP3->(DbCloseArea())
	EndIf

	cQuery := " SELECT LEFT(SD4.D4_OP,6) AS OP ,SD4.D4_COD AS CODIGO,B4_DESC AS DESCRIPRO,SUBSTRING(D4_COD,12,4) AS TAMANHO ,SUM(SD4.D4_QUANT) AS QUANT,ISNULL(SBV2.BV_DESCRI,'') AS DESCTAM, " 
	cQuery += "	SUBSTRING(D4_COD,9,3) AS COR, SBV1.BV_DESCRI AS DESCCOR, SB.B1_UM AS UN " 
	cQuery += "	FROM "+RetSqlName("SD4")+ " SD4 " 
	cQuery += " JOIN "+RetSqlName("SC2")+" SC2 WITH (NOLOCK) ON C2_FILIAL=D4_FILIAL AND D4_OP=C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD AND C2_PRODUTO = D4_PRODUTO  AND SC2.D_E_L_E_T_ = '' " 
	cQuery += " JOIN "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) ON SB1.B1_COD=SD4.D4_PRODUTO AND SB1.D_E_L_E_T_='' " 
	cQuery += " JOIN "+RetSqlName("SBM")+" SBM WITH (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' " 
	cQuery += " JOIN "+RetSqlName("SB4")+" SB4 WITH (NOLOCK) ON SB4.D_E_L_E_T_ = '' AND B4_COD = SUBSTRING(D4_COD,1,8) " 
	cQuery += " JOIN "+RetSqlName("SBV")+" SBV1 WITH (NOLOCK) ON SBV1.D_E_L_E_T_<>'*' AND SBV1.BV_TABELA=B4_LINHA AND SBV1.BV_CHAVE=SUBSTRING(D4_COD,9,3) " 
	cQuery += " JOIN "+RetSqlName("SBV")+" SBV2 WITH (NOLOCK) ON SBV2.D_E_L_E_T_<>'*' AND SBV2.BV_TABELA=B4_COLUNA AND SBV2.BV_CHAVE=SUBSTRING(D4_COD,12,4) "  
	cQuery += " JOIN "+RetSqlName("SB1")+" SB WITH (NOLOCK) ON  SB.B1_COD = SD4.D4_COD AND SB.D_E_L_E_T_ = '' " 
	cQuery += " WHERE SD4.D_E_L_E_T_='' " 
	cQuery += " AND BM_GRUPO IN ("+ GETMV("HP_GRPOPCO") +") " 
	cQuery += " AND C2_NUM = '"+AllTrim(_cOrdem)+"'  AND LEFT(SD4.D4_PRODUTO,8) = '"+Alltrim(cProdPri)+"'  " 
	cQuery += " GROUP BY LEFT(SD4.D4_OP,6),SD4.D4_COD,SBV2.BV_DESCRI,SBV1.BV_DESCRI,SB.B1_UM,B4_DESC " 
	cQuery += " HAVING SUM(SD4.D4_QUANT)>0 " 


	TCQUERY cQuery NEW ALIAS TMP3
	
	//dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP3",.T.,.T.)
	//DbSelectArea("TMP3")
	//TMP->(DbGoTop())
		
	While TMP3->(!EOF())
		Aadd(aFieldFill, TMP3->CODIGO)
		Aadd(aFieldFill, TMP3->DESCRIPRO)
		Aadd(aFieldFill, TMP3->TAMANHO)
		Aadd(aFieldFill, TMP3->DESCTAM)
		Aadd(aFieldFill, TMP3->COR)
		Aadd(aFieldFill, TMP3->DESCCOR)
		Aadd(aFieldFill, TMP3->UN)
		Aadd(aFieldFill, TMP3->QUANT)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)

Static Function ORDALCA(cTipoMat,_cOrdem,cProdPri)

	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery1     := " "
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	If Select("ALC") > 0
		ALC->(DbCloseArea())
	EndIf

	cQuery1 := " SELECT LEFT(SD4.D4_OP,6) AS OP ,SD4.D4_COD AS CODIGO ,B4_DESC AS DESCRIPRO ,SUM(SD4.D4_QUANT) AS QUANT,SUBSTRING(D4_COD,12,4) AS TAMANHO,ISNULL(SBV2.BV_DESCRI,'') AS DESCTAM, " 
	cQuery1 += "	SUBSTRING(D4_COD,9,3) AS COR, SBV1.BV_DESCRI AS DESCCOR, SB.B1_UM AS UN " 
	cQuery1 += "	FROM "+RetSqlName("SD4")+" SD4 "  
	cQuery1 += " JOIN "+RetSqlName("SC2")+" SC2 WITH (NOLOCK) ON C2_FILIAL=D4_FILIAL AND D4_OP = C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD AND C2_PRODUTO = D4_PRODUTO AND SC2.D_E_L_E_T_ = '' " 
	cQuery1 += "	JOIN "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) ON SB1.B1_COD=SD4.D4_PRODUTO AND SB1.D_E_L_E_T_='' " 
	cQuery1 += " JOIN "+RetSqlName("SBM")+" SBM WITH (NOLOCK) ON SBM.BM_GRUPO=SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' " 
	cQuery1 += " JOIN "+RetSqlName("SB4")+" SB4 WITH (NOLOCK) ON SB4.D_E_L_E_T_ = '' AND B4_COD = SUBSTRING(D4_COD,1,8) " 
	cQuery1 += " JOIN "+RetSqlName("SBV")+" SBV1 WITH (NOLOCK) ON SBV1.D_E_L_E_T_ = '' AND SBV1.BV_TABELA = B4_LINHA AND SBV1.BV_CHAVE = SUBSTRING(D4_COD,9,3) " 
	cQuery1 += " JOIN "+RetSqlName("SBV")+" SBV2 WITH (NOLOCK) ON SBV2.D_E_L_E_T_ = '' AND SBV2.BV_TABELA = B4_COLUNA AND SBV2.BV_CHAVE = SUBSTRING(D4_COD,12,4) " 
	cQuery1 += " JOIN "+RetSqlName("SB1")+" SB WITH (NOLOCK) ON  SB.B1_COD = SD4.D4_COD AND SB.D_E_L_E_T_ = ''  " 
	cQuery1 += " WHERE SD4.D_E_L_E_T_='' " 
	
	if !Empty(Alltrim(cProdPri))
	
		cQuery1  += "  AND SD4.D4_PRODUTO = '"+Alltrim(cProdPri)+"' " 
	
	else 
	
		cQuery1  += " " 
	
	endif

	cQuery1 += "	AND BM_GRUPO IN ("+ GETMV("HP_GRPOPAL") +") " 
	cQuery1 += "	AND C2_NUM = '"+AllTrim(_cOrdem)+"' " 
	cQuery1 += " GROUP BY LEFT(SD4.D4_OP,6),SD4.D4_COD,SBV2.BV_DESCRI,SBV1.BV_DESCRI,SB.B1_UM,B4_DESC " 
	cQuery1 += "	HAVING SUM(SD4.D4_QUANT)>0 " 

	TCQUERY cQuery1 NEW ALIAS ALC
	
		
	While ALC->(!EOF())
		Aadd(aFieldFill, ALC->CODIGO)
		Aadd(aFieldFill, ALC->DESCRIPRO)
		Aadd(aFieldFill, ALC->TAMANHO)
		Aadd(aFieldFill, ALC->DESCTAM)
		Aadd(aFieldFill, ALC->COR)
		Aadd(aFieldFill, ALC->DESCCOR)
		Aadd(aFieldFill, ALC->UN)
		Aadd(aFieldFill, ALC->QUANT)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)

