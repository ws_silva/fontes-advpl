#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL019 
Saldo Por Endere�o
@author Weskley Silva
@since 14/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL021()

Private oReport
Private cPergCont	:= 'HPREL021' 

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'SLE', 'SALDO POR ENDERE�O', , {|oReport| ReportPrint( oReport ), 'SALDO POR ENDERE�O' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'SALDO POR ENDERE�O', { 'SLE', 'SBF','SB1','SBV'})
			
	TRCell():New( oSection1, 'FILIAL'				,'SLE', 		'FILIAL',						    "@!"                        ,10)
	TRCell():New( oSection1, 'ARMAZEN'				,'SLE', 		'ARMAZEN',			  			    "@!"				        ,05)
	TRCell():New( oSection1, 'TIPO'					,'SLE', 		'TIPO',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'GRUPO'				,'SLE', 		'GRUPO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'SKU'					,'SLE', 		'SKU',				  			    "@!"				        ,20)
	TRCell():New( oSection1, 'REFERENCIA'			,'SLE', 		'REFERENCIA',		  			    "@!"				        ,20)
	TRCell():New( oSection1, 'COD_COR'				,'SLE', 		'COD_COR',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DESC_COR'				,'SLE', 		'DESC_COR',			  			    "@!"				        ,25)
	TRCell():New( oSection1, 'TAM'					,'SLE', 		'TAM',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'DESCRICAO'			,'SLE', 		'DESCRICAO',		  			    "@!"				        ,35)
	TRCell():New( oSection1, 'UNID_MED'			    ,'SLE', 		'UNID_MED',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'ENDERECO'				,'SLE', 		'ENDERECO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'SALDO'				,'SLE', 		'SALDO',			     			"@E 999,999.99"			        ,10)
	TRCell():New( oSection1, 'EMPENHO'				,'SLE', 		'EMPENHO',			     			"@E 999,999.99"			        ,10)
			
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 13 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("SLE") > 0
		SLE->(dbCloseArea())
	Endif
      
    cQuery := " SELECT " 
    cQuery += " BF_FILIAL AS FILIAL, "  
    cQuery += " BF_LOCAL AS ARMAZEN, "
    cQuery += " BF_LOCALIZ AS ENDERECO, " 
    cQuery += " BF_PRODUTO AS SKU, "
    cQuery += " B1_UM AS UNID_MED,"
    cQuery += " B1_TIPO AS TIPO, "
    cQuery += " BM_DESC AS GRUPO, "
    cQuery += " LEFT(BF_PRODUTO,8) AS REFERENCIA,"
    cQuery += " B1_DESC AS DESCRICAO,"
    cQuery += " LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AS COD_COR,"
    cQuery += " SBV.BV_DESCRI AS DESC_COR,"
    cQuery += " RIGHT(SB1.B1_COD,4)  AS TAM, "
    cQuery += " BF_QUANT AS SALDO, " 
    cQuery += " (SELECT B2_QEMP FROM "+RetSqlName('SB2')+" (NOLOCK) WHERE B2_COD = BF_PRODUTO AND B2_LOCAL = 'AP' AND SB2010.D_E_L_E_T_ = '' ) AS EMPENHO "
    cQuery += " FROM "+RetSqlName('SBF')+" (NOLOCK) SBF "
    cQuery += " JOIN "+RetSqlName('SB1')+" (NOLOCK) SB1 ON SB1.B1_COD=SBF.BF_PRODUTO AND  SB1.D_E_L_E_T_='' "
    cQuery += " JOIN "+RetSqlName('SBV')+" (NOLOCK) SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SB1.B1_COD,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
    cQuery += " JOIN "+RetSqlName('SBM')+" (NOLOCK) SBM ON SBM.BM_GRUPO=B1_GRUPO AND SBM.D_E_L_E_T_='' "
    cQuery += " WHERE SBF.D_E_L_E_T_='' "
    
    IF !Empty(mv_par01)
    	cQuery += " AND BF_FILIAL = '"+Alltrim(mv_par01)+"' "
    Else 
        cQuery += " "
    Endif
    
    IF !Empty(mv_par02)
    	cQuery += " AND BF_LOCAL = '"+Alltrim(mv_par02)+"' "
    else
    	cQuery += " "
    endif
           
	TCQUERY cQuery NEW ALIAS SLE

	While SLE->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(SLE->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")

		oSection1:Cell("ARMAZEN"):SetValue(SLE->ARMAZEN)
		oSection1:Cell("ARMAZEN"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(SLE->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(SLE->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("SKU"):SetValue(SLE->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(SLE->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COD_COR"):SetValue(SLE->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_COR"):SetValue(SLE->DESC_COR)
		oSection1:Cell("DESC_COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAM"):SetValue(SLE->TAM)
		oSection1:Cell("TAM"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(SLE->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("UNID_MED"):SetValue(SLE->UNID_MED)
		oSection1:Cell("UNID_MED"):SetAlign("LEFT")
		
		oSection1:Cell("ENDERECO"):SetValue(SLE->ENDERECO)
		oSection1:Cell("ENDERECO"):SetAlign("LEFT")
		
		oSection1:Cell("SALDO"):SetValue(SLE->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("EMPENHO"):SetValue(SLE->EMPENHO)
		oSection1:Cell("EMPENHO"):SetAlign("LEFT")
				
		oSection1:PrintLine()
		
		SLE->(DBSKIP()) 
	enddo
	SLE->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 14 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	
	PutSx1(cPergCont, "01","Filial ?"           ,""		,""		,"mv_ch1","C",06,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Armazem ? "	        ,""		,""		,"mv_ch2","C",02,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")

Return
