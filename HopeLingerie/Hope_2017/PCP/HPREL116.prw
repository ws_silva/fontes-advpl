#INCLUDE 'TOTVS.CH'
#INCLUDE 'RWMAKE.CH'
#Include "Topconn.ch"
#Include 'Protheus.ch'
#INCLUDE "AP5MAIL.CH"
#INCLUDE 'FWMVCDEF.CH'




user function HPREL116()

	Local aRet := {}
	Local aPerg := {}

	Private cCadastro := "Follow-up de Compras"

	aAdd(aPerg,{1,"Tipo de Produto"			,Space(2),""							 	,""		,""		,"",0,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Solicita��o" 			,Space(6) ,""							 	,""		,""		,"",0,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Referencia" 				,Space(15) ,""							 	,""		,""		,"",0,.F.}) // Tipo caractere
	aAdd(aPerg,{1,"Data da Solicita��o de" 	,dDataBase,PesqPict("SC1", "C1_EMISSAO")	,'.T.'	,""		,'.T.',50,.F.}) // Tipo data
	aAdd(aPerg,{1,"Data da Solicita��o ate"	,dDataBase,PesqPict("SC1", "C1_EMISSAO")	,'.T.'	,""		,'.T.',50,.F.}) // Tipo data
	aAdd(aPerg,{2,"Status do pedido" 		,"Todos" ,{" ","Aberto","Encerrado","Todos"},75,'.T.',.F.}) // Tipo combo

	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| GeraExc(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
	
Return


STATIC fUNCTION GeraExc(aRet)

	Local oExcel := FWMsExcelEx():New()
	Local cQuery := ""
	Local i := 0
	
	IF Select("FLW") > 0
		FLW->(dbCloseArea())
	Endif
 

	cQuery := "SELECT "
	cQuery += " 	SC7.C7_FILIAL AS FILIAL_PEDIDO, "
	cQuery += " 	SC7.C7_FILENT AS FILIAL_ENTREGA, "
	cQuery += " 	SB1.B1_TIPO AS TIPO, "
	cQuery += " 	SB1.B1_UM AS B1_UM, "	
	cQuery += " 	SB1.B1_GRUPO AS GRUPOCD, "
	cQuery += " 	SBM.BM_DESC AS GRUPODS, "	
	cQuery += " 	SC7.C7_PRODUTO AS SKU, "
	cQuery += " 	SB4.B4_COD AS PRODUTO, "
	cQuery += " 	SB4.B4_DESC AS DESCRICAO, "
	cQuery += " 	SUBSTRING(SC7.C7_PRODUTO,9,3) AS COD_COR, "
	cQuery += " 	SBV.BV_DESCRI AS DESC_COR, "
	cQuery += " 	RIGHT(SC7.C7_PRODUTO,4) AS TAM, "
	cQuery += " 	SB4.B4_UM AS UNID_MEDIDA, "
	cQuery += " 	ISNULL(SC1.C1_NUM,'') AS SOLICITACAO, "
	cQuery += " 	ISNULL(SC1.C1_SOLICIT,'') AS SOLICITANTE, "
	cQuery += " 	ISNULL(SC1.C1_EMISSAO,'') AS DT_SC_EMISSAO, "
	cQuery += " 	ISNULL(SC1.C1_QUANT,'0') AS QTDE_SOLICITADA, "
	cQuery += " 	SC7.C7_NUM AS PEDIDO, "
	cQuery += " 	SC7.C7_CONAPRO AS STATUS_APROVACAO, "
	cQuery += " 	SC7.C7_FORNECE AS COD_FORNECEDOR, "	
	cQuery += " 	SA2.A2_NOME AS FORNECEDOR, "
	cQuery += " 	ISNULL(CTT.CTT_DESC01,'') AS CCUSTO, "
	cQuery += " 	SC7.C7_EMISSAO AS DT_PED_EMISSAO, "
	cQuery += " 	SC7.C7_TOTAL AS VLR_TOTAL_ITEM, "
	cQuery += " 	SC7.C7_QUANT AS QTDE_PEDIDO, "
	cQuery += " 	SC7.C7_DATPRF AS DATA_ENTREGA, "
	cQuery += " 	SC7.C7_QUJE AS QTDE_ENTREGUE, "
	cQuery += " 	(SC7.C7_QUANT - SC7.C7_QUJE) AS SALDO, "
	cQuery += " 	(SC7.C7_TOTAL / SC7.C7_QUANT) AS CUSTO, "
	cQuery += " 	SC7.C7_RESIDUO AS QUITADO "
	cQuery += " FROM SC7010 SC7 "
	cQuery += " 	LEFT JOIN SC1010 SC1 ON SC7.C7_NUMSC = SC1.C1_NUM AND SC7.C7_ITEMSC=SC1.C1_ITEM AND SC7.C7_FILIAL = SC1.C1_FILIAL AND SC1.D_E_L_E_T_=''  "
	cQuery += " 	LEFT JOIN CTT010 CTT ON CTT.CTT_CUSTO = SC7.C7_CC AND CTT.D_E_L_E_T_='' "
	cQuery += " 	LEFT JOIN SB4010 SB4 ON SB4.B4_COD=LEFT(SC7.C7_PRODUTO,8)AND SB4.D_E_L_E_T_='' "
	cQuery += " 	LEFT JOIN SA2010 SA2 ON SA2.A2_COD=SC7.C7_FORNECE "
	cQuery += " 	LEFT JOIN SBV010 SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC7.C7_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
	cQuery += " 	LEFT JOIN SB1010 SB1 ON SB1.B1_COD = SC7.C7_PRODUTO AND SB1.D_E_L_E_T_='' "
	cQuery += " 	LEFT JOIN SBM010 SBM ON SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' "	
 	cQuery += " WHERE  "
	cQuery += " 	 SC7.D_E_L_E_T_=''  "
	
	if !Empty(aRet[1])
		cQuery += " AND B1_TIPO = '"+Alltrim(aRet[1])+"'  "
	endif
	
	if !Empty(aRet[2])
		cQuery += " AND SC1.C1_NUM = '"+Alltrim(aRet[2])+"' "
	endif
	
	if !Empty(aRet[3]) 
		cQuery += " AND LEFT(SC7.C7_PRODUTO,8) = LEFT('"+Alltrim(aRet[3])+"',8)"
	endif
	
	if !Empty(aRet[4])
		cQuery += " AND SC7.C7_EMISSAO BETWEEN '"+ DTOS(aRet[4]) +"' AND '"+ DTOS(aRet[5]) +"' "
	endif
	
	if !Empty(aRet[6])
	
		DO CASE
		  CASE Alltrim(aRet[6]) == "Aberto" 
		    cQuery += "  AND (SC7.C7_QUANT-SC7.C7_QUJE)>0 AND SC7.C7_RESIDUO=''"
		  CASE Alltrim(aRet[6])	== "Encerrado" 
		    cQuery += " AND SC7.C7_QUJE>=C7_QUANT or SC7.C7_RESIDUO<>'' OR C7_ENCER<>''"
		ENDCASE
	endif
	

	cQuery += " UNION ALL SELECT SC1.C1_FILIAL AS FILIAL_PEDIDO, "
	cQuery += "  SC1.C1_FILENT AS FILIAL_ENTREGA, "
	cQuery += "  SB1.B1_TIPO AS TIPO, "
	cQuery += "  SB1.B1_UM AS B1_UM, "
	cQuery += "  SB1.B1_GRUPO AS GRUPOCD, "
	cQuery += "  SBM.BM_DESC AS GRUPODS, "
	cQuery += "  SC1.C1_PRODUTO AS SKU, "
	cQuery += "  SB4.B4_COD AS PRODUTO, "
	cQuery += "  SB4.B4_DESC AS DESCRICAO, "
	cQuery += "  SUBSTRING(SC1.C1_PRODUTO,9,3) AS COD_COR, "
	cQuery += "  SBV.BV_DESCRI AS DESC_COR, "
	cQuery += "  RIGHT(SC1.C1_PRODUTO,4) AS TAM, "
	cQuery += "  SB4.B4_UM AS UNID_MEDIDA, "
	cQuery += "  ISNULL(SC1.C1_NUM,'') AS SOLICITACAO, "
	cQuery += "  ISNULL(SC1.C1_SOLICIT,'') AS SOLICITANTE, "
	cQuery += "  ISNULL(SC1.C1_EMISSAO,'') AS DT_SC_EMISSAO, "
	cQuery += "  ISNULL(SC1.C1_QUANT,'0') AS QTDE_SOLICITADA, "
	cQuery += "  '' AS PEDIDO, "
	cQuery += "  C1_APROV AS STATUS_APROVACAO, "
	cQuery += "  '' AS COD_FORNECEDOR, "
	cQuery += "  '' AS FORNECEDOR, "
	cQuery += "  ISNULL(CTT.CTT_DESC01,'') AS CCUSTO, "
	cQuery += "  '' AS DT_PED_EMISSAO, "
	cQuery += "  '' AS VLR_TOTAL_ITEM, "
	cQuery += "  0 AS QTDE_PEDIDO, "
	cQuery += "  SC1.C1_DATPRF AS DATA_ENTREGA, "
	cQuery += "  0 AS QTDE_ENTREGUE, "
	cQuery += "  (SC1.C1_QUANT - SC1.C1_QUJE) AS SALDO, "
	cQuery += "  (SC1.C1_TOTAL / SC1.C1_QUANT) AS CUSTO, "
	cQuery += "  SC1.C1_RESIDUO AS QUITADO "
	cQuery += "  FROM SC1010 SC1 "
	cQuery += "  LEFT JOIN CTT010 CTT ON CTT.CTT_CUSTO = SC1.C1_CC AND CTT.D_E_L_E_T_='' "
	cQuery += "  LEFT JOIN SB4010 SB4 ON SB4.B4_COD=LEFT(SC1.C1_PRODUTO,8) AND SB4.D_E_L_E_T_='' "
	cQuery += "  LEFT JOIN SBV010 SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC1.C1_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "
	cQuery += "  LEFT JOIN SB1010 SB1 ON SB1.B1_COD = SC1.C1_PRODUTO AND SB1.D_E_L_E_T_='' "
	cQuery += "  LEFT JOIN SBM010 SBM ON SBM.BM_GRUPO = SB1.B1_GRUPO AND SBM.D_E_L_E_T_='' "
	cQuery += "  WHERE SC1.D_E_L_E_T_='' AND C1_NUM+C1_ITEM NOT IN (SELECT DISTINCT C7_NUMSC+C7_ITEMSC FROM SC7010 WHERE D_E_L_E_T_='') "
	
	if !Empty(aRet[1])
		cQuery += " AND B1_TIPO = '"+Alltrim(aRet[1])+"'  "
	endif
		
	
	if !Empty(aRet[2])
		cQuery += " AND SC1.C1_NUM = '"+Alltrim(aRet[2])+"' "
	endif		  


	if !Empty(aRet[3]) 
		cQuery += " AND LEFT(SC1.C1_PRODUTO,8) = LEFT('"+Alltrim(aRet[3])+"',8)"
	endif
	
	if !Empty(aRet[4])
		cQuery += " AND SC1.C1_EMISSAO BETWEEN '"+ DTOS(aRet[4]) +"' AND '"+ DTOS(aRet[5]) +"' "
	endif
	
	if !Empty(aRet[6])
	
		DO CASE
		  CASE Alltrim(aRet[6]) == "Aberto" 
		    cQuery += " AND C1_APROV <> 'R'"
		  CASE Alltrim(aRet[6]) == "Encerrado" 
		    cQuery += " AND 0 = 1"		    
		ENDCASE
	endif	
	
	
	
	TCQUERY cQuery NEW ALIAS FLW
	
	memowrite("c:\temp\followup.sql", cQuery)
	
	cRel := "Follow-up"
	cRelTit := "Follow-up de Compras"
	oExcel:AddworkSheet(cRel)
	oExcel:AddTable(cRel,cRelTit)
	oExcel:AddColumn(cRel,cRelTit,"FILIAL_PEDIDO",1)
	oExcel:AddColumn(cRel,cRelTit,"FILIAL_ENTREGA",1)
	oExcel:AddColumn(cRel,cRelTit,"TIPO",1)
	oExcel:AddColumn(cRel,cRelTit,"SKU",1)

	oExcel:AddColumn(cRel,cRelTit,"COD_GRUPO",1)
	oExcel:AddColumn(cRel,cRelTit,"DESCRICAO_GRUPO",1)
	
	oExcel:AddColumn(cRel,cRelTit,"PRODUTO",1)
	oExcel:AddColumn(cRel,cRelTit,"DESCRICAO",1)
	oExcel:AddColumn(cRel,cRelTit,"COD_COR",1)
	oExcel:AddColumn(cRel,cRelTit,"DESC_COR",1)
	oExcel:AddColumn(cRel,cRelTit,"TAM",1)
	oExcel:AddColumn(cRel,cRelTit,"SOLICITACAO",1)
	oExcel:AddColumn(cRel,cRelTit,"SOLICITANTE",1)
	oExcel:AddColumn(cRel,cRelTit,"DT_EMISSAO_SC",1)
	oExcel:AddColumn(cRel,cRelTit,"QTDE_SOLICITADA",3)
	oExcel:AddColumn(cRel,cRelTit,"PEDIDO",1)
	oExcel:AddColumn(cRel,cRelTit,"STATUS_APROVACAO",1)
	oExcel:AddColumn(cRel,cRelTit,"COD_FORNECEDOR",1)	
	oExcel:AddColumn(cRel,cRelTit,"FORNECEDOR",1)
	oExcel:AddColumn(cRel,cRelTit,"CCUSTO",1)
	oExcel:AddColumn(cRel,cRelTit,"DT_EMISSAO_PED",1)
	oExcel:AddColumn(cRel,cRelTit,"VLR_TOTAL_ITEM",3)
	oExcel:AddColumn(cRel,cRelTit,"QTDE_PEDIDO",3)
	oExcel:AddColumn(cRel,cRelTit,"UNIDADE",1)
	oExcel:AddColumn(cRel,cRelTit,"DATA_ENTREGA",1)
	oExcel:AddColumn(cRel,cRelTit,"QTDE_ENTREGUE",3)
	oExcel:AddColumn(cRel,cRelTit,"SALDO",3)
	oExcel:AddColumn(cRel,cRelTit,"CUSTO",3)
	oExcel:AddColumn(cRel,cRelTit,"STATUS",1)
	oExcel:AddColumn(cRel,cRelTit,"QUITADO",1)	
	
	
	While FLW->(!EOF())
		
		i++
		incproc()
		oExcel:AddRow(cRel,cRelTit,;
					{ FLW->FILIAL_PEDIDO,;
					  FLW->FILIAL_ENTREGA,;
					  FLW->TIPO,;
					  FLW->SKU,;
					  FLW->GRUPOCD,;
					  FLW->GRUPODS,;
					  FLW->PRODUTO,;
					  FLW->DESCRICAO,;
					  FLW->COD_COR,;
					  FLW->DESC_COR,;
					  FLW->TAM,;
					  FLW->SOLICITACAO,;
					  FLW->SOLICITANTE,;
					  DTOC(STOD(FLW->DT_SC_EMISSAO)),;
					  Transform(FLW->QTDE_SOLICITADA,"@E 9,999,999.9999"),;
					  FLW->PEDIDO,;
					  FLW->STATUS_APROVACAO,;
					  FLW->COD_FORNECEDOR,;
					  FLW->FORNECEDOR,;
					  FLW->CCUSTO,;
					  DTOC(STOD(FLW->DT_PED_EMISSAO)),;
					  Transform(FLW->VLR_TOTAL_ITEM,"@E 9,999,999.9999"),;
					  Transform(FLW->QTDE_PEDIDO,"@E 9,999,999.9999"),;
					  FLW->B1_UM,;
					  DTOC(STOD(FLW->DATA_ENTREGA)),;
					  Transform(FLW->QTDE_ENTREGUE,"@E 9,999,999.9999"),;
					  IIF(ALLTRIM(FLW->QUITADO) == "S",0,Transform(FLW->SALDO,"@E 9,999,999.9999")),;
					  Transform(FLW->CUSTO,"@E 9,999,999.9999"),;
					  IIF(FLW->SALDO <= 0 .OR. FLW->QUITADO == "S","Encerrado","Aberto"),;
					  FLW->QUITADO	})
		FLW->(DBSKIP()) 
		
	End Do
	
	cFileName = "c:\temp\HPREL116_"+DTOS(DDATABASE)+"_"+REPLACE(TIME(),":","")+".xml"
	oExcel:Activate()
	oExcel:GetXMLFile(cFileName)
	
	oExcelApp := MsExcel():New()
	oExcelApp:WorkBooks:Open(cFileName) // Abre uma planilha
	oExcelApp:SetVisible(.T.)
	oExcelApp:Destroy()		

RETURN

