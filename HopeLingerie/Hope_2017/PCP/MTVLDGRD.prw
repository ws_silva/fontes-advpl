#Include 'Protheus.ch'

User Function MTVLDGRD()

Local lRet := .T. 
Local cProduto := PARAMIXB[1]
_area := GetArea()
DbSelectArea("SB1")
_areaSB1 := GetArea()
DbSetOrder(1)
DbSeek(xfilial("SB1")+cProduto)

If Found()
	lRet := U_HFATP20A(SB1->B1_COD,"OPCORTE",.F.)  
	Alert("Produto "+cproduto+" n�o permite abrir OP!")                     
Else
	alert("Produto "+cproduto+" n�o est� cadastrado!")
	lRet := .F.
Endif

RestArea(_areaSB1)
RestArea(_area)

Return lRet
