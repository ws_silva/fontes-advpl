#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function HPCPP010()
                        
Local oButton1
Local oButton2
Local oGet2
Local oGet3
Local oGet4
Local oGet5
Local oGroup1
Local oGroup2
Local oSay1
Local oSay2
Local oSay3
Local oSay4
Local oSay5
Local oSay6
Local oSay7
Local oSay8
Local oSay9
Local oSay10
Local oSay11
Local oSay12
Local oSay13
Local oSay14
Local oSay15
Local oSay16
Local oSay17
Local oSay18
Local oSay19
Local oSay20
Local oSay21
Local oSay22
Local oSay23
Local oSay24
Local oSay25
Local oSay26
Local oSay27
Local oSay28
Local oSay29
Local oSay30
Local oSay31
Local oSay32
Private oGet1
Private oGet6
Private oRadMenu1
Private nRadMenu1 := 0
Private nComboBo1 := "Piking"
Private cGet1 := space(15)
Private cGet2 := space(15)
Private cGet3 := space(40)
Private cGet4 := space(15)
Private cGet5 := space(4)
Private cGet6 := space(15)
Private cGet7 := 0
Private cGet8 := space(2)
Private cGet10:= Space(2)
Private cGet9 := Space(15)

Static oDlg

  DEFINE MSDIALOG oDlg TITLE "Tranferencia de Endere�o" FROM 000, 000  TO 475, 1100 COLORS 0, 16777215 PIXEL

    @ 012, 010 SAY oSay1   PROMPT "Recipiente:" 				SIZE 033, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 010, 049 MSGET oGet1 VAR cGet1 							SIZE 116, 010 OF oDlg VALID _x := Atutela(1,Right(alltrim(cGet1),6),0) COLORS 0, 16777215 PIXEL
    @ 031, 009 SAY oSay2   PROMPT "Codigo:" 					SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 028, 041 MSGET oGet2 VAR cGet2 							SIZE 068, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    @ 030, 129 SAY oSay3   PROMPT "Descricao:" 				SIZE 030, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 028, 161 MSGET oGet3 VAR cGet3 							SIZE 188, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    @ 030, 363 SAY oSay4   PROMPT "Cor:" 						SIZE 019, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 028, 388 MSGET oGet4 VAR cGet4 							SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
    @ 030, 457 SAY oSay5   PROMPT "Tamanho:" 					SIZE 028, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 028, 491 MSGET oGet5 VAR cGet5 							SIZE 047, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

    @ 047, 009 GROUP oGroup1 TO 108, 539 PROMPT " Endere�o de Origem " 	OF oDlg COLOR 0, 16777215 PIXEL
    @ 061, 014 SAY oSay6   PROMPT "Selecionado: " 			SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 061, 055 SAY oSay7   PROMPT "Cod Selecionado" 			SIZE 077, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 061, 120 MSGET oGet9 VAR cGet9							SIZE 047, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

    @ 076, 014 SAY oSay8   PROMPT "Quantidade:" 				SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 076, 056 SAY oSay9   PROMPT "Qtd" 						SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 076, 120 MSGET oGet5 VAR cGet7 							SIZE 047, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL

//    @ 076, 169 SAY oSay10  PROMPT "Qtd Reserva:" 			SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 076, 215 SAY oSay11  PROMPT "Qtd Resv" 					SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 076, 340 SAY oSay12  PROMPT "Qtd Disponivel:" 			SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 076, 386 SAY oSay13  PROMPT "Qtd Resv" 					SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 092, 014 SAY oSay14  PROMPT "Recipiente:" 				SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 092, 056 SAY oSay15  PROMPT "Qtd" 						SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 092, 169 SAY oSay16  PROMPT "Ordem de Corte:" 			SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 092, 215 SAY oSay17  PROMPT "Qtd Resv" 					SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 092, 386 SAY oSay19  PROMPT "Qtd Resv" 					SIZE 118, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 092, 340 SAY oSay18  PROMPT "Vencimento:" 				SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL

    @ 125, 010 SAY oSay20  PROMPT "Transferir Para:" 		SIZE 055, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 120, 071 RADIO oRadMenu1 VAR nRadMenu1 ITEMS "Piking","Aereo" 			SIZE 137, 019 OF oDlg COLOR 0, 16777215 PIXEL
//    @ 041, 051 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"Item 1","Item 2"} SIZE 072, 010 OF oDlg VALID teste() COLORS 0, 16777215 ON CHANGE bchange() PIXEL
    @ 120, 071 MSCOMBOBOX oComboBo1 VAR nComboBo1 ITEMS {"Piking","Aereo"} SIZE 072, 010 OF oDlg VALID Atutela(2,cGet2,nComboBo1) COLORS 0, 16777215 PIXEL    
    
    @ 148, 011 SAY oSay21  PROMPT "Endereco Destino:" 		SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 146, 073 MSGET oGet6 VAR cGet6 							SIZE 116, 010 OF oDlg VALID u_vld1sbe(cGet6,nComboBo1) COLORS 0, 16777215 PIXEL
    
    @ 163, 009 GROUP oGroup2 TO 209, 539 PROMPT " Endere�o de Destino " 	OF oDlg COLOR 0, 16777215 PIXEL
//    @ 177, 015 SAY oSay22  PROMPT "Piking Caixa:" 			SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 177, 057 SAY oSay23  PROMPT "Qtd" 						SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 177, 170 SAY oSay24  PROMPT "Peso:" 					SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 177, 196 SAY oSay25  PROMPT "Peso" 						SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 193, 015 SAY oSay26  PROMPT "Unidades:" 				SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 193, 057 SAY oSay27  PROMPT "Unidades" 					SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 193, 170 SAY oSay28  PROMPT "Caixa:" 					SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
//    @ 193, 196 SAY oSay29  PROMPT "Caixa" 					SIZE 043, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 217, 011 SAY oSay31  PROMPT "Usu�rio:" 					SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
    @ 217, 052 SAY oSay32  PROMPT cUserName 					SIZE 075, 007 OF oDlg COLORS 0, 16777215 PIXEL
    
    @ 216, 495 BUTTON oButton1 PROMPT "Transferir" 			SIZE 043, 012 ACTION (Save(cGet2,cGet7,cGet8,cGet10,cGet9,cGet6)) OF oDlg PIXEL
    @ 216, 446 BUTTON oButton2 PROMPT "Cancelar" 			SIZE 043, 012 ACTION oDlg:End() OF oDlg PIXEL

	//oRadMenu1:bSetGet := {|u|Iif (Atutela(2,cGet2,nRadMenu1)==0,nRadMenu1,nRadMenu1)}
	//oRadMenu1:bSetGet := {|u|Atutela(2,cGet2,nRadMenu1)}

  ACTIVATE MSDIALOG oDlg CENTERED

Return

Static Function Atutela(ntipo, Codigo,nradio)

_RET := 0
If nTipo = 1
	_qry := "Select * from "+RetSqlname("SBF")+" where D_E_L_E_T_ = '' and BF_NUMLOTE = '"+Codigo+"' "
	_Qry := ChangeQuery(_qry)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSBF",.T.,.T.)
	
	DbSelectArea("TMPSBF")
	DbGoTop()
	
	DbselectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+TMPSBF->BF_PRODUTO)
	
	cGet2 := TMPSBF->BF_PRODUTO
	cGet3 := SB1->B1_DESC
	cGet4 := SUBSTRING(TMPSBF->BF_PRODUTO,9,3)
	cGet5 := SUBSTRING(TMPSBF->BF_PRODUTO,12,4)
	cGet7 := TMPSBF->BF_QUANT
	cGet8 := TMPSBF->BF_LOCAL
	cGet9 := TMPSBF->BF_LOCALIZ	
	oDlg:refresh()
	
	DbSelectArea("TMPSBF")
	DbCloseArea()
	Atutela(2,cGet2,nComboBo1)
	oGet6:setfocus()
Else
	_RET := 0
	If nRadio = "Piking"
		DbSelectArea("SBE")
		DbSetOrder(10)
		DbSeek(xfilial("SBE")+Codigo+"E0")
		
		cGet6 := SBE->BE_LOCALIZ
		cGet10:= "E0"
		oDlg:Refresh()
	Else
		cGet10:= "E1"
	Endif
Endif

Return _RET
				
Static Function Save(cProd,nQuant,cAmzOri,cAmzDest, cEndOri, cEndDest)
Local cDoc      := ""
Local lContinua := .F.
Local aLnTran   := {}
Local aAgrupa   := {}
Local cQry      := ""
//Local cEndOri   := ""
//Local cEndDest  := ""
Local cUM       := ""
Local cDescri   := ""
Local cSPadrao  := AllTrim(SuperGetMV("MV_XSUBPAD",.F.,"0"))

Local nTransf   := 0
Local aTransf   := {}
Local cTime     := ""
Local cDtTime   := ""
Local nx,ny     := 0
Local nOpcAuto  := 3 // Indica qual tipo de a��o ser� tomada (Inclus�o/Exclus�o)
Local dData     := DDATABASE
Local cHora     := SubStr(Time(),1,5)
Local dDtValid  := DDATABASE
Local cDocSeq   := ""
Local nStatus   := 0
//Local cSubPad   := AllTrim(SuperGetMV("MV_XSUBPAD",.F.,"0"))

Private lMsHelpAuto := .T.
Private lMsErroAuto := .F.

DbSelectArea("SB1")
DbSetOrder(1)
DbSeek(xFilial("SB1")+cProd)

cQry := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,BF_LOCALIZ "
cQry += CRLF + "from "+RetSqlName("SBF")+" SBF "
cQry += CRLF + "where SBF.D_E_L_E_T_ = '' "
cQry += CRLF + "and BF_PRODUTO = '"+cProd+"' "
cQry += CRLF + "and BF_LOCAL   = '"+cAmzOri+"' "
cQry += CRLF + "and BF_LOCALIZ = '"+cEndOri+"' "
cQry += CRLF + "and BF_NUMLOTE = '"+Right(alltrim(cGet1),6)+"' "
cQry += CRLF + "and BF_QUANT > 0 "
cQry += CRLF + "order by BF_PRODUTO,BF_NUMLOTE,BF_LOTECTL "

MemoWrite("HPCPP010.txt",cQry)
    
cQry := ChangeQuery(cQry)

DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQry),"QRY",.F.,.T.)

DbSelectArea("QRY")

nTransf := QRY->BF_QUANT

If QRY->(!EOF())
	While QRY->(!EOF())
		If nTransf > 0
			cEndOri := QRY->BF_LOCALIZ				
			//                     1         2              3        4       5       6        7            8                 9      10       11
			aAdd(aTransf,{QRY->BF_PRODUTO,SB1->B1_DESC,SB1->B1_UM,cAmzOri,cEndOri,cAmzDest,cEndDest,QRY->BF_LOTECTL,QRY->BF_NUMLOTE,"",QRY->BF_QUANT})
			
			nTransf := nTransf - QRY->BF_QUANT
			
			lContinua := .T.
		Else
			EXIT
		EndIf
		
		QRY->(DbSkip())
	EndDo
EndIf

QRY->(DbCloseArea()) 

If lContinua
	cTrnDAP := GetSxENum("SD3","D3_DOC",1)
	cDoc := cTrnDAP
	
	For nx := 1 to Len(aTransf)
		If aTransf[nx][11] < 0
			MsgAlert("N�o � poss�vel realizar uma transfer�ncia com valor negativo.","Aten��o")
			Loop
		EndIf
		
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xFilial("SB1")+aTransf[nx][1])
		
		cTpConv := SB1->B1_TIPCONV
		nFtConv := SB1->B1_CONV
		cSegUm  := SB1->B1_SEGUM
		
		DbSelectArea("SB2")
		DbSetOrder(1)
		DbSeek(xFilial("SB2")+aTransf[nx][1]+aTransf[nx][4])
		
		cDocSeq := PROXNUM(.F.) 
		If GetMV("MV_DOCSEQ") <> cDocSeq 
		   PutMV("MV_DOCSEQ",cDocSeq) 
		EndIf
		
		//Begin Transaction

		If Alltrim(nComboBo1) == "Piking"
			cLocal   := "E0"
			aTransf[nx][6] := "E0"
			cSubPad  := cSPadrao
		Else
			DbSelectArea("SBE")
			DbSetOrder(10)
			DbSeek(xfilial("SBE")+alltrim(cGet2)+"E0")
			
			If Found() .and. alltrim(SBE->BE_LOCALIZ) = alltrim(cGet6)
				cLocal   := "E0"
				aTransf[nx][6] := "E0"
				cSubPad := cSPadrao
			Else
				cLocal   := "E1"
				aTransf[nx][6] := "E1"
				cSubPad := aTransf[nx][9]
			Endif
		End
	
  		lTransfSBF := AtuSBF(aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][9],aTransf[nx][5],aTransf[nx][7],cTpConv,nFtConv)
  		
		DbSelectArea("SB2")
		DbSetOrder(2)
		DbSeek(xFilial("SB2")+aTransf[nx][4]+aTransf[nx][1])
		nSldAtu := SB2->B2_QATU		
		nSldAlt := nSldAtu - aTransf[nx][11]
		
		nSld2 := 0
		If !Empty(cTpConv)
			If cTpConv = "M"
				nSld2 := nSldAlt * nFtConv
			Else
				nSld2 := nSldAlt / nFtConv
			EndIf
		EndIf
		
		RecLock("SB2",.F.)
			SB2->B2_QATU    := nSldAlt
			SB2->B2_QTSEGUM := nSld2 
		MsUnlock()
		
		//Armazem Destino
		DbSelectArea("SB2")
		DbSetOrder(2)
		DbSeek(xFilial("SB2")+aTransf[nx][6]+aTransf[nx][1])
		nSldAtu := SB2->B2_QATU		
		nSldAlt := nSldAtu + aTransf[nx][11]
		
		nSld2 := 0
		If !Empty(cTpConv)
			If cTpConv = "M"
				nSld2 := nSldAlt * nFtConv
			Else
				nSld2 := nSldAlt / nFtConv
			EndIf
		EndIf
		
		RecLock("SB2",.F.)
			SB2->B2_QATU := nSldAlt
			SB2->B2_QTSEGUM := nSld2
		MsUnlock()

		lTransfSB8 := AtuSB8(cDoc,aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][9],cTpConv,nFtConv)
		
		nQtd2 := 0
		If !Empty(cTpConv)
			If cTpConv = "M"
				nQtd2 := aTransf[nx][11] * nFtConv
			Else
				nQtd2 := aTransf[nx][11] / nFtConv
			EndIf
		EndIf
		
		//Grava SD3 Transferencia
		//Requisicao
		DbSelectArea("SD3")
		RecLock("SD3",.T.)
			SD3->D3_FILIAL  := xFilial("SD3")
			SD3->D3_TM      := "999"
			SD3->D3_COD     := aTransf[nx][1]
			SD3->D3_UM      := aTransf[nx][3]
			SD3->D3_QUANT   := aTransf[nx][11]
			SD3->D3_QTSEGUM := nQtd2
			SD3->D3_SEGUM   := cSegUm
			SD3->D3_CF      := "RE4"
			SD3->D3_CONTA   := SB1->B1_CONTA
			SD3->D3_LOCAL   := aTransf[nx][4]
			SD3->D3_DOC     := cDoc
			SD3->D3_EMISSAO := DDATABASE
			SD3->D3_GRUPO   := SB1->B1_GRUPO
			SD3->D3_CUSTO1  := SB2->B2_CM1
			SD3->D3_NUMSEQ  := cDocseq
			SD3->D3_TIPO    := SB1->B1_TIPO
			SD3->D3_USUARIO := UsrRetName(__cUserID)
			SD3->D3_CHAVE   := "E0"
			SD3->D3_LOTECTL := aTransf[nx][8]
			SD3->D3_NUMLOTE := aTransf[nx][9]
			SD3->D3_DTVALID := dDtValid
			SD3->D3_LOCALIZ := aTransf[nx][5]
		MsUnlock()
		
		//Devolucao
		RecLock("SD3",.T.)
			SD3->D3_FILIAL  := xFilial("SD3")
			SD3->D3_TM      := "499"
			SD3->D3_COD     := aTransf[nx][1]
			SD3->D3_UM      := aTransf[nx][3]
			SD3->D3_QUANT   := aTransf[nx][11]
			SD3->D3_QTSEGUM := nQtd2
			SD3->D3_SEGUM   := cSegUm
			SD3->D3_CF      := "DE4"
			SD3->D3_CONTA   := SB1->B1_CONTA
			SD3->D3_LOCAL   := cLocal
			SD3->D3_DOC     := cDoc
			SD3->D3_EMISSAO := DDATABASE
			SD3->D3_GRUPO   := SB1->B1_GRUPO
			SD3->D3_CUSTO1  := SB2->B2_CM1
			SD3->D3_NUMSEQ  := cDocSeq
			SD3->D3_TIPO    := SB1->B1_TIPO
			SD3->D3_USUARIO := UsrRetName(__cUserID)
			SD3->D3_CHAVE   := "E9"
			SD3->D3_LOTECTL := aTransf[nx][8]
			SD3->D3_NUMLOTE := cSubPad
			SD3->D3_DTVALID := dDtValid
			SD3->D3_LOCALIZ := aTransf[nx][7]
		MsUnlock()
		
		cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
		ConfirmSX8()
		
		DbSelectArea("SDB")
		RecLock("SDB",.T.)
			SDB->DB_FILIAL  := xFilial("SDB")
			SDB->DB_ITEM    := "0001"
			SDB->DB_PRODUTO := aTransf[nx][1]
			SDB->DB_LOCAL   := aTransf[nx][4]
			SDB->DB_LOCALIZ := aTransf[nx][5]
			SDB->DB_DOC     := cDoc
			SDB->DB_TM      := "999"
			SDB->DB_ORIGEM  := "SD3"
			SDB->DB_QUANT   := aTransf[nx][11]
			SDB->DB_QTSEGUM := nQtd2
			SDB->DB_DATA    := DDATABASE
			SDB->DB_LOTECTL := aTransf[nx][8]
			SDB->DB_NUMLOTE := aTransf[nx][9]
			SDB->DB_NUMSEQ  := cDocseq
			SDB->DB_TIPO    := "M"
			SDB->DB_SERVIC  := "999"
			SDB->DB_ATIVID  := "ZZZ"
			SDB->DB_HRINI   := SubStr(Time(),1,5)
			SDB->DB_ATUEST  := "S"
			SDB->DB_STATUS  := "M"
			SDB->DB_ORDATIV := "ZZ"
			SDB->DB_IDOPERA := cNumIDOper 
		MsUnlock()
		
		cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
		ConfirmSX8()
		
		RecLock("SDB",.T.)
			SDB->DB_FILIAL  := xFilial("SDB")
			SDB->DB_ITEM    := "0001"
			SDB->DB_PRODUTO := aTransf[nx][1]
			SDB->DB_LOCAL   := aTransf[nx][6]
			SDB->DB_LOCALIZ := aTransf[nx][7]
			SDB->DB_DOC     := cDoc
			SDB->DB_TM      := "499"
			SDB->DB_ORIGEM  := "SD3"
			SDB->DB_QUANT   := aTransf[nx][11]
			SDB->DB_QTSEGUM := nQtd2
			SDB->DB_DATA    := DDATABASE
			SDB->DB_LOTECTL := aTransf[nx][8]
			SDB->DB_NUMLOTE := cSubPad
			SDB->DB_NUMSEQ  := cDocseq
			SDB->DB_TIPO    := "M"
			SDB->DB_SERVIC  := "499"
			SDB->DB_ATIVID  := "ZZZ"
			SDB->DB_HRINI   := SubStr(Time(),1,5)
			SDB->DB_ATUEST  := "S"
			SDB->DB_STATUS  := "M"
			SDB->DB_ORDATIV := "ZZ"
			SDB->DB_IDOPERA := cNumIDOper 
		MsUnlock()
			
		DbSelectArea("SD5")
		RecLock("SD5",.T.)
			SD5->D5_FILIAL  := xFilial("SD5")
			SD5->D5_PRODUTO := aTransf[nx][1]
			SD5->D5_LOCAL   := aTransf[nx][4]
			SD5->D5_DOC     := cDoc
			SD5->D5_DATA    := DDATABASE
			SD5->D5_ORIGLAN := "999"
			SD5->D5_NUMSEQ  := cDocseq
			SD5->D5_QUANT   := aTransf[nx][11]
			SD5->D5_QTSEGUM := nQtd2
			SD5->D5_LOTECTL := aTransf[nx][8]
			SD5->D5_NUMLOTE := aTransf[nx][9]
			SD5->D5_DTVALID := dDtValid
		MsUnlock()
		
		RecLock("SD5",.T.)
			SD5->D5_FILIAL  := xFilial("SD5")
			SD5->D5_PRODUTO := aTransf[nx][1]
			SD5->D5_LOCAL   := aTransf[nx][6]
			SD5->D5_DOC     := cDoc
			SD5->D5_DATA    := DDATABASE
			SD5->D5_ORIGLAN := "499"
			SD5->D5_NUMSEQ  := cDocseq
			SD5->D5_QUANT   := aTransf[nx][11]
			SD5->D5_QTSEGUM := nQtd2
			SD5->D5_LOTECTL := aTransf[nx][8]
			SD5->D5_NUMLOTE := cSubPad
			SD5->D5_DTVALID := dDtValid
		MsUnlock()
		
		//End Transaction
		
	Next
	
	cPrxTrn := SOMA1(cTrnDAP,6)
	MsgInfo("Transfer�ncia realizada com Sucesso!","Aviso")
Else
	MsgAlert("SBF - Dados n�o encontrados.","Aten��o")		
EndIf

cGet1 := space(15)
cGet2 := space(15)
cGet3 := space(40)
cGet4 := space(15)
cGet5 := space(4)
cGet6 := space(15)
cGet7 := 0
cGet8 := space(2)
cGet10:= Space(2)
cGet9 := Space(15)
//nComboBo1 := 0
oGet1:setfocus()
oDlg:refresh()

Return

Static Function AtuSBF(cPrdSBF,cAOriSBF,cADesSBF,cLotSBF,cSubSBF,nQtdSBF,cSubOri,cESBFOrig,cESBFDest,cFator,nFator)
Local cQuery  := ""
Local nRecNo  := 0
Local nSldSBF := 0
Local lRet    := .T.

//Atualiza Saldo do amazem de origem
cQuery := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SBF")+" SBF "
cQuery += CRLF + "where BF_FILIAL  = '"+xFilial("SBF")+"' "
cQuery += CRLF + "and   BF_PRODUTO = '"+cPrdSBF+"' "
cQuery += CRLF + "and   BF_LOCAL   = '"+cAOriSBF+"' "
cQuery += CRLF + "and   BF_LOTECTL = '"+cLotSBF+"' "
cQuery += CRLF + "and   BF_NUMLOTE = '"+cSubOri+"' "
cQuery += CRLF + "and   BF_LOCALIZ = '"+cESBFOrig+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSBF_Orig.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSBF", .F., .T.)

DbSelectArea("TMPSBF")

If TMPSBF->(!EOF())
	nRecNo  := TMPSBF->R_E_C_N_O_
	nSldSBF := TMPSBF->BF_QUANT - nQtdSBF
	//nSldSBF := IF(nSldSBF < 0,0,nSldSBF)
	
	nSldSBF2 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSBF2 := nSldSBF * nFator
		Else
			nSldSBF2 := nSldSBF / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SBF")+" SET BF_QUANT = "+AllTrim(Str(nSldSBF))+" , BF_QTSEGUM = "+AllTrim(Str(nSldSBF2))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSBF_Orig_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
	
	If nSldSBF = 0
		cUpdate := "UPDATE "+RetSqlName("SBF")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_, BF_QUANT = 0 , BF_QTSEGUM = 0 WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
		MemoWrite("HFATA010_AtuSBF_Orig_Delete.txt",cUpdate)
		nStatus := TCSQLEXEC(cUpdate)
	EndIf
	
EndIf

TMPSBF->(DbCloseArea())

//Atualiza Saldo do armazem de destino
cQuery := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SBF")+" SBF "
cQuery += CRLF + "where BF_FILIAL  = '"+xFilial("SBF")+"' "
cQuery += CRLF + "and   BF_PRODUTO = '"+cPrdSBF+"' "
cQuery += CRLF + "and   BF_LOCAL   = '"+cADesSBF+"' "
cQuery += CRLF + "and   BF_LOTECTL = '"+cLotSBF+"' "
cQuery += CRLF + "and   BF_NUMLOTE = '"+cSubSBF+"' "
cQuery += CRLF + "and   BF_LOCALIZ = '"+cESBFDest+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSBF_Dest.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSBF", .F., .T.)

DbSelectArea("TMPSBF")
DbGoTop()

If TMPSBF->(!EOF())
	nRecNo  := TMPSBF->R_E_C_N_O_
	nSldSBF := TMPSBF->BF_QUANT + nQtdSBF
	
	nSldSBF2 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSBF2 := nSldSBF * nFator
		Else
			nSldSBF2 := nSldSBF / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SBF")+" SET BF_QUANT = "+AllTrim(Str(nSldSBF))+" , BF_QTSEGUM = "+AllTrim(Str(nSldSBF2))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSBF_Dest_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
Else
	nSldSBF2 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSBF2 := nQtdSBF * nFator
		Else
			nSldSBF2 := nQtdSBF / nFator
		EndIf
	EndIf
	
	RecLock("SBF",.T.)
		SBF->BF_FILIAL  := xFilial("SBF")
		SBF->BF_PRODUTO := cPrdSBF
		SBF->BF_LOCAL   := cADesSBF
		SBF->BF_LOCALIZ := cESBFDest
		SBF->BF_LOTECTL := cLotSBF
		SBF->BF_NUMLOTE := cSubSBF
		SBF->BF_QUANT   := nQtdSBF
		SBF->BF_QTSEGUM := nSldSBF2
	MsUnlock()
EndIf

TMPSBF->(DbCloseArea())

Return lRet

Static Function AtuSB8(cDocument,cPrdSB8,cAOriSB8,cADesSB8,cLotSB8,cSubSB8,nQtdSB8,cSubOri,cFator,nFator)
Local cQuery  := ""
Local nRecNo  := 0
Local nSldSB8 := 0
Local lRet    := .T.

//Atualiza Saldo do amazem de origem
cQuery := "select B8_PRODUTO,B8_LOCAL,B8_SALDO,B8_LOTECTL,B8_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SB8")+" SB8 "
cQuery += CRLF + "where B8_FILIAL  = '"+xFilial("SB8")+"' "
cQuery += CRLF + "and   B8_PRODUTO = '"+cPrdSB8+"' "
cQuery += CRLF + "and   B8_LOCAL   = '"+cAOriSB8+"' "
cQuery += CRLF + "and   B8_LOTECTL = '"+cLotSB8+"' "
cQuery += CRLF + "and   B8_NUMLOTE = '"+cSubOri+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSB8_Orig.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSB8", .F., .T.)

DbSelectArea("TMPSB8")

If TMPSB8->(!EOF())
	nRecNo  := TMPSB8->R_E_C_N_O_
	nSldSB8 := TMPSB8->B8_SALDO - nQtdSB8
	//nSldSB8 := IF(nSldSB8 < 0,0,nSldSB8)
	
	nSldSB82 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSB82 := nSldSB8 * nFator
		Else
			nSldSB82 := nSldSB8 / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SB8")+" SET B8_DOC = '"+cDocument+"' , B8_SALDO = "+AllTrim(Str(nSldSB8))+" , B8_SALDO2 = "+AllTrim(Str(nSldSB82))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSB8_Dest_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
EndIf

TMPSB8->(DbCloseArea())

//Atualiza Saldo do armazem de destino
cQuery := "select B8_PRODUTO,B8_LOCAL,B8_SALDO,B8_LOTECTL,B8_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SB8")+" SB8 "
cQuery += CRLF + "where B8_FILIAL  = '"+xFilial("SB8")+"' "
cQuery += CRLF + "and   B8_PRODUTO = '"+cPrdSB8+"' "
cQuery += CRLF + "and   B8_LOCAL   = '"+cADesSB8+"' "
cQuery += CRLF + "and   B8_LOTECTL = '"+cLotSB8+"' "
cQuery += CRLF + "and   B8_NUMLOTE = '"+cSubSB8+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSB8_Dest.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSB8", .F., .T.)

DbSelectArea("TMPSB8")

If TMPSB8->(!EOF())
	nRecNo  := TMPSB8->R_E_C_N_O_
	nSldSB8 := TMPSB8->B8_SALDO + nQtdSB8
	
	nSldSB82 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSB82 := nSldSB8 * nFator
		Else
			nSldSB82 := nSldSB8 / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SB8")+" SET B8_DOC = '"+cDocument+"' , B8_SALDO = "+AllTrim(Str(nSldSB8))+" , B8_SALDO2 = "+AllTrim(Str(nSldSB82))+" , B8_QTDORI = "+AllTrim(Str(nSldSB8))+" , B8_QTDORI2 = "+AllTrim(Str(nSldSB82))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSB8_Dest_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
Else
	nSldSB82 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSB82 := nQtdSB8 * nFator
		Else
			nSldSB82 := nQtdSB8 / nFator
		EndIf
	EndIf
	
	RecLock("SB8",.T.)
		SB8->B8_FILIAL  := xFilial("SB8")
		SB8->B8_QTDORI  := nQtdSB8
		SB8->B8_QTDORI2 := nSldSB82
		SB8->B8_PRODUTO := cPrdSB8
		SB8->B8_LOCAL   := cADesSB8
		SB8->B8_DATA    := DDATABASE
		SB8->B8_DTVALID := StoD('20491231')
		SB8->B8_SALDO   := nQtdSB8
		SB8->B8_SALDO2  := nSldSB82
		SB8->B8_ORIGLAN := 'MI' //Movimento Interno
		SB8->B8_LOTECTL := cLotSB8
		SB8->B8_NUMLOTE := cSubSB8
		SB8->B8_DOC     := cDocument
		SB8->B8_DFABRIC := DDATABASE
	MsUnlock()
EndIf

TMPSB8->(DbCloseArea())

Return lRet

user function vld1sbe(_Get6,_ComboBo1)
	lret := .T.
	_qry := "Select BE_LOCALIZ from "+RetSqlName("SBE")+" where D_E_L_E_T_ = '' and BE_XCODEND = '"+_Get6+"'"
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSBE",.T.,.T.)
	
	DbSelectArea("TMPSBE")
	DbGoTOP()
	If !EOF()
		_Get6 := TMPSBE->BE_LOCALIZ
		cGet6 := TMPSBE->BE_LOCALIZ
		oDlg:Refresh()
	Endif
	DbCloseArea()
	
	lret := u_vldsbe(_Get6,_ComboBo1)
Return(lret)