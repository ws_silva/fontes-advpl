#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function SEQTESTE()
Local cSeq := "01"
Local nx := 0

PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' TABLES 'ZTS' MODULO 'FAT'

DbSelectArea("ZTS")

For nx := 1 to 1000
	RecLock("ZTS",.T.)
	ZTS->ZTS_SEQ := cSeq
	MsUnlock()
	cSeq := SOMA1(cSeq,2)
Next 

RESET ENVIRONMENT

Return

