// #########################################################################################
// Projeto: Hope
// Modulo : Estoque
// Fonte  : HESTG001
// ---------+-------------------+-----------------------------------------------------------
// Date     | Author             | Description
// ---------+-------------------+-----------------------------------------------------------
// 17/10/16 | Actual Trend      | Gatilho de c�digo inteligente
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

user function HESTG001(_ini)
	_area := GetArea()
	_cod := ""
	
	IF !SubStr(_ini,1,2) $ "KT|PF|ME|PI|SV|MO|"
	
		cQuery := "select Top 1 B4_COD from "+RetSqlName("SB4")+" SB4 "
		cQuery += "where SB4.D_E_L_E_T_ = '' "
		cQuery += "and B4_COD like '"+alltrim(_ini)+"%' "
		cQuery += "order by B4_COD desc "
    
		cQuery := ChangeQuery(cQuery)

		If Select("TMP3") > 0                                 
    		TMP3->(dbclosearea())
		EndIf
	    
		DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),"TMP3",.F.,.F.)       
	
		DbSelectArea("TMP3")
		DbGoTop()
		If EOF()
			_cod := _ini+"0001"
		Else
			_cod := _ini+Soma1(SubStr(TMP3->B4_COD,5,4))
		Endif

   		TMP3->(dbclosearea())
   	Endif
	RestArea(_area)
return(_cod)
