#include "rwmake.ch"
#include "protheus.ch"
#INCLUDE "TBICONN.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    �RETIQ001  � Autor �  Bruno Parreira       � Data � 23/11/16 ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao � Imprime etiquetas                                          ���
���          �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Uso       � HOPE                                                       ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/         

User Function HETIQ002()
Private cPerg := "HETIQ002"

AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
	Return
EndIf

//���������������������������������������������������������������������Ŀ
//� Montagem da tela                                                    �
//�����������������������������������������������������������������������
    @ 200,001 TO 380,380 DIALOG oDlg TITLE OemToAnsi("Impress�o de Etiquetas")
    @ 001,001 Say OemToAnsi(" Este programa imprimi etiquetas de produtos")
    @ 002,001 Say OemToAnsi(" de acordo com os  par�metros  definidos  pelo  ")
    @ 003,001 Say OemToAnsi(" usu�rio. ")
    
    @ 75,098 BMPBUTTON TYPE 01 ACTION ImprEtiq()
    @ 75,128 BMPBUTTON TYPE 02 ACTION Close(oDlg)
    @ 75,158 BMPBUTTON TYPE 05 ACTION Pergunte(cPerg,.T.)

    Activate Dialog oDlg Centered

Return

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    � ImprEtiq � Autor � Mauro Nagata          � Data � 02/06/16 ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao � Imprime as etiquetas                                       ���
�������������������������������������������������������������������������Ĵ��
���Uso       � TChristina                                                 ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ImprEtiq()

//���������������������������������������������������������������������Ŀ
//� Prepara a porta para impressao                                      �
//�����������������������������������������������������������������������

If MsgYesNo("A etiqueta est� posicionada e a impressora ligada?","Confirma��o")
	RptStatus({|| ImprRun() })
EndIf

Return(nil)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun�ao    � ImprRun  � Autor � Mauro Nagata          � Data � 02/06/16 ���
�������������������������������������������������������������������������Ĵ��
���Descri�ao � Executando a impressao                                     ���
�������������������������������������������������������������������������Ĵ��
���Uso       � AP                                                         ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

Static Function ImprRun()
Local cQry := ""

cQry := "SELECT C2_NUM,C2_ITEM,C2_SEQUEN,C2_PRODUTO,C2_QUANT, B1_CODBAR "
cQry += CRLF + "FROM "+RetSqlName("SC2")+" SC2  "
cQry += CRLF + "JOIN "+RetSqlName("SB1")+" SB1  "
cQry += CRLF + "ON (B1_COD = C2_PRODUTO)"
cQry += CRLF + "WHERE SC2.D_E_L_E_T_ = '' "
cQry += CRLF + "AND C2_NUM = '"+substr(mv_par01,1,6)+"' "
cQry += CRLF + "AND C2_ITEM = '"+substr(mv_par01,7,2)+"' "
cQry += CRLF + "AND C2_SEQUEN = '"+substr(mv_par01,9,3)+"' "
cQry += CRLF + "AND C2_ITEMGRD = '"+substr(mv_par01,12,3)+"' "
cQry += CRLF + "AND SB1.D_E_L_E_T_ = ''" 


dbUseArea( .T., "TOPCONN", TcGenQry(,,cQry), "QRY", .T., .F. )
dbSelectArea("QRY")
dbGoTop()

SetRegua( RecCount() )

ZebraEtq()

Set Device To Screen
Set Printer To

Return

/*/
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ���
����Fun�ao    � ZebraEtq � Autor � Mauro Nagata          � Data � 07/06/16 ����
��������������������������������������������������������������������������Ĵ���
����Descri�ao � Imprime n etiquetas de codigo de barras conforme solicitado����
����          � nos parametros.                                            ����
��������������������������������������������������������������������������Ĵ���
����Uso       �                                                            ����
���������������������������������������������������������������������������ٱ��
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
/*/                                              
Static Function ZebraEtq()
LOCAL cModelo,cPorta
Local nCont:=0
Local cCliente:=""
Local cTransp:=""
Local cResult := ""
Local cEspaco := SUPERGETMV("MV_XESPACO",.F.,"0,5") //Define espa�o para etiqueta de produto acabado 

cPorta	:= "LPT1" 
cModelo := "ZEBRA"

MSCBPRINTER(cModelo,cPorta,,,.F.,,,,,,.F.,)
MSCBCHKStatus(.F.)

/* Etiqueta 40 mm x 25 mm 
                                                           
                   (x1,y1)                                 
                          \                                
                           +--------------------------------+                                 
                       --> | REF: 99999   COR: 9999 | 9     |
                       --> | BR 99  US 99B  EUR 99B | 9     |
                       --> | |||||||||||||||||||||| | 9     |
                       --> |    9999999999999999    | 9     |
                           +--------------------------------+
                       --> | REF: 99999   COR: 9999 | 9     |
                       --> | BR 99  US 99B  EUR 99B | 9     |
                       --> | |||||||||||||||||||||| | 9     |
                       --> |    9999999999999999    | 9     |
                           +--------------------------------+                                 
                                                             \
                                                              (x2, y2)
*/ 

DbSelectArea("QRY")
DbGoTop()
Do While QRY->(!EOF())
	nCont    := nCont+1
	nQtdEtiq := QRY->C2_QUANT
    MscbBEGIN(nQtdEtiq,2)
    
    MSCBWRITE(CRLF+"^LH"+cEspaco)
    MSCBWRITE(CRLF+"^FO16,16^ABN,01,01^FH_^FDREF:"+SubStr(QRY->C2_PRODUTO,1,8)+"^FS")
	MSCBWRITE(CRLF+"^FO160,16^ABN,01,01^FH_^FDCOR:"+SubStr(QRY->C2_PRODUTO,9,3)+"^FS")
	MSCBWRITE(CRLF+"^FO16,40^ABN,01,01^FH_^FDBR 38^FS")
	MSCBWRITE(CRLF+"^FO96,40^ABN,01,01^FH_^FDUS 30B^FS")
	MSCBWRITE(CRLF+"^FO176,40^ABN,01,01^FH_^FDEUR 65B^FS")
	MSCBWRITE(CRLF+"^FO32,56^BY2,2,20^BEN, 20,Y,N^FD"+AllTrim(QRY->B1_CODBAR)+"^FS")
	MSCBWRITE(CRLF+"^FO264,48^ABB,01,01^FH_^FD"+AllTrim(Str(Month(DDATABASE)))+SubStr(AllTrim(Str(Year(DDATABASE))),3,2)+"^FS")
	MSCBWRITE(CRLF+"^FO16,120^ABN,01,01^FH_^FDREF:"+SubStr(QRY->C2_PRODUTO,1,8)+"^FS")
	MSCBWRITE(CRLF+"^FO160,120^ABN,01,01^FH_^FDCOR:"+SubStr(QRY->C2_PRODUTO,9,3)+"^FS")
	MSCBWRITE(CRLF+"^FO16,144^ABN,01,01^FH_^FDBR 38^FS")
	MSCBWRITE(CRLF+"^FO96,144^ABN,01,01^FH_^FDUS 30B^FS")
	MSCBWRITE(CRLF+"^FO176,144^ABN,01,01^FH_^FDEUR 65B^FS")
	MSCBWRITE(CRLF+"^FO32,160^BY2,2,20^BEN, 20,Y,N^FD"+AllTrim(QRY->B1_CODBAR)+"^FS")
	MSCBWRITE(CRLF+"^FO288,112^ABB,01,01^FH_^FDREF"+SubStr(QRY->C2_PRODUTO,1,8)+"^FS")
    
	/*MscbSAY(02,02,"REF:"+SubStr(QRY->B1_COD,1,8),"N","B","01,01") 
	MscbSAY(20,02,"COR:"+SubStr(QRY->B1_COD,9,3),"N","B","01,01") 
	MscbSAY(02,05,"BR 38","N","B","01,01")
	MscbSAY(12,05,"US 30B","N","B","01,01")
	MscbSAY(22,05,"EUR 65B","N","B","01,01")
	MscbSAYBAR(04,07,AllTrim(QRY->B1_CODBAR),"N","MB04",2.5,.F.,.T.,.F.,,1.5,2,.F.,.F.,"3",.T.)
	MscbSAY(33,06,AllTrim(Str(Month(DDATABASE)))+SubStr(AllTrim(Str(Year(DDATABASE))),3,2),"B","B","01,01")
	
	MscbSAY(02,15,"REF:"+SubStr(QRY->B1_COD,1,8),"N","B","01,01") 
	MscbSAY(20,15,"COR:"+SubStr(QRY->B1_COD,9,3),"N","B","01,01") 
	MscbSAY(02,18,"BR 38","N","B","01,01")
	MscbSAY(12,18,"US 30B","N","B","01,01")
	MscbSAY(22,18,"EUR 65B","N","B","01,01")
	MscbSAYBAR(04,20,AllTrim(QRY->B1_CODBAR),"N","MB04",2.5,.F.,.T.,.F.,,1.5,2,.F.,.F.,"3",.T.)
	MscbSAY(36,14,"REF"+SubStr(QRY->B1_COD,1,8),"B","B","01,01")
*/
	cResult := MscbEnd() 
 	
 	QRY->(dbskip()) 	
EndDo        

MemoWrite("C:\TEMP\HETIQ002.txt",cResult)         
  
MscbClosePrinter()

DbCloseArea()
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
  
PutSx1(cPerg,"01","OP ?           ","OP ?           ","OP ?           ","Mv_ch1",TAMSX3("D3_OP"    )[3],TAMSX3("D3_OP"    )[1],TAMSX3("D3_OP"    )[2],0,"G","",   "SC2","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o numero da OP.    ",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)