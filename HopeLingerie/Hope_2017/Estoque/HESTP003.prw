#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

User Function HESTP003

_qry := "sELECT * FROM SD7010 WHERE D_E_L_E_T_ = '' AND D7_LIBERA <> 'S' " 

dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSDA",.T.,.T.)

DbselectArea("TMPSDA")
DbGoTop()

While !EOF()
	
		aLibera := {}

		DbSelectArea("SD7")
		DbSetOrder(1)
		DbGoTop()

		dbSeek(xFilial()+TMPSDA->D7_NUMERO+TMPSDA->D7_PRODUTO+TMPSDA->D7_LOCAL)

		aAdd(aLibera,{  {"D7_TIPO" ,1 ,Nil},; //D7_TIPO
        	            {"D7_DATA" ,DDATABASE,Nil},; //D7_DATA
            	        {"D7_QTDE" ,TMPSDA->D7_SALDO ,Nil},; //D7_QTDE
                	    {"D7_OBS" ,"" ,Nil},; //D7_OBS
                    	{"D7_QTSEGUM" ,0 ,Nil},; //D7_QTSEGUM
	                    {"D7_MOTREJE" ,"" ,Nil},; //D7_MOTREJE
    	                {"D7_LOCDEST" ,TMPSDA->D7_LOCDEST ,Nil},; //D7_LOCDEST
        	            {"D7_SALDO" ,NIL ,Nil},; //D7_SALDO
            	        {"D7_ESTORNO" ,NIL ,Nil}}) //D7_ESTORNO

		lMsErroAuto := .F.

		MSExecAuto({|x,y| mata175(x,y)},aLibera,4)


		IF lMsErroAuto
//     		Alert("Erro")
//     		MOSTRAERRO()
		Endif

		DBSELECTAREA("TMPSDA")
		DBSKIP()
	END
	
DBCLOSEAREA()

RETURN