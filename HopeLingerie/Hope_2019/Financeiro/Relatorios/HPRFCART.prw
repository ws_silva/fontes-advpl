#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"  
#INCLUDE "RWMAKE.CH"  

/*/{Protheus.doc} RF013 
Relat髍io de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User function HPRFCART()
Local aArea := GetArea()
If MsgYesNo("Confirma a impress鉶 do Relat髍io de devolu珲es? ")
	Processa({|| U_HPFATCAR()})
Endif
RestArea(aArea)
Return


User Function HPFATCAR()

	Private oReport
	Private cPergCont	:= 'HPRFCARTAO' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf
	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________ 
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'HPFATCAR', 'RELA敲O FATURAMENTO POR CARTAO DE CR蒁ITO ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELA敲O FATURAMENTO POR CARTAO DE CR蒁ITO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'FATURAMENTO POR TITULOS ', { 'XREL', 'SA1','SE1','SC5','SF2','SD2'})
//CLIENTE	NOME	NOTA_FISCAL	EMISSAO	VALOR	COND_PGTO	PEDIDO1	PEDIDO2	TIPO_PGTO	XNU	AUTORIZ	QTDE_PARCELAS	VLR_PARC

   	TRCell():New( oSection1,'CLIENTE'	    ,'XREL','CLIENTE'       , "@!"              ,10)
   	TRCell():New( oSection1,'NOME'	        ,'XREL','NOME'          , "@!"              ,10)
   	TRCell():New( oSection1,'NOTA_FISCAL'	,'XREL','NOTA_FISCAL'   , "@!"              ,10)
   	TRCell():New( oSection1,'EMISSAO'	    ,'XREL','EMISSAO'       , "@D"              ,08) 
   	TRCell():New( oSection1,'VALOR'	        ,'XREL','VALOR'         , "@E 99,999,999.99",13)
   	TRCell():New( oSection1,'COND_PGTO'  	,'XREL','COND_PGTO'     , "@!"              ,10)
   	TRCell():New( oSection1,'PEDIDO1'	    ,'XREL','PEDIDO1'       , "@!"              ,10)
   	TRCell():New( oSection1,'PEDIDO2'	    ,'XREL','PEDIDO2'       , "@!"              ,10)
   	TRCell():New( oSection1,'TIPO_PGTO'	    ,'XREL','TIPO_PGTO'     , "@!"              ,10)
   	TRCell():New( oSection1,'XNU'	        ,'XREL','XNU'           , "@!"              ,10)
  	TRCell():New( oSection1,'AUTORIZ'	    ,'XREL','AUTORIZ'       , "@!"              ,10)
   	TRCell():New( oSection1,'QTDE_PARCELAS'	,'XREL','QTDE_PARCELAS' , "@!"              ,10)
   	TRCell():New( oSection1,'VLR_PARC'     	,'XREL','VLR_PARC'      , "@E 99,999,999.99",13)


Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
 @since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
    LocaL lApaga := .f.
	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	                    
   
	IF Select("xREL") > 0
		xREL->(dbCloseArea())
	Endif             

 	cQuery := " SELECT F2_CLIENTE AS CLIENTE,A1_NOME AS NOME,F2_DOC     AS NOTA_FISCAL, "
	cQuery += " SUBSTRING(F2_EMISSAO,7,2)+'/'+SUBSTRING(F2_EMISSAO,5,2)+'/'+SUBSTRING(F2_EMISSAO,3,2)  AS EMISSAO "
	cQuery += " ,F2_VALBRUT AS VALOR,F2_COND    AS COND_PGTO,"
	cQuery += " E1_XPEDRAK AS PEDIDO1,E1_XPEDWEB AS PEDIDO2,E1_XTPPAG  AS TIPO_PGTO,E1_XNSU    AS XNU,E1_XAUTNSU AS AUTORIZ, "
	cQuery += " E4_XNUMPAR AS QTDE_PARCELAS,E1_VALOR   AS VLR_PARC "
	cQuery += " FROM "+RetSqlName("SF2")+" F2, "+RetSqlName("SE1")+" E1, "+RetSqlName("SE4")+" E4, "+RetSqlName("SA1")+" A1 WITH (NOLOCK) "
	cQuery += " WHERE F2_SERIE+F2_DOC+F2_CLIENTE+F2_LOJA = E1_PREFIXO+E1_NUM+E1_CLIENTE+E1_LOJA AND E4_CODIGO = F2_COND " 
	cQuery += " AND F2.D_E_L_E_T_ <> '*' AND E1.D_E_L_E_T_ <> '*' AND E4.D_E_L_E_T_ <> '*' AND A1.D_E_L_E_T_ <> '*'  "
	cQuery += " AND (E1_PARCELA = '' OR E1_PARCELA = 'A') AND E1_XTPPAG LIKE '%"+upper(MV_PAR03)+"%' "
	cQuery += " AND F2_EMISSAO BETWEEN '"+DtoS(MV_PAR01)+"' AND '"+DtoS(MV_PAR02)+"' " 
	cQuery += " AND F2_CLIENTE+F2_LOJA = A1_COD+A1_LOJA "
	cQuery += " ORDER BY  F2_DOC " 
    TCQUERY cQuery NEW ALIAS xREL 
    xREL->(DBGOTOP())
    
  While xREL->(!EOF())      
	
	  IF oReport:Cancel()
		 Exit
	  EndIf
	
	  oReport:IncMeter()
	  oReport:IncMeter()

		oSection1:Cell("CLIENTE"):SetValue(xREL->CLIENTE)	
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")

		oSection1:Cell("NOME"):SetValue(xREL->NOME)
		oSection1:Cell("NOME"):SetAlign("LEFT")

		oSection1:Cell("NOTA_FISCAL"):SetValue(xREL->NOTA_FISCAL)	
		oSection1:Cell("NOTA_FISCAL"):SetAlign("LEFT")

		oSection1:Cell("EMISSAO"):SetValue(xREL->EMISSAO)	
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")

		oSection1:Cell("VALOR"):SetValue(xREL->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")

		oSection1:Cell("COND_PGTO"):SetValue(xREL->COND_PGTO)	
		oSection1:Cell("COND_PGTO"):SetAlign("LEFT")

		oSection1:Cell("PEDIDO1"):SetValue(xREL->PEDIDO1)	
		oSection1:Cell("PEDIDO1"):SetAlign("LEFT")

		oSection1:Cell("PEDIDO2"):SetValue(xREL->PEDIDO2)
		oSection1:Cell("PEDIDO2"):SetAlign("LEFT")

		oSection1:Cell("TIPO_PGTO"):SetValue(xREL->TIPO_PGTO)	
		oSection1:Cell("TIPO_PGTO"):SetAlign("LEFT")

		oSection1:Cell("XNU"):SetValue(xREL->XNU)
		oSection1:Cell("XNU"):SetAlign("LEFT")

		oSection1:Cell("AUTORIZ"):SetValue(xREL->AUTORIZ)
		oSection1:Cell("AUTORIZ"):SetAlign("LEFT")

		oSection1:Cell("QTDE_PARCELAS"):SetValue(xREL->QTDE_PARCELAS)	
		oSection1:Cell("QTDE_PARCELAS"):SetAlign("LEFT")

		oSection1:Cell("VLR_PARC"):SetValue(xREL->VLR_PARC)
		oSection1:Cell("VLR_PARC"):SetAlign("LEFT")
 	
		oSection1:PrintLine()
		
		xREL->(DBSKIP()) 
	enddo
	xREL->(DBCLOSEAREA())
Return( Nil )


/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Verifica/cria SX1 a partir de matriz para verificacao          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Especifico para Clientes Microsiga                             潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

	Aadd(aPergs,{"Dt Emiss.de? ","","","mv_ch1","D",08,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Dt Emiss.ate?","","","mv_ch2","D",08,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
    Aadd(aPergs,{"Tp Pagto    ?","","","mv_ch3","C",06,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})

dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next
