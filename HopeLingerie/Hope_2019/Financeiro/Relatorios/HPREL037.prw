#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL037 
Relat�rio de reten��es
@author Ronaldo Pereira
@since 18/10/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
user function HPREL037()

Private oReport
Private cPergCont	:= 'HPREL037' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Ronaldo Pereira
@since 18 de Outubro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
	
Static Function ReportDef()

	Local oReport
	Local oSection1

	oReport := TReport():New( 'RDR', 'RELAT�RIO DE RETEN��ES', , {|oReport| ReportPrint( oReport ), 'RELAT�RIO DE RETEN��ES' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELAT�RIO DE RETEN��ES', {'RDE','SE2'})
	
	TRCell():New( oSection1, 'FILIAL'	    	    ,'RDR', 		'FILIAL',							"@!"                        ,04)		
	TRCell():New( oSection1, 'EMISSAO'	    	    ,'RDR', 		'EMISSAO',							"@!"                        ,15)
	TRCell():New( oSection1, 'VENCIMENTO'			,'RDR', 		'ENTRADA',       					"@!"		                ,15)
	TRCell():New( oSection1, 'TITULO'				,'RDR', 	    'NFISCAL',       					"@!"		                ,09)
	TRCell():New( oSection1, 'COD_FORNECE' 		    ,'RDR', 		'COD_FORNECE',              		"@!"						,08)
	TRCell():New( oSection1, 'FORNECEDOR' 		    ,'RDR', 		'FORNECEDOR',              			"@!"						,45)
	TRCell():New( oSection1, 'PARCELA'         		,'RDR', 		'PARCELA',			           		"@!"						,03)
	TRCell():New( oSection1, 'VALOR' 				,'RDR', 	    'VALOR',     						"@E 999,99.99"		    	,15)
	TRCell():New( oSection1, 'TIPO' 				,'RDR', 	    'TIPO',     						"@!"		    		    ,06)
	TRCell():New( oSection1, 'NATUREZA'      		,'RDR', 		'NATUREZA',      					"@!"				    	,15)
		
Return( oReport )
	
//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Ronaldo Pereira
@since 18 de Outubro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("RDR") > 0
		RDE->(dbCloseArea())
	Endif     
    
    cQuery := " SELECT E2_MSFIL AS FILIAL, "
    cQuery += " 	   CONVERT(CHAR, CAST(E2_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQuery += "        CONVERT(CHAR, CAST(E2_VENCTO AS SMALLDATETIME), 103) AS VENCIMENTO, "
    cQuery += "        E2_NUM AS TITULO, "
    cQuery += "        E2_FORNECE AS COD_FORNECE, "
    cQuery += "        E2_NOMFOR AS FORNECEDOR, "
    cQuery += "        E2_PARCELA AS PARCELA, "
    cQuery += "        E2_VALOR AS VALOR, "
    cQuery += "        E2_TIPO AS TIPO, "
    cQuery += "        E2_NATUREZ AS NATUREZA "
    cQuery += " FROM "+RetSqlName("SE2")+" WITH (NOLOCK) "
    cQuery += " WHERE D_E_L_E_T_ = '' "
    cQuery += "   AND E2_NATUREZ IN ('ISS', "
    cQuery += "                      'PIS', "
    cQuery += "                      'COFINS', "
    cQuery += "                      'CSLL', "
    cQuery += "                      'INSS', "
    cQuery += "                      'IRF') "
    cQuery += "   AND E2_MSFIL = '0101' "
    cQuery += "   AND E2_VENCTO BETWEEN '"+ DTOS(MV_PAR01) +"'  AND '"+ DTOS(MV_PAR02) + "' "

    IF !EMPTY(MV_PAR03)	
		cQuery += " AND E2_NUM = '"+MV_PAR03+"' "	
	ENDIF
    
    cQuery += " ORDER BY E2_EMISSAO, E2_NUM, E2_PARCELA "
       	
       	
       	
	TCQUERY cQuery NEW ALIAS RDR
	
	While RDR->(!EOF())
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(RDR->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")

		oSection1:Cell("EMISSAO"):SetValue(RDR->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("VENCIMENTO"):SetValue(RDR->VENCIMENTO)
		oSection1:Cell("VENCIMENTO"):SetAlign("LEFT")		
					
		oSection1:Cell("TITULO"):SetValue(RDR->TITULO)
		oSection1:Cell("TITULO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_FORNECE"):SetValue(RDR->COD_FORNECE)
		oSection1:Cell("COD_FORNECE"):SetAlign("LEFT")
		
		oSection1:Cell("FORNECEDOR"):SetValue(RDR->FORNECEDOR)
		oSection1:Cell("FORNECEDOR"):SetAlign("LEFT")
		
		oSection1:Cell("PARCELA"):SetValue(RDR->PARCELA)
		oSection1:Cell("PARCELA"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR"):SetValue(RDR->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(RDR->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("NATUREZA"):SetValue(RDR->NATUREZA)
		oSection1:Cell("NATUREZA"):SetAlign("LEFT")
										
		oSection1:PrintLine()
		
		RDR->(DBSKIP()) 
	enddo
	RDR->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Ronaldo Pereira
@since 18 de Outubro de 2018
@version 1.0
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Data Vencimento de ?"		        		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data Vencimento ate ?"                      ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","T�tulo ?"		        					,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	
return