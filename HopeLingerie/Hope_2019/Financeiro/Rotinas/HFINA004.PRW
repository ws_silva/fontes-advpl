#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWIZARD.CH"
#INCLUDE "FILEIO.CH"

// Array do RETORNO
#DEFINE NR_SEQ			1
#DEFINE FIL_ESTAB		2
#DEFINE NOME_ESTAB		3
#DEFINE COD_ESTAB		4
#DEFINE ADQ				5		//ADQUIRENTE
#DEFINE NOME_ADQ		6
#DEFINE TP_MOV			7
#DEFINE DESC_MOV		8
#DEFINE DT_MOV			9
#DEFINE LOTE			10
#DEFINE LOTE_UNICO		11
#DEFINE NR_PARC			12
#DEFINE BANCO			13
#DEFINE AGENCIA			14
#DEFINE CONTA			15
#DEFINE CRED_DEB		16
#DEFINE VLR_BRUTO		17
#DEFINE VLR_COMIS		18		//VALOR COMISSAO
#DEFINE VLR_LIQ			19
#DEFINE BAND			20		//BANDEIRA
#DEFINE NOME_BAND		21
#DEFINE PROD			22
#DEFINE DESC_PROD		23
#DEFINE DT_VENDA		24
#DEFINE HR_VENDA		25
#DEFINE AUTORIZ			26		//AUTORIZACAO
#DEFINE NSU				27
#DEFINE NR_TID			28
#DEFINE NR_CARTAO		29
#DEFINE TERMINAL		30
#DEFINE TOT_PARC		31
#DEFINE STATUS			32
#DEFINE SIT_CONC		33		//SITUACAO CONCILIACAO
#DEFINE DT_VENC_PARC	34
#DEFINE COD_CLI			35
#DEFINE NOME_CLI		36
#DEFINE NR_UNICO_ERP	37		//NUMERO UNICO ERP
#DEFINE NR_UNICO_NEG	38		//NUMERO UNICO NEGOCIO

// Array da BAIXA
#DEFINE PREFIXO			1
#DEFINE NUM				2
#DEFINE PARCELA			3
#DEFINE TIPO			4
#DEFINE CLIENTE			5
#DEFINE LOJA			6
#DEFINE NATUREZA		7
#DEFINE AUTMOTBX		8
#DEFINE AUTBANCO		9
#DEFINE AUTAGENCIA		10
#DEFINE AUTCONTA		11
#DEFINE AUTDTBAIXA		12
#DEFINE AUTDTCREDITO	13
#DEFINE AUTHIST			14
#DEFINE AUTDESCONT		15
#DEFINE AUTACRESC		16
#DEFINE AUTDECRESC		17
#DEFINE AUTMULTA		18
#DEFINE AUTJUROS		19
#DEFINE AUTVALREC		20

/*/
------------------------------------------------------------------
{Protheus.doc} HFINA004
Rotina de importacao/baixa automatica da Equals

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
User Function HFINA004()

	Local oWizard	:= Nil		//Objeto matriz
	Local oCamArq	:= Nil		//Objeto do caminho do arquivo
	Local lOk		:= .F.		//Variavel que verifica se o procedimento foi executado com um Finalizar
	
	Private cCamArq := ""
	Private cArqGer := ""

	DEFINE WIZARD oWizard TITLE "Concilia��o Equals" HEADER "Wizard utilizado para importa��o de arquivos de concilia��o da Equals";
	MESSAGE "";
	TEXT "Esta rotina tem por objetivo importar os arquivos de concilia��o da Equals" ;
	PANEL NEXT {|| .T. } FINISH {|| .T. };

	// Painel da selecao do arquivo
	CREATE PANEL oWizard HEADER "Dados concilia��o"	MESSAGE "Selecione o arquivo de integra��o de concilia��o da Equals" ;
	PANEL BACK {|| .T. } NEXT {|| HF004ExtArq( cCamArq ) } FINISH {|| .T. } EXEC {|| .T. }

	@ C(005),C(005) Say "Arquivo" 			  Size C(051),C(008) COLOR CLR_BLACK PIXEL OF oWizard:oMPanel[2]
	@ C(004),C(055) MsGet oCamArq Var cCamArq Size C(105),C(009) COLOR CLR_BLACK PIXEL OF oWizard:oMPanel[2]

	@ C(004),C(162) Button "Procurar" Size C(037),C(009) Action HF004SelArq( @cCamArq, @oCamArq ) PIXEL OF oWizard:oMPanel[2]

	// Painel da importacao do arquivo e finalizacao do processo
	CREATE PANEL oWizard HEADER "Finalizar" MESSAGE "Para confirmar a importa��o do arquivo de concilia��o da Equals clique em Finalizar ou clique em Cancelar para sair da rotina" ;
	PANEL BACK {|| .T. } FINISH {|| lOk := .T. } EXEC {|| .T.}

	ACTIVATE WIZARD oWizard CENTERED

	// Rotina dos processos de importacao
	If lOk
		HF004Proc( cCamArq )
	EndIf

Return

/*/
------------------------------------------------------------------
{Protheus.doc} HF004Proc
Processos

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function HF004Proc( cCamArq )

	Local nPosFim		:= 0
    Local cDirDest		:= ""
	Local cFileName		:= ""

	Private aTitulo		:= {}
	Private aLog		:= {}

	//--------------------------------------------
	// Processo da Baixa dos Titulos
	//--------------------------------------------
	Processa( {|| ProcBaixa( cCamArq ) }, "Processando" )

	//--------------------------------------------
	// Processo da Copia de Seguranca
	//--------------------------------------------
	nPosFim		:= Rat( "\", cCamArq )
   	cDirDest	:= Rtrim( Substr( cCamArq, 1, ( nPosFim - 1 ) ) )
	cFileName	:= Rtrim( Substr( cCamArq, ( nPosFim + 1 ) ) )
		
	// Copia de seguranca
	If _CopyFile( cCamArq, cDirDest + "\backup\" + cFileName )
		FErase( cCamArq )
	Else
		MsgStop( "N�o foi poss�vel fazer a c�pia de seguran�a do arquivo " + cCamArq, "Aten��o!" )
	Endif

	//--------------------------------------------
	// Processo de Geracao do Arquivo de Log
	//--------------------------------------------
	If Len( aLog ) > 0
	
		Processa( {|| GeraLog( aLog ) }, "Processando" )
	
		//--------------------------------------------
		// Mensagem de conclusao do processo
		//--------------------------------------------
		cMensagem := "Arquivo Importado: " + CRLF + Lower( cCamArq ) + CRLF + CRLF
		cMensagem += "Arquivo de Log: " + CRLF + Lower( cArqGer )

		MsgInfo( cMensagem, "Processo Finalizado" )

	Endif

Return

/*/
------------------------------------------------------------------
{Protheus.doc} HF004ExtArq
Rotina de valida��o de arquivo externo

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function HF004ExtArq( cCamArq )

	Local lRet := .T.

	If Empty( cCamArq )

		MsgInfo( "Para continuar � necessario a selecionar o arquivo de retorno", "Hope" )
		lRet := .F.

	ElseIf !File( cCamArq )

		MsgInfo( "Arquivo " + cCamArq + " nao encontrado!", "Hope" )
		lRet := .F.

	EndIf

Return( lRet )

/*/
------------------------------------------------------------------
{Protheus.doc} HF004SelArq
Rotina de sele��o de arquivos

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function HF004SelArq( cCamArq, oCamArq )

	Local cType := OemToAnsi("Arquivos CSV") + "(*.csv) |*.csv|"	//"Arquivos CSV"

	cCamArq := Upper( Alltrim( cGetFile( cType, "Selecione o Arquivo", 0,, .F., GETF_LOCALHARD + GETF_NETWORKDRIVE ) ) )		//"Selecione o Arquivo"

Return( oCamArq:Refresh() )

/*/
------------------------------------------------------------------
{Protheus.doc} ProcBaixa
Processo de Baixa Automatica:
	1 - Leitura do arquivo
	2 - Valida arquivo
	3 - Executa a baixa

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function ProcBaixa( cCamArq )

	Local aLayOut		:= {}
	Local aBaixa		:= Array( 20 )
	Local nX			:= 0
	Local cMensagem		:= ""

	BEGIN SEQUENCE

		//-------------------------------------------
		// Realiza leitura do arquivo
		//-------------------------------------------
		If !LeArquivo( @aLayOut, cCamArq )
			BREAK
		Endif

		//-------------------------------------------
		// Realiza a validacao do arquivo
		//-------------------------------------------
		If !ValArquivo( @aLayOut, cCamArq )
			BREAK
		Endif

		//-------------------------------------------
		// Realiza a baixa dos titulos
		//-------------------------------------------
		If Len( aLayOut ) > 0
		
			ProcRegua( ( Len( aLayOut ) ) )

			For nX := 1 To Len( aLayOut )

				IncProc( "Aguarde baixando titulos..." )

				// Dados da baixa
				aBaixa	:= Array( 20 )

				If PreparaBx( aLayOut[nX], @aBaixa )
					BxTitulo( aLayOut[nX], aBaixa )
				Endif

			Next

		Else

			cMensagem := "Arquivo Importado: " + CRLF + Lower( cCamArq ) + CRLF + CRLF
			cMensagem += "Arquivo de Log: " + CRLF + "Arquivo inv�lido ou n�o houve movimento para a HOPE do Nordeste."

			MsgInfo( cMensagem, "Processo Finalizado!" )

		Endif

	END SEQUENCE

Return

/*/
------------------------------------------------------------------
{Protheus.doc} PreparaBx
Prepara os dados para baixa do titulo

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function PreparaBx( aDados, aBaixa )

	Local aArea			:= GetArea()
	Local aAreaSE1		:= SE1->( GetArea() )
	Local lBaixa		:= .F.
	Local lRet			:= .T.
	Local aKey			:= {}
	Local cPrefixo		:= ""
	Local cNum			:= ""
	Local cParcela		:= ""
	Local cTipo 		:= ""
	Local cNatureza		:= ""
	Local cCliente		:= ""
	Local cLoja			:= ""
	Local cCC 			:= ""
	Local nVlrLiq		:= 0
	Local cMsg			:= ""
	Local cOcorrBx		:= ""
	Local aMargem		:= &( GetMv( "EQ_MARGEM", .T., "{ 0.03, 0.50 }" ) )
	Local nTamPref		:= TamSX3("E1_PREFIXO")[1]
	Local nTamNum		:= TamSX3("E1_NUM")[1]
	Local nTamParc		:= TamSX3("E1_PARCELA")[1]
	Local nTamTipo		:= TamSX3("E1_TIPO")[1]
	Local nTamNat		:= TamSX3("E1_NATUREZ")[1]
	Local nTamCli		:= TamSX3("E1_CLIENTE")[1]
	Local nTamLoja		:= TamSX3("E1_LOJA")[1]
	Local nTamBanc 		:= TamSX3("E8_BANCO")[1]
	Local nTamAgec 		:= TamSX3("E8_AGENCIA")[1]
	Local nTamCont 		:= TamSX3("E8_CONTA")[1]

	aKey		:= StrToKArr( aDados[NR_UNICO_NEG], "|" )
	cParcela	:= PadR( Alltrim( getParc( aDados[TOT_PARC], aDados[NR_PARC] ) ), nTamParc )
	
	cCC 		:= Alltrim( Str( Val( aDados[CONTA] ) ) )
	cCC 		:= Substr( cCC, 1, (Len(cCC)-1) )

	If Len( aKey ) <> 6
		cNum	:= PadR( Alltrim( aKey[1] ), nTamNum )
		aKey 	:= getKey( cNum, cParcela )
	Endif

	cPrefixo	:= PadR( Alltrim( aKey[1] ), nTamPref )
	cNum		:= PadR( Alltrim( aKey[2] ), nTamNum )
	cTipo		:= PadR( Alltrim( aKey[3] ), nTamTipo )
	cNatureza	:= PadR( Alltrim( aKey[4] ), nTamNat )
	cCliente	:= PadR( Alltrim( aKey[5] ), nTamCli )
	cLoja		:= PadR( Alltrim( aKey[6] ), nTamLoja )

	dbSelectArea( "SE1" )
	dbSetOrder( 1 )
	If dbSeek( xFilial( "SE1" ) + cPrefixo + cNum + cParcela + cTipo )

		nVlrLiq := Val( StrTran( StrTran( aDados[VLR_LIQ], ".", "" ), ",", "." ) )

		// Verifica se o titulo j� foi baixado
		If Empty( SE1->E1_BAIXA )

			// Valor da Equals < Protheus
			If nVlrLiq < SE1->E1_VLRLIQ
				
				// Valida margem para baixo
				If ABS( SE1->E1_VLRLIQ - nVlrLiq ) <= aMargem[1]
					lBaixa := .T.
				Endif
			
			ElseIf nVlrLiq > SE1->E1_VLRLIQ

				// Valida margem para cima
				If ABS( SE1->E1_VLRLIQ - nVlrLiq ) <= aMargem[2]
					lBaixa := .T.
				Endif

			Else

				lBaixa := .T.

			Endif

			// Valida a baixa
			If lBaixa

				aBaixa[PREFIXO]			:= cPrefixo
				aBaixa[NUM]				:= cNum
				aBaixa[PARCELA]			:= cParcela
				aBaixa[TIPO]			:= cTipo
				aBaixa[CLIENTE]			:= cCliente
				aBaixa[LOJA]			:= cLoja
				aBaixa[NATUREZA]		:= cNatureza
				aBaixa[AUTMOTBX]		:= "NOR"
				aBaixa[AUTBANCO]		:= PadR( Alltrim( aDados[BANCO] ), nTamBanc )
				aBaixa[AUTAGENCIA]		:= PadR( Alltrim( aDados[AGENCIA] ), nTamAgec )
				aBaixa[AUTCONTA]		:= PadR( Alltrim( cCC ), nTamCont )
				aBaixa[AUTDTBAIXA]		:= SToD( aDados[DT_MOV] )
				aBaixa[AUTDTCREDITO]	:= SToD( aDados[DT_MOV] )
				aBaixa[AUTHIST]			:= "Conciliador EQUALS"
				aBaixa[AUTDESCONT]		:= 0
				aBaixa[AUTACRESC]		:= 0
				aBaixa[AUTDECRESC]		:= 0
				aBaixa[AUTMULTA]		:= 0
				aBaixa[AUTJUROS]		:= 0
				aBaixa[AUTVALREC]		:= nVlrLiq

			Else

				lRet		:= .F.
				cOcorrBx	:= "1"
				cMsg 		:= "Valores das parcelas est�o divergentes: Protheus R$ " + Transform( SE1->E1_VLRLIQ, "@E 999,999,999.99" ) + " <> Equals R$ " + Transform( nVlrLiq, "@E 999,999,999.99" ) + "."

			Endif

		Else

			lRet		:= .F.
			cOcorrBx	:= "1"	// Falha
			cMsg 		:= "Titulo ja baixado ou nao existe titulo para baixar neste periodo."
			
		Endif

	Else

		lRet		:= .F.
		cOcorrBx	:= "1"
		cMsg 		:= "Titulo nao cadastrado."

	Endif

	If !Empty( cMsg )
		GravaLog( aDados, cPrefixo, cNum, cParcela, cTipo, cOcorrBx, cMsg )
	Endif

	RestArea( aAreaSE1 )
	RestArea( aArea )

Return( lRet )

/*/
------------------------------------------------------------------
{Protheus.doc} BxTitulo
Rotina de baixa de titulos

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function BxTitulo( aLayOut, aTitulo )

	Local aArea   	:= GetArea()
	Local aBaixa 	:= {}
	Local cMsg  	:= ""
	Local cOcorrBx	:= "0"	// Titulo Baixado com Sucesso
	Local cPrefixo	:= aTitulo[01]
	Local cNum 		:= aTitulo[02]
	Local cParcela	:= aTitulo[03]
	Local cTipo		:= aTitulo[04]

	//Private lAutoErrNoFile 	:= .T.
	Private lMsErroAuto		:= .F.

	aBaixa	:= ;
	{{"E1_PREFIXO"		,cPrefixo		,NiL},;
	{"E1_NUM"			,cNum			,NiL},;
	{"E1_PARCELA"		,cParcela		,NiL},;
	{"E1_TIPO"			,cTipo			,NiL},;
	{"E1_CLIENTE"		,aTitulo[05]	,NiL},;
	{"E1_LOJA"			,aTitulo[06]	,NiL},;
	{"E1_NATUREZA"		,aTitulo[07]	,NiL},;
	{"AUTMOTBX"			,aTitulo[08]	,NiL},;
	{"AUTBANCO"			,aTitulo[09]	,Nil},;
	{"AUTAGENCIA"		,aTitulo[10]	,Nil},;
	{"AUTCONTA"	 		,aTitulo[11]	,Nil},;
	{"AUTDTBAIXA"		,aTitulo[12] 	,NiL},;
	{"AUTDTCREDITO"		,aTitulo[13] 	,NiL},;
	{"AUTHIST"			,aTitulo[14] 	,NiL},; //Conciliador EQUALS
	{"AUTDESCONT"		,aTitulo[15]    ,NiL},; //Valores de desconto
	{"AUTACRESC"		,aTitulo[16] 	,NiL},; //Valores de acrescimo - deve estar cadastrado no titulo previamente
	{"AUTDECRESC"		,aTitulo[17] 	,NiL},; //Valore de decrescimo - deve estar cadastrado no titulo previamente
	{"AUTMULTA"			,aTitulo[18] 	,NiL},; //Valores de multa
	{"AUTJUROS"			,aTitulo[19] 	,NiL},; //Valores de Juros
	{"AUTVALREC"		,aTitulo[20] 	,NiL}}  //Valor recebido

	//Chama rotina que altera o vencimento real para nao cobrar juros                                           		
	fAltVenc( cPrefixo, cNum, cParcela, cTipo )
	
	//Efetua baixa automatica por titulo
	lMsErroAuto := .F.

	MSExecAuto( {|x, y| FINA070(x, y)}, aBaixa, 3 )
											
	If lMsErroAuto
		lRet 		:= .F.
		cOcorrBx	:= "1"
		cMsg 		:= getDescOcor( GetAutoGRLog() )
	Endif

	GravaLog( aLayOut, cPrefixo, cNum, cParcela, cTipo, cOcorrBx, cMsg )

	RestArea( aArea )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} GravaLog
Rotina de gravacao do Log de Processamento

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function GravaLog( aDados, cPrefixo, cNum, cParcela, cTipo, cOcorrBx, cMsg )

	Local aDadosLog		:= Array(14)
	Local cSeq			:= Alltrim( aDados[NR_SEQ] )
	Local cTpMov 		:= Alltrim( aDados[TP_MOV] )
	Local cDescMov		:= Alltrim( aDados[DESC_MOV] )
	Local cStatVend		:= Alltrim( aDados[STATUS] )
	Local cDescStatVend	:= ""
	Local cSitConc		:= Alltrim( aDados[SIT_CONC] )
	Local cDescSitConc	:= ""

	// Sequencia do arquivo
	aDadosLog[01] := cSeq

	// Tipo de Movimento
	aDadosLog[02] := IIf( !Empty( cTpMov ), ( cTpMov + "-" + cDescMov ), "" )

	// Data do Movimento
	aDadosLog[03] := DtoC( StoD( Alltrim( aDados[DT_MOV] ) ) )

	// Status da Venda
	If cStatVend == "1"
		cDescStatVend := "Acatada"
	ElseIf cStatVend == "2"
		cDescStatVend := "Rejeitada"
	ElseIf cStatVend == "3"
		cDescStatVend := "Cancelada"
	ElseIf cStatVend == "4"
		cDescStatVend := "Liquidada"
	Endif
	aDadosLog[04] := IIf( !Empty( cStatVend ), ( cStatVend + "-" + cDescStatVend ), "" )

	// Situacao de Conciliacao
	If cSitConc == "1"
		cDescSitConc := "Em Aberto"
	ElseIf cSitConc == "2"
		cDescSitConc := "Divergente"
	ElseIf cSitConc == "3"
		cDescSitConc := "Conciliada"
	ElseIf cSitConc == "4"
		cDescSitConc := "Conciliada Manualmente"
	ElseIf cSitConc == "5"
		cDescSitConc := "Justificada"
	ElseIf cSitConc == "6"
		cDescSitConc := "N�o Aplic�vel"
	Endif
	aDadosLog[05] := IIF( !Empty( cSitConc ), ( cSitConc + "-" + cDescSitConc ), "" )

	aDadosLog[06] := cPrefixo
	aDadosLog[07] := cNum
	aDadosLog[08] := cParcela
	aDadosLog[09] := cTipo
	aDadosLog[10] := Alltrim( aDados[AUTORIZ] )
	aDadosLog[11] := Alltrim( aDados[NSU] )
	aDadosLog[12] := Alltrim( aDados[VLR_LIQ] )

	// Status da Baixa
	If cOcorrBx == "0"
		aDadosLog[13] := ( cOcorrBx + "-" + "Baixado com Sucesso" )
		aDadosLog[14] := ""
	Else
		aDadosLog[13] := ( cOcorrBx + "-" + "Nao Baixado" )
		aDadosLog[14] := cMsg
	Endif

	AADD( aLog, aDadosLog )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} GeraLog
Rotina de geracao do Log de Processamento

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function GeraLog( aLog )

	Local cType 	:= "Arquivos LOG"+ "(*.CSV) |*.CSV|"
	Local nHdl		:= 0
	Local cDados	:= ""
	Local nI		:= 0
	Local cLin		:= ""
	Local cEOL		:= CHR(13)+CHR(10)

	cArqGer := cGetFile(cType ,"Selecione o diretorio para grava��o do LOG",0,,.F.,GETF_LOCALHARD + GETF_NETWORKDRIVE + GETF_RETDIRECTORY)

	BEGIN SEQUENCE

		//Incluo o nome do arquivo no caminho ja selecionado pelo usuario
		cArqGer := Lower( Alltrim( cArqGer ) ) + "log_equals_" + DtoS( dDataBase ) + StrTran( Time(), ":", "" ) + ".csv"

		If (nHdl := FCreate( cArqGer )) == -1
			MsgInfo( "Falha ao criar o arquivo" + cArqGer, "Aten��o!")
			BREAK
		EndIf

		// Cabecalho
		cDados	:= "SEQUENCIA;"						// 01
		cDados	+= "TIPO DE MOVIMENTO;"				// 02
		cDados	+= "DATA DO MOVIMENTO;"				// 03
		cDados	+= "STATUS DA VENDA;"				// 04
		cDados	+= "SITUACAO DE CONCILIACAO;"		// 05
		cDados	+= "PREFIXO;"						// 06
		cDados	+= "TITULO;"						// 07
		cDados	+= "PARCELA;"						// 08
		cDados	+= "TIPO;"							// 09
		cDados	+= "AUTORIZACAO;"					// 09
		cDados	+= "NSU;"							// 10
		cDados	+= "VALOR DA PARCELA;"				// 12
		cDados	+= "OCORRENCIA;"					// 13
		cDados	+= "DETALHE DA OCORRENCIA"			// 14

		cLin	:= Space( Len( cDados ) ) + cEOL
		cLin	:= Stuff( cLin, 01, Len( cDados ), cDados )

		If FWrite( nHdl, cLin, Len( cLin ) ) != Len( cLin )
			If Aviso( "Aten��o!", "Ocorreu um erro na gravacao do arquivo. Continua ?", { "Sim", "N�o" } ) == 2
				FClose( nHdl )
				BREAK
			EndIf
		EndIf

		ProcRegua( Len( aLog ) )

		For nI := 1 to Len(aLog)
			
			IncProc( "Aguarde gravando arquivo de Log..." )

			// Detalhes
			cDados := aLog[nI][01] + ';' 
			cDados += aLog[nI][02] + ';'
			cDados += aLog[nI][03] + ';'
			cDados += aLog[nI][04] + ';'
			cDados += aLog[nI][05] + ';'
			cDados += aLog[nI][06] + ';'
			cDados += aLog[nI][07] + ';'
			cDados += aLog[nI][08] + ';'
			cDados += aLog[nI][09] + ';'
			cDados += aLog[nI][10] + ';'
			cDados += aLog[nI][11] + ';'
			cDados += aLog[nI][12] + ';'
			cDados += aLog[nI][13] + ';'
			cDados += aLog[nI][14]								

			cLin	:= Space( Len( cDados ) ) + cEOL
			cLin	:= Stuff( cLin, 01, Len( cDados ), cDados ) 
			
			If FWrite( nHdl, cLin, Len( cLin ) ) != Len( cLin )
				If Aviso( "Aten��o!", "Ocorreu um erro na gravacao do arquivo. Continua ?", { "Sim", "N�o" } ) == 2
					FClose( nHdl )
					BREAK
				EndIf
			EndIf

		Next

		FClose( nHdl )

		If File( cArqGer )
			ShellExecute( "OPEN", cArqGer, "", "", 1 )
		Endif

	END SEQUENCE

Return

/*/
------------------------------------------------------------------
{Protheus.doc} fAltVenc
Rotina que altera o vencimento real para nao cobrar juros

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function fAltVenc( cPrefixo, cNum, cParcela, cTipo )

	Local aArea		:= GetArea()
	Local aAreaSE1	:= SE1->( GetArea() )

	dbSelectArea( "SE1" )
	dbSetOrder( 1 ) // E1_FILIAL, E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO
	If dbSeek( xFilial( "SE1" ) + cPrefixo + cNum + cParcela + cTipo )
		RecLock( "SE1", .F. )
		SE1->E1_VENCREA := DataValida( SE1->E1_VENCREA+3, .T. )  
		SE1->( MsUnlock() )
	Endif

	RestArea( aAreaSE1 )
	RestArea( aArea )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} 
Retorna a Parcela para Baixa

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function getParc( cParT, cParP )

   Local cRet   := ""
   Local _cParc := "ABCDEFGHIJKLMNOPQRSTU"
   Local nPos	:= Val( cParP )

   If Val( cParP ) == 1
      If Val( cParT ) == 1
         cRet := ""
      Else
         cRet := Substr( _cParc, nPos, 1 )
      Endif
   Else
      cRet := Substr( _cParc, nPos, 1 )
   Endif

Return( cRet )

/*/
------------------------------------------------------------------
{Protheus.doc} getDescOcorr
Retorna a descricao das ocorrencias

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function getDescOcor( xOcorr )

	Local nX		:= 0
	Local cRet		:= ""

	Default xOcorr	:= {}

	If ValType( xOcorr ) == "A"

		For nX := 1 To Len( xOcorr )

			If "AJUDA:TITBAIXADO" $ Upper( xOcorr[nX] )

				cRet := "Titulo ja baixado ou nao existe titulo para baixar neste periodo."
				Exit

			ElseIf "AJUDA:FA100BCO" $ Upper( xOcorr[nX] )

				cRet := "Banco/Agencia/Conta nao cadastrado."
			
			ElseIf "PESQUISA NAO ENCONTRADA" $ Upper( xOcorr[nX] )

				cRet := "Titulo nao cadastrado."
				Exit

			ElseIf "INVALIDO" $ Upper( xOcorr[nX] )

				cRet += Alltrim( xOcorr[nX] ) + IIf( nX  < Len( xOcorr ), " | ", "" )

			Endif

		Next

		If Empty( cRet )

			For nX := 1 To Len( xOcorr )

				cRet += Alltrim( xOcorr[nX] ) + IIf( nX  < Len( xOcorr ), " | ", "" )

			Next
		Endif

	ElseIf ValType( xOcorr ) == "C"

		If "AJUDA:TITBAIXADO" $ Upper( xOcorr )

			cRet := "Titulo ja baixado."

		ElseIf "AJUDA:FA100BCO" $ Upper( xOcorr )

			cRet := "Banco/Agencia/Conta nao cadastrado."

		Else

			cRet := Alltrim( xOcorr )

		Endif
	
	Endif

Return( cRet )

/*/
------------------------------------------------------------------
{Protheus.doc} getKey
Retorna campos da chave de pesquisa

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function getKey( cNum, cParcela )

	Local aArea		:= GetArea()
	Local aRet 		:= { "", "", "", "", "", "" }
	Local cAlias	:= GetNextAlias()
	Local cQuery	:= ""

	cQuery := "SELECT E1_PREFIXO, E1_NUM, E1_TIPO, E1_NATUREZ, E1_CLIENTE, E1_LOJA "
	cQuery += "FROM " + RetSqlName( "SE1" ) + " "
	cQuery += "WHERE E1_FILIAL = '" + xFilial( "SE1" ) + "' AND "
	cQuery += "E1_NUM = '" + cNum + "' AND "
	cQuery += "E1_PARCELA = '" + cParcela + "' AND "
	cQuery += "E1_TIPO = 'NF' AND "
	cQuery += "D_E_L_E_T_ = ''"

	dbUseArea( .T., "TOPCONN", TcGenQry(,,cQuery), cAlias, .T., .T. ) 
	dbSelectArea( cAlias )

	If ( cAlias )->( !Eof() )

		aRet[1] := ( cAlias )->E1_PREFIXO
		aRet[2] := cNum
		aRet[3] := 'NF'
		aRet[4] := ( cAlias )->E1_NATUREZ
		aRet[5] := ( cAlias )->E1_CLIENTE
		aRet[6] := ( cAlias )->E1_LOJA

	Else

		aRet[2] := cNum
		aRet[3] := 'NF'

	Endif

	( cAlias )->( dbCloseArea() )

	RestArea( aArea )

Return( aRet )

/*/
------------------------------------------------------------------
{Protheus.doc} LeArquivo
Realiza a leitura do arquivo de retorno

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function LeArquivo( aLayOut, cCamArq )

	Local lRet			:= .T.
	Local nHdlFile		:= 0
	Local nRecCount		:= 0
	Local cLine			:= ""

	nHdlFile	:= FT_FUse( cCamArq )
	nRecCount	:= ( FT_FLastRec() - 1 )
		
	fClose( nHdlFile )
		
	FT_FUse()

	nHdlFile := FT_FUse( cCamArq )
		
	If nHdlFile == -1

		lRet := .F.

		MsgStop( "Problemas na abertura do Arquivo." + cCamArq, "Aten��o!" )

	Else

		ProcRegua( nRecCount )

		FT_FGoTop()

		While !FT_FEof()

			cLine := FT_FReadLn()

			IncProc( "Aguarde importando titulos..." )

			If Len( Alltrim( cLine ) ) == 0
				FT_FSkip()
				Loop
			Endif

			cLine := StrTran( cLine, '"', "'" )
			cLine := '{"' + cLine + '"}'
			cLine := StrTran( cLine, ';', '", "' )
			
			AADD( aLayOut, &cLine )

			FT_FSkip()

		Enddo
		
		fClose( nHdlFile )
		
		FT_FUse()

	Endif

Return( lRet )

/*/
------------------------------------------------------------------
{Protheus.doc} ValArquivo
Realiza a validacao do arquivo de retorno

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function ValArquivo( aLayOut, cCamArq )

	Local lRet		:= .T.
	Local nX		:= 0
	Local cDtMov	:= ""
	Local cTipoMov	:= ""
	Local aTitulos	:= {}

	If Len( aLayOut ) > 1

		ProcRegua( ( Len( aLayOut ) ) )
		
		For nX := 2 To Len( aLayOut )

			IncProc( "Aguarde validando titulos..." )		
		
			// Tipo do Movimento
			cTipoMov	:= StrZero( Val( aLayOut[nX][TP_MOV] ), 2 )

			// Processar somente HOPE do Nordeste
			If Alltrim( aLayOut[nX][FIL_ESTAB] ) <> "1016859349"
				Loop
			Endif

			If Len( aLayOut[nX] ) > 38
				lRet := .F.
				MsgStop( "Arquivo com estrutura inv�lida!" + CRLF + CRLF + Lower( cCamArq ), "Aten��o!" )				
				Exit
			Endif

			Do Case

				// Grupo Baixa: 01-Baixa por Pagamento|02-Baixa por Antecipacao|03-Baixa por Aceleracao
				Case cTipoMov $ "01|02|03"
			
					// 1-Em Aberto| 2-Divergente| 3-Conciliada| 4-Conciliada Manualmente| 5-Justificada| 6-N�o Aplic�vel
					If Alltrim( aLayOut[nX][SIT_CONC] ) $ "3|4"

						If Empty( cDtMov )
							cDtMov	:= Alltrim( aLayOut[nX][DT_MOV] )
						ElseIf cDtMov <> Alltrim( aLayOut[nX][DT_MOV] )
							lRet := .F.
							MsgStop( "Arquivo com mais de um movimento!" + CRLF + CRLF + Lower( cCamArq ), "Aten��o!" )				
							Exit
						Endif

						AADD( aTitulos, aLayOut[nX] )

					Else

						GravaLog( aLayOut[nX], "", "", "", "", "1", "Titulo nao conciliado." )

					Endif

				// Grupo Cancelamento: 11-Debito por Cancelamento|12-Desagendamento por Cancelamento|13-Estorno de Cancelamento
				Case cTipoMov $ "11|12|13"		

				// Grupo ChargeBack: 21-Debito por ChargeBack|22-Desagendamento por ChargeBack|23-Estorno de ChargeBack
				Case cTipoMov $ "21|22|23"

				// Grupo Cobranca de Tecnologia: 31-Cobranca de Tecnologia|32-Estorno de Tecnologia
				Case cTipoMov $ "31|32"

				// Grupo Ajustes: 41-Taxa de Antecipacao de Recebiveis|42-Outros Motivos nao Identificados|43-Retencao de Pagamento
				Case cTipoMov $ "41|42|43"
				
				Otherwise

			EndCase

		Next

	Endif

	If lRet
		aLayOut := aClone( aTitulos )
	Else
		aLayOut := {}
		aLog	:= {}
	Endif

Return( lRet )


