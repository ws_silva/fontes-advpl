#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"  
#INCLUDE "RWMAKE.CH"  

/*/{Protheus.doc} RF013 
Relat髍io de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPFIN910D()

	Private oReport
	Private cPergCont	:= 'HPRFIN910D' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf
	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'ALM', 'RELA敲O DE DEVOLU钦ES POR NF ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELA敲O DE DEVOLU钦ES POR NF' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELA敲O DE DEVOLU钦ES POR NF ', { 'ALM', 'ZAK','SE1','SA1'})
			
	TRCell():New( oSection1, 'NOTA_FISCAL'		  	,'ALM', 		'NOTA_FISCAL',	   "@!"     		,09)
	TRCell():New( oSection1, 'EMISSAO_NF'  			,'ALM', 		'EMISSAO_NF',	   "@D"	    		,08)
	TRCell():New( oSection1, 'TITULO'      			,'ALM', 		'TITULO',		   "@!"	    		,09)
	TRCell():New( oSection1, 'EMISSAO'  			,'ALM', 		'EMISSAO',   	   "@D"	    		,08)
	TRCell():New( oSection1, 'VENCIMENTO'  			,'ALM', 		'VENCIMENTO',	   "@D"	    		,08)
	TRCell():New( oSection1, 'CLIENTE'          	,'ALM', 		'CLIENTE',         "@!"				,35)
	TRCell():New( oSection1, 'VLR_TITULO'     	    ,'ALM', 		'VLR_TITULO',      "@E 99,999,999.99"  ,13)
	TRCell():New( oSection1, 'VLR_DEVOLV'       	,'ALM', 		'VLR_DEVOLV',      "@E 99,999,999.99"	,13)
	


Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
    LocaL lApaga := .f.
	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	                    
    // arquivo de retorno do romaneio
	aStru := {}

	aAdd( aStru , { "STATUS"        , "C" , 01 , 00 } )  
    aAdd( aStru , { "TITULO"        , "C" , 09 , 00 } )
    aAdd( aStru , { "CODCLI"        , "C" , 06 , 00 } )
    aAdd( aStru , { "NOME"          , "C" , 35 , 00 } )
    aAdd( aStru , { "EMISSAO"       , "D" , 08 , 00 } )
    aAdd( aStru , { "VENCIMENTO"    , "D" , 08 , 00 } )
    aAdd( aStru , { "PAGAMENTO"     , "D" , 08 , 02 } )
    aAdd( aStru , { "V_TOTAL"       , "N" , 14 , 02 } )
    aAdd( aStru , { "V_LIQUIDO"     , "N" , 14 , 02 } )
    aAdd( aStru , { "NOTAFISCAL"    , "C" , 09 , 00 } )
    aAdd( aStru , { "PARCELA"       , "C" , 03 , 00 } )
    aAdd( aStru , { "TIPO"          , "C" , 03 , 00 } )
    aAdd( aStru , { "CLIENTE"       , "C" , 06 , 00 } )
    aAdd( aStru , { "LOJACLI"       , "C" , 04 , 00 } )
    aAdd( aStru , { "NOMECLI"       , "C" , 35 , 00 } )
    aAdd( aStru , { "EMISSAO_NF"    , "D" , 08 , 00 } )
    aAdd( aStru , { "VENCTO_NF"     , "D" , 08 , 00 } )
    aAdd( aStru , { "VALOR_NF"      , "N" , 14 , 02 } )
    aAdd( aStru , { "TAXA"          , "N" , 24 , 02 } )
  
	IF Select("xREL") > 0
		xREL->(dbCloseArea())
	Endif             

	cArqa := CriaTrab(aStru,.F.)

	If File(cArqa+".dbf")
		Inkey(4)
		cArqa := CriaTrab(aStru,.F.)
	EndIf
	
	dbCreate (cArqa,aStru)
	dbUseArea(.T.,,cArqa,"xREL",.T.)
	cInda := CriaTrab(NIL,.F.)
	
	IndRegua("xREL",cInda," TITULO",,,"Selecionando Registros...")

	IF Select("XTRB") > 0
		XTRB->(dbCloseArea())
	Endif      
	
//	    cQuery := " SELECT ZAK_NUM,ZAK_CODCLI,ZAK_NOMECL, ZAK_VLINI,E1_NUM, E1_VALOR"
//	    cQuery += " FROM "+RetSqlName("ZAK")+" AK (NOLOCK) " 
 //       cQuery += " JOIN "+RetSqlName("SE1")+" E1 (NOLOCK) ON ZAK_NUM = E1_NUM+'/'+E1_PARCELA  and E1_TIPO = 'NCC' AND E1.D_E_L_E_T_ = '' "
//        cQuery += "	WHERE AK.D_E_L_E_T_ = ''  " 
	       
   cQuery := " SELECT ZAK_STATUS,	ZAK_NUM,ZAK_CODCLI,	ZAK_NOMECL,	ZAK_EMISSA,	ZAK_VENCTO,	ZAK_PAGTO,	ZAK_VLINI,	ZAK_VLLIQ,"
 	 cQuery += "	 E1_NUM,	E1_PARCELA,	E1_TIPO,	E1_NATUREZ,	E1_PORTADO,	E1_AGEDEP,	E1_CLIENTE,	E1_LOJA,	E1_NOMCLI,"
	 cQuery += "	 E1_XCOD,	E1_EMISSAO,	E1_VENCREA,	E1_VALOR,E1_XTXCC" 
   cQuery += " FROM "+RetSqlName("ZAK")+" AK (NOLOCK) " 
   cQuery += " JOIN "+RetSqlName("SE1")+" E1 (NOLOCK) ON ZAK_NUM = E1_NUM+'/'+E1_PARCELA  and E1_TIPO = 'NCC' AND E1.D_E_L_E_T_ = '' "
   cQuery += "	WHERE AK.D_E_L_E_T_ = ''  "
    
    TCQUERY cQuery NEW ALIAS xTRB 
    XTRB->(DBGOTOP())
    While XTRB->(!EOF())        
       
           dbselectarea("xREL")
           xREL->(Reclock("xREL",.t.))
           
	       xREL->STATUS        := XTRB->ZAK_STATUS  
	       xREL->TITULO        := XTRB->ZAK_NUM
	       xREL->CODCLI        := XTRB->ZAK_CODCLI
	       xREL->NOME          := XTRB->ZAK_NOMECL
	       xREL->EMISSAO       := ctod(SUBSTR(XTRB->ZAK_EMISSA,7,2)+'/'+SUBSTR(XTRB->ZAK_EMISSA,5,2)+'/'+SUBSTR(XTRB->ZAK_EMISSA,3,2) )
	       xREL->VENCIMENTO    := ctod(SUBSTR(XTRB->ZAK_VENCTO,7,2)+'/'+SUBSTR(XTRB->ZAK_VENCTO,5,2)+'/'+SUBSTR(XTRB->ZAK_VENCTO,3,2) )
	       xREL->PAGAMENTO     := ctod(SUBSTR(XTRB->ZAK_PAGTO,7,2)+'/'+SUBSTR(XTRB->ZAK_PAGTO,5,2)+'/'+SUBSTR(XTRB->ZAK_PAGTO,3,2) )
	       xREL->V_TOTAL       := XTRB->ZAK_VLINI
	       xREL->V_LIQUIDO     := XTRB->ZAK_VLLIQ
	       xREL->NOTAFISCAL    := XTRB->E1_NUM
	       xREL->PARCELA       := XTRB->E1_PARCELA
	       xREL->TIPO          := XTRB->E1_TIPO
	       xREL->CLIENTE       := XTRB->E1_CLIENTE
	       xREL->LOJACLI       := XTRB->E1_LOJA
	       xREL->NOMECLI       := XTRB->E1_NOMCLI
	       xREL->EMISSAO_NF    := ctod(SUBSTR(XTRB->E1_EMISSAO,7,2)+'/'+SUBSTR(XTRB->E1_EMISSAO,5,2)+'/'+SUBSTR(XTRB->E1_EMISSAO,3,2) )
	       xREL->VENCTO_NF     := ctod(SUBSTR(XTRB->E1_VENCREA,7,2)+'/'+SUBSTR(XTRB->E1_VENCREA,5,2)+'/'+SUBSTR(XTRB->E1_VENCREA,3,2) )  
	       xREL->VALOR_NF      := XTRB->E1_VALOR
	       xREL->TAXA          := XTRB->E1_XTXCC
           xREL->(MsUnlock())
		       
        dbselectarea("XTRB")
        XTRB->(dbSKIP())
    Enddo 

    xREL->(DBGOTOP())
	While xREL->(!EOF())      
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()
	oReport:IncMeter()

		oSection1:Cell("NOTA_FISCAL"):SetValue(xREL->NOTAFISCAL)
		oSection1:Cell("NOTA_FISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("EMISSAO_NF"):SetValue(xREL->EMISSAO_NF)
		oSection1:Cell("EMISSAO_NF"):SetAlign("LEFT")
		
		oSection1:Cell("TITULO"):SetValue(xREL->TITULO)
		oSection1:Cell("TITULO"):SetAlign("LEFT")
		
		
		oSection1:Cell("EMISSAO"):SetValue( xREL->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")

		oSection1:Cell("VENCIMENTO"):SetValue(xREL->VENCIMENTO)
		oSection1:Cell("VENCIMENTO"):SetAlign("LEFT")
		
			                                                                      
		oSection1:Cell("CLIENTE"):SetValue(xREL->NOME)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("VLR_TITULO"):SetValue(xREL->V_TOTAL)
		oSection1:Cell("VLR_TITULO"):SetAlign("LEFT")
		
		oSection1:Cell("VLR_DEVOLV"):SetValue(xREL->VALOR_NF)
		oSection1:Cell("VLR_DEVOLV"):SetAlign("LEFT")
		
		oSection1:PrintLine()
		
		xREL->(DBSKIP()) 
	enddo
	xREL->(DBCLOSEAREA())
Return( Nil )


/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Verifica/cria SX1 a partir de matriz para verificacao          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Especifico para Clientes Microsiga                             潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

//	Aadd(aPergs,{"Ate Cliente","","","mv_ch8","C",6,0,0,"G","","MV_PAR08","","","","ZZZZZZ","","","","","","","","","","","","","","","","","","","","","SE1","","","",""})
	Aadd(aPergs,{"Cliente de ?","","","mv_ch1","C",6,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Cliente At�?","","","mv_ch2","C",6,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Nota Inicial","","","mv_ch3","C",9,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Nota Final"  ,"","","mv_ch4","C",9,0,0,"G","","mv_par04","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data de? "   ,"","","mv_ch5","D",8,0,0,"G","","mv_par05","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data ate? "  ,"","","mv_ch6","D",8,0,0,"G","","mv_par06","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next
