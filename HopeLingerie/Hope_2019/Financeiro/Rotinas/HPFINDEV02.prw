#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"  
#INCLUDE "RWMAKE.CH"  

/*/{Protheus.doc} RF013 
Relat髍io de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

//User function FINDER1A()
//Local aArea := GetArea()
//If MsgYesNo("Confirma a impress鉶 do Relat髍io de devolu珲es? ")
//	Processa({|| HPFINDEV02()})
//Endif
//RestArea(aArea)
//Return


User Function FINDER1A()

	Private oReport
	Private cPergCont	:= 'HPFINDEV1A' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf
	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'ALM', 'RELA敲O DE DEVOLU钦ES POR NF ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELA敲O DE DEVOLU钦ES POR NF' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELA敲O DE DEVOLU钦ES POR NF ', { 'ALM', 'ZAK','SAS','SA1'})
			
	TRCell():New( oSection1, 'DATA'		  	          ,'ALM','DATA',	               "@D"              ,08)
	TRCell():New( oSection1, 'NOTA'  			      ,'ALM','NOTA',	               "@!"              ,09)
	TRCell():New( oSection1, 'NOTA REFERENCIA'        ,'ALM','NOTA REFERENCIA',   	   "@!"	    		 ,09)
	TRCell():New( oSection1, 'DATA NOTA REFEREN'      ,'ALM','DATA NOTA REFEREN',      "@D"	    		 ,08)
	TRCell():New( oSection1, 'PEDIDO DE VENDA'        ,'ALM','PEDIDO DE VENDA',	       "@!"	    		 ,06)
	TRCell():New( oSection1, 'DATA DO PEDIDO'         ,'ALM','DATA DO PEDIDO',         "@D"				 ,08)
	TRCell():New( oSection1, 'NSU_CARTAO'	          ,'ALM','NSU_CARTAO',             "@!"				 ,10)      
	TRCell():New( oSection1, 'AUTORIZACAO_CARTAO'     ,'ALM','AUTORIZACAO_CARTAO',     "@!" 			 ,10)
	TRCell():New( oSection1, 'COD_CLIENTE'            ,'ALM','COD_CLIENTE',            "@!" 			 ,06)
	TRCell():New( oSection1, 'NOME'                   ,'ALM','NOME',                   "@!" 			 ,30)
	TRCell():New( oSection1, 'CPF'                    ,'ALM','CPF',                    "@!" 			 ,11)
	TRCell():New( oSection1, 'TIPO_PGTO'	          ,'ALM','TIPO_PGTO',              "@!" 			 ,15)
	TRCell():New( oSection1, 'Valor Nota de Devolucao','ALM','VALOR NOTA DE DEVOLUCAO',"@E 99,999,999.99",13)
	TRCell():New( oSection1, 'Valor Nota Referencia	' ,'ALM','VALOR NOTA REFERENCIA'  ,"@E 99,999,999.99",13)
	TRCell():New( oSection1, 'T/P'                    ,'ALM','T/P',                    "@!" 			 ,15)
	TRCell():New( oSection1, 'Orienta玢o'	          ,'ALM','ORIENTACAO',             "@!" 			 ,15)
	TRCell():New( oSection1, 'NF Venda Troca'         ,'ALM','NF VENDA TROCA'	,      "@!" 			 ,15)
	TRCell():New( oSection1, 'Motivo'                 ,'ALM','MOTIVO',                 "@!" 			 ,15)
	TRCell():New( oSection1, 'CLIENTE MILENIUM'       ,'ALM','CLIENTE MILENIUM',       "@!" 			 ,15)
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
    LocaL lApaga := .f.
	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	                    
   
	IF Select("xREL") > 0
		xREL->(dbCloseArea())
	Endif             

	cQuery := " SELECT * , space(12) as CODMIL "
	cQuery += " FROM "+RetSqlName("ZAS")+" with (NOLOCK)"
	cQuery += " WHERE ZAS_DATA  BETWEEN '"+DtoS(MV_PAR05)+"' AND '"+DtoS(MV_PAR06)+"' AND ZAS_CODCLI  BETWEEN '"+MV_PAR01+"' "  
    cQuery += "      AND '"+MV_PAR02+"' AND ZAS_NOTAFE BETWEEN '"+MV_PAR03+"'  AND '"+MV_PAR04+"' AND D_E_L_E_T_ = '' "
 
    TCQUERY cQuery NEW ALIAS xREL 
    xREL->(DBGOTOP())
    
  While xREL->(!EOF())      
	
	  IF oReport:Cancel()
		 Exit
	  EndIf
	  IF Select("XTBR") > 0
		 XTBR->(dbCloseArea())
	  Endif             
   	  cQry := "SELECT E1_XPEDWEB, * FROM SE1010 NOLOCK WHERE E1_NUM = '"+xREL->ZAS_NTREFE+"' AND E1_CLIENTE = '"+xREL->ZAS_CODCLI+"' AND E1_LOJA = '"+xREL->ZAS_LOJCLI+"' AND D_E_L_E_T_  <> '*' "
      TCQUERY cQry NEW ALIAS xTBR 
      xTBR->(DBGOTOP())
      If xTBR->(!eof()) 
         xPedido := xTBR->E1_XPEDWEB
      Else
         xPedido := xREL->ZAS_CODPED
      Endif    
	  oReport:IncMeter()
	  oReport:IncMeter()

		oSection1:Cell("Data"):SetValue(CTOD(SUBSTR(xREL->ZAS_DATA,7,2)+'/'+SUBSTR(xREL->ZAS_DATA,5,2)+'/'+SUBSTR(xREL->ZAS_DATA,3,2))) // xREL->ZAS_DATA) 	
		oSection1:Cell("Data"):SetAlign("LEFT")

		oSection1:Cell("NOTA"):SetValue(xREL->ZAS_NOTAFE) 	
		oSection1:Cell("NOTA"):SetAlign("LEFT")

		oSection1:Cell("NOTA REFERENCIA"):SetValue(xREL->ZAS_NTREFE) 	
		oSection1:Cell("NOTA REFERENCIA"):SetAlign("LEFT")

		oSection1:Cell("DATA NOTA REFEREN"):SetValue(CTOD(SUBSTR(xREL->ZAS_EMISAO,7,2)+'/'+SUBSTR(xREL->ZAS_EMISAO,5,2)+'/'+SUBSTR(xREL->ZAS_EMISAO,3,2))) //xREL->ZAS_EMISAO) 	
		oSection1:Cell("DATA NOTA REFEREN"):SetAlign("LEFT")

		oSection1:Cell("PEDIDO DE VENDA"):SetValue(xPedido) 
		oSection1:Cell("PEDIDO DE VENDA"):SetAlign("LEFT")

		oSection1:Cell("DATA DO PEDIDO"):SetValue(CTOD(SUBSTR(xREL->ZAS_DTAPED,7,2)+'/'+SUBSTR(xREL->ZAS_DTAPED,5,2)+'/'+SUBSTR(xREL->ZAS_DTAPED,3,2))) //xREL->ZAS_DTAPED) 
		oSection1:Cell("DATA DO PEDIDO"):SetAlign("LEFT")

		oSection1:Cell("NSU_CARTAO"):SetValue(xREL->ZAS_NSUCAR) 	
		oSection1:Cell("NSU_CARTAO"):SetAlign("LEFT")

		oSection1:Cell("AUTORIZACAO_CARTAO"):SetValue(xREL->ZAS_AUTCAR)	
		oSection1:Cell("AUTORIZACAO_CARTAO"):SetAlign("LEFT")

		oSection1:Cell("COD_CLIENTE"):SetValue(xREL->ZAS_CODCLI) 
		oSection1:Cell("COD_CLIENTE"):SetAlign("LEFT")

		oSection1:Cell("NOME"):SetValue(xREL->ZAS_NOMECL)	
		oSection1:Cell("NOME"):SetAlign("LEFT")

		oSection1:Cell("CPF"):SetValue(xREL->ZAS_CPF)
		oSection1:Cell("CPF"):SetAlign("LEFT")

		oSection1:Cell("TIPO_PGTO"):SetValue(xREL->ZAS_TPPAGT) 		
		oSection1:Cell("TIPO_PGTO"):SetAlign("LEFT")

		oSection1:Cell("Valor Nota de Devolucao"):SetValue(xREL->ZAS_VLNFDE) 	
		oSection1:Cell("Valor Nota de Devolucao"):SetAlign("LEFT")

		oSection1:Cell("Valor Nota Referencia"):SetValue(xREL->ZAS_VLNFRE)	
		oSection1:Cell("Valor Nota Referencia"):SetAlign("LEFT")

		oSection1:Cell("T/P"):SetValue(xREL->ZAS_TP)	
		oSection1:Cell("T/P"):SetAlign("LEFT")

		oSection1:Cell("Orienta玢o"):SetValue(xREL->ZAS_ORIENT) 	
		oSection1:Cell("Orienta玢o"):SetAlign("LEFT")

		oSection1:Cell("NF Venda Troca"):SetValue(xREL->ZAS_NFVTRO) 	
		oSection1:Cell("NF Venda Troca"):SetAlign("LEFT")

		oSection1:Cell("Motivo"):SetValue(xREL->ZAS_MOTIVO)								
		oSection1:Cell("Motivo"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE MILENIUM"):SetValue(xREL->ZAS_CLIMIL)	 
		oSection1:Cell("CLIENTE MILENIUM"):SetAlign("LEFT")
	
		oSection1:PrintLine()
		
		xREL->(DBSKIP()) 
	enddo
	xREL->(DBCLOSEAREA())
Return( Nil )


/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Verifica/cria SX1 a partir de matriz para verificacao          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Especifico para Clientes Microsiga                             潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

//	Aadd(aPergs,{"Ate Cliente","","","mv_ch8","C",6,0,0,"G","","MV_PAR08","","","","ZZZZZZ","","","","","","","","","","","","","","","","","","","","","SE1","","","",""})
	Aadd(aPergs,{"Cliente de ?","","","mv_ch1","C",6,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Cliente At�?","","","mv_ch2","C",6,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Nota Inicial","","","mv_ch3","C",9,0,0,"G","","mv_par03","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Nota Final"  ,"","","mv_ch4","C",9,0,0,"G","","mv_par04","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data de? "   ,"","","mv_ch5","D",8,0,0,"G","","mv_par05","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data ate? "  ,"","","mv_ch6","D",8,0,0,"G","","mv_par06","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif
	
	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif
	
	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."
		
		
		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif
		
		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif
		
		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next
