#INCLUDE "PROTHEUS.CH"
/*
Le arquivo CSV (planilha com dados de titulos a pagar
Conforme seguencia abaixo:
PREFIXO	NUMERO PARCELA TIPO	NATUREZA PORTADOR FORNECEDOR LOJA NOME_FORNECEDOR EMISSAO VENCIMENTO VALOR CODIGO_DE_BARRAS	HISTORICO

*/

USER FUNCTION IMPAPG()
	Local cTitAnt   := '0'
	Local cForAnt   := '0'
	Local cInvalido := "N"
	Local aCPagar   := {}
	Local aCrateio  := {}
	Local aErros    := {}
	Local NSEQEV    := 0
	Local NSEQEZ    := 0
	Local lSim      := .T.
	Local nlin      := 0
	Local nTotal    := 0
	Local nqtd      := 0
	LOCAL aAuxEv    :={} // array auxiliar do rateio multinaturezas
	LOCAL aRatEvEz  :={} //array do rateio multinaturezas
	Local nvlCC     := 0
	LOCAL aAuxEz    := {} // Array auxiliar de multiplos centros de custo
	LOCAL aRatEz    := {} // Array do rateio de centro de custo em multiplas naturezas
	Local aRet      := {} //
	Local cTexto    := ""
	Local i			:= 0

	PRIVATE lMsHelpAuto := .F.
	PRIVATE lMsErroAuto := .F.


	nVisibilidade := Nil													// Exibe todos os diret�rios do servidor e do cliente
	//		nVisibilidade := GETF_ONLYSERVER										// Somente exibe o diret�rio do servidor
	//		nVisibilidade := GETF_LOCALFLOPPY + GETF_LOCALHARD + GETF_NETWORKDRIVE	// Somente exibe o diret�rio do cliente

	cArquivo := cGetFile(	"Todos os Arquivos (*.*)     | *.* | ","Selecione um arquivo",0,,.T.,nVisibilidade, .T. )
	//nHandle := FT_FUse("C:\NEWBRIDGE\TESTES\CPAGTO.TXT")
	nHandle := FT_FUse(cArquivo)
	if nHandle = -1
		MsgStop(" Problemas na abertura do Arquivo")
		return
	endif
	FT_FGoTop()
	While !FT_FEOF()
		cLine := FT_FReadLn()
		if Len(alltrim(cLine))== 0
			FT_FSKIP()
			Loop
		Endif
		nRecno := FT_FRecno()
		cLinha := StrTran(cLine,'"',"'")
		cLinha := '{"'+cLinha+'"}'
		//adiciona o cLinha no array trocando o delimitador ; por , para ser reconhecido como elementos de um array
		cLinha := StrTran(cLinha,';','","')
		aAdd(aRet, &cLinha)
		nlin++
		If nlin >=2
			dEmis := ctod(aRet[1][10])   //14
			dVenc := ctod(aRet[1][11])   //16
    		cFornec := alltrim(aRet[1][7]) // Posicione("SA2",3,xFilial("SA2")+PADR(cCnpj, 14 ),"A2_COD")
			cA2LOJA := alltrim(aRet[1][8]) //SA2->A2_LOJA
			if Len(alltrim(cFornec)) == 0  //cFornec == " "
				aadd(aErros,{aRet[1][7]+ "  Cnpj ou Codigo n�o encontrado no cadastro de Fornecedores -  Linha ref. "+strzero(nlin,4) })
				lSim := .F.
			endif
			cNatureza := Posicione("SED",1,xFilial("SED")+PADR(aRet[1][5],10) ,"ED_CODIGO") //cNatureza := Posicione("SED",1,xFilial("SED")+PADR(aRet[1][8],10) ,"ED_CODIGO")
			if Len(alltrim(cNatureza)) == 0  //cNatureza == " "
				aadd(aErros,{aRet[1][5]+ "  C�digo da Natureza n�o encontrado  -  Linha ref. "+strzero(nlin,4)})
				lSim := .T.
			endif
			//cCcd        := Posicione("CTT",1, xFilial("CTT")+PADR(aRet[1][11], TamSX3("CTT_CUSTO")[1]),"CTT_CUSTO")
			nTotal:=VAL(aRet[1][12])
			cValor:= alltrim(aRet[1][12])
			npos2 := At(',',Alltrim(cValor))
			if npos2 == 0
				nValor := nValor := val(cValor)
			Else
				nInt   := substr(Alltrim(aRet[1][12]),1,npos2-1)
				nCent  := substr(Alltrim(aRet[1][12]),npos2+1,15)
				if substr(nCent,2,1) ==" "
					nCent:= nCent+'0'
				Endif
				cValor := nInt+nCent
				nValor := val(cValor)/100
			Endif
			If nValor < 0
			   nValor := nValor*-1
			Endif
			AADD(aCpagar, {'E2_NUM', 	    ALLTRIM(aRet[1][2])	  ,NIL})
			AADD(aCpagar, {'E2_FORNECE', 	cFornec 	  ,NIL})
			AADD(aCpagar, {'E2_LOJA', 		cA2LOJA	      ,NIL})
			AADD(aCpagar, {'E2_TIPO', 	    alltrim(aret[1][4])  ,NIL})  //aRet[5]
			AADD(aCpagar, {'E2_PARCELA', 	alltrim(aRet[1][3])	  ,NIL})
			AADD(aCpagar, {'E2_PREFIXO' , 	aret[1][1]    ,NIL})
			AADD(aCpagar, {'E2_NATUREZ', 	aRet[1][5]    ,NIL})
			AADD(aCpagar, {'E2_EMISSAO',	dEmis         ,NIL})
			AADD(aCpagar, {'E2_VALOR',    	nValor        ,NIL})
			AADD(aCpagar, {'E2_VENCTO' , 	dVenc         ,NIL})
			AADD(aCpagar, {'E2_MULTNAT',    "2"           ,NIL})
			AADD(aCpagar, {'E2_HIST'   ,    aRet[1][14]   ,NIL})                           //aRet[1][14]+' - '+aRet[1][48]   ,NIL})
			AADD(aCpagar, {"E2_ORIGEM" , "FINA050"     , Nil})
			U_grava02(aCPagar,nTotal)   //Verifica se existe realmente os registos, se n�o encontrar ir� grava-los
			cxTexto:=""
			for i := 1 to len(aErros)
				cxTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
			Next i
		Endif
		FT_FSKIP()
		aRet      := {}
		aCpagar   := {}

	Enddo
	aRet      := {}
	lSim      := .T.
	//cTitAnt   := aRet[1][4]
	aCpagar   := {}
	aCCusto   := {}
	aNatureza := {}
	aAuxEv    := {} // array auxiliar do rateio multinaturezas
	aRatEvEz  := {} //array do rateio multinaturezas
	aAuxEz    := {} // Array auxiliar de multiplos centros de custo
	aRatEz    := {} //A
	cFornec   := " "
	cNatureza := " "
	cCcd      := " "
	NSEQEZ    := 0
	NSEQEV    := 0
	cTexto    := cxTexto

	If len(aErros) >0
		cTexto := "Log de Importa��o, Dados n�o Importados. "+CHR(13)+CHR(10)
		for i := 1 to len(aErros)
			cTexto+= aErros[i][1]+"  "+CHR(13)+CHR(10)
		Next i
		__cFileLog := MemoWrite(Criatrab(,.f.)+".LOG",cTexto)

	Endif
	if len(alltrim(cTexto)) == 0
		MsgInfo("Fim da Importa��o -  com sucesso        "+CHR(13)+CHR(10)+cTexto)

	Else
		MsgInfo("Fim da Importa��o -  Verifique erros   "+CHR(13)+CHR(10)+cTexto)
	Endif
Return()



User Function grava02(aCpagar,nTotal)
	Local aArea		:= GetArea()

	_cNr      := PADR(aCpagar[1][2], 9)
	_cPrefixo := PADR(aCpagar[6][2], 3)
	_cParcela := PADR(aCpagar[5][2], 3)
	_cTipo    := PADR(aCpagar[4][2], 3)
	_cForn    := PADR(aCpagar[2][2], 6)
	_cLoja    := PADR(aCpagar[3][2], 4)
	Dbselectarea("SE2")
	SE2->(Dbgobottom())
	SE2->(DbSetOrder(1))
	iF .not. SE2->(DBSEEK(xFilial("SE2")+_cPrefixo+_cNr+_cParcela+_cTipo+_cForn+_cLoja))
		//	mSGsTOP("N�o Achei Viu!")
		SE2->(Reclock("SE2",.T.))
	ELSE
	   SE2->(Reclock("SE2",.F.))
	ENDIF
		SE2->E2_FILIAL    := xFilial("SE2")
		SE2->E2_NUM       := _cNr
		SE2->E2_FORNECE   := _cForn
		SE2->E2_NOMFOR    := Posicione("SA2",1, xFilial("SA2")+PADR(_cForn, 6)+_cLoja,"A2_NREDUZ")
		SE2->E2_LOJA      := _cLoja
		SE2->E2_TIPO      := _cTipo
		SE2->E2_PARCELA   := _cParcela
		SE2->E2_PREFIXO   := _cPrefixo
		SE2->E2_NATUREZ   := aCpagar[07][2]
		SE2->E2_EMISSAO   := aCpagar[08][2]
		SE2->E2_VALOR     := aCpagar[09][2]
		SE2->E2_VENCTO    := aCpagar[10][2]
		SE2->E2_VENCREA   := aCpagar[10][2]
		SE2->E2_VENCORI   := aCpagar[10][2]
		SE2->E2_MULTNAT   := aCpagar[11][2]
		SE2->E2_HIST      := aCpagar[12][2]
		SE2->E2_CCD       := aCpagar[13][2]
		SE2->E2_SALDO     := aCpagar[09][2]
		SE2->E2_RATEIO    := 'N'
		SE2->E2_VLCRUZ    := aCpagar[09][2]
		SE2->E2_OCORREN   := '01'
		SE2->E2_ORIGEM    := aCpagar[13][2]
		SE2->E2_MOEDA     :=  1
		SE2->E2_FLUXO     := 'S'
		SE2->E2_DESDOBR   := 'N'
		SE2->E2_PROJPMS   := '2'
		SE2->E2_DIRF      := '2'
		SE2->E2_MODSPB    := '1'
		SE2->E2_FILORIG   := XFILIAL("SE2")
		SE2->E2_PRETPIS   := '1'
		SE2->E2_PRETCOF   := '1'
		SE2->E2_PRETCSL   := '1'
		SE2->E2_BASEPIS   := aCpagar[09][2]
		SE2->E2_BASECOF   := aCpagar[09][2]
		SE2->E2_BASECSL   := aCpagar[09][2]
		SE2->E2_MDRTISS   := '1'
		SE2->E2_FRETISS   := '1'
		SE2->E2_APLVLMN   := '1'
		SE2->E2_TRETISS   := '1'
		SE2->E2_BASEISS   := aCpagar[09][2]
		SE2->E2_BASEIRF   := aCpagar[09][2]
		SE2->E2_DATAAGE   := DDATABASE
		SE2->E2_BASEINS   := aCpagar[09][2]
		SE2->E2_TEMDOCS   := '2'
		SE2->E2_STATLIB   := '01'
		SE2->(MsUnLock("SE2"))
	//Endif
	RestArea(aArea)
Return()

