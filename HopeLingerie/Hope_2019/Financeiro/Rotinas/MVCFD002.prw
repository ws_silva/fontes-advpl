#include 'parmtype.ch'
#Include 'Protheus.ch'
#Include 'FwMVCDef.ch'
#INCLUDE "TBICONN.CH"
#INCLUDE "RWMAKE.CH"
#Include "ApWizard.ch"
#include "fileio.ch" 

#Define cEOL	Chr(13)+Chr(10)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA001  �Autor  �Bruno Parreira      � Data �  08/11/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para geracao dos t�tulos de Fundo de Marketing     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/


User Function MVCFD002()
Local aArea := GetArea()
If MsgYesno("Confirma gera��o dos Novos T�tulos de Fundo de Marketing? " )
	Processa({|| u_fdmkg()})
Endif
RestArea(aArea)	
Return

USER FUNCTION fdmkg()
Local _astru     := {}
Local _afields   := {}     
Local _carq      := ""         
Local cQuery     := ""
Local xFornec    := ""
Local xLoja      := ""
Local xVecto     := ctod(space(8))
Local nValor     := 0
Local xItens     := ""
Local xRazao     := ""
Local cFiltraSCR
Local ca097User  := RetCodUsr()
Local Fazer      := .f.
Local aRet       := {}
Local nlin       := 0
Local aMenu      :=	{}
Local oFont1  := TFont():New("Verdana",,012,,.T.,,,,,.F.,.F.)
Local oFont2  := TFont():New("Verdana",,012,,.F.,,,,,.F.,.F.)
Local aColunas := {{"MK_OK"       ,,""       	           },;
{"ZAV_CODIGO" ,,"CODIGO"       ,"@!" },;
{"ZAV_LOJA"   ,,"Loja"         ,"@!" },;
{"ZAV_CNPJ"   ,,"CNPJ"         ,"@!" },;
{"ZAV_NOMECL" ,,"Nome Cliente" ,"@!" },;
{"ZAV_END"    ,,"Endere�o "    ,"@!" },;
{"ZAV_VLRLIQ" ,,"Valor Liquido","@E 99,999,999.99"},;
{"ZAV_VLRMKT" ,,"Valor Fundo " ,"@E 99,999,999.99"},;
{"ZAV_MESREF" ,,"Mes Ref�ncia" ,"@!" },;
{"ZAV_VENCTO" ,,"Vencimento"   ,"@D" },;
{"ZAV_TITULO" ,,"Titulo"   ,	"@!" }}

Private lMarcar  := .F.
Private oMark2
Private cPerg     := "MVC_FDMKG0"
Private aRotina   := {} 
Private lContinua := .F.
Private atuar     := ""
Private aHeader := {}
Private aCOLS := {}
Private aGets := {}
Private aTela := {}
Private aREG := {}
Private cCadastro := "Registros de Titulos de Fundo de Marketing "
Private aRotina := {}
Private oCliente
Private oTotal
Private cCliente := ""
Private nTotal := 0
Private bCampo := {|nField| FieldName(nField) }
Private aSize := {}
Private aInfo := {}
Private aObj := {}
Private aPObj := {}
Private aPGet := {}
Private cGet1 := Space(25)
Private cGet2 := Space(25)
Private lContinua := .F.
Private atuar     := ""
Private cMark     := GetMark()
Private lInverte  := .F.
Private oChk
Private lChkSel   := .F.
Private lRefresh  := .T.
Private oDlgT
Private onVlrCom
Private onVlrSld
Private onVlrMar
Private onVlrfIL
Private onVlrSob
Private nVlrCom   := 0
Private nVlrSld   := 0
Private nVlrMar   := 0
Private nVlrfIL   := 0
Private nVlrSob   := 0
Private lMarcar  := .F.
Private oMark3
Private cPerg     := "MVC_FDMKG0"
Private aRotina   := {}
Private lContinua := .F.
Private atuar     := ""
Private cMark2    := GetMark()

// Retorna a �rea �til das janelas Protheus
aSize := MsAdvSize()
// Ser� utilizado tr�s �reas na janela
// 1� - Enchoice, sendo 80 pontos pixel
// 2� - MsGetDados, o que sobrar em pontos pixel � para este objeto
// 3� - Rodap� que � a pr�pria janela, sendo 15 pontos pixel
AADD( aObj, { 100, 080, .T., .F. })
AADD( aObj, { 100, 100, .T., .T. })
AADD( aObj, { 100, 015, .T., .F. })
aInfo := { aSize[1], aSize[2], aSize[3], aSize[4], 3, 3 }
aPObj := MsObjSize( aInfo, aObj )
// C�lculo autom�tico de dimens�es dos objetos MSGET
aPGet := MsObjGetPos( (aSize[3] - aSize[1]), 315, { {004, 024, 240, 270} } )

If Select("XTRB") > 0 
	DbSelectArea("XTRB")
	XTRB->(DbCloseArea())
Endif
//AjustaSX1(cPerg)
If !Pergunte(cPerg,.T.)
	Return
Endif


cQuery := "SELECT  SPACE(2) AS MK_OK,ZAV_CODIGO,ZAV_LOJA,ZAV_CNPJ,ZAV_NOMECL,ZAV_END,ZAV_VLRLIQ,ZAV_VLRMKT,	
cQuery += CRLF + "ZAV_MESREF,ZAV_VENCTO,ZAV_TITULO" 
cQuery += CRLF + "from "+RetSqlName("ZAV")+" ZAV WITH (NOLOCK)  WHERE ZAV.D_E_L_E_T_ = '' and ZAV_FILIAL = '"+xFilial("ZAV")+"' " 
cQuery += CRLF + "AND ZAV_MESREF = '"+ALLTRIM(MV_PAR01)+"' AND ZAV_STATUS = '1'  ORDER BY ZAV_NOMECL "

MemoWrite("HPGerTIT_2.txt",cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"XTRB", .F., .T.)       
XTRB->(dbgotop())

If XTRB->(Eof()) 
	MsgStop("Nenhum registro encontrado para gerar T�tulos, Verifique") 
	Return()
Endif   
If Select("XMKT") > 0 
	DbSelectArea("XMKT")
	XMKT->(DbCloseArea())
Endif

//Estrutura da tabela temporaria
AADD(_astru,{"MK_OK"      ,"C",  2,0})
AADD(_astru,{"ZAV_CODIGO" ,"C",  6,0})
AADD(_astru,{"ZAV_LOJA"   ,"C",  4,0})
AADD(_astru,{"ZAV_CNPJ"   ,"C", 18,0})
AADD(_astru,{"ZAV_NOMECL" ,"C", 30,0})
AADD(_astru,{"ZAV_END"    ,"C", 35,0})
AADD(_astru,{"ZAV_VLRLIQ" ,"N", 16,2})
AADD(_astru,{"ZAV_VLRMKT" ,"N", 16,2})
AADD(_astru,{"ZAV_MESREF" ,"C",  8,6})
AADD(_astru,{"ZAV_VENCTO" ,"D", 08,0})
AADD(_astru,{"ZAV_TITULO" ,"C",  9,0})

cArqTrab  := CriaTrab(_astru)
dbUseArea( .T.,, cArqTrab, "XMKT", .F., .F. )

While XTRB->(!EOF())
	DbSelectArea("XMKT")        
	XMKT->(RecLock("XMKT",.T.))
	XMKT->ZAV_CODIGO := XTRB->ZAV_CODIGO   
	XMKT->ZAV_LOJA   := XTRB->ZAV_LOJA
	XMKT->ZAV_CNPJ   := XTRB->ZAV_CNPJ
	XMKT->ZAV_NOMECL := XTRB->ZAV_NOMECL
	XMKT->ZAV_END    := XTRB->ZAV_END
	XMKT->ZAV_VLRLIQ := XTRB->ZAV_VLRLIQ
	XMKT->ZAV_VLRMKT := XTRB->ZAV_VLRMKT
	XMKT->ZAV_MESREF := XTRB->ZAV_MESREF
	XMKT->ZAV_VENCTO := ctod(substr(XTRB->ZAV_VENCTO,7,2)+'/'+substr(XTRB->ZAV_VENCTO,5,2)+'/'+substr(XTRB->ZAV_VENCTO,3,2))
	XMKT->ZAV_TITULO := XTRB->ZAV_TITULO
	XMKT->(MsUnlock())        
	XTRB->(DbSkip())
Enddo	
//	oMark2 := FWMarkBrowse():New()
//oMark2:SetAlias('XMKT')        


//define as colunas para o browse


DbSelectArea("XMKT")
XMKT->(DbGotop())
DEFINE MSDIALOG oDlgT TITLE "HOPE - Gerar T�tulos no Financeiro referente Fundo de Marketing" FROM aSize[7],100 To aSize[6],aSize[5] COLORS 0, 16777215 PIXEL Style DS_MODALFRAME
cMarca    := GetMark()
@ 005, 005 SAY oSay2 PROMPT "Lista dos Titulos A serem gerados de Fundo de marketing." SIZE 242, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
oMark := MsSelect():New("XMKT","MK_OK",,aColunas,lInverte,cMarca,{ 015, 003, 242, 570})
oMark:bAval:= {||(HPFDK002E(cMarca),oMark:oBrowse:Refresh())}
oMark:oBrowse:Refresh(.F.)
oMark:oBrowse:lHasMark    := .T.
oMark:oBrowse:lCanAllMark := .T.
oMark:oBrowse:bAllMark := {|| U_HPFDK002F(cMarca),oMark:oBrowse:Refresh()}
@ 250, 005 BUTTON oButton1 PROMPT " Gerar T�tulos " SIZE 055, 013 OF oDlgT ACTION lretG:= HPFDK002H(cMarca,1) PIXEL
@ 250, 085 BUTTON oButton1 PROMPT "  Fechar   "  	SIZE 035, 013 OF oDlgT ACTION LretG:= HPFDK002H(cMarca,5) PIXEL
@ 270, 190 Say oSay7 prompt " Valor Total Marcado para Gerar:" SIZE 100, 007 OF oDlgT FONT oFont1 COLORS CLR_RED PIXEL
@ 270,315 MSGET onVlrMar VAR nVlrMar When .f. SIZE 060,08 OF oDlgT PIXEL Picture "@E 999,999,999.99"

ACTIVATE MSDIALOG oDlgT CENTERED
If !Empty('XMKT')
	XMKT->(DbCloseArea())
Endif
Return()

Static Function HPFDK002H(cMarca,xopc)
Private nSaldoComp:= 0
If xOpc <> 5
	dbSelectArea("XMKT")
	ProcRegua(XMKT->(RecCount()))
	XMKT->(dbGoTop())
	While XMKT->(!Eof())
		IncProc("Aguarde... Gerando T�tulos" )
		If Alltrim(XMKT->MK_OK) <> ""
			If xOpc == 1
				lGer := U_wGerBol02(XMKT->ZAV_CODIGO,XMKT->ZAV_LOJA,XMKT->ZAV_MESREF)  // Gerar T�tulo
			Endif
		Endif
		XMKT->(DBSKIP())
	End
	msgStop("Processo Finalizado!")
Endif
onVlrMar:Refresh()
XMKT->(dbGoTop())
oMark:oBrowse:Refresh(.F.)
oDlgT:Refresh(.F.)
oDlgT:End()
Return(Nil)

Static Function HPFDK002E(cMarca)
If Alltrim(XMKT->MK_OK) == ""
	XMKT->(RecLock( "XMKT", .F. ))
	XMKT->MK_OK   := cMarca
	XMKT->(MsUnlock())
	nVlrMar :=	nVlrMar+XMKT->ZAV_VLRMKT
	//msgStop("Diminuindo saldo Pois foi Marcado Aqui")
Else
	nVlrSob := 0
	XMKT->(RecLock( "XMKT", .F. ))
	XMKT->MK_OK   := Space(2)
	XMKT->(MsUnlock())
	nVlrMar :=	nVlrMar-XMKT->ZAV_VLRMKT
	//msgStop("Somando saldo Pois foi Marcado Aqui")
Endif
onVlrMar:Refresh()
oMark:oBrowse:Refresh(.F.)
oDlgT:Refresh(.F.)
Return

User Function HPFDK002F(cMarca)
XMKT->(dbGoTop())
nVlrMar := 0
While XMKT->(!Eof())
	If Alltrim(XMKT->MK_OK) == ""
		XMKT->(RecLock( "XMKT", .F. ))
		XMKT->MK_OK   := cMarca
		XMKT->(MsUnlock())
		nVlrMar :=	nVlrMar+XMKT->ZAV_VLRMKT
		//msgStop("Diminuindo saldo Pois foi Marcado")
	Else
		XMKT->(RecLock( "XMKT", .F. ))
		XMKT->MK_OK   := Space(2)
		XMKT->(MsUnlock())
		//nVlrMar :=	nVlrMar-XMKT->ZAV_VLRMKT
		//msgStop("Somonado o saldo Pois foi desMarcado")
	Endif
	XMKT->(DBSKIP())
End
onVlrMar:Refresh()
XMKT->(dbGoTop())
oMark:oBrowse:Refresh(.F.)
oDlgT:Refresh(.F.)
Return(Nil)


User Function wGerBol02(cCODIGO,cLOJA,cMESREF)        //U_wprocprc("M","215853")
Local  aArea     := GetArea()
Local  XNATUREZA := "" //GETMV("HP_NATFMK")
Local  DEMISSAO  := GERDTAE(MV_PAR01) 
Local  XHIST     := '1% - FUNDO OPERA��O DE PROPAGANDA'
Local lRet       := .f.
Local lProsegue:= .t.

U_criamv("HP_NATFMK")
XNATUREZA := GETMV("HP_NATFMK")
DEMISSAO  := GERDTAE(MV_PAR01) 
XHIST     := iif(MV_PAR03 == 1 ,'1% - FUNDO OPERA��O DE PROPAGANDA','1% - FUNDO OPERA��O DE PROPAGANDA')
lMsErroAuto := .F.
If cCODIGO == XMKT->ZAV_CODIGO .and. cLOJA == XMKT->ZAV_LOJA .and. cMESREF == XMKT->ZAV_MESREF

	If !Empty(XMKT->MK_OK) //Se diferente de vazio, foi marcado
		//XNUMERO  := GetSxeNum("SE1", "E1_NUM") // Obtem o numero do Titulo.
		//XNUMERO  := U_HFING002('ND') //  ---  PASSAGEM DO PARAMETRO ('ND') - PARA A FUN��O QUE GERAO NUMERO DO T�TULO
		//If MV_PAR03 == 1
		XPREF := "FMK"
		//Else
		//	XPREF := "FMK" //"RFT"
		//Endif  
		
		//dbSelectArea("SE1")
		//DbSetOrder(1)
		//If DbSeek(xFilial("SE1")+XPREF+XNUMERO)
		//	MsgInfo("Titulo j� gerado! Excluir o titulo "+Alltrim(XNUMERO)+" e gerar novamente", "Hope")
		
		If Empty(Alltrim(XMKT->ZAV_TITULO))
			lProssegue:= .t.
		Else
			lProssegue:= MsgYesNo("Nota de D�bito j� foi gerada anteriormente: " + XMKT->ZAV_TITULO + cEOL + "Deseja prosseguir com a inclus�o de uma nova ND?")
		Endif
		
		If !lProssegue
			Return
		Else
			
			XNUMERO:= fGetSe1N('ND')

			aReg := {{"E1_PREFIXO",XPREF                      ,Nil},;
			{"E1_NUM    ",XNUMERO           ,Nil},;
			{"E1_PARCELA",""                ,Nil},;
			{"E1_TIPO   ","NDC"             ,Nil},;
			{"E1_NATUREZ",XNATUREZA         ,Nil},;
			{"E1_CLIENTE",XMKT->ZAV_CODIGO  ,Nil},;
			{"E1_LOJA   ",XMKT->ZAV_LOJA    ,Nil},;
			{"E1_EMISSAO",DEMISSAO          ,Nil},;
			{"E1_VENCTO ",MV_PAR02          ,Nil},;
			{"E1_VALOR  ",XMKT->ZAV_VLRMKT  ,Nil},;
			{"E1_XTPPAG ",'BOLETO IMPRESSO' ,Nil},;
			{"E1_HIST   ",XHIST             ,Nil} }

			MSExecAuto({|x,y| fina040(x,y)},aReg,3)

			If lMsErroAuto
				MostraErro()
				ConfirmSx8()
			Else
				ConfirmSx8()
				cQuery := "UPDATE "+RETSQLNAME("ZAV")+" SET ZAV_STATUS = '2', ZAV_TITULO = '"+XNUMERO+"' WHERE ZAV_CODIGO = '"+XMKT->ZAV_CODIGO+"' AND ZAV_LOJA = '"+XMKT->ZAV_LOJA+"' AND ZAV_MESREF = '"+XMKT->ZAV_MESREF+"' "
				MemoWrite("HPGerTIT_3.txt",cQuery) 
				TcSqlExec( cQuery )
				TcSqlExec( "COMMIT" )
				lRet := .T.				     	
			Endif
			lMsErroAuto := .F.
		Endif
	Endif  
Endif
Return(lRet)

Static Function GERDTAE(xref)
Local dRet := ddatabase
Local cMes := Substr(xref,1,2)
Local cAno := Substr(xref,6,2)
Local cDia := ""
If cMes $ '01/03/05/07/08/10/12'
	cDia := '31'
Endif
If cMes $ '04/06/09/11'  
	cDia := '30'
Endif
If cMes == '02' 
	cDia := '28'
Endif     
dRet := ctod(cDia+'/'+cMes+'/'+cAno) 
Return(dRet)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA001  �Autor  �Bruno Parreira      � Data �  08/11/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para geracao dos t�tulos de Fundo de Marketing     ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fGetSe1N(cParam)
Local nPos := len(cParam)
Local nRst := 0
Local aAreaSE1:= SE1->(GetArea())

nRst := 9 - nPos 
cNumSe1Cod:= cParam+SUBSTR(GETSXENUM("SE1","E1_NUM"),nPos+1,nRst) 
ConfirmSX8()
   
DbSelectArea("SE1")
SE1->(DbSetOrder(1))
SE1->(dbGoTop())

While SE1->(DbSeek(xfilial("SE1")+cNumSe1Cod))
	cNumSe1Cod:= cParam+SUBSTR(GETSXENUM("SE1","E1_NUM"),nPos+1,nRst) 
	ConfirmSX8()
    SE1->(dbGoTop())
Enddo

RestArea(aAreaSE1)

Return(cNumSe1Cod)

