#Include 'totvs.ch'

//Fun��o para gerar Bordero autom�tico
User Function GerBord()
Local aArea      := GetArea()
Static cAliasQry := GetNextAlias()
Static cNBor    := SuperGetMV("MV_BORAUAT", ," ")
Static cBanco   := ' '
Static cAgencia := ' '
Static cConta   := ' '


//E1_XTPPAG = 'BOLETO IMPRESSO '
BeginSql Alias cAliasQry
	
    SELECT E1_NUMBOR,E1_NUMBCO,R_E_C_N_O_ AS REGISTRO FROM SE1010 WHERE   E1_EMISSAO >= '20180419' 
    AND len(E1_NUMBCO) = 5 ORDER BY E1_NUM
		
EndSql
	
(cAliasQry)->(DbGoTop())
While (cAliasQry)->(!EOF())

  If LEN(ALLTRIM((cAliasQry)->E1_NUMBCO)) == 5
    DbSelectArea("SE1") 
	SE1->(DbGoTo((cAliasQry)->REGISTRO))
	SE1->(RecLock("SE1",.F.))
	SE1->E1_NUMBCO   := strzero(val(SE1->E1_NUMBCO),8) 
	SE1->E1_MOVIMENT := CTOD("19/04/18")
	SE1->E1_SITUACA	 := '1'
	SE1->E1_NUMBOR	 := '000351'  // STRZERO(VAL(SE1->E1_NUMBOR),8)
	SE1->E1_DATABOR	 := CTOD("19/04/18")
	SE1->(MsUnlock())
    
    //ATUALIZA BORDERO.
	SEA->(RecLock("SEA",.T.))
	SEA->EA_FILIAL   := XFilial("SEA")
	SEA->EA_PREFIXO  := SE1->E1_PREFIXO
	SEA->EA_NUM      := SE1->E1_NUM
	SEA->EA_PARCELA  := SE1->E1_PARCELA
	SEA->EA_PORTADO  := SE1->E1_PORTADO
	SEA->EA_AGEDEP   := SE1->E1_AGEDEP
	SEA->EA_NUMBOR   := SE1->E1_NUMBOR
	SEA->EA_DATABOR  := CTOD("19/04/18")
	SEA->EA_TIPO     := SE1->E1_TIPO
	SEA->EA_CART     := 'R'
	SEA->EA_NUMCON   := SE1->E1_CONTA
	SEA->EA_SITUACA  := '1'
	SEA->EA_SITUANT  := '0'
	SEA->EA_FILORIG  := XFilial("SEA")
	SEA->EA_ORIGEM   := SE1->E1_ORIGEM
	SEA->(MsUnlock())
  Endif	
 (cAliasQry)->(Dbskip())
EndDo

(cAliasQry)->(DbCloseArea())

RestArea(aArea)

Return