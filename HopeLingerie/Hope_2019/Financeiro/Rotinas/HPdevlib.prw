#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"
#INCLUDE "RWMAKE.CH"

/*/{Protheus.doc} RF013
Relat髍io de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

User Function hpdevlib()

	Private oReport
	Private cPergCont	:= 'HPDEVLIB02'

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf
	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'xREL', 'RELA敲O DE DEVOLU钦ES APROVADAS ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELA敲O DE DEVOLU钦ES APROVADAS' } )
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()

	oSection1 := TRSection():New( oReport, 'RELA敲O DE DEVOLU钦ES APROVADAS ', { 'xREL', 'ZAX','ZAZ','SA1'})

	TRCell():New( oSection1, 'PREFIXO' ,'xREL','PREFIXO' ,"@!" 			    ,03)
	TRCell():New( oSection1, 'NUMERO'  ,'xREL','NUMERO'  ,"@!" 			    ,09)
	TRCell():New( oSection1, 'PARCELA' ,'xREL','PARCELA' ,"@!" 			    ,03)
	TRCell():New( oSection1, 'TIPO'    ,'xREL','TIPO'    ,"@!"              ,03)
	TRCell():New( oSection1, 'STATUS'  ,'xREL','STATUS'  ,"@!"              ,10)
	TRCell():New( oSection1, 'CLIENTE' ,'xREL','CLIENTE' ,"@!"              ,06)
	TRCell():New( oSection1, 'LOJA'    ,'xREL','LOJA'    ,"@!" 			    ,04)
	TRCell():New( oSection1, 'NOMECLI' ,'xREL','NOMECLI' ,"@!" 			    ,30)
	TRCell():New( oSection1, 'VALOR'   ,'xREL','VALOR'   ,"@E 99,999,999.99",13)
	TRCell():New( oSection1, 'ESTADO'  ,'xREL','ESTADO'  ,"@!" 			    ,02)
	TRCell():New( oSection1, 'DATLIB'  ,'xREL','LIBERADO',"@D" 			    ,08)
	TRCell():New( oSection1, 'BAIXA'   ,'xREL','BAIXADO' ,"@D" 			    ,08)
	TRCell():New( oSection1, 'AUTORI'  ,'xREL','AUTORI'  ,"@!" 			    ,20)
	TRCell():New( oSection1, 'CANAL'   ,'xREL','CANAL'   ,"@!" 			    ,20)
	TRCell():New( oSection1, 'DIVISAO' ,'xREL','DIVISAO' ,"@!" 			    ,20)
	TRCell():New( oSection1, 'MREGIAO' ,'xREL','MREGIAO' ,"@!" 			    ,20)


Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
    LocaL lApaga := .f.
	oSection1:Init()
	oSection1:SetHeaderSection(.T.)

	IF Select("xREL") > 0
		xREL->(dbCloseArea())
	Endif

	cQuery := " SELECT E1_PREFIXO,E1_NUM,E1_PARCELA,E1_TIPO,E1_XSTATUS,E1_CLIENTE,E1_LOJA ,E1_NOMCLI,E1_VALOR,E1_XDESREG,A1_EST,ZAX_DTALIB,E1_BAIXA,E1_XCANALD, E1_XNONDIV,ZAX_MREGIA, ZAX_AUTORI"
	cQuery += " FROM SE1010 E1, SA1010 A1,ZAX010 AX  with (NOLOCK) WHERE E1_XSTATUS = 'Proc.Finan'  AND A1_COD = E1_CLIENTE AND A1_LOJA = E1_LOJA "
    cQuery += " AND ZAX_NFENTR = E1_NUM AND ZAX_SERIE = E1_PREFIXO AND ZAX_DTALIB BETWEEN '"+DtoS(MV_PAR01)+"' AND '"+DtoS(MV_PAR02)+"' AND ZAX_STATUS >= 5 "
    cQuery += " AND A1.D_E_L_E_T_ <> '*' AND E1.D_E_L_E_T_ <> '*' AND AX.D_E_L_E_T_ <> '*' "

    TCQUERY cQuery NEW ALIAS xREL
    xREL->(DBGOTOP())

  While xREL->(!EOF())

	  IF oReport:Cancel()
		 Exit
	  EndIf

	  oReport:IncMeter()
	  oReport:IncMeter()


		oSection1:Cell("PREFIXO"):SetValue(xREL->E1_PREFIXO)
		oSection1:Cell("PREFIXO"):SetAlign("LEFT")

		oSection1:Cell("NUMERO"):SetValue(xREL->E1_NUM)
		oSection1:Cell("NUMERO"):SetAlign("LEFT")

		oSection1:Cell("PARCELA"):SetValue(xREL->E1_PARCELA)
		oSection1:Cell("PARCELA"):SetAlign("LEFT")

		oSection1:Cell("TIPO"):SetValue(xREL->E1_TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")

		oSection1:Cell("STATUS"):SetValue(xREL->E1_XSTATUS)
 		oSection1:Cell("STATUS"):SetAlign("LEFT")

 		oSection1:Cell("CLIENTE"):SetValue(xREL->E1_CLIENTE)
 		oSection1:Cell("CLIENTE"):SetAlign("LEFT")

 		oSection1:Cell("LOJA"):SetValue(xREL->E1_LOJA)
 		oSection1:Cell("LOJA"):SetAlign("LEFT")

 		oSection1:Cell("NOMECLI"):SetValue(xREL->E1_NOMCLI)
		oSection1:Cell("NOMECLI"):SetAlign("LEFT")

		oSection1:Cell("VALOR"):SetValue(xREL->E1_VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")

		oSection1:Cell("ESTADO"):SetValue(xREL->A1_EST)
		oSection1:Cell("ESTADO"):SetAlign("LEFT")

		oSection1:Cell("DATLIB"):SetValue(CTOD(SUBSTR(xREL->ZAX_DTALIB,7,2)+'/'+SUBSTR(xREL->ZAX_DTALIB,5,2)+'/'+SUBSTR(xREL->ZAX_DTALIB,3,2)))
        oSection1:Cell("DATLIB"):SetAlign("LEFT")

        oSection1:Cell("BAIXA"):SetValue(CTOD(SUBSTR(xREL->E1_BAIXA,7,2)+'/'+SUBSTR(xREL->E1_BAIXA,5,2)+'/'+SUBSTR(xREL->E1_BAIXA,3,2)))
        oSection1:Cell("BAIXA"):SetAlign("LEFT")

        oSection1:Cell("AUTORI"):SetValue(xREL->ZAX_AUTORI)
		oSection1:Cell("AUTORI"):SetAlign("LEFT")


	    oSection1:Cell("CANAL"):SetValue(xREL->E1_XCANALD)
        oSection1:Cell("CANAL"):SetAlign("LEFT")

        oSection1:Cell("DIVISAO"):SetValue(xREL->E1_XNONDIV)
        oSection1:Cell("DIVISAO"):SetAlign("LEFT")

        oSection1:Cell("MREGIAO"):SetValue(xREL->ZAX_MREGIA)
        oSection1:Cell("MREGIAO"):SetAlign("LEFT")

    	oSection1:PrintLine()

		xREL->(DBSKIP())
	enddo
	xREL->(DBCLOSEAREA())
Return( Nil )


/*/
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
北谀哪哪哪哪穆哪哪哪哪哪哪哪履哪哪哪履哪哪哪哪哪哪哪哪哪哪哪履哪哪穆哪哪哪哪哪勘�
北矲un噮o    � AjustaSx1    � Autor � Microsiga            	� Data � 13/10/03 潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪聊哪哪哪聊哪哪哪哪哪哪哪哪哪哪哪聊哪哪牧哪哪哪哪哪幢�
北矰escri噮o � Verifica/cria SX1 a partir de matriz para verificacao          潮�
北媚哪哪哪哪呐哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪幢�
北砋so       � Especifico para Clientes Microsiga                             潮�
北滥哪哪哪哪牧哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪哪俦�
北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北北�
哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌哌
/*/
Static Function AjustaSX1(cPerg)

Local _sAlias	:= Alias()
Local aCposSX1	:= {}
Local aPergs	:= {}
Local nX 		:= 0
Local lAltera	:= .F.
Local nCondicao
Local cKey		:= ""
Local nJ		:= 0

aCposSX1:={"X1_PERGUNT","X1_PERSPA","X1_PERENG","X1_VARIAVL","X1_TIPO","X1_TAMANHO",;
"X1_DECIMAL","X1_PRESEL","X1_GSC","X1_VALID",;
"X1_VAR01","X1_DEF01","X1_DEFSPA1","X1_DEFENG1","X1_CNT01",;
"X1_VAR02","X1_DEF02","X1_DEFSPA2","X1_DEFENG2","X1_CNT02",;
"X1_VAR03","X1_DEF03","X1_DEFSPA3","X1_DEFENG3","X1_CNT03",;
"X1_VAR04","X1_DEF04","X1_DEFSPA4","X1_DEFENG4","X1_CNT04",;
"X1_VAR05","X1_DEF05","X1_DEFSPA5","X1_DEFENG5","X1_CNT05",;
"X1_F3", "X1_GRPSXG", "X1_PYME","X1_HELP" }

	Aadd(aPergs,{"Data Lib. de?  ","","","mv_ch1","D",8,0,0,"G","","mv_par01","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})
	Aadd(aPergs,{"Data Lib. ate? ","","","mv_ch2","D",8,0,0,"G","","mv_par02","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""})

dbSelectArea("SX1")
dbSetOrder(1)
For nX:=1 to Len(aPergs)
	lAltera := .F.
	If MsSeek(cPerg+Right(aPergs[nX][11], 2))
		If (ValType(aPergs[nX][Len(aPergs[nx])]) = "B" .And.;
			Eval(aPergs[nX][Len(aPergs[nx])], aPergs[nX] ))
			aPergs[nX] := ASize(aPergs[nX], Len(aPergs[nX]) - 1)
			lAltera := .T.
		Endif
	Endif

	If ! lAltera .And. Found() .And. X1_TIPO <> aPergs[nX][5]
		lAltera := .T.		// Garanto que o tipo da pergunta esteja correto
	Endif

	If ! Found() .Or. lAltera
		RecLock("SX1",If(lAltera, .F., .T.))
		Replace X1_GRUPO with cPerg
		Replace X1_ORDEM with Right(aPergs[nX][11], 2)
		For nj:=1 to Len(aCposSX1)
			If 	Len(aPergs[nX]) >= nJ .And. aPergs[nX][nJ] <> Nil .And.;
				FieldPos(AllTrim(aCposSX1[nJ])) > 0
				Replace &(AllTrim(aCposSX1[nJ])) With aPergs[nx][nj]
			Endif
		Next nj
		MsUnlock()
		cKey := "P."+AllTrim(X1_GRUPO)+AllTrim(X1_ORDEM)+"."


		If ValType(aPergs[nx][Len(aPergs[nx])]) = "A"
			aHelpSpa := aPergs[nx][Len(aPergs[nx])]
		Else
			aHelpSpa := {}
		Endif

		If ValType(aPergs[nx][Len(aPergs[nx])-1]) = "A"
			aHelpEng := aPergs[nx][Len(aPergs[nx])-1]
		Else
			aHelpEng := {}
		Endif

		If ValType(aPergs[nx][Len(aPergs[nx])-2]) = "A"
			aHelpPor := aPergs[nx][Len(aPergs[nx])-2]
		Else
			aHelpPor := {}
		Endif

		PutSX1Help(cKey,aHelpPor,aHelpEng,aHelpSpa)
	Endif
Next

