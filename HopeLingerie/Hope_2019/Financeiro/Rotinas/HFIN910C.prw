#INCLUDE "RWMAKE.CH"
#include "protheus.ch"
#include "TbiConn.ch"
#include "TbiCode.ch"
#include "topconn.ch"
#Include 'ApWebEx.ch'
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �HFIN910C  � Autor � Daniel Pereira de Souza�Data �05/08/2009���
�������������������������������������������������������������������������Ĵ��
���Locacao   � CSA              �Contato � 				   			      ���
�������������������������������������������������������������������������Ĵ��
���Descricao �Rotina que efetua a atualiza��o da ZAK com a SE1            ���
���Descricao �pelo arquivo do SITFEF e os dados do Contas a Receber       ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Aplicacao � Somente em Banco de Dados com uso de TopConnect            ���
�������������������������������������������������������������������������Ĵ��
���Uso       �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Analista Resp.�  Data  � Bops � Manutencao Efetuada                    ���
�������������������������������������������������������������������������Ĵ��
���              �  /  /  �      �                                        ���
���              �  /  /  �      �                                        ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFIN910C(nPar)
Local aButtons			:= {}												//Variavel para a inclusao de botoes na EnchoiceBar
Local aColsAux			:= {}												//Array auxiliar que guarda os valores padroes
Local aConc				:= {}												//Array que contem todos os registros que estao disponiveis no Folder Conciliar
Local aConcPar			:= {}												//Array que contem todos os registros que estao disponiveis no Folder Conc. Parcialmente
Local aConcMan			:= {}												//Array que contem todos os registros que estao disponiveis no Folder Conc. Manualmente
Local aNaoConc			:= {}												//Array que contem todos os registros que estao disponiveis no Folder Nao Conciliadas
Local aIndic			:= {}												//Array que armazena os itens parecidos para Conciliacao Parcial
Local lRet					:= .T.		 										//Variavel para tratamento dos retornos de funcoes
Local cPerg				:= "HFIN910C"										//Grupo de Perguntas para filtro de informacoes para a Tela de Conciliacao do SITEF

Local cQry				:= ""								//Instrucao de query no banco
Local cAliasSitef		:= GetNextAlias()         		//Variavel que recebe o proximo Alias disponivel

Local aArea		    	:= GetArea()						//Array que armazena a ultima area utilizada
Local dDataIni          := Space(08)
Local dDataFin          := Space(08)
Local aDados			:= {}
Local TIPOPGTO			:= 'VERIFICAR BANDEIRA E CC'
Local TIPOCRED			:= 'C'
Local TPCOBR			:= 'C'
Local PREVIS			:= 'F'
Local EFETUA			:= 'F'
Local CLASSIF			:= "01.01"
Local PLANOC			:= "RECEITAS DE VENDA"
Local NPTAXA            := 0

Static __lDefTop		:= NIL

default nPar := 1
//��������������������������������������������������������������Ŀ
//� Inicializa o log de processamento                            �
//����������������������������������������������������������������

ProcLogIni( aButtons )

If nPar == 2
  	Prepare Environment Empresa "01" Filial "0102"
   	dDataIni        := DTOS(date())
    dDataFin        := DTOS(Date())
    criaSx1(cPerg)
Else
    If !Pergunte(cPerg,.T.)
       Return
    EndIf
    dDataIni        := DTOS(MV_PAR03)
    dDataFin        := DTOS(MV_PAR04)
Endif

//Zero as variaveis antes de atualizar
aConc		:= {}
aConcPar	:= {}
aNaoConc	:= {}
aConcMan	:= {}
aIndic		:= {}
nReg        := 0
If __lDefTop == Nil
   __lDefTop :=  .t.
EndIf

If __lDefTop

   cQry := "SELECT   E1_NSUTEF, E1_XNSU ,   E1_XAUTNSU ,E1_FILIAL ,E1_PREFIXO, E1_NUM,E1_PARCELA, E1_TIPO, E1_NATUREZ, E1_PORTADO, E1_AGEDEP, E1_CLIENTE, E1_LOJA, E1_NOMCLI , "
   cQry += "E1_XCOD  , E1_EMISSAO ,E1_VENCTO, E1_VENCREA, E1_VALOR,  E1_XFORREC,  E1_INDICE, E1_BAIXA, E1_HIST, E1_SITUACA, E1_CONTRAT, E1_SALDO, E1_VLRLIQ, E1_VENCORI,"
   cQry += "E1_MOEDA , E1_PEDIDO , E1_VLCRUZ, E1_TIPODES, E1_CARTAO, E1_CARTVAL, E1_CARTAUT, E1_ACRESC, E1_SDACRES, E1_DECRESC, E1_SDDECRE, E1_FORMREC, E1_DOCTEF, E1_SDOC,"
   cQry += "E1_SDOCREC, E1_XDI  , E1_XCI, E1_XNSU ,  E1_XAUTNSU,   E1_USERLGI ,       E1_USERLGA,  E1_XTPPAG, E1_XCARTID ,E1_XTXCC ,E1_VLTXCC, R_E_C_N_O_ AS REGISTRO "
   cQry += "FROM " + AllTrim(RetSqlName("SE1")) + " SE1 WHERE D_E_L_E_T_ <> '*' "
   cQry += "AND SE1.E1_XNSU <> '' "
   cQry += "AND SE1.E1_SALDO > 0  "
   cQry += "AND SE1.E1_FILIAL BETWEEN '" + MV_PAR01 + "' AND '" + MV_PAR02 + "' "
   cQry += "AND SE1.E1_EMISSAO BETWEEN '" + dDataIni + "' AND '" + dDataFin+ "'"
   cQry += " ORDER BY "
   cQry += "  E1_NUM "

    cQry := ChangeQuery(cQry)

    dbUseArea(.T.,"TOPCONN",TCGenQry(,,cQry),cAliasSitef,.F.,.T.)

  
    dbSelectArea("ZAK")
    ZAK->(dbSetOrder(1))

    If !(cAliasSitef)->(Eof())

        While !(cAliasSitef)->(Eof())
			//Limpa o array auxiliar
            aColsAux := {}

     //         D_E_L_E_T_ R_E_C_N_O_  R_E_C_D_E_L_

            TIPOPGTO := Posicione ( "SC5" , 1 , xFilial('SC5')+(cAliasSitef)->E1_PEDIDO , "C5_XDESCOD"   )
            NPTAXA   := Posicione ( "SE4" , 1 , XFilial('SE4')+SC5->C5_CONDPAG,"E4_XTXCC" )
            xDtaEmis := ctod(substr((cAliasSitef)->E1_EMISSAO,7,2)+'/'+substr((cAliasSitef)->E1_EMISSAO,5,2)+'/'+substr((cAliasSitef)->E1_EMISSAO,3,2))
            xDtaVenc := ctod(substr((cAliasSitef)->E1_VENCREA,7,2)+'/'+substr((cAliasSitef)->E1_VENCREA,5,2)+'/'+substr((cAliasSitef)->E1_VENCREA,3,2))
//            VLTAXA   := (cAliasSitef)->E1_XTXCC  //Round(((cAliasSitef)->E1_VALOR*NPTAXA/100),2)
//            VALLIQ   := (cAliasSitef)->E1_VALLIQ
            aAdd(aColsAux,{'ZAK_FILIAL',(cAliasSitef)->E1_FILIAL })
            aAdd(aColsAux,{'ZAK_STATUS','1' })
            aAdd(aColsAux,{'ZAK_NUM',(cAliasSitef)->E1_NUM+'/'+(cAliasSitef)->E1_PARCELA })
            aAdd(aColsAux,{'ZAK_CODCLI',(cAliasSitef)->E1_CLIENTE })
            aAdd(aColsAux,{'ZAK_LOJA',(cAliasSitef)->E1_LOJA })
            aAdd(aColsAux,{'ZAK_NOMECL',(cAliasSitef)->E1_NOMCLI })
            aAdd(aColsAux,{'ZAK_EMISSA',xDtaEmis })                   //(cAliasSitef)->E1_EMISSAO })
            aAdd(aColsAux,{'ZAK_VENCTO',xDtaVenc })                 //(cAliasSitef)->E1_VENCREA })
//            aAdd(aColsAux,{'ZAK_PAGTO' ,DATAPGTO })
            aAdd(aColsAux,{'ZAK_VLINI' ,(cAliasSitef)->E1_VALOR })
            aAdd(aColsAux,{'ZAK_VLLIQ' ,(cAliasSitef)->E1_VLRLIQ})
//            aAdd(aColsAux,{'ZAK_VLPAGO',VALPAGO })
            aAdd(aColsAux,{'ZAK_TPPAGO',TIPOPGTO })
            aAdd(aColsAux,{'ZAK_CARTAO',(cAliasSitef)->E1_CARTAO })
            aAdd(aColsAux,{'ZAK_NSUCAR',(cAliasSitef)->E1_XNSU })
            aAdd(aColsAux,{'ZAK_AUTCAR',(cAliasSitef)->E1_XAUTNSU })
            aAdd(aColsAux,{'ZAK_PTXACC',(cAliasSitef)->E1_XTXCC   })
            aAdd(aColsAux,{'ZAK_VTXACC',(cAliasSitef)->E1_VLTXCC  })
            aAdd(aColsAux,{'ZAK_FILORI',(cAliasSitef)->E1_FILIAL })
            aAdd(aColsAux,{'ZAK_PREFIX',(cAliasSitef)->E1_PREFIXO })
            aAdd(aColsAux,{'ZAK_PARCEL',(cAliasSitef)->E1_PARCELA })
            aAdd(aColsAux,{'ZAK_TPCRED',TIPOCRED })
            aAdd(aColsAux,{'ZAK_MOEDA',(cAliasSitef)->E1_MOEDA })
            aAdd(aColsAux,{'ZAK_ACRDCR',(cAliasSitef)->E1_ACRESC-(cAliasSitef)->E1_DECRESC })
 //           aAdd(aColsAux,{'ZAK_BANCOT',(cAliasSitef)->E1_PORTADO })
 //           aAdd(aColsAux,{'ZAK_DTCHEQ',DTACHEQ })
 //           aAdd(aColsAux,{'ZAK_NCHEQU',NUMCHEQ })
            aAdd(aColsAux,{'ZAK_DOCORI',(cAliasSitef)->E1_NUM })
            aAdd(aColsAux,{'ZAK_TPCOBR',TPCOBR })
            aAdd(aColsAux,{'ZAK_PREVIS',PREVIS })
            aAdd(aColsAux,{'ZAK_EFETUA',EFETUA })
            aAdd(aColsAux,{'ZAK_HISTOR',(cAliasSitef)->E1_HIST })
            aAdd(aColsAux,{'ZAK_FILBAI',(cAliasSitef)->E1_FILIAL })
            aAdd(aColsAux,{'ZAK_CLASSF',CLASSIF })
            aAdd(aColsAux,{'ZAK_PLANOC',PLANOC })
            aAdd(aColsAux,{'ZAK_USRLGI',(cAliasSitef)->E1_USERLGI })
            aAdd(aColsAux,{'ZAK_USRLGA',(cAliasSitef)->E1_USERLGA })
            aAdd(aColsAux,{'ZAK_REGIST',(cAliasSitef)->REGISTRO })

            nReg++
            (cAliasSitef)->(dbSkip())
            Processa({|| A910GrvZak(aColsAux)},"Aguarde!.... Emiss�o "+DtoC(xDtaEmis)+" Qtde  "+strzero(nReg,5)+" " , "Atualizando Tabela ZKA " ) //"Aguarde..."#"Atualizando Tabela FIF..."

        EndDo
    Else
        MsgStop("Aten��o, nenhum t�tulos cart�o de Credito encontrado no Per�odo passado no Par�metro!","Verifique!")
    EndIf

    (cAliasSitef)->(dbCloseArea())

    RestArea(aArea)
End
If nPar == 2
	Reset Environment
EndIf

Return(lRet)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �A910Grvzak�Autor  �Rafael Rosa da Silva� Data �  08/12/09   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao que efetua a gravacao dos registrosn na tabela ZAK	  ���
���          �(Conciliacao do SITEF)									  ���
�������������������������������������������������������������������������͹��
���Uso       �															  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function A910GrvZak(aDados)

	Local nI	:= 0			//Variavel para contador de registros
    Local nFator := If(Len(aDados) > 2000, 1000, 1)

	dbSelectArea("ZAK")
//    ZAK->(DbSertOrder(1))
    ZAK->(DbOrderNickName("ZAKNUMSTAT"))
	CONOUTR("Conciliador Concil - FINA910B - A910GrvArq - INICIO GRAVANDO ARQUIVO - " + DToC(Date()) + " - Hora: " + TIME())


	ProcRegua(Len(aDados)/nFator)

	BeginTran()

			IncProc("Gravando os Registros...  " + "(" + ALLTRIM(aDados[3][2]) + "/" + AllTrim(Str(Len(aDados))) + ")")		//"Gravando os Registros..."
		    ZAK->(Dbgotop())
		    ZAK->(Dbseek(xFilial("ZAK")+PADR(ALLTRIM(aDados[3][2]),12)))
		    iF PADR(ALLTRIM(aDados[3][2]),12) == ZAK->ZAK_NUM
		       ZAK->(RecLock("ZAK",.F.))
		    Else
			   ZAK->(RecLock("ZAK",.T.))
		 	Endif
			For nI := 1 to Len(aDados)
				ZAK->&(aDados[nI][1]) := aDados[nI][2]
		 	Next nI
			ZAK->( MsUnLock() )
	EndTran()

	CONOUTR("Conciliador TEF - FINA910B - A910GrvArq - FIM GRAVANDO ARQUIVO - " + DToC(Date()) + " - Hora: " + TIME())

Return


/*/
����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � CriaSX1  � Autor � Microsiga             � Data � 10.05.08 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Cria e analisa grupo de perguntas                          ���
�������������������������������������������������������������������������Ĵ��
���Sintaxe   �                                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros� cPerg                                                      ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
����������������������������������������������������������������������������
*/

Static Function CriaSX1( cPerg )
Local i,j
Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
Local aTamSX3	:= {}
Local aRegs     := {}
cPerg:= padr(cPerg,10)
//�����������������������������������������������������������������������������������������Ŀ
//� Grava as perguntas no arquivo SX1                                                       �
//�������������������������������������������������������������������������������������������
aTamSX3	:= TAMSX3( "E1_FILIAL" )
AADD(aRegs,{cPerg,	"01","Da Filial  ?" ,"Da Filial  ?","Da Filial   ?","mv_ch1", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR01","","","",""    ,"","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"02","Ate Filial ?" ,"Ate Filial ?","Ate Filial  ?","mv_ch2", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR02","","","","ZZZZ","","","","","","","","","","","","","","","","","","","","","","S","","","","" })

aTamSX3	:= TAMSX3( "E1_EMISSAO" )
AADD(aRegs,{cPerg,	"03","Da Emissao ?" ,"Da Emissao ?","Da Emissao ?" ,"mv_ch3", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR03","","","","","","","","","","","","","","","","","","","","","","","","","","S","","","","" })
AADD(aRegs,{cPerg,	"04","Ate Emissao?" ,"Ate Emissao?","Ate Emissao?" ,"mv_ch4", aTamSX3[3],aTamSx3[1],aTamSX3[2],0,"G" ,"","MV_PAR04","","","","","","","","","","","","","","","","","","","","","","","","","","S","","","","" })


DbSelectArea("SX1")
SX1->(DbSetOrder(1))

For I := 1 To Len(aRegs)
	If 	!dbSeek(cPerg+aRegs[i,2])
		RecLock("SX1",.T.)
		For j:=1 to FCount()
			IF j <= Len(aRegs[i])
				FieldPut(j,aRegs[i,j])
			EndIf
		Next

		MsUnLock()
	EndIf
Next

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return()


USER FUNCTION REFAREG()
Local aAreaAtu	:= GetArea()
lOCAL cQuery    := {}
dbSelectArea("ZAK")
ZAK->(DBGOTOP())
WHILE ZAK->(!EOF())
    if ZAK_STATUS <= '3'
       cQuery    := "SELECT  SE1.R_E_C_N_O_ AS REGISTRO  FROM ZAK010 ZAK, SE1010 SE1 WITH (NOLOCK ) WHERE SUBSTRING(ZAK_NUM,1,9) = E1_NUM AND SUBSTRING(ZAK_NUM,11,3) = E1_PARCELA "
       cQuery    += "AND ZAK_CODCLI = E1_CLIENTE AND SE1.D_E_L_E_T_  <> '*' AND  ZAK_NUM = '"+ZAK->ZAK_NUM+"'    "
       IF Select("XTBR") > 0
		 XTBR->(dbCloseArea())
	   Endif             
       TCQUERY cQuery NEW ALIAS xTBR 
       xTBR->(DBGOTOP())
       If xTBR->(!eof())
          ZAK->(RecLock("ZAK",.F.))
          ZAK->ZAK_REGIST := xTBR->REGISTRO
		  ZAK->( MsUnLock() )
	   Endif
    Endif
    ZAK->(DBskip())
Enddo    
MSGSTOP('FINALIZADO COM SUCESSO!')
RestArea( aAreaAtu )
Return()
