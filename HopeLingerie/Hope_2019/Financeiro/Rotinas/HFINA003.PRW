#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "FILEIO.CH"

/*/
------------------------------------------------------------------
{Protheus.doc} HFINA003
Execucao via Job da Integra��o Protheus x Equals

@author Sergio S. Fuzinaka
@since Dez/2019
@version 1.00
------------------------------------------------------------------
/*/
User Function HJobEquals()

   Local lJob  := .T.

   U_HFINA003( lJob )

Return

/*/
------------------------------------------------------------------
{Protheus.doc} HFINA003
Rotina de Integra��o Protheus x Equals

@author Sergio S. Fuzinaka
@since Dez/2019
@version 1.00
------------------------------------------------------------------
/*/
User Function HFINA003( lJob )

   Local cDir        := ""
   Local aRet        := {}
	Local aPerg       := {}

   Private cCadastro := "Exporta arquivo Equals"
   Private aLog      := {}
   Private cMsgLog   := ""

   Default lJob      := .F.

   // Processamento via Job
   If lJob

      // Configuracao de ambiente
      RpcClearEnv()
      RpcSetType( 3 )
      RpcSetEnv( "01", "0101" )

      cDir  := GetMV( "DIR_EQUALS", .T., "f:\equals\upload\venda-interna\" )
      //cDir  := GetMV( "DIR_EQUALS", .T., "c:\equals\upload\venda-interna\" )        

      printLog( "Inicializando job..." )
      FileProc( lJob, cDir, dDataBase, ( dDataBase - 1 ) )
      printLog( "Finalizando job..." )

   Else

      cDir  := GetMV( "DIR_EQUALS", .T., "f:\equals\upload\venda-interna\" )
      //cDir  := GetMV( "DIR_EQUALS", .T., "c:\equals\upload\venda-interna\" )        

      aAdd(aPerg,{1,"Pasta"      ,cDir,      "",   "",      "",   '.F.',  100, .F.})
      aAdd(aPerg,{1,"Data de"    ,dDataBase, PesqPict("SE1", "E1_EMISSAO"),   "",      "",        , 50, .F.})
      aAdd(aPerg,{1,"Data at�"   ,dDataBase, PesqPict("SE1", "E1_EMISSAO"),   "",      "",        , 50, .F.})

      While .T.

         If ParamBox( aPerg, "Par�metros", @aRet,,,,,,,,.F. ) 

            If !Empty( aRet[2] ) .And. !Empty( aRet[3] )

               If aRet[3] < aRet[2]

                  MsgAlert( "Data Inicial maior que Data Final", "Aten��o!" )

               Else

                  While aRet[2] <= aRet[3]
                     Processa( {|| FileProc( lJob, aRet[1], aRet[2], (aRet[2] - 1) )}, "Aguarde...",  "Processando arquivo ..." )
                     aRet[2]++
                  Enddo

                  Exit

               Endif

            Else

               MsgAlert( "Data inv�lida!", "Aten��o!" )

            Endif

         Else

            Exit

         Endif

      Enddo

   Endif

   delClassIntf()

Return

Static Function FileProc( lJob, cDir, dData, dDataMov )

   Local cPedido  := ""
   Local cHora    := Time()
   Local cDtHora  := DToS(dData) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)
   Local cArq     := "equals_"+cDtHora+".txt"
   Local nHandle  := MSFCreate(cDir+cArq)
   Local cQuery   := ""
   Local lGerado  := .F.
   Local nTotReg  := 0
   Local nTotVlr  := 0
   Local cAssunto := "Log de Processamento da Exporta��o Equals"
   Local cMailDest:= GetMV( "EQ_MAIL", .T., "sergio.fuzinaka@hopelingerie.com.br" )
   //Local cMailDest:= "sergio.fuzinaka@hopelingerie.com.br"

   If Select( "TMP" ) > 0
      TMP->( dbCloseArea() )
   Endif

   cQuery := "SELECT ROW_NUMBER() OVER(ORDER BY E1_NUM ASC) AS NSR, " 
   cQuery += "ROW_NUMBER() OVER(ORDER BY E1_NUM ASC) AS NSA , " 
   cQuery += "REPLACE(CONVERT(VARCHAR, CAST(GETDATE() AS DATETIME),102),'.','') AS DATA_ARQUIVO, " 
   cQuery += "'000001' AS LOJA, "
   cQuery += "'0' AS TERMINAL, "
   cQuery += "'0' AS BANDEIRA, "
   cQuery += "'0' AS FORMA_DE_PAGAMENTO, "
   cQuery += "'0' AS NUMERO_CARTAO, "
   cQuery += "E1_EMISSAO AS DATA_VENDA, " 
   cQuery += "E1_XPEDWEB AS PEDIDO, " 
   cQuery += "E1_PREFIXO AS PREFIXO, "
   cQuery += "E1_NUM AS CUPOM, "
   cQuery += "E1_TIPO AS TIPO, "
   cQuery += "E1_NATUREZ AS NATUREZA, "
   cQuery += "E1_XNSU AS NSU, " 
   cQuery += "E1_XAUTNSU AS CODIGO_AUTORIZACAO, "
   cQuery += "'0' AS TID, "
   cQuery += "'0' AS LOTE_ADQUIRENTE, "
   cQuery += "E1_CLIENTE AS DOCUMENTO_CLIENTE, "
   cQuery += "E1_LOJA AS LOJA_CLIENTE, "   
   cQuery += "A1_NOME AS NOME_CLIENTE, "
   cQuery += "COUNT(E1_PARCELA) AS QTD_PARCELA, "
   cQuery += "REPLACE(CAST(SUM(E1_VALOR) / COUNT(E1_PARCELA) AS NUMERIC(11,2)) ,'.','') AS VALOR_PARCELA, "
   cQuery += "REPLACE(CAST(SUM(E1_VALOR) AS NUMERIC(11,2)),'.','') AS VALOR_VENDA, "
   cQuery += "'0' VALOR_ADICIONAL_1 , "
   cQuery += "'0' VALOR_ADICIONAL_2, "
   cQuery += "'0' AS TIPO_TRANSACAO, "
   cQuery += "'' AS  ADQUIRENTE, "
   cQuery += "'0' AS MEIO_CAPTURA, "
   cQuery += "'0' AS INFORMACA0_ADICIONAL_1_VENDA, "
   cQuery += "'0' AS INFORMACAO_ADICIONAL_2_VENDA, "
   cQuery += "'0' AS INFORMACAO_ADICIONAL_1_PAGAME, "
   cQuery += "'0' AS INFORMACAO_ADICIONAL_2_PAGAME  "
   cQuery += "FROM "+RetSqlName("SE1")+" SE1 JOIN "+RetSqlName("SA1")+" SA1 ON E1_CLIENTE = A1_COD AND E1_LOJA = A1_LOJA AND SA1.D_E_L_E_T_ = '' "
   cQuery += "WHERE SE1.D_E_L_E_T_ = '' "
   cQuery += "AND SE1.E1_MSFIL = '0101' "
   //cQuery += "AND SE1.E1_XPEDWEB <> '' "
   cQuery += "AND SE1.E1_XNSU <> '' AND ISNUMERIC(E1_XNSU) = 1 "
   cQuery += "AND SE1.E1_BAIXA = '' "
   cQuery += "AND SE1.E1_SALDO > 0 "
   cQuery += "AND SE1.E1_EMISSAO = '" + DtoS( dDataMov ) + "' "
   cQuery += "GROUP BY E1_CLIENTE,E1_LOJA,E1_EMISSAO,E1_XNSU,A1_CGC,A1_NOME,E1_PREFIXO,E1_NUM,E1_TIPO,E1_NATUREZ,E1_XPEDWEB,E1_XAUTNSU"

   dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.) 
   dbSelectArea("TMP")

   If nHandle < 0
      
      If lJob
         cMsgLog := "Falha durante a gera��o do arquivo [ " + cDir + cArq + " ]."
         AADD( aLog, cMsgLog )
         printLog( cMsgLog )
  		   U_SendMail( cMailDest, cAssunto, cMsgLog )
      Else
         MsgStop( "Falha durante a cria��o do arquivo em disco.", "Aten��o!" )
      Endif
   
   Else

      If !lJob
         ProcRegua( RecCount() )
      Endif
      
      While TMP->(!Eof())

            If !lJob
               IncProc()
            Endif

            If Empty( TMP->PEDIDO )
               cPedido := getNrPedido( TMP->NSU, TMP->CODIGO_AUTORIZACAO, TMP->PREFIXO, TMP->CUPOM )
            Else
               cPedido := TMP->PEDIDO
            Endif
         
            fWrite( nHandle, StrZero(TMP->NSR,6)+";"+;
                     StrZero(TMP->NSA,6)+";"+;
                     Alltrim(TMP->DATA_ARQUIVO)+";"+;
                     Alltrim(TMP->LOJA)+";"+;
                     Alltrim(TMP->TERMINAL)+";"+;
                     Alltrim(TMP->BANDEIRA)+";"+;
                     Alltrim(TMP->FORMA_DE_PAGAMENTO)+";"+;
                     Alltrim(TMP->NUMERO_CARTAO)+";"+;
                     Alltrim(TMP->DATA_VENDA)+";"+;
                     Alltrim(cPedido)+";"+;
                     Alltrim(TMP->PREFIXO+"|"+TMP->CUPOM+"|"+TMP->TIPO+"|"+TMP->NATUREZA+"|"+TMP->DOCUMENTO_CLIENTE+"|"+TMP->LOJA_CLIENTE)+";"+;
                     Alltrim(TMP->NSU)+";"+;
                     Alltrim(TMP->CODIGO_AUTORIZACAO)+";"+;
                     Alltrim(TMP->TID)+";"+;
                     Alltrim(TMP->LOTE_ADQUIRENTE)+";"+;
                     Alltrim(TMP->DOCUMENTO_CLIENTE)+";"+;
                     Alltrim(TMP->NOME_CLIENTE)+";"+;
                     Alltrim(Str(TMP->QTD_PARCELA,2))+";"+;
                     Alltrim(TMP->VALOR_PARCELA)+";"+;
                     Alltrim(TMP->VALOR_VENDA)+";"+;
                     Alltrim(TMP->VALOR_ADICIONAL_1)+";"+;
                     Alltrim(TMP->VALOR_ADICIONAL_2)+";"+;
                     Alltrim(TMP->TIPO_TRANSACAO)+";"+; 
                     Alltrim(TMP->ADQUIRENTE)+";"+;
                     Alltrim(TMP->MEIO_CAPTURA)+";"+;
                     Alltrim(TMP->INFORMACA0_ADICIONAL_1_VENDA)+";"+;
                     Alltrim(TMP->INFORMACA0_ADICIONAL_2_VENDA)+";"+;
                     Alltrim(TMP->INFORMACAO_ADICIONAL_1_PAGAME)+";"+;
                     Alltrim(TMP->INFORMACAO_ADICIONAL_2_PAGAME)+CRLF)

            nTotReg++
            nTotVlr += ( Val( TMP->VALOR_VENDA ) / 100 )

            TMP->( dbSkip() )

            lGerado := .T.
      
      Enddo    

      TMP->( dbCloseArea() )

      fClose(nHandle)

  		U_SendMail( cMailDest, cAssunto, getBody( ( cDir + cArq ), dDataMov, nTotReg, nTotVlr ) )

      _CopyFile( cDir + cArq, cDir + "\backup\" + cArq )

      If lGerado

         If lJob
            printLog( "Arquivo " + cDir + cArq + " gerado com sucesso!" )
         Else
            MsgInfo( "Arquivo " + cDir + cArq + " gerado com sucesso!" )
         Endif

      Else

         If lJob
            printLog( 'N�o h� dados a serem processados em "' + DToC( dData ) + '".' )
         Else
            MsgStop( 'N�o h� dados a serem processados em "' + DToC( dData ) + '".', "Aten��o!" )
         Endif

      Endif

   EndIf  

Return  

/*/
------------------------------------------------------------------
{Protheus.doc} getBody
Retorna o Texto do Body do Email

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function getBody( cFile, dDtMov, nTotReg, nTotVlr )

Local cHtml	:= ""
Local cData := DtoC( dDataBase )

cHtml := '<!DOCTYPE HTML>'
cHtml += '<html lang="pt-br">'

cHtml += '  <head>'
cHtml +=       '<meta charset="utf-8">'
cHtml +=    '</head>'

cHtml +=    '<body>'
cHtml +=       '<h3>Dados do Processamento:</h3>'
cHtml +=       '<ul>'
cHtml +=          '<li>Data/Hora do Processamento: ' + cData + " - " + Left( Time(), 5 ) + "hs" + '</li>'
cHtml +=          '<li>Data do Movimento: ' + DtoC( dDtMov ) + '</li>'
cHtml +=          '<li>Arquivo Exportado: ' + cFile + '</li>'
cHtml +=          '<li>Total de Registros: ' + cValToChar( nTotReg ) + '</li>'
cHtml +=          '<li>Total da Venda: R$ ' + Transform( nTotVlr, "@E 999,999,999.99" ) + '</li>'
cHtml +=       '</ul>'

cHtml +=    '</body>'
cHtml += '</html>'

Return( cHtml )

/*/
------------------------------------------------------------------
{Protheus.doc} getNrPedido
Retorna o numero do pedido

@author Sergio S. Fuzinaka
@since Jan/2020
@version 1.00
------------------------------------------------------------------
/*/
Static Function getNrPedido( cNSU, cAutNSU, cPrefixo, cNum )

   Local aArea    := GetArea()
   Local aAreaSE1 := SE1->( GetArea() )
   Local cPedido  := ( Alltrim( cNSU ) + "-" + Alltrim( cAutNSU ) )

   dbSelectArea( "SE1" )
   dbSetOrder( 1 )
   If dbSeek( xFilial( "SE1" + cPrefixo + cNum ) )
      If !Empty( SE1->E1_XPEDWEB )
         cPedido := SE1->E1_XPEDWEB
      Endif
   Endif

   RestArea( aAreaSE1 )
   RestArea( aArea )

Return( cPedido )

/*/
------------------------------------------------------------------
{Protheus.doc} printLog
Impressao no Console Log

@author Sergio S. Fuzinaka
@since Set/2019
@version 1.00
------------------------------------------------------------------
/*/
Static Function printLog( cMsg )
Return( ConOut( "[JOB][HFINA003][" + Dtoc( Date() ) + "-" + Time() + "][" + cMsg + "]" ) )
