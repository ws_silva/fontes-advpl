#INCLUDE "TOPCONN.CH"            
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOTVS.CH"
#INCLUDE "MSOLE.CH"
/*/{Protheus.doc} HPFINR01
Programa criado para gerar carta de anu�ncia
@author Robson Melo
@since  26/02/2019
@version P12
/*/
User Function HPFINR01() 

Local aStru   := {}        
Local cQuery  := ""  
Local cAliasQry:= GetNextAlias()
Local cPergCrt   := "HPFINR01"

If !Pergunte(cPergCrt, .T.)
	Return
Endif

cQuery := "SELECT E1_PREFIXO, E1_NUM, E1_PARCELA, E1_TIPO, E1_CLIENTE, E1_LOJA, E1_NOMCLI, E1_VALOR, E1_VENCTO"+ CRLF
cQuery += "  FROM "+RetSqlName("SE1")+" SE1 "         + CRLF
cQuery += " WHERE SE1.D_E_L_E_T_ <> '*' "             + CRLF
cQuery += "   AND E1_FILIAL = '"+xFilial("SE1")+"' "  + CRLF
cQuery += "   AND E1_SITUACA = 'F' "         		  + CRLF
cQuery += "   AND E1_BAIXA BETWEEN '"+DTOS(MV_PAR01)+"' AND '"+DTOS(MV_PAR02)+"'"                + CRLF
cQuery += "   AND E1_SALDO = 0 "                      + CRLF
cQuery += "ORDER BY E1_CLIENTE, E1_NUM, E1_PARCELA "                + CRLF

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),cAliasQry, .F., .T.)       

//Estrutura da tabela temporaria
AADD(aStru,{"E1_OK"     ,"C", 2,0})
AADD(aStru,{"E1_PREFIXO","C", 3,0})
AADD(aStru,{"E1_NUM"    ,"C", 9,0})
AADD(aStru,{"E1_PARCELA","C", 3,0})
AADD(aStru,{"E1_CLIENTE","C", 6,0})
AADD(aStru,{"E1_LOJA"   ,"C", 4,0})
AADD(aStru,{"E1_NOMCLI" ,"C",20,0}) 
AADD(aStru,{"E1_TIPO"   ,"C", 3,0})
AADD(aStru,{"E1_VALOR"  ,"N", 16,2})
AADD(aStru,{"E1_VENCTO" ,"C", 10,0})

If Select("XTRB") > 0
	XTRB->(dbCloseArea())
Endif

cArqTrab  := CriaTrab(aStru)
dbUseArea( .T.,, cArqTrab, "XTRB", .F., .F. )

Dbselectarea(cAliasQry)
(cAliasQry)->(DbGoTop())

While (cAliasQry)->(!EOF())
	DbSelectArea("XTRB")        
	RecLock("XTRB",.T.)             
	XTRB->E1_PREFIXO  := (cAliasQry)->E1_PREFIXO 
	XTRB->E1_PARCELA  := (cAliasQry)->E1_PARCELA                
	XTRB->E1_NUM      := (cAliasQry)->E1_NUM
	XTRB->E1_CLIENTE  := (cAliasQry)->E1_CLIENTE 
	XTRB->E1_LOJA     := (cAliasQry)->E1_LOJA 
	XTRB->E1_NOMCLI   := (cAliasQry)->E1_NOMCLI 
	XTRB->E1_TIPO     := (cAliasQry)->E1_TIPO 
	XTRB->E1_VALOR    := (cAliasQry)->E1_VALOR 
	XTRB->E1_VENCTO   := (cAliasQry)->E1_VENCTO 
	MsUnlock()        
	(cAliasQry)->(DbSkip())
EndDo

//Criando o MarkBrow
oMark := FWMarkBrowse():New()
oMark:SetAlias('XTRB')        

//define as colunas para o browse
aColunas := {;
{"Prefixo"   	  ,"E1_PREFIXO","C", 3,0,"@!"},;
{"Titulo"         ,"E1_NUM"    ,"C", 9,0,"@!"},;
{"Parcela"        ,"E1_PARCELA","C", 3,0,"@!"},;
{"Tipo"           ,"E1_TIPO"   ,"C", 3,0,"@!"},;
{"Cliente"        ,"E1_CLIENTE","C", 6,0,"@!"},;
{"Loja"           ,"E1_LOJA"   ,"C", 4,0,"@!"},;
{"Nome"           ,"E1_NOMCLI" ,"C",20,0,"@!"}}

//Seta as colunas para o browse
oMark:SetFields(aColunas)

//Setando sem�foro, descri��o e campo de mark
//oMark:lCanAllMark := .T.
oMark:SetSemaphore(.T.)
oMark:SetDescription('Seleciona Clientes')    
oMark:SetFieldMark('E1_OK')

oMark:AddButton("Carta de Anu�ncia","U_HPFINCRT(oMark)",,2,0)
oMark:AddButton("Marca/Descmarca Todos","U_HPMarkRec(oMark)",,2,0)

//Indica o Code-Block executado no clique do header da coluna de marca/desmarca
//oMark:SetAllMark({ || HFAT01INVE(oMark:Mark(),lMarcar := !lMarcar ),oMark:Refresh(.T.)})
oMark:SetAllMark({ || MarkRec(),oMark:Refresh(.T.)})
oMark:SetDoubleClick({|| Iif(Empty(Alltrim(XTRB->E1_OK)),oMark:Mark(),"  ")  })
oMark:SetTemporary()

//Ativando a janela
oMark:Activate() 

Return Nil 

/*/{Protheus.doc} HPMarkRec
Programa criado para marcar e desmarcar todos os registros
@author Robson Melo
@since  26/02/2019
@version P12
/*/
User Function HPMarkRec(oMark)

Local cMarca:= oMark:Mark()
Local aAreaTRB:= XTRB->(GetArea())

dbSelectArea("XTRB")
XTRB->(dbGoTop())
cMarca:= Iif(Empty(Alltrim(XTRB->E1_OK)),oMark:Mark(),"  ") 
While !XTRB->(Eof())
	RecLock("XTRB",.F.)   
    	XTRB->E1_OK:= cMarca
	MsUnlock()
    XTRB->(dbSkip())
EndDo

RestArea( aAreaTRB )

Return

/*/{Protheus.doc} HPFINR01
Programa criado para emitir a carta de anu�ncia
@author Robson Melo
@since  26/02/2019
@version P12
/*/
User Function HPFINCRT(oMark)
Local cMarca   := oMark:Mark()
Local aAreaTRB := XTRB->(GetArea())
Local cCliAnt  := ""
Local cSaveFile := cGetFile( '*.*' , 'Arquivos', 1, 'C:\', .F., nOR( GETF_LOCALHARD, GETF_LOCALFLOPPY, GETF_RETDIRECTORY ),.T., .T. )

//Local cSaveFile:= cGetFile("",OemToAnsi("Selecione o diret�rio "),0,"C:\",,GETF_LOCALHARD+GETF_NETWORKDRIVE)
Local cLocDot  := SUPERGETMV("HP_DIRDOT", .T., "P:\TI\ModelosDot\" )  //"C:\ModelosDot\ANUENCIA.DOT" "P:\TI\ModelosDot"
Local cNomeDot := SUPERGETMV("HP_DOTANU", .T., "ANUENCIA.dot" )
Local cFileMod := cLocDot + cNomeDot 
Local aDados   := {}
Local cCliente := ""
Local cVencto  := ""
Local cValor   := ""
Local cTitulo  := ""
Local cInfoCli := ""
Local cParcela := ""
Local nZ		:= 0

dbSelectArea("XTRB")
XTRB->(dbGoTop())

While !XTRB->(Eof())
	If XTRB->E1_OK == cMarca
		AADD(aDados, {XTRB->E1_CLIENTE, XTRB->E1_NUM, DTOC(STOD(XTRB->E1_VENCTO)),cValtoChar(XTRB->E1_VALOR), XTRB->E1_PARCELA })
	Endif
	XTRB->(dbSkip())
Enddo
		
For nZ := 1 to Len(aDados)
	//Verifica se o cliente atual � o mesmo que o pr�ximo para continuar incluindo registros nas vari�veis desse cleinte
	If aDados[nZ,1] == Iif(nZ == Len(aDados),"FIM", aDados[nZ+1,1])//cCliAnt
		cCliente:= aDados[nZ,1]  
		cTitulo += aDados[nZ,2] + CRLF 
		cVencto += aDados[nZ,3] + CRLF
		cValor  += aDados[nZ,4] + CRLF	
		cParcela+= aDados[nZ,5] + CRLF	
		cCliAnt := cCliente
	Else
		cCliente:= aDados[nZ,1] 
		cTitulo += aDados[nZ,2] + CRLF 
		cVencto += aDados[nZ,3] + CRLF
		cValor  += aDados[nZ,4] + CRLF	
		cParcela+= aDados[nZ,5] + CRLF	
		cCliAnt := cCliente

		dbSelectArea("SA1")
		SA1->(dbSetOrder(1))
		
		If SA1->(dbSeek(xFilial("SA1")+Alltrim(cCliente)))

			cCep:= Transform(Alltrim(SA1->A1_CEP), "@R 99999-999" ) 
 			cCgc:= Transform(Alltrim(SA1->A1_CGC), "@R 99.999.999/9999-99")  

			cInfoCli:= Alltrim(SA1->A1_NOME) + ' - CPF/CNPJ: ' + cCgc + ' - '+'SITUADO ' + Alltrim(SA1->A1_END) +' - '+ ;
					Alltrim(SA1->A1_BAIRRO) +' - '+ Alltrim(SA1->A1_MUN) +' - '+ Alltrim(SA1->A1_EST) +' - CEP: '+ cCep
			cDataDec:= STRZERO(Day(dDataBase), 2) + " de " + MesExtenso(dDataBase) + " de " + StrZero(Year(dDataBase), 4) 
			oWord	:=	OLE_CreateLink()//cria o link
			OLE_SetPropertie( oWord, oleWdVisible, .F.)
			OLE_SetPropertie( oWord, oleWdPrintBack, .T.)
			OLE_NewFile(oWord, cFileMod)
			OLE_SaveAsFile( oWord, (  cSaveFile+ "CARTA DE ANUENCIA - "+cCliAnt+ " - " +StrTran(Time()  ,":","")+ ".DOC"),,, .f., oleWdFormatDocument )
			OLE_SetDocumentVar( oWord, 'adv_titulo'	 , cTitulo ) // Titulo
			OLE_SetDocumentVar( oWord, 'adv_vencto'	 , cVencto ) // Vencimento
			OLE_SetDocumentVar( oWord, 'adv_valor'	 , cValor  ) // Valor
			OLE_SetDocumentVar( oWord, 'adv_dadocli' , cInfoCli) // Dados Cliente
			OLE_SetDocumentVar( oWord, 'adv_datadec' , cDataDec) // Data da Declara��o
			OLE_SetDocumentVar( oWord, 'adv_parcela' , cParcela) // Data da Declara��o
			OLE_UpdateFields( oWord )
			OLE_SaveFile( oWord )
			OLE_SetProperty( oWord, oleWdVisible, .T. )
			OLE_CloseFile( oWord )
			OLE_CloseLink ( oWord, .T. )
		Endif
		
		cCliente:= ""
		cTitulo := ""
		cVencto := ""
		cValor  := ""
		cParcela:= ""
		//cCliAnt := cCliente
	Endif


Next nZ
		
		
RestArea( aAreaTRB )	
		

MsgInfo("Processamento Finalizado")

Return