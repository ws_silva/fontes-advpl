User Function TK062SU5()

Local _cFilSU5 	:= ""
Local _cAlias  	:= PARAMIXB[1]	//Entidade - Ex: SA1
Local _cOper   	:= PARAMIXB[2]	//Operador que foi selecionado na lista de cobran�a
Local _lLog    	:= PARAMIXB[3]	//"Habilita log de registros ? A utiliza��o do registro de log implicar� em um processo de sele��o mais lento."			
								//.T. - Utiliza express�o ADVPL para o filtro			
								//.F. - Utiliza express�o SQL para o filtro

_area := GetArea()								

DbSelectArea("SU7")
_areaSU7 := GetArea()
DbSetOrder(1)
DbSeek(xfilial("SU7")+_cOper)

If _cAlias == "SA1"	//Filtra somente os contatos que pertencem ao operador
	If !_lLog				
		_cFilSU5 := " U5_XCODREG = '" + SU7->U7_XCODREG + "' "   
	Else		 		
		_cFilSU5 := " U5_XCODREG == '" + SU7->U7_XCODREG + "' "	
	EndIf
EndIf

RestArea(_areaSU7)
RestArea(_area)

Return _cFilSU5