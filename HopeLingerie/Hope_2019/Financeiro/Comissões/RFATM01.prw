#include "protheus.ch"
#include "topconn.ch"
/******************************************************************************
* Programa: RFATM01           09/2019            Totalit Solutions            *
* Funcionalidade: Programa para calcular as comissoes de vendas               *
* Parametros: cTipo = F - Faturamento                                         *
*                     E - Exclusao de notas de saida                          *
*                     D - Devolucao de vendas                                 *
*                     R - Exclusao de notas de devolucao de vendas            *
******************************************************************************/
User Function RFATM01(cTipo)
Local nCount := 0
Local nPercCom := 0
Local nPos := 0
Local cCriter := ""
Local cCond := ""
Local cTabela := ""
Local aComis := {}
Local aAreaAnt := GetArea()
Local aAreaDA0 := da0->(GetArea())
Local aAreaSA1 := sa1->(GetArea())
Local aAreaSA3 := sa3->(GetArea())
Local aAreaSB1 := sb1->(GetArea())
Local aAreaSC5 := sc5->(GetArea())
Local aAreaSD1 := sd1->(GetArea())
Local aAreaSD2 := sd2->(GetArea())
Local aAreaSE1 := se1->(GetArea())
Local aAreaSE3 := se3->(GetArea())
Local aAreaSE4 := se4->(GetArea())
Local aAreaSF2 := sf2->(GetArea())
Local aAreaSZ1 := sz1->(GetArea())
Local aAreaZAI := zai->(GetArea())
Local aAreaZZG := zzg->(GetArea())
Local i := 0
Private cVend  := ""

da0->(dBSetOrder(1))  // DA0_FILIAL+DA0_CODTAB
sa1->(dBSetOrder(1))  // A1_FILIAL+A1_COD+A1_LOJA
sa3->(dBSetOrder(1))  // A3_FILIAL+A3_COD
sb1->(dBSetOrder(1))  // B1_FILIAL+B1_COD
sc5->(dBSetOrder(1))  // C5_FILIAL+C5_NUM
sd1->(dBSetOrder(1))  // D1_FILIAL+D1_DOC+D1_SERIE+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEM
sd2->(dBSetOrder(3))  // D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
se1->(dBSetOrder(1))  // E1_FILIAL+E1_PREFIXO+E1_NUM+E1_PARCELA+E1_TIPO
se3->(dBOrderNickName("VENDMARCA"))  // E3_FILIAL+E3_VEND+E3_XMARCA+E3_PREFIXO+E3_NUM+E3_TIPO+E3_CODCLI+E3_LOJA
se4->(dBSetOrder(1))  // E4_FILIAL+E4_CODIGO
sz1->(dBSetOrder(1))  // Z1_FILIAL+Z1_CODIGO+Z1_DESC
zai->(dBSetOrder(1))  // ZAI_FILIAL+ZAI_CODIGO
zzg->(dBSetOrder(1))  // ZZG_FILIAL+ZZG_CHVSD2+ZZG_VEND

BEGIN TRANSACTION

If cTipo $ "FE" .and. sd2->(MSSeek(xFilial("SD2")+sf2->(f2_doc+f2_serie+f2_cliente+f2_loja)))
	cCond := 'sd2->(!Eof() .and. d2_filial = xFilial("SD2") .and. d2_doc = sf2->f2_doc .and. '
	cCond += 'd2_serie = sf2->f2_serie .and. d2_cliente = sf2->f2_cliente .and. d2_loja = sf2->f2_loja)'
	cTabela := "SD2"
ElseIf cTipo $ "DR" .and. sd1->(MSSeek(xFilial("SD1")+sf1->(f1_doc+f1_serie+f1_fornece+f1_loja)))
	cCond := 'sd1->(!Eof() .and. d1_filial = xFilial("SD1") .and. d1_doc = sf1->f1_doc .and. '
	cCond += 'd1_serie = sf1->f1_serie .and. d1_fornece = sf1->f1_fornece .and. d1_loja = sf1->f1_loja)'
	cTabela := "SD1"
Endif

While &cCond
	// So calcular comissoes para tipos de pedidos que estejam configurados para gerar e para 
	// notas em que o vendedor tenha percentual maior que zero
	If If(cTipo $ "DR",sf2->(!MSSeek(xFilial("SF2")+sd1->(d1_nfori+d1_seriori+d1_fornece+d1_loja))) .or. ;
							sd2->(!MSSeek(xFilial("SD2")+sd1->(d1_nfori+d1_seriori+d1_fornece+d1_loja+d1_cod+d1_itemori))),.F.) .or. ;
		Empty(sf2->(f2_vend1+f2_vend2+f2_vend3+f2_vend4+f2_vend5)) .or. Empty(sf2->f2_dupl) .or. ;
		sc5->(!MSSeek(xFilial("SC5")+sd2->d2_pedido)) .or. ;
		sz1->(!MSSeek(xFilial("SZ1")+sc5->c5_tpped) .or. z1_comis <> "S") .or. ;
		sa3->(!MSSeek(xFilial("SA3")+sf2->f2_vend1) .or. a3_comis = 0) .or. ;
		da0->(!MSSeek(xFilial("DA0")+sc5->c5_tabela)) .or. ;
		sa1->(!MSSeek(xFilial("SA1")+sf2->f2_cliente+sf2->f2_loja)) .or. ;
		se4->(!MSSeek(xFilial("SE4")+sc5->c5_condpag))

		(cTabela)->(dBSkip())
		Loop
	Endif

	If cTipo = "R"
		se1->(MSSeek(xFilial("SE1")+sf1->(f1_serie+f1_doc)))
		While se1->(!Eof() .and. e1_filial = xFilial("SE1") .and. e1_prefixo = sf1->f1_serie .and. e1_num = sf1->f1_doc .and. e1_tipo <> "NCC")
			se1->(dBSkip())
		Enddo
	Else
		se1->(MSSeek(xFilial("SE1")+sf2->(f2_serie+f2_doc)))
		While se1->(!Eof() .and. e1_filial = xFilial("SE1") .and. e1_prefixo = sf2->f2_serie .and. e1_num = sf2->f2_doc .and. e1_tipo <> "NF")
			se1->(dBSkip())
		Enddo
	Endif

	For nCount := 1 to 5
		cVend := "F2_VEND"+Str(nCount,1)
		If Empty(sf2->&cVend)
			Loop
		Endif

		sa3->(MSSeek(xFilial("SA3")+sf2->&cVend))
		sb1->(MSSeek(xFilial("SB1")+sd2->d2_cod))
		zai->(MSSeek(xFilial("ZAI")+sb1->b1_ymarca))

		// Se estiver calculando a partir do faturamento, analisa todas as regras para encontrar o percentual de comissao
		// Se estiver tratando de nota ja emitida, buscar na ZZG qual foi o criterio e o percentual utilizado
		// Caso o campo F2_VEND4 esteja preenchido a comissao para os representantes (F2_VEND1 e F2_VEND4) sera dividida e obedecera
		// a regra do pedido. Para os demais campos e situacoes, a comissao utilizada no calculo sera a menor entre o cadastro do vendedor,
		// do cliente, da tabela de precos, da marca, da condicao de pagamento, do produto e do digitado no pedido
		If cTipo = "F"
			// Caso tenha sido registrado o segundo vendedor
			If (nCount = 1 .or. nCount = 4) .and. !Empty(sf2->f2_vend4)
				nPercCom := &("SC5->C5_COMIS"+Str(nCount,1))
				cCriter := "1" // COMISSAO COMPARTILHADA
			Else
				// Se for gerente buscar o percentual definido no cadastro do vendedor/representante
/*				If nCount = 2 .and. !Empty(sf2->f2_vend1)
					nPercCom := Posicione("SA3",1,xFilial("SA3")+sf2->f2_vend1,"SA3->A3_COMIS2")
				Else
					nPercCom := sa3->a3_comis
				Endif */
				// Se for gerente buscar o percentual definido no campo de % gerente - Alteracao em 09/03/2020 por solicitacao de Roseli
				If nCount = 2
					nPercCom := sa3->a3_comis2
				Else
					nPercCom := sa3->a3_comis
				Endif
				cCriter := "2"  // CADASTRO VENDEDOR
				If !Empty(sa1->&("A1_XCOMIS"+Str(nCount,1))) .and. sa1->&("A1_XCOMIS"+Str(nCount,1)) < nPercCom
					nPercCom := sa1->&("A1_XCOMIS"+Str(nCount,1))
					cCriter := "3"  // CADASTRO CLIENTE
				Endif
				If !Empty(da0->&("DA0_XCOMI"+Str(nCount,1))) .and. da0->&("DA0_XCOMI"+Str(nCount,1)) < nPercCom
					nPercCom := da0->&("DA0_XCOMI"+Str(nCount,1))
					cCriter := "4"  // TABELA DE PRECOS
				Endif
				If !Empty(zai->&("ZAI_COMIS"+Str(nCount,1))) .and. zai->&("ZAI_COMIS"+Str(nCount,1)) < nPercCom
					nPercCom := zai->&("ZAI_COMIS"+Str(nCount,1))
					cCriter := "5"  // CADASTRO MARCA
				Endif
				If !Empty(se4->&("E4_XCOMIS"+Str(nCount,1))) .and. se4->&("E4_XCOMIS"+Str(nCount,1)) < nPercCom
					nPercCom := se4->&("E4_XCOMIS"+Str(nCount,1))
					cCriter := "6"  // CONDICAO PAGAMENTO
				Endif
				If !Empty(sb1->&("B1_XCOMIS"+Str(nCount,1))) .and. sb1->&("B1_XCOMIS"+Str(nCount,1)) < nPercCom
					nPercCom := sb1->&("B1_XCOMIS"+Str(nCount,1))
					cCriter := "7"  // CADASTRO PRODUTO
				Endif
				If !Empty(sc5->&("C5_COMIS"+Str(nCount,1))) .and. sc5->&("C5_COMIS"+Str(nCount,1)) < nPercCom
					nPercCom := sc5->&("C5_COMIS"+Str(nCount,1))
					cCriter := "8"  // PEDIDO
				Endif
			Endif
		Elseif zzg->(MSSeek(xFilial("ZZG")+PadR(sd2->(d2_filial+d2_doc+d2_serie+d2_cliente+d2_loja+d2_cod+d2_item),TamSX3("ZZG_CHVSD2")[1])+sf2->&(cVend)))
		 	nPercCom := If(zzg->zzg_criter $ "18",zzg->zzg_ppedid,;
		 				If(zzg->zzg_criter = "2",zzg->zzg_pvend,;
		 				If(zzg->zzg_criter = "3",zzg->zzg_pclien,;
		 				If(zzg->zzg_criter = "4",zzg->zzg_ptbprc,;
		 				If(zzg->zzg_criter = "5",zzg->zzg_pmarca,;
		 				If(zzg->zzg_criter = "6",zzg->zzg_pcdpag,zzg->zzg_pprod))))))
		Endif

		// array aComis: {vendedor,marca,base,percentual,comissao,dia vencto comis,fora o mes}
		//                  1        2     3       4        5           6              7
		nPos := aScan(aComis,{|x| x[1] = sf2->&cVend .and. x[2] = zai->zai_codigo})
		If nPos = 0
			aAdd(aComis,{sf2->&cVend,zai->zai_codigo,0,0,0,sa3->a3_dia,sa3->a3_ddd})
			nPos := Len(aComis)
		Endif

		// Identificar como ser� o calculo da base da comissao
		cValBase := If(cTipo="D","SD1->D1_TOTAL","SD2->D2_TOTAL")
		If sa3->a3_icm = "N"
			cValBase += If(cTipo="D","-SD1->D1_VALICM","-SD2->D2_VALICM")
		Endif
		If sa3->a3_icmsret = "S"
			cValBase += If(cTipo="D","+SD1->D1_ICMSRET","+SD2->D2_ICMSRET")
		Endif
		If sa3->a3_iss = "S"
			cValBase += If(cTipo="D","+SD1->D1_VALISS","+SD2->D2_VALISS")
		Endif
		If sa3->a3_ipi = "S"
			cValBase += If(cTipo="D","+SD1->D1_VALIPI","+SD2->D2_VALIPI")
		Endif
		If sa3->a3_frete = "S"
			cValBase += If(cTipo="D","+SD1->D1_VALFRE","+SD2->D2_VALFRE")
		Endif

		aComis[nPos,3] += &cValBase
//		aComis[nPos,4] += nPercCom
		aComis[nPos,5] += If(nPercCom = 0 .or. &(cValBase) = 0,0,&(cValBase)*nPercCom/100)

		If cTipo = "F"
			// Gravar na tabela de memoria de calculo o detalhe por item
			RecLock("ZZG",.T.)
			zzg->zzg_filial := xFilial("ZZG")
			zzg->zzg_chvsd2 := sd2->(d2_filial+d2_doc+d2_serie+d2_cliente+d2_loja+d2_cod+d2_item)
			zzg->zzg_vend   := sf2->&cVend
			zzg->zzg_pvend  := If(nCount = 2 .and. !Empty(sf2->f2_vend1),Posicione("SA3",1,xFilial("SA3")+sf2->f2_vend1,"SA3->A3_COMIS2"),sa3->a3_comis)
			zzg->zzg_pclien := sa1->&("A1_XCOMIS"+Str(nCount,1))
			zzg->zzg_ptbprc := da0->&("DA0_XCOMI"+Str(nCount,1))
			zzg->zzg_pprod  := sb1->&("B1_XCOMIS"+Str(nCount,1))
			zzg->zzg_pmarca := zai->&("ZAI_COMIS"+Str(nCount,1))
			zzg->zzg_ppedid := sc5->&("C5_COMIS"+Str(nCount,1))
			zzg->zzg_pcdpag := se4->&("E4_XCOMIS"+Str(nCount,1))
			zzg->zzg_criter := cCriter
			zzg->(MsUnlock())
		ElseIf cTipo = "E" .and. zzg->(MSSeek(xFilial("ZZG")+PadR(sd2->(d2_filial+d2_doc+d2_serie+d2_cliente+d2_loja+d2_cod+d2_item),TamSX3("ZZG_CHVSD2")[1])+sf2->&(cVend)))
			RecLock("ZZG",.F.)
			zzg->(dBDelete())
			zzg->(MsUnlock())
		Endif
	Next

	(cTabela)->(dBSkip())
Enddo

// Tratar SE3
For i := 1 to Len(aComis)
	// Se for comissao gerada no faturamento gravar a SE3 positiva
	// Se for exclusao de nota cuja comissao ja foi paga gravar a SE3 negativa
	If cTipo $ "FD" .or. ;
		(cTipo $ "ER" .and. se3->(MSSeek(xFilial("SE3")+aComis[i,1]+aComis[i,2]+se1->(e1_prefixo+e1_num+e1_tipo+e1_cliente+e1_loja)) .and. ;
		 !Empty(se3->e3_data)))

		RecLock("SE3",.T.)
		se3->e3_filial  := xFilial("SE3")
		se3->e3_vend    := aComis[i,1]
		se3->e3_xmarca  := aComis[i,2]
		se3->e3_xorigem := If(cTipo="F","1",If(cTipo="E","2","5"))  // 1-Vendas, 2-Canc. vendas, 5-Devolucao
		se3->e3_xsituac := "S"	// Considerar para o fechamento
		se3->e3_emissao := If(cTipo $ "FE",se1->e1_emissao,dDEmissao)
		se3->e3_num     := If(cTipo $ "FE",se1->e1_num,cNFiscal)
		se3->e3_serie   := If(cTipo $ "FE",se1->e1_prefixo,cSerie)
		se3->e3_codcli  := se1->e1_cliente
		se3->e3_loja    := se1->e1_loja
		se3->e3_prefixo := If(cTipo $ "FE",se1->e1_prefixo,cSerie)
		se3->e3_tipo    := If(cTipo $ "FE","NF","NCC")
		se3->e3_baiemi  := "E"
		se3->e3_pedido  := se1->e1_pedido
		se3->e3_origem  := If(cTipo="D","D","F")  // Devolucao ou Faturamento
		se3->e3_vencto	:= U_RFATM011(aComis[i,6],aComis[i,7])
		se3->e3_moeda   := Strzero(se1->e1_moeda,2)
		se3->e3_sdoc    := If(cTipo $ "FE",se1->e1_prefixo,cSerie)
		se3->e3_base    := If(cTipo $ "FR",aComis[i,3],-aComis[i,3])
		se3->e3_porc    := Round(aComis[i,5]/aComis[i,3]*100,2)
		se3->e3_comis   := If(cTipo $ "FR",aComis[i,5],-aComis[i,5])
		se3->(MsUnlock())

	// Se for exclusao de nota cuja comissao nao foi paga excluir a SE3
	ElseIf cTipo $ "ER" .and. se3->(MSSeek(xFilial("SE3")+aComis[i,1]+aComis[i,2]+se1->(e1_prefixo+e1_num+e1_tipo+e1_cliente+e1_loja)) .and. ;
		 Empty(se3->e3_data))

		RecLock("SE3",.F.)
		se3->(dBDelete())
		se3->(Msunlock())
	Endif
Next

END TRANSACTION

RestArea(aAreaDA0)
RestArea(aAreaSA1)
RestArea(aAreaSA3)
RestArea(aAreaSB1)
RestArea(aAreaSC5)
RestArea(aAreaSD1)
RestArea(aAreaSD2)
RestArea(aAreaSE1)
RestArea(aAreaSE3)
RestArea(aAreaSE4)
RestArea(aAreaSF2)
RestArea(aAreaSZ1)
RestArea(aAreaZAI)
RestArea(aAreaZZG)
RestArea(aAreaAnt)

Return Nil
/*---------------------------------------------------------------------------*/
// Logica copiada do FINA440 para definir a data de vencimento da comissao   //
/*---------------------------------------------------------------------------*/
User Function RFATM011(cDia,cDDD)
Local dVencto := se1->e1_emissao
Local nDia    := If(!Empty(cDia) .and. Type("cDia")="N",Val(cDia),0)
Local nMes,nAno

If nDia > 0
	dVencto := Ctod(StrZero(nDia,2)+"/"+StrZero(Month(se1->e1_emissao),2)+"/"+StrZero(year(se1->e1_emissao),4),"ddmmyy")

	While Empty(dVencto)
		nDia -= 1
		dVencto := ctod(StrZero(nDia,2)+"/"+StrZero(Month(SE1->E1_EMISSAO),2)+"/"+StrZero(year(SE1->E1_EMISSAO),4),"ddmmyy")
	EndDo
EndIf

if cDDD == "F" .or. dVencto < SE1->E1_EMISSAO		//Fora o mes
	nDia := If(!Empty(cDia) .and. Type("cDia")="N",Val(cDia),0)
	nMes := Month(dVencto)+1
	nAno := year (dVencto)
	If nMes == 13
		nMes := 01
		nAno++
	Endif
	nDia	  := StrZero(nDia,2)
	nMes	  := StrZero(nMes,2)
	nAno	  := substr(lTrim(str(nAno)),3,2)
	dVencto := CtoD(nDia+"/"+nMes+"/"+nAno,"ddmmyy")
Else
	nDia	  := StrZero(day(dVencto),2)
	nMes	  := StrZero(Month(dVencto),2)
	nAno	  := substr(lTrim(str(Year(dVencto))),3,2)
Endif

While Empty(dVencto)
	nDia := If(Valtype(nDia)=="C",Val(nDia),nDia)
	nDia -= 1
	dVencto := CtoD(StrZero(nDia,2)+"/"+nMes+"/"+nAno,"ddmmyy")
	If !Empty(dVencto)
		If dVencto < SE1->E1_EMISSAO
			dVencto += 2
		EndIf
	EndIf
Enddo

Return dVencto
/*---------------------------------------------------------------------------*/
// Funcao para criar creditos por inadimplencia para os titulos que tiveram  //
// lancamentos de debitos e foram pagos                                      //
/*---------------------------------------------------------------------------*/
User Function RFATM012()
Local aAreaAnt := GetArea()
Local aAreaSA3 := SA3->(GetArea())
Local aAreaSE3 := SE3->(GetArea())
Local nValBase := 0
Local nValCom  := 0
Local nPorc    := 0
Local cQuery   := ""
Local cNewAlias  := GetNextAlias()

// So serao considerados os titulos gerados de notas fiscais de saida 
If se1->e1_tipo <> "NF"
	Return Nil
Endif

sa3->(dBSetOrder(1)) // A3_FILIAL+A3_COD
se3->(dBSetOrder(3)) // E3_FILIAL+E3_VEND+E3_CODCLI+E3_LOJA+E3_PREFIXO+E3_NUM+E3_PARCELA+E3_TIPO+E3_SEQ

cQuery := "SELECT E3_VEND,E3_XMARCA,SUM(E3_COMIS) nTotal FROM "
cQuery += RetSqlName("SE3")+" WITH (NOLOCK) WHERE "
cQuery += "E3_FILIAL = '"+xFilial("SE3")+"' AND E3_PREFIXO = '"+se1->e1_prefixo+"' AND E3_NUM = '"+se1->e1_num+"' AND "
cQuery += "E3_CODCLI = '"+se1->e1_cliente+"' AND E3_LOJA = '"+se1->e1_loja+"' AND E3_XORIGEM IN ('3','4') AND D_E_L_E_T_ = ' ' "
cQuery += "GROUP BY E3_VEND,E3_XMARCA "
cQuery += "ORDER BY E3_VEND,E3_XMARCA"
TcQuery cQuery Alias (cNewAlias) New

BEGIN TRANSACTION

(cNewAlias)->(dBGotop())
While (cNewAlias)->(!Eof())
	If (cNewAlias)->nTotal < 0 .and. ;
		sa3->(MSSeek(xFilial("SA3")+(cNewAlias)->e3_vend)) .and. ;
		se3->(MSSeek(xFilial("SE3")+(cNewAlias)->e3_vend+se1->(e1_cliente+e1_loja+e1_prefixo+e1_num+e1_parcela+e1_tipo)))

		nValBase := Abs(se3->e3_base)
		nValCom  := Abs(se3->e3_comis)
		nPorc    := se3->e3_porc

		RecLock("SE3",.T.)
		se3->e3_filial  := xFilial("SE3")
		se3->e3_vend    := (cNewAlias)->e3_vend
		se3->e3_xmarca  := (cNewAlias)->e3_xmarca
		se3->e3_xorigem := "4"  // credito por inadimplencia
		se3->e3_xsituac := "S"	// Considerar para o fechamento
		se3->e3_emissao := dDataBase
		se3->e3_num     := se1->e1_num
		se3->e3_serie   := se1->e1_prefixo
		se3->e3_codcli  := se1->e1_cliente
		se3->e3_loja    := se1->e1_loja
		se3->e3_prefixo := se1->e1_prefixo
		se3->e3_parcela := se1->e1_parcela
		se3->e3_tipo    := se1->e1_tipo
		se3->e3_baiemi  := "E"
		se3->e3_pedido  := se1->e1_pedido
		se3->e3_origem  := "F"
		se3->e3_vencto	:= U_RFATM011(sa3->a3_dia,sa3->a3_ddd)
		se3->e3_moeda   := Strzero(se1->e1_moeda,2)
		se3->e3_sdoc    := se1->e1_prefixo
		se3->e3_base    := nValBase
		se3->e3_porc    := nPorc    //If(nValBase > 0 .and. nValCom > 0, nValCom/nValBase*100, 0)
		se3->e3_comis   := nValCom
		se3->(MsUnlock())
	Endif 

	// Contabilizar o credito por inadimplencia (sera feito pela contabilizacao do fechamento)
//	U_RFATM021("C")

	(cNewAlias)->(dBSkip())
Enddo

END TRANSACTION

(cNewAlias)->(dBCloseArea())

RestArea(aAreaSA3)
RestArea(aAreaSE3)
RestArea(aAreaAnt)

Return Nil