#Include "RwMake.ch"
#Include "Protheus.ch"   
#INCLUDE "TBICONN.CH"      
#Include "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPRATCOM  �Autor  �Microsiga           � Data �  10/09/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HPRatCom

Private nSaldo := SE3->E3_COMIS
Private oSaldo
/*
DbSelectArea("SE3")
SE3->(DbSetOrder(1))
SE3->(DbSeek(xFilial("SE3") + "1  000649146"))
*/

If !Empty(DTOS(SE3->E3_DATA))
	Alert('Comiss�o j� paga n�o pode ser transferida')
	Return
Endif  

If SE3->E3_COMIS <= 0
	Alert('Comiss�o negativa ou zerada n�o pode ser transferida')
	Return
Endif

If SE3->E3_ORIGEM <> "F" .Or. SE3->E3_XORIGEM <> "1" 
	Alert('Comiss�o n�o originada de venda n�o pode ser transferida')
	Return
Endif
/*
Private aRotina := {{ "Pesquisa","AxPesqui", 0 , 1},;
           			{ "Visual","AxVisual", 0 , 2},;
     				{ "Inclui","AxInclui", 0 , 3},;
     				{ "Altera","AxAltera", 0 , 4, 20 },;
    				{ "Exclui","AxDeleta", 0 , 5, 21 }}     */
cOpcao:="VISUALIZAR"

Private nOpc := 3 
Private o2Get
Private oDlg

Do Case
     Case cOpcao=="INCLUIR"; nOpc:=3 
     Case cOpcao=="ALTERAR"; nOpc:=3 
     Case cOpcao=="VISUALIZAR"; nOpc:=2 
EndCase

Private aGets  		:= Array(0)

bOK 	  := {||u_ProcRat()}
bCancel   := {||oDlg:End()}  

oSize := FwDefSize():New( .T. )

oSize:AddObject( "ENCHOICE" , 100, 020, .T., .T. ) // Adiciona enchoice   
oSize:AddObject( "GETDADOS" , 100, 080, .T., .T. ) // Adiciona Folder       

oSize:lProp 	:= .T. // Proporcional    
oSize:Process()

oFontR := TFont():New('Arial',,-18,,.T.)     
oFont  := TFont():New('Arial',,-12,,.T.)

aHeader	:= {}
aCols	:= {}                           

Aadd(aCols,Array(5))

nI 		:= 0

DbSelectArea("SX3")  
SX3->(DbSetOrder(1))
SX3->(DbSeek("SE3"))
While SX3->(!EoF()) .And. SX3->X3_ARQUIVO == "SE3" 
    	
   	If AllTrim(SX3->X3_CAMPO) $ "E3_VEND|E3_BASE|E3_PORC|E3_COMIS"
   	
   		nI ++ 
   		
		aAdd(aHeader,{Trim(X3Titulo()),;                      
	        SX3->X3_CAMPO,;                      
	        SX3->X3_PICTURE,;                      
	        SX3->X3_TAMANHO,;                      
	        SX3->X3_DECIMAL,;                      
	        SX3->X3_VALID,;                      
	        "",;                      
	        SX3->X3_TIPO,;                      
	        "",;                      
	        "" })   		         			  		
            
			aCols[1][nI] := CriaVar(aHeader[nI][2])
			If AllTrim(SX3->X3_CAMPO) == "E3_BASE"
				aCols[1][nI] := SE3->E3_COMIS
			Endif
			
   	Endif 
   	
   	SX3->(DbSkip())
EndDo

aCols[1][nI+1] := .F.

nValCom := SE3->E3_COMIS//Transform(SE3->E3_COMIS,"@E 9,999,999.99")

cVend 	:= SE3->E3_VEND
DbSelectArea("SA3")
SA3->(DbSetOrder(1))
SA3->( xFilial("SA3") + cVend )
cNomeVen := SA3->A3_NOME

cCli 	:= AllTrim(SE3->E3_CODCLI) + "/" + SE3->E3_LOJA
cLoja 	:= SE3->E3_LOJA
DbSelectArea("SA1")
SA1->(DbSetOrder(1))
SA1->(DbSeek( xFilial("SA1") + SE3->E3_CODCLI + SE3->E3_LOJA ))   
cNomeCli := SA1->A1_NOME

cNF 	:= SE3->E3_NUM
cSerie 	:= SE3->E3_SERIE
dEmis 	:= DTOC(SE3->E3_EMISSAO)

Define MSDialog oDlg Title "Transfer�ncia de comiss�o" From oSize:aWindSize[1],oSize:aWindSize[2] To oSize:aWindSize[3],oSize:aWindSize[4] of oMainWnd Pixel     

	tSay():New(oSize:GetDimension("ENCHOICE","LININI")+03 ,oSize:GetDimension("ENCHOICE","COLINI")+10,{|| "NF: " },oDlg,,,,,,.T.,,,110,20)
	tSay():New(oSize:GetDimension("ENCHOICE","LININI")+03 ,oSize:GetDimension("ENCHOICE","COLINI")+110,{|| "S�rie: " },oDlg,,,,,,.T.,,,110,20) 
	tSay():New(oSize:GetDimension("ENCHOICE","LININI")+03 ,oSize:GetDimension("ENCHOICE","COLINI")+260,{|| "Emiss�o: " },oDlg,,,,,,.T.,,,110,20)

	@ oSize:GetDimension("ENCHOICE","LININI")+03 ,oSize:GetDimension("ENCHOICE","COLINI")+40 MSGET cNF SIZE 15,10 WHEN .F. OF oDlg Pixel 
	@ oSize:GetDimension("ENCHOICE","LININI")+03 ,oSize:GetDimension("ENCHOICE","COLINI")+150 MSGET cSerie SIZE 5,10 WHEN .F. OF oDlg Pixel 
	@ oSize:GetDimension("ENCHOICE","LININI")+03 ,oSize:GetDimension("ENCHOICE","COLINI")+310 MSGET dEmis SIZE 35,10 WHEN .F. OF oDlg Pixel 

	tSay():New(oSize:GetDimension("ENCHOICE","LININI")+18 ,oSize:GetDimension("ENCHOICE","COLINI")+10,{|| "Cliente: " },oDlg,,,,,,.T.,,,110,20)
	tSay():New(oSize:GetDimension("ENCHOICE","LININI")+18 ,oSize:GetDimension("ENCHOICE","COLINI")+260,{|| "Vl.Comiss�o: " },oDlg,,,,,,.T.,,,110,20)		

	@ oSize:GetDimension("ENCHOICE","LININI")+18 ,oSize:GetDimension("ENCHOICE","COLINI")+40 MSGET cCli SIZE 45,10 WHEN .F. OF oDlg Pixel 
	@ oSize:GetDimension("ENCHOICE","LININI")+18 ,oSize:GetDimension("ENCHOICE","COLINI")+90 MSGET cNomeCli SIZE 150,10 WHEN .F. OF oDlg Pixel 
	@ oSize:GetDimension("ENCHOICE","LININI")+18 ,oSize:GetDimension("ENCHOICE","COLINI")+310 MSGET nValCom SIZE 50,10 PICTURE PesqPict("SE3","E3_COMIS") WHEN .F. OF oDlg Pixel  

	tSay():New(oSize:GetDimension("ENCHOICE","LININI")+33 ,oSize:GetDimension("ENCHOICE","COLINI")+10,{|| "Vendedor: " },oDlg,,,,,,.T.,,,210,120) 
	tSay():New(oSize:GetDimension("ENCHOICE","LININI")+33 ,oSize:GetDimension("ENCHOICE","COLINI")+260,{|| "Saldo Transfer�ncia: " },oDlg,,,,,,.T.,,,110,20)	

	@ oSize:GetDimension("ENCHOICE","LININI")+33 ,oSize:GetDimension("ENCHOICE","COLINI")+40 MSGET cVend SIZE 20,10 WHEN .F. OF oDlg Pixel 
	@ oSize:GetDimension("ENCHOICE","LININI")+33 ,oSize:GetDimension("ENCHOICE","COLINI")+90 MSGET cNomeVen SIZE 150,10 WHEN .F. OF oDlg Pixel 
	@ oSize:GetDimension("ENCHOICE","LININI")+33 ,oSize:GetDimension("ENCHOICE","COLINI")+310 MSGET oSaldo VAR nSaldo SIZE 50,10 PICTURE PesqPict("SE3","E3_COMIS") WHEN .F. OF oDlg Pixel  

	o2Get 		:= MSGetDados():New( oSize:GetDimension("GETDADOS","LININI"),;
				oSize:GetDimension("GETDADOS","COLINI"),;
				oSize:GetDimension("GETDADOS","LINEND"),;
				oSize:GetDimension("GETDADOS","COLEND"),;
				3,"U_LinOk()","U_TudOk()",,.T.,{"E3_VEND","E3_PORC","E3_COMIS"},,,;
				,"U_FieldOK()",,,"U_DelOk()",oDlg)
	
ACTIVATE MSDIALOG oDlg ON INIT EnchoiceBar(oDlg, bOK, bCancel,,,,,,,.F.) CENTERED   

Return    
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPRATCOM  �Autor  �Microsiga           � Data �  10/09/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function LinOk
Local nAC	:=  0
nPosVen := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_VEND" })
nPosCom := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_COMIS" })

nSaldo := SE3->E3_COMIS
For nAC := 1 To Len(aCols)
	If !aCols[nAC][Len(aHeader)+1] 
		nSaldo -= aCols[nAC][nPosCom]
	Endif
Next
oSaldo:CtrlRefresh()

If nSaldo < 0    
	Alert('O Saldo est� zerado')
	Return .F.
Endif

If Empty(AllTrim(aCols[Len(aCols)][nPosVen]))
	Alert('Preencha o vendedor!')
	Return .F.
Endif

Return .T.
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPRATCOM  �Autor  �Microsiga           � Data �  10/10/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function TudOk

If nSaldo > 0
	Alert('Existe saldo de comiss�o no valor de R$ ' + Transform(nSaldo,'@E 99,999,999.99') + ' a ser transferida')
	Return .F.	
Endif

Return .T.

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPRATCOM  �Autor  �Microsiga           � Data �  10/09/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function FieldOK
Local nAC	:=  0

nPosCpo := o2Get:oBrowse:ColPos
cCpoAtu := aHeader[nPosCpo][2] 
nValCpo := &("M->"+AllTrim(cCpoAtu))//aCols[n][nPosCpo]

nPosVen := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_VEND" })
nPosCom := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_COMIS" })
nPosBas := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_BASE" })
nPosPor := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_PORC" })

If AllTrim(cCpoAtu) == "E3_VEND"
    
	M->E3_COMIS := 0
    M->E3_PORC	:= 0

	aCols[n][nPosBas] 	:= 0
	aCols[n][nPosCom] 	:= 0
    
    nSaldo := SE3->E3_COMIS
	For nAC := 1 To Len(aCols)
		If !aCols[nAC][Len(aHeader)+1] .And. nAC <> n
			nSaldo -= aCols[nAC][nPosCom]
		Endif
	Next

	M->E3_COMIS := nSaldo
    M->E3_PORC	:= ( nSaldo / SE3->E3_COMIS ) * 100

	aCols[n][nPosBas] 	:= SE3->E3_COMIS 
	aCols[n][nPosCom] 	:= nSaldo
	aCols[n][nPosPor] 	:= ( nSaldo / SE3->E3_COMIS ) * 100  
	
	nSaldo := 0
Endif	

If AllTrim(cCpoAtu) == "E3_PORC"	
	
	If nValCpo > 100 .Or. nValCpo <= 0
		Alert("Deve ser informado um percentual entre 0,01 e 100,00")
		Return .F.
	Else		
		
		aCols[n][nPosCom] 	:= aCols[n][nPosBas] * ( nValCpo / 100 )
		M->E3_COMIS 		:= aCols[n][nPosBas] * ( nValCpo / 100 )   
	//	nSaldo 				-= M->E3_COMIS    
	    nSaldoAtu := nSaldo
	    nSaldo := SE3->E3_COMIS
		For nAC := 1 To Len(aCols)
			If !aCols[nAC][Len(aHeader)+1] .And. nAC <> n
				nSaldo -= aCols[nAC][nPosCom]
			Endif
		Next 
			
		If nSaldo < (aCols[n][nPosBas] * ( nValCpo / 100 ))          
			Alert("O valor dessa comiss�o deve ser menor que o saldo a transferir")  
			nSaldo := nSaldoAtu
			Return .F.						
		Endif		

	Endif
Endif   

If AllTrim(cCpoAtu) == "E3_COMIS"	
	If nValCpo > SE3->E3_COMIS .Or. nValCpo <= 0
		Alert("Deve ser informado um percentual entre 0,01 e R$ " + AllTrim(cValToChar(SE3->E3_COMIS)))
		Return .F.  
	Else
		
		aCols[n][nPosPor] := ( nValCpo / aCols[n][nPosBas] ) * 100
		M->E3_PORC	:= nValCpo / aCols[n][nPosBas] 
	//	nSaldo		-= nValCpo
		nSaldoAtu := nSaldo  
	    nSaldo := SE3->E3_COMIS
		For nAC := 1 To Len(aCols)
			If !aCols[nAC][Len(aHeader)+1] .And. nAC <> n
				nSaldo -= aCols[nAC][nPosCom]
			Endif
		Next					
		If nSaldo < nValCpo
			Alert("O valor dessa comiss�o deve ser menor que o saldo a transferir") 
			nSaldo := nSaldoAtu
			Return .F.		
		Endif
	Endif
Endif
/* o2Get:ForceRefresh()        */
oSaldo:CtrlRefresh()
Return .T.          


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPRATCOM  �Autor  �Microsiga           � Data �  10/10/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function DelOk
Local nAC	:=  0
nPosCom := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_COMIS" })

nSaldo := SE3->E3_COMIS
For nAC := 1 To Len(aCols)
	If !aCols[nAC][Len(aHeader)+1] 
		nSaldo -= aCols[nAC][nPosCom]
	Endif
Next
oSaldo:CtrlRefresh()
Return .T.                    


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPRATCOM  �Autor  �Microsiga           � Data �  10/10/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function ProcRat                                        
Local nAC	:=  0
nPosCom := aScan(aHeader,{ |x| AllTrim(x[2]) == "E3_COMIS" })

nSaldo := SE3->E3_COMIS
For nAC := 1 To Len(aCols)
	If !aCols[nAC][Len(aHeader)+1] 
		nSaldo -= aCols[nAC][nPosCom]
	Endif
Next
oSaldo:CtrlRefresh()

If nSaldo > 0
	Alert('Existe saldo de comiss�o no valor de R$ ' + Transform(nSaldo,'@E 99,999,999.99') + ' a ser transferida')
	Return 	
Endif

lMsg := MsgYesNo('Confirma a transfer�ncia da comiss�o?','Transfer�ncia de Comiss�es')
If lMsg
	lRet := U_HPMATA490(aHeader,aCols) 
	If lRet
		oDlg:End()
	Endif   
Else
	Alert('Transfer�ncia cancelada')
Endif

Return