#include "protheus.ch"
#include "topconn.ch"
/******************************************************************************
* Programa: RFATM02           11/2019            Totalit Solutions            *
* Funcionalidade: Programa para calcular o valor dos lancamentos de contabi-  *
*                 lizacao das comissoes por item da nota fiscal               *
* Parametros: cTipo = F - Faturamento                                         *
*                     D - Devolucao de vendas                                 *
******************************************************************************/
User Function RFATM02(cTipo)
Local cValBase  := ""
Local nPerCom   := 0
Local nValComis := 0
Local aAreaAnt  := GetArea()
Local aAreaSA3  := sa3->(GetArea())
Local aAreaSD2  := sd2->(GetArea())
Local aAreaSE3  := se3->(GetArea())
Local aAreaZZG  := zzg->(GetArea())

sa3->(dBSetOrder(1))  // A3_FILIAL+A3_COD
sd2->(dBSetOrder(3))  // D2_FILIAL+D2_DOC+D2_SERIE+D2_CLIENTE+D2_LOJA+D2_COD+D2_ITEM
se3->(dBSetOrder(1))  // E3_FILIAL+E3_PREFIXO+E3_NUM+E3_PARCELA+E3_SEQ+E3_VEND
zzg->(dBSetOrder(1))  // ZZG_FILIAL+ZZG_CHVSD2+ZZG_VEND

// Verificar se o tipo da nota esta correto, se encontra o item na nota original e se existe registro de comissao
If ((cTipo = "F" .and. sd2->d2_tipo = "N") .or. ;
	(cTipo = "D" .and. sd1->d1_tipo = "D" .and. sd2->(MSSeek(SD1->(D1_FILIAL+D1_NFORI+D1_SERIORI+D1_FORNECE+D1_LOJA+D1_COD+D1_ITEMORI))))) .and. ;
	se3->(dBSeek(xFilial("SE3")+sd2->d2_serie+sd2->d2_doc))

	zzg->(MSSeek(xFilial("ZZG")+sd2->(d2_filial+d2_doc+d2_serie+d2_cliente+d2_loja+d2_cod+d2_item)))
	While zzg->(!Eof() .and. zzg_filial = xFilial("ZZG") .and. zzg_chvsd2 = sd2->(d2_filial+d2_doc+d2_serie+d2_cliente+d2_loja+d2_cod+d2_item))
		sa3->(MSSeek(xFilial("SA3")+zzg->zzg_vend))

		// Identificar como ser� o calculo da base da comissao
		cValBase := If(cTipo="D","SD1->D1_TOTAL","SD2->D2_TOTAL")

		If sa3->a3_icm = "N"
			cValBase += If(cTipo="D","-SD1->D1_VALICM","-SD2->D2_VALICM")
		Endif
		If sa3->a3_icmsret = "S"
			cValBase += If(cTipo="D","+SD1->D1_ICMSRET","+SD2->D2_ICMSRET")
		Endif
		If sa3->a3_iss = "S"
			cValBase += If(cTipo="D","+SD1->D1_VALISS","+SD2->D2_VALISS")
		Endif
		If sa3->a3_ipi = "S"
			cValBase += If(cTipo="D","+SD1->D1_VALIPI","+SD2->D2_VALIPI")
		Endif
		If sa3->a3_frete = "S"
			cValBase += If(cTipo="D","+SD1->D1_VALFRE","+SD2->D2_VALFRE")
		Endif

		// Comissao compartilhada ou pedido
		If zzg->zzg_criter $ "18"
			nPerCom := zzg->zzg_ppedid
		// Cadastro de vendedor
		ElseIf zzg->zzg_criter = "2"
			nPerCom := zzg->zzg_pvend
		// Cadastro do cliente
		ElseIf zzg->zzg_criter = "3"
			nPerCom := zzg->zzg_pclien
		// Tabela de preco
		ElseIf zzg->zzg_criter = "4"
			nPerCom := zzg->zzg_ptbprc
		// Cadastro de marca
		ElseIf zzg->zzg_criter = "5"
			nPerCom := zzg->zzg_pmarca
		// Condicao de pagamento
		ElseIf zzg->zzg_criter = "6"
			nPerCom := zzg->zzg_pcdpag
		// Cadastro de produtos
		ElseIf zzg->zzg_criter = "7"
			nPerCom := zzg->zzg_pprod
		Endif

		nValComis += &cValBase*nPerCom/100

		zzg->(dBSkip())
	Enddo
Endif

RestArea(aAreaZZG)
RestArea(aAreaSD2)
RestArea(aAreaSE3)
RestArea(aAreaSA3)
RestArea(aAreaAnt)

Return nValComis
/******************************************************************************
* Programa: RFATM021          11/2019            Totalit Solutions            *
* Funcionalidade: Programa para criar lancamentos contabeis ONLINE nas manu-  *
*                 tencoes das comissoes                                       *
* Parametros: cOrigem = M - Manutencao da comissao (inclusao ou alteracao)    *
*                       E - Exclusao da comissao                              *
*                       D - Debito por inadimplencia                          *
*                       C - Credito por inadimplencia                         *
******************************************************************************/
User Function RFATM021(cOrigem)
Local aAreaAnt := GetArea()
Local aAreaSA2 := sa2->(GetArea())
Local aAreaSA3 := sa3->(GetArea())
Local cLote    := "000001"
Local cDoc     := "000001"
Local aItens   := {}
Local aCab     := {}
Local cQuery   := ""
Local cNewAlias := GetNextAlias()
Local cCtaDeb  := ""
Local cCtaCrd  := ""
Local cCCDeb   := ""
Local cCCCrd   := ""
Local cItDeb   := ""
Local cItCrd   := ""
Local cNaturez := ""
Local cItConta := ""
Local nValor   := 0
Local cCtaComD := GetMV("HP_CTACOMD",.F.,"301070102005") // Conta contabil de debito nas manutencoes de comissao para inclusao ou alteracao a maior
Local cCtaComC := GetMV("HP_CTACOMC",.F.,"201011728850") // Conta contabil de credito nas manutencoes de comissao para inclusao ou alteracao a maior
Local cTxtOrig := "" 
 
Private lMsErroAuto := .F.

// Contabilizacao online desabilitada
Return Nil
/*
If GetMV("MV_LOTECON",.F.,"T") == "T"				// Numero de Lote vindo da Tabela SX5 (MV_LOTECON = T)
	cLote := If(cModulo=="CTB",Tabela("09","CON"),Tabela("09",cModulo))
	cLote := Iif(Len(Alltrim(cLote)) < 6, PADL(cLote,6,"0"),cLote)
Else							// Numero de Lote Sequencial pelo CTF (MV_LOTECON = U)
	ctf->(dbSetOrder(1))
	If ctf->(!MsSeek(xFilial("CTF")+Dtos(dDataBase)+"ZZZZZZZZZ",.t.))
		ctf->(dBSkip(-1))         
		If dtos(CTF->CTF_DATA) == dtos(dDataCTF)
			cLote := Soma1(CTF->CTF_LOTE)
		Else
			cLote := "000001"
		Endif
	Endif
Endif

cQuery := "SELECT MAX(CT2_DOC) CT2_DOC FROM "+RetSqlName("CT2")+" WHERE CT2_FILIAL = '"+xFilial("CT2")+"' AND "
cQuery += "CT2_DATA = '"+dtos(dDataBase)+"' AND CT2_LOTE = '"+cLote+"' AND CT2_SBLOTE = '001' AND D_E_L_E_T_ = ' '"
TcQuery cQuery Alias (cNewAlias) New 

If !Empty((cNewAlias)->ct2_doc)
	cDoc := Soma1((cNewAlias)->ct2_doc)
Endif
(cNewAlias)->(dBCloseArea())

// outro debitos e outros creditos
If se3->e3_xorigem $ "78"
	cNaturez := Posicione("SA2",1,xFilial("SA2")+se3->e3_codcli+se3->e3_loja,"SA2->A2_NATUREZ")
Else
	cNaturez := Posicione("SA1",1,xFilial("SA1")+se3->e3_codcli+se3->e3_loja,"SA1->A1_NATUREZ")
Endif

sa2->(dBSetOrder(3))  // A2_FILIAL+A2_CGC 
sa3->(dBSetorder(1))  // A3_FILIAL+A3_COD  

If sa3->(MSSeek(xFilial("SA3")+se3->e3_vend) .and. !Empty(a3_cgc)) .and. sa2->(MSSeek(xFilial("SA2")+sa3->a3_cgc))
	cItConta := "2"+Alltrim(sa2->(a2_cod+a2_loja))                                                                                                                                                                                                                                                                                               
Endif

// Verificar se e debito por inadimplencia, exclusao da comissao ou alteracao com reducao na comissao para definir qual sera o lancamento contabil
If cOrigem $ "ED" .or. (cOrigem = "M" .and. ALTERA .and. m->e3_comis < se3->e3_comis)
	cCtaDeb := cCtaComC  // "201011728850"
	cCtaCrd := cCtaComD  // "301070102005"
	cCCDeb  := ""
	cCCCrd  := Posicione("SED",1,xFilial("SED")+cNaturez,"ED_CCC")
	cItDeb  := cItConta
	cItCrd  := ""
Else
	cCtaDeb := cCtaComD  // "301070102005"
	cCtaCrd := cCtaComC  // "201011728850"
	cCCDeb  := Posicione("SED",1,xFilial("SED")+cNaturez,"ED_CCD")
	cCCCrd  := ""
	cItDeb  := ""
	cItCrd  := cItConta
Endif

// Se for credito ou debito por inadimplencia ou exclusao da comissao, usar o valor da SE3. Na inclusao usar o valor da variavel e 
// na alteracao a diferenca entre o valor original e o novo valor
If cOrigem $ "CDE"
	nValor := Abs(se3->e3_comis)
ElseIf INCLUI
	nValor := m->e3_comis
Else
	nValor := Abs(se3->e3_comis-m->e3_comis)
Endif

If cOrigem = "E"
	cTxtOrig := "EXCLUSAO DE COMISSAO"
Elseif cOrigem = "C"
	cTxtOrig := "CREDITO POR INADIMPLENCIA"
ElseIf cOrigem = "D"
	cTxtOrig := "DEBITO POR INADIMPLENCIA"
Elseif cOrigem = "M" .and. INCLUI
	cTxtOrig := "INCLUSAO DE COMISSAO"
Else
	cTxtOrig := "ALTERACAO DE COMISSAO"
Endif

aCab := {{"DDATALANC" , dDataBase, NIL},;
		 {"CLOTE"     , cLote    , NIL},;
		 {"CSUBLOTE"  , "001"    , NIL},;
		 {"CDOC"      , cDoc     , NIL},;
		 {"CPADRAO"   , ""       , NIL},;
		 {"NTOTINF"   , 0        , NIL},;
		 {"NTOTINFLOT", 0        , NIL}}

aAdd(aItens,{{"CT2_FILIAL", xFilial("CT2"), NIL},;
			 {"CT2_LINHA" , "001"         , NIL},;
			 {"CT2_MOEDLC", "01"          , NIL},;
			 {"CT2_DC"    , "3"           , NIL},;
			 {"CT2_DEBITO", cCtaDeb       , NIL},;
			 {"CT2_CREDIT", cCtaCrd       , NIL},;
			 {"CT2_CCD"   , cCCDeb        , NIL},;
			 {"CT2_CCC"   , cCCCrd        , NIL},;
			 {"CT2_ITEMD" , cItDeb        , NIL},;
			 {"CT2_ITEMC" , cItCrd        , NIL},;
			 {"CT2_VALOR" , nValor        , NIL},;
			 {"CT2_HP"    , ""            , NIL},;
			 {"CT2_ORIGEM", cTxtOrig      , NIL},;
			 {"CT2_HIST"  ,"MANUTENCAO DE COMISSAO", NIL}})

MSExecAuto({|X,Y,Z| CTBA102(X,Y,Z)},aCab,aItens,3)
If lMsErroAuto
	MsgAlert("Erro na contabiliza��o!")
	MostraErro()
EndIf

RestArea(aAreaSA3)
RestArea(aAreaSA2)
RestArea(aAreaAnt)

Return Nil */