#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPMATA490 �Autor  �Microsiga           � Data �  10/10/19   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HPMata490(xHeader,xCols)

Local aAutoI := {} 
Local aAutoE := {}
Local nXC	:=  0
Local nCI	:=  0

nPosVen := aScan(xHeader,{ |x| AllTrim(x[2]) == "E3_VEND" })
nPosCom := aScan(xHeader,{ |x| AllTrim(x[2]) == "E3_COMIS" })
nPosBas := aScan(xHeader,{ |x| AllTrim(x[2]) == "E3_BASE" })
nPosPor := aScan(xHeader,{ |x| AllTrim(x[2]) == "E3_PORC" })

Private lMsErroAuto := .F.

aAdd(aAutoE,{"E3_VEND" 		,SE3->E3_VEND 	,Nil})
aAdd(aAutoE,{"E3_NUM" 		,SE3->E3_NUM 	,Nil})
aAdd(aAutoE,{"E3_CODCLI"	,SE3->E3_CODCLI ,Nil})
aAdd(aAutoE,{"E3_LOJA" 		,SE3->E3_LOJA 	,Nil})
aAdd(aAutoE,{"E3_PREFIXO"	,SE3->E3_PREFIXO,Nil})
aAdd(aAutoE,{"E3_PARCELA"	,SE3->E3_PARCELA,Nil})
aAdd(aAutoE,{"E3_TIPO" 		,SE3->E3_TIPO 	,Nil})

cChvExc 	:= SE3->E3_FILIAL + SE3->E3_VEND + SE3->E3_CODCLI + SE3->E3_LOJA + SE3->E3_PREFIXO + SE3->E3_NUM + SE3->E3_PARCELA
lIncTudo	:= .T.
aComInc 	:= {}

SA1->(dbSetOrder(1))
SA1->(dbSeek(xFilial("SA1")+SE3->E3_CODCLI+SE3->E3_LOJA))

For nXC := 1 to Len(xCols) 
	If xCols[nXC,Len(xHeader)+1]
		Loop
	Endif	

	aAutoI := {} 

	SA3->(dbSetOrder(1))
	SA3->(dbSeek(xFilial("SA3") + xCols[nXC][nPosVen]))

	nNewCom := xCols[nXC][nPosCom] 
	cNewPor := ( nNewCom / SE3->E3_BASE ) * 100
	
	aAdd(aAutoI,{"E3_VEND" 		,SA3->A3_COD 	,Nil})
	aAdd(aAutoI,{"E3_NUM" 		,SE3->E3_NUM 	,Nil})
	aAdd(aAutoI,{"E3_EMISSAO" 	,SE3->E3_EMISSAO,Nil})
	aAdd(aAutoI,{"E3_SERIE" 	,SE3->E3_SERIE 	,Nil})
	aAdd(aAutoI,{"E3_CODCLI" 	,SA1->A1_COD 	,Nil})
	aAdd(aAutoI,{"E3_LOJA" 		,SA1->A1_LOJA 	,Nil})
	aAdd(aAutoI,{"E3_BASE" 		,SE3->E3_BASE 	,Nil})
	aAdd(aAutoI,{"E3_PORC" 		,cNewPor		,Nil})
	aAdd(aAutoI,{"E3_DATA" 		,SE3->E3_DATA 	,Nil})
	aAdd(aAutoI,{"E3_PREFIXO" 	,SE3->E3_PREFIXO,Nil})
	aAdd(aAutoI,{"E3_PARCELA" 	,SE3->E3_PARCELA,Nil})
	aAdd(aAutoI,{"E3_SEQ" 		,''				,Nil})
	aAdd(aAutoI,{"E3_TIPO" 		,SE3->E3_TIPO 	,Nil}) 
	aAdd(aAutoI,{"E3_BAIEMI"	,SE3->E3_BAIEMI	,Nil})
	aAdd(aAutoI,{"E3_PEDIDO" 	,SE3->E3_PEDIDO ,Nil}) 
	aAdd(aAutoI,{"E3_ORIGEM" 	,SE3->E3_ORIGEM ,Nil})
	aAdd(aAutoI,{"E3_VENCTO" 	,SE3->E3_VENCTO ,Nil})
	aAdd(aAutoI,{"E3_PROCCOM" 	,SE3->E3_PROCCOM,Nil})
	aAdd(aAutoI,{"E3_MOEDA" 	,SE3->E3_MOEDA 	,Nil})        
	aAdd(aAutoI,{"E3_SDOC"	 	,SE3->E3_SDOC 	,Nil})
	aAdd(aAutoI,{"E3_XMARCA"	,SE3->E3_XMARCA	,Nil})	  
	aAdd(aAutoI,{"E3_XINADIM"	,SE3->E3_XINADIM,Nil})
	aAdd(aAutoI,{"E3_XORIGEM"	,'9'			,Nil})    
	aAdd(aAutoI,{"E3_XSITUAC"	,SE3->E3_XSITUAC,Nil})
	
	MSExecAuto({|x,y| Mata490(x,y)},aAutoI,3) //Inclusao   
	
	If lMsErroAuto 
		Alert('Falha ao tentar incluir um dos itens da transfer�ncia')
		MostraErro()
		lIncTudo	:= .F. 
		lMsErroAuto := .F.
	Else
		aAdd(aComInc,aAutoI)
	Endif
Next 

If lIncTudo
	SE3->(dbSetOrder(3)) //E3_FILIAL+E3_VEND+E3_CODCLI+E3_LOJA+E3_PREFIXO+E3_NUM+E3_PARCELA
	SE3->(dbSeek(cChvExc))
	
	MSExecAuto({|x,y| Mata490(x,y)},aAutoE,5) //Exclus�o
	
	If lMsErroAuto
		Alert('Falha no momento da exclus�o da comiss�o original. A transfer�ncia n�o ser� executada')
		MostraErro() 
		For nCI := 1 to Len(aComInc)
			MSExecAuto({|x,y| Mata490(x,y)},aComInc[nCI],5) //Exclus�o	
		Next		
	Else
		MsgInfo('Transfer�ncia conclu�da com sucesso') 
		Return .T.		
	Endif
Else
	If Len(aComInc) > 0
		For nCI := 1 to Len(aComInc)
			MSExecAuto({|x,y| Mata490(x,y)},aComInc[nCI],5) //Exclus�o	
		Next
	Endif 
	Alert('Processo n�o executado, verifique os erros e tente novamente.')	
Endif
Return .F.