#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"
/******************************************************************************
* TOTALIT                           11/2019                                   *
* Ponto de entrada antes da exclusao da comissao. Utilizado para efetuar a    *
* contabilizacao do estorno da comissao                                       *
*******************************************************************************/
User Function MA490DE2()

U_RFATM021("E")

Return Nil