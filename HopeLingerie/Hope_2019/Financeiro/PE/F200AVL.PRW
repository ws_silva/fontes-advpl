#INCLUDE "RWMAKE.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"
/*
+----------------------------------------------------------------------------+
!                         FICHA TECNICA DO PROGRAMA                          !
+----------------------------------------------------------------------------+
!   DADOS DO PROGRAMA                                                        !
+------------------+---------------------------------------------------------+
!Tipo              ! Ponto de Entrada                                        !
+------------------+---------------------------------------------------------+
!Modulo            ! FINANCEIRO                                              !
+------------------+---------------------------------------------------------+
!Nome              ! F200AVL                                                 ! 
+------------------+---------------------------------------------------------+
!Descricao         ! P.E. Substituir pequisa Cnab(Contas a Receber)          !
+------------------+---------------------------------------------------------+
!Autor             ! Luiz Fernando Berti                                     !
+------------------+---------------------------------------------------------+
!Data de Criacao   ! 11/07/2012                                              !
+------------------+---------------------------------------------------------+
*/
User Function F200AVL()
Local aAreaSE1 := SE1->(GetArea())
Local aValores := PARAMIXB
Local cNmTt    := aValores[01][01]//Id.Cnab
Local cNsNum   := aValores[01][04]//Nosso Numero
Local xBuffer  := aValores[01][16]//Linha Inteira
Local cBanco   := MV_PAR06
Local cAgencia := MV_PAR07
Local cConta   := MV_PAR08
Local lAchou   := .f.       
Local npos     := 0

/*[01] - N�mero do T�tulo
[02] - Data da Baixa
[03] - Tipo do T�tulo
[04] - Nosso N�mero
[05] - Valor da Despesa
[06] - Valor do Desconto
[07] - Valor do Abatimento
[08] - Valor Recebido
[09] - Juros
[10] - Multa
[11] - Outras Despesas
[12] - Valor do Cr�dito
[13] - Data Cr�dito
[14] - Ocorr�ncia
[15] - Motivo da Baixa
[16] - Linha Inteira
[17] - Data de Vencto*/
 
/*----------------------------------------------------------
	PROTHEUS PADRAO											|
-----------------------------------------------------------*/
dbSelectArea("SE1")
SE1->(dbSetOrder(19))// IdCnab
SE1->(dbGoTop())

If SE1->(DbSeek(Substr(cNmTt,1,10)))
	cNumBc := SE1->E1_PORTADO
	cNumAg := SE1->E1_AGEDEP
	cNumCnt:= SE1->E1_CONTA
	//Verifica se os registros do CNAB estao sendo baixados nos bancos/agencias/contas ligadas ao titulo em questao
	If Alltrim(cNumBc) == Alltrim(cBanco) .and. Alltrim(cNumAg) == Alltrim(cAgencia) .and. Alltrim(cNumCnt) == Alltrim(cConta)

		//Return(.F.)
		If mv_par13 == 2
			//Busca por IdCnab (sem filial)
			SE1->(dbSetOrder(19)) // IdCnab
			If SE1->(DbSeek(Substr(cNmTt,1,10)))
				lAchou  := .t.
				cFilAnt	:= SE1->E1_FILIAL
				If !Empty( xFilial("CT2") )//verIfica se a contabilizacao eh exclusiva...
					mv_par11 := 2  //Desligo contabilizacao on-line
				Endif
			Else
				lAchou := .f.
				DBSelectArea("SE1")
				DBGoTop()
				cNumTit:= ""
			Endif
		Else
			//Busca por IdCnab
			SE1->(dbSetOrder(16)) // Filial+IdCnab
			If !SE1->(DbSeek(xFilial("SE1")+Substr(cNmTt,1,10)))
				DBSelectArea("SE1")
				DBGoTop()
				cNumTit:= ""
				lAchou := .f.
			Else
				lAchou := .t.
			Endif
		Endif
		
		/*----------------------------------------------------------
		|	BUSCA POR NOSSO NUMERO									|
		------------------------------------------------------------*/
		If !lAchou  // !SE1->(Found())	.And. !Empty(cNsNum)
			cQuery := "SELECT R_E_C_N_O_ AS E1_RECNO FROM "+RetSQLName("SE1")
			cQuery += " WHERE  "
			cQuery += " E1_FILIAL = '"+xFilial("SE1")+"' AND "
			cQuery += " E1_NUMBCO = '"+cNsNum+"' AND "
			cQuery += " D_E_L_E_T_ <> '*' "
			If Select("TBSA1") <> 0
				DBSelectArea("TBSA1")
				DBCloseArea()
			Endif
			TCQuery cQuery New Alias "TBSA1"
			If !TBSA1->(Eof())
				SE1->(dbGoTo(TBSA1->E1_RECNO))
				lAchou := .t.
			Else
				nPos := At('/',cNmTt)
				If npos <> 0
					cxNumero := substr(cNmTt,1,npos-1)
					cxParcel := substr(cNmTt,npos+1,3)
				Else
					cxNumero := cNmTt
					cxParcel := "   "
				Endif
				cxNumero := StrZero(VAL(CxNumero),9)
		 	    cQuery := "SELECT R_E_C_N_O_ AS E1_RECNO FROM "+RetSQLName("SE1")
				cQuery += " WHERE  "
				cQuery += " E1_FILIAL = '"+xFilial("SE1")+"' AND "
				cQuery += " E1_NUM = '"+cxNumero+"' AND "
				cQuery += " E1_PARCELA = '"+cxParcel+"' AND "
				cQuery += " D_E_L_E_T_ <> '*' "
				If Select("TBSA1") <> 0
				   DBSelectArea("TBSA1")
				   DBCloseArea()
				Endif
				TCQuery cQuery New Alias "TBSA1"
				If !TBSA1->(Eof())
				   SE1->(dbGoTo(TBSA1->E1_RECNO))
				   lachou := .t.
			    Endif
			Endif
			If Select("TBSA1") <> 0
				DBSelectArea("TBSA1")
				DBCloseArea()
			Endif
			DBSelectArea("SE1")
		Endif   
		If lAchou
		 // MSGSTOP('IREI GRAVAR NOSSO NUMERO')
		  If Len(ALLTRIM(SE1->E1_NUMBCO))== 0
			 SE1->(RecLock("SE1",.f.))
			 SE1->E1_NUMBCO := cNsNum 
			 SE1->(MsUnlock())
		  Endif 
		Endif
	Else
		//Dados dos parametros nao batem com os dados do cnab
		Conout('Banco/Agencia/Conta selecionada nos parametros do CNAB estao divergentes dos registros do arquivo.')
		lAchou:=.f.
	Endif
Endif
RestArea(aAreaSE1)

Return(lAchou) 
