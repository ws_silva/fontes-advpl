#include 'protheus.ch'
#include 'parmtype.ch'

user function F300VALID()
Local lRet := .F.
If SE1->E1_TIPO == 'NCC'
   MsgAlert("Aten��o..  Para compensar um t�tulo com NCCs, posicione no T�tulo Original a ser compensado!", "Verifique!")
ELSE
  lRet := .T.
ENDIF  
Return lRet
