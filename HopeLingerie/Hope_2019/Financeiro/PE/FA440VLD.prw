#include "protheus.ch"
/******************************************************************************
* Programa: FA440VLD       26/08/2019      Oscar Lira - Totalit Solutions     *
* Funcionalidade: P.E. para validar se deve efetuar o c�lculo da comiss�o     *
*                 pela emiss�o da nota                                        *
******************************************************************************/
User Function FA440VLD()
Local lRet := .F. // Retorna falso para que o c�lculo seja feito atrav�s da customiza��o
                                                                               
Return lRet