#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"
/******************************************************************************
* TOTALIT                           11/2019                                   *
* Ponto de entrada na manutencao da comissao (inclusao e alteracao) apos con- *
* firmacao                                                                    *
*******************************************************************************/
User Function A490TDOK()
Local lRet     := .T.

If INCLUI .or. (ALTERA .and. m->e3_comis <> se3->e3_comis)
	U_RFATM021("M")
Endif

Return lRet