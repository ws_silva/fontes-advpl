#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL123 
Relat�rio de entradas
@author Geyson Albano
@since 02/03/2020
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL123()

    Local aPergs := {}
    Local aRet := {}
    Private oReport
    Private cPergCont	:= 'HPREL123'
   

/************************
*Monta pergunte do Log *
************************/

aAdd(aPergs,{1,"Data Inicio",Ctod(Space(8)),"","","","",50,.F.}) // Tipo data
aAdd(aPergs,{1,"Data fim",Ctod(Space(8)),"","","","",50,.F.}) // Tipo data

    If ParamBox(aPergs,"Par�metros...",@aRet)

    oReport := ReportDef()
        If oReport == Nil
        Return( Nil )
        EndIf

    oReport:PrintDialog()
    Else
    MessageBox("Cancelado Pelo Usu�rio!!!", "TOTVS", 16)
    Endif
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Geyson Albano
@since 02 de mar�o de 2020
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

    Local oReport
    Local oSection1
   
    oReport := TReport():New( 'REE', 'REALTORIO DE ENTRADAS', , {|oReport| ReportPrint( oReport ), 'REALTORIO DE ENTRADAS' } )
    oReport:nfontbody:=10
    oReport:SetLandScape()
    oReport:SetTotalInLine(.F.)
    oReport:lParamReadOnly := .T.
    oReport:ShowHeader()

    oSection1 := TRSection():New( oReport, 'REALTORIO DE ENTRADAS', { 'REE', 'SD1','SB1','SA2','SF4','SBM','SBV'})

    TRCell():New( oSection1, 'TES'			        ,'REE', 		'TES',						    	"@!"                        ,10)
    TRCell():New( oSection1, 'DESC_TES'			    ,'REE', 		'DESC_TES',		  					"@!"				        ,04)
    TRCell():New( oSection1, 'ANO'		 	        ,'REE', 		'ANO',		          				"@!"						,10)
    TRCell():New( oSection1, 'NOTA' 			    ,'REE', 		'NOTA',				       			"@!"						,15)
    TRCell():New( oSection1, 'EMISSAO'			    ,'REE', 		'EMISSAO',	       				    "@!"		                ,35)
    TRCell():New( oSection1, 'FORNECEDOR' 			,'REE', 		'FORNECEDOR',			 			"@!"				        ,20)
    TRCell():New( oSection1, 'CNPJ'  			    ,'REE', 		'CNPJ',		    			        "@!"				        ,35)
    TRCell():New( oSection1, 'DESC_FORNECEDOR' 		,'REE', 	    'DESC_FORNECEDOR',                  "@!"		    	        ,35)
    TRCell():New( oSection1, 'UF'	   			    ,'REE', 		'UF',      	   					    "@!"						,20)
    TRCell():New( oSection1, 'TIPO_PROD'	   	    ,'REE', 		'TIPO_PROD',      	   				"@!"						,20)
    TRCell():New( oSection1, 'REF'	   		        ,'REE', 		'REF',  	   					    "@!"						,20)
    TRCell():New( oSection1, 'DESC_PROD'	   	    ,'REE', 		'DESC_PROD', 					    "@!"						,20)
    TRCell():New( oSection1, 'COR'			        ,'REE', 		'COR',			   				    "@!"						,15)
    TRCell():New( oSection1, 'TAMANHO'				,'REE', 		'TAMANHO',				   			"@!"						,10)
    TRCell():New( oSection1, 'UN'				    ,'REE', 		'UN',			   				    "@!"						,20)
    TRCell():New( oSection1, 'QTD'					,'REE', 		'QTD',								"@!"						,06)
    TRCell():New( oSection1, 'VLR_UNIT'				,'REE', 		'VLR_UNIT',		   					"@E 999,99.99"				,10)
    TRCell():New( oSection1, 'VLR_TOTAL'			,'REE', 		'VLR_TOTAL',	   					"@E 999,99.99"				,10)
    TRCell():New( oSection1, 'EAN'					,'REE', 		'EAN',			   					"@!"						,40)
    TRCell():New( oSection1, 'SKU'				    ,'REE', 		'SKU',		   					    "@!"						,20)
    TRCell():New( oSection1, 'DESC_COR'		        ,'REE', 		'DESC_COR',					        "@!"						,10)
    TRCell():New( oSection1, 'IPI'				    ,'REE', 		'IPI',		   					    "@E 999,99.99"				,16)
    TRCell():New( oSection1, 'DESC_GRP_PROD'		,'REE', 		'DESC_GRP_PROD',	   				"@E 999,99.99"				,06)



Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 08 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

    Local oSection1 := oReport:Section(1)
    Local cQuery := ""

    oSection1:Init()
    oSection1:SetHeaderSection(.T.)

    IF Select("REE") > 0
        REE->(dbCloseArea())
    Endif

    cQuery:= "SELECT										                "
    cQuery+= "F4_CODIGO AS TES								                "
    cQuery+= ",F4_TEXTO AS DESC_TES							                "
    cQuery+= ",SUBSTRING(D1_EMISSAO,1,4) AS ANO				                "
    cQuery+= ",D1_DOC AS NOTA								                "
    cQuery+= ",D1_EMISSAO AS EMISSAO						                "
    cQuery+= ",D1_FORNECE AS FORNECEDOR						                "
    cQuery+= ",A2.A2_CGC AS CNPJ							                "
    cQuery+= ",A2_NOME AS DESC_FORNECEDOR					                "
    cQuery+= ",A2_EST AS UF									                "
    cQuery+= ",B1_TIPO AS TIPO_PROD							                "
    cQuery+= ",LEFT(D1_COD,8) AS REF						                "
    cQuery+= ",B1_DESC AS DESC_PROD 						                "
    cQuery+= ",SUBSTRING(D1_COD,9,3) AS COR					                "
    cQuery+= ",SUBSTRING(D1_COD,12,4) AS TAMANHO			                "
    cQuery+= ",B1_UM AS UN									                "
    cQuery+= ",D1_QUANT AS QTD      						                "
    cQuery+= ",D1_VUNIT AS VLR_UNIT							                "
    cQuery+= ",D1_TOTAL AS VLR_TOTAL							                "
    cQuery+= ",B1_CODBAR AS EAN								                "
    cQuery+= ",D1_COD AS SKU								                "
    cQuery+= ",BV_DESCRI AS DESC_COR						                "
    cQuery+= ",D1_VALIPI AS IPI								                "
    cQuery+= ",BM_DESC AS DESC_GRP_PROD						                "
    cQuery+= "FROM  "+RetSqlName("SD1")+" D1 (NOLOCK) 						"
    cQuery+= "INNER JOIN  "+RetSqlName("SB1")+" B1 (NOLOCK) ON				"
    cQuery+= "D1_COD = B1_COD								                "
    cQuery+= "INNER JOIN  "+RetSqlName("SF4")+" F4 (NOLOCK) ON				"
    cQuery+= "D1_TES = F4_CODIGO							                "
    cQuery+= "INNER JOIN  "+RetSqlName("SA2")+" A2 (NOLOCK) ON				"
    cQuery+= "D1_FORNECE = A2_COD							                "
    cQuery+= "AND D1_LOJA = A2_LOJA							                "
    cQuery+= "INNER JOIN  "+RetSqlName("SBM")+" BM (NOLOCK) ON				"
    cQuery+= "B1_GRUPO = BM_GRUPO							                "
    cQuery+= "INNER JOIN  "+RetSqlName("SBV")+" BV (NOLOCK) ON				"
    cQuery+= "SUBSTRING(D1_COD,9,3) = BV_CHAVE				                "
    cQuery+= "AND BV_TABELA  = 'COR'						                "
    cQuery+= "WHERE											                "
    cQuery+= "D1.D_E_L_E_T_ = ''							                "
    cQuery+= "AND B1.D_E_L_E_T_ = ''						                "
    cQuery+= "AND F4.D_E_L_E_T_ = ''						                "
    cQuery+= "AND A2.D_E_L_E_T_ = ''						                "
    cQuery+= "AND BM.D_E_L_E_T_ = ''						                "
    cQuery+= "AND BV.D_E_L_E_T_  =''						                "
    cQuery+= "AND D1_EMISSAO BETWEEN  '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"' " 

    TCQUERY cQuery NEW ALIAS REE

    //ProcRegua()

    While REE->(!EOF())
    //IncProc("Gerando relat�rio...")

        IF oReport:Cancel()
            Exit
        EndIf
        oReport:IncMeter()

        oSection1:Cell("TES"):SetValue(REE->TES)
        oSection1:Cell("TES"):SetAlign("LEFT")

        oSection1:Cell("DESC_TES"):SetValue(REE->DESC_TES)
        oSection1:Cell("DESC_TES"):SetAlign("LEFT")

        oSection1:Cell("ANO"):SetValue(REE->ANO)
        oSection1:Cell("ANO"):SetAlign("LEFT")

        oSection1:Cell("NOTA"):SetValue(REE->NOTA)
        oSection1:Cell("NOTA"):SetAlign("LEFT")

        oSection1:Cell("EMISSAO"):SetValue(REE->EMISSAO)
        oSection1:Cell("EMISSAO"):SetAlign("LEFT")

        oSection1:Cell("FORNECEDOR"):SetValue(REE->FORNECEDOR)
        oSection1:Cell("FORNECEDOR"):SetAlign("LEFT")

        oSection1:Cell("CNPJ"):SetValue(REE->CNPJ)
        oSection1:Cell("CNPJ"):SetAlign("LEFT")

        oSection1:Cell("UF"):SetValue(REE->UF)
        oSection1:Cell("UF"):SetAlign("LEFT")

        oSection1:Cell("TIPO_PROD"):SetValue(REE->TIPO_PROD)
        oSection1:Cell("TIPO_PROD"):SetAlign("LEFT")

        oSection1:Cell("REF"):SetValue(REE->REF)
        oSection1:Cell("REF"):SetAlign("LEFT")

        oSection1:Cell("DESC_PROD"):SetValue(REE->DESC_PROD)
        oSection1:Cell("DESC_PROD"):SetAlign("LEFT")

        oSection1:Cell("COR"):SetValue(REE->COR)
        oSection1:Cell("COR"):SetAlign("LEFT")

        oSection1:Cell("TAMANHO"):SetValue(REE->TAMANHO)
        oSection1:Cell("TAMANHO"):SetAlign("LEFT")

        oSection1:Cell("UN"):SetValue(REE->UN)
        oSection1:Cell("UN"):SetAlign("LEFT")

        oSection1:Cell("QTD"):SetValue(REE->QTD)
        oSection1:Cell("QTD"):SetAlign("LEFT")

        oSection1:Cell("VLR_UNIT"):SetValue(REE->VLR_UNIT)
        oSection1:Cell("VLR_UNIT"):SetAlign("LEFT")

        oSection1:Cell("VLR_TOTAL"):SetValue(REE->VLR_TOTAL)
        oSection1:Cell("VLR_TOTAL"):SetAlign("LEFT")

        oSection1:Cell("EAN"):SetValue(REE->EAN)
        oSection1:Cell("EAN"):SetAlign("LEFT")

        oSection1:Cell("SKU"):SetValue(REE->SKU)
        oSection1:Cell("SKU"):SetAlign("LEFT")

        oSection1:Cell("DESC_COR"):SetValue(REE->DESC_COR)
        oSection1:Cell("DESC_COR"):SetAlign("LEFT")

        oSection1:Cell("IPI"):SetValue(REE->IPI)
        oSection1:Cell("IPI"):SetAlign("LEFT")

        oSection1:Cell("DESC_GRP_PROD"):SetValue(REE->DESC_GRP_PROD)
        oSection1:Cell("DESC_GRP_PROD"):SetAlign("LEFT")

        oSection1:PrintLine()

        REE->(DBSKIP())
    enddo
    REE->(DBCLOSEAREA())
Return( Nil )