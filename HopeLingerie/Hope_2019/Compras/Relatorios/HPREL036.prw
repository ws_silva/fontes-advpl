#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*/{Protheus.doc} HPREL036 
Relatório de Followup do Fornecedor 
@author T.I - HOPE
@since 10/11/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL036()

Private oReport
Private cPergCont	:= 'HPREL036' 

/************************
*Monta pergunte do Log *
************************/

//AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
Endif

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2

	oReport := TReport():New( 'FLW', 'FOLLOW-UP DE COMPRAS', , {|oReport| ReportPrint( oReport ), 'FOLLOW-UP DE COMPRAS' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'FOLLOW-UP DE COMPRAS', { 'FLW', 'SC1','SB4','CTT','SBV','SBM','SC7','SA2'})
	
	TRCell():New( oSection1, 'COD_FORNEC'				,'FLW', 		'COD_FORNEC',		  			    "@!"				        ,06)		
	TRCell():New( oSection1, 'FORNECEDOR'				,'FLW', 		'FORNECEDOR',		  			    "@!"				        ,35)		
	TRCell():New( oSection1, 'FILIAL_PEDIDO'			,'FLW', 		'FILIAL_PEDIDO',				    "@!"                        ,10)
	TRCell():New( oSection1, 'FILIAL_ENTREGA'			,'FLW', 		'FILIAL_ENTREGA',	  			    "@!"				        ,10)
	TRCell():New( oSection1, 'TIPO'						,'FLW', 		'TIPO',				  			    "@!"				        ,04)
	TRCell():New( oSection1, 'SKU'						,'FLW', 		'SKU',				  			    "@!"				        ,15)
	TRCell():New( oSection1, 'PRODUTO'					,'FLW', 		'PRODUTO',			  			    "@!"				        ,15)
	TRCell():New( oSection1, 'DESCRICAO'				,'FLW', 		'DESCRICAO',		  			    "@!"				        ,35)
	TRCell():New( oSection1, 'COD_COR'					,'FLW', 		'COD_COR',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DESC_COR'					,'FLW', 		'DESC_COR',			  			    "@!"				        ,35)
	TRCell():New( oSection1, 'TAM'						,'FLW', 		'TAM',				  			    "@!"				        ,06)
	TRCell():New( oSection1, 'UNID_MEDIDA'				,'FLW', 		'UNID_MEDIDA',		  			    "@!"				        ,04)
	TRCell():New( oSection1, 'SOLICITACAO'				,'FLW', 		'SOLICITACAO',		  			    "@!"				        ,10)
	TRCell():New( oSection1, 'SC_COMPRADOR'				,'FLW', 		'SC_COMPRADOR',		  			    "@!"				        ,20)
	TRCell():New( oSection1, 'SOLICITANTE'				,'FLW', 		'SOLICITANTE',		  			    "@!"				        ,25)
	TRCell():New( oSection1, 'DT_EMISSAO_SC'			,'FLW', 		'DT_EMISSAO_SC',     			    ""							,08)
	TRCell():New( oSection1, 'QTDE_SOLICITADA'			,'FLW', 		'QTDE_SOLICITADA',	  			    "@E 999,999,999.999999"     ,16)

	TRCell():New( oSection1, 'TIPO_REQUISICAO'			,'FLW', 		'TIPO_REQUISICAO',	  				"@!"				        ,03)
	TRCell():New( oSection1, 'COLECAO' 					,'FLW', 		'COLECAO',		  		   			"@!"				        ,50)
	TRCell():New( oSection1, 'SUB_COLECAO' 				,'FLW', 		'SUB_COLECAO',		  				"@!"				        ,50)
	TRCell():New( oSection1, 'DT_NEC_DEMANDA' 			,'FLW', 		'DT_NEC_DEMANDA',			 	 	""					        ,08)
	TRCell():New( oSection1, 'QUANT_MOSTRUARIO' 		,'FLW', 		'QUANT_MOSTRUARIO',	   				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'DT_NEC_MOSTRUARIO'		,'FLW',		 	'DT_NEC_MOSTRUARIO',				""   				        ,08)

	TRCell():New( oSection1, 'PEDIDO'					,'FLW', 		'PEDIDO',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'PED_COMPRADOR'			,'FLW', 		'PED_COMPRADOR',		  			"@!"					    ,20)
	TRCell():New( oSection1, 'STATUS_APROVACAO'			,'FLW', 		'STATUS_APROVACAO',	  				"@!"				        ,06)
	//TRCell():New( oSection1, 'FORNECEDOR'				,'FLW', 		'FORNECEDOR',		  			  	"@!"				        ,35)
	TRCell():New( oSection1, 'CCUSTO'					,'FLW', 		'CCUSTO',			  			    "@!"				        ,10)
	TRCell():New( oSection1, 'DT_EMISSAO_PED'			,'FLW', 		'DT_EMISSAO_PED',	  				""  				        ,08)
	TRCell():New( oSection1, 'VLR_TOTAL_ITEM'			,'FLW', 		'VLR_TOTAL_ITEM',	  				"@E 999,999,999.99"		    ,14)
	TRCell():New( oSection1, 'QTDE_PEDIDO'				,'FLW', 		'QTDE_PEDIDO',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'DATA_ENTREGA'				,'FLW', 		'DATA_ENTREGA',		  				""				            ,08)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'			,'FLW', 		'QTDE_ENTREGUE',	  				"@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'SALDO'					,'FLW', 		'SALDO',			  			    "@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'CUSTO'					,'FLW', 		'CUSTO',			  			    "@E 99,999.99"		       	,08)
	TRCell():New( oSection1, 'QUITADO'					,'FLW', 		'QUITADO',			  			 	"@!"				     	,06)
	TRCell():New( oSection1, 'ENCERRADO'				,'FLW', 		'ENCERRADO',			  			"@!"				       	,01)

	TRCell():New( oSection1, 'NOTA_1FISCAL'				,'FLW', 		'NOTA_1FISCAL',		  			    "@!"				        ,09)
	TRCell():New( oSection1, 'DATA_1FATURA'				,'FLW', 		'DATA_1FATURA',		  			    ""				        	,08)
	TRCell():New( oSection1, 'QTDE_1FATURA'				,'FLW', 		'QTDE_1FATURA',		  			    "@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_1'					,'FLW', 		'MODAL_1',				  		    "@!"				        ,01)
	TRCell():New( oSection1, 'X1OBSERVACAO'				,'FLW', 		'X1OBSERVACAO',			  		    "@!"				        ,90)
	TRCell():New( oSection1, 'PREV_1RECEBIMENTO'		,'FLW', 		'PREV_1RECEBIMENTO',	  		    ""				        ,08)
	TRCell():New( oSection1, 'SALDO_1'					,'FLW', 		'SALDO_1',				  		    "@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'DATA_1SALDO'				,'FLW', 		'DATA_1SALDO',			  		    ""				        ,08)
	TRCell():New( oSection1, 'NOTA_2FSCAL'				,'FLW', 		'NOTA_2FISCAL',		  			    "@!"				        ,09)
	TRCell():New( oSection1, 'DATA_2FATURA'				,'FLW', 		'DATA_2FATURA',		  			    ""				        	,08)
	TRCell():New( oSection1, 'QTDE_2FATURA'				,'FLW', 		'QTDE_2FATURA',		  			    "@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'MODAL_2'					,'FLW', 		'MODAL_2',				  		    "@!"				        ,01)
	//TRCell():New( oSection1, 'X2OBSERVACAO'			,'FLW', 		'X2OBSERVACAO',			  		    "@!"				        ,90)
	TRCell():New( oSection1, 'PREV_2RECEBIMENTO'		,'FLW', 		'PREV_2RECEBIMENTO',	  		    ""				        ,08)
	TRCell():New( oSection1, 'SALDO_2'					,'FLW', 		'SALDO_2',				  		    "@E 999,999,999.999999"     ,16)
	TRCell():New( oSection1, 'DATA_2SALDO'				,'FLW', 		'DATA_2SALDO',			  		    ""				        	,08)
	TRCell():New( oSection1, 'NOTA_3FISCAL'				,'FLW', 		'NOTA_3FISCAL'	,		  		   	"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_3FATURA'				,'FLW', 		'DATA_3FATURA',			  		    ""				        	,08)
	TRCell():New( oSection1, 'QTDE_3FATURA'				,'FLW', 		'QTDE_3FATURA',			  		    "@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_3'					,'FLW', 		'MODAL_3',				  			"@!"				        ,01)
	//TRCell():New( oSection1, 'X3OBSERVACAO'			,'FLW', 		'X3OBSERVACAO',			  			"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_3RECEBIMENTO'		,'FLW', 		'PREV_3RECEBIMENTO',	  			""				        ,08)
	TRCell():New( oSection1, 'SALDO_3'					,'FLW', 		'SALDO_3',				  			"@E 999,999,999.999999"	    ,16)
	TRCell():New( oSection1, 'DATA_3SALDO'				,'FLW', 		'DATA_3SALDO',			  			""				       		,08)
	TRCell():New( oSection1, 'NOTA_4FISCAL'				,'FLW', 		'NOTA_4FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_4FATURA'				,'FLW', 		'DATA_4FATURA',		  				""				      		,08)
	TRCell():New( oSection1, 'QTDE_4FATURA'				,'FLW', 		'QTDE_4FATURA',		  				"@E 999,999,999.999999"	 	,16)
	TRCell():New( oSection1, 'MODAL_4'					,'FLW', 		'MODAL_4',				  			"@!"				        ,01)
	//TRCell():New( oSection1, 'X4OBSERVACAO'			,'FLW', 		'X4OBSERVACAO',			  			"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_4RECEBIMENTO'		,'FLW', 		'PREV_4RECEBIMENTO',	  			""				        ,08)
	TRCell():New( oSection1, 'SALDO_4'					,'FLW', 		'SALDO_4',				  			"@E 999,999,999.999999"		,09)
	TRCell():New( oSection1, 'DATA_4SALDO'				,'FLW', 		'DATA_4SALDO',			  			""						,16)
	TRCell():New( oSection1, 'NOTA_5FISCAL'				,'FLW', 		'NOTA_5FISCAL',		  				"@!"				        ,09)
	TRCell():New( oSection1, 'DATA_5FATURA'				,'FLW', 		'DATA_5FATURA',		  				""				        	,08)
	TRCell():New( oSection1, 'QTDE_5FATURA'				,'FLW', 		'QTDE_5FATURA',		  				"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'MODAL_5'					,'FLW', 		'MODAL_5',				  			"@!"				        ,01)
	//TRCell():New( oSection1, 'X5OBSERVACAO'			,'FLW', 		'X5OBSERVACAO',			  			"@!"				        ,90)
	TRCell():New( oSection1, 'PREV_5RECEBIMENTO'		,'FLW', 		'PREV_5RECEBIMENTO',	  			""				        ,08)
	TRCell():New( oSection1, 'SALDO_5'					,'FLW', 		'SALDO_5',				  			"@R 999,999,999.999999"     ,09)
	TRCell():New( oSection1, 'DATA_5SALDO'				,'FLW', 		'DATA_5SALDO',			  			""  				        ,08)
	TRCell():New( oSection1, 'TRANSITO'			   		,'FLW', 		'TRANSITO',			  	     		"@E 999,999,999.999999"		,16)
	TRCell():New( oSection1, 'SALDO_FINAL'				,'FLW', 		'SALDO_FINAL',			  	  	  	"@E 999,999,999.999999"		,16)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________
static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
	Local lEncerra:= .f.
	Local cDscSubC:= ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	If Select("FLW") > 0
		FLW->(dbCloseArea())
	Endif
      
	cQuery += "SELECT  "+ CRLF
	cQuery += "ISNULL(SC7.C7_NUM ,'') AS PEDIDO, "+ CRLF
	cQuery += "SC1.C1_NUM AS SOLICITACAO,"+ CRLF
	cQuery += "SC1.C1_USER AS SC_COMPRADOR , "+ CRLF
	cQuery += "SC7.C7_USER AS PED_COMPRADOR , "+ CRLF
	cQuery += "SC1.C1_FILIAL AS FILIAL_PEDIDO, "+ CRLF
	cQuery += "SC7.C7_FILENT AS FILIAL_ENTREGA, "+ CRLF
	cQuery += "SB4.B4_TIPO AS TIPO, "+ CRLF
	cQuery += "SC1.C1_PRODUTO AS SKU, "+ CRLF
	cQuery += "SB4.B4_COD AS PRODUTO, "+ CRLF
	cQuery += "SB4.B4_DESC AS DESCRICAO, "+ CRLF
	cQuery += "SUBSTRING(SC1.C1_PRODUTO,9,3) AS COD_COR, "+ CRLF
	cQuery += "SBV.BV_DESCRI AS DESC_COR, "+ CRLF
	cQuery += "RIGHT(SC1.C1_PRODUTO,4)  AS TAM, "+ CRLF
	cQuery += "SB4.B4_UM AS UNID_MEDIDA, "+ CRLF
	cQuery += "SC1.C1_SOLICIT AS SOLICITANTE, "+ CRLF
	//cQuery += "RIGHT(SC1.C1_EMISSAO,2)+'/'+SUBSTRING(SC1.C1_EMISSAO,5,2)+'/'+LEFT(SC1.C1_EMISSAO,4) AS DT_EMISSAO_SC, "+ CRLF
	cQuery += "SC1.C1_EMISSAO AS DT_EMISSAO_SC, "+ CRLF
	cQuery += "SC1.C1_QUANT AS QTDE_SOLICITADA, "+ CRLF
	cQuery += "SC1.C1_XTPREQU AS TIPO_REQUISICAO,  "+ CRLF
	cQuery += "SC1.C1_XCOLECA AS COLECAO,  "+ CRLF
	cQuery += "SC1.C1_XSBCOLE AS SUB_COLECAO,  "+ CRLF
	//cQuery += "RIGHT(SC1.C1_XDTNPCP,2)+'/'+SUBSTRING(SC1.C1_XDTNPCP,5,2)+'/'+LEFT(SC1.C1_XDTNPCP,4) AS DT_NEC_DEMANDA, "+ CRLF
	cQuery += "SC1.C1_XDTNPCP AS DT_NEC_DEMANDA, "+ CRLF
	cQuery += "SC1.C1_XQTDMOS AS QUANT_MOSTRUARIO,  "+ CRLF
	//cQuery += "RIGHT(SC1.C1_XDTPCPM,2)+'/'+SUBSTRING(SC1.C1_XDTPCPM,5,2)+'/'+LEFT(SC1.C1_XDTPCPM,4) AS DT_NEC_MOSTRUARIO, "+ CRLF
	cQuery += "SC1.C1_XDTPCPM AS DT_NEC_MOSTRUARIO, "+ CRLF
	cQuery += "SC7.C7_CONAPRO AS STATUS_APROVACAO, "+ CRLF
 	cQuery += "SA2.A2_COD  AS CODFOR, "+ CRLF
	cQuery += "SA2.A2_NOME AS FORNECEDOR, "+ CRLF
	cQuery += "CTT.CTT_DESC01 AS CCUSTO, "+ CRLF
	//cQuery += "RIGHT(SC7.C7_EMISSAO,2)+'/'+SUBSTRING(SC7.C7_EMISSAO,5,2)+'/'+LEFT(SC7.C7_EMISSAO,4) AS DT_EMISSAO_PED, "+ CRLF
	cQuery += "SC7.C7_EMISSAO AS DT_EMISSAO_PED, "+ CRLF
	cQuery += "SC7.C7_TOTAL AS VLR_TOTAL_ITEM, "+ CRLF
	cQuery += "SC7.C7_QUANT AS QTDE_PEDIDO, "+ CRLF
	//cQuery += "RIGHT(SC7.C7_DATPRF,2)+'/'+SUBSTRING(SC7.C7_DATPRF,5,2)+'/'+LEFT(SC7.C7_DATPRF,4) AS DATA_ENTREGA, "+ CRLF
	cQuery += "SC7.C7_DATPRF AS DATA_ENTREGA, "+ CRLF
	cQuery += "SC7.C7_QUJE AS QTDE_ENTREGUE , "+ CRLF
	cQuery += "(SC7.C7_QUANT - SC7.C7_QUJE) AS SALDO, "+ CRLF
	cQuery += "(SC7.C7_TOTAL / SC7.C7_QUANT ) AS CUSTO, "+ CRLF
	cQuery += "SC7.C7_RESIDUO AS QUITADO,  "+ CRLF
	cQuery += "SC7.C7_X1NF AS NOTA_1FISCAL, "+ CRLF
	//cQuery += "RIGHT(SC7.C7_X1DT_FA,2)+'/'+SUBSTRING(SC7.C7_X1DT_FA,5,2)+'/'+LEFT(SC7.C7_X1DT_FA,4) AS DATA_1FATURA, "+ CRLF
	cQuery += "SC7.C7_X1DT_FA AS DATA_1FATURA, "+ CRLF
	cQuery += "SC7.C7_X1QTD_F AS QTDE_1FATURA, "+ CRLF
	cQuery += "SC7.C7_X1MODAL AS MODAL_1, "+ CRLF
	cQuery += "SC7.C7_X1OBS   AS X1OBSERVACAO, "+ CRLF
	cQuery += "SC7.C7_X1PRREC AS PREV_1RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X1SALDO AS SALDO_1, "+ CRLF
	cQuery += "SC7.C7_X1DTSAL AS DATA_1SALDO, "+ CRLF
	cQuery += "SC7.C7_X2NF AS NOTA_2FISCAL, "+ CRLF
	//cQuery += "RIGHT(SC7.C7_X2DT_FA,2)+'/'+SUBSTRING(SC7.C7_X2DT_FA,5,2)+'/'+LEFT(SC7.C7_X2DT_FA,4) AS DATA_2FATURA, "+ CRLF
	cQuery += "SC7.C7_X2DT_FA AS DATA_2FATURA, "+ CRLF
	cQuery += "SC7.C7_X2QTD_F AS QTDE_2FATURA, "+ CRLF
	cQuery += "SC7.C7_X2MODAL AS MODAL_2, "+ CRLF
	cQuery += "SC7.C7_X2OBS   AS X2OBSERVACAO, "+ CRLF
	cQuery += "SC7.C7_X2PRREC AS PREV_2RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X2SALDO AS SALDO_2, "+ CRLF
	cQuery += "SC7.C7_X2DTSAL AS DATA_2SALDO, "+ CRLF
	cQuery += "SC7.C7_X3NF AS NOTA_3FISCAL, "+ CRLF
	//cQuery += "RIGHT(SC7.C7_X3DT_FA,2)+'/'+SUBSTRING(SC7.C7_X3DT_FA,5,2)+'/'+LEFT(SC7.C7_X3DT_FA,4) AS DATA_3FATURA, "+ CRLF
	cQuery += "SC7.C7_X3DT_FA AS DATA_3FATURA, "+ CRLF
	cQuery += "SC7.C7_X3QTD_F AS QTDE_3FATURA, "+ CRLF
	cQuery += "SC7.C7_X3MODAL AS MODAL_3, "+ CRLF
	cQuery += "SC7.C7_X3OBS   AS X3OBSERVACAO, "+ CRLF
	cQuery += "SC7.C7_X3PRREC AS PREV_3RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X3SALDO AS SALDO_3, "+ CRLF
	cQuery += "SC7.C7_X3DTSAL AS DATA_3SALDO, "+ CRLF
	cQuery += "SC7.C7_X4NF AS NOTA_4FISCAL, "+ CRLF
	//cQuery += "RIGHT(SC7.C7_X4DT_FA,2)+'/'+SUBSTRING(SC7.C7_X4DT_FA,5,2)+'/'+LEFT(SC7.C7_X4DT_FA,4) AS DATA_4FATURA, "+ CRLF
	cQuery += "SC7.C7_X4DT_FA AS DATA_4FATURA, "+ CRLF
	cQuery += "SC7.C7_X4QTD_F AS QTDE_4FATURA, "+ CRLF
	cQuery += "SC7.C7_X4MODAL AS MODAL_4, "+ CRLF
	cQuery += "SC7.C7_X4OBS   AS X4OBSERVACAO, "+ CRLF
	cQuery += "SC7.C7_X4PRREC AS PREV_4RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X4SALDO AS SALDO_4, "+ CRLF
	cQuery += "SC7.C7_X4DTSAL AS DATA_4SALDO, "+ CRLF
	cQuery += "SC7.C7_X5NF AS NOTA_5FISCAL, "+ CRLF
	//cQuery += "RIGHT(SC7.C7_X5DT_FA,2)+'/'+SUBSTRING(SC7.C7_X5DT_FA,5,2)+'/'+LEFT(SC7.C7_X5DT_FA,4) AS DATA_5FATURA, "+ CRLF
	cQuery += "SC7.C7_X5DT_FA AS DATA_5FATURA, "+ CRLF
	cQuery += "SC7.C7_X5QTD_F AS QTDE_5FATURA, "+ CRLF
	cQuery += "SC7.C7_X5MODAL AS MODAL_5, "+ CRLF
	cQuery += "SC7.C7_X5OBS   AS X5OBSERVACAO, "+ CRLF
	cQuery += "SC7.C7_X5PRREC AS PREV_5RECEBIMENTO, "+ CRLF
	cQuery += "SC7.C7_X5SALDO AS SALDO_5, "+ CRLF
	cQuery += "SC7.C7_X5DTSAL AS DATA_5SALDO "+ CRLF
	cQuery += " FROM "+RetSqlName('SC1')+" SC1 "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SB4')+" SB4 ON SB4.B4_COD=LEFT(SC1.C1_PRODUTO,8) AND SB4.D_E_L_E_T_='' " + CRLF
	
	

	cQuery += " LEFT JOIN "+RetSqlName('CTT')+" CTT ON CTT.CTT_CUSTO=SC1.C1_CC "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SBV')+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC1.C1_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SBM')+" SBM ON SBM.BM_GRUPO=SB4.B4_GRUPO AND SBM.D_E_L_E_T_='' "+ CRLF
	cQuery += " LEFT JOIN "+RetSqlName('SC7')+" SC7 ON SC7.C7_NUMSC=SC1.C1_NUM AND SC7.C7_ITEMSC=SC1.C1_ITEM AND SC7.C7_FILIAL=SC1.C1_FILIAL AND  SC7.D_E_L_E_T_='' " + CRLF
	
	If MV_PAR06 == 1 // 1 = Aberto || 2 = Finalizado || 3 = Todos
		cQuery += " AND SC7.C7_QUJE < SC7.C7_QUANT "+ CRLF//QTDE_ENTREGUE < QTDE_PEDIDO" 
	Elseif  MV_PAR06 == 2 //Finalizado
		cQuery += " AND SC7.C7_QUJE = SC7.C7_QUANT "+ CRLF //AND QTDE_ENTREGUE = QTDE_PEDIDO"
	Endif

	If MV_PAR07 == 1 // 1 = Pedido || 2 = Todos ||
		cQuery += " AND SC7.C7_DATPRF BETWEEN '"+ DTOS(mv_par08) +"' AND '"+ DTOS(mv_par09) +"' " + CRLF
	Endif
	 
	cQuery += " LEFT JOIN SA2010 SA2 ON  SA2.A2_COD=SC1.C1_FORNECE AND SA2.A2_LOJA = SC1.C1_LOJA"+ CRLF
	cQuery += " WHERE SC1.D_E_L_E_T_= ''  " + CRLF
//Tipo
	If !Empty(mv_par01)
		cQuery += " AND SB4.B4_TIPO  IN "+Formatin(Upper(Alltrim(mv_par01)),";")+"  " + CRLF
	Endif

 	//Codigo Base do Produto (Referencia)
	If !Empty(mv_par03) 
		cQuery += " AND SB4.B4_COD = '"+Alltrim(mv_par03)+"' " + CRLF
	Endif
	//Numero da Solicitacao
	If !Empty(mv_par02)
		cQuery += " AND SC1.C1_NUM = '"+Alltrim(mv_par02)+"' " + CRLF
	Endif
	
	//Emissao SC
	If !Empty(mv_par04)
		cQuery += " AND SC1.C1_EMISSAO BETWEEN '"+ DTOS(mv_par04) +"' AND '"+ DTOS(mv_par05) +"' " + CRLF
	Endif
	
	cQuery += "UNION  " + CRLF
	

cQuery += "SELECT"+ CRLF
 cQuery += "SC7.C7_NUM AS PEDIDO, "+ CRLF
 cQuery += "ISNULL(SC1.C1_NUM,'') AS SOLICITACAO,"+ CRLF
 cQuery += "SC1.C1_USER AS SC_COMPRADOR , "+ CRLF
 cQuery += "SC7.C7_USER AS PED_COMPRADOR , "+ CRLF
 cQuery += "SC7.C7_FILIAL AS FILIAL_PEDIDO, "+ CRLF
 cQuery += "SC7.C7_FILENT AS FILIAL_ENTREGA, "+ CRLF
 cQuery += "SB4.B4_TIPO AS TIPO, "+ CRLF
 cQuery += "SC7.C7_PRODUTO AS SKU, "+ CRLF
 cQuery += "SB4.B4_COD AS PRODUTO, "+ CRLF
 cQuery += "SB4.B4_DESC AS DESCRICAO, "+ CRLF
 cQuery += "SUBSTRING(SC7.C7_PRODUTO,9,3) AS COD_COR, "+ CRLF
 cQuery += "SBV.BV_DESCRI AS DESC_COR, "+ CRLF
 cQuery += "RIGHT(SC7.C7_PRODUTO,4)  AS TAM, "+ CRLF
 cQuery += "SB4.B4_UM AS UNID_MEDIDA, "+ CRLF
 cQuery += "SC1.C1_SOLICIT AS SOLICITANTE, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_EMISSAO,2)+'/'+SUBSTRING(SC7.C7_EMISSAO,5,2)+'/'+LEFT(SC7.C7_EMISSAO,4) AS DT_EMISSAO_SC, "+ CRLF
 cQuery += "SC7.C7_EMISSAO AS DT_EMISSAO_SC, "+ CRLF
 cQuery += "SC7.C7_QUANT AS QTDE_SOLICITADA, "+ CRLF
 cQuery += "SC1.C1_XTPREQU AS TIPO_REQUISICAO,  "+ CRLF
 cQuery += "SC1.C1_XCOLECA AS COLECAO,  "+ CRLF
 cQuery += "SC1.C1_XSBCOLE AS SUB_COLECAO,  "+ CRLF
 //cQuery += "RIGHT(SC1.C1_XDTNPCP,2)+'/'+SUBSTRING(SC1.C1_XDTNPCP,5,2)+'/'+LEFT(SC1.C1_XDTNPCP,4) AS DT_NEC_DEMANDA, "+ CRLF
 cQuery += "SC1.C1_XDTNPCP AS DT_NEC_DEMANDA, "+ CRLF
 cQuery += "SC1.C1_XQTDMOS AS QUANT_MOSTRUARIO,  "+ CRLF
 //cQuery += "RIGHT(SC1.C1_XDTPCPM,2)+'/'+SUBSTRING(SC1.C1_XDTPCPM,5,2)+'/'+LEFT(SC1.C1_XDTPCPM,4) AS DT_NEC_MOSTRUARIO, "+ CRLF
 cQuery += "SC1.C1_XDTPCPM DT_NEC_MOSTRUARIO, "+ CRLF
 cQuery += "SC7.C7_CONAPRO AS STATUS_APROVACAO, "+ CRLF
 cQuery += "SA2.A2_COD  AS CODFOR, "+ CRLF
 cQuery += "SA2.A2_NOME AS FORNECEDOR, "+ CRLF
 cQuery += "CTT.CTT_DESC01 AS CCUSTO, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_EMISSAO,2)+'/'+SUBSTRING(SC7.C7_EMISSAO,5,2)+'/'+LEFT(SC7.C7_EMISSAO,4) AS DT_EMISSAO_PED, "+ CRLF
 cQuery += "SC7.C7_EMISSAO AS DT_EMISSAO_PED, "+ CRLF
 cQuery += "SC7.C7_TOTAL AS VLR_TOTAL_ITEM, "+ CRLF
 cQuery += "SC7.C7_QUANT AS QTDE_PEDIDO, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_DATPRF,2)+'/'+SUBSTRING(SC7.C7_DATPRF,5,2)+'/'+LEFT(SC7.C7_DATPRF,4) AS DATA_ENTREGA, "+ CRLF
 cQuery += "SC7.C7_DATPRF AS DATA_ENTREGA, "+ CRLF
 cQuery += "SC7.C7_QUJE AS QTDE_ENTREGUE , "+ CRLF
 cQuery += "(SC7.C7_QUANT - SC7.C7_QUJE) AS SALDO, "+ CRLF
 cQuery += "(SC7.C7_TOTAL / SC7.C7_QUANT ) AS CUSTO, "+ CRLF
 cQuery += "SC7.C7_RESIDUO AS QUITADO,  "+ CRLF
 cQuery += "SC7.C7_X1NF AS NOTA_1FISCAL, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_X1DT_FA,2)+'/'+SUBSTRING(SC7.C7_X1DT_FA,5,2)+'/'+LEFT(SC7.C7_X1DT_FA,4) AS DATA_1FATURA, "+ CRLF
 cQuery += "SC7.C7_X1DT_FA AS DATA_1FATURA, "+ CRLF
 cQuery += "SC7.C7_X1QTD_F AS QTDE_1FATURA, "+ CRLF
 cQuery += "SC7.C7_X1MODAL AS MODAL_1, "+ CRLF
 cQuery += "SC7.C7_X1OBS   AS X1OBSERVACAO, "+ CRLF
 cQuery += "SC7.C7_X1PRREC AS PREV_1RECEBIMENTO, "+ CRLF
 cQuery += "SC7.C7_X1SALDO AS SALDO_1, "+ CRLF
 cQuery += "SC7.C7_X1DTSAL AS DATA_1SALDO, "+ CRLF
 cQuery += "SC7.C7_X2NF AS NOTA_2FISCAL, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_X2DT_FA,2)+'/'+SUBSTRING(SC7.C7_X2DT_FA,5,2)+'/'+LEFT(SC7.C7_X2DT_FA,4) AS DATA_2FATURA, "+ CRLF
 cQuery += "SC7.C7_X2DT_FA AS DATA_2FATURA, "+ CRLF
 cQuery += "SC7.C7_X2QTD_F AS QTDE_2FATURA, 
 cQuery += "SC7.C7_X2MODAL AS MODAL_2, "+ CRLF
 cQuery += "SC7.C7_X2OBS   AS X2OBSERVACAO, "+ CRLF
 cQuery += "SC7.C7_X2PRREC AS PREV_2RECEBIMENTO, "+ CRLF
 cQuery += "SC7.C7_X2SALDO AS SALDO_2, "+ CRLF
 cQuery += "SC7.C7_X2DTSAL AS DATA_2SALDO, "+ CRLF
 cQuery += "SC7.C7_X3NF AS NOTA_3FISCAL, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_X3DT_FA,2)+'/'+SUBSTRING(SC7.C7_X3DT_FA,5,2)+'/'+LEFT(SC7.C7_X3DT_FA,4) AS DATA_3FATURA, "+ CRLF
 cQuery += "SC7.C7_X3DT_FA AS DATA_3FATURA, "+ CRLF
 cQuery += "SC7.C7_X3QTD_F AS QTDE_3FATURA, "+ CRLF
 cQuery += "SC7.C7_X3MODAL AS MODAL_3, "+ CRLF
 cQuery += "SC7.C7_X3OBS   AS X3OBSERVACAO, "+ CRLF
 cQuery += "SC7.C7_X3PRREC AS PREV_3RECEBIMENTO, "+ CRLF
 cQuery += "SC7.C7_X3SALDO AS SALDO_3, "+ CRLF
 cQuery += "SC7.C7_X3DTSAL AS DATA_3SALDO, "+ CRLF
 cQuery += "SC7.C7_X4NF AS NOTA_4FISCAL, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_X4DT_FA,2)+'/'+SUBSTRING(SC7.C7_X4DT_FA,5,2)+'/'+LEFT(SC7.C7_X4DT_FA,4) AS DATA_4FATURA, "+ CRLF
 cQuery += "SC7.C7_X4DT_FA AS DATA_4FATURA, "+ CRLF
 cQuery += "SC7.C7_X4QTD_F AS QTDE_4FATURA, "+ CRLF
 cQuery += "SC7.C7_X4MODAL AS MODAL_4, "+ CRLF
 cQuery += "SC7.C7_X4OBS   AS X4OBSERVACAO, "+ CRLF
 cQuery += "SC7.C7_X4PRREC AS PREV_4RECEBIMENTO, "+ CRLF
 cQuery += "SC7.C7_X4SALDO AS SALDO_4, "+ CRLF
 cQuery += "SC7.C7_X4DTSAL AS DATA_4SALDO, "+ CRLF
 cQuery += "SC7.C7_X5NF AS NOTA_5FISCAL, "+ CRLF
 //cQuery += "RIGHT(SC7.C7_X5DT_FA,2)+'/'+SUBSTRING(SC7.C7_X5DT_FA,5,2)+'/'+LEFT(SC7.C7_X5DT_FA,4) AS DATA_5FATURA, "+ CRLF
 cQuery += "SC7.C7_X5DT_FA AS DATA_5FATURA, "+ CRLF
 cQuery += "SC7.C7_X5QTD_F AS QTDE_5FATURA, "+ CRLF
 cQuery += "SC7.C7_X5MODAL AS MODAL_5, "+ CRLF
 cQuery += "SC7.C7_X5OBS   AS X5OBSERVACAO, "+ CRLF
 cQuery += "SC7.C7_X5PRREC AS PREV_5RECEBIMENTO, "+ CRLF
 cQuery += "SC7.C7_X5SALDO AS SALDO_5, "+ CRLF
 cQuery += "SC7.C7_X5DTSAL AS DATA_5SALDO "+ CRLF
 cQuery += " FROM "+RetSqlName('SC7')+" SC7 "+ CRLF
 cQuery += " LEFT JOIN "+RetSqlName('SB4')+" SB4 ON SB4.B4_COD=LEFT(SC7.C7_PRODUTO,8) AND SB4.D_E_L_E_T_='' "+ CRLF

 cQuery += " LEFT JOIN "+RetSqlName('CTT')+" CTT ON  CTT.CTT_CUSTO=SC7.C7_CC  "+ CRLF
 cQuery += " LEFT JOIN "+RetSqlName('SBV')+" SBV ON SBV.BV_CHAVE=LEFT(SUBSTRING(SC7.C7_PRODUTO,9,16),3) AND SBV.BV_TABELA='COR' AND SBV.D_E_L_E_T_='' "+ CRLF 
 cQuery += " LEFT JOIN "+RetSqlName('SBM')+" SBM ON SBM.BM_GRUPO=SB4.B4_GRUPO AND SBM.D_E_L_E_T_=''  "+ CRLF
 cQuery += " LEFT JOIN "+RetSqlName('SC1')+" SC1 ON SC1.C1_NUM=SC7.C7_NUMSC " + CRLF
 cQuery += "  AND SC1.C1_ITEM=SC7.C7_ITEMSC " + CRLF
 cQuery += "  AND SC1.C1_FILIAL=SC7.C7_FILIAL " + CRLF
 cQuery += "  AND SC1.D_E_L_E_T_= ''  " + CRLF

   //Numero da Solicitacao
	If !Empty(mv_par02)
		cQuery += " AND SC1.C1_NUM = '"+Alltrim(mv_par02)+"' " + CRLF
	Endif
	
	//Emissao SC
	If !Empty(mv_par04)
		cQuery += " AND SC1.C1_EMISSAO BETWEEN '"+ DTOS(mv_par04) +"' AND '"+ DTOS(mv_par05) +"' " + CRLF
	Endif



  If MV_PAR06 == 1 // 1 = Aberto || 2 = Finalizado || 3 = Todos
		cQuery += " AND SC7.C7_QUJE < SC7.C7_QUANT "+ CRLF//QTDE_ENTREGUE < QTDE_PEDIDO" 
	Elseif  MV_PAR06 == 2 //Finalizado
		cQuery += " AND SC7.C7_QUJE = SC7.C7_QUANT "+ CRLF //AND QTDE_ENTREGUE = QTDE_PEDIDO"
	Endif

 cQuery += "LEFT JOIN SA2010 SA2 ON SA2.A2_COD=SC7.C7_FORNECE AND SA2.A2_LOJA = SC7.C7_LOJA"+ CRLF
 cQuery += "    WHERE  SC7.D_E_L_E_T_=''"    + CRLF
 cQuery += "      AND SC7.C7_NUMSC = '      '"+ CRLF
	
//Tipo
	If !Empty(mv_par01)
		cQuery += " AND SB4.B4_TIPO  IN "+Formatin(Upper(Alltrim(mv_par01)),";")+"  " + CRLF
	Endif

 	//Codigo Base do Produto (Referencia)
	If !Empty(mv_par03) 
		cQuery += " AND SB4.B4_COD = '"+Alltrim(mv_par03)+"' " + CRLF
	Endif

 //Emissao SC
	cQuery += " AND SC7.C7_DATPRF BETWEEN '"+ DTOS(mv_par08) +"' AND '"+ DTOS(mv_par09) +"' " + CRLF

 cQuery += "ORDER BY PEDIDO" + CRLF



	TCQUERY cQuery NEW ALIAS FLW

	While FLW->(!EOF())

		If oReport:Cancel()
			Exit
		Endif

		//If !(EmptyFLW->PEDIDO)

			oReport:IncMeter()

			oSection1:Cell("FILIAL_PEDIDO"):SetValue(FLW->FILIAL_PEDIDO)
			oSection1:Cell("FILIAL_PEDIDO"):SetAlign("LEFT")

			oSection1:Cell("FILIAL_ENTREGA"):SetValue(FLW->FILIAL_ENTREGA)
			oSection1:Cell("FILIAL_ENTREGA"):SetAlign("LEFT")
			
			oSection1:Cell("TIPO"):SetValue(FLW->TIPO)
			oSection1:Cell("TIPO"):SetAlign("LEFT")
			
			oSection1:Cell("SKU"):SetValue(FLW->SKU)
			oSection1:Cell("SKU"):SetAlign("LEFT")
			
			oSection1:Cell("PRODUTO"):SetValue(FLW->PRODUTO)
			oSection1:Cell("PRODUTO"):SetAlign("LEFT")
			
			oSection1:Cell("DESCRICAO"):SetValue(FLW->DESCRICAO)
			oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
			
			oSection1:Cell("COD_COR"):SetValue(FLW->COD_COR)
			oSection1:Cell("COD_COR"):SetAlign("LEFT")
			
			oSection1:Cell("DESC_COR"):SetValue(FLW->DESC_COR)
			oSection1:Cell("DESC_COR"):SetAlign("LEFT")
			
			oSection1:Cell("TAM"):SetValue(FLW->TAM)
			oSection1:Cell("TAM"):SetAlign("LEFT")
			
			oSection1:Cell("UNID_MEDIDA"):SetValue(FLW->UNID_MEDIDA)
			oSection1:Cell("UNID_MEDIDA"):SetAlign("LEFT")
			
			cSC1Comp:= Posicione('SY1',3,xFilial('SY1')+FLW->SC_COMPRADOR,"Y1_NOME")

			oSection1:Cell("SC_COMPRADOR"):SetValue(cSC1Comp)
			oSection1:Cell("SC_COMPRADOR"):SetAlign("LEFT")
			
			oSection1:Cell("SOLICITACAO"):SetValue(FLW->SOLICITACAO)
			oSection1:Cell("SOLICITACAO"):SetAlign("LEFT")
			
			oSection1:Cell("SOLICITANTE"):SetValue(FLW->SOLICITANTE)
			oSection1:Cell("SOLICITANTE"):SetAlign("LEFT")
			
			oSection1:Cell("DT_EMISSAO_SC"):SetValue(STOD(FLW->DT_EMISSAO_SC))
			oSection1:Cell("DT_EMISSAO_SC"):SetAlign("LEFT")
			
			oSection1:Cell("QTDE_SOLICITADA"):SetValue(FLW->QTDE_SOLICITADA)
			oSection1:Cell("QTDE_SOLICITADA"):SetAlign("LEFT")
			
			oSection1:Cell("TIPO_REQUISICAO"):SetValue(FLW->TIPO_REQUISICAO)
			oSection1:Cell("TIPO_REQUISICAO"):SetAlign("LEFT")	
			
			oSection1:Cell("COLECAO"):SetValue(FLW->COLECAO)
			oSection1:Cell("COLECAO"):SetAlign("LEFT")
			
			//+ Inserido por Robson, anteriormente estava trazendo C1_XSBCOLE e a SC1 encontra-se sem descricoes fieis
			cDscSubC:= Posicione('SB1',1,xFilial('SB1')+FLW->PRODUTO,"B1_YDESSCO")

			//oSection1:Cell("SUB_COLECAO"):SetValue(FLW->SUB_COLECAO)
			oSection1:Cell("SUB_COLECAO"):SetValue(cDscSubC)
			oSection1:Cell("SUB_COLECAO"):SetAlign("LEFT")
			
			oSection1:Cell("DT_NEC_DEMANDA"):SetValue(STOD(FLW->DT_NEC_DEMANDA))
			oSection1:Cell("DT_NEC_DEMANDA"):SetAlign("LEFT")
			
			oSection1:Cell("QUANT_MOSTRUARIO"):SetValue(FLW->QUANT_MOSTRUARIO)
			oSection1:Cell("QUANT_MOSTRUARIO"):SetAlign("LEFT")

			oSection1:Cell("DT_NEC_MOSTRUARIO"):SetValue(STOD(FLW->DT_NEC_MOSTRUARIO))
			oSection1:Cell("DT_NEC_MOSTRUARIO"):SetAlign("LEFT")

			oSection1:Cell("PEDIDO"):SetValue(FLW->PEDIDO)
			oSection1:Cell("PEDIDO"):SetAlign("LEFT")

			cSC7Comp:= Posicione('SY1',3,xFilial('SY1')+FLW->PED_COMPRADOR,"Y1_NOME") 

			oSection1:Cell("PED_COMPRADOR"):SetValue(cSC7Comp)
			oSection1:Cell("PED_COMPRADOR"):SetAlign("LEFT")
			
			oSection1:Cell("STATUS_APROVACAO"):SetValue(FLW->STATUS_APROVACAO)
			oSection1:Cell("STATUS_APROVACAO"):SetAlign("LEFT")
			
			oSection1:Cell("COD_FORNEC"):SetValue(FLW->CODFOR)
			oSection1:Cell("COD_FORNEC"):SetAlign("LEFT")

			oSection1:Cell("FORNECEDOR"):SetValue(FLW->FORNECEDOR)
			oSection1:Cell("FORNECEDOR"):SetAlign("LEFT")
			
			oSection1:Cell("CCUSTO"):SetValue(FLW->CCUSTO)
			oSection1:Cell("CCUSTO"):SetAlign("LEFT")
			
			oSection1:Cell("DT_EMISSAO_PED"):SetValue(STOD(FLW->DT_EMISSAO_PED))
			oSection1:Cell("DT_EMISSAO_PED"):SetAlign("LEFT")
			
			oSection1:Cell("VLR_TOTAL_ITEM"):SetValue(FLW->VLR_TOTAL_ITEM)
			oSection1:Cell("VLR_TOTAL_ITEM"):SetAlign("LEFT")
			
			oSection1:Cell("QTDE_PEDIDO"):SetValue(FLW->QTDE_PEDIDO)
			oSection1:Cell("QTDE_PEDIDO"):SetAlign("LEFT")
			
			oSection1:Cell("DATA_ENTREGA"):SetValue(STOD(FLW->DATA_ENTREGA))
			oSection1:Cell("DATA_ENTREGA"):SetAlign("LEFT")
			
			oSection1:Cell("QTDE_ENTREGUE"):SetValue(FLW->QTDE_ENTREGUE)
			oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
			
			If  FLW->QUITADO == "S"
				oSection1:Cell("SALDO"):SetValue("0")
			Else
				oSection1:Cell("SALDO"):SetValue(FLW->SALDO)
			Endif
			oSection1:Cell("SALDO"):SetAlign("LEFT")
			
			oSection1:Cell("CUSTO"):SetValue(FLW->CUSTO)
			oSection1:Cell("CUSTO"):SetAlign("LEFT")
			
			oSection1:Cell("QUITADO"):SetValue(FLW->QUITADO)
			oSection1:Cell("QUITADO"):SetAlign("LEFT")	

			//Verifica se existe SC7 para gravar o status do pedido (a = aberto, e = encerrado)  Solicitado pela Gilselly 22/01/2019
			If !Empty(Alltrim(FLW->PEDIDO))
				If FLW->SALDO > 0 .and. FLW->QUITADO <> "S"
					lEncerra:= .f.
					oSection1:Cell("ENCERRADO"):SetValue("")
					oSection1:Cell("ENCERRADO"):SetAlign("LEFT")	
				Else
					lEncerra:= .t.
					oSection1:Cell("ENCERRADO"):SetValue("E")
					oSection1:Cell("ENCERRADO"):SetAlign("LEFT")	
				Endif
			Endif

			oSection1:Cell("NOTA_1FISCAL"):SetValue(FLW->NOTA_1FISCAL)
			oSection1:Cell("NOTA_1FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_1FATURA"):SetValue(STOD(FLW->DATA_1FATURA))
			oSection1:Cell("DATA_1FATURA"):SetAlign("LEFT")

			nQtd1:= Iif(Empty(Alltrim(FLW->QTDE_1FATURA)),0,val(FLW->QTDE_1FATURA))
			
			oSection1:Cell("QTDE_1FATURA"):SetValue(nQtd1)
			oSection1:Cell("QTDE_1FATURA"):SetAlign("LEFT")

			oSection1:Cell("MODAL_1"):SetValue(FLW->MODAL_1)
			oSection1:Cell("MODAL_1"):SetAlign("LEFT")

			oSection1:Cell("X1OBSERVACAO"):SetValue(FLW->X1OBSERVACAO)
			oSection1:Cell("X1OBSERVACAO"):SetAlign("LEFT")

			oSection1:Cell("PREV_1RECEBIMENTO"):SetValue(STOD(FLW->PREV_1RECEBIMENTO))
			oSection1:Cell("PREV_1RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_1"):SetValue(FLW->SALDO_1)
			oSection1:Cell("SALDO_1"):SetAlign("LEFT")

			oSection1:Cell("DATA_1SALDO"):SetValue(STOD(FLW->DATA_1SALDO))
			oSection1:Cell("DATA_1SALDO"):SetAlign("LEFT")

			oSection1:Cell("NOTA_2FISCAL"):SetValue(FLW->NOTA_2FISCAL)
			oSection1:Cell("NOTA_2FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_2FATURA"):SetValue(STOD(FLW->DATA_2FATURA))
			oSection1:Cell("DATA_2FATURA"):SetAlign("LEFT")

			nQtd2:= Iif(Empty(Alltrim(FLW->QTDE_2FATURA)),0,val(FLW->QTDE_2FATURA))
			
			oSection1:Cell("QTDE_2FATURA"):SetValue(nQtd2)
			oSection1:Cell("QTDE_2FATURA"):SetAlign("LEFT")

			oSection1:Cell("MODAL_2"):SetValue(FLW->MODAL_2)
			oSection1:Cell("MODAL_2"):SetAlign("LEFT")

			//oSection1:Cell("X2OBSERVACAO"):SetValue(FLW->X2OBSERVACAO)
			//oSection1:Cell("X2OBSERVACAO"):SetAlign("LEFT")

			oSection1:Cell("PREV_2RECEBIMENTO"):SetValue(STOD(FLW->PREV_2RECEBIMENTO))
			oSection1:Cell("PREV_2RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_2"):SetValue(FLW->SALDO_2)
			oSection1:Cell("SALDO_2"):SetAlign("LEFT")

			oSection1:Cell("DATA_2SALDO"):SetValue(STOD(FLW->DATA_2SALDO))
			oSection1:Cell("DATA_2SALDO"):SetAlign("LEFT")

			oSection1:Cell("NOTA_3FISCAL"):SetValue(FLW->NOTA_3FISCAL)
			oSection1:Cell("NOTA_3FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_3FATURA"):SetValue(STOD(FLW->DATA_3FATURA))
			oSection1:Cell("DATA_3FATURA"):SetAlign("LEFT")

			nQtd3:= Iif(Empty(Alltrim(FLW->QTDE_3FATURA)),0,val(FLW->QTDE_3FATURA))
			
			oSection1:Cell("QTDE_3FATURA"):SetValue(nQtd3)
			oSection1:Cell("QTDE_3FATURA"):SetAlign("LEFT")

			oSection1:Cell("MODAL_3"):SetValue(FLW->MODAL_3)
			oSection1:Cell("MODAL_3"):SetAlign("LEFT")

			//oSection1:Cell("X3OBSERVACAO"):SetValue(FLW->X3OBSERVACAO)
			//oSection1:Cell("X3OBSERVACAO"):SetAlign("LEFT")

			oSection1:Cell("PREV_3RECEBIMENTO"):SetValue(STOD(FLW->PREV_3RECEBIMENTO))
			oSection1:Cell("PREV_3RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_3"):SetValue(FLW->SALDO_3)
			oSection1:Cell("SALDO_3"):SetAlign("LEFT")

			oSection1:Cell("DATA_3SALDO"):SetValue(STOD(FLW->DATA_3SALDO))
			oSection1:Cell("DATA_3SALDO"):SetAlign("LEFT")

			oSection1:Cell("NOTA_4FISCAL"):SetValue(FLW->NOTA_4FISCAL)
			oSection1:Cell("NOTA_4FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_4FATURA"):SetValue(STOD(FLW->DATA_4FATURA))
			oSection1:Cell("DATA_4FATURA"):SetAlign("LEFT")

			nQtd4:= Iif(Empty(Alltrim(FLW->QTDE_4FATURA)),0,Val(FLW->QTDE_4FATURA))
			
			oSection1:Cell("QTDE_4FATURA"):SetValue(nQtd4)
			oSection1:Cell("QTDE_4FATURA"):SetAlign("LEFT")

			oSection1:Cell("MODAL_4"):SetValue(FLW->MODAL_4)
			oSection1:Cell("MODAL_4"):SetAlign("LEFT")

			//oSection1:Cell("X4OBSERVACAO"):SetValue(FLW->X4OBSERVACAO)
			//oSection1:Cell("X4OBSERVACAO"):SetAlign("LEFT")

			oSection1:Cell("PREV_4RECEBIMENTO"):SetValue(STOD(FLW->PREV_4RECEBIMENTO))
			oSection1:Cell("PREV_4RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_4"):SetValue(FLW->SALDO_4)
			oSection1:Cell("SALDO_4"):SetAlign("LEFT")

			oSection1:Cell("DATA_4SALDO"):SetValue(STOD(FLW->DATA_4SALDO))
			oSection1:Cell("DATA_4SALDO"):SetAlign("LEFT")

			oSection1:Cell("NOTA_5FISCAL"):SetValue(FLW->NOTA_5FISCAL)
			oSection1:Cell("NOTA_5FISCAL"):SetAlign("LEFT")

			oSection1:Cell("DATA_5FATURA"):SetValue(STOD(FLW->DATA_5FATURA))
			oSection1:Cell("DATA_5FATURA"):SetAlign("LEFT")
			
			nQtd5:= Iif(Empty(Alltrim(FLW->QTDE_5FATURA)),0,Val(FLW->QTDE_5FATURA))
			
			oSection1:Cell("QTDE_5FATURA"):SetValue(nQtd5)
			oSection1:Cell("QTDE_5FATURA"):SetAlign("LEFT")

			oSection1:Cell("MODAL_5"):SetValue(FLW->MODAL_5)
			oSection1:Cell("MODAL_5"):SetAlign("LEFT")

			//oSection1:Cell("X5OBSERVACAO"):SetValue(FLW->X5OBSERVACAO)
			//oSection1:Cell("X5OBSERVACAO"):SetAlign("LEFT")

			oSection1:Cell("PREV_5RECEBIMENTO"):SetValue(STOD(FLW->PREV_5RECEBIMENTO))
			oSection1:Cell("PREV_5RECEBIMENTO"):SetAlign("LEFT")

			oSection1:Cell("SALDO_5"):SetValue(FLW->SALDO_5)
			oSection1:Cell("SALDO_5"):SetAlign("LEFT")

			oSection1:Cell("DATA_5SALDO"):SetValue(STOD(FLW->DATA_5SALDO))
			oSection1:Cell("DATA_5SALDO"):SetAlign("LEFT")

			nTotFat:= (nQtd1 + nQtd2 + nQtd3 + nQtd4 + nQtd5) 

			If nTotFat >= FLW->QTDE_ENTREGUE
				nTransito:= nTotFat-FLW->QTDE_ENTREGUE
			Else
				nTransito:= FLW->QTDE_ENTREGUE - nTotFat
			Endif

			//Adicionado a pedido da Magda (Pedidos encerrados devem ser impressos com '0' Zero)
			If !lEncerra
				oSection1:Cell("TRANSITO"):SetValue(nTransito)
			Else
				oSection1:Cell("TRANSITO"):SetValue(0)
			Endif
			oSection1:Cell("TRANSITO"):SetAlign("LEFT")

			nSldFim:= FLW->QTDE_PEDIDO - FLW->QTDE_ENTREGUE - nTransito

			oSection1:Cell("SALDO_FINAL"):SetValue(nSldFim)
			oSection1:Cell("SALDO_FINAL"):SetAlign("LEFT")

			oSection1:PrintLine()

		FLW->(DBSKIP()) 
	enddo
	FLW->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 10 de Novembro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Tipo de Produto?"            		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Solicitacao?"		    			,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Referencia ?"	                	,""		,""		,"mv_ch3","C",15,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Data da Solicitacao de?"	  	  	,""		,""		,"mv_ch4","D",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data da Solicitacao ate?"	    	,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "06","Situacao"	   						,""		,""		,"mv_ch6","N",01,0,1,"C",""	,""	,"","","mv_par06","Aberto","","","","Finalizado","","","","Todos","","","","","","","")
Return
