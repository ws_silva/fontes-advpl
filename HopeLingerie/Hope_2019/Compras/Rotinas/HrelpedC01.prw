#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} RF013 
Relat�rio de Fases
@author Weskley Silva
@since 06/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HRELPEDC01()

	Private oReport
	Private cPergCont	:= 'HRELPEDC01' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf
	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'xREL', 'RELA��O PEDIDOS DE COMPRAS LIBERADOS', cPergCont, {|oReport| ReportPrint( oReport ), 'PEDIDOS DE COMPRAS LIBERADO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'PEDIDO DE COMPRAS LIBERADOS', { 'xREL', 'SC7','SB1','SCR','SD1','CTT'})

			          
	TRCell():New( oSection1, 'EMISSAO'         ,'xREL', 		'EMISSAO'      ,   "@D"                  ,08)
	TRCell():New( oSection1, 'PEDIDO'          ,'xREL', 		'PEDIDO'       ,   "@!"                  ,15)
	TRCell():New( oSection1, 'ITEM'            ,'xREL', 		'ITEM'         ,   "@!"                  ,04)
   	TRCell():New( oSection1, 'PRODUTO'		   ,'xREL', 		'PRODUTO'      ,   "@!"                  ,15)
	TRCell():New( oSection1, 'DESCRICAO'       ,'xREL', 		'DESCRICAO'    ,   "@!"                  ,25)
	TRCell():New( oSection1, 'FORNECEDOR'      ,'xREL', 		'FORNECEDOR'   ,   "@!"                  ,25)
	TRCell():New( oSection1, 'QUANTIDADE'      ,'xREL', 		'QUANTIDADE'   ,   "@E 99,999,999.99"    ,14)
	TRCell():New( oSection1, 'VL UNITARIO'     ,'xREL', 		'V_UNITARIO'   ,   "@E 99,999,999.99"    ,14)
	TRCell():New( oSection1, 'TOTAL PED'       ,'xREL', 		'TOTAL_PED'    ,   "@E 99,999,999.99"    ,14)
	TRCell():New( oSection1, 'TOTAL NF '       ,'xREL', 		'TOTAL_NF'     ,   "@E 99,999,999.99"    ,14)
	TRCell():New( oSection1, 'NOTA FISCAL'     ,'xREL', 		'NOTAFISCAL'   ,   "@!"                  ,09)
	TRCell():New( oSection1, 'SERIE'           ,'xREL', 		'SERIE'        ,   "@!"                  ,03)
	TRCell():New( oSection1, 'ITEM NF'         ,'xREL', 		'ITEM_NF'      ,   "@!"                  ,04)
	TRCell():New( oSection1, 'USUARIO'         ,'xREL', 		'USUARIO'      ,   "@!"                  ,06)
	TRCell():New( oSection1, 'NOME USUARIO'    ,'xREL', 		'NO_USUARIO'   ,   "@!"                  ,20)
	TRCell():New( oSection1, 'APROVADOR'       ,'xREL', 		'APROVADOR'    ,   "@!"                  ,06)
	TRCell():New( oSection1, 'NOME APROVADOR'  ,'xREL', 		'NAPROVADOR'   ,   "@!"                  ,20)
	TRCell():New( oSection1, 'CENTRO CUSTO'    ,'xREL', 		'CENT_CUSTO'   ,   "@!"                  ,10)
	TRCell():New( oSection1, 'CLASSE VALOR'    ,'xREL', 		'CLAS_VALOR'   ,   "@!"                  ,10)
	TRCell():New( oSection1, 'DESCR C CUSTO'   ,'xREL',      'DESCCCUSTO'      ,   "@!"                  ,20)
	TRCell():New( oSection1, 'DESCR CLAS VALOR','xREL',      'DESCCLASVL'      ,   "@!"                  ,20)
		

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 06 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""
    LocaL lApaga := .f.
	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	                    
    // arquivo de retorno do romaneio
	aStru := {}

	aAdd( aStru , { "EMISSAO"            , "D" , 08 , 00 } )  
    aAdd( aStru , { "PEDIDO"             , "C" , 15 , 00 } )
    aAdd( aStru , { "ITEM"               , "C" , 04 , 00 } )
    aAdd( aStru , { "PRODUTO"            , "C" , 15 , 00 } )
    aAdd( aStru , { "DESCRICAO"          , "C" , 25 , 00 } )
    aAdd( aStru , { "FORNECEDOR"         , "C" , 25 , 00 } )
    aAdd( aStru , { "QUANTIDADE"         , "N" , 14 , 02 } )
    aAdd( aStru , { "V_UNITARIO"         , "N" , 14 , 02 } )
    aAdd( aStru , { "TOTAL_PED"          , "N" , 14 , 02 } )
    aAdd( aStru , { "TOTAL_NF "          , "N" , 14 , 02 } )
    aAdd( aStru , { "NOTAFISCAL"         , "C" , 09 , 00 } )
    aAdd( aStru , { "SERIE"              , "C" , 03 , 00 } )
    aAdd( aStru , { "ITEM_NF"            , "C" , 04 , 00 } )
    aAdd( aStru , { "USUARIO"            , "C" , 06 , 00 } )
    aAdd( aStru , { "NO_USUARIO"         , "C" , 20 , 00 } )
    aAdd( aStru , { "APROVADOR"          , "C" , 06 , 00 } )
    aAdd( aStru , { "NAPROVADOR"         , "C" , 20 , 00 } )
    aAdd( aStru , { "CENT_CUSTO"         , "C" , 10 , 00 } )
    aAdd( aStru , { "CLAS_VALOR"         , "C" , 10 , 00 } )
    aAdd( aStru , { "DESCCCUSTO"         , "C" , 20 , 00 } )
    aAdd( aStru , { "DESCCLASVL"         , "C" , 20 , 00 } )

	IF Select("xREL") > 0
		xREL->(dbCloseArea())
	Endif             

	cArqa := CriaTrab(aStru,.F.)

	If File(cArqa+".dbf")
		Inkey(4)
		cArqa := CriaTrab(aStru,.F.)
	EndIf
	
	dbCreate (cArqa,aStru)
	dbUseArea(.T.,,cArqa,"xREL",.T.)
	cInda := CriaTrab(NIL,.F.)
	
	IndRegua("xREL",cInda," EMISSAO+ITEM+PEDIDO",,,"Selecionando Registros...")

	IF Select("XTRB") > 0
		XTRB->(dbCloseArea())
	Endif             

    cQuery := "SELECT CR_NIVEL,C7_FILIAL, C7_EMISSAO, C7_NUM,C7_ITEM,C7_PRODUTO,C7_DESCRI,A2_NREDUZ AS FORNECEDOR, C7_QUANT,C7_PRECO,C7_TOTAL,"
    cQuery += "ISNULL((SELECT D1_TOTAL FROM "+RetSqlName("SD1")+" D1 WHERE D1.D_E_L_E_T_='' AND   C7.C7_FILIAL = D1.D1_FILIAL AND  " 
    cQuery += "C7.C7_ITEM = D1.D1_ITEMPC AND C7.C7_NUM = D1.D1_PEDIDO) ,0) AS TOTALNF , "
    cQuery += "ISNULL((SELECT D1_DOC FROM "+RetSqlName("SD1")+" D1 WHERE D1.D_E_L_E_T_='' AND   C7.C7_FILIAL = D1.D1_FILIAL AND   "
    cQuery += "C7.C7_ITEM = D1.D1_ITEMPC AND C7.C7_NUM = D1.D1_PEDIDO),'') AS NOTAFISCAL, "
    cQuery += "ISNULL((SELECT D1_SERIE  FROM "+RetSqlName("SD1")+" D1 (NOLOCK) WHERE D1.D_E_L_E_T_='' AND   C7.C7_FILIAL = D1.D1_FILIAL AND "
    cQuery += "C7.C7_ITEM = D1.D1_ITEMPC AND C7.C7_NUM = D1.D1_PEDIDO),'') AS SERIE, " 
    cQuery += "ISNULL((SELECT D1_ITEMPC  FROM "+RetSqlName("SD1")+" D1 (NOLOCK) WHERE D1.D_E_L_E_T_='' AND   C7.C7_FILIAL = D1.D1_FILIAL AND "
    cQuery += "C7.C7_ITEM = D1.D1_ITEMPC AND C7.C7_NUM = D1.D1_PEDIDO),'') AS ITEM , " 
    cQuery += "MIN(C7_USER) AS USUARIO, Y1_NOME, MIN(CR_LIBAPRO) APROVADOR, AL_NOME, C7_CC,C7_CLVL, "
    cQuery += "ISNULL((SELECT CTT_DESC01 FROM "+RetSqlName("CTT")+" TT (NOLOCK) WHERE TT.D_E_L_E_T_ <> '*' AND C7_CC = TT.CTT_CUSTO),'') AS DESC_C_CUSTO, "
    cQuery += "ISNULL((SELECT CTH_DESC01 FROM "+RetSqlName("CTH")+" TH (NOLOCK) WHERE TH.D_E_L_E_T_ <> '*' AND C7_CLVL = TH.CTH_CLVL),'') AS  DESC_CLAS_VALOR "
    cQuery += "FROM "+RetSqlName("SC7")+" C7 INNER JOIN  "
    cQuery += " "+RetSqlName("SA2")+" A2 ON C7.C7_FORNECE = A2.A2_COD AND C7.C7_LOJA = A2.A2_LOJA INNER JOIN "
    cQuery += " "+RetSqlName("SCR")+" CR ON CR_LIBAPRO <> ' ' AND CR.CR_NUM = C7.C7_NUM AND C7.C7_EMISSAO = CR_EMISSAO INNER JOIN  "  
    cQuery += " "+RetSqlName("SY1")+" Y1 ON Y1.Y1_USER = C7.C7_USER AND Y1.Y1_GRUPCOM = C7.C7_GRUPCOM INNER JOIN "
    cQuery += " "+RetSqlName("SAL")+" AL ON AL.AL_APROV = CR.CR_APROV AND AL.AL_COD = CR.CR_GRUPO AND CR.CR_NUM = C7.C7_NUM "
    cQuery += "AND C7_EMISSAO BETWEEN '"+DTOS(MV_PAR01)+"' AND '"+DTOS(MV_PAR02)+"' AND C7_FILIAL = '"+MV_PAR03+"'  AND C7_CONAPRO = 'L' "
    cQuery += "GROUP BY CR_NIVEL,C7_FILIAL,C7_EMISSAO, C7_ITEM,C7_PRODUTO, C7_DESCRI,A2_NREDUZ,C7_NUM,C7_QUANT,C7_PRECO,C7_TOTAL,C7_USER,Y1_NOME, CR_LIBAPRO,AL_NOME,C7_CC,C7_CLVL "
    cQuery += "ORDER BY C7_EMISSAO, C7_ITEM, C7_NUM ,CR_NIVEL desc"

    TCQUERY cQuery NEW ALIAS xTRB 
    XTRB->(DBGOTOP())
    While XTRB->(!EOF())        
        if !lApaga 
           cDatam:= SUBSTR(C7_EMISSAO,7,2)+'/'+SUBSTR(C7_EMISSAO,5,2)+'/'+SUBSTR(C7_EMISSAO,3,2) 
           dbselectarea("xREL")
           xREL->(Reclock("xREL",.t.))
	       xREL->EMISSAO     := ctod(cDatam)  //XTRB->C7_EMISSAO
	       xREL->PEDIDO      := XTRB->C7_NUM
	       xREL->ITEM        := XTRB->C7_ITEM
	       xREL->PRODUTO     := XTRB->C7_PRODUTO
	       xREL->DESCRICAO   := XTRB->C7_DESCRI
	       xREL->FORNECEDOR  := XTRB->FORNECEDOR
	       xREL->QUANTIDADE  := XTRB->C7_QUANT
	       xREL->V_UNITARIO  := XTRB->C7_PRECO
	       xREL->TOTAL_PED   := XTRB->C7_TOTAL
	       xREL->TOTAL_NF    := XTRB->TOTALNF
	       xREL->NOTAFISCAL  := XTRB->NOTAFISCAL
	       xREL->SERIE       := XTRB->SERIE
	       xREL->ITEM_NF     := XTRB->ITEM
	       xREL->USUARIO     := XTRB->USUARIO
	       xREL->NO_USUARIO  := XTRB->Y1_NOME
	       xREL->APROVADOR   := XTRB->APROVADOR
	       xREL->NAPROVADOR  := XTRB->AL_NOME
	       xREL->CENT_CUSTO  := XTRB->C7_CC
	       xREL->CLAS_VALOR  := XTRB->C7_CLVL
	       xREL->DESCCCUSTO  := XTRB->DESC_C_CUSTO
	       xREL->DESCCLASVL  := XTRB->DESC_CLAS_VALOR
		   xREL->(MsUnlock())
		Endif       
        dbselectarea("XTRB")
        IF ALLTRIM(XTRB->CR_NIVEL) == '02'
           lApaga := .t.
        else
           lApaga := .f.
        Endif
        XTRB->(dbSKIP())
    Enddo 

    xREL->(DBGOTOP())
	While xREL->(!EOF())      
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

       
		oSection1:Cell("EMISSAO"):SetValue(xREL->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("PEDIDO"):SetValue(xREL->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("ITEM"):SetValue(xREL->ITEM)
		oSection1:Cell("ITEM"):SetAlign("LEFT")	
		
		oSection1:Cell("PRODUTO"):SetValue(xREL->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")

        oSection1:Cell("DESCRICAO"):SetValue(xREL->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("FORNECEDOR"):SetValue(xREL->FORNECEDOR)
		oSection1:Cell("FORNECEDOR"):SetAlign("LEFT")
		
		oSection1:Cell("QUANTIDADE"):SetValue(xREL->QUANTIDADE)
		oSection1:Cell("QUANTIDADE"):SetAlign("LEFT")
		
		oSection1:Cell("VL UNITARIO"):SetValue(xREL->V_UNITARIO)
		oSection1:Cell("VL UNITARIO"):SetAlign("LEFT")
		
		oSection1:Cell("TOTAL PED"):SetValue(xREL->TOTAL_PED)
		oSection1:Cell("TOTAL PED"):SetAlign("LEFT")
		
		oSection1:Cell("TOTAL NF "):SetValue(xREL->TOTAL_NF)
		oSection1:Cell("TOTAL NF "):SetAlign("LEFT")

		oSection1:Cell("NOTA FISCAL"):SetValue(xREL->NOTAFISCAL)
		oSection1:Cell("NOTA FISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("SERIE"):SetValue(xREL->SERIE)
		oSection1:Cell("SERIE"):SetAlign("LEFT")

		oSection1:Cell("ITEM NF"):SetValue(xREL->ITEM_NF)
		oSection1:Cell("ITEM NF"):SetAlign("LEFT")
		
		oSection1:Cell("USUARIO"):SetValue(xREL->USUARIO)
		oSection1:Cell("USUARIO"):SetAlign("LEFT")
		
		oSection1:Cell("NOME USUARIO"):SetValue(xREL->NO_USUARIO)
		oSection1:Cell("NOME USUARIO"):SetAlign("LEFT")

		oSection1:Cell("APROVADOR"):SetValue(xREL->APROVADOR)
		oSection1:Cell("APROVADOR"):SetAlign("LEFT")

		oSection1:Cell("NOME APROVADOR"):SetValue(xREL->NAPROVADOR)
		oSection1:Cell("NOME APROVADOR"):SetAlign("LEFT")

		oSection1:Cell("CENTRO CUSTO"):SetValue(xREL->CENT_CUSTO)
		oSection1:Cell("CENTRO CUSTO"):SetAlign("LEFT")

		oSection1:Cell("CLASSE VALOR"):SetValue(xREL->CLAS_VALOR)
		oSection1:Cell("CLASSE VALOR"):SetAlign("LEFT")

		oSection1:Cell("DESCR C CUSTO"):SetValue(xREL->DESCCCUSTO)
		oSection1:Cell("DESCR C CUSTO"):SetAlign("LEFT")

		oSection1:Cell("DESCR CLAS VALOR"):SetValue(xREL->DESCCLASVL)
		oSection1:Cell("DESCR CLAS VALOR"):SetAlign("LEFT")

		oSection1:PrintLine()
		
		xREL->(DBSKIP()) 
	enddo
	xREL->(DBCLOSEAREA())
Return( Nil )


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � AjustaSX1    �Autor �  Mauro Nagata        �Data� 21/04/17 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Ajusta perguntas do SX1                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)
          
Local cAlias := Alias(), aPerg := {}
Local nI,nJ

//             01		,02	,03								,04	,05	,06		,07	,08	,09,10,11	,12,13			,14,15,16,17.18,19 ,20,21,22,23,24,25,26 ,27,28,29,30,31,32,33,34,35 ,36,37,38                                                        
aAdd(aPerg, {cPerg	,"01"	,"Data Emiss�o De  ?" ,"?"	,"?"	,"mv_ch1","D"	, 08 	, 0, 0,"G"	,"","mv_par01"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aPerg, {cPerg	,"02"	,"Data Emiss�o ate ?" ,"?"	,"?"	,"mv_ch2","D"	, 08 	, 0, 0,"G"	,"","mv_par02"	,"","","","","","","","","","","","","","","","","","","","","","","","",""})
aAdd(aPerg, {cPerg	,"03"	,"Filial           ?" ,"?"	,"?"	,"mv_ch3","C"	, 04 	, 0, 0,"G"	,"","mv_par03"	,"","","","","","","","","","","","","","","","","","","","","","","","","SM0"})

nPerg := Len(aPerg)

DbSelectArea("SX1")
DbSetOrder(1)        

For nI := 1 To Len(aPerg)                                                
	If !DbSeek(cPerg+aPerg[nI,2])		
		RecLock("SX1",.T.)
		For nJ := 1 to FCount()
			If nJ <= Len(aPerg[nI])
				FieldPut(nJ,aPerg[nI,nJ])
			Endif
		Next
		MsUnlock()
	Endif
Next

DbSelectArea(cAlias)

Return                  

