#include "protheus.ch"
#include "topconn.ch"
/******************************************************************************
* Programa: MT100AGR          09/2019            Totalit Solutions            *
* Funcionalidade: Programa para calcular o estorno das comissoes a partir das *
*                 notas de devolucoes de vendas                               *
******************************************************************************/
User Function MT100AGR()

// Calcular a comissao negativa se for nota de devolucao
If cTipo = "D" .and. INCLUI
	U_RFATM01("D")
Endif

Return Nil