#INCLUDE "rwmake.ch"
#INCLUDE 'protheus.ch'

/*/
�����������������������������������������������������������������������������
> �����������������������������������������������������������������������������
> �������������������������������������������������������������������������ͻ��
> ���Programa  �MT120LOK  � Autor � Cristiano Lima     � Data �  09/02/17   ���
> �������������������������������������������������������������������������͹��
> ���Descricao � Ponto de entrada para validar a digita��o do centro de     ���
> ���          � custos para produtos nao comercializaveis.                 ���
> �������������������������������������������������������������������������͹��
> ���Uso       � HOPE - Itens Pedido de compras                             ���
> �������������������������������������������������������������������������͹��
> ���Alteracoes� Data  Analista Descricao                                   ���
- �������������������������������������������������������������������������͹��
- ���	  				      		                  						         ���
- �������������������������������������������������������������������������ͼ��
- �����������������������������������������������������������������������������
- �����������������������������������������������������������������������������
/*/
User Function MT120LOK()

Local _lRet       := .T.
Local _nPosCC     := aScan(aHeader,{|x| Upper(Trim(x[2])) == "C7_CC"})  // Pega a posicao do campo nos itens
Local _nPosRat	   := aScan(aHeader,{|x| Upper(Trim(x[2])) == 'C7_RATEIO'})
Local i           := 1
Local nC7_PRODUTO := GdFieldPos("C7_PRODUTO", aHeader) 
Local nLinhaAtual := n 
Local nLoop       := 0 
Local nCount      := 0
Local nTotRat     := 0 
Local nCols       := 0

//cTipo := POSICIONE("SB1",1,xFilial("SB1")+Alltrim((aCols[n][2])),"B1_TIPO")
//Retirada a validação de tipo de produto, conforme solicitação do Adelson, todos os pedidos agora exigem centro de custo 
//Alteração realizada por Ferrari
//If cTipo $ GETMV("HP_GRUPEST")
//   _lRet := .T.

//Else   
For nCols:= 1 to Len(aCols)
	If Empty(Alltrim(aCols[n][_nPosCc]))
		If Alltrim(aCols[n][_nPosRat]) == '2'
			_lRet := .F.

		EndIf

   EndIf	

Next

If _lRet == .F.
   MsgStop('Preencha o Centro de Custo do Item caso n�o tenha Rateio!!')

EndIf

For nLoop := 1 to Len( aCols ) 
   // Comentado por Robson 09/01/2019 para incluir a regra de preenchimento do centro de custo acima
   // N�o deve validar a linha atual. 
  /* If Alltrim(aCols[i][_nPosRat]) == '1' // Caso n�o tenha rateio
      IF LEN(ACPISCH) == 0 .AND. LEN(ACPISCH) < Len( aCols )
         MSGALERT('Por gentileza, cadastre o rateio deste PEDIDO DE COMPRAS!')   
         _lRet := .F.
      Endif
   Endif
   */

   If nLoop == nLinhaAtual 
      Loop 
   Endif 
   
   If (aCols[n,nC7_PRODUTO] == aCols[nLoop,nC7_PRODUTO])  
      MSGALERT("Produto duplicado, oper��o n�o permitida!!!" ) 
      _lRet := .F. 
   Endif

Next 

Return _lRet