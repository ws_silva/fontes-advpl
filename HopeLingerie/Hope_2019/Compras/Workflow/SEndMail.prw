#include "Protheus.ch"
#Include "Ap5Mail.Ch"
//
/*����������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o    �SEndMail  � Autor �      � Data �24/05/2014 ���
��������������������������������������������������������������������������Ĵ��
���          �Envio de E-mail                                              ���
��������������������������������������������������������������������������Ĵ��
���Parametros�ExpC1: Servido de E-mail                                     ���
���          �ExpC2: Conta de E-mail                                       ���
���          �ExpC3: Senha Conta E-mail                                    ���
���          �ExpC4: String Contas E-mail destino                          ���
���          �ExpC5: Assunto                                               ���
���          �ExpC6: Corpo de Texto                                        ���
���          �ExpC7: Arquivos Anexos                                       ���
��������������������������������������������������������������������������Ĵ��
���Retorno   �Logico - .T. - Operacao realizada                            ���
���          �       - .F. - Operacao NAO realizada                        ���
��������������������������������������������������������������������������Ĵ��
���Uso       � Minera��o Caraiba                                           ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
User Function SEndMail(cMailDest,cAssunto,cTexto,cAnexos)               '
                                


//����������������������Ŀ
//�Define Valores padroes�
//������������������������
Private cMailServer  := GetMv("MV_RElSERV")
Private cMailUser    := GetMv("MV_RELACNT")
Private cMailSenha   := GetMv("MV_RELPSW" )
Private cMailConta   := GetMv("MV_RELFROM" )
Private cMailDestino := cMailDest
Private _lRet := .t.
conout("1"+cMailServer)
conout("2"+cMailUser)
conout("3"+cMailSenha)
conout("4"+cMailConta)

MsgRun("Enviando Informa��es...",, {||ProcEMail(cMailDestino,cAssunto,cTexto,cAnexos)} )

Return(_lRet)

/*����������������������������������������������������������������������������
������������������������������������������������������������������������������
��������������������������������������������������������������������������Ŀ��
���Fun��o    � ProcEMail� Autor � Francisco Valdeni     � Data �10/04/2013 ���
��������������������������������������������������������������������������Ĵ��
���          � Processa o envio do e-mail                                  ���
��������������������������������������������������������������������������Ĵ��
���Uso       � Especifico Pleimec                                          ���
���������������������������������������������������������������������������ٱ�
������������������������������������������������������������������������������
�����������������������������������������������������������������������������*/
Static Function ProcEMail(cMailDestino,cAssunto,cTexto,cAnexos)
cAssunto 		:= If( ValType(cAssunto) 		!= "U" , cAssunto    ,  "" )
cTexto 			:= If( ValType(cTexto)   		!= "U" , cTexto      ,  "" )
cAnexos			:= If( ValType(cAnexos)  		!= "U" , cAnexos     ,  "" )
lConexao		:= .F.
Lenvio   		:= .T.
lDesconexao		:= .F.
cErro_Conexao 	:= ""
cErro_Envio		:= ""
cErro_Desconexao:= ""


/// Inicializa regua processamento
ProcRegua(3)

//���������������Ŀ
//�AValia conteudo�
//�����������������
If Empty( cMailDestino )
	MsgStop( "Conta(s) de E-Mail Destino - NAO INFORMADA.")
	conout("Conta(s) de E-Mail Destino - NAO INFORMADA.")
	_lRet:=.f.
	Return()
EndIf

If Empty(Alltrim( cAssunto ))
	MsgStop( "Assunto do E-Mail - NAO INFORMADO.")
	conout("Assunto do E-Mail - NAO INFORMADO.")
	_lRet:=.f.
	Return()
EndIf

If Empty(Alltrim( cTexto ))
	MsgStop( "Texto do E-Mail - NAO INFORMADO.")
	conout("Texto do E-Mail - NAO INFORMADO.")
	Return(.F.)
EndIf

//���������������������������������������������������Ŀ
//�EXECUTA conex�o ao servidor mencionado no parametro�
//�����������������������������������������������������
Connect Smtp Server cMailServer ACCOUNT cMailUser PASSWORD cMailSenha RESULT lConexao

If !lConexao
	GET MAIL ERROR cErro_Conexao
	MsgStop("Nao foi possivel estabelecer a CONEXAO com o servidor - " + cErro_Conexao )
	conout("Nao foi possivel estabelecer a CONEXAO com o servidor - " + cErro_Conexao )
	Return( .F. )
EndIf    


lConectou := MAILAUTH(cMailUser,cMailSenha)  

If !lConectou
	Conout("Erro ao conectar ao servidor de E-Mail - " + cMailServer)
Endif 


_lRet:=.t.
IncProc("Enviando E-Mail !!!")
conout("Enviando E-Mail !!!")

//�������������������������Ŀ
//�EXECUTA envio da mensagem�
//���������������������������
IF lConectou
   If !Empty( cANEXOS )
     	SEnd Mail From cMAILCONTA to cMAILDESTINO SubJect cASSUNTO BODY cTEXTO FORMAT TEXT ATTACHMENT cANEXOS RESULT Lenvio
   Else
    	SEnd Mail From cMAILCONTA to cMAILDESTINO SubJect cASSUNTO BODY cTEXTO FORMAT TEXT RESULT Lenvio
  EndIf


  If !Lenvio
     Get Mail Error cErro_Envio
     MsgStop("Nao foi possivel ENVIAR a mensagem - " + cErro_Envio )
     conout("Nao foi possivel ENVIAR a mensagem - " + cErro_Envio ) 
     _lRet:=.f.
	 Return()
  EndIf

  IncProc("Desconectando do servidor de E-Mail !!!")
  conout("Desconectando do servidor de E-Mail !!!")

  //�����������������������������������Ŀ
  //�EXECUTA disconexao ao servidor SMTP�
  //�������������������������������������
  DisConnect Smtp Server Result lDesconexao

  If !lDesconexao
     Get Mail Error cErro_Desconexao
     MsgStop("Nao foi possivel DESCONECTAR do servidor - " + cErro_Desconexao )
     conout("Nao foi possivel DESCONECTAR do servidor - " + cErro_Desconexao )
     _lRet:=.f.
     //Return()
  EndIf
else
  MsgStop("Nao foi possivel ENVIAR a mensagem  ") 
  conout("Nao foi possivel ENVIAR a mensagem  ")
  _lRet:=.f.
end
  
Return _lRet