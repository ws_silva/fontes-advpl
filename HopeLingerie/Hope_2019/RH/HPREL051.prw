//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de Funcion�rios.
Rotina para gerar rela��o de todos funcion�rios;

@author Ronaldo Pereira
@since 24 de Abril de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL051()

	Local aRet := {}
	Local aPerg := {}
		
	aAdd(aPerg,{1,"Filial de"	,Space(04),""	,""		,""		,""		,0	,.F.}) 
	aAdd(aPerg,{1,"Filial at�" 	,Space(04),""	,""		,""		,""		,0	,.F.}) 
	aAdd(aPerg,{1,"Setor: " 	,Space(09),""   ,""		,"CTT"	,""		,0	,.F.}) 
	aAdd(aPerg,{1,"Sexo: "		,Space(01),""	,""		,""		,""		,0	,.F.}) 
	aAdd(aPerg,{1,"Matr�cula: "	,Space(06),""	,""		,""		,""		,0	,.F.})
	aAdd(aPerg,{2,"Aut�nomos: " ,"N�o" ,{" ","N�o","Sim","Todos"},	40,'.T.',.F.}) 
	aAdd(aPerg,{2,"Demitidos: " ,"N�o" ,{" ","N�o","Sim","Todos"},	40,'.T.',.F.})
	aAdd(aPerg,{1,"Data Demiss�o de"   ,dDataBase,"",'.T.'	,""	 ,'.T.'	,50	,.F.})
	aAdd(aPerg,{1,"Data demiss�o At�"  ,dDataBase,"",'.T.'	,""	 ,'.T.'	,50	,.F.})  

	
	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| Geradados(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Funcion�rios '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Dados"
    Local cTable1    	:= "Funcion�rios - Informa��es Gerais" 

        
    //Dados tabela 1          
    cQry := "SELECT RA.RA_FILIAL AS FILIAL, "
    cQry += "       CONVERT(CHAR,CAST(RA.RA_ADMISSA AS SMALLDATETIME), 103) AS DT_ADMISSAO, "   
    cQry += "		CASE WHEN RA.RA_DEMISSA <> '' THEN CONVERT(CHAR,CAST(RA.RA_DEMISSA AS SMALLDATETIME), 103) ELSE '' END DT_DEMISSAO, "          
    cQry += "       RA.RA_MAT AS MATRICULA, "
    cQry += "       RA.RA_NOMECMP AS NOME, "
    cQry += "	   RA.RA_SEXO AS SEXO, "
    cQry += "	   X5.X5_DESCRI AS GRAU_INSTRUCAO,
    cQry += "	   CC.CTT_DESC01 AS CENTRO_CUSTO, "
    cQry += "	   RJ.RJ_CODCBO AS CBO, "
    cQry += "	   RJ.RJ_DESC AS FUNCAO, "
    cQry += "	   RA.RA_SITFOLH AS SITUACAO, "
    cQry += "	   CASE LEFT(RA_MAT,2) WHEN '99' THEN 'SIM' ELSE 'N�O' END AS AUTONOMO, "
    cQry += "	   RA.RA_CIC AS CPF, "
    cQry += "	   RA.RA_RG AS RG, "
    cQry += "	   RA.RA_PIS AS PIS, "
    cQry += "	   CONVERT(CHAR, CAST(RA.RA_NASC AS SMALLDATETIME), 103) AS DT_NASC, "
    cQry += "	   MONTH(RA.RA_NASC) AS MES_NIVER, "
    cQry += " 	   RA.RA_MAE AS MAE, "
    cQry += "	   RA.RA_PAI AS PAI, "

    cQry += "	   CASE " 
    cQry += "		(SELECT RB.RB_GRAUPAR FROM "+RetSqlName('SRB')+" AS RB (NOLOCK) WHERE D_E_L_E_T_ = '' AND RB.RB_MAT = RA.RA_MAT AND RB.RB_GRAUPAR = 'F' "
    cQry += "			GROUP BY RB.RB_MAT,RB_GRAUPAR) WHEN 'F' THEN 'SIM' ELSE 'N�O' " 
    cQry += "	   END AS TEM_FILHOS, "

    cQry += "	   RA.RA_ENDEREC AS ENDERECO, "
    cQry += "	   RA.RA_NUMENDE AS NUMERO, "
    cQry += "	   RA.RA_BAIRRO AS BAIRRO, "
    cQry += "	   RA.RA_MUNICIP AS MUNICIPIO, "
    cQry += "	   RA.RA_CEP AS CEP, "
    cQry += "	   RA.RA_TELEFON AS TELEFONE, "
    cQry += "	   RA.RA_EMAIL AS EMAIL, "
    cQry += "	   ISNULL(RA.RA_AFASFGT + ' - ' + SUBSTRING(RCC.RCC_CONTEU,3,300),'') AS COD_AFAST_FGTS "        
    cQry += "FROM "+RetSqlName('SRA')+" AS RA WITH (NOLOCK) " 
    cQry += "INNER JOIN "+RetSqlName('CTT')+" AS CC WITH (NOLOCK) ON (CC.CTT_CUSTO = RA.RA_CC "
    cQry += "                                       AND CC.CTT_FILIAL = '01' "
    cQry += "                                       AND CC.D_E_L_E_T_ = '') "
    cQry += "INNER JOIN "+RetSqlName('SRJ')+" AS RJ WITH (NOLOCK) ON (RJ.RJ_FUNCAO = RA.RA_CODFUNC "
    cQry += "                                       AND RJ.RJ_FILIAL = '01' "
    cQry += "                                       AND RJ.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName('SX5')+" AS X5 WITH (NOLOCK) ON (X5_TABELA = '26'  " 
    cQry += "                                      AND X5_CHAVE = RA_GRINRAI " 
    cQry += "                                      AND X5.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN "+RetSqlName('RCC0')+" AS RCC WITH (NOLOCK) ON (LEFT(RCC.RCC_CONTEU,2) = RA.RA_AFASFGT  " 
    cQry += "                                      AND RCC.RCC_FILIAL = RIGHT(RA.RA_FILIAL,2) " 
    cQry += "								       AND RCC.RCC_CODIGO = 'S046' "  
    cQry += "								       AND RCC.D_E_L_E_T_ = '') "        
    cQry += "WHERE RA.D_E_L_E_T_ = '' " 

    If !Empty(aRet[1])
    	cQry += "    AND RA.RA_FILIAL BETWEEN '"+Alltrim(aRet[1])+"' AND '"+Alltrim(aRet[2])+"' "
    Endif	 
    If !Empty(aRet[3])
    	cQry += "	AND CC.CTT_CUSTO = '"+Alltrim(aRet[3])+"' "
    Endif	
    If !Empty(aRet[4])
    	cQry += "	AND RA.RA_SEXO = '"+Alltrim(aRet[4])+"' "
	Endif
	If !Empty(aRet[5])	          
    	cQry += "	AND RA.RA_MAT = '"+Alltrim(aRet[5])+"' "
    Endif

	If !Empty(aRet[6])	
		DO CASE
		  CASE Alltrim(aRet[6]) == "N�o" 
		    cQry += " AND RA.RA_MAT NOT LIKE '99%' "
		  CASE Alltrim(aRet[6])	== "Sim" 
		    cQry += " AND RA.RA_MAT LIKE '99%' "
		ENDCASE
	EndIf
	
	If !Empty(aRet[7])	
		DO CASE
		  CASE Alltrim(aRet[7]) == "N�o" 
		    cQry += " AND RA.RA_SITFOLH <> 'D'  "
		  CASE Alltrim(aRet[7])	== "Sim" 
		    cQry += " AND RA.RA_SITFOLH = 'D' "
		ENDCASE
	EndIf
	
	If !Empty(aRet[8])
		cQry += " AND RA.RA_DEMISSA BETWEEN '"+ DTOS(aRet[8]) +"' AND '"+ DTOS(aRet[9]) +"' "
	EndIf
	
    cQry += "ORDER BY RA.RA_ADMISSA "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_ADMISSAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_DEMISSAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MATRICULA",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NOME",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SEXO",1)  
        oFWMsExcel:AddColumn(cPlan1,cTable1,"GRAU_INSTRUCAO",1)       
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CENTRO_CUSTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CBO",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FUNCAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SITUACAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"AUTONOMO",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CPF",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"RG",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PIS",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_NASC",1)                
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MES_NIVER",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MAE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"PAI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TEM_FILHOS",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"ENDERECO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NUMERO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"BAIRRO",1)         
        oFWMsExcel:AddColumn(cPlan1,cTable1,"MUNICIPIO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CEP",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TELEFONE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"EMAIL",1)     
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_AFAST_FGTS",1)   

                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->FILIAL,;
                                               QRY->DT_ADMISSAO	,;
                                               QRY->DT_DEMISSAO,;
                                               QRY->MATRICULA,;
                                               QRY->NOME,;
                                               QRY->SEXO,;
                                               QRY->GRAU_INSTRUCAO,;
                                               QRY->CENTRO_CUSTO,;
                                               QRY->CBO,;                                               
                                               QRY->FUNCAO,;
                                               QRY->SITUACAO,;
                                               QRY->AUTONOMO,;
                                               QRY->CPF,;
                                               QRY->RG,;
                                               QRY->PIS,;
                                               QRY->DT_NASC,;
                                               QRY->MES_NIVER,;
                                               QRY->MAE,;
                                               QRY->PAI,;
                                               QRY->TEM_FILHOS,;
                                               QRY->ENDERECO,;
                                               QRY->NUMERO,;
                                               QRY->BAIRRO,;
                                               QRY->MUNICIPIO,;
                                               QRY->CEP,;
                                               QRY->TELEFONE,;
                                               QRY->EMAIL,;
                                               QRY->COD_AFAST_FGTS;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return