#include 'protheus.ch'
#include 'parmtype.ch'

user function MT750CMP()

Local aAcho := PARAMIXB[1]

AADD (aAcho, "HC_REVISAO")
AADD (aAcho, "HC_XPERDA")

Return aAcho