#include "rwmake.ch"

User Function MT681INC()

	_area := GetArea()


IF FUNNAME() != 'HPCPP001'
	DbSelectArea("SC2")
	DbSetOrder(1)
	DbGoTop()
	DbSeek(xfilial("SC2")+alltrim(SH6->H6_OP))

	DbSelectArea("SG2")
	DbSetOrder(3)
	DbSeek(xfilial("SG2")+SC2->C2_PRODUTO+SH6->H6_OPERAC)

	If !Found()
		DbSelectArea("SG2")
		DbSetOrder(8)
		DbSeek(xfilial("SG2")+Padr(SubStr(SC2->C2_PRODUTO,1,8),26)+SH6->H6_OPERAC)
	Endif

	/*if SH6->H6_OPERAC = "30"
	
	_cFase := "30"

		If SELECT("TMPSC2") > 0
			TMPSC2->(DBCLOSEAREA())
		endif

		_cQuery := " SELECT  C2_RECURSO FROM SC2010 WITH(NOLOCK) JOIN SB1010 WITH(NOLOCK) ON C2_PRODUTO = B1_COD AND SB1010.D_E_L_E_T_ = '' " 
		_cQuery += " WHERE C2_NUM = '"+SC2->C2_NUM+"' AND SC2010.D_E_L_E_T_ = '' AND B1_TIPO IN ('PI','PF') " 
		_cQuery += " GROUP BY C2_RECURSO "

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuery),"TMPSC2",.T.,.T.)

		If alltrim(SC2->C2_RECURSO) <> "" 
			_recurso := SC2->C2_RECURSO
		Else	
			If SC2->C2_STATUS = "S"
				DbSelectarea("SH8")
				DbSetOrder(1)
				DbSeek(xfilial("SH8")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+_cFase)
				If Found()
					_recurso := SH8->H8_RECURSO
				Else
					_recurso := SG2->G2_RECURSO
				Endif
			Else
				_recurso := SG2->G2_RECURSO
			Endif
		Endif

	endif
	
	/*RECLOCK('SH6',.T.)
		SH6->H6_RECURSO := _recurso
	MSUNLOCK()
	*/		

	//DbSkip()
	If alltrim(SG2->G2_PRODUTO) == alltrim(SC2->C2_PRODUTO) .or. SubStr(SC2->C2_PRODUTO,1,8) == alltrim(SG2->G2_REFGRD)
		_ultimo := .F.
	Else
		_ultimo := .T.
	Endif

	If _ultimo .and. alltrim(SC2->C2_YFORNEC) == ""
		_qry := "update "+RetSqlName("SD4")+" set D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_ where D4_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D4_COD like 'B"+SubStr(SC2->C2_PRODUTO,2,7)+"%'"
		TcSqlExec(_qry)
	Endif

	_qry := "update "+RetSqlName("SD3")+" set D3_QUANT = round(((G2_TEMPAD/G2_LOTEPAD)*G2_MAOOBRA)*H6_QTDPROD,2) "
	_qry += "from "+RetSqlName("SD3")+" SD3 "
	_qry += "inner join "+RetSqlName("SC2")+" SC2 on SC2.D_E_L_E_T_ = '' and C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD = D3_OP "
	_qry += "inner join "+RetSqlName("SH6")+" SH6 on SH6.D_E_L_E_T_ = '' and H6_OP = D3_OP and D3_OP = H6_OP and C2_PRODUTO = H6_PRODUTO and D3_EMISSAO = H6_DATAFIN and D3_IDENT = H6_IDENT "
	_qry += "inner join "+RetSqlName("SG2")+" SG2 on SG2.D_E_L_E_T_ = '' and G2_REFGRD = left(C2_PRODUTO,8) and G2_OPERAC = H6_OPERAC "
	//_qry += "inner join "+RetSqlName("SH1")+" SH1 on SH1.D_E_L_E_T_ = '' and H1_CODIGO = G2_RECURSO "
	_qry += "where SD3.D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "
	TcSqlExec(_qry)

	_qry := "Select * from "+RetSqlName("SD3")+" where D_E_L_E_T_ = '' and D3_COD <> 'MODGGF' and D3_COD like 'MOD%' and D3_OP = '"+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN+SC2->C2_ITEMGRD+"' and D3_XGGF = '' "

	If Select("TMPSD3") > 0 
		TMPSD3->(DbCloseArea()) 
	EndIf 

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD3",.T.,.T.)
	DbSelectArea("TMPSD3")
	DbGoTop()

	While !EOF()

		cDocSeq := PROXNUM(.F.) // Funcao que VERIFICA o MV_DOCSEQ
		cProduto:= GetMV("MV_XPRDGGF") 
		If GetMV("MV_DOCSEQ") <> cDocSeq 
			PutMV("MV_DOCSEQ",cDocSeq) 
		EndIf

		DbSelectArea("SD3")
		RecLock("SD3",.T.)
		SD3->D3_FILIAL  := xFilial("SD3")
		SD3->D3_TM      := TMPSD3->D3_TM
		SD3->D3_COD     := cProduto
		SD3->D3_UM      := TMPSD3->D3_UM
		SD3->D3_QUANT   := TMPSD3->D3_QUANT
		SD3->D3_OP		:= TMPSD3->D3_OP
		SD3->D3_CF      := TMPSD3->D3_CF
		SD3->D3_CONTA   := TMPSD3->D3_CONTA
		SD3->D3_LOCAL   := TMPSD3->D3_LOCAL
		SD3->D3_DOC     := SubStr(TMPSD3->D3_OP,1,6)
		SD3->D3_EMISSAO := stod(TMPSD3->D3_EMISSAO)
		SD3->D3_GRUPO   := TMPSD3->D3_GRUPO
		SD3->D3_CUSTO1  := TMPSD3->D3_CUSTO1
		SD3->D3_NUMSEQ  := cDocseq
		SD3->D3_TIPO    := TMPSD3->D3_TIPO
		SD3->D3_USUARIO := UsrRetName(__cUserID)
		SD3->D3_CHAVE   := TMPSD3->D3_CHAVE
		MsUnlock()

		_qry := "Update "+RetSqlName("SD3")+" set D3_XGGF = 'S' where R_E_C_N_O_ = "+Str(TMPSD3->R_E_C_N_O_)+" "
		TcSqlExec(_qry)

		DbSelectArea("TMPSD3")
		DbSkip()
	End
	TMPSD3->(DbCloseArea())
ENDIF
	RestArea(_area)
Return()