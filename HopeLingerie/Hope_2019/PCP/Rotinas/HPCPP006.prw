#INCLUDE "RWMAKE.CH" 

User Function HPCPP006

	If MsgYesNo("Atualizar todas estrutura?","Confirma��o")
//		_qry := "update "+RetSqlName("SZE")+" set D_E_L_E_T_ = '*' and ZE_PRODUTO = '0HR90470' "
//		TcSqlExec(_qry)
//		_qry := "update "+RetSqlName("SZF")+" set D_E_L_E_T_ = '*' and ZF_PRODUTO = '0HR90470'  "
//		TcSqlExec(_qry)
//		_qry := "update "+RetSqlName("SZG")+" set D_E_L_E_T_ = '*' and ZG_PRODUTO = '0HR90470' "
//		TcSqlExec(_qry)
//		_qry := "update "+RetSqlName("SZK")+" set D_E_L_E_T_ = '*' and ZK_PRODUTO = '0HR90470' "
//		TcSqlExec(_qry)

		_qry := "Select left(G1_COD,8) as G1_COD from "+RetSqlName("SG1")+" SG1 with (nolock) where SG1.D_E_L_E_T_ = '' "
		_qry += "and (Select top 1 ZE_PRODUTO from "+RetSqlName("SZE")+" SZE with (nolock) where SZE.D_E_L_E_T_ = '' and left(ZE_PRODUTO,8) = left(G1_COD,8)) is null "
		_qry += "and left(G1_COD,1) <> 'B' "
		_qry += "group by left(G1_COD,8) "
		If Select("TMPSG1") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSG1",.T.,.T.)

		DbSelectArea("TMPSG1")
		
		While TMPSG1->(!EOF())
		DbSelectArea("SG1")
		DbSetOrder(1)
		DbGoTop()
		DbSeek(xfilial("SG1")+left(TMPSG1->G1_COD,8))
		
		While !EOF() .and. SubStr(SG1->G1_COD,1,8) == left(TMPSG1->G1_COD,8)
			DbSelectArea("SB1")
			DbSetOrder(1)
			Dbseek(xfilial("SB1")+SG1->G1_COD)
			
			IF SB1->B1_TIPO = "MI" .OR. SB1->B1_TIPO = "PI" .OR. SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT" .or. SB1->B1_TIPO = "MB" 
				If SubStr(SG1->G1_COMP,1,8) <> "B"+SubStr(SG1->G1_COD,1,7)
	
					DbSelectArea("SZE")
					DbSetOrder(2)
					DbSeek(xfilial("SZE")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+SG1->G1_TRT+SubStr(SG1->G1_COMP,1,8))
				
					If !Found()
						RecLock("SZE",.T.)
							Replace ZE_FILIAL		with xfilial("SZE")
							Replace ZE_PRODUTO		with SubStr(SG1->G1_COD,1,8)
							Replace ZE_REVISAO		with SG1->G1_REVINI
							Replace ZE_INDICE		with SG1->G1_TRT
							Replace ZE_COMP			with SubStr(SG1->G1_COMP,1,8)
							Replace ZE_DESCCOM		with POSICIONE("SB1",1,xFilial("SB1")+SG1->G1_COMP,"B1_DESC")
							Replace ZE_DESCPRO		with POSICIONE("SB1",1,xFilial("SB1")+SG1->G1_COD,"B1_DESC")
							Replace ZE_UM			with SB1->B1_UM
							Replace ZE_CORTE		with "N"
						MsUnLock()
					Endif
				
					DbSelectArea("SZG")
					DbSetOrder(2)
					DbSeek(xfilial("SZG")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
				
					If !Found()
						RecLock("SZG",.T.)
							Replace ZG_FILIAL		with xfilial("SZG")
							Replace ZG_PRODUTO		with SubStr(SG1->G1_COD,1,8)
							Replace ZG_REVISAO		with SG1->G1_REVINI
							Replace ZG_COMP			with SubStr(SG1->G1_COMP,1,8)
							Replace ZG_INDICE		with SG1->G1_TRT
							Replace ZG_COR			with SubStr(SG1->G1_COD,9,3)
							IF SG1->G1_QUANT > 0 
							Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
							ENDIF
						MsUnLock()
					ELSE
					
					RecLock("SZG",.F.)
					IF SG1->G1_QUANT > 0 
						Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
					ENDIF
					MsUnlock()	
						
					Endif
				
					DbSelectArea("SZK")
					DbSetOrder(2)
					DbSeek(xfilial("SZK")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,12,4))
	
					If !Found()
						RecLock("SZK",.T.)
							Replace ZK_FILIAL		with xfilial("SZK")
							Replace ZK_PRODUTO		with SubStr(SG1->G1_COD,1,8)
							Replace ZK_REVISAO		with SG1->G1_REVINI
							Replace ZK_COMP			with SubStr(SG1->G1_COMP,1,8)
							Replace ZK_INDICE		with SG1->G1_TRT
							Replace ZK_TAM			with SubStr(SG1->G1_COD,12,4)
							Replace ZK_TAMNV		with SubStr(SG1->G1_COMP,12,4)
						MsUnLock()
					Endif
				
					DbSelectArea("SB4")
					DbSetOrder(1)
					Dbseek(xfilial("SB4")+SubStr(SG1->G1_COD,1,8))
				
					_wtam := rettam(SB4->B4_COLUNA,SubStr(SG1->G1_COD,12,4))
					_campo:= "ZF_TAM"+_wtam
				
					DbSelectArea("SZF")
					DbSetOrder(2)
					DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
	
					If !Found()
						RecLock("SZF",.T.)
							Replace ZF_FILIAL		with xfilial("SZF")
							Replace ZF_PRODUTO		with SubStr(SG1->G1_COD,1,8)
							Replace ZF_REVISAO		with SG1->G1_REVINI
							Replace ZF_COMP			with SubStr(SG1->G1_COMP,1,8)
							Replace ZF_INDICE		with SG1->G1_TRT
							Replace ZF_COR			with SubStr(SG1->G1_COD,9,3)
							Replace &_campo			with SG1->G1_QUANT
						MsUnLock()
					Else
						RecLock("SZF",.F.)
							Replace &_campo			with SG1->G1_QUANT
						MsUnLock()
					Endif
				Else
					DbselectArea("SG1")
					_areaG1 := GetArea()
					_cod := SG1->G1_COD
	
					DbSetOrder(1)
					DbSeek(xfilial("SG1")+"B"+SubStr(_cod,1,7)+SubStr(_cod,9,7))
					
					While !EOF() .and. SG1->G1_COD ="B"+SubStr(_cod,1,7)+SubStr(_cod,9,7)
					
						DbSelectArea("SZE")
						DbSetOrder(2)
						DbSeek(xfilial("SZE")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+SG1->G1_TRT+SubStr(SG1->G1_COMP,1,8))
				
						If !Found()
							RecLock("SZE",.T.)
								Replace ZE_FILIAL		with xfilial("SZE")
								Replace ZE_PRODUTO		with SubStr(_COD,1,8)
								Replace ZE_REVISAO		with SG1->G1_REVINI
								Replace ZE_INDICE		with SG1->G1_TRT
								Replace ZE_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZE_DESCCOM		with POSICIONE("SB1",1,xFilial("SB1")+SG1->G1_COMP,"B1_DESC")
								Replace ZE_DESCPRO		with POSICIONE("SB1",1,xFilial("SB1")+_COD,"B1_DESC")
								Replace ZE_UM			with SB1->B1_UM
								Replace ZE_CORTE		with "S"
							MsUnLock()
						Endif
				
						DbSelectArea("SZG")
						DbSetOrder(2)
						DbSeek(xfilial("SZG")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(_COD,9,3))
				
						If !Found()
							RecLock("SZG",.T.)
								Replace ZG_FILIAL		with xfilial("SZG")
								Replace ZG_PRODUTO		with SubStr(_COD,1,8)
								Replace ZG_REVISAO		with SG1->G1_REVINI
								Replace ZG_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZG_INDICE		with SG1->G1_TRT
								Replace ZG_COR			with SubStr(_COD,9,3)
								IF SG1->G1_QUANT > 0 
								Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
								ENDIF
							MsUnLock()
						ELSE
					
						RecLock("SZG",.F.)
						IF SG1->G1_QUANT > 0 
							Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
						ENDIF
						MsUnlock()	
						Endif
				
						DbSelectArea("SZK")
						DbSetOrder(2)
						DbSeek(xfilial("SZK")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(_COD,12,4))
	
						If !Found()
							RecLock("SZK",.T.)
								Replace ZK_FILIAL		with xfilial("SZK")
								Replace ZK_PRODUTO		with SubStr(_COD,1,8)
								Replace ZK_REVISAO		with SG1->G1_REVINI
								Replace ZK_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZK_INDICE		with SG1->G1_TRT
								Replace ZK_TAM			with SubStr(_COD,12,4)
								Replace ZK_TAMNV		with SubStr(SG1->G1_COMP,12,4)
							MsUnLock()
						Endif
				
						DbSelectArea("SB4")
						DbSetOrder(1)
						Dbseek(xfilial("SB4")+SubStr(_COD,1,8))
				
						_wtam := rettam(SB4->B4_COLUNA,SubStr(_COD,12,4))
						_campo:= "ZF_TAM"+_wtam
				
						DbSelectArea("SZF")
						DbSetOrder(2)
						DbSeek(xfilial("SZF")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(_COD,9,3))
	
						If !Found()
							RecLock("SZF",.T.)
								Replace ZF_FILIAL		with xfilial("SZF")
								Replace ZF_PRODUTO		with SubStr(_COD,1,8)
								Replace ZF_REVISAO		with SG1->G1_REVINI
								Replace ZF_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZF_INDICE		with SG1->G1_TRT
								Replace ZF_COR			with SubStr(_COD,9,3)
								Replace &_campo			with SG1->G1_QUANT
							MsUnLock()
						Else
							RecLock("SZF",.F.)
								Replace &_campo			with SG1->G1_QUANT
							MsUnLock()
						Endif
						
						DbSelectArea("SG1")
						DbSkip()
					End
				
					RestArea(_areaG1)
				Endif
			Endif
		
			DbSelectArea("SG1")
			DbSkip()
		End
		
		DbSelectArea("TMPSG1")
		DbSkip()
		End
		DbCloseArea()
	Else
		If Pergunte("HPCPP006",.T.)

			DbSelectArea("SG1")
			DbSetOrder(1)
			DbGoTop()
			DbSeek(xfilial("SG1")+ALLTRIM(MV_PAR01))
			
			While !EOF() .and. SubStr(SG1->G1_COD,1,8) == alltrim(MV_PAR01)
				DbSelectArea("SB1")
				DbSetOrder(1)
				Dbseek(xfilial("SB1")+SG1->G1_COD)
				
				IF SB1->B1_TIPO = "MI" .OR. SB1->B1_TIPO = "PI" .OR. SB1->B1_TIPO = "PF" .or. SB1->B1_TIPO = "KT" .or. SB1->B1_TIPO = "MB" 
					If SubStr(SG1->G1_COMP,1,8) <> "B"+SubStr(SG1->G1_COD,1,7)
		
						DbSelectArea("SZE")
						DbSetOrder(2)
						DbSeek(xfilial("SZE")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+SG1->G1_TRT+SubStr(SG1->G1_COMP,1,8))
					
						If !Found()
							RecLock("SZE",.T.)
								Replace ZE_FILIAL		with xfilial("SZE")
								Replace ZE_PRODUTO		with SubStr(SG1->G1_COD,1,8)
								Replace ZE_REVISAO		with SG1->G1_REVINI
								Replace ZE_INDICE		with SG1->G1_TRT
								Replace ZE_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZE_DESCCOM		with POSICIONE("SB1",1,xFilial("SB1")+SG1->G1_COMP,"B1_DESC")
								Replace ZE_DESCPRO		with POSICIONE("SB1",1,xFilial("SB1")+SG1->G1_COD,"B1_DESC")
								Replace ZE_UM			with SB1->B1_UM
								Replace ZE_CORTE		with "N"
							MsUnLock()
						Endif
					
						DbSelectArea("SZG")
						DbSetOrder(2)
						DbSeek(xfilial("SZG")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
					
						If !Found()
							RecLock("SZG",.T.)
								Replace ZG_FILIAL		with xfilial("SZG")
								Replace ZG_PRODUTO		with SubStr(SG1->G1_COD,1,8)
								Replace ZG_REVISAO		with SG1->G1_REVINI
								Replace ZG_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZG_INDICE		with SG1->G1_TRT
								Replace ZG_COR			with SubStr(SG1->G1_COD,9,3)
								IF SG1->G1_QUANT > 0 
								Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
								ENDIF
							MsUnLock()
						ELSE
					
					RecLock("SZG",.F.)
					IF SG1->G1_QUANT > 0 
						Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
					ENDIF
					MsUnlock()
					Endif
					
						DbSelectArea("SZK")
						DbSetOrder(2)
						DbSeek(xfilial("SZK")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,12,4))
		
						If !Found()
							RecLock("SZK",.T.)
								Replace ZK_FILIAL		with xfilial("SZK")
								Replace ZK_PRODUTO		with SubStr(SG1->G1_COD,1,8)
								Replace ZK_REVISAO		with SG1->G1_REVINI
								Replace ZK_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZK_INDICE		with SG1->G1_TRT
								Replace ZK_TAM			with SubStr(SG1->G1_COD,12,4)
								Replace ZK_TAMNV		with SubStr(SG1->G1_COMP,12,4)
							MsUnLock()
						Endif
					
						DbSelectArea("SB4")
						DbSetOrder(1)
						Dbseek(xfilial("SB4")+SubStr(SG1->G1_COD,1,8))
					
						_wtam := rettam(SB4->B4_COLUNA,SubStr(SG1->G1_COD,12,4))
						_campo:= "ZF_TAM"+_wtam
					
						DbSelectArea("SZF")
						DbSetOrder(2)
						DbSeek(xfilial("SZF")+Padr(SubStr(SG1->G1_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(SG1->G1_COD,9,3))
		
						If !Found()
							RecLock("SZF",.T.)
								Replace ZF_FILIAL		with xfilial("SZF")
								Replace ZF_PRODUTO		with SubStr(SG1->G1_COD,1,8)
								Replace ZF_REVISAO		with SG1->G1_REVINI
								Replace ZF_COMP			with SubStr(SG1->G1_COMP,1,8)
								Replace ZF_INDICE		with SG1->G1_TRT
								Replace ZF_COR			with SubStr(SG1->G1_COD,9,3)
								Replace &_campo			with SG1->G1_QUANT
							MsUnLock()
						Else
							RecLock("SZF",.F.)
								Replace &_campo			with SG1->G1_QUANT
							MsUnLock()
						Endif
					Else
						DbselectArea("SG1")
						_areaG1 := GetArea()
						_cod := SG1->G1_COD
		
						DbSetOrder(1)
						DbSeek(xfilial("SG1")+"B"+SubStr(_cod,1,7)+SubStr(_cod,9,7))
						
						While !EOF() .and. SG1->G1_COD ="B"+SubStr(_cod,1,7)+SubStr(_cod,9,7)
						
							DbSelectArea("SZE")
							DbSetOrder(2)
							DbSeek(xfilial("SZE")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+SG1->G1_TRT+SubStr(SG1->G1_COMP,1,8))
					
							If !Found()
								RecLock("SZE",.T.)
									Replace ZE_FILIAL		with xfilial("SZE")
									Replace ZE_PRODUTO		with SubStr(_COD,1,8)
									Replace ZE_REVISAO		with SG1->G1_REVINI
									Replace ZE_INDICE		with SG1->G1_TRT
									Replace ZE_COMP			with SubStr(SG1->G1_COMP,1,8)
									Replace ZE_DESCCOM		with POSICIONE("SB1",1,xFilial("SB1")+SG1->G1_COMP,"B1_DESC")
									Replace ZE_DESCPRO		with POSICIONE("SB1",1,xFilial("SB1")+_COD,"B1_DESC")
									Replace ZE_UM			with SB1->B1_UM
									Replace ZE_CORTE		with "S"
								MsUnLock()
							Endif
					
							DbSelectArea("SZG")
							DbSetOrder(2)
							DbSeek(xfilial("SZG")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(_COD,9,3))
					
							If !Found()
								RecLock("SZG",.T.)
									Replace ZG_FILIAL		with xfilial("SZG")
									Replace ZG_PRODUTO		with SubStr(_COD,1,8)
									Replace ZG_REVISAO		with SG1->G1_REVINI
									Replace ZG_COMP			with SubStr(SG1->G1_COMP,1,8)
									Replace ZG_INDICE		with SG1->G1_TRT
									Replace ZG_COR			with SubStr(_COD,9,3)
									IF SG1->G1_QUANT > 0
									Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
									ENDIF
								MsUnLock()
							ELSE
					
					RecLock("SZG",.F.)
					IF SG1->G1_QUANT > 0 
						Replace ZG_CORNV		with SubStr(SG1->G1_COMP,9,3)
					ENDIF
					MsUnlock()
							Endif
					
							DbSelectArea("SZK")
							DbSetOrder(2)
							DbSeek(xfilial("SZK")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(_COD,12,4))
		
							If !Found()
								RecLock("SZK",.T.)
									Replace ZK_FILIAL		with xfilial("SZK")
									Replace ZK_PRODUTO		with SubStr(_COD,1,8)
									Replace ZK_REVISAO		with SG1->G1_REVINI
									Replace ZK_COMP			with SubStr(SG1->G1_COMP,1,8)
									Replace ZK_INDICE		with SG1->G1_TRT
									Replace ZK_TAM			with SubStr(_COD,12,4)
									Replace ZK_TAMNV		with SubStr(SG1->G1_COMP,12,4)
								MsUnLock()
							Endif
					
							DbSelectArea("SB4")
							DbSetOrder(1)
							Dbseek(xfilial("SB4")+SubStr(_COD,1,8))
					
							_wtam := rettam(SB4->B4_COLUNA,SubStr(_COD,12,4))
							_campo:= "ZF_TAM"+_wtam
					
							DbSelectArea("SZF")
							DbSetOrder(2)
							DbSeek(xfilial("SZF")+Padr(SubStr(_COD,1,8),15)+SG1->G1_REVINI+Padr(SubStr(SG1->G1_COMP,1,8),15)+SG1->G1_TRT+SubStr(_COD,9,3))
		
							If !Found()
								RecLock("SZF",.T.)
									Replace ZF_FILIAL		with xfilial("SZF")
									Replace ZF_PRODUTO		with SubStr(_COD,1,8)
									Replace ZF_REVISAO		with SG1->G1_REVINI
									Replace ZF_COMP			with SubStr(SG1->G1_COMP,1,8)
									Replace ZF_INDICE		with SG1->G1_TRT
									Replace ZF_COR			with SubStr(_COD,9,3)
									Replace &_campo			with SG1->G1_QUANT
								MsUnLock()
							Else
								RecLock("SZF",.F.)
									Replace &_campo			with SG1->G1_QUANT
								MsUnLock()
							Endif
							
							DbSelectArea("SG1")
							DbSkip()
						End
					
						RestArea(_areaG1)
					Endif
				Endif
			
				DbSelectArea("SG1")
				DbSkip()
			End
		Endif
	Endif
ALERT("FIM")
Return

Static Function rettam(xcoluna,_tam)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
_BVTAM := ""

cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WHERE "
cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' " 
cQuery += "ORDER BY R_E_C_N_O_"

cQuery := ChangeQuery(cQuery)
If Select("TMPSBV") > 0 
   	 TMPSBV->(DbCloseArea()) 
EndIf 
dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

DbSelectArea("TMPSBV")
DbGoTop()
_xtam := 0
_achou := .F.
While !EOF() .and. !_achou
	_xtam++
	If alltrim(TMPSBV->BV_CHAVE) == alltrim(_tam)
		_achou := .T.
	Endif
	DbSkip()
End

DbCloseArea()
_BVTAM := StrZero(_xtam,3)
Return(_BVTAM)
