#INCLUDE "RWMAKE.CH" 
#INCLUDE "TBICONN.CH" 
#include "xmlxfun.ch"        
#include "fileio.ch"

User Function HPCPP012() 

        Local aPergs    := {}
        Local lConv      := .F.
        
        Private cArq       := ""
		Private cArqMacro  := "XLS2DBF.XLA"
		Private cTemp      := GetTempPath() //pega caminho do temp do client
		Private cSystem    := Upper(GetSrvProfString("STARTPATH",""))//Pega o caminho do sistema
        Private aResps     := {}    
        Private aArquivos  := {}    
 
        aAdd(aPergs,{6,"Arquivo: ",Space(150),"",'.T.','.T.',80,.F.,""})            
		
        If ParamBox(aPergs,"Parametros", @aResps)

	      	aAdd(aArquivos, AllTrim(aResps[1]))
			cArq := AllTrim(aResps[1]) 
       		Processa({|| ProcMovs()})
        Endif           
Return
                         
Static Function ProcMovs()
        Local cError    	 := ""
        Local cWarning      := ""
        Local nFile         := 0
        Local aLinhas   	 := {}                          
        Local cFile     	 := AllTrim(aResps[1])
        Local nI            := 0
		Local cLinha  := ""
		Local nLintit    := 1 
		Local nLin 		 := 0
		Local nTotLin := 0
		Local aDados  := {}
		Local nHandle := 0
		Local _i 		:= 0
		dDataFec    := GetMV("MV_ULMES") 
		lIntQual    := .F. 
		lDelOpSC 	:= GetMV("MV_DELOPSC")== "S"
		lProdAut := GetMv("MV_PRODAUT")
		
    	nFile := fOpen(cFile, FO_READ + FO_DENYWRITE)
   
        If nFile == -1
        	If !Empty(cFile)
            	MsgAlert("O arquivo nao pode ser aberto!", "Atencao!")
            EndIf
            Return
        EndIf          

		  nHandle := Ft_Fuse(cFile)
		  Ft_FGoTop()                                                         
		  nLinTot := FT_FLastRec()-1
		  ProcRegua(nLinTot)

		  While nLinTit > 0 .AND. !Ft_FEof()
			   Ft_FSkip()
			   nLinTit--
		  EndDo

		  Do While !Ft_FEof()
			   IncProc("Carregando Linha "+AllTrim(Str(nLin))+" de "+AllTrim(Str(nLinTot)))
			   nLin++
			   cLinha := Ft_FReadLn()
			   If Empty(AllTrim(StrTran(cLinha,',','')))
			      Ft_FSkip()
			      Loop
			   EndIf
			   cLinha := StrTran(cLinha,'"',"'")
			   cLinha := '{"'+cLinha+'"}' 
			   cLinha := StrTran(cLinha,';',',')
			   cLinha := StrTran(cLinha,',','","')
			   aAdd(aDados, &cLinha)
   
			   FT_FSkip()
		  EndDo

		  FT_FUse()             
       
		For _i := 1 to len(aDados)
			
			DbSelectArea("SC2")
			DbSetOrder(1)
			DbSeek(xfilial("SC2")+aDados[_i,1])
			
			While !EOF() .and. SC2->C2_NUM = aDados[_i,1]
			
				If SC2->C2_QUJE < SC2->C2_QUANT .and. Empty(SC2->C2_DATRF)
					DbSelectArea("SH6")
					DbSetOrder(1)
					DbSeek(xfilial("SH6")+SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN)
					
					If !Found() 
				
						If alltrim(SC2->C2_SEQPAI) = '' 
							If SC2->C2_QUJE == 0
								_C2Area := GetArea()
								_nRecSC2:=Recno()
								A650Deleta("SC2",_nRecSC2,5,{},"ELIMOP")
								RestArea(_C2Area)
							Endif
						Endif
/*
					Else
						If Empty(SC2->C2_DATRF) 
							DbSelectArea("SD3")
							DbSetOrder(1)
							cChave := xFilial("SD3")+SH6->(H6_OP+H6_PRODUTO+H6_LOCAL)
							nOpc := 5
							aAcho := {}
							
							If SD3->(DbSeek(cChave))
							
								AADD(aAcho,"D3_TM"		)
								AADD(aAcho,"D3_OP"		)
								AADD(aAcho,"D3_COD"		)
								AADD(aAcho,"D3_QUANT"	)
								AADD(aAcho,"D3_UM"		)
								AADD(aAcho,"D3_QTSEGUM"	)
								AADD(aAcho,"D3_SEGUM"	)
								AADD(aAcho,"D3_PARCTOT"	)
								AADD(aAcho,"D3_LOCAL"	)
								AADD(aAcho,"D3_CC"		)
								AADD(aAcho,"D3_CONTA"	)
								AADD(aAcho,"D3_EMISSAO"	)
								AADD(aAcho,"D3_DOC"		)
								AADD(aAcho,"D3_PERDA"	)
								AADD(aAcho,"D3_DESCRI"	)
								AADD(aAcho,"D3_LOTECTL")
							
								dbSelectArea("SX3")
								dbSeek("SD3")
								While !EOF() .And. (X3_ARQUIVO == "SD3")
									If X3USO(X3_USADO) .And. cNivel >= X3_NIVEL .And. (ASCAN(aAcho,Trim(X3_CAMPO)) == 0) .And. X3_PROPRI == "U"
										AADD(aAcho,TRIM(X3_CAMPO))
									EndIf
									dbSkip()
								EndDo
							
								Pergunte("MTA250",.F.)
								//Chama funcao de encerramento do Op do MATA250
								A250Encer("SD3",SD3->(RecNo()),7)
							Endif
						Endif
					*/
					Endif
				Endif
				DbSelectArea("SC2")
				DbSkip()
			End
		Next
Return



User Function LiberarCQ()

Local aLibera := {}

dDataL := dDataBase

_qry := "Select * from SD7010 where D_E_L_E_T_ = '' and D7_SALDO > 0 and D7_FILIAL = '0101' and D7_LIBERA = '' order by R_E_C_N_O_ "
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSD7",.T.,.T.)

DbSelectArea("TMPSD7")
DbGoTop()

While !EOF()
	lMsErroAuto := .F.

	DbSelectArea("SD7")
	DbSetOrder(1)
	DbGoTop()
	
	dbSeek(xFilial()+TMPSD7->D7_NUMERO+TMPSD7->D7_PRODUTO+TMPSD7->D7_LOCAL)
	aLibera := {}
	aAdd(aLibera,{      {"D7_TIPO" ,1 ,Nil},; //D7_TIPO
	                    {"D7_DATA" ,dDataL,Nil},; //D7_DATA
	                    {"D7_QTDE" ,TMPSD7->D7_SALDO ,Nil},; //D7_QTDE
	                    {"D7_OBS" ,"" ,Nil},; //D7_OBS
	                    {"D7_QTSEGUM" ,TMPSD7->D7_SALDO2 ,Nil},; //D7_QTSEGUM
	                    {"D7_MOTREJE" ,"" ,Nil},; //D7_MOTREJE
	                    {"D7_LOCDEST" ,TMPSD7->D7_LOCDEST ,Nil},; //D7_LOCDEST
	                    {"D7_LOCALIZ" ,"PADRAO" ,Nil},; //D7_LOCDEST
	                    {"D7_SALDO" ,NIL ,Nil},; //D7_SALDO
	                    {"D7_ESTORNO" ,NIL ,Nil}}) //D7_ESTORNO
	
	
	MSExecAuto({|x,y| mata175(x,y)},aLibera,4)
	
	
	IF lMsErroAuto
	     MOSTRAERRO()
	Endif
	
	DbSelectArea("TMPSD7")
	DbSkip()
End

DbCloseArea()

Return