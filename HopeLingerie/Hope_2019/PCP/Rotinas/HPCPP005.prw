#include "protheus.ch"
#include "rwmake.ch"
#include "topconn.ch"

User Function  HPCPP005()

_area := GetArea()
MATA712()

_qry := "Select * from "+RetSqlName("SC1")+" where D_E_L_E_T_ = '' and C1_SEQMRP <> '' and C1_XDTINI = '' "

cQuery := ChangeQuery(_qry)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC1",.T.,.T.)
	
DbSelectArea("TMPSC1")
DbGoTop()		    

While !EOF()
	dbSelectArea('SB1')
	dbSetOrder(1)
	DbSeek(xfilial("SB1")+TMPSC1->C1_PRODUTO)
	
	If SB1->B1_TIPE = "D"
		_lead := SB1->B1_PE
	ElseIf SB1->B1_TIPE = "S"
		_lead := SB1->B1_PE*7
	ElseIf SB1->B1_TIPE = "M"
		_lead := SB1->B1_PE*30
	ElseIf SB1->B1_TIPO = "A"
		_lead := SB1->B1_PE*365
	Else 
		_lead := 1
	Endif
	
	_qry := "update "+RetSqlName("SC1")+" set C1_XDTINI = '"+dtos(stod(TMPSC1->C1_DATPRF)-_lead)+"' where R_E_C_N_O_ = "+str(TMPSC1->R_E_C_N_O_)
	TcSqlExec(_qry)
	
	DbSelectArea("TMPSC1")
	DbSkip()
End

DbCloseArea()
/*
_qry := "Select Max(C2_SEQMRP) as MAX from "+RetSqlName("SC2")+" where D_E_L_E_T_ = '' "
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC2",.T.,.T.)

DbSelectArea("TMPSC2")
DbGoTop()
_seq := TMPSC2->MAX

DbCloseArea()

_qry := "update "+RetSqlName("SC2")+" set C2_DESTINA = '' where D_E_L_E_T_ = '' and C2_TPOP = 'P' and C2_SEQMRP = '"+_seq+"' and C2_DESTINA = 'E'"
TcSqlExec(_qry)

_qry := "update "+RetSqlName("SC2")+" set C2_DATPRF = (Select C2_DATPRF from "+RetSqlName("SC2")+" SC2A where SC2A.D_E_L_E_T_ = '' and SC2A.C2_TPOP = 'P' and SC2A.C2_SEQMRP = '"+_seq+"' and SC2A.C2_SEQUEN = SC2.C2_SEQPAI and SC2A.C2_NUM = SC2.C2_NUM), "
_qry += "C2_DATPRI = (Select C2_DATPRI from "+RetSqlName("SC2")+" SC2A where SC2A.D_E_L_E_T_ = '' and SC2A.C2_TPOP = 'P' and SC2A.C2_SEQMRP = '"+_seq+"' and SC2A.C2_SEQUEN = SC2.C2_SEQPAI and SC2A.C2_NUM = SC2.C2_NUM) "
_qry += "from SC2010 SC2 where SC2.D_E_L_E_T_ = '' and C2_TPOP = 'P' and C2_SEQMRP = '"+_seq+"' and C2_SEQUEN <> '001' and "
_qry += "C2_DATPRF > (Select C2_DATPRF from "+RetSqlName("SC2")+" SC2A where SC2A.D_E_L_E_T_ = '' and SC2A.C2_TPOP = 'P' and SC2A.C2_SEQMRP = '"+_seq+"' and SC2A.C2_SEQUEN = SC2.C2_SEQPAI and SC2A.C2_NUM = SC2.C2_NUM) "
TcSqlExec(_qry)

_QRY := "Select * from "+RetSqlName("SC2")+" SC2 "
_QRY += "INNER JOIN "+RetSqlName("SB1")+" SB1 ON SB1.D_E_L_E_T_ = '' AND SB1.B1_COD = C2_PRODUTO "
_QRY += "inner join "+RetSqlName("SG1")+" SG1 on SG1.D_E_L_E_T_ = '' and G1_COD = C2_PRODUTO "
_QRY += "inner join "+RetSqlName("SB1")+" SB1A on SB1A.D_E_L_E_T_ = '' and SB1A.B1_COD = G1_COMP "
_QRY += "where SC2.D_E_L_E_T_ = '' and C2_TPOP = 'P' and C2_SEQMRP = '"+_SEQ+"' and C2_SEQUEN = '001' and SB1.B1_TIPO IN ('PA','KT','PF','ME') AND "
_QRY += "(Select top 1 C2_PRODUTO from "+RetSqlName("SC2")+" SC2A where SC2A.D_E_L_E_T_ = '' and SC2A.C2_TPOP = 'P' and SC2A.C2_SEQMRP = '"+_SEQ+"' and SC2.C2_SEQUEN = SC2A.C2_SEQPAI and SC2A.C2_NUM = SC2.C2_NUM) is null "
_QRY += "and SB1A.B1_TIPO Not in ('MP','BN') "
_QRY += "order by C2_NUM, G1_COMP "

dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC2",.T.,.T.)

DbSelectArea("TMPSC2")
DbGoTop()

While !EOF()
	_QRY := "SELECT TOP 1 R_E_C_N_O_ AS REC FROM SC2010 WHERE C2_PRODUTO = '"+TMPSC2->G1_COMP+"' and D_E_L_E_T_ = '' AND C2_SEQMRP = '"+_SEQ+"' AND C2_QUANT = "+STR(TMPSC2->C2_QUANT*TMPSC2->G1_QUANT)+" AND C2_SEQUEN = '001' "
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPX",.T.,.T.)
	
	DbSelectArea("TMPX")
	DbGoTop()
	_rec := TMPX->REC
	DbCloseArea()

	_qry := "Select Max(C2_SEQUEN) as MAX from "+RetSqlName("SC2")+" where D_E_L_E_T_ = '' and C2_NUM = '"+TMPSC2->C2_NUM+"' "
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"WSC2",.T.,.T.)
	
	DbSelectArea("WSC2")
	DbGoTop()
	_sequen := WSC2->MAX
	
	DbCloseArea()
	
	_qry := "Update "+RetSqlName("SC2")+" set C2_NUM = '"+TMPSC2->C2_NUM+"', C2_SEQUEN = '"+soma1(_sequen,3)+"', C2_DATPRF = '"+TMPSC2->C2_DATPRF+"', C2_DATPRI = '"+TMPSC2->C2_DATPRI+"', C2_SEQPAI = '001', C2_ROTEIRO = '', C2_BATORCA = '' where R_E_C_N_O_ = "+str(_rec)
	TcSqlExec(_qry)
	
	DbSelectArea("TMPSC2")
	DbSkip()
End

DbCloseArea()
*/
RestArea(_area)

Return