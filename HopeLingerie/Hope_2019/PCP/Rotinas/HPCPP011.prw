#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function HPCPP011()

_qry := "select BF_PRODUTO, BF_NUMLOTE, BF_QUANT, 'E1' as ORIG, 'E0' as DEST, BF_LOCALIZ, (Select BE_LOCALIZ from SBE010 SBE where SBE.D_E_L_E_T_ = '' and BE_LOCAL = 'E0' and BE_CODPRO = BF_PRODUTO) as EDEST from SBF010 SBF " 
_qry += "where SBF.D_E_L_E_T_ = '' and BF_LOCALIZ = 'A.FATURAR' "
_qry += "and (Select BE_LOCALIZ from SBE010 SBE where SBE.D_E_L_E_T_ = '' and BE_LOCAL = 'E0' and BE_CODPRO = BF_PRODUTO) is not null "
_qry += "order by BF_NUMLOTE "

_qry := ChangeQuery(_QRY)
If Select("TMPFAT") > 0 
   	 TMPFAT->(DbCloseArea()) 
EndIf 
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPFAT",.T.,.T.)

DbSelectArea("TMPFAT")
DbGoTop()
WHILE !EOF()
	Save(TMPFAT->BF_PRODUTO,TMPFAT->BF_QUANT,TMPFAT->ORIG,TMPFAT->DEST, TMPFAT->BF_LOCALIZ, TMPFAT->EDEST, TMPFAT->BF_NUMLOTE)
	DbSelectArea("TMPFAT")
	DbSkip()
End

TMPFAT->(DbCloseArea()) 

alert("acabou")
Return

				
Static Function Save(cProd,nQuant,cAmzOri,cAmzDest, cEndOri, cEndDest, Lote)
Local cDoc      := ""
Local lContinua := .F.
Local aLnTran   := {}
Local aAgrupa   := {}
Local cQry      := ""
//Local cEndOri   := ""
//Local cEndDest  := ""
Local cUM       := ""
Local cDescri   := ""
Local cSPadrao  := AllTrim(SuperGetMV("MV_XSUBPAD",.F.,"0"))

Local nTransf   := 0
Local aTransf   := {}
Local cTime     := ""
Local cDtTime   := ""
Local nx,ny     := 0
Local nOpcAuto  := 3 // Indica qual tipo de a��o ser� tomada (Inclus�o/Exclus�o)
Local dData     := DDATABASE
Local cHora     := SubStr(Time(),1,5)
Local dDtValid  := DDATABASE
Local cDocSeq   := ""
Local nStatus   := 0
//Local cSubPad   := AllTrim(SuperGetMV("MV_XSUBPAD",.F.,"0"))

Private lMsHelpAuto := .T.
Private lMsErroAuto := .F.

DbSelectArea("SB1")
DbSetOrder(1)
DbSeek(xFilial("SB1")+cProd)

cQry := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,BF_LOCALIZ "
cQry += CRLF + "from "+RetSqlName("SBF")+" SBF "
cQry += CRLF + "where SBF.D_E_L_E_T_ = '' "
cQry += CRLF + "and BF_PRODUTO = '"+cProd+"' "
cQry += CRLF + "and BF_LOCAL   = '"+cAmzOri+"' "
cQry += CRLF + "and BF_LOCALIZ = '"+cEndOri+"' "
cQry += CRLF + "and BF_NUMLOTE = '"+Lote+"' "
cQry += CRLF + "and BF_QUANT > 0 "
cQry += CRLF + "order by BF_PRODUTO,BF_NUMLOTE,BF_LOTECTL "

MemoWrite("HPCPP010.txt",cQry)
    
cQry := ChangeQuery(cQry)

DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQry),"QRY",.F.,.T.)

DbSelectArea("QRY")

nTransf := QRY->BF_QUANT

If QRY->(!EOF())
	While QRY->(!EOF())
		If nTransf > 0
			cEndOri := QRY->BF_LOCALIZ				
			//                     1         2              3        4       5       6        7            8                 9      10       11
			aAdd(aTransf,{QRY->BF_PRODUTO,SB1->B1_DESC,SB1->B1_UM,cAmzOri,cEndOri,cAmzDest,cEndDest,QRY->BF_LOTECTL,QRY->BF_NUMLOTE,"",QRY->BF_QUANT})
			
			nTransf := nTransf - QRY->BF_QUANT
			
			lContinua := .T.
		Else
			EXIT
		EndIf
		
		QRY->(DbSkip())
	EndDo
EndIf

QRY->(DbCloseArea()) 

If lContinua
	cTrnDAP := GetSxENum("SD3","D3_DOC",1)
	cDoc := cTrnDAP
	
	For nx := 1 to Len(aTransf)
		If aTransf[nx][11] < 0
			MsgAlert("N�o � poss�vel realizar uma transfer�ncia com valor negativo.","Aten��o")
			Loop
		EndIf
		
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xFilial("SB1")+aTransf[nx][1])
		
		cTpConv := SB1->B1_TIPCONV
		nFtConv := SB1->B1_CONV
		cSegUm  := SB1->B1_SEGUM
		
		DbSelectArea("SB2")
		DbSetOrder(1)
		DbSeek(xFilial("SB2")+aTransf[nx][1]+aTransf[nx][4])
		
		cDocSeq := PROXNUM(.F.) 
		If GetMV("MV_DOCSEQ") <> cDocSeq 
		   PutMV("MV_DOCSEQ",cDocSeq) 
		EndIf
		
		//Begin Transaction

		cLocal   := "E0"
		aTransf[nx][6] := "E0"
		cSubPad  := cSPadrao
	
  		lTransfSBF := AtuSBF(aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][9],aTransf[nx][5],aTransf[nx][7],cTpConv,nFtConv)
  		
		DbSelectArea("SB2")
		DbSetOrder(2)
		DbSeek(xFilial("SB2")+aTransf[nx][4]+aTransf[nx][1])
		nSldAtu := SB2->B2_QATU		
		nSldAlt := nSldAtu - aTransf[nx][11]
		
		nSld2 := 0
		If !Empty(cTpConv)
			If cTpConv = "M"
				nSld2 := nSldAlt * nFtConv
			Else
				nSld2 := nSldAlt / nFtConv
			EndIf
		EndIf
		
		RecLock("SB2",.F.)
			SB2->B2_QATU    := nSldAlt
			SB2->B2_QTSEGUM := nSld2 
		MsUnlock()
		
		//Armazem Destino
		DbSelectArea("SB2")
		DbSetOrder(2)
		DbSeek(xFilial("SB2")+aTransf[nx][6]+aTransf[nx][1])
		nSldAtu := SB2->B2_QATU		
		nSldAlt := nSldAtu + aTransf[nx][11]
		
		nSld2 := 0
		If !Empty(cTpConv)
			If cTpConv = "M"
				nSld2 := nSldAlt * nFtConv
			Else
				nSld2 := nSldAlt / nFtConv
			EndIf
		EndIf
		
		RecLock("SB2",.F.)
			SB2->B2_QATU := nSldAlt
			SB2->B2_QTSEGUM := nSld2
		MsUnlock()

		lTransfSB8 := AtuSB8(cDoc,aTransf[nx][1],aTransf[nx][4],aTransf[nx][6],aTransf[nx][8],cSubPad,aTransf[nx][11],aTransf[nx][9],cTpConv,nFtConv)
		
		nQtd2 := 0
		If !Empty(cTpConv)
			If cTpConv = "M"
				nQtd2 := aTransf[nx][11] * nFtConv
			Else
				nQtd2 := aTransf[nx][11] / nFtConv
			EndIf
		EndIf
		
		//Grava SD3 Transferencia
		//Requisicao
		DbSelectArea("SD3")
		RecLock("SD3",.T.)
			SD3->D3_FILIAL  := xFilial("SD3")
			SD3->D3_TM      := "999"
			SD3->D3_COD     := aTransf[nx][1]
			SD3->D3_UM      := aTransf[nx][3]
			SD3->D3_QUANT   := aTransf[nx][11]
			SD3->D3_QTSEGUM := nQtd2
			SD3->D3_SEGUM   := cSegUm
			SD3->D3_CF      := "RE4"
			SD3->D3_CONTA   := SB1->B1_CONTA
			SD3->D3_LOCAL   := aTransf[nx][4]
			SD3->D3_DOC     := cDoc
			SD3->D3_EMISSAO := DDATABASE
			SD3->D3_GRUPO   := SB1->B1_GRUPO
			SD3->D3_CUSTO1  := SB2->B2_CM1
			SD3->D3_NUMSEQ  := cDocseq
			SD3->D3_TIPO    := SB1->B1_TIPO
			SD3->D3_USUARIO := UsrRetName(__cUserID)
			SD3->D3_CHAVE   := "E0"
			SD3->D3_LOTECTL := aTransf[nx][8]
			SD3->D3_NUMLOTE := aTransf[nx][9]
			SD3->D3_DTVALID := dDtValid
			SD3->D3_LOCALIZ := aTransf[nx][5]
		MsUnlock()
		
		//Devolucao
		RecLock("SD3",.T.)
			SD3->D3_FILIAL  := xFilial("SD3")
			SD3->D3_TM      := "499"
			SD3->D3_COD     := aTransf[nx][1]
			SD3->D3_UM      := aTransf[nx][3]
			SD3->D3_QUANT   := aTransf[nx][11]
			SD3->D3_QTSEGUM := nQtd2
			SD3->D3_SEGUM   := cSegUm
			SD3->D3_CF      := "DE4"
			SD3->D3_CONTA   := SB1->B1_CONTA
			SD3->D3_LOCAL   := cLocal
			SD3->D3_DOC     := cDoc
			SD3->D3_EMISSAO := DDATABASE
			SD3->D3_GRUPO   := SB1->B1_GRUPO
			SD3->D3_CUSTO1  := SB2->B2_CM1
			SD3->D3_NUMSEQ  := cDocSeq
			SD3->D3_TIPO    := SB1->B1_TIPO
			SD3->D3_USUARIO := UsrRetName(__cUserID)
			SD3->D3_CHAVE   := "E9"
			SD3->D3_LOTECTL := aTransf[nx][8]
			SD3->D3_NUMLOTE := cSubPad
			SD3->D3_DTVALID := dDtValid
			SD3->D3_LOCALIZ := aTransf[nx][7]
		MsUnlock()
		
		cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
		ConfirmSX8()
		
		DbSelectArea("SDB")
		RecLock("SDB",.T.)
			SDB->DB_FILIAL  := xFilial("SDB")
			SDB->DB_ITEM    := "0001"
			SDB->DB_PRODUTO := aTransf[nx][1]
			SDB->DB_LOCAL   := aTransf[nx][4]
			SDB->DB_LOCALIZ := aTransf[nx][5]
			SDB->DB_DOC     := cDoc
			SDB->DB_TM      := "999"
			SDB->DB_ORIGEM  := "SD3"
			SDB->DB_QUANT   := aTransf[nx][11]
			SDB->DB_QTSEGUM := nQtd2
			SDB->DB_DATA    := DDATABASE
			SDB->DB_LOTECTL := aTransf[nx][8]
			SDB->DB_NUMLOTE := aTransf[nx][9]
			SDB->DB_NUMSEQ  := cDocseq
			SDB->DB_TIPO    := "M"
			SDB->DB_SERVIC  := "999"
			SDB->DB_ATIVID  := "ZZZ"
			SDB->DB_HRINI   := SubStr(Time(),1,5)
			SDB->DB_ATUEST  := "S"
			SDB->DB_STATUS  := "M"
			SDB->DB_ORDATIV := "ZZ"
			SDB->DB_IDOPERA := cNumIDOper 
		MsUnlock()
		
		cNumIDOper := GetSx8Num('SDB','DB_IDOPERA')
		ConfirmSX8()
		
		RecLock("SDB",.T.)
			SDB->DB_FILIAL  := xFilial("SDB")
			SDB->DB_ITEM    := "0001"
			SDB->DB_PRODUTO := aTransf[nx][1]
			SDB->DB_LOCAL   := aTransf[nx][6]
			SDB->DB_LOCALIZ := aTransf[nx][7]
			SDB->DB_DOC     := cDoc
			SDB->DB_TM      := "499"
			SDB->DB_ORIGEM  := "SD3"
			SDB->DB_QUANT   := aTransf[nx][11]
			SDB->DB_QTSEGUM := nQtd2
			SDB->DB_DATA    := DDATABASE
			SDB->DB_LOTECTL := aTransf[nx][8]
			SDB->DB_NUMLOTE := cSubPad
			SDB->DB_NUMSEQ  := cDocseq
			SDB->DB_TIPO    := "M"
			SDB->DB_SERVIC  := "499"
			SDB->DB_ATIVID  := "ZZZ"
			SDB->DB_HRINI   := SubStr(Time(),1,5)
			SDB->DB_ATUEST  := "S"
			SDB->DB_STATUS  := "M"
			SDB->DB_ORDATIV := "ZZ"
			SDB->DB_IDOPERA := cNumIDOper 
		MsUnlock()
			
		DbSelectArea("SD5")
		RecLock("SD5",.T.)
			SD5->D5_FILIAL  := xFilial("SD5")
			SD5->D5_PRODUTO := aTransf[nx][1]
			SD5->D5_LOCAL   := aTransf[nx][4]
			SD5->D5_DOC     := cDoc
			SD5->D5_DATA    := DDATABASE
			SD5->D5_ORIGLAN := "999"
			SD5->D5_NUMSEQ  := cDocseq
			SD5->D5_QUANT   := aTransf[nx][11]
			SD5->D5_QTSEGUM := nQtd2
			SD5->D5_LOTECTL := aTransf[nx][8]
			SD5->D5_NUMLOTE := aTransf[nx][9]
			SD5->D5_DTVALID := dDtValid
		MsUnlock()
		
		RecLock("SD5",.T.)
			SD5->D5_FILIAL  := xFilial("SD5")
			SD5->D5_PRODUTO := aTransf[nx][1]
			SD5->D5_LOCAL   := aTransf[nx][6]
			SD5->D5_DOC     := cDoc
			SD5->D5_DATA    := DDATABASE
			SD5->D5_ORIGLAN := "499"
			SD5->D5_NUMSEQ  := cDocseq
			SD5->D5_QUANT   := aTransf[nx][11]
			SD5->D5_QTSEGUM := nQtd2
			SD5->D5_LOTECTL := aTransf[nx][8]
			SD5->D5_NUMLOTE := cSubPad
			SD5->D5_DTVALID := dDtValid
		MsUnlock()
		
		//End Transaction
		
	Next
	
	cPrxTrn := SOMA1(cTrnDAP,6)
Else
	MsgAlert("SBF - Dados n�o encontrados.","Aten��o")		
EndIf

Return

Static Function AtuSBF(cPrdSBF,cAOriSBF,cADesSBF,cLotSBF,cSubSBF,nQtdSBF,cSubOri,cESBFOrig,cESBFDest,cFator,nFator)
Local cQuery  := ""
Local nRecNo  := 0
Local nSldSBF := 0
Local lRet    := .T.

//Atualiza Saldo do amazem de origem
cQuery := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SBF")+" SBF "
cQuery += CRLF + "where BF_FILIAL  = '"+xFilial("SBF")+"' "
cQuery += CRLF + "and   BF_PRODUTO = '"+cPrdSBF+"' "
cQuery += CRLF + "and   BF_LOCAL   = '"+cAOriSBF+"' "
cQuery += CRLF + "and   BF_LOTECTL = '"+cLotSBF+"' "
cQuery += CRLF + "and   BF_NUMLOTE = '"+cSubOri+"' "
cQuery += CRLF + "and   BF_LOCALIZ = '"+cESBFOrig+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSBF_Orig.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSBF", .F., .T.)

DbSelectArea("TMPSBF")

If TMPSBF->(!EOF())
	nRecNo  := TMPSBF->R_E_C_N_O_
	nSldSBF := TMPSBF->BF_QUANT - nQtdSBF
	//nSldSBF := IF(nSldSBF < 0,0,nSldSBF)
	
	nSldSBF2 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSBF2 := nSldSBF * nFator
		Else
			nSldSBF2 := nSldSBF / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SBF")+" SET BF_QUANT = "+AllTrim(Str(nSldSBF))+" , BF_QTSEGUM = "+AllTrim(Str(nSldSBF2))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSBF_Orig_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
	
	If nSldSBF = 0
		cUpdate := "UPDATE "+RetSqlName("SBF")+" SET D_E_L_E_T_ = '*', R_E_C_D_E_L_ = R_E_C_N_O_, BF_QUANT = 0 , BF_QTSEGUM = 0 WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
		MemoWrite("HFATA010_AtuSBF_Orig_Delete.txt",cUpdate)
		nStatus := TCSQLEXEC(cUpdate)
	EndIf
	
EndIf

TMPSBF->(DbCloseArea())

//Atualiza Saldo do armazem de destino
cQuery := "select BF_PRODUTO,BF_LOCAL,BF_QUANT,BF_LOTECTL,BF_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SBF")+" SBF "
cQuery += CRLF + "where BF_FILIAL  = '"+xFilial("SBF")+"' "
cQuery += CRLF + "and   BF_PRODUTO = '"+cPrdSBF+"' "
cQuery += CRLF + "and   BF_LOCAL   = '"+cADesSBF+"' "
cQuery += CRLF + "and   BF_LOTECTL = '"+cLotSBF+"' "
cQuery += CRLF + "and   BF_NUMLOTE = '"+cSubSBF+"' "
cQuery += CRLF + "and   BF_LOCALIZ = '"+cESBFDest+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSBF_Dest.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSBF", .F., .T.)

DbSelectArea("TMPSBF")
DbGoTop()

If TMPSBF->(!EOF())
	nRecNo  := TMPSBF->R_E_C_N_O_
	nSldSBF := TMPSBF->BF_QUANT + nQtdSBF
	
	nSldSBF2 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSBF2 := nSldSBF * nFator
		Else
			nSldSBF2 := nSldSBF / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SBF")+" SET BF_QUANT = "+AllTrim(Str(nSldSBF))+" , BF_QTSEGUM = "+AllTrim(Str(nSldSBF2))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSBF_Dest_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
Else
	nSldSBF2 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSBF2 := nQtdSBF * nFator
		Else
			nSldSBF2 := nQtdSBF / nFator
		EndIf
	EndIf
	
	RecLock("SBF",.T.)
		SBF->BF_FILIAL  := xFilial("SBF")
		SBF->BF_PRODUTO := cPrdSBF
		SBF->BF_LOCAL   := cADesSBF
		SBF->BF_LOCALIZ := cESBFDest
		SBF->BF_LOTECTL := cLotSBF
		SBF->BF_NUMLOTE := cSubSBF
		SBF->BF_QUANT   := nQtdSBF
		SBF->BF_QTSEGUM := nSldSBF2
	MsUnlock()
EndIf

TMPSBF->(DbCloseArea())

Return lRet

Static Function AtuSB8(cDocument,cPrdSB8,cAOriSB8,cADesSB8,cLotSB8,cSubSB8,nQtdSB8,cSubOri,cFator,nFator)
Local cQuery  := ""
Local nRecNo  := 0
Local nSldSB8 := 0
Local lRet    := .T.

//Atualiza Saldo do amazem de origem
cQuery := "select B8_PRODUTO,B8_LOCAL,B8_SALDO,B8_LOTECTL,B8_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SB8")+" SB8 "
cQuery += CRLF + "where B8_FILIAL  = '"+xFilial("SB8")+"' "
cQuery += CRLF + "and   B8_PRODUTO = '"+cPrdSB8+"' "
cQuery += CRLF + "and   B8_LOCAL   = '"+cAOriSB8+"' "
cQuery += CRLF + "and   B8_LOTECTL = '"+cLotSB8+"' "
cQuery += CRLF + "and   B8_NUMLOTE = '"+cSubOri+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSB8_Orig.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSB8", .F., .T.)

DbSelectArea("TMPSB8")

If TMPSB8->(!EOF())
	nRecNo  := TMPSB8->R_E_C_N_O_
	nSldSB8 := TMPSB8->B8_SALDO - nQtdSB8
	//nSldSB8 := IF(nSldSB8 < 0,0,nSldSB8)
	
	nSldSB82 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSB82 := nSldSB8 * nFator
		Else
			nSldSB82 := nSldSB8 / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SB8")+" SET B8_DOC = '"+cDocument+"' , B8_SALDO = "+AllTrim(Str(nSldSB8))+" , B8_SALDO2 = "+AllTrim(Str(nSldSB82))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSB8_Dest_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
EndIf

TMPSB8->(DbCloseArea())

//Atualiza Saldo do armazem de destino
cQuery := "select B8_PRODUTO,B8_LOCAL,B8_SALDO,B8_LOTECTL,B8_NUMLOTE,R_E_C_N_O_ "
cQuery += CRLF + "from "+RetSqlName("SB8")+" SB8 "
cQuery += CRLF + "where B8_FILIAL  = '"+xFilial("SB8")+"' "
cQuery += CRLF + "and   B8_PRODUTO = '"+cPrdSB8+"' "
cQuery += CRLF + "and   B8_LOCAL   = '"+cADesSB8+"' "
cQuery += CRLF + "and   B8_LOTECTL = '"+cLotSB8+"' "
cQuery += CRLF + "and   B8_NUMLOTE = '"+cSubSB8+"' "
cQuery += CRLF + "and   D_E_L_E_T_ = '' " 

MemoWrite("HFATA010_AtuSB8_Dest.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSB8", .F., .T.)

DbSelectArea("TMPSB8")

If TMPSB8->(!EOF())
	nRecNo  := TMPSB8->R_E_C_N_O_
	nSldSB8 := TMPSB8->B8_SALDO + nQtdSB8
	
	nSldSB82 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSB82 := nSldSB8 * nFator
		Else
			nSldSB82 := nSldSB8 / nFator
		EndIf
	EndIf
	
	cUpdate := "UPDATE "+RetSqlName("SB8")+" SET B8_DOC = '"+cDocument+"' , B8_SALDO = "+AllTrim(Str(nSldSB8))+" , B8_SALDO2 = "+AllTrim(Str(nSldSB82))+" , B8_QTDORI = "+AllTrim(Str(nSldSB8))+" , B8_QTDORI2 = "+AllTrim(Str(nSldSB82))+" WHERE R_E_C_N_O_ = "+AllTrim(Str(nRecNo))
	MemoWrite("HFATA010_AtuSB8_Dest_Update.txt",cUpdate)
	nStatus := TCSQLEXEC(cUpdate)
Else
	nSldSB82 := 0
	If !Empty(cFator)
		If cFator = "M"
			nSldSB82 := nQtdSB8 * nFator
		Else
			nSldSB82 := nQtdSB8 / nFator
		EndIf
	EndIf
	
	RecLock("SB8",.T.)
		SB8->B8_FILIAL  := xFilial("SB8")
		SB8->B8_QTDORI  := nQtdSB8
		SB8->B8_QTDORI2 := nSldSB82
		SB8->B8_PRODUTO := cPrdSB8
		SB8->B8_LOCAL   := cADesSB8
		SB8->B8_DATA    := DDATABASE
		SB8->B8_DTVALID := StoD('20491231')
		SB8->B8_SALDO   := nQtdSB8
		SB8->B8_SALDO2  := nSldSB82
		SB8->B8_ORIGLAN := 'MI' //Movimento Interno
		SB8->B8_LOTECTL := cLotSB8
		SB8->B8_NUMLOTE := cSubSB8
		SB8->B8_DOC     := cDocument
		SB8->B8_DFABRIC := DDATABASE
	MsUnlock()
EndIf

TMPSB8->(DbCloseArea())

Return lRet