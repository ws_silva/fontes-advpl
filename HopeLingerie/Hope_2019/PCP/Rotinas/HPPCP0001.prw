#include "protheus.ch"
#INCLUDE "TBICONN.CH" 
#include "TOTVS.CH"
#INCLUDE "rwmake.ch"
#INCLUDE "TOPCONN.CH"

/*/{Protheus.doc} HPPCP0001
Apontamento de Produ��o - PCP mod 2
@author Weskley Silva
@since 21/06/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPPCP0001()


	Processa( {|| U_Import2() }, "Aguarde...", "Apontamento de Produ��o",.F.)
	

User Function Import2()

	Local aVetor := {}
	Local nQtd 	   := 0
	Local cProd	   := ""
	Local cArmazem     := ""
	Local dEmissao     := ""
	Local cOP := ""
	Local cOP1 := ""
	Local cFase := ""
	//Local cQuery := ""
	Local cQuery1 := " "
	Local aArea := GetArea()
	Local aRet := {}
	Local aParamBox := {}


	PRIVATE lMsErroAuto := .F.

	aAdd(aParamBox,{6,"Buscar arquivo",Space(50),"","","",50,.F.,"Todos os arquivos (*.txt) |*.txt"})
	
	If !ParamBox(aParamBox,"Par�metros...",@aRet)
	      MsgInfo("Cancelado pelo usu�rio.","HOPE")
	      Return
	Endif
	
	_cArq := @aRet[1]

	If !File(_cArq)
		MsgStop("O arquivo " +_cArq + " n�o foi encontrado.","ATENCAO")
		Return
	EndIf

	FT_FUse( Alltrim(_cArq) )
	FT_FGoTop()

	ProcRegua(RecCount())

	While !(FT_FEof())
	
	
		cLinha := FT_FREADLN()	
		aLinha := StrTokArr(cLinha,";")
		cProd := PADR(aLinha[2],15)
		cOP := Alltrim(aLinha[1])
		nQtd := val(aLinha[3])
		//cFase := Alltrim(aLinha[3])
		cOP1 := LEFT(cOP,6)
	
		iF nQtd <= 0
			FT_FSkip()
			LOOP		
		Endif
		
			dbSelectArea("SB1")
			dbSetOrder(1)
			if !dbSeek(xFilial("SB1")+cProd)
				Alert("Produto ("+cProd+") N�o Cadastrado.")
				FT_FSkip()
				LOOP				
			endif
			
					
		cProd := SB1->B1_COD
		dEmissao := Date()
		
		IF SELECT("SC2") > 0
     		SC2->(dbclosearea())	
     	ENDIF
     	
 		
		cQuery1 := " SELECT C2_LOCAL FROM "+RetSqlName("SC2")+" WHERE C2_NUM = '"+cOP1+"' AND C2_PRODUTO = '"+cProd+"'  AND D_E_L_E_T_ = '' "
		
		TCQUERY cQuery1 NEW ALIAS SC2
		
		cArmazem := SC2->C2_LOCAL
		
			
     	//IF SELECT("SH6") > 0
     	//	SH6->(dbclosearea())	
     	//ENDIF	
     		
     			
		//cQuery := " SELECT MAX(H6_OPERAC) AS FASE, H6_LOCAL FROM SH6010 WHERE H6_OP = '"+cOp+"' AND H6_PRODUTO = '"+cProd+"' AND D_E_L_E_T_ = '' GROUP BY H6_LOCAL "
		
		//TCQUERY cQuery NEW ALIAS SH6
		
		
	//	cFase1 := SH6->FASE
		
		
	//	if cFase1 == cFase
		//	FT_FSkip()
			//LOOP
		//endif
		
		/*if cFase == "15" 
			cFase = "20"
		elseif cFase == "20"
			cFase = "30"
		elseif cFase == "30"
			cFase = "40"
		else	
			cFase = "40"
		endif
		
		*/					
		//SH6->(dbclosearea())	
		SC2->(dbclosearea())
				
				Begin Transaction
					
				   IncProc()
				   lMsErroAuto := .F.
				   aVetor := {} 		
				   Aadd(aVetor,{"H6_FILIAL"	 ,xFilial("SH6") ,NIL})
				   Aadd(aVetor,{"H6_OP"	     ,cOP            ,NIL})
				   Aadd(aVetor,{"H6_PRODUTO" ,cProd          ,NIL})
				   Aadd(aVetor,{"H6_OPERAC" ,'60'  			 ,NIL})
				   Aadd(aVetor,{"H6_RECURSO" ," "            ,NIL})
				   Aadd(aVetor,{"H6_DTAPONT" ,dEmissao       ,NIL})
				   Aadd(aVetor,{"H6_DATAINI" ,dEmissao       ,NIL})
				   Aadd(aVetor,{"H6_HORAINI","08:00"         ,NIL})
				   Aadd(aVetor,{"H6_DATAFIN",dEmissao        ,NIL})
				   Aadd(aVetor,{"H6_HORAFIN","18:00"         ,NIL})
				   Aadd(aVetor,{"H6_PT"     ,'P'             ,NIL})
				   Aadd(aVetor,{"H6_LOCAL"  ,alltrim(cArmazem),NIL})
				   Aadd(aVetor,{"H6_QTDPROD",nQtd            ,NIL})
				   Aadd(aVetor,{"H6_QTDPERD",0               ,NIL})
	        
				   	
				   	MSExecAuto({|x| mata681(x)},aVetor,3)		
				   	
				   	If !lMsErroAuto		
				   		ConOut("Incluido com sucesso! "+cOP)		
				   	Else		
					   	ConOut("Erro na inclusao!"+cLinha)	
				   		AutoGrLog("Linha:"+cLinha+" -- Valor:")
						MostraErro()
				   	EndIf	
				   	
				   	ConOut("Fim  : "+cLinha)	         
				   	
			   	End Transaction       			   	
		FT_FSkip()
	End Do
 RestArea(aArea)	
Return Nil
