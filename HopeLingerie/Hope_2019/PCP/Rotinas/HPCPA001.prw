#INCLUDE "protheus.ch" 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HPCPA001  �Autor  �Bruno Parreira      � Data �  03/11/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa para geracao dos pedidos de venda de remessa para  ���
���          �beneficiamento nas faccoes.                                 ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HPCPA001()

	local cVldAlt	:= ".T." 
	local cVldExc	:= ".T." 
	local cAliasd
	
	cAlias	:= "SC2"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Gera��o de Pedidos de Beneficiamento"

	aRotina := {;
		{ "Pesquisar" 			, "AxPesqui"			, 0, 1},;
		{ "Ger.Pedido"			, "U_HLib(SC2->C2_NUM)"	, 0, 3}}

	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)
	
return

USer Function HLib(_op)

Local cPerg   := "HPCPA001"
Local cQuery  := ""
Local cGrpExc := AllTrim(SuperGetMv("MV_XGRPEXC",.F.,"'MP90','BN00','MP05'")) //Grupos de produto que serao excluidos do pedido de beneficiamento
Local cNroPed := ""
Local nGerado := 0
Local nErro   := 0
//Local cTabPrc := AllTrim(SuperGetMv("MV_XTBPRBN",.F.,"303")) //Tabela de preco para pedidos de beneficiamento
Local nPrcTab := 0 
Local cTpOper := AllTrim(SuperGetMv("MV_XTPOPBN",.F.,"04")) //Tipo de Operacao para Pedido de Benefiamento
Local cTES    := ""
Local cOperac := AllTrim(SuperGetMv("MV_XOPERBN",.F.,"20")) //Operacao que deve ser apontada para permitir nota de beneficiamento
Local cOPErro := ""
Local aErro   := {}
Local cPoliti := AllTrim(SuperGetMv("MV_XPOLIBN",.F.,"099")) //Politica Comercial pedido de beneficiamento
Local cTpPedi := AllTrim(SuperGetMv("MV_XTPPDBN",.F.,"070")) //Tipo de pedido para pedido de beneficiamento
Local cCondPg := AllTrim(SuperGetMv("MV_XCONDBN",.F.,"B01")) //Condicao de pagamento padrao pedido de beneficiamento
Local dEntreg := DaySum(DDATABASE,Val(SuperGetMv("MV_XENTRBN",.F.,"15"))) //Dias da data de entrega do pedido
Local cNature := AllTrim(SuperGetMv("MV_XNATUBN",.F.,"21617801")) //Natureza padrao caso nao tenha amarrada ao fornecedor
Local nx      := 0

Private lMsErroAuto := .F.

Private cLog := ""

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
                
If Pergunte(cPerg)
	If MsgYesNo("Confirma atualiza��o do Fornecedor?","Confirma��o")
		_QRY := "UPDATE "+RETSQLNAME("SC2")+" SET C2_YFORNEC = '"+MV_PAR01+"' ,C2_YLOJAFO = '"+MV_PAR02+"', C2_TPPR = 'E' WHERE C2_NUM = '"+_OP+"' AND D_E_L_E_T_ = ''"
		TCSQLEXEC(_QRY)
		
		_qry := "UPDATE "+RetSqlName("SC2")+" set C2_RECURSO = '"+MV_PAR03+"' from "+RetSqlName("SC2")+" SC2 " 
		_qry += "inner join "+RetSqlName("SB1")+" SB1 on SB1.D_E_L_E_T_ = '' and B1_COD = C2_PRODUTO "
		_qry += "where SC2.D_E_L_E_T_ = '' and B1_TIPO in ('PI','PF') and C2_NUM = '"+_OP+"' "
		TcSqlExec(_qry)
	Endif
EndIf

GravaLog("In�cio da rotina")

cQuery := "select D4_COD,SUBSTRING(D4_OP,1,11) AS D4_OP, D4_LOCAL,C2_YFORNEC,C2_YLOJAFO,SUM(D4_QTDEORI) AS D4_QTDEORI,SUM(D4_QUANT) AS D4_QUANT from ( "
cQuery += CRLF + "select D4_COD,D4_OP,D4_LOCAL,D4_DATA,D4_QTDEORI,D4_QUANT,D4_LOTECTL,D4_NUMLOTE,D4_SITUACA,C2_YFORNEC,C2_YLOJAFO,C2_NUM,C2_PRODUTO,C2_ITEM "
cQuery += CRLF + "from "+RetSqlName("SD4")+" SD4 "
cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 " 
cQuery += CRLF + "on B1_COD = D4_COD "  
cQuery += CRLF + "and B1_GRUPO NOT IN ("+cGrpExc+") "
cQuery += CRLF + "and SB1.D_E_L_E_T_   = '' "
cQuery += CRLF + "inner join "+RetSqlName("SC2")+" SC2 " 
cQuery += CRLF + "on C2_NUM      = SUBSTRING(D4_OP,1,6) "
cQuery += CRLF + "and C2_ITEM    = SUBSTRING(D4_OP,7,2) "
cQuery += CRLF + "and C2_SEQUEN  = SUBSTRING(D4_OP,9,3) "
cQuery += CRLF + "and C2_ITEMGRD = SUBSTRING(D4_OP,12,3) "
cQuery += CRLF + "and SC2.D_E_L_E_T_   = '' "
cQuery += CRLF + "LEFT join "+RetSqlName("SG1")+" SG1 "
cQuery += CRLF + "on G1_COD = C2_PRODUTO "
cQuery += CRLF + "and G1_COMP = D4_COD AND G1_COD+''+G1_REVINI NOT IN (SELECT G5_PRODUTO+''+G5_REVISAO FROM SG5010 WHERE D_E_L_E_T_='' AND G5_STATUS=2) "
cQuery += CRLF + "and SG1.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SH6")+" SH6 "
cQuery += CRLF + "on H6_OP = D4_OP "
cQuery += CRLF + "and H6_PRODUTO = C2_PRODUTO "
cQuery += CRLF + "and H6_OPERAC >= '"+cOperac+"' "
cQuery += CRLF + "and SH6.D_E_L_E_T_   = '' "
cQuery += CRLF + "where SD4.D_E_L_E_T_ = '' "
cQuery += CRLF + "and D4_FILIAL  = '"+xFilial("SD4")+"' "
cQuery += CRLF + "and D4_QUANT   > 0 "
cQuery += CRLF + "and D4_OP      like '"+_op+"%' ) X "//between '"+mv_par01+"' and '"+mv_par02+"ZZZZZZZ' ) X "
cQuery += CRLF + "group by D4_COD,SUBSTRING(D4_OP,1,11), D4_LOCAL,C2_YFORNEC,C2_YLOJAFO "
cQuery += CRLF + "order by D4_OP,D4_COD "
    
MemoWrite("HPCPA001.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)       

GravaLog("Parametros: OP de: "+mv_par01+" OP ate: "+mv_par02)

DbSelectArea("TRB")
If TRB->(!EOF())
	
	If !MsgYesNo("Confirma gera��o dos pedidos?","Confirma��o")  
		TRB->(DbCloseArea())
		GravaLog("Cancelado pelo usu�rio.")
		Return
	EndIf

	cOP := ""

	While TRB->(!EOF())
    	    
	    If Empty(TRB->C2_YFORNEC)
	    	GravaLog("OP sem fornecedor amarrado. "+TRB->D4_OP)
	    	TRB->(DbSkip())
	    	Loop
	    EndIf

		 IF SELECT("TMP") > 0
			TMP->(DbCloseArea())
		 EndIf

		_cqry := " SELECT C5_XNUMOP AS OP FROM "+RetSqlName("SC5")+" WHERE C5_XNUMOP = '"+TRB->D4_OP+"' AND D_E_L_E_T_ = ''  "

		DbUseArea( .T., 'TOPCONN', TCGENQRY(,,_cqry),"TMP", .F., .T.) 

		IF !Empty(Alltrim(TMP->OP)) 
			MsgAlert("J� existe um pedido de beneficiamento gerado para essa OP "+TRB->D4_OP+" " ,"Hope")
			return
		Endif
	    
	    DbSelectArea("SB1")
    	DbSeek(xFilial("SB1")+TRB->D4_COD)
	   
	    nPrcTab := 0
	    
	    If SB1->B1_CUSTD > 0
	    	nPrcTab := SB1->B1_CUSTD
	    Else
	    	aAdd(aErro,{TRB->D4_OP,TRB->D4_COD,SB1->B1_DESC})
			cOPErro := TRB->D4_OP
			TRB->(DbSkip())
	    	Loop
	    EndIf
		
		DbSelectArea("SA2")
		DbSetOrder(1)
		DbSeek(xFilial("SA2")+TRB->C2_YFORNEC+TRB->C2_YLOJAFO)
	    
		If cOP <> TRB->D4_OP
			If !Empty(cNroPed)
				RollBackSX8()
			EndIf
			
			DbSelectArea("SC5")
			cNroPed := GetSX8Num("SC5","C5_NUM",,1)
		    
		    GravaLog("Montando array para a OP: "+TRB->D4_OP+" Fornecedor: "+TRB->C2_YFORNEC+"/"+TRB->C2_YLOJAFO)

		    aCabec := {}
			//aadd(aCabec,{"C5_NUM"    ,cNroPed        ,Nil})		
			aadd(aCabec,{"C5_TIPO"   ,"B"            ,Nil})		
			aadd(aCabec,{"C5_CLIENTE",TRB->C2_YFORNEC,Nil})
			aadd(aCabec,{"C5_LOJACLI",TRB->C2_YLOJAFO,Nil})
			aadd(aCabec,{"C5_LOJAENT",TRB->C2_YLOJAFO,Nil})
			aadd(aCabec,{"C5_POLCOM" ,cPoliti        ,Nil})
			aadd(aCabec,{"C5_TPPED"  ,cTpPedi        ,Nil}) 
			aadd(aCabec,{"C5_CONDPAG",cCondPg        ,Nil})		
			aadd(aCabec,{"C5_NATUREZ",SA2->A2_NATUREZ,Nil})
			aadd(aCabec,{"C5_XNUMOP" ,TRB->D4_OP     ,Nil})
			aadd(aCabec,{"C5_XBLQ"   ,"L"            ,Nil})
			                                                                                 
			aItens := {}
			
			cIt := "00"
			
			cOP := TRB->D4_OP
		EndIf		
        
		cIt := SOMA1(cIt)

    	cTES  := MaTesInt(2,cTpOper,TRB->C2_YFORNEC,TRB->C2_YLOJAFO,"F",SB1->B1_COD,)  
		
		aLinha := {}			
		aadd(aLinha,{"C6_ITEM"   ,cIt          ,Nil})			
		aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD  ,Nil})			
		aadd(aLinha,{"C6_QTDVEN" ,TRB->D4_QUANT,Nil})			
		aadd(aLinha,{"C6_PRCVEN" ,nPrcTab      ,Nil})			
		aadd(aLinha,{"C6_PRUNIT" ,nPrcTab      ,Nil})			
		aadd(aLinha,{"C6_VALOR"  ,ROUND(TRB->D4_QUANT*nPrcTab,2),Nil})
		aadd(aLinha,{"C6_OPER"   ,cTpOper      ,Nil})			
		aadd(aLinha,{"C6_TES"    ,cTES         ,Nil})
		aadd(aLinha,{"C6_ENTREG" ,dEntreg      ,Nil})
		aadd(aLinha,{"C6_LOCAL"  ,TRB->D4_LOCAL,Nil})	
		aadd(aItens,aLinha)		
        
        GravaLog("OP: "+TRB->D4_OP+" Item: "+SB1->B1_COD)
        
		TRB->(DbSkip())
		
		If cOP <> TRB->D4_OP
			If cOp <> cOPErro
				MATA410(aCabec,aItens,3)		
				
				If !lMsErroAuto
					//MsgInfo("Pedido gerado com sucesso!","Aviso")
					GravaLog("Pedido "+SC5->C5_NUM+" inclu�do com sucesso para a OP "+TRB->D4_OP)	 
					DbSelectArea("SC5")
					ConfirmSX8()
					nGerado++
					DbSelectArea("SC2")
					DbSetOrder(1)
					If DbSeek(xFilial("SC2")+cOP)
						While SC2->(!EOF()) .And. SC2->C2_NUM+SC2->C2_ITEM+SC2->C2_SEQUEN = cOP
							RecLock("SC2",.F.)
							SC2->C2_XPEDBEN := SC5->C5_NUM//cNroPed 
							MsUnlock()
							SC2->(DbSkip())
						EndDo
					EndIf
				Else
					GravaLog("Ocorreu erro na inclusao do pedido "+SC5->C5_NUM+" para a OP "+TRB->D4_OP)
					MostraErro()
					RollBackSX8()
					nErro++
				EndIf
				lMsErroAuto := .F.
				
				cNroPed := ""	
			EndIf
			cOPErro := ""
		EndIf	
    EndDo 
    
    If Len(aErro) > 0
	    cMsgErro := "As OPs e produtos abaixo n�o tem custo standard cadastrado:"
	    For nx := 1 to Len(aErro)
	    	cMsgErro += "OP: "+aErro[nx][1]+" Prod.:"+aErro[nx][2]+" - "+aErro[nx][3]
	    Next
	    MsgAlert(cMsgErro,"Aten��o")
    EndIf 
    
    MsgInfo("Pedidos gerados: "+AllTrim(Str(nGerado))+". Pedidos com erro:"+AllTrim(Str(nErro))+"."+CRLF+"Para mais detalhes, verifique o log.","Aviso")
    GravaLog("Pedidos gerados: "+AllTrim(Str(nGerado))+". Pedidos com erro:"+AllTrim(Str(nErro))+".")
Else
	MsgAlert("OP n�o tem a opera��o 20 apontada.","A T E N � � O")
	GravaLog("OP n�o tem a opera��o 20 apontada.")
EndIf 

TRB->(DbCloseArea())

GravaLog("Fim do processamento.")

MemoWrite("\log_benef\PEDBEN_"+cDtTime+".log",cLog)

Return

Static Function HSALDO(cProduto,cArmazem) 
Local nRet := 0

DbSelectArea("SB2")
DbSetOrder(1)
If DbSeek(xFilial("SB2")+cProduto+cArmazem)
	nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()	
EndIf

Return nRet

Static Function GravaLog(cMsg)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cMsg)
cLog += CRLF+cDtHora+": HPCPA001 - "+cMsg

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 03/11/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )
  
PutSx1(cPerg,"01","N�mero da OP de ? ","N�mero da OP de ? ","N�mero da OP de ? ","Mv_ch1",TAMSX3("D4_OP")[3],6,TAMSX3("D4_OP")[2],0,"G","","SC2","","N","Mv_par01","","","","","","","","","","","","","","","","",{"Informe o n�mero da OP de.         ",""},{""},{""},"")
PutSx1(cPerg,"02","N�mero da OP at� ?","N�mero da OP at� ?","N�mero da OP at� ?","Mv_ch2",TAMSX3("D4_OP")[3],6,TAMSX3("D4_OP")[2],0,"G","","SC2","","N","Mv_par02","","","","","","","","","","","","","","","","",{"Informe o n�mero da OP at�.        ",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)