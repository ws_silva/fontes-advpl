#include 'protheus.ch'
#include 'parmtype.ch'
#include 'rwmake.ch'
#include 'TBICONN.ch'
#include 'totvs.ch'

/*
Fonte: ExcOrdem (Exclus�o das ordens de produ��o via arquivo)
Autor: Weskley Silva
Data: 16/10/2017
*/

user function ExcOrdem()

	Processa( {|| U_Ordem()} , " Aguarde...", "Excluido as Ordens de Produ��o", .F.)

return

User Function Ordem()

Local ExpA1 := {}
Local cProd := ""
Local cItem := ""
Local cNum := ""
Local cFiliall := ""
Local cSeq := ""
Local aRet := {}
Local aParambox := {}
Local nOpc := 5

Private lMsErroAuto := .F.

aAdd(aParambox,{6,"Buscar o arquivo",Space(50),"","","",50,.F.,"Todos os arquivos (*.txt)|*.txt"})

IF !Parambox(aParamBox,"Parametros...",@aRet)
	Msginfo("Cancelado pelo usuario.","HOPE")
	return
Endif

_cArq := @aRet[1]

if !File(_cArq)
	MsgStop("O arquivo " +_cArq+ " n�o foi encontrado.","Aten��o")
	return
endif	

FT_FUse( Alltrim(_cArq))
FT_FGotop()

ProcRegua(RecCount())

While !(FT_FEof())

	cLinha := FT_FREADLN()
	aLinha := StrTokArr(cLinha,";")
	cFiliall := Alltrim(aLinha[1])
	cProd := PADR(aLinha[2],15)
	cNum := Alltrim(aLinha[3])
	cItem := Alltrim(aLinha[4])
	cSeq := Alltrim(aLinha[5])
	
	Begin Transaction 
	
	IncProc()
	lMsErroAuto := .F. 
	ExpA1 := {}
	
	ExpA1 := {	{'C2_FILIAL',    cFiliall, NIL},;
				{'C2_PRODUTO',   cProd,    NIL },;
				{'C2_NUM',       cNum,     NIL},;
				{'C2_ITEM',      cItem,    NIL},;
				{'C2_SEQUEN',    cSeq,     NIL}}	
				
	IF nOpc == 4 .or. nOpc == 5
		SC2->(DbSetOrder(1))// FILIAL+NUM+ITEM+SEQUEN+ITEMGRD
		SC2->(DbSeek(xFilial("SC2")+cNum+cItem+cSeq))
	endif
	
	msExecAuto({|x,y| Mata650(x,y)},ExpA1,nOpc)
	
	If !lMsErroAuto
		(ConOut("Sucesso! "))
	Else
		Conout("Erro!")
		//MostraErro()
	endif	

ConOut("Fim : "+Time())

End Transaction

FT_FSkip()

Enddo

Return NIl