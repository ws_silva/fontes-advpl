#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} RF013 
Controle de Produ��o - PCP
@author Weskley Silva
@since 12/06/2017
@version 2.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL001()


	Private oReport
	Private cPergCont	:= 'HPREL001' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 12 de Junho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'PCP', 'CONTROLE DE PRODUCAO ', cPergCont, {|oReport| ReportPrint( oReport ), 'CONTROLE DE PRODUCAO' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'CONTROLE DE PRODUCAO', { 'PCP', 'SH6','SB1','SBM','SG2','SC2'})
			
	TRCell():New( oSection1, 'FILIAL'		    	,'PCP', 		'FILIAL',							"@!"                        ,5)
	TRCell():New( oSection1, 'COD_PRODUTO'      	,'PCP', 		'COD_PRODUTO',		    			"@!"				        ,15)
	TRCell():New( oSection1, 'PRODUTO'          	,'PCP', 		'PRODUTO',              			"@!"						,20)
	TRCell():New( oSection1, 'QUANTIDADE'          	,'PCP', 		'QUANTIDADE',              			"@!"						,03)
	TRCell():New( oSection1, 'DESCANSO_TERCIDO' 	,'PCP', 		'DESCANSO_TERCIDO' ,	        	"@!"		                ,20)
	TRCell():New( oSection1, 'CORTE'	        	,'PCP', 		'CORTE' ,	        				"@!"		                ,20)
	TRCell():New( oSection1, 'SEPARACAO_AVIAMENTO' 	,'PCP', 		'SEPARACAO_AVIAMENTO' ,	    		"@!"		                ,20)
	TRCell():New( oSection1, 'POOL'	        		,'PCP', 		'POOL' ,	        				"@!"		                ,20)
	TRCell():New( oSection1, 'COSTURA_MONTAGEM'	    ,'PCP', 		'COSTURA_MONTAGEM' ,	       	 	"@!"		                ,20)
	TRCell():New( oSection1, 'COSTURA_HOPE_I_CEL'	,'PCP', 		'COSTURA_HOPE_I_CEL' ,	    		"@!"	                    ,20)
	TRCell():New( oSection1, 'RECEBIMENTO_FINALIZA'	,'PCP', 		'RECEBIMENTO_FINALIZA' ,	    	"@!"		                ,20)
	TRCell():New( oSection1, 'QUANTIDADE_PRODUZIDA'	,'PCP', 		'QUANTIDADE_PRODUZIDA' ,	    	"@!"		                ,03)
	TRCell():New( oSection1, 'DATA_APONTAMENTO'		,'PCP', 		'DATA_APONTAMENTO' ,    			"@!"	                    ,08)
	TRCell():New( oSection1, 'OPERACAO'		        ,'PCP', 		'OPERACAO',				   			"@!"						,15)

Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 12 de Junho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("PCP") > 0
		PCP->(dbCloseArea())
	Endif
      
    
	cQuery := " SELECT " 
	cQuery += " H6_FILIAL AS FILIAL, " 
	cQuery += " H6_PRODUTO AS COD_PRODUTO, "
	cQuery += " SBM010.BM_DESC AS PRODUTO, "
	cQuery += " SC2.C2_QUANT AS QUANTIDADE, "
	cQuery += " ISNULL([00],0) AS DESCANSO_TERCIDO, "
	cQuery += " ISNULL([05],0) AS CORTE, "
	cQuery += " ISNULL([15],0) AS SEPARACAO_AVIAMENTO, "
	cQuery += " ISNULL([20],0) AS POOL, "
	cQuery += " ISNULL([25],0) AS COSTURA_MONTAGEM, "
	cQuery += " ISNULL([30],0) AS COSTURA_HOPE_I_CEL, "
	cQuery += " ISNULL([40],0) AS RECEBIMENTO_FINALIZA, "
	cQuery += " SC2.C2_QUJE AS QUANTIDADE_PRODUZIDA, "
	cQuery += " H6_DTAPONT AS DATA_APONTAMENTO, "
	cQuery += " H6_OP AS OPERACAO "
	cQuery += " FROM "+RetSqlName("SH6")+" PIVOT (SUM(H6_QTDPROD) FOR H6_OPERAC IN ([00],[05],[15],[20],[25],[30],[40])) P "
	cQuery += " JOIN "+RetSqlName("SB1")+" ON B1_COD = H6_PRODUTO "
	cQuery += " JOIN "+RetSqlName("SBM")+" ON SBM010.BM_GRUPO=SB1010.B1_GRUPO "
	cQuery += " JOIN "+RetSqlName("SC2")+" SC2 ON C2_PRODUTO = H6_PRODUTO AND C2_LOCAL = H6_LOCAL AND C2_FILIAL = H6_FILIAL AND C2_NUM=LEFT(H6_OP,6) "
	cQuery += " WHERE P.D_E_L_E_T_ = '' "
	cQuery += " AND SB1010.D_E_L_E_T_ = '' "
	cQuery += " AND SBM010.D_E_L_E_T_ = '' "
	cQuery += " AND SC2.D_E_L_E_T_ = ' ' "
	cQuery += " AND H6_DTAPONT BETWEEN '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"' "
	cQuery += " group by H6_FILIAL,H6_OP,H6_PRODUTO, BM_DESC,P.[00],P.[05],P.[15],P.[20],P.[25],P.[30],P.[40], H6_DTAPONT,P.H6_OP,SC2.C2_QUANT,SC2.C2_QUJE "
	cQuery += " ORDER BY P.H6_OP,P.H6_DTAPONT "
    
	TCQUERY cQuery NEW ALIAS PCP

	While PCP->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(PCP->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection1:Cell("COD_PRODUTO"):SetValue(PCP->COD_PRODUTO)
		oSection1:Cell("COD_PRODUTO"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTO"):SetValue(PCP->PRODUTO)
		oSection1:Cell("PRODUTO"):SetAlign("LEFT")	
		
		oSection1:Cell("QUANTIDADE"):SetValue(PCP->QUANTIDADE)
		oSection1:Cell("QUANTIDADE"):SetAlign("LEFT")
		
		oSection1:Cell("DESCANSO_TERCIDO"):SetValue(PCP->DESCANSO_TERCIDO)
		oSection1:Cell("DESCANSO_TERCIDO"):SetAlign("LEFT")
		
		oSection1:Cell("CORTE"):SetValue(PCP->CORTE)
		oSection1:Cell("CORTE"):SetAlign("LEFT")
		
		oSection1:Cell("SEPARACAO_AVIAMENTO"):SetValue(PCP->SEPARACAO_AVIAMENTO)
		oSection1:Cell("SEPARACAO_AVIAMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("POOL"):SetValue(PCP->POOL)
		oSection1:Cell("POOL"):SetAlign("LEFT")
		
		oSection1:Cell("COSTURA_MONTAGEM"):SetValue(PCP->COSTURA_MONTAGEM)
		oSection1:Cell("COSTURA_MONTAGEM"):SetAlign("LEFT")
		
		oSection1:Cell("COSTURA_HOPE_I_CEL"):SetValue(PCP->COSTURA_HOPE_I_CEL)
		oSection1:Cell("COSTURA_HOPE_I_CEL"):SetAlign("LEFT")

		oSection1:Cell("RECEBIMENTO_FINALIZA"):SetValue(PCP->RECEBIMENTO_FINALIZA)
		oSection1:Cell("RECEBIMENTO_FINALIZA"):SetAlign("LEFT")
		
		oSection1:Cell("QUANTIDADE_PRODUZIDA"):SetValue(PCP->QUANTIDADE_PRODUZIDA)
		oSection1:Cell("QUANTIDADE_PRODUZIDA"):SetAlign("LEFT")

		oSection1:Cell("DATA_APONTAMENTO"):SetValue(PCP->DATA_APONTAMENTO)
		oSection1:Cell("DATA_APONTAMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("OPERACAO"):SetValue(PCP->OPERACAO)
		oSection1:Cell("OPERACAO"):SetAlign("LEFT")

		oSection1:PrintLine()
		
		PCP->(DBSKIP()) 
	enddo
	PCP->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 12 de Junho de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Dt AP Inicial"		        ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt AP Final"			    ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
Return
