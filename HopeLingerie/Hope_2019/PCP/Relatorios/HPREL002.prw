#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL002 
Boletim de recebimento de NF - Almoxarifado
@author Weskley Silva
@since 18/07/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL002()

	Private oReport
	Private cPergCont	:= 'HPREL002' 

	************************
	*Monta pergunte do Log *
	************************
	
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif
	
	oReport := ReportDef()
	
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 18 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'ALM', 'BOLETIM DE RECEBIMENTO NF ', cPergCont, {|oReport| ReportPrint( oReport ), 'BOLETIM DE RECEBIMENTO NF' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'BOLETIM DE RECEBIMENTO NF', { 'ALM', 'SD1','SB1','SBV'})
	
	TRCell():New( oSection1, 'DT_EMISSAO'	   			,'ALM', 		'DT_EMISSAO' ,		       	 	"@!"                        ,10)		
	TRCell():New( oSection1, 'DT_RECEBIMENTO'			,'ALM', 		'DT_RECEBIMENTO' ,       	 	"@!"                        ,10)
	TRCell():New( oSection1, 'DT_LANCAMENTO'			,'ALM', 		'DT_LANCAMENTO' ,       	 	"@!"                        ,10)
	TRCell():New( oSection1, 'NOTA_FISCAL'		    	,'ALM', 		'NOTA_FISCAL',					"@!"                        ,10)
	TRCell():New( oSection1, 'NUM_PEDIDO'      			,'ALM', 		'NUM_PEDIDO',		    		"@!"				        ,06)
	TRCell():New( oSection1, 'COD_PRODUTO'          	,'ALM', 		'COD_PRODUTO',              	"@!"						,15)
	TRCell():New( oSection1, 'REFERENCIA'  	        	,'ALM', 		'REFERENCIA',	              	"@!"						,15)
	TRCell():New( oSection1, 'DESCRICAO'          		,'ALM', 		'DESCRICAO',              		"@!"						,20)
	TRCell():New( oSection1, 'UNI_MEDIDA'          		,'ALM', 		'UNI_MEDIDA',              		"@!"						,04)	
	TRCell():New( oSection1, 'COD_COR'	        		,'ALM', 		'COD_COR' ,	        			"@!"		                ,04)
	TRCell():New( oSection1, 'COR' 						,'ALM', 		'COR' ,	        				"@!"		                ,20)
	TRCell():New( oSection1, 'TAMANHO' 					,'ALM', 		'TAMANHO' ,	    				"@!"		                ,04)
	TRCell():New( oSection1, 'QUANTIDADE'	    		,'ALM', 		'QUANTIDADE' ,	       	 		"@E 999.99"                 ,10)
	TRCell():New( oSection1, 'VALOR_NF'	    			,'ALM', 		'VALOR_NF' ,	       	 		"@E 999.99"                 ,10)
	TRCell():New( oSection1, 'TIPO'	        			,'ALM', 		'TIPO' ,	        			"@!"		                ,03)
	TRCell():New( oSection1, 'COD_GRUPO'          		,'ALM', 		'COD_GRUPO',              		"@!"						,10)
	TRCell():New( oSection1, 'DESC_GRUPO'          		,'ALM', 		'DESC_GRUPO',              		"@!"						,20)
	TRCell():New( oSection1, 'COD_FORNECE'	    		,'ALM', 		'COD_FORNECE' ,	       	 		"@!"                        ,06)
	TRCell():New( oSection1, 'NOME'	    				,'ALM', 		'NOME' ,		       	 		"@!"                        ,35)
	TRCell():New( oSection1, 'TES'	    				,'ALM', 		'TES' ,		       	 			"@!"                        ,10)
	TRCell():New( oSection1, 'DESC_TES'	    			,'ALM', 		'DESC_TES' ,	       	 		"@!"                        ,35)
	
	
Return( oReport )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 18 de Julho de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("ALM") > 0
		ALM->(dbCloseArea())
	Endif
      
    
	cQuery := " SELECT D1_DOC AS 'NOTA_FISCAL', " 
	cQuery += "	D1_PEDIDO AS 'NUM_PEDIDO', " 
	cQuery += "	B1_COD AS 'COD_PRODUTO', " 
	cQuery += "	B1_DESC AS 'DESCRICAO',"
	cQuery += "	B1_UM AS 'UNI_MEDIDA',"	 
	cQuery += " RIGHT(F1_EMISSAO,2)+'/'+SUBSTRING(F1_EMISSAO,5,2)+'/'+LEFT(F1_EMISSAO,4) AS 'DT_EMISSAO', "
	cQuery += " LEFT(B1_COD,8) AS 'REFERENCIA', "
	cQuery += " RIGHT(F1_DTDIGIT,2)+'/'+SUBSTRING(F1_DTDIGIT,5,2)+'/'+LEFT(F1_DTDIGIT,4) AS 'DT_RECEBIMENTO', "
	cQuery += " RIGHT(SD1.D1_DTDIGIT,2)+'/'+SUBSTRING(SD1.D1_DTDIGIT,5,2)+'/'+LEFT(SD1.D1_DTDIGIT,4) AS 'DT_LANCAMENTO', "
	cQuery += " BM_GRUPO AS COD_GRUPO,"
	cQuery += " SF4.F4_CODIGO AS TES, "
	cQuery += " SF4.F4_TEXTO AS DESC_TES, "
	cQuery += " BM_DESC AS DESC_GRUPO,"
	cQuery += "	BV_DESCRI AS 'COR'," 
	cQuery += "	BV_CHAVE AS 'COD_COR', " 
	cQuery += "	RIGHT(B1_COD,4) AS 'TAMANHO', " 
	cQuery += "	B1_TIPO AS 'TIPO', " 
	cQuery += "	D1_QUANT AS 'QUANTIDADE', SUM(D1_TOTAL) AS 'VALOR_NF',  " 
	cQuery += " D1_FORNECE AS 'COD_FORNECE',  "
	cQuery += " A2_NOME AS 'NOME' "
	cQuery += " FROM "+RetSqlName("SD1")+" SD1 (NOLOCK) " 
	cQuery += " JOIN "+RetSqlName("SB1")+" SB1 (NOLOCK) ON SD1.D1_COD = SB1.B1_COD AND SB1.D_E_L_E_T_ = '' " 
	cQuery += " JOIN "+RetSqlName("SBV")+" SBV (NOLOCK) ON SUBSTRING(B1_COD,9,3) = SBV.BV_CHAVE  AND SBV.BV_TABELA = 'COR' AND SBV.D_E_L_E_T_ = '' " 
	cQuery += " JOIN "+RetSqlName('SA2')+" SA2 (NOLOCK) ON D1_FORNECE = A2_COD AND D1_LOJA = A2_LOJA AND SA2.D_E_L_E_T_ = '' "
	cQuery += " JOIN "+RetSqlName('SBM')+" SBM (NOLOCK) ON BM_GRUPO = B1_GRUPO AND SBM.D_E_L_E_T_ = '' "
	cQuery += " JOIN "+RetSqlName('SF4')+" SF4 (NOLOCK) ON F4_CODIGO = D1_TES AND SF4.D_E_L_E_T_ = '' "
	cQuery += " JOIN "+RetSqlName('SF1')+" SF1 (NOLOCK) ON F1_DOC = D1_DOC AND F1_FORNECE = D1_FORNECE AND F1_LOJA = D1_LOJA AND F1_FILIAL = D1_FILIAL "
	cQuery += " WHERE SD1.D_E_L_E_T_ = '' " 
	cQuery += " AND SB1.B1_TIPO = 'MP' " 
	cQuery += " AND SD1.D1_PEDIDO <> '' " 
	
	if !Empty(mv_par01)
		cQuery += " AND D1_PEDIDO BETWEEN '"+Alltrim(mv_par01)+"' AND '"+Alltrim(mv_par02)+"'  "
	else 
		cQuery += " "
	endif
	
	if !Empty(mv_par02)
		cQuery += " AND D1_DOC BETWEEN '"+Alltrim(mv_par02)+"' AND '"+Alltrim(mv_par03)+"'  "
	else 
		cQuery += " "
	endif
	
	if !Empty(mv_par04)
		cQuery += " AND F1_EMISSAO BETWEEN '"+Alltrim(mv_par04)+"' AND '"+Alltrim(mv_par05)+"'  "
	else 
		cQuery += " "
	endif
	
	cQuery += " GROUP BY D1_DOC,D1_PEDIDO,B1_COD,B1_DESC,B1_UM,D1_DTDIGIT,F1_EMISSAO,B1_COD,F1_DTDIGIT,BM_GRUPO,SF4.F4_CODIGO,SF4.F4_TEXTO,BM_DESC,BV_DESCRI,BV_CHAVE,B1_TIPO,D1_QUANT,D1_FORNECE,A2_NOME "
	
	cQuery += " ORDER BY D1_DOC " 

	TCQUERY cQuery NEW ALIAS ALM

	While ALM->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		
		oSection1:Cell("DT_EMISSAO"):SetValue(ALM->DT_EMISSAO)
		oSection1:Cell("DT_EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("DT_RECEBIMENTO"):SetValue(ALM->DT_RECEBIMENTO)
		oSection1:Cell("DT_RECEBIMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("DT_LANCAMENTO"):SetValue(ALM->DT_LANCAMENTO)
		oSection1:Cell("DT_LANCAMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("NOTA_FISCAL"):SetValue(ALM->NOTA_FISCAL)
		oSection1:Cell("NOTA_FISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("NUM_PEDIDO"):SetValue(ALM->NUM_PEDIDO)
		oSection1:Cell("NUM_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_PRODUTO"):SetValue(ALM->COD_PRODUTO)
		oSection1:Cell("COD_PRODUTO"):SetAlign("LEFT")	
		
		oSection1:Cell("REFERENCIA"):SetValue(ALM->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("DESCRICAO"):SetValue(ALM->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("UNI_MEDIDA"):SetValue(ALM->UNI_MEDIDA)
		oSection1:Cell("UNI_MEDIDA"):SetAlign("LEFT")		
		
		oSection1:Cell("COD_COR"):SetValue(ALM->COD_COR)
		oSection1:Cell("COD_COR"):SetAlign("LEFT")

		oSection1:Cell("COR"):SetValue(ALM->COR)
		oSection1:Cell("COR"):SetAlign("LEFT")
		
		oSection1:Cell("TAMANHO"):SetValue(ALM->TAMANHO)
		oSection1:Cell("TAMANHO"):SetAlign("LEFT")
		
		oSection1:Cell("QUANTIDADE"):SetValue(ALM->QUANTIDADE)
		oSection1:Cell("QUANTIDADE"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_NF"):SetValue(ALM->VALOR_NF)
		oSection1:Cell("VALOR_NF"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO"):SetValue(ALM->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_GRUPO"):SetValue(ALM->COD_GRUPO)
		oSection1:Cell("COD_GRUPO"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_GRUPO"):SetValue(ALM->DESC_GRUPO)
		oSection1:Cell("DESC_GRUPO"):SetAlign("LEFT")
				
		oSection1:Cell("COD_FORNECE"):SetValue(ALM->COD_FORNECE)
		oSection1:Cell("COD_FORNECE"):SetAlign("LEFT")
		
		oSection1:Cell("NOME"):SetValue(ALM->NOME)
		oSection1:Cell("NOME"):SetAlign("LEFT")
		
		oSection1:Cell("TES"):SetValue(ALM->TES)
		oSection1:Cell("TES"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_TES"):SetValue(ALM->DESC_TES)
		oSection1:Cell("DESC_TES"):SetAlign("LEFT")
		
		oSection1:PrintLine()
		
		ALM->(DBSKIP()) 
	enddo
	ALM->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 13 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Pedido de ? "	        ,""		,""		,"mv_ch1","C",06,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Pedido At� ? "		    ,""		,""		,"mv_ch2","C",06,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Nota Inicial"		    ,""		,""		,"mv_ch3","C",09,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Nota Final"			    ,""		,""		,"mv_ch4","C",09,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data de? "		        ,""		,""		,"mv_ch5","D",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Data ate? "		        ,""		,""		,"mv_ch6","D",08,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
Return
