#INCLUDE "MATR820.CH"
#include "RWMAKE.CH" 
#include "TOPCONN.CH"
#Include "PROTHEUS.CH"

//#INCLUDE "FIVEWIN.CH"

Static cAliasTop

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � MATR820  � Autor � Felipe Nunes Toledo   � Data � 26/09/06 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Ordens de Producao                                         ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � Generico                                                   ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
*/
User Function UMATR820()

Local oReport

//-- Verifica se o SH8 esta locado para atualizacao por outro processo
If !IsLockSH8()

	//������������������������������������������������������������������������Ŀ
	//�Interface de impressao                                                  �
	//��������������������������������������������������������������������������
	oReport:= ReportDef()
	If !oReport:PrintDialog()
   		dbSelectArea("SH8")
		dbClearFilter()
		dbCloseArea()
		Return Nil
	EndIf

EndIf

Return NIL

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �ReportDef � Autor �Felipe Nunes Toledo    � Data �27/09/06  ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �A funcao estatica ReportDef devera ser criada para todos os ���
���          �relatorios que poderao ser agendados pelo usuario.          ���
�������������������������������������������������������������������������Ĵ��
���Parametros�Nenhum                                                      ���
�������������������������������������������������������������������������Ĵ��
���Uso       �MATR820                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ReportDef()
Local oReport
Local oSection1, oSection2, oSection2a, Section3, Section4, Section5
Local aOrdem	:= {STR0002,STR0003,STR0004,STR0005}	//"Por Numero"###"Por Produto"###"Por Centro de Custo"###"Por Prazo de Entrega"
Local cTitle	:= STR0039 //"Ordens de Producao"
Local lVer116   := (VAL(GetVersao(.F.)) == 11 .And. GetRpoRelease() >= "R6" .Or. VAL(GetVersao(.F.))  > 11)

#IFDEF TOP
	cAliasTop := GetNextAlias()
#ELSE
	cAliasTop := "SC2"
	cAliasTop9 := "SC2"
#ENDIF

//������������������������������������������������������������������������Ŀ
//�Criacao do componente de impressao                                      �
//�                                                                        �
//�TReport():New                                                           �
//�ExpC1 : Nome do relatorio                                               �
//�ExpC2 : Titulo                                                          �
//�ExpC3 : Pergunte                                                        �
//�ExpB4 : Bloco de codigo que sera executado na confirmacao da impressao  �
//�ExpC5 : Descricao                                                       �
//�                                                                        �
//��������������������������������������������������������������������������
oReport:= TReport():New("MATR820",cTitle,"MTR820", {|oReport| ReportPrint(oReport, cAliasTop)},OemToAnsi(STR0001)) //"Este programa ira imprimir a Rela��o das Ordens de Produ��o"
oReport:SetPortrait()     // Define a orientacao de pagina do relatorio como retrato.
oReport:HideParamPage()   // Desabilita a impressao da pagina de parametros.
oReport:nFontBody	:= 9  // Define o tamanho da fonte.
oReport:nLineHeight	:= 50 // Define a altura da linha.
//��������������������������������������������������������������Ŀ
//� Verifica as perguntas selecionadas - MTR820                  �
//����������������������������������������������������������������
//��������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros                         �
//� mv_par01            // Da OP                                 �
//� mv_par02            // Ate a OP                              �
//� mv_par03            // Da data                               �
//� mv_par04            // Ate a data                            �
//� mv_par05            // Imprime roteiro de operacoes          �
//� mv_par06            // Imprime codigo de barras              �
//� mv_par07            // Imprime Nome Cientifico               �
//� mv_par08            // Imprime Op Encerrada                  �
//� mv_par09            // Impr. por Ordem de                    �
//� mv_par10            // Impr. OP's Firmes, Previstas ou Ambas �
//� mv_par11            // Impr. Item Negativo na Estrutura      �
//� mv_par12            // Imprime Lote/Sub-Lote                 �
//� mv_par13            // Imprime opera��es vencidas?           �
//����������������������������������������������������������������
AjustaSX1()
Pergunte(oReport:GetParam(),.F.)
//������������������������������������������������������������������������Ŀ
//�Criacao da secao utilizada pelo relatorio                               �
//�                                                                        �
//�TRSection():New                                                         �
//�ExpO1 : Objeto TReport que a secao pertence                             �
//�ExpC2 : Descricao da secao                                              �
//�ExpA3 : Array com as tabelas utilizadas pela secao. A primeira tabela   �
//�        sera considerada como principal para a secao.                   �
//�ExpA4 : Array com as Ordens do relatorio                                �
//�ExpL5 : Carrega campos do SX3 como celulas                              �
//�        Default : False                                                 �
//�ExpL6 : Carrega ordens do Sindex                                        �
//�        Default : False                                                 �
//��������������������������������������������������������������������������

//��������������������������������������������������������������Ŀ
//� Sessao 1 (oSection1)                                         �
//����������������������������������������������������������������
oSection1 := TRSection():New(oReport,STR0050,{"SC2","SB1","SC5","SA1"},aOrdem) // "Ordens de Produ��o"
oSection1:SetLineStyle() //Define a impressao da secao em linha
oSection1:SetReadOnly()

TRCell():New(oSection1,'C2_NUM'		,'SC2',STR0073   ,PesqPict('SC2','C2_NUM')	 ,TamSX3('C2_NUM')[1]	  ,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C2_PRODUTO'	,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'B1_DESC' 	,'SB1',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'Emissao'   	,'SC2',STR0040   ,PesqPict("SC2","C2_EMISSAO")	,10						,/*lPixel*/,{|| DTOC(dDataBase) })
TRCell():New(oSection1,'C5_CLIENTE'	,'SC5',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C5_LOJACLI'	,'SC5',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'A1_NOME'  	,'SA1',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'QtdeProd'	,'SC2',STR0041   ,PesqPict("SC2","C2_QUANT")	,TamSX3("C2_QUANT")[1]	,/*lPixel*/,{|| aSC2Sld(cAliasTop) })
TRCell():New(oSection1,'C2_QUANT'	,'SC2',STR0042   ,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'OpQuant'  	,'SC2',STR0043   ,PesqPict("SC2","C2_QUANT")	,TamSX3("C2_QUANT")[1]	,/*lPixel*/,{|| (cAliasTop)->C2_QUANT - (cAliasTop)->C2_QUJE })
TRCell():New(oSection1,'B1_UM'	 	,'SB1',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C2_CC'		,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C2_STATUS'	,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C2_DATPRI'	,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C2_DATPRF'	,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C2_DATAJI'	,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'C2_DATAJF'	,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection1,'RealIni'	,'SC2',STR0044   ,'@!'							,8						,/*lPixel*/,{|| "__/__/__" })
TRCell():New(oSection1,'RealFim'	,'SC2',STR0045   ,'@!'							,8						,/*lPixel*/,{|| "__/__/__" })
TRCell():New(oSection1,'C2_OBS'		,'SC2',/*Titulo*/,/*Picture*/					,/*Tamanho*/			,/*lPixel*/,/*{|| code-block de impressao }*/)

oSection1:Cell('B1_DESC'  ):SetCellBreak()
oSection1:Cell('Emissao'  ):SetCellBreak()
oSection1:Cell('A1_NOME'  ):SetCellBreak()
oSection1:Cell('C2_QUANT' ):SetCellBreak()
oSection1:Cell('OpQuant'  ):SetCellBreak()
oSection1:Cell('B1_UM'    ):SetCellBreak()
oSection1:Cell('C2_CC'    ):SetCellBreak()
oSection1:Cell('C2_STATUS'):SetCellBreak()
oSection1:Cell('C2_DATPRF'):SetCellBreak()
oSection1:Cell('C2_DATAJF'):SetCellBreak()
oSection1:Cell('RealFim'  ):SetCellBreak()

//��������������������������������������������������������������Ŀ
//� Sessao 2 (oSection2)                                         �
//����������������������������������������������������������������
oSection2 := TRSection():New(oSection1,STR0051,{"SD4","SB1","NNR"},/*Ordem*/) //"Empenhos"
//oSection2:SetLineStyle() //Define a impressao da secao em linha
oSection2:SetHeaderBreak()
oSection2:SetReadOnly()

TRCell():New(oSection2,'D4_COD'	 	,'SD4',STR0058   ,PesqPict('SD4','D4_COD')    ,TamSX3('D4_COD')[1]+2    ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,.F./*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2,'B1_DESC' 	,'SB1',STR0059   ,PesqPict('SB1','B1_DESC')   ,30 ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
oSection2:Cell("B1_DESC"):SetLineBreak()
TRCell():New(oSection2,'D4_QUANT' 	,'SD4',STR0043   ,PesqPict('SD4','D4_QUANT')  ,TamSX3('D4_QUANT')[1]  ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2,'B1_UM'   	,'SB1',STR0061   ,PesqPict('SB1','B1_UM')     ,TamSX3('B1_UM')[1]     ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2,'D4_LOCAL'	,'SD4',STR0072   ,PesqPict('SD4','D4_LOCAL')  ,TamSX3('D4_LOCAL')[1]  ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
If lVer116
	TRCell():New(oSection2,'NNR_DESCRI'	,'NNR',/*Titulo*/,PesqPict('NNR','NNR_DESCRI'),TamSX3('NNR_DESCRI')[1],/*lPixel*/,{||Posicione("NNR",1,xFilial("NNR")+SD4->D4_LOCAL,"NNR_DESCRI")},/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
Else
	TRCell():New(oSection2,'B2_LOCALIZ'	,'SB2',/*Titulo*/,PesqPict('SB2','B2_LOCALIZ'),TamSX3('B2_LOCALIZ')[1],/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
EndIf
TRCell():New(oSection2,'D4_TRT'  	,'SD4',STR0064   ,PesqPict('SD4','D4_TRT')    ,TamSX3('D4_TRT')[1]    ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2,'D4_LOTECTL'	,'SD4',/*Titulo*/,PesqPict('SD4','D4_LOTECTL'),TamSX3('D4_LOTECTL')[1],/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2,'D4_NUMLOTE'	,'SD4',/*Titulo*/,PesqPict('SD4','D4_NUMLOTE'),TamSX3('D4_NUMLOTE')[1],/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2,'D4_OP'  	,'SD4',/*Titulo*/,PesqPict('SD4','D4_OP')     ,TamSX3('D4_OP')[1]     ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)

//��������������������������������������������������������������Ŀ
//� Sessao 2a (oSection2a)                                         �
//����������������������������������������������������������������
oSection2a := TRSection():New(oSection1,STR0051,{"SD4","SB1","NNR"},/*Ordem*/) //"Empenhos"
oSection2a:SetHeaderBreak()
oSection2a:SetReadOnly()

TRCell():New(oSection2a,'C2a_PRODUTO'		,'SC2','Produto',PesqPict('SC2','C2_NUM')  	,17							,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,.F./*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2a,'C2a_COR'			,'SC2','  COR  ',PesqPict('SC2','C2_NUM')		,4							,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2a,'C2a_TAMANHO'		,'SC2','Tamanho',PesqPict('SC2','C2_NUM')		,5							,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2a,'C2a_QUJE' 		,'SC2','Qtd.Ent',PesqPict('SD4','D4_QUANT')  ,TamSX3('D4_QUANT')[1]	,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
TRCell():New(oSection2a,'C2a_QUANT' 		,'SC2','Qtd.Sol',PesqPict('SD4','D4_QUANT')  ,TamSX3('D4_QUANT')[1]	,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)

//��������������������������������������������������������������Ŀ
//� Sessao 3 (oSection3)                                         �
//����������������������������������������������������������������
oSection3 := TRSection():New(oSection1,STR0052,{"SG2","SH8","SH1","SH4"},/*Ordem*/) //"Opera��es"
oSection3:SetHeaderBreak()
oSection3:SetReadOnly()

TRCell():New(oSection3,'G2_RECURSO','SG2',/*Titulo*/,/*Picture*/,10 		   ,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection3,'H1_DESCRI'	,'SH1',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection3,'G2_FERRAM'	,'SG2',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection3,'H4_DESCRI'	,'SH4',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/)
TRCell():New(oSection3,'G2_OPERAC'	,'SG2',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/)
TRCell():New(oSection3,'G2_DESCRI'	,'SG2',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,.T.)

oSection3:Cell('H1_DESCRI'):HideHeader()
oSection3:Cell('H4_DESCRI'):HideHeader()
oSection3:Cell('G2_DESCRI'):HideHeader()

//��������������������������������������������������������������Ŀ
//� Sessao 4 (oSection4)                                         �
//����������������������������������������������������������������
oSection4 := TRSection():New(oSection3,STR0053,{"SG2","SH8","SH1","SH4"},/*Ordem*/) //"Tempo Rot. Oper."
oSection4:SetLineStyle() //Define a impressao da secao em linha
oSection4:SetReadOnly()

TRCell():New(oSection4,'H8_DTINI'	,'SH8',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection4,'H8_HRINI'	,'SH8',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection4,'H8_DTFIM'	,'SH8',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection4,'H8_HRFIM'	,'SH8',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection4,'IniAloc'	,'SH8',STR0046   ,'@!'  , 24        ,/*lPixel*/,{|| " ____/ ____/____ ___:___" })
TRCell():New(oSection4,'FimAloc'	,'SH8',STR0047   ,'@!'  , 24        ,/*lPixel*/,{|| " ____/ ____/____ ___:___" })
TRCell():New(oSection4,'H8_QUANT'	,'SH8',/*Titulo*/,/*Picture*/,/*Tamanho*/,/*lPixel*/,/*{|| code-block de impressao }*/)
TRCell():New(oSection4,'QtdeProd'	,'SH8',STR0048   ,/*Picture*/, 12       ,/*lPixel*/,{|| Space(12) })
TRCell():New(oSection4,'QtdPerda'	,'SH8',STR0049   ,/*Picture*/,/*Tamanho*/,/*lPixel*/,{|| Space(12) })
If SG2->(FieldPos("G2_DTINI")) > 0
	TRCell():New(oSection4,'VldInici','SH8',STR0074,/*Picture*/, 10,/*lPixel*/,/*{|| code-block de impressao }*/) //"Validade inicial"
	TRCell():New(oSection4,'VldFinal','SH8',STR0075,/*Picture*/, 10,/*lPixel*/,/*{|| code-block de impressao }*/) //"Validade Final"
EndIf

oSection4:Cell('H8_HRFIM'):SetCellBreak()
oSection4:Cell('FimAloc' ):SetCellBreak()
If SG2->(FieldPos("G2_DTINI")) > 0
	oSection4:Cell('QtdPerda' ):SetCellBreak()
EndIf

oSection1:SetNoFilter({"SA1","SC5"})
oSection2:SetNoFilter({"SD4","SB1","SB2","SC2"})
oSection2a:SetNoFilter({"SD4","SB1","SB2","SC2"})
oSection3:SetNoFilter({"SG2","SH8","SH1","SH4"})
oSection4:SetNoFilter({"SG2","SH8","SH1","SH4"})

Return(oReport)

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �ReportPrint � Autor �Felipe Nunes Toledo  � Data �27/09/06  ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �A funcao estatica ReportPrint devera ser criada para todos  ���
���          �os relatorios que poderao ser agendados pelo usuario.       ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Nenhum                                                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpO1: Objeto Report do Relatorio                           ���
�������������������������������������������������������������������������Ĵ��
���Uso       �MATR820                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function ReportPrint(oReport, cAliasTop)
Local oSection1	:= oReport:Section(1)
Local oSection2	:= oReport:Section(1):Section(1)
Local oSection2a	:= oReport:Section(1):Section(1)
Local oSection3	:= oReport:Section(1):Section(2)
Local oSection4	:= oReport:Section(1):Section(2):Section(1)
Local oSection5
Local nOrdem    := oSection1:GetOrder()
Local oBreak
Local cIndex	:= ""
Local cCondicao	:= ""
Local cCode    	:= ""
Local nCntFor   := 0
Local nLinBar	:= 0
Local cWhere01, cWhere02, cWhere03
Local cOrderBy
Local lVer116   := (VAL(GetVersao(.F.)) == 11 .And. GetRpoRelease() >= "R6" .Or. VAL(GetVersao(.F.))  > 11)
Local lPlanilha := oReport:nDevice == 4
Local lAchou	:= 0
Local aColsEx := {}
Local aFieldFill := {}
Local aProds := {}
Private aArray	:= {}
Private lItemNeg := GetMv("MV_NEGESTR") .And. mv_par11 == 1

If !lPlanilha .Or. len(oReport:aXlsTable) == 0
	oSection1:Cell("C2_NUM"):Disable()
Endif

// Definindo quebra para secao 2 e ocultando celula utilizada somente para quebra
oBreak := TRBreak():New(oSection2,oSection2:Cell("D4_OP"),Nil,.F.)
oSection2:Cell("D4_OP"):Disable()

If lPlanilha
	oSection5 := TRSection():New(oReport,'',{"SC2"},/*Ordem*/)
	TRCell():New(oSection5,'C2_NUM'  	,'SC2',/*Titulo*/,PesqPict('SC2','C2_NUM')     ,TamSX3('C2_NUM')[1]     ,/*lPixel*/,/*{|| code-block de impressao }*/,/*cAlign*/,/*lLineBreak*/,/*cHeaderAlign*/,/*lCellBreak*/,/*nColSpace*/,/*lAutoSize*/,/*nClrBack*/,/*nClrFore*/)
Endif

If mv_par12 == 2
	oSection2:Cell("D4_LOTECTL"):Disable()
	oSection2:Cell("D4_NUMLOTE"):Disable()
ElseIf !oReport:oPage:IsLandScape()
	oSection2:Cell("B1_DESC"):SetSize(19)
	If lVer116
		oSection2:Cell("NNR_DESCRI"):SetSize(10)
	Else
		oSection2:Cell("B2_LOCALIZ"):SetSize(10)
	EndIf
	oSection2:Cell("D4_NUMLOTE"):SetTitle(STR0057)
EndIf

//������������������������������������������������������������������������Ŀ
//�Filtragem do relatorio                                                  �
//��������������������������������������������������������������������������
#IFDEF TOP

   	//������������������������������������������������������������������������Ŀ
	//�Transforma parametros Range em expressao SQL                            �
	//��������������������������������������������������������������������������
	MakeSqlExpr(oReport:GetParam())

	//��������������������������������������������������������������Ŀ
	//� Condicao Where para filtrar OP's                             �
	//����������������������������������������������������������������
	cWhere01 := "%'"+mv_par01+"'%"
	cWhere02 := "%'"+mv_par02+"'%"

	cWhere03 := "%"
	If mv_par08 == 2
		cWhere03 += "AND SC2.C2_DATRF = ' '"
	Endif
	cWhere03 += "%"

	cOrderBy := "%"
	If nOrdem == 4
		cOrderBy += "SC2.C2_FILIAL, SC2.C2_DATPRF"
	Else
		cOrderBy += SqlOrder(SC2->(IndexKey(nOrdem)))
	EndIf
	cOrderBy += "%"

	//������������������������������������������������������������������������Ŀ
	//�Query do relatorio da secao 1                                           �
	//��������������������������������������������������������������������������
	oSection1:BeginQuery()
	BeginSql Alias cAliasTop

/*
	SELECT SC2.C2_FILIAL, SC2.C2_NUM, SC2.C2_ITEM, SC2.C2_SEQUEN, SC2.C2_ITEMGRD, SC2.C2_DATPRF,
	       SC2.C2_DATRF, SC2.C2_PRODUTO, SC2.C2_DESTINA, SC2.C2_PEDIDO, SC2.C2_ROTEIRO, SC2.C2_QUJE,
	       SC2.C2_PERDA, SC2.C2_QUANT, SC2.C2_DATPRI, SC2.C2_CC, SC2.C2_DATAJI, SC2.C2_DATAJF,
	       SC2.C2_STATUS, SC2.C2_OBS, SC2.C2_TPOP,
	       SC2.R_E_C_N_O_  SC2RECNO

	FROM %table:SC2% SC2

	WHERE SC2.C2_FILIAL = %xFilial:SC2% AND
		  SC2.C2_NUM || SC2.C2_ITEM || SC2.C2_SEQUEN || SC2.C2_ITEMGRD >= %Exp:cWhere01% AND
		  SC2.C2_NUM || SC2.C2_ITEM || SC2.C2_SEQUEN || SC2.C2_ITEMGRD <= %Exp:cWhere02% AND
		  SC2.C2_DATPRF BETWEEN %Exp:mv_par03% AND %Exp:mv_par04% AND
		  SC2.%NotDel%
		  %Exp:cWhere03%

	ORDER BY %Exp:cOrderby%
*/

	SELECT SC2.C2_FILIAL, SC2.C2_NUM, SC2.C2_ITEM, SC2.C2_SEQUEN,/* SC2.C2_ITEMGRD,*/ SC2.C2_DATPRF,
	       SC2.C2_DATRF, SUBSTRING(SC2.C2_PRODUTO,1,8) C2_PRODUTO, SC2.C2_DESTINA, SC2.C2_PEDIDO, 
	       SC2.C2_ROTEIRO, SUM(SC2.C2_QUJE) C2_QUJE, SUM(SC2.C2_PERDA) C2_PERDA, SUM(SC2.C2_QUANT)   
	       C2_QUANT, SC2.C2_DATPRI, SC2.C2_CC, SC2.C2_DATAJI, SC2.C2_DATAJF, SC2.C2_STATUS, SC2.C2_OBS, 
	       SC2.C2_TPOP/*, SC2.R_E_C_N_O_  SC2RECNO*/

	FROM %table:SC2% SC2

	WHERE SC2.C2_FILIAL = %xFilial:SC2% AND
		  SC2.C2_NUM || SC2.C2_ITEM || SC2.C2_SEQUEN || SC2.C2_ITEMGRD >= %Exp:cWhere01% AND
		  SC2.C2_NUM || SC2.C2_ITEM || SC2.C2_SEQUEN || SC2.C2_ITEMGRD <= %Exp:cWhere02% AND
		  SC2.C2_DATPRF BETWEEN %Exp:mv_par03% AND %Exp:mv_par04% AND
		  SC2.C2_LOCAL BETWEEN %Exp:mv_par13% AND %Exp:mv_par14% AND
		  SC2.%NotDel%
		  %Exp:cWhere03%
	
	GROUP BY SC2.C2_FILIAL, SC2.C2_NUM, SC2.C2_ITEM, SC2.C2_SEQUEN, /*SC2.C2_ITEMGRD,*/ SC2.C2_DATPRF,
			SC2.C2_DATRF, SUBSTRING(SC2.C2_PRODUTO,1,8), 	SC2.C2_DESTINA, SC2.C2_PEDIDO, SC2.C2_ROTEIRO,
			/*SC2.C2_QUJE, SC2.C2_PERDA, SC2.C2_QUANT,*/ SC2.C2_DATPRI, SC2.C2_CC, SC2.C2_DATAJI,
			SC2.C2_DATAJF, SC2.C2_STATUS, SC2.C2_OBS, SC2.C2_TPOP/*, SC2.R_E_C_N_O_ SC2RECNO */
	
//	ORDER BY %Exp:cOrderby%
	ORDER BY  SC2.C2_FILIAL, SC2.C2_NUM, SC2.C2_ITEM, SC2.C2_SEQUEN/*, SC2.C2_ITEMGRD*/

	EndSql
	oSection1:EndQuery()

//***********************************
	If Select("BDGRD") > 0 
    	BDGRD->(DbCloseArea()) 
	EndIf 

	cQuery := " SELECT SC2.C2_FILIAL C2a_FILIAL, SC2.C2_NUM C2a_NUM, SC2.C2_ITEM C2a_ITEM, SC2.C2_SEQUEN C2a_SEQUEN, "+CRLF  
	cQuery += " 		  SC2.C2_ITEMGRD C2a_ITEMGRD, SC2.C2_PRODUTO C2a_PRODUTO, SUBSTRING(SC2.C2_PRODUTO,1,8) C2a_PRODUTOX, "+CRLF
	cQuery += " 		  SUBSTRING(SC2.C2_PRODUTO,9,3) C2a_COR, SUBSTRING(SC2.C2_PRODUTO,12,4) C2a_TAMANHO, SUM(SC2.C2_QUJE) C2a_QUJE, "+CRLF
	cQuery += " 		  SUM(SC2.C2_PERDA) C2a_PERDA, SUM(SC2.C2_QUANT) C2a_QUANT "+CRLF
	cQuery += " FROM "+RetSqlName("SC2")+" SC2 "+CRLF
	cQuery += " WHERE SC2.C2_FILIAL = '"+xFilial("SC2")+"' AND "+CRLF
	cQuery += " 		  SC2.C2_NUM + SC2.C2_ITEM + SC2.C2_SEQUEN + SC2.C2_ITEMGRD >= '"+mv_par01+"' AND "+CRLF
	cQuery += " 		  SC2.C2_NUM + SC2.C2_ITEM + SC2.C2_SEQUEN + SC2.C2_ITEMGRD <= '"+mv_par02+"' AND "+CRLF
	cQuery += " 		  SC2.C2_DATPRF BETWEEN '"+DTOS(mv_par03)+"' AND '"+DTOS(mv_par04)+"' AND "+CRLF
	cQuery += " 		  SC2.C2_LOCAL BETWEEN '"+mv_par13+"' AND '"+mv_par14+"' AND "+CRLF
	cQuery += " 		  SC2.D_E_L_E_T_ = '' "+CRLF
	cQuery += " GROUP BY SC2.C2_FILIAL, SC2.C2_NUM, SC2.C2_ITEM, SC2.C2_SEQUEN, SC2.C2_NUM, "+CRLF
	cQuery += " 			SC2.C2_ITEM, SC2.C2_SEQUEN, SC2.C2_ITEMGRD, SC2.C2_PRODUTO "+CRLF
	cQuery += " ORDER BY SC2.C2_FILIAL, SC2.C2_NUM, SC2.C2_ITEM, SC2.C2_SEQUEN "+CRLF

	tcQuery cQuery New Alias "BDGRD"
	
//***********************************

#ELSE
	//������������������������������������������������������������������������Ŀ
	//�Transforma parametros Range em expressao ADVPL                          �
	//��������������������������������������������������������������������������
	MakeAdvplExpr(oReport:GetParam())

	dbSelectArea(cAliasTop)

	If nOrdem == 4
		cIndex := "C2_FILIAL+DTOS(C2_DATPRF)"
	Else
		dbSetOrder(nOrdem)
	EndIf

	cCondicao := "C2_FILIAL=='"+xFilial("SC2")+"'"
	cCondicao += ".And.C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD>='"+mv_par01+"'"
	cCondicao += ".And.C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD<='"+mv_par02+"'"
	cCondicao += ".And.DTOS(C2_DATPRF)>='"+DTOS(mv_par03)+"'"
	cCondicao += ".And.DTOS(C2_DATPRF)<='"+DTOS(mv_par04)+"'"
	If mv_par08 == 2
		cCondicao += ".And.Empty(C2_DATRF)"
	EndIf

	oReport:Section(1):SetFilter(cCondicao,If(nOrdem==4,cIndex,IndexKey()))
#ENDIF
//��������������������������Ŀ
//�Posicionamento das tabelas�
//����������������������������
TRPosition():New(oSection1,"SB1",1,{|| xFilial("SB1")+(cAliasTop)->C2_PRODUTO })
TRPosition():New(oSection1,"SC5",1,{|| xFilial("SC5")+(cAliasTop)->C2_PEDIDO })
TRPosition():New(oSection1,"SA1",1,{|| xFilial("SA1")+SC5->C5_CLIENTE+SC5->C5_LOJACLI })

//������������������������������������������������������������������������Ŀ
//�Inicio da impressao do fluxo do relatorio                               �
//��������������������������������������������������������������������������
oReport:SetMeter(SC2->(LastRec()))
oSection1:Init()
oSection2:Init()
//oSection3:Init()
//oSection4:Init()
If lPlanilha
	oSection5:Init()
Endif

dbSelectArea(cAliasTop)

While !oReport:Cancel() .And. !(cAliasTop)->(Eof())

	//-- Valida se a OP deve ser Impressa ou nao
	If !MtrAValOP(mv_par10,"SC2",cAliasTop)
		dbSkip()
		Loop
	EndIf

	//Definindo a descricao do produto
	MR820Desc(oReport, cAliasTop)
	
	//���������������������������������������������������������Ŀ
	//� Desabilitando celulas que nao deverao serem impressas   �
	//�����������������������������������������������������������
	If (cAliasTop)->C2_DESTINA <> "P"
    	oSection1:Cell('C5_CLIENTE'):Disable()
    	oSection1:Cell('C5_LOJACLI'):Disable()
    	oSection1:Cell('A1_NOME'   ):Disable()
 	EndIf
	If Empty((cAliasTop)->C2_STATUS)
		oSection1:Cell("C2_STATUS"):SetValue("Normal")
	EndIf
	If (cAliasTop)->C2_QUJE + (cAliasTop)->C2_PERDA > 0
		oSection1:Cell('OpQuant'):Disable()
	Else
		oSection1:Cell('QtdeProd'):Disable()
		oSection1:Cell('C2_QUANT'):Disable()
	Endif
	If (Empty((cAliasTop)->C2_OBS))
		oSection1:Cell('C2_OBS'):Disable()
	EndIf
	
	//�������������������������������Ŀ
	//�Definindo o titulo do Relatorio�
	//���������������������������������
//	oReport:SetTitle(STR0010+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD)) //"        O R D E M   D E   P R O D U C A O       NRO :"
	oReport:SetTitle(STR0010+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD) //"        O R D E M   D E   P R O D U C A O       NRO :"
	
	If mv_par06 == 1
		nLinBar := 0.95
	
		oReport:PrintText("")
		For nCntFor := 1 to 5
			oReport:SkipLine()
		Next nCntFor
	
//		cCode := (cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD)
		cCode := (cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD
	
		If oReport:lHeaderVisible .And. oReport:nEnvironment == 2
			nLinBar += 1
		EndIf
	
		If oReport:GetOrientation()== 1
			nLinBar += 0.2
		EndIf
		MSBAR3("CODE128",nLinBar,0.5,Trim(cCode),@oReport:oPrint,Nil,Nil,Nil,Nil ,1.5 ,Nil,Nil,Nil,.F.)
	EndIf
	
	If lPlanilha
		oReport:SkipLine()
//		oSection5:Cell("C2_NUM"):SetValue((cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD))
		oSection5:Cell("C2_NUM"):SetValue((cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD)
		oSection5:Cell('C2_NUM'):Enable()
		If len(oReport:aXlsTable) > 0
//			oSection1:Cell("C2_NUM"):SetValue((cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD))
			oSection1:Cell("C2_NUM"):SetValue((cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD)
			oSection1:Cell('C2_NUM'):Enable()
		Endif
		//Impressao da Section 5
		oSection5:PrintLine()
		oReport:IncMeter()
	Endif

	//Impressao da Section 1
	oSection1:PrintLine()
	oReport:IncMeter()

	//���������������������
	//�Habilitando celulas�
	//���������������������
	If (cAliasTop)->C2_DESTINA <> "P"
    	oSection1:Cell('C5_CLIENTE'):Enable()
    	oSection1:Cell('C5_LOJACLI'):Enable()
    	oSection1:Cell('A1_NOME'   ):Enable()
 	EndIf
	If (cAliasTop)->C2_QUJE + (cAliasTop)->C2_PERDA > 0
		oSection1:Cell('OpQuant'):Enable()
	Else
		oSection1:Cell('QtdeProd'):Enable()
		oSection1:Cell('C2_QUANT'):Enable()
	Endif
	If (Empty((cAliasTop)->C2_OBS))
		oSection1:Cell('C2_OBS'):Enable()
	EndIf
	
	//--- Inicio fluxo impressao secao 2
	SB1->(dbSeek(xFilial("SB1")+(cAliasTop)->C2_PRODUTO))

	Mr820Ogr(oReport, cAliasTop) //Impressao da Section 2a

	dbSelectArea("BDGRD")
	BDGRD->(Dbgotop())
		
	While !BDGRD->(Eof())

		aArray := {}
	//	MontStruc((cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD))
		MontStruc(BDGRD->(C2a_NUM+C2a_ITEM+C2a_SEQUEN+C2a_ITEMGRD))

		If (cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN) == BDGRD->(C2a_NUM+C2a_ITEM+C2a_SEQUEN) 		
			If mv_par09 == 1
				aSort( aArray,1,, { |x, y| (x[1]+x[8]) < (y[1]+y[8]) } )
			Else
				aSort( aArray,1,, { |x, y| (x[8]+x[1]) < (y[8]+y[1]) } )
			ENDIF
	
			For nX := 1 TO Len(aArray) 
				For nI := 1 TO 12
					lAchou := ASCAN(aColsEx, {|x| Alltrim(x[12])+Alltrim(x[1]) == aArray[nX][12]+aArray[nX][1]})
					//lAchou := ASCAN(aColsEx, {|x| Alltrim(x[1]) == aArray[nX][1]})
					If lAchou == 0 
						aadd(aFieldFill, aArray[nX][nI])
					ElseIf nI == 5
						aColsEx[lAchou][5] := aColsEx[lAchou][5]+aArray[nX][nI]
					End
				Next nI
				If lAchou == 0
					aadd(aFieldFill, nX)
					Aadd(aFieldFill, .F.)
			  		Aadd(aColsEx, aFieldFill)
	  				aFieldFill :={}
	  			End
					
			Next nX
			
			aadd(aProds, BDGRD->(C2a_PRODUTOX+C2a_COR+C2a_TAMANHO))
		End

		BDGRD->(dbSkip())
				
	EndDo
		
	ASORT(aColsEx, , , { |x,y| x[13] < y[13] } )
	
	For nCntFor := 1 TO Len(aColsEx)
		
	    SB1->(dbSetOrder(1))
	    SB1->(MsSeek(xFilial("SB1")+aColsEx[nCntFor][1]))

	    SD4->(dbSetOrder(2))
//	    SD4->(MsSeek(xFilial("SD4")+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD)+aColsEx[nCntFor][1]+aColsEx[nCntFor][6]))
	    SD4->(MsSeek(xFilial("SD4")+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD+aColsEx[nCntFor][1]+aColsEx[nCntFor][6]))

		
		oSection2:Cell("D4_COD"    ):SetValue(aColsEx[nCntFor][1])
		oSection2:Cell("B1_DESC"   ):SetValue(aColsEx[nCntFor][2])
		oSection2:Cell("D4_QUANT"  ):SetValue(aColsEx[nCntFor][5])
		oSection2:Cell("B1_UM"     ):SetValue(aColsEx[nCntFor][4])
		oSection2:Cell("D4_LOCAL"  ):SetValue(aColsEx[nCntFor][6])
		If lVer116
			oSection2:Cell("NNR_DESCRI"):SetValue(aColsEx[nCntFor][7])
		Else
			oSection2:Cell("B2_LOCALIZ"):SetValue(aColsEx[nCntFor][7])
		EndIf
		oSection2:Cell("D4_TRT"    ):SetValue(aColsEx[nCntFor][8])
//		oSection2:Cell("D4_OP"     ):SetValue((cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD))
		oSection2:Cell("D4_OP"     ):SetValue((cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD)
		If mv_par12 == 1
			oSection2:Cell("D4_LOTECTL"):SetValue(aColsEx[nCntFor][10])
			oSection2:Cell("D4_NUMLOTE"):SetValue(aColsEx[nCntFor][11])
		EndIf
	
	    //Impressao da Section 2
		oSection2:PrintLine()

	Next nCntFor
	
//	If mv_par05 == 1 .AND. SubString(aColsEx[1][12],1,2) <> "BN"  
//		Mr820Ope(oReport, cAliasTop, aProds) //Impressao da Section 3 e Section 4
//	EndIf

	oReport:EndPage() //-- Salta Pagina
		
	dbSelectArea(cAliasTop)
	dbSkip()
	
	aColsEx := {}

EndDo

If lPlanilha
	oSection5:Finish()
Endif
//oSection4:Finish()
//oSection3:Finish()
oSection2:Finish()
oSection1:Finish()
(cAliasTop)->(DbCloseArea())
dbSelectArea("SH8")
dbClearFilter()
dbCloseArea()

Return Nil

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Programa  �MR820Desc   � Autor �Felipe Nunes Toledo  � Data �28/09/06  ���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Atribui a descricao do produto conforme opcao selecionada   ���
���          �no parametro mv_par07 (Descricao do Produto ?).             ���
�������������������������������������������������������������������������Ĵ��
���Retorno   �Nenhum                                                      ���
�������������������������������������������������������������������������Ĵ��
���Parametros�ExpO1: Objeto Report do Relatorio                           ���
���          �ExpC1: Alias do arquivo Ordem de Producao                   ���
�������������������������������������������������������������������������Ĵ��
���Uso       �MATR820                                                     ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function MR820Desc(oReport, cAliasTop)
Local oSection1 := oReport:Section(1)
Local lSB1Desc 	:= .T.

If mv_par07 == 1
	SB5->(dbSetOrder(1))
	If SB5->(dbSeek(xFilial("SB5")+(cAliasTop)->C2_PRODUTO) .And. !Empty(B5_CEME))
		oSection1:Cell("B1_DESC"):GetFieldInfo("B5_CEME")
		oSection1:Cell("B1_DESC"):SetValue(SB5->B5_CEME)
		lSB1Desc := .F.
	EndIf
ElseIf mv_par07 == 3
	If (cAliasTop)->C2_DESTINA == "P"
		SC6->(dbSetOrder(1))
		If SC6->(dbSeek(xFilial("SC6")+(cAliasTop)->C2_PEDIDO+(cAliasTop)->C2_ITEM))
			If !Empty(SC6->C6_DESCRI) .and. SC6->C6_PRODUTO == (cAliasTop)->C2_PRODUTO
				oSection1:Cell("B1_DESC"):GetFieldInfo("C6_DESCRI")
				oSection1:Cell("B1_DESC"):SetValue(SC6->C6_DESCRI)
				lSB1Desc := .F.
			EndIf
		EndIf
	EndIf
EndIf

If (mv_par07 <> 2) .And. lSB1Desc
	SB1->(dbSetOrder(1))
	If SB1->( dbSeek(xFilial("SB1")+(cAliasTop)->C2_PRODUTO) )
		oSection1:Cell("B1_DESC"):GetFieldInfo("B1_DESC")
		oSection1:Cell("B1_DESC"):SetValue(SB1->B1_DESC)
	EndIf
EndIf

Return Nil

/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
��� Fun��o   � Mr820Ope � Autor � Felipe Nunes Toledo   � Data � 18/07/92 ���
�������������������������������������������������������������������������Ĵ��
��� Descri��o� Imprime Roteiro de Operacoes                               ���
�������������������������������������������������������������������������Ĵ��
��� Sintaxe  � Mr820Ope()                                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � MATR820                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
*/
Static Function Mr820Ope(oReport, cAliasTop, aProds)

Local oSection3   := oReport:Section(1):Section(2)
Local oSection4	:= oReport:Section(1):Section(2):Section(1)
Local oBreak
Local cRoteiro	:= ""
Local cSeekWhile:= ""
Local lSH8 		:= .F.
Local aArea   	:= GetArea()
Local lValidade := Iif(SG2->(FieldPos("G2_DTINI")) > 0,.T.,.F.)

TRCell():New(oSection3,'G2_RECURSO','SG2',/*Titulo*/,/*Picture*/,10 		   ,/*lPixel*/,/*{|| code-block de impressao }*/)
oBreak:= TRBreak():New(oSection3,oSection3:Cell("G2_RECURSO"),Nil,.F.)

//oBreak := TRBreak():New(oSection4,oSection4:Cell("H8_DTINI"),Nil,.F.)


cProdNew := aProds[1]

//�����������������������������������������������������������Ŀ
//� Verifica se imprime ROTEIRO da OP ou PADRAO do produto    �
//�������������������������������������������������������������
If !Empty((cAliasTop)->C2_ROTEIRO)
	cRoteiro:=(cAliasTop)->C2_ROTEIRO
Else
	If !Empty(SB1->B1_OPERPAD)
		cRoteiro:=SB1->B1_OPERPAD
	Else
//		If a630SeekSG2(1,(cAliasTop)->C2_PRODUTO,xFilial("SG2")+(cAliasTop)->C2_PRODUTO+"01")
		If a630SeekSG2(1,cProdNew,xFilial("SG2")+cProdNew+"01")
			cRoteiro:="01"
		EndIf
	EndIf
EndIf

cSeekWhile := "SG2->(G2_FILIAL+G2_PRODUTO+G2_CODIGO)"
//If a630SeekSG2(1,(cAliasTop)->C2_PRODUTO,xFilial("SG2")+(cAliasTop)->C2_PRODUTO+cRoteiro,@cSeekWhile)
If a630SeekSG2(1,cProdNew,xFilial("SG2")+cProdNew+cRoteiro,@cSeekWhile)

	oSection3:Init()
	oSection4:Init()

	While SG2->(!Eof()) .And. Eval(&cSeekWhile)
		SH8->(dbSetOrder(1))
//		If SH8->(dbSeek(xFilial("SH8")+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD)+SG2->G2_OPERAC))
		If SH8->(dbSeek(xFilial("SH8")+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD+SG2->G2_OPERAC))
			lSH8 := .T.
		EndIf

		If lSH8
//			While SH8->(!Eof()) .And. SH8->(H8_FILIAL+H8_OP+H8_OPER) == xFilial("SH8")+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN+C2_ITEMGRD)+SG2->G2_OPERAC
			While SH8->(!Eof()) .And. SH8->(H8_FILIAL+H8_OP+H8_OPER) == xFilial("SH8")+(cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN)+BDGRD->C2a_ITEMGRD+SG2->G2_OPERAC
				SH1->(dbSeek(xFilial("SH1")+SH8->H8_RECURSO))
				SH4->(dbSeek(xFilial("SH4")+SG2->G2_FERRAM))

				If lValidade .And. mv_par13 == 2
					If !Empty(SG2->G2_DTINI)
						If SG2->G2_DTINI > (cAliasTop)->C2_DATPRI
							SH8->(dbSkip())
							Loop
						EndIf
					EndIf
					If !Empty(SG2->G2_DTFIM)
						If SG2->G2_DTFIM < (cAliasTop)->C2_DATPRI
							SH8->(dbSkip())
							Loop
						EndIf
					EndIf
				EndIf

				oSection3:Cell('G2_RECURSO'):SetValue(SH8->H8_RECURSO)
				oSection3:Cell('H1_DESCRI' ):SetValue(SH1->H1_DESCRI)
				oSection3:Cell('G2_FERRAM' ):SetValue(SG2->G2_FERRAM)
				oSection3:Cell('H4_DESCRI' ):SetValue(SH4->H4_DESCRI)
				oSection3:Cell('G2_OPERAC' ):SetValue(SG2->G2_OPERAC)
				oSection3:Cell('G2_DESCRI' ):SetValue(SG2->G2_DESCRI)

				If lValidade
					oSection4:Cell('VldInici'):SetValue(SG2->G2_DTINI)
					oSection4:Cell('VldFinal'):SetValue(SG2->G2_DTFIM)
				EndIf

				oSection3:PrintLine()
				oSection4:PrintLine()
				oReport:ThinLine()
				SH8->(dbSkip())
			EndDo
		Else
			If lValidade .And. mv_par13 == 2
				If !Empty(SG2->G2_DTINI)
					If SG2->G2_DTINI > (cAliasTop)->C2_DATPRI
						SG2->(dbSkip())
						Loop
					EndIf
				EndIf
				If !Empty(SG2->G2_DTFIM)
					If SG2->G2_DTFIM < (cAliasTop)->C2_DATPRI
						SG2->(dbSkip())
						Loop
					EndIf
				EndIf
			EndIf

			SH1->(dbSeek(xFilial("SH1")+SG2->G2_RECURSO))
			SH4->(dbSeek(xFilial("SH4")+SG2->G2_FERRAM))

			oSection3:Cell('G2_RECURSO'):SetValue(SG2->G2_RECURSO)
			oSection3:Cell('H1_DESCRI' ):SetValue(SH1->H1_DESCRI)
			oSection3:Cell('G2_FERRAM' ):SetValue(SG2->G2_FERRAM)
			oSection3:Cell('H4_DESCRI' ):SetValue(SH4->H4_DESCRI)
			oSection3:Cell('G2_OPERAC' ):SetValue(SG2->G2_OPERAC)
			oSection3:Cell('G2_DESCRI' ):SetValue(SG2->G2_DESCRI)
			oSection3:PrintLine()

			oSection4:Cell('H8_DTINI'):Disable()
			oSection4:Cell('H8_HRINI'):Disable()
			oSection4:Cell('H8_DTFIM'):Disable()
			oSection4:Cell('H8_HRFIM'):Disable()
			oSection4:Cell('H8_QUANT'):SetValue(aSC2Sld(cAliasTop))
			If lValidade
				oSection4:Cell('VldInici'):SetValue(SG2->G2_DTINI)
				oSection4:Cell('VldFinal'):SetValue(SG2->G2_DTFIM)
			EndIf
			oSection4:PrintLine()
			oReport:ThinLine()

			oSection4:Cell('H8_DTINI'):Enable()
			oSection4:Cell('H8_HRINI'):Enable()
			oSection4:Cell('H8_DTFIM'):Enable()
			oSection4:Cell('H8_HRFIM'):Enable()
		Endif
		SG2->(dbSkip())
	EndDo

	oSection3:Finish()
	oSection4:Finish()
Endif

RestArea(aArea)
Return Nil



/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
��� Fun��o   � AddAr820 � Autor � Paulo Boschetti       � Data � 07/07/92 ���
�������������������������������������������������������������������������Ĵ��
��� Descri��o� Adiciona um elemento ao Array                              ���
�������������������������������������������������������������������������Ĵ��
��� Sintaxe  � AddAr820(ExpN1)                                            ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpN1 = Quantidade da estrutura                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � MATR820                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
*/
Static Function AddAr820(nQuantItem)
Local cDesc   := SB1->B1_DESC
Local cLocal  := ""
Local cKey    := ""
Local cRoteiro:= ""
Local nQtdEnd
Local lExiste

Local lVer116   := (VAL(GetVersao(.F.)) == 11 .And. GetRpoRelease() >= "R6" .Or. VAL(GetVersao(.F.))  > 11)

//�����������������������������������������������������������Ŀ
//� Verifica se imprime nome cientifico do produto. Se Sim    �
//� verifica se existe registro no SB5 e se nao esta vazio    �
//�������������������������������������������������������������
If mv_par07 == 1
	dbSelectArea("SB5")
	dbSeek(xFilial()+SB1->B1_COD)
	If Found() .and. !Empty(B5_CEME)
		cDesc := B5_CEME
	EndIf
ElseIf mv_par07 == 2
	cDesc := SB1->B1_DESC
Else
	//�����������������������������������������������������������Ŀ
	//� Verifica se imprime descricao digitada ped.venda, se sim  �
	//� verifica se existe registro no SC6 e se nao esta vazio    �
	//�������������������������������������������������������������
	If (cAliasTop)->C2_DESTINA == "P"
		dbSelectArea("SC6")
		dbSetOrder(1)
		dbSeek(xFilial()+(cAliasTop)->C2_PEDIDO+(cAliasTop)->C2_ITEM)
		If Found() .and. !Empty(C6_DESCRI) .and. C6_PRODUTO==SB1->B1_COD
			cDesc := C6_DESCRI
		ElseIf C6_PRODUTO # SB1->B1_COD
			dbSelectArea("SB5")
			dbSeek(xFilial()+SB1->B1_COD)
			If Found() .and. !Empty(B5_CEME)
				cDesc := B5_CEME
			EndIf
		EndIf
	EndIf
EndIf

//�����������������������������������������������������������Ŀ
//� Verifica se imprime ROTEIRO da OP ou PADRAO do produto    �
//�������������������������������������������������������������
If !Empty((cAliasTop)->C2_ROTEIRO)
	cRoteiro:=(cAliasTop)->C2_ROTEIRO
Else
	If !Empty(SB1->B1_OPERPAD)
		cRoteiro:=SB1->B1_OPERPAD
	Else
		dbSelectArea("SG2")
		If dbSeek(xFilial()+(cAliasTop)->C2_PRODUTO+"01")
			cRoteiro:="01"
		EndIf
	EndIf
EndIf

If lVer116
	dbSelectArea("NNR")
	dbSeek(xFilial()+SD4->D4_LOCAL)
Else
	dbSelectArea("SB2")
	dbSeek(xFilial()+SB1->B1_COD+SD4->D4_LOCAL)
EndIf

dbSelectArea("SD4")
cKey:=SD4->D4_COD+SD4->D4_LOCAL+SD4->D4_OP+SD4->D4_TRT+SD4->D4_LOTECTL+SD4->D4_NUMLOTE
cLocal:=SB2->B2_LOCALIZ
cProdBase := (cAliasTop)->C2_PRODUTO

lExiste := .F.

//If !lVer116
	DbSelectArea("SDC")
	DbSetOrder(2)
	DbSeek(xFilial("SDC")+cKey)
	//If !Eof() .And. SDC->(DC_PRODUTO+DC_LOCAL+DC_OP+DC_TRT+DC_LOTECTL+DC_NUMLOTE) == cKey
		While !Eof().And. SDC->(DC_PRODUTO+DC_LOCAL+DC_OP+DC_TRT+DC_LOTECTL+DC_NUMLOTE) == cKey
			cLocal  :=DC_LOCALIZ
			nQtdEnd :=DC_QTDORIG

			AADD(aArray, {SB1->B1_COD,cDesc,SB1->B1_TIPO,SB1->B1_UM,nQtdEnd,SD4->D4_LOCAL,cLocal,SD4->D4_TRT,cRoteiro,If(mv_par12 == 1,SD4->D4_LOTECTL,""),If(mv_par12 == 1,SD4->D4_NUMLOTE,""),cProdBase} )
			lExiste := .T.
			dbSkip()
		end
	//EndIf
//endif

dbSelectArea("SD4")

if !lExiste
	If lVer116
		AADD(aArray, {SB1->B1_COD,cDesc,SB1->B1_TIPO,SB1->B1_UM,nQuantItem,SD4->D4_LOCAL,NNR->NNR_DESCRI,SD4->D4_TRT,cRoteiro,If(mv_par12 == 1,SD4->D4_LOTECTL,""),If(mv_par12 == 1,SD4->D4_NUMLOTE,""),cProdBase } )
	Else
	    AADD(aArray, {SB1->B1_COD,cDesc,SB1->B1_TIPO,SB1->B1_UM,nQuantItem,SD4->D4_LOCAL,cLocal,SD4->D4_TRT,cRoteiro,If(mv_par12 == 1,SD4->D4_LOTECTL,""),If(mv_par12 == 1,SD4->D4_NUMLOTE,""),cProdBase } )
	EndIf
endif

Return
/*/
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
��� Fun��o   � MontStruc� Autor � Ary Medeiros          � Data � 19/10/93 ���
�������������������������������������������������������������������������Ĵ��
��� Descri��o� Monta um array com a estrutura do produto                  ���
�������������������������������������������������������������������������Ĵ��
��� Sintaxe  � MontStruc(ExpC1,ExpN1,ExpN2)                               ���
�������������������������������������������������������������������������Ĵ��
���Parametros� ExpC1 = Codigo do produto a ser explodido                  ���
���          � ExpN1 = Quantidade base a ser explodida                    ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � MATR820                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
/*/
Static Function MontStruc(cOp,nQuant)

dbSelectArea("SD4")
dbSetOrder(2)
dbSeek(xFilial()+cOp)

While !Eof() .And. D4_FILIAL+D4_OP == xFilial()+cOp
	//���������������������������������������������������������Ŀ
	//� Posiciona no produto desejado                           �
	//�����������������������������������������������������������
	dbSelectArea("SB1")
	If dbSeek(xFilial()+SD4->D4_COD)
		If SD4->D4_QUANT > 0 .Or. (lItemNeg .And. SD4->D4_QUANT < 0)
			AddAr820(SD4->D4_QUANT)
		EndIf
	Endif
	dbSelectArea("SD4")
	dbSkip()
Enddo

dbSetOrder(1)

Return


/*
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
��� Fun��o   � Mr820Ogr � Autor � Daniel R. Melo        � Data � 19/03/17 ���
�������������������������������������������������������������������������Ĵ��
��� Descri��o� Imprime a grade de produtos                                ���
�������������������������������������������������������������������������Ĵ��
��� Sintaxe  � Mr820Ogr()                                                 ���
�������������������������������������������������������������������������Ĵ��
���Parametros�                                                            ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � MATR820                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
*/
Static Function Mr820Ogr(oReport)

Local oSection2a := oReport:Section(1):Section(2)
Local oBreak
Local aArea   	:= GetArea()

//oBreak:= TRBreak():New(oSection2a,oSection2a:Cell("G2_RECURSO"),Nil,.F.)

//cProdNew := aProds[1]

dbSelectArea("BDGRD")
BDGRD->(Dbgotop())

If (cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN) == BDGRD->(C2a_NUM+C2a_ITEM+C2a_SEQUEN) .AND. SubString(BDGRD->C2a_PRODUTOX,1,2) <> "BN"		

	oSection2a:Init()

	While !BDGRD->(Eof())
	
		If (cAliasTop)->(C2_NUM+C2_ITEM+C2_SEQUEN) == BDGRD->(C2a_NUM+C2a_ITEM+C2a_SEQUEN) 
	
			oSection2a:Cell('C2a_PRODUTO'):SetValue(BDGRD->C2a_PRODUTO)
			oSection2a:Cell('C2a_COR'    ):SetValue(BDGRD->C2a_COR)
			oSection2a:Cell('C2a_TAMANHO'):SetValue(BDGRD->C2a_TAMANHO)
			oSection2a:Cell('C2a_QUANT'  ):SetValue(BDGRD->C2a_QUANT)
			oSection2a:Cell('C2a_QUJE'   ):SetValue(BDGRD->C2a_QUJE)
	
			oSection2a:PrintLine()
	
		End
		
		BDGRD->(dbSkip())
		
	EndDo

	oSection2a:Finish()

End
	
RestArea(aArea)

Return Nil


/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Funcao    �AjustaSX1 � Autor � Ricardo Berti         � Data �21/02/2008���
�������������������������������������������������������������������������Ĵ��
���Descri��o �Cria pergunta para o grupo			                      ���
�������������������������������������������������������������������������Ĵ��
��� Uso      � MATR820                                                    ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/
Static Function AjustaSX1()

Local aHelpPor := {	'Opcao para a impressao do produto com  ','rastreabilidade por Lote ou Sub-Lote.  '}
Local aHelpEng := {	'Option to print the product with       ','trackability by Lot or Sub-lot.        '}
Local aHelpSpa := {	'Opcion para la impresion del producto  ','con trazabilidad por Lote o Sublote.   '}

PutSx1("MTR820","12","Imprime Lote/S.Lote ?","�Imprime Lote/Subl. ?","Print Lot/Sublot ?","mv_chc","N",1,0,2,"C","",   "","","","mv_par12","Sim","Si","Yes","" ,"Nao","No","No","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)
PutSx1("MTR820","13","Almoxarifado de ?"    ,                     "",                  "","mv_chd","C",2,0,0,"G","","NNR","","","mv_par13",   "",  "",   "","" ,   "",  "",  "","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)
PutSx1("MTR820","14","Almoxarifado At� ?"   ,                     "",                  "","mv_che","C",2,0,0,"G","","NNR","","","mv_par14",   "",  "",   "","" ,   "",  "",  "","","","","","","","","","",aHelpPor,aHelpEng,aHelpSpa)

Return
