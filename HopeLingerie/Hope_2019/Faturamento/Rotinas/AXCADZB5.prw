#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} AXCADZB5
Fun��o de Teste
@Desc PROGRAMA PARA MONTAR TELA DE CADASTRO DA TABELA ZB5-TROCA PROD
@type function
@author R. Melo 
@since 09/10/2018
@version 1.0
/*/

user function AXCADZB5()
    Local aArea    := GetArea()
    Local aAreaZB5 := ZB5->(GetArea())
    Local cDelOk   := ".T."
    Local cFunTOk  := ".T."
     
    //Chamando a tela de cadastros
    AxCadastro('ZB5', 'Regra Troca de Produtos', cDelOk, cFunTOk)
     
    RestArea(aAreaZB5)
    RestArea(aArea)
Return

/*/{Protheus.doc} AXCADZB5
Fun��o de Teste
@Desc PROGRAMA UTILIZADO NO GATILHO ZB5_PRDNAC PARA BUSCAR O CODIGO DA SUBCOLECAO DO PRODUTO
@type function
@author R. Melo 
@since 09/10/2018
@version 1.0
/*/
User Function HZB5B1()
  Local aAreaSB1:= SB1->(GetArea())
  Local aAreaSB4:= SB4->(GetArea())
  Local cCodSubCol:= ""
  
  dbSelectArea("SB1")
  SB1->(dbSetOrder(1))
  SB1->(dbGoTop())
  
  If SB1->(DbSeek(xFilial("SB1")+ M->ZB5_PRDNAC))
  	cCodSubCol:= SB1->B1_YSUBCOL
  Endif
  
  RestArea(aAreaSB1)
  RestArea(aAreaSB4)
  
Return cCodSubCol 
  
/*/{Protheus.doc} AXCADZB5
Fun��o de Teste
@Desc PROGRAMA UTILIZADO NO GATILHO ZB5_PRDIMP PARA BUSCAR A DESCRICAO DO PRODUTO
@type function
@author R. Melo 
@since 09/10/2018
@version 1.0
/*/
User Function HZB5B2()
  Local aAreaSB1:= SB1->(GetArea())
  Local aAreaSB4:= SB4->(GetArea())
  Local cNomProd:= ""
  
  dbSelectArea("SB1")
  SB1->(dbSetOrder(1))
  SB1->(dbGoTop())
  
  If SB1->(DbSeek(xFilial("SB1")+ M->ZB5_PRDIMP))
  	cNomProd:= SB1->B1_DESC
  Endif
  
  RestArea(aAreaSB1)
  RestArea(aAreaSB4)
  
Return cNomProd     


/*/{Protheus.doc} AXCADZB5
Fun��o de Teste
@Desc PROGRAMA UTILIZADO NO GATILHO ZB5_PRDNAC PARA BUSCAR A DESCRICAO DO PRODUTO
@type function
@author R. Melo 
@since 09/10/2018
@version 1.0
/*/
User Function HZB5B3()
  Local aAreaSB1:= SB1->(GetArea())
  Local aAreaSB4:= SB4->(GetArea())
  Local cNomProd:= ""
  
  dbSelectArea("SB1")
  SB1->(dbSetOrder(1))
  SB1->(dbGoTop())
  
  If SB1->(DbSeek(xFilial("SB1")+ M->ZB5_PRDNAC))
  	cNomProd:= SB1->B1_DESC
  Endif
  
  RestArea(aAreaSB1)
  RestArea(aAreaSB4)
  
Return cNomProd       

/*/{Protheus.doc} AXCADZB5
Fun��o de Teste
@Desc PROGRAMA UTILIZADO NO GATILHO ZB5_PRDNAC PARA BUSCAR A DESCRICAO DA SUBCOLECAO DO PRODUTO
@type function
@author R. Melo 
@since 09/10/2018
@version 1.0
/*/
User Function HZB5B4()
  Local aAreaSB1:= SB1->(GetArea())
  Local aAreaSB4:= SB4->(GetArea())
  Local cNomSubCol:= ""
  
  dbSelectArea("SB1")
  SB1->(dbSetOrder(1))
  SB1->(dbGoTop())
  
  If SB1->(DbSeek(xFilial("SB1")+ M->ZB5_PRDNAC))
  	cNomSubCol:= SB1->B1_YDESSCO
  Endif
  
  RestArea(aAreaSB1)
  RestArea(aAreaSB4)
  
Return cNomSubCol                                                                               