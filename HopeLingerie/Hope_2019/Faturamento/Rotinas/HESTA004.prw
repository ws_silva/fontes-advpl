#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTA004  �Autor  �Bruno Parreira      � Data �  23/04/17   ���
�������������������������������������������������������������������������͹��
���Desc.     � Abastecimento preventivo.                                  ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HESTA004()
Local cPerg   := "HESTA004"
Private cLog  := ""
Private cCodAbast := ""

AjustaSX1(cPerg)�

If !Pergunte(cPerg)
	Return
EndIf

If !MsgYesNo("Confirma o abastecimento preventivo para a Esta��o: "+mv_par01+" ?","Confirma��o")
	Return
EndIf

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

cLog += "Processo de abastecimento preventivo para Esta��o: "+mv_par01+" iniciado."

Processa( {|| ABASTECE() }, "Aguarde...", "Processando...",.F.)

MemoWrite("\log_abast\"+cDtTime+"_ABAST"+cCodAbast+".log",cLog)

Return

Static Function ABASTECE()
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cAmzPul := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao
Local cCodAtiv := 0 //C�digo de Ativa��o
Local cSenha   := '' 
Local nReg := ''
Local cQuery   := ""
Local lContinua := .F.

//TODO Inserido c�digo de Ativa��o para realizar abastecimento. Ronaldo Pereira	21/03/2019
cCodAtiv := GetMV("MV_XCODATI")
cSenha   := AllTrim(mv_par05)
If cCodAtiv <> cSenha 
	lContinua := .F.
	Else
	lContinua := .T.
EndIf

//TODO Gerar abastecimento por Esta�ao e Bloco. Ronaldo Pereira 31/01/2019
If Select("TMPSBE") > 0
	TMPSBE->(DbCloseArea())
EndIf

	cQuery := "SELECT BE.BE_LOCAL, BE.BE_LOCALIZ, BE.BE_CODPRO, BE.BE_XCODEST, BE.BE_XBLOCO, BE_XQTDREC, B5.B5_QE1 AS FATOR " 
	cQuery += "FROM "+RetSqlName("SBE")+" AS BE WITH (NOLOCK) "
	cQuery += "LEFT JOIN "+RetSqlName("SB5")+" AS B5 WITH (NOLOCK) ON (B5.B5_COD = BE.BE_CODPRO AND B5.D_E_L_E_T_ = '') " 
	cQuery += "WHERE BE.D_E_L_E_T_ = '' AND BE.BE_FILIAL = '0101' AND BE.BE_LOCAL = 'E0' "
	cQuery += "AND BE.BE_XCODEST = '"+mv_par01+"' " 
	cQuery += "AND BE.BE_XBLOCO BETWEEN '"+mv_par02+"' AND '"+mv_par03+"' "
	cQuery += "AND BE.BE_XLADO = '"+mv_par04+"' " 	
	cQuery += "ORDER BY BE.BE_XBLOCO, BE.BE_LOCALIZ 
	
	DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSBE", .F., .T.)
	
	DbSelectArea("TMPSBE")
	
If lContinua	
  If !Empty(TMPSBE->BE_XCODEST) .And. !Empty(TMPSBE->BE_XBLOCO)
	cParAbast := GetMV("MV_XCODABA")
	cCodAbast := "A"+cParAbast
	cItem := "01"
	While TMPSBE->(!EOF()) //.And. SBE->BE_LOCAL+SBE->BE_XCODEST+SBE->BE_XBLOCO = cAmzPik+cCodEst+cBloco
		If !Empty(TMPSBE->BE_CODPRO) .AND. TMPSBE->FATOR > 0
			nFator  := TMPSBE->FATOR     //Fator de conversao (quantas pecas cabem no recipiente)
			nQtdRec := TMPSBE->BE_XQTDREC    //Quantidade de recipeientes que cabem no endere�o
			
			nSldPik := HESTSALDO(TMPSBE->BE_CODPRO,cAmzPik) //Saldo do produto no picking
			
			nQtdAtu := nSldPik / nFator  //Quantidade de recipientes atualmente
			
			If nQtdAtu > int(nQtdAtu)
				nQtdAtu := int(nQtdAtu) + 1
			Else
				nQtdAtu := int(nQtdAtu)
			EndIf
			
			If nQtdAtu < nQtdRec
				nQtdFalt := nQtdRec - nQtdAtu   //Quantidade de recipientes que estao faltando no endere�o do picking
				nSldPos  := nQtdAtu * nFator    //Quantidade possivel de pecas com os recipientes atuais
				nSldFalt := nQtdFalt * nFator   //Quantidade de pecas faltantes para transferir para o picking
				
				nSldPul  := HESTSALDO(TMPSBE->BE_CODPRO,cAmzPul)  //Saldo de pecas no pulmao
					
				If nSldPul <= 0
					IncProc()
					TMPSBE->(DbSkip())
					Loop
				EndIf

				U_HTRANSF(TMPSBE->BE_CODPRO,nSldFalt,cAmzPul,cAmzPik,cCodAbast,cItem,cCodAbast) //Faz a transferencia da quantidade faltante do pulmao para o picking
				cItem := SOMA1(cItem,2)
			EndIf
		EndIf
		IncProc()
		TMPSBE->(DbSkip())
	EndDo
	cPrxAbast := SOMA1(cParAbast,5) 
	PutMV("MV_XCODABA",cPrxAbast)
	
	MsgInfo("Abastecimento: "+cCodAbast+" realizado com sucesso!","Aviso")
  Else
	 MsgAlert("Esta��o n�o encontrada!","Aten��o")	
  EndIf 

	While TMPSBE->(!EOF()) 
		If !Empty(TMPSBE->BE_CODPRO) .And. TMPSBE->FATOR > 0
			nReg++
		EndIf
		TMPSBE->(DbSkip())
	EndDo
		
	ProcRegua(nReg)
	
Else
	MsgAlert("Codigo de Ativa��o Errado!","Aten��o")
EndIf	

TMPSBE->(DbCloseArea())

Return

Static Function HESTSALDO(cProduto,cArmazem) 
Local nRet := 0

DbSelectArea("SB2")
DbSetOrder(1)
If DbSeek(xFilial("SB2")+cProduto+cArmazem)
	nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()	
EndIf

Return nRet

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Codigo da Estacao","","","Mv_ch1",TAMSX3("BE_XCODEST")[3],TAMSX3("BE_XCODEST")[1],TAMSX3("BE_XCODEST")[2],0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"Codigo da estacao.",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)