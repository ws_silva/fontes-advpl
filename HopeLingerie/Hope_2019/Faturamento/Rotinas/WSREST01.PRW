#INCLUDE "TOTVS.CH"
#INCLUDE "RESTFUL.CH"
#Include "TOPCONN.CH"

/*/{Protheus.doc} WsRest01
//Integração WS Rest Protheus x Lincros
@author Robson Melo
@since 26/03/2019
@version 1.0
@type function
/*/

User Function WsRest01()


//Local cUrl:= "https://homolog.transpofrete.com.br/api/pedido/gravarPedido/?token=748B3F322A3FDF054F4AEB9A40712F43"
Private oRestClient := FWRest():New("https://homolog.transpofrete.com.br/api")
Private aHeader:= {}
Private cUser:= "hope.ws"
Private cPass:= "123456a"
//"748B3F322A3FDF054F4AEB9A40712F43"  


//aadd(aHeader,"GET /rest/sample")
//aadd(aHeader,"Host: https://homolog.transpofrete.com.br")
//aadd(aHeader,"Content-Type: application/json")
//aadd(aHeader,"Authorization: BASIC " + Encode64(cUser+":"+cPass)) 
aadd(aHeader,'Content-Type: application/json')

//oRestClient:setPath("/api/pedido/gravarPedido/?token=" + Encode64(cUser+":"+cPass)) 
oRestClient:setPath("/auth/login/"+Encode64(cUser+":"+cPass)) 

If oRestClient:Get()
   Alert(oRestClient:GetResult())
Else
   Alert(oRestClient:GetLastError())
Endif

Return

