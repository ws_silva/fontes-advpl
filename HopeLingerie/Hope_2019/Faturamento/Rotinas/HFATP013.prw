#Include "PROTHEUS.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"

User Function HFATP013()

	local cVldAlt := ".T."
	local cVldExc := ".T."
	local cAlias
	Private oMSNewGe1
	
	cAlias := "SZY"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Regra de libera��o dos Usu�rios"
	aRotina := {;
		{ "Pesquisar" , "AxPesqui", 0, 1},;
		{ "Visualizar", "u_PolCo1(4)", 0, 2},;
		{ "Alterar"   , "u_PolCo1(2)", 0, 4},;
		{ "Exlcuir"   , "u_PolCo1(3)", 0, 5},;
		{ "Incluir"   , "u_PolCo1(1)", 0, 3};
		}

	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)
	
return

User Function PolCo1(_tp)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oCheckBo01
	Local oCheckBo02
	Local oCheckBo03
	Local oCheckBo04
	Local oCheckBo05
	Local oCheckBo06
	Local oCheckBo07
	Local oCheckBo08
	Local oCheckBo09
	Local oCheckBo10
	Local oCheckBo11
	Local oCheckBo12
	Local oCheckBo13
	Local oCheckBo14
	Local oCheckBo15
	Local oCheckBo16
	Local oCheckBo17
//	Local oCheckBo18
	Local oSay1
	Local oSay2
	Private oFolder1
	Private cGet1			:= Space(6)
	Private cGet2			:= Space(40)
	Private lCheckBo01	:= .F.
	Private lCheckBo02	:= .F.
	Private lCheckBo03	:= .F.
	Private lCheckBo04	:= .F.
	Private lCheckBo05	:= .F.
	Private lCheckBo06	:= .F.
	Private lCheckBo07	:= .F.
	Private lCheckBo08	:= .F.
	Private lCheckBo09	:= .F.
	Private lCheckBo10	:= .F.
	Private lCheckBo11	:= .F.
	Private lCheckBo12	:= .F.
	Private lCheckBo13	:= .F.
	Private lCheckBo14	:= .F.
	Private lCheckBo15	:= .F.
	Private lCheckBo16	:= .F.
	Private lCheckBo17	:= .F.
//	Private lCheckBo18	:= .F.

	Static oDlg

	if __cuserid $ GETMV("HP_LIBUSER") 

	If _tp <> 1
		cGet1 := SZY->ZY_CODUSU
		cGet2 := SZY->ZY_NOME
		lCheckBo01  := If(SZY->ZY_CHEDEV  ="S",.T.,.F.)	//Cheque Devolvido
		lCheckBo02  := If(SZY->ZY_CLIENTE ="S",.T.,.F.)	//Cliente
		lCheckBo03  := If(SZY->ZY_CLIINAT ="S",.T.,.F.)	//Cliente Inativo
		lCheckBo04  := If(SZY->ZY_CLINOVO ="S",.T.,.F.)	//Cliente Novo
		lCheckBo05  := If(SZY->ZY_CONDPAG ="S",.T.,.F.)	//Condi��o de Pagamento
		lCheckBo06  := If(SZY->ZY_CTPERDA ="S",.T.,.F.)	//Conta Perdas
		lCheckBo07  := If(SZY->ZY_DINCOMP ="S",.T.,.F.)	//Dados Incompletos
		lCheckBo08  := If(SZY->ZY_LMULTRA ="S",.T.,.F.)	//Limite Ultrapassado
		lCheckBo09  := If(SZY->ZY_NOTADEB ="S",.T.,.F.)	//Nota Debito
		lCheckBo10  := If(SZY->ZY_OSBPED  ="S",.T.,.F.)	//Observa��o do Pedido
		lCheckBo11  := If(SZY->ZY_VALPED  ="S",.T.,.F.)	//Valor do Pedido
		lCheckBo12  := If(SZY->ZY_PRODINA ="S",.T.,.F.)	//Produto inativo
		lCheckBo13  := If(SZY->ZY_TBPRECO ="S",.T.,.F.)	//Tabela de Pre�o
		lCheckBo14  := If(SZY->ZY_TITATRA ="S",.T.,.F.)	//Titulos em Atraso (em Aberto Vecidos)
		lCheckBo15  := If(SZY->ZY_TITATRP ="S",.T.,.F.)	//Titulos em Atraso (Pagos com Atraso)
		lCheckBo16  := If(SZY->ZY_TITPROT ="S",.T.,.F.)	//Titulos em Protesto
		lCheckBo17  := If(SZY->ZY_PEDB2B  ="S",.T.,.F.)	//Pedidos B2B
//  	lCheckBo18  := If(SZY->ZY_PRZADIT ="S",.T.,.F.)	//Prazo Adicional

	Endif

	DEFINE MSDIALOG oDlg TITLE "Regra de Libera��o de Pedidos" FROM 000, 000  TO 500, 900 COLORS 0, 16777215 PIXEL

	@ 012, 015 SAY oSay1 PROMPT "Codigo Usu�rio" SIZE 040, 008 OF oDlg COLORS 0, 16777215 PIXEL
	If _tp <> 1
		@ 010, 060 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 F3 "USR" READONLY PIXEL
	Else
		@ 010, 060 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg VALID U_VldRLU(1,cGet1) COLORS 0, 16777215 F3 "USR" PIXEL
	Endif
	@ 012, 145 SAY oSay1 PROMPT "Usu�rio" SIZE 035, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 010, 172 MSGET oGet2 VAR cGet2 SIZE 200, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	@ 030, 015 CHECKBOX oCheckBo01 VAR lCheckBo01 		PROMPT "Cheque Devolvido" 							SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 155 CHECKBOX oCheckBo02 VAR lCheckBo02 		PROMPT "Cliente" 										SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 030, 295 CHECKBOX oCheckBo03 VAR lCheckBo03 		PROMPT "Cliente Inativo" 							SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 045, 015 CHECKBOX oCheckBo04 VAR lCheckBo04 		PROMPT "Cliente Novo"	 							SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 045, 155 CHECKBOX oCheckBo05 VAR lCheckBo05 		PROMPT "Condi��o de Pagamento" 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 045, 295 CHECKBOX oCheckBo06 VAR lCheckBo06 		PROMPT "Conta Perdas"		 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 060, 015 CHECKBOX oCheckBo07 VAR lCheckBo07 		PROMPT "Dados Incompletos"	 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 060, 155 CHECKBOX oCheckBo08 VAR lCheckBo08 		PROMPT "Limite Ultrapassado" 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 060, 295 CHECKBOX oCheckBo09 VAR lCheckBo09 		PROMPT "Nota Debito" 								SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 075, 015 CHECKBOX oCheckBo10 VAR lCheckBo10 		PROMPT "Observa��o do Pedido" 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 075, 155 CHECKBOX oCheckBo11 VAR lCheckBo11 		PROMPT "Valor do Pedido"								SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 075, 295 CHECKBOX oCheckBo12 VAR lCheckBo12 		PROMPT "Produto inativo"		 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 090, 015 CHECKBOX oCheckBo13 VAR lCheckBo13 		PROMPT "Tabela de Pre�o" 							SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 090, 155 CHECKBOX oCheckBo14 VAR lCheckBo14 		PROMPT "Titulos em Atraso (em Aberto Vecidos)" 	SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 090, 295 CHECKBOX oCheckBo15 VAR lCheckBo15 		PROMPT "Titulos em Atraso (Pagos com Atraso)"	 	SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 105, 015 CHECKBOX oCheckBo16 VAR lCheckBo16 		PROMPT "Titulos em Protesto"					 	SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 105, 155 CHECKBOX oCheckBo17 VAR lCheckBo17 		PROMPT "Pedido B2B"			 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL
//	@ 105, 295 CHECKBOX oCheckBo18 VAR lCheckBo18 		PROMPT "Prazo Adicional"		 						SIZE 120, 008 OF oDlg COLORS 0, 16777215 PIXEL

	@ 120, 010 FOLDER oFolder1 								SIZE 433, 110 OF oDlg ITEMS "Tipo de Pedido","Regional" COLORS 0, 16777215 PIXEL
	fMSNewGe1(_tp)
	fMSNewGe2(_tp)
	If _tp == 4
		@ 233, 388 BUTTON oButton2 PROMPT "Cancelar" 		SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL
	Else
		@ 233, 388 BUTTON oButton1 PROMPT "Salvar" 			SIZE 037, 012 ACTION (Save(_tp),oDlg:End()) OF oDlg PIXEL
		@ 233, 344 BUTTON oButton2 PROMPT "Cancelar" 		SIZE 037, 012 ACTION oDlg:End()  OF oDlg PIXEL
	Endif

	ACTIVATE MSDIALOG oDlg CENTERED

	Else
		ALERT("Voc� n�o possui permiss�o para acessar essa rotina")
	Endif

Return

//------------------------------------------------ 
Static Function fMSNewGe1(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZZ_TPPED","ZZ_DESCRIC"}
	Local aAlterFields := {"ZZ_TPPED"}
	Private aHeader1 := {}
	Private aCols1 := {}

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek("ZZ_TPPED")) .AND. aFields[nX]=="ZZ_TPPED"
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldRLU(2,M->ZZ_TPPED)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader1, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("SZZ")
		DbSetOrder(1)
		DbSeek(xfilial("SZZ")+SZY->ZY_CODUSU)

		If Found()
			While !EOF() .and. SZZ->ZZ_USUARIO == SZY->ZY_CODUSU
				Aadd(aCols1,{SZZ->ZZ_TPPED,SZZ->ZZ_DESCRIC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols1, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols1, aFieldFill)
	Endif
  
	oMSNewGe1 := MsNewGetDados():New( 001, 001, 091, 430, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[1], aHeader1, aCols1)

Return


//------------------------------------------------ 
Static Function fMSNewGe2(_tp)
//------------------------------------------------ 
	Local nX
	Local aFieldFill := {}
	Local aFields := {"ZZ4_REGIAO","ZZ4_DESCRIC"}
	Local aAlterFields := {"ZZ4_REGIAO"}
	Private aHeader2 := {}
	Private aCols2 := {}

	Static oMSNewGe2

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	For nX := 1 to Len(aFields)
		If SX3->(DbSeek("ZZ4_REGIAO")) .AND. aFields[nX]=="ZZ4_REGIAO"
			Aadd(aHeader2, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,"U_VldRLU(3,M->ZZ4_REGIAO)",;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		ElseIf SX3->(DbSeek(aFields[nX]))
			Aadd(aHeader2, {AllTrim(X3Titulo()),SX3->X3_CAMPO,SX3->X3_PICTURE,SX3->X3_TAMANHO,SX3->X3_DECIMAL,SX3->X3_VALID,;
				SX3->X3_USADO,SX3->X3_TIPO,SX3->X3_F3,SX3->X3_CONTEXT,SX3->X3_CBOX,SX3->X3_RELACAO})
		Endif
	Next nX

  // Define field values
	If _tp <> 1
		DbSelectArea("ZZ4")
		DbSetOrder(1)
		DbSeek(xfilial("ZZ4")+SZY->ZY_CODUSU)

		If Found()
			While !EOF() .and. ZZ4->ZZ4_USER == SZY->ZY_CODUSU
				Aadd(aCols2,{ZZ4->ZZ4_REGIAO,ZZ4->ZZ4_DESCRIC,.F.})
				DbSkip()
			End
		Else
			For nX := 1 to Len(aFields)
				DbSelectArea("SX3")
				SX3->(DbSetOrder(2))
				If DbSeek(aFields[nX])
					Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
				Endif
			Next nX
			Aadd(aFieldFill, .F.)
			Aadd(aCols2, aFieldFill)
		Endif
	Else
		For nX := 1 to Len(aFields)
			DbSelectArea("SX3")
			SX3->(DbSetOrder(2))
			If DbSeek(aFields[nX])
				Aadd(aFieldFill, CriaVar(SX3->X3_CAMPO))
			Endif
		Next nX
		Aadd(aFieldFill, .F.)
		Aadd(aCols2, aFieldFill)
	Endif
  
	oMSNewGe2 := MsNewGetDados():New( 001, 001, 091, 430, GD_INSERT+GD_DELETE+GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oFolder1:aDialogs[2], aHeader2, aCols2)

Return


Static Function Save(_tp)

	Local I

	DbSelectArea("SZY")
	DbSetOrder(1)
	DbSeek(xfilial("SZY")+cGet1)


	If _tp <> 3 .or. _tp <> 4
		If !Found()
			RecLock("SZY",.T.)
			Replace ZY_FILIAL			with xfilial("SZY")
			Replace ZY_CODUSU			with cGet1
			Replace ZY_NOME			with cGet2
		Else
			RecLock("SZY",.F.)
		Endif
		Replace ZY_CHEDEV			with If(lCheckBo01=.T.,"S","N")			//Cheque Devolvido
		Replace ZY_CLIENTE		with If(lCheckBo02=.T.,"S","N")			//Cliente
		Replace ZY_CLIINAT		with If(lCheckBo03=.T.,"S","N")			//Cliente Inativo
		Replace ZY_CLINOVO		with If(lCheckBo04=.T.,"S","N")			//Cliente Novo
		Replace ZY_CONDPAG		with If(lCheckBo05=.T.,"S","N")			//Condi��o de Pagamento
		Replace ZY_CTPERDA		with If(lCheckBo06=.T.,"S","N")			//Conta Perdas
		Replace ZY_DINCOMP		with If(lCheckBo07=.T.,"S","N")			//Dados Incompletos
		Replace ZY_LMULTRA		with If(lCheckBo08=.T.,"S","N")			//Limite Ultrapassado
		Replace ZY_NOTADEB		with If(lCheckBo09=.T.,"S","N")			//Nota Debito
		Replace ZY_OSBPED			with If(lCheckBo10=.T.,"S","N")			//Observa��o do Pedido
		Replace ZY_VALPED			with If(lCheckBo11=.T.,"S","N")			//Valor do Pedido
		Replace ZY_PRODINA		with If(lCheckBo12=.T.,"S","N")			//Produto inativo
		Replace ZY_TBPRECO		with If(lCheckBo13=.T.,"S","N")			//Tabela de Pre�o
		Replace ZY_TITATRA		with If(lCheckBo14=.T.,"S","N")			//Titulos em Atraso (em Aberto Vecidos)
		Replace ZY_TITATRP		with If(lCheckBo15=.T.,"S","N")			//Titulos em Atraso (Pagos com Atrazo)
		Replace ZY_TITPROT		with If(lCheckBo16=.T.,"S","N")			//Titulos em Protesto
		Replace ZY_PEDB2B			with If(lCheckBo17=.T.,"S","N")			//Pedidos B2B
//		Replace ZY_PRZADIT		with If(lCheckBo18=.T.,"S","N")			//Prazo Adicional
		MsUnLock()
	End
	
	If _tp == 1
		For i := 1 to Len(OMSNEWGE1:ACOLS)
			If OMSNEWGE1:ACOLS[i,len(OMSNEWGE1:aheader)+1] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
				RecLock("SZZ",.T.)
				Replace ZZ_FILIAL			with xfilial("SZZ")
				Replace ZZ_USUARIO		with cGet1
				Replace ZZ_TPPED			with OMSNEWGE1:ACOLS[i,1]
				Replace ZZ_DESCRIC		with OMSNEWGE1:ACOLS[i,2]
				MsUnLock()
			Endif
		Next

		For i := 1 to Len(OMSNEWGE2:ACOLS)
			If OMSNEWGE2:ACOLS[i,len(OMSNEWGE2:aheader)+1] == .F. .and. alltrim(OMSNEWGE2:ACOLS[i,1]) <> ""
				RecLock("ZZ4",.T.)
				Replace ZZ4_FILIAL		with xfilial("ZZ4")
				Replace ZZ4_USER			with cGet1
				Replace ZZ4_REGIAO		with OMSNEWGE2:ACOLS[i,1]
				Replace ZZ4_DESCRIC		with OMSNEWGE2:ACOLS[i,2]
				MsUnLock()
			Endif
		Next

	ElseIf _tp == 2
		For i := 1 to Len(OMSNEWGE1:ACOLS)
			If OMSNEWGE1:ACOLS[i,len(OMSNEWGE1:aheader)+1] == .F. .and. alltrim(OMSNEWGE1:ACOLS[i,1]) <> ""
				DbSelectArea("SZZ")
				DbSetOrder(1)
				DbSeek(xfilial("SZZ")+cGet1+OMSNEWGE1:ACOLS[i,1])
			
				If !Found()
					RecLock("SZZ",.T.)
					Replace ZZ_FILIAL			with xfilial("SZZ")
					Replace ZZ_USUARIO			with cGet1
					Replace ZZ_TPPED			with OMSNEWGE1:ACOLS[i,1]
					Replace ZZ_DESCRIC			with OMSNEWGE1:ACOLS[i,2]
					MsUnLock()
				Endif
			Else
				DbSelectArea("SZZ")
				DbSetOrder(1)
				DbSeek(xfilial("SZZ")+cGet1+OMSNEWGE1:ACOLS[i,1])
			
				If Found()
					RecLock("SZZ",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next

		For i := 1 to Len(OMSNEWGE2:ACOLS)
			If OMSNEWGE2:ACOLS[i,len(OMSNEWGE2:aheader)+1] == .F. .and. alltrim(OMSNEWGE2:ACOLS[i,1]) <> ""
				DbSelectArea("ZZ4")
				DbSetOrder(1)
				DbSeek(xfilial("ZZ4")+cGet1+OMSNEWGE2:ACOLS[i,1])
			
				If !Found()
					RecLock("ZZ4",.T.)
					Replace ZZ4_FILIAL			with xfilial("ZZ4")
					Replace ZZ4_USER			with cGet1
					Replace ZZ4_REGIAO			with OMSNEWGE2:ACOLS[i,1]
					Replace ZZ4_DESCRIC			with OMSNEWGE2:ACOLS[i,2]
					MsUnLock()
				Endif
			Else
				DbSelectArea("ZZ4")
				DbSetOrder(1)
				DbSeek(xfilial("ZZ4")+cGet1+OMSNEWGE2:ACOLS[i,1])
			
				If Found()
					RecLock("ZZ4",.F.)
					DbDelete()
					MsUnLock()
				Endif
			Endif
		Next

	ElseIf _tp == 3
		DbSelectArea("SZY")
		DbSetOrder(1)
		DbSeek(xfilial("SZY")+cGet1)

		If Found()
			RecLock("SZY",.F.)
			DbDelete()
			MsUnLock()
		Endif
	
		DbSelectArea("SZZ")
		DbSetOrder(1)
		DbSeek(xfilial("SZZ")+cGet1)
	
		While !EOF() .and. SZZ->ZZ_USUARIO == cGet1
			RecLock("SZZ",.F.)
			DbDelete()
			MsUnlock()
			DbSkip()
		End
		
		DbSelectArea("ZZ4")
		DbSetOrder(1)
		DbSeek(xfilial("ZZ4")+cGet1)
	
		While !EOF() .and. ZZ4->ZZ4_USER == cGet1
			RecLock("ZZ4",.F.)
			DbDelete()
			MsUnlock()
			DbSkip()
		End
		
	Endif

Return


User Function VldRLU(_nItem,_Cod)

	Local lRet := .T.
	Local _i

	If _nItem == 1
		cGet2	:= UPPER(UsrFullName(cGet1))
		If Alltrim(cGet1)==""
			lRet := .F.
			Return(lRet)
		End
	
	ElseIf _nItem == 2
  
		For _i:=1 to len(oMSNewGe1:aCols)
			If alltrim(_Cod) == alltrim(oMSNewGe1:aCols[_i,1]) .and. _i <> oMSNewGe1:nAT
				Alert("O Tipo de Pedido j� foi incluido, selecione outra Tipo.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(_Cod) == ""
				Alert("N�o poder� incluir uma linha em branco.")
				lRet := .F.
				Return(lRet)
			Else
				If Select("VLDDB") > 0
					VLDDB->(DbCloseArea())
				EndIf
		 	
				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("SZ1")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND Z1_CODIGO='"+_Cod+"'"
			
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
				If VLDDB->COUNT==0 .and. _Cod<>""
					Alert("A Tipo de Pedido digitado n�o existe no cadastro.")
					lRet := .F.
					VLDDB->(DbCloseArea())
					Return(lRet)
				End
			
				VLDDB->(DbCloseArea())
			Endif
		Next

	ElseIf _nItem == 3
  
		For _i:=1 to len(oMSNewGe2:aCols)
			If alltrim(_Cod) == alltrim(oMSNewGe2:aCols[_i,1]) .and. _i <> oMSNewGe2:nAT
				Alert("A regiao j� foi incluida, selecione outra Regi�o.")
				lRet := .F.
				Return(lRet)
			ElseIf alltrim(_Cod) == ""
				Alert("N�o poder� incluir uma linha em branco.")
				lRet := .F.
				Return(lRet)
			Else
				If Select("VLDDB") > 0
					VLDDB->(DbCloseArea())
				EndIf
		 	
				cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM "+RetSQLName("ZA4")
				cQuery  += " WHERE D_E_L_E_T_<>'*' AND ZA4_CODMAC='"+_Cod+"'"
			
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"VLDDB",.T.,.T.)
			
				If VLDDB->COUNT==0 .and. _Cod<>""
					Alert("A Regi�o digitada n�o existe no cadastro.")
					lRet := .F.
					VLDDB->(DbCloseArea())
					Return(lRet)
				End
			
				VLDDB->(DbCloseArea())
			Endif
		Next

	End

Return(lRet)
