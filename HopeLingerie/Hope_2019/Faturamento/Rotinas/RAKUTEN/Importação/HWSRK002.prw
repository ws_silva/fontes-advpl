#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"
#include "fileio.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HWSRK001  �Autor  �Bruno Parreira      � Data �  01/12/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa utilizado para importar os pedidos de venda B2B    ���
���          �da RAKUTEN                                                  ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HWSRK002(lAut)
Local aWsRet   := {}
Local cWsRet     := ""

Private oWs      := NIL
Private cLog     := ""
Private cPerg    := "HWSRK002"
Private cTime    := ""
Private cDtTime  := ""
Private cPlB2BV  := ""
Private cTpB2BV  := ""
Private cPlB2BF  := ""
Private cTpB2BF  := ""
Private cPlB2BH  := ""
Private cTpB2BH  := ""
Private cPlB2BL  := ""
Private cTpB2BL  := ""
Private cPlB2B1  := ""
Private cTpB2B1  := ""
Private cAmzPk   := ""
Private cTpOper  := ""
Private cChvA1   := ""
Private cChvA2   := ""
Private nErro    := 0
Private nGerados := 0
Private cPolBonif := ""
Private cTESBonif := ""
Private cTipBonif := ""

Default lAut     := .F.

lMenu := .f.
/*If Select("SX2") <> 0
	lMenu := .t.
Else
	Prepare Environment Empresa "01" Filial "0101"
Endif*/

If !lAut
	lMenu := .t.
Endif

cTime   := Time()
cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)

MemoWrite("\log_rakuten\B2B\"+cDtTime+"_IMP.log","In�cio do processamento...")

nHandle := fopen("\log_rakuten\B2B\"+cDtTime+"_IMP.log",FO_READWRITE+FO_SHARED)

GravaLog("In�cio do processamento...") 

MemoWrite("\log_rakuten\B2B\ERROS_IMPORTACAO\"+cDtTime+"_ERROS.log","ERROS...")

nHandleErr := fopen("\log_rakuten\B2B\ERROS_IMPORTACAO\"+cDtTime+"_ERROS.log",FO_READWRITE+FO_SHARED)

oWsClient := WSPedido():New()

//cChvA1  := AllTrim(SuperGetMV("MV_XRA1B2C",.F.,"8B3CD696-D70A-4E59-8E2D-24A96ED8115E")) //Chave A1 para validar acesso ao webservice
//cChvA2  := AllTrim(SuperGetMV("MV_XRA2B2C",.F.,"2DD1CBF7-4B95-43BA-9F16-2C295E80E8CF")) //Chave A2 para validar acesso ao webservice
//cCnB2BV := AllTrim(SuperGetMV("MV_XRCN2BV",.F.,"007")) //Canal de vendas dos clientes do B2B Varejo
cPlB2BV := AllTrim(SuperGetMV("MV_XRPL2BV",.F.,"001")) //Politica comercial dos pedidos B2B Varejo
cTpB2BV := AllTrim(SuperGetMV("MV_XRTP2BV",.F.,"032")) //Tipo de Pedido dos pedidos B2B Varejo
cPlB2BF := AllTrim(SuperGetMV("MV_XRPL2BF",.F.,"050")) //Politica comercial dos pedidos B2B Franquia
cTpB2BF := AllTrim(SuperGetMV("MV_XRTP2BF",.F.,"030")) //Tipo de Pedido dos pedidos B2B Franquia
cPlB2BH := AllTrim(SuperGetMV("MV_XRPL2BH",.F.,"050")) //Politica comercial dos pedidos B2B Hope Store
cTpB2BH := AllTrim(SuperGetMV("MV_XRTP2BH",.F.,"033")) //Tipo de Pedido dos pedidos B2B Hope Store
cPlB2BL := AllTrim(SuperGetMV("MV_XRPL2BL",.F.,"053")) //Politica comercial dos pedidos B2B Loja Propria
cTpB2BL := AllTrim(SuperGetMV("MV_XRTP2BL",.F.,"047")) //Tipo de Pedido dos pedidos B2B Loja Propria
cPlB2B1 := AllTrim(SuperGetMV("MV_XRPL2B1",.F.,"054")) //Politica comercial dos pedidos B2B Hope 1.0 
cTpB2B1 := AllTrim(SuperGetMV("MV_XRTP2B1",.F.,"023")) //Tipo de Pedido dos pedidos B2B Hope 1.0
cAmzPk  := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking

cPolBonif := AllTrim(SuperGetMV("MV_XPOLBON",.F.,"240")) //Politica comercial de bonificacao
cTipBonif := AllTrim(SuperGetMV("MV_XTIPBON",.F.,"002")) //Tipo de Pedido de bonificacao
cTESBonif := AllTrim(SuperGetMV("MV_XTESBON",.F.,"754")) //TES para pedidos de bonificacao

cTabVar := SuperGetMV("MV_XTABVAR",.F.,"90UN-5")
cTabFra := SuperGetMV("MV_XTABFRA",.F.,"150K")
cTabHps := SuperGetMV("MV_XTABHPS",.F.,"311B")
cTabLoj := SuperGetMV("MV_XTABLOJ",.F.,"150K")
cTabHp1 := SuperGetMV("MV_XTABHP1",.F.,"150K")

cPrzImed := AllTrim(SuperGetMV("MV_XPRZIME",.F.,"002")) //Codigo Prazo Imediato

//cNtB2C := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCnB2BV,1,"11116001")

cTpOper := AllTrim(SuperGetMV("MV_XRTOV2C",.F.,"01")) //Tipo de Operacao de venda para pedidos B2C

If !lMenu
	GravaLog("Executando funcao ListarNovos automatica")
	If oWsClient:ListarNovos(0,7,,cChvA1,cChvA2)  // Codigo Loja, Status Pedido	
		cWsRet := oWsClient:oWSListarNovosResult:cDescricao
		If "SUCESSO" $ UPPER(cWsRet)	
			aWsRet := aClone(oWsClient:oWSListarNovosResult:oWSLista:oWSClsPedido)
			IMPORTA(aWsRet)
		Else
			GravaLog("Problema nos parametros da fun��o ListarNovos: "+cWsRet,"ERRO")
		EndIf		
	Else
		GravaLog("Erro na funcao ListarNovos: "+GetWSCError())
		Return		
	EndIf
Else
	GravaLog("Executando via menu.")
	AjustaSX1(cPerg)
	If Pergunte(cPerg)
		If mv_par01 = 1
			GravaLog("Executando Listar via menu.")
			If oWsClient:Listar(0,Val(mv_par02),7,,cChvA1,cChvA2)  // Codigo Loja, Codigo Pedido, Status Pedido	
				cWsRet := oWsClient:oWSListarResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)	
					aWsRet := aClone(oWsClient:oWSListarResult:oWSLista:oWSClsPedido)
					Processa( {|| IMPORTA(aWsRet,.F.) }, "Aguarde...", "Processando...",.F.)
				Else
					GravaLog("Problema nos parametros da fun��o Listar: "+cWsRet,"ERRO")
				EndIf		
			Else
				MsgAlert("Erro Listar: "+GetWSCError(),"Erro")
				GravaLog("Erro na funcao Listar via menu: "+GetWSCError())
				Return		
			EndIf
		ElseIf mv_par01 = 2
			GravaLog("Executando ListarNovos via menu.")
			If oWsClient:ListarNovos(0,7,,cChvA1,cChvA2)  // Codigo Loja, Status Pedido	
				cWsRet := oWsClient:oWSListarNovosResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)	
					aWsRet := aClone(oWsClient:oWSListarNovosResult:oWSLista:oWSClsPedido)
					Processa( {|| IMPORTA(aWsRet,.F.) }, "Aguarde...", "Processando...",.F.)
				Else
					GravaLog("Problema nos parametros da fun��o ListarNovos: "+cWsRet,"ERRO")
				EndIf		
			Else
				MsgAlert("Erro ListarNovos: "+GetWSCError(),"Erro")
				GravaLog("Erro na funcao ListarNovos via menu: "+GetWSCError(),"ERRO")
				Return		
			EndIf
		Else
			GravaLog("Executando Listar SZT via menu.")
			Processa( {|| IMPORTASZT() }, "Aguarde...", "Processando...",.F.)		
		EndIf
		
		If lMenu
			If mv_par01 <> 1
				If nGerados > 0
					MsgInfo("Foram importados "+AllTrim(Str(nGerados))+" pedidos com sucesso.","Aviso")
					GravaLog("Foram importados "+AllTrim(Str(nGerados))+" pedidos com sucesso.","Aviso")
				Else
					MsgInfo("N�o foram encontrados pedidos aptos a importar.","Aviso")
					GravaLog("N�o foram encontrados pedidos aptos a importar.")
				EndIf	
			EndIf
		Endif	
	Else
		GravaLog("Processo cancelado pelo usu�rio.")
	EndIf			
Endif

GravaLog("Fim do processamento.")
		
//MemoWrite("\log_rakuten\B2B\"+cDtTime+"_IMP.log",cLog)

If !lMenu
	Reset Environment
EndIf	

Return

Static Function IMPORTASZT()
Local nCont := 0
Local nPed  := 0

DbSelectArea("SZT")
DbSetOrder(2)
If DbSeek(xFilial("SZT")+"B2B")
	While SZT->(!EOF()) .And. SZT->ZT_ORIGEM = "B2B"
		If Empty(SZT->ZT_STATUS)
			nCont++
		EndIf
		SZT->(DbSkip())
	EndDo	
EndIf		

If nCont = 0
	GravaLog("N�o h� itens a serem improtados na SZT.")
	Return
EndIf

ProcRegua(nCont)

DbSelectArea("SZT")
DbSetOrder(2)
If DbSeek(xFilial("SZT")+"B2B")
	While SZT->(!EOF()) .And. SZT->ZT_ORIGEM = "B2B"
		If Empty(SZT->ZT_STATUS)
			IncProc()
			DbSelectArea("SC5")
			DbOrderNickName("XPEDRAK")
			If DbSeek(xFilial("SC5")+SZT->ZT_PEDIDO)
				GravaLog("Pedido Rakuten: "+SZT->ZT_PEDIDO+" j� importado no protheus. Alterando status...") 	
				RecLock("SZT",.F.)
				SZT->ZT_STATUS = "X"
				MsUnlock()
				SZT->(DbSkip())
				Loop				
			EndIf	
			//BEGIN TRANSACTION
			nPed++
			If oWsClient:Listar(0,Val(SZT->ZT_PEDIDO),7,,cChvA1,cChvA2)  // Codigo Loja, Codigo Pedido, Status Pedido
				cWsRet := oWsClient:oWSListarResult:cDescricao
				If "SUCESSO" $ UPPER(cWsRet)
					aWsRet := aClone(oWsClient:oWSListarResult:oWSLista:oWSClsPedido)
					IMPORTA(aWsRet,.T.,SZT->ZT_EMISSAO)
				Else
					GravaLog("Problema nos parametros da fun��o Listar: "+cWsRet,"ERRO")
				EndIf			
			Else
				GravaLog("Pedido "+SZT->ZT_PEDIDO+".Erro na funcao Listar SZT via menu: "+GetWSCError(),"ERRO")		
			EndIf
			//END TRANSACTION
			/*If nPed > 20
				cTime   := Time()
				cDtTime := DTOS(DDATABASE) + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
				MemoWrite("\log_rakuten\B2B\"+cDtTime+"_IMP.log",cLog)
				cLog := "_"
				nPed := 0
			EndIf*/		
		EndIf
		SZT->(DbSkip())
	EndDo
EndIf

Return

Static Function IMPORTA(aWsRet,lSZT,dEmissao)
Local nx,ny    := 0
Local cNroPed  := ""
Local cCGC     := ""
Local cCodCli  := ""
Local cLojCli  := ""
Local cNovCli  := ""
Local cTipPes  := ""
Local cNomCli  := ""
Local nPosP    := 0
Local cProd    := ""
Local cRef     := ""
Local cCor     := ""
Local cTam     := ""
Local cPedRak  := ""
Local cCanal   := ""
Local cNature  := ""
Local cPolitc  := ""
Local cTipPed  := ""
Local cCndPag  := ""
Local cCdEnd   := ""
Local cCdMun   := ""
Local cCdEnt   := ""
Local cCdHop   := ""
Local cCdGer   := ""
Local cCdTip   := ""
Local cCdGrp   := ""
Local cCdDiv   := ""
Local cCdReg   := ""
Local cCdMac   := ""
Local cNtB2C   := ""
Local cNmCan   := ""
Local cNmTip   := ""
Local cNmGrp   := ""
Local cNmDiv   := ""
Local cNmReg   := ""
Local cNmMac   := ""
Local cEndere  := ""
Local cComple  := ""
Local cBairro  := ""
Local cEstado  := ""
Local cCEP     := ""
Local cMunici  := ""
Local cEmail   := ""
Local cDDDTel  := ""
Local cNumTel  := ""
Local cDDDCel  := ""
Local cNumCel  := ""
Local cTES     := ""
Local cCodPed  := ""
Local nParce   := 0
Local cCdPrz   := ""
Local nxx        := 0
Local _i        := 0
Private lMsErroAuto := .F.

If Len(aWsRet) > 0
	If lMenu .And. !lSZT
		ProcRegua(Len(aWsRet))
	EndIf
	
	For nx := 1 to Len(aWsRet)
		If lMenu .And. !lSZT
			IncProc()
		EndIf
			
		cPedRak := StrZero(aWsRet[nx]:nPedidoCodigo,6)
		
		GravaLog("Iniciando a importa��o do pedido: "+cPedRak)
		
		DbSelectArea("SC5")
		DbOrderNickName("XPEDRAK")
		If DbSeek(xFilial("SC5")+cPedRak)
			GravaLog("Pedido "+cPedRak+" j� importado no protheus")
			If lMenu
				If mv_par01 = 1
					If !MsgYesNo("Pedido "+cPedRak+" j� importado. Importar novamente?","Aviso")
						Exit
					EndIf
				ElseIf mv_par01 = 3
					DbSelectArea("SZT")
					DbSetOrder(1)
					If DbSeek(xFilial("SZT")+cPedRak)
						RecLock("SZT",.F.)
						SZT->ZT_STATUS = "X"
						MsUnlock()
					EndIf
					ValidPed(cPedRak)
					Loop
				Else
					ValidPed(cPedRak)
					Loop		
				EndIf
			Else
				ValidPed(cPedRak)
				Loop
			EndIf
		EndIf
		
		Do Case
			Case AllTrim(aWsRet[nx]:oWSPessoa:Value) = 'PessoaF�sica'
				cTipPes := "F"
				cCGC    := aWsRet[nx]:cCPF
			Case AllTrim(aWsRet[nx]:oWSPessoa:Value) = 'PessoaJur�dica'
				cTipPes := "J"
				cCGC    := aWsRet[nx]:cCNPJ
			Otherwise
				GravaLog("Tipo de cliente diferente de F ou J. Pedido: "+cPedRak,"ERRO")
				Loop	 
		EndCase
		
		/*Do Case
			Case !Empty(aWsRet[nx]:cCPF)
				cTipPes := "F"
				cCGC    := AllTrim(aWsRet[nx]:cCPF)
			Case !Empty(aWsRet[nx]:cCNPJ)
				cTipPes := "J"
				cCGC    := AllTrim(aWsRet[nx]:cCNPJ)
			Otherwise
				GravaLog("Tipo de cliente diferente de F ou J. Pedido: "+cPedRak)
				Loop
		EndCase*/

		DbSelectArea("SA1")
		DbSetOrder(3)
		If DbSeek(xFilial("SA1")+cCGC)
			cCodCli := ""
			cLojCli := ""
			
			//If SA1->A1_MSBLQL = "1" //Inativo
				While SA1->(!EOF()) .And. AllTrim(SA1->A1_CGC) = AllTrim(cCGC)
					If SA1->A1_MSBLQL <> "1"
						cCodCli := SA1->A1_COD
						cLojCli := SA1->A1_LOJA
						Exit
					EndIf
					SA1->(DbSkip())
				EndDo	
			//EndIf
			
			If Empty(cCodCli)
				GravaLog("Cliente ativo n�o encontrado. Pedido: "+cPedRak,"ERRO")
				Loop
			EndIf
			
			cNature := ""
			
			If !Empty(SA1->A1_NATUREZ)
				cNature := SA1->A1_NATUREZ
			EndIf
			
			cCanal := AllTrim(SA1->A1_XCANAL)
			nParce := aWsRet[nx]:nParceiroCodigo
			cTabela := ""
			cTab    := ""

			Do Case
				Case nParce = 29 .Or. nParce = 25 //Varejo
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11113001")
					EndIf
					If Empty(cNature)
						cNature := "11113001"
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabVar,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabVar,4,"") +" - "+cTabVar+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabVar,4,"") 
					cCodPed := "B2BV-"
					cPolitc := cPlB2BV
					cTipPed := cTpB2BV
				Case nParce = 21 //Franquia
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111001")
					EndIf
					If Empty(cNature)
						cNature := "11111001"
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabFra,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabFra,4,"") +" - "+cTabFra+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabFra,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2BF
					cTipPed := cTpB2BF
				Case nParce = 23 //Hope Store
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111001")
					EndIf
					If Empty(cNature)
						cNature := "11111001"
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHps,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHps,4,"") +" - "+cTabHps+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabHps,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2BH
					cTipPed := cTpB2BH
				Case nParce = 24 //Loja Propria
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111401")
					EndIf
					If Empty(cNature)
						cNature := "11111401"
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabLoj,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabLoj,4,"") +" - "+cTabLoj+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabLoj,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2BL
					cTipPed := cTpB2BL
				Case nParce = 22 //Hope 1.0
					If Empty(cNature)
						cNature := GetAdvFVal("ZA3","ZA3_NATURE",xFilial("ZA3")+cCanal,1,"11111001")
					EndIf
					If Empty(cNature)
						cNature := "11111001"
					EndIf
					cTab    := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHp1,4,"")
					cTabela := GetAdvFVal("DA0","DA0_CODTAB",xFilial("DA0")+cTabHp1,4,"") +" - "+cTabHp1+" - "+GetAdvFVal("DA0","DA0_DESCRI",xFilial("DA0")+cTabHp1,4,"")
					cCodPed := "B2B"
					cPolitc := cPlB2B1
					cTipPed := cTpB2B1				
				Otherwise
					GravaLog("Canal "+cCanal+" nao esta apto a ser importado. Pedido: "+cPedRak,"ERRO")
					Loop
			End Case
		Else
			GravaLog("Cliente n�o encontrado. Pedido: "+cPedRak,"ERRO")
			Loop
		EndIf
		
		If Empty(cNature)
			GravaLog("Natureza n�o encontrada. Cliente: "+cCodCli+" Canal:"+cCanal+" Pedido: "+cPedRak,"ERRO")
			Loop
		EndIf
		
		cNroPed := GetMV("MV_XNMPVRK")
		
		DbSelectArea("SC5")
		DbSetOrder(1)
		DbSeek(xfilial("SC5")+cNroPed)
		
		While Found()
			cNroPed := "R"+SOMA1(SubStr(cNroPed,2,5))
			PutMV("MV_XNMPVRK",cNroPed)
			DbSeek(xfilial("SC5")+cNroPed)
		End
		
		DbSelectArea("SE4")
		DbOrderNickName("XCONDRA2")
		If DbSeek(xFilial("SE4")+SubStr(AllTrim(Str(aWsRet[nx]:nPagamento))+SPACE(3),1,3)+SubStr(AllTrim(Str(aWsRet[nx]:nQtdeParcelas))+SPACE(3),1,2))
			cCndPag := SE4->E4_CODIGO
		Else
			cCndPag := ""
			GravaLog("Condicao de pagamento nao encontrada. Pedido: "+cPedRak,"ERRO")
			Loop	
		EndIf
		
		DbSelectArea("SZ6")
		DbSetOrder(4)
		If DbSeek(xFilial("SZ6")+cPolitc+AllTrim(Str(aWsRet[nx]:nPrazoEntrega)))
			cCdPrz  := SZ6->Z6_PRAZO
			cDCdPrz := SZ6->Z6_DESC
			If aWsRet[nx]:nPrazoEntrega = 15 // PROGRAMADO
				cDataEnt := SubStr(aWsRet[nx]:cDataEntrega,1,4)+SubStr(aWsRet[nx]:cDataEntrega,6,2)+SubStr(aWsRet[nx]:cDataEntrega,9,2)
			Else
				If lSZT
					cDataEnt := DtoS(dEmissao)
				Else
					cDataEnt := DtoS(DDATABASE)
				EndIf		
			EndIf
		Else
			cCdPrz   := ""
			If lSZT
				cDataEnt := DtoS(dEmissao)
			Else
				cDataEnt := DtoS(DDATABASE)
			EndIf
			cDCdPrz  := ""	
		EndIf  
		
		nDesconto := aWsRet[nx]:nDesconto
		nVlrTotal := aWsRet[nx]:nValorSubTotal 
		nPercDesc := Round(((nDesconto / nVlrTotal) * 100),2)
		
		nVlrVale  := aWsRet[nx]:nValorVale
		
		If nVlrVale >= nVlrTotal
			cPolitc := cPolBonif
			cTipPed := cTipBonif
		EndIf
		
		aCabec := {}
		cDataEmi := SubStr(aWsRet[nx]:cData,1,4)+SubStr(aWsRet[nx]:cData,6,2)+SubStr(aWsRet[nx]:cData,9,2)
		
		DbSelectArea("SA1")
		DbSetOrder(1)
		DbSeek(xFilial("SA1")+cCodCli+cLojCli)
		
	   	aadd(aCabec,{"C5_NUM"    ,cNroPed,Nil})
		aadd(aCabec,{"C5_TIPO"   ,"N",Nil})
		aadd(aCabec,{"C5_CLIENTE",cCodCli,Nil})
		aadd(aCabec,{"C5_LOJACLI",cLojCli,Nil})
		aadd(aCabec,{"C5_CLIENT" ,cCodCli,Nil})
		aadd(aCabec,{"C5_LOJAENT",cLojCli,Nil})
		aadd(aCabec,{"C5_POLCOM" ,cPolitc,Nil})
		aadd(aCabec,{"C5_TPPED"  ,cTipPed,Nil})
		aadd(aCabec,{"C5_CONDPAG",cCndPag,Nil})
		aadd(aCabec,{"C5_NATUREZ",cNature,Nil})
		aadd(aCabec,{"C5_XPEDRAK",StrZero(aWsRet[nx]:nPedidoCodigo,6),Nil})
		aadd(aCabec,{"C5_XPEDWEB",cCodPed+aWsRet[nx]:cPedidoCodigoCliente,Nil})
		aadd(aCabec,{"C5_FRETE"  ,aWsRet[nx]:nValorFreteCobrado,Nil})
		aadd(aCabec,{"C5_DESCONT",0,Nil})
		aadd(aCabec,{"C5_DESC1"  ,nPercDesc,Nil})
		aadd(aCabec,{"C5_XCREDRK",aWsRet[nx]:nValorVale,Nil}) //Altera��o do campo cortesia para Rakuten [Weskley Silva 29/08/2018]
		//aadd(aCabec,{"C5_DESCONT",aWsRet[nx]:nValorVale,Nil})
		aadd(aCabec,{"C5_XNSU"   ,aWsRet[nx]:oWsCartao:cCartID,Nil})
		aadd(aCabec,{"C5_XAUTCAR",aWsRet[nx]:oWsCartao:cCarAutorizacaoCodigo,Nil})
		If lSZT
			aadd(aCabec,{"C5_EMISSAO",dEmissao,Nil})
		Else 
			aadd(aCabec,{"C5_EMISSAO",StoD(cDataEmi),Nil})
		EndIf	
		aadd(aCabec,{"C5_FECENT" ,StoD(cDataEnt),Nil})
		
		aadd(aCabec,{"C5_XCANAL" ,SA1->A1_XCANAL,Nil})
		aadd(aCabec,{"C5_XCANALD",SA1->A1_XCANALD,Nil})
		aadd(aCabec,{"C5_XCODREG",SA1->A1_XCODREG,Nil})
		aadd(aCabec,{"C5_XDESREG",SA1->A1_XDESREG,Nil})
		aadd(aCabec,{"C5_XMICRRE",SA1->A1_XMICRRE,Nil})
		aadd(aCabec,{"C5_XMICRDE",SA1->A1_XMICRDE,Nil})
		
		//aadd(aCabec,{"C5_PRAZO"  ,cCdPrz,Nil})
		aadd(aCabec,{"C5_XTABRAK",cTabela,Nil})
		aadd(aCabec,{"C5_ORIGEM","B2B",Nil})
		
		If !Empty(cTab)
			aadd(aCabec,{"C5_TABELA",cTab,Nil})
		EndIf
		
		//aadd(aCabec,{"C5_XDESCRK",0,Nil})
		
		aItens := {}

		lContinua := .T.
		cItem     := "01"
		//cDataEnt  := SubStr(aWsRet[nx]:cDataEntrega,1,4)+SubStr(aWsRet[nx]:cDataEntrega,6,2)+SubStr(aWsRet[nx]:cDataEntrega,9,2)
		
		aWsItens := aClone(aWsRet[nx]:oWsItens:oWsItem)
		
		aItWs := {}
		
		For ny := 1 to len(aWsItens)
			//Formato do codigo do produto vindo da RAKUTEN. Ex.: OUT_12345678_PRT_P  -> PROMOCAO_REFERENCIA_COR_TAMANHO
			cProd := aWsItens[ny]:cItemPartNumber   //Codigo do produto que vem da RAKUTEN
			nPosP := Len(cProd)
			cProd := AllTrim(If(SubStr(cProd,1,4)='OUT_',SubStr(cProd,5),cProd)) //Retira o OUT_ caso seja um produto de promocao
			
			//aAdd(aItWs,{SB1->B1_COD,nPrTab,aWsItens[ny]:nItemQtde,SubStr(cProd,1,8),AllTrim(Str(nPrTab*100))})
			aAdd(aItWs,{cProd,aWsItens[ny]:nItemValorFinal,aWsItens[ny]:nItemQtde,SubStr(cProd,1,8),AllTrim(Str(aWsItens[ny]:nItemValorFinal*100))})
		Next
		
		//ASORT(aItWs,,,{ |x,y| SubStr(x[1],1,8)+AllTrim(Str(x[2]*100)) < SubStr(y[1],1,8)+AllTrim(Str(y[2]*100)) } ) //Grade: Ordenacao por Referencia 8 digitos + Valor
		ASORT(aItWs,,,{ |x,y| x[4]+x[5] < y[4]+y[5] } ) //Grade: Ordenacao por Referencia 8 digitos + Valor
		
		/*For y:=1 to Len(aItWs)
			MsgInfo(aItWs[y][1]+Str(aItWs[y][2]))
		Next*/
		
		//aItWs := AGRUPA()
		cPrdBlq := ""
		lBlqDem := .F.
		lBloqDEM := SuperGetMV("MV_HBLQDEM",.F.,".T.")    //Bloqueia itens por Demanda
		
		For ny := 1 to Len(aItWs)
			
			cProd := aItWs[ny][1]
			nQtdPrd:= aItWs[ny][3]
			
			aChkEstoq:= fTradeNacImp(cProd, nQtdPrd)
			
				If lExEst:= aChkEstoq[6]//Existe estoque para o pedido?
				If lQbrPed:= aChkEstoq[1]//Havera nova linha de produto para completar estoque?
					//Caso tenha sido feita a quebra de quantidade em produtos diferentes por falta de estoque
					cProd:= aChkEstoq[2]
					nQtdPrd:= aChkEstoq[3]
					cProd2:= aChkEstoq[4]
					nQtd2Prd:= aChkEstoq[5]
				Else
					cProd:= aChkEstoq[2]
					nQtdPrd:= aChkEstoq[3]
				Endif
			Endif
			
			nQtPr:= Iif(lQbrPed,2,1)
			
			For nXX:= 1 to Len(nQtPr)
				
				If nXX > 1 
					//Informa a quantidade e codigo do item 2 que foi dividido por falta de estoque do item 1
					cProd  := cProd2
					nQtdPrd:= nQtd2Prd
				Endif
			
				DbSelectArea("SB1")
				DbSetOrder(1)
				If !DbSeek(xFilial("SB1")+cProd)
			    	DbSelectArea("SB1")
			    	DbOrderNickName("YFORMAT")
					If !DbSeek(xFilial("SB1")+cProd)
						GravaLog("Produto: "+cProd+" n�o encontrado. Pedido: "+cPedRak,"ERRO")
						lContinua := .F.
						Exit
					EndIf
		    	EndIf
		    	
		    	If AllTrim(cPolitc) = AllTrim(cPolBonif)
		    		cTES := cTESBonif
		    	Else
			    	cTES  := MaTesInt(2,cTpOper,cCodCli,cLojCli,"C",SB1->B1_COD,)
			    	If Empty(cTes)
			    		GravaLog("TES nao encontrada para o produto: "+SB1->B1_COD+". Cliente Protheus: "+cCodCli+"/"+cLojCli+" Tp Operacao: "+cTpOper+". Pedido: "+cPedRak,"ERRO")
						lContinua := .F.
						Exit
			    	EndIf
			    EndIf
		    	
		    	nPrUnit := aItWs[ny][2]
		    	nPrVend := Round(aItWs[ny][2],2)
		    	
				/*If !Empty(cTab)
					DbSelectArea("DA1")
					DbSetOrder(1)
					If DbSeek(xFilial("DA1")+cTab+SB1->B1_COD)
						nPrUnit := DA1->DA1_PRCVEN
					EndIf
				EndIf*/
				
				cBlqDem := AllTrim(SB1->B1_YBLQDEM)
				If cBlqDem = "S"
					cPrdBlq += CRLF + SB1->B1_COD
					lBlqDem := .T.
				EndIf
				
				If nPercDesc > 0
					nPrVend := Round(nPrVend - ((nPercDesc/100)*nPrVend),2)
				EndIf
				
				cDescri := GetAdvFVal("SB4","B4_DESC",xFilial("SB4")+SubStr(SB1->B1_COD,1,8),1,"") //Grade: Incluida descricao da referencia pois estava trazendo descricao do tamanho G
				
				aLinha := {}
				aadd(aLinha,{"C6_ITEM",cItem,Nil})			
				aadd(aLinha,{"C6_PRODUTO",SB1->B1_COD,Nil})
				//aadd(aLinha,{"C6_QTDVEN",aItWs[ny][3],Nil})	nQtdPrd	
				aadd(aLinha,{"C6_QTDVEN",nQtdPrd,Nil})		
				aadd(aLinha,{"C6_PRCVEN",nPrVend,Nil})
				aadd(aLinha,{"C6_PRUNIT",nPrUnit,Nil})			
				//aadd(aLinha,{"C6_VALOR",ROUND(aItWs[ny][3]*nPrVend,2),Nil}) nQtdPrd
				aadd(aLinha,{"C6_VALOR",ROUND(nQtdPrd*nPrVend,2),Nil}) 
				aadd(aLinha,{"C6_OPER" ,cTpOper,Nil})			
				aadd(aLinha,{"C6_TES",cTES,Nil})
				aadd(aLinha,{"C6_NUM",cNroPed,Nil})
				aadd(aLinha,{"C6_LOCAL",cAmzPk,Nil})
				aadd(aLinha,{"C6_ENTREG",StoD(cDataEnt),Nil})
				aadd(aLinha,{"C6_GRADE","S",Nil})
				aadd(aLinha,{"C6_ITEMGRD",strzero(val(cItem),3),Nil})
				
				If !Empty(cDescri)                                   //Grade: campo da descricao C6_DESCRI
					aadd(aLinha,{"C6_DESCRI",cDescri,Nil})
				EndIf
				aadd(aItens,aLinha)
	
				cItem   := SOMA1(cItem)
				cDescri := ""
				cTES    := ""
			Next
			
			If lBloqDEM .And. lBlqDem 
				GravaLog("Pedido n�o pode ser salvo pois os seguintes produtos est�o bloqueados por demanda: "+cPrdBlq)
				lContinua := .F.
			EndIf
			
			If !lContinua
				GravaLog("Erro na importa��o dos itens do pedido: "+cPedRak,"ERRO")
				Loop		
			EndIf
			
			For _i := 1 to len(aItens)
				If _i <> 1
					If SubStr(aitens[_i][2][2],1,8)+AllTrim(Str(aitens[_i][5][2]*100)) = SubStr(aitens[_i-1][2][2],1,8)+AllTrim(Str(aitens[_i-1][5][2]*100)) //Grade: Alterado If para quebrar por referencia + Valor
						aItens[_i][1][2] := aItens[_i-1][1][2]
						aItens[_i][13][2] := SOMA1(aItens[_i-1][13][2])
					Else
						aItens[_i][1][2] := SOMA1(aItens[_i-1][1][2])
						aItens[_i][13][2] := "001"
					Endif
				Endif
			Next
		Next nXX
		
		BEGIN TRANSACTION

		MATA410(aCabec,aItens,3)

		If lMsErroAuto
			GravaLog("Erro na gera��o do pedido Protheus n�: "+cNroPed+". Pedido: "+cPedRak,"ERRO")
			If lMenu
				MostraErro()
				MostraErro("\log_rakuten\B2B\ERROS_IMPORTACAO\",cDtTime+"_ERROPED.log")
			Else
				MostraErro("\log_rakuten\B2B\ERROS_IMPORTACAO\",cDtTime+"_ERROPED.log")
			EndIf	
			nErro++
			lMsErroAuto := .T.
		Else
			If lMenu
				If mv_par01 == 1
					MsgInfo("Pedido importado com sucesso. N� Protheus: "+cNroPed,"Aviso")
				EndIf
			EndIf
			If !Empty(cCdPrz)
				DbSelectArea("SC5")
				DbSetOrder(1)
				If DbSeek(xFilial("SC5")+cNroPed)
					RecLock("SC5",.F.)
					SC5->C5_PRAZO   := cCdPrz  //Gravo o Prazo depois pois da erro no execauto
					SC5->C5_XDPRAZO := cDCdPrz
					MsUnlock()
					If nDesconto > 0
						U_AjusDesc(cNroPed,nDesconto,nPercDesc) //Executa a funcao 3x
						U_AjusDesc(cNroPed,nDesconto,nPercDesc)
						U_AjusDesc(cNroPed,nDesconto,nPercDesc)
					EndIf
				EndIf
			EndIf
			
			GravaLog("Pedido Protheus n� "+cNroPed+" foi gerado com sucesso. Pedido: "+cPedRak) 
			nGerados++
			cNroPed := "R"+SOMA1(SubStr(cNroPed,2,5))
			PutMV("MV_XNMPVRK",cNroPed)
			
			DbSelectArea("SZT")
			DbSetOrder(1)
			If DbSeek(xFilial("SZT")+cPedRak)
				RecLock("SZT",.F.)
				SZT->ZT_STATUS = "X"
				MsUnlock()
			EndIf
			
			ValidPed(cPedRak)
		EndIf
		
		END TRANSACTION	
	Next
EndIf

ATUSTATUS()

Return

Static Function ATUSTATUS()
Local cQuery := ""
Local oWsPedido

oWsPedido := WSPedido():New()

cQuery := "select D2_SERIE,D2_DOC,D2_CLIENTE,D2_LOJA,D2_PEDIDO,C5_XPEDRAK,F2_CHVNFE,C5_ORIGEM "
cQuery += CRLF + "from "+RetSqlName("SD2")+" SD2 "
cQuery += CRLF + "inner join "+RetSqlName("SF2")+" SF2 "
cQuery += CRLF + "on F2_DOC = D2_DOC "
cQuery += CRLF + "and F2_SERIE = D2_SERIE "
cQuery += CRLF + "and F2_CLIENTE = D2_CLIENTE "
cQuery += CRLF + "and F2_LOJA = D2_LOJA "
cQuery += CRLF + "and SF2.D_E_L_E_T_ = '' "
cQuery += CRLF + "and F2_CHVNFE <> '' "
cQuery += CRLF + "and F2_EMISSAO >= '20180101' "
cQuery += CRLF + "and F2_XNUMSEP = '' "
cQuery += CRLF + "inner join "+RetSqlName("SC5")+" SC5 "
cQuery += CRLF + "on C5_NUM = D2_PEDIDO "
cQuery += CRLF + "and SC5.D_E_L_E_T_ = '' "
cQuery += CRLF + "and C5_XPEDRAK <> '' "
cQuery += CRLF + "and C5_ORIGEM = 'B2B' "
cQuery += CRLF + "where SD2.D_E_L_E_T_ = '' "
cQuery += CRLF + "group by D2_SERIE,D2_DOC,D2_CLIENTE,D2_LOJA,D2_PEDIDO,C5_XPEDRAK,F2_CHVNFE,C5_ORIGEM "
cQuery += CRLF + "order by D2_DOC "

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

DbSelectArea("TRB")

GravaLog("Executando funcao AlterarStatus para os pedidos faturados.")

If TRB->(!EOF())
	While TRB->(!EOF())
		cPedAtu := AllTrim(TRB->C5_XPEDRAK)
		
		If oWsPedido:AlterarStatus(1,Val(cPedAtu),"",137,"","",Val(TRB->D2_DOC),"",cChvA1,cChvA2)
			cWsRet := oWsPedido:oWSAlterarStatusResult:cDescricao
			GravaLog("Resultado funcao AlterarStatus para o pedido: "+cPedAtu+": "+cWsRet)
			//If "SUCESSO" $ UPPER(cWsRet)
				DbSelectArea("SF2")
				DbSetOrder(1)
				If DbSeek(xFilial("SF2")+TRB->D2_DOC+TRB->D2_SERIE+TRB->D2_CLIENTE+TRB->D2_LOJA)
					RecLock("SF2",.F.)
					SF2->F2_XNUMSEP := "A" //Status Alterado
					MsUnlock()
				EndIf
			//EndIf
		EndIf
		
		TRB->(DbSkip())
	EndDo
Else
	GravaLog("N�o Foram encontrados pedidos faturados para altera��o de Status.")
EndIf

TRB->(DbCloseArea())

Return

Static Function AGRUPA()
Local aAux := aClone(aItWs)
Local nLen,ny := 0
Local aRet := {}
Local nPos := 0
Local nQtd := 0

//While lRet
	//aAdd(aRet,{aAux[1][1],aAux[1][2],aAux[1][3],aAux[1][4],aAux[1][5]})
	//nLen := Len(aAux)
	For ny := 1 to Len(aAux)
		nPos := Ascan(aRet,{|x| x[4]+x[5] = aAux[ny][4]+aAux[ny][5] })
		If nPos > 0
			nQtd := aRet[nPos][3]
			aRet[nPos][3] := nQtd + aAux[ny][3]
		Else
			aAdd(aRet,{aAux[ny][1],aAux[ny][2],aAux[ny][3],aAux[ny][4],aAux[ny][5]})
		EndIf
		/*If aAux[ny][1]+AllTrim(Str(aAux[ny][2])) = aAux[ny-1][1]+AllTrim(Str(aAux[ny-1][2]))
			//aRet[Len(aRet)][3] := aAux[ny-1][3] + aAux[ny][3]
			//aAux[ny][3] := aAux[ny-1][3] + aAux[ny][3]
			aAux[ny-1][3] := aAux[ny-1][3] + aAux[ny][3] 
			aDel(aAux,ny)
			aSize(aAux,Len(aAux)-1)
			nLen := nLen - 1
			ny := ny - 1
		//Else
			//aAdd(aRet,{aAux[ny][1],aAux[ny][2],aAux[ny][3]})
		EndIf*/
	Next
//EndDo

Return aRet

Static Function ValidPed(cPedido)

If oWsClient:Validar(0,Val(cPedido),'?',cChvA1,cChvA2)
	GravaLog("Executado comando Validar para o pedido "+cPedido)
Else
	GravaLog("Erro na funcao Validar: "+GetWSCError())
EndIf

Return

Static Function GravaLog(cMsg,cErro)
Local cHora   := ""
Local cDtHora := ""

cHora   := Time()
cDtHora := DTOS(DDATABASE) + SubStr(cHora,1,2) + SubStr(cHora,4,2) + SubStr(cHora,7,2)

Conout(cDtHora+": HWSRK002 - "+cMsg)
FWRITE(nHandle,CRLF+cDtHora+": HWSRK002 - "+cMsg)
//cLog += CRLF+cDtHora+": HWSRK002 - "+cMsg

If !Empty(cErro)
	If AllTRIM(cErro) = "ERRO"
		FWRITE(nHandleErr,CRLF+cDtHora+": HWSRK001 - "+cMsg)
	EndIf
EndIf

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Cria os parametros na tabela SX1.                           ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Funcao:"        ,"Funcao:"        ,"Funcao:"        ,"mv_ch1","N",1,0,2,"C","","","","" ,"mv_par01","Listar","Listar","Listar","","ListarNovos","ListarNovos","ListarNovos","Listar SZT","Listar SZT","Listar SZT","","","","","","",{"Informe a funcao para importacao",""},{""},{""},"")  
PutSx1(cPerg,"02","Pedido RAKUTEN:","Pedido Rakuten:","Pedido Rakuten:","Mv_ch2","C",6,0,0,"G","","","","N","Mv_par02",""   ,""   ,""   ,"",""   ,""   ,""   ,"","","","","","","","","",{"Informe o numero do pedido RAKUTEN",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �fTradeNacImp �Autor  �R.Melo - Totalit � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Verifica se existe estoque para o produto e caso nao haja   ���
���          �o programa pega do mesmo produto porem da origem nac ou imp ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function fTradeNacImp(cProd, nQtdPrd)
Local cPrd1   := cProd
Local nQtdOrig   := nQtdPrd
Local cPrd2   := ""
Local nQtd2   := 0
Local aRet    := {}
Local lBlqDem := .f.
Local cPV	  := ""
Local lConEst := .t. //Consulta Estoque
Local aAreaSB1:= SB1->(GetArea())
Local aAreaSB2:= SB2->(GetArea())
Local cCodSeq := ''
Local cCodPrio:= ''
Local cPriori := ''
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cAmzPul := AllTrim(SuperGetMV("MV_XAMZPUL",.F.,"E1")) //Armazem padrao pulmao

dbSelectArea("ZB5")
dbSetOrder(1)
ZB5->(dbGoTop())
//Verifica se existe priorizacao de produtos, caso nao exista o pedido segue igual.
If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
	cPriori:= ZB5->ZB5_PROIRI
	cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
	//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
	cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
Else
	dbSelectArea("ZB5")
	dbSetOrder(2)
	ZB5->(dbGoTop())
	If ZB5->(DbSeek(xFilial("ZB5")+cPrd1))
		cPriori:= ZB5->ZB5_PROIRI
		cCodPrio:= Iif(cPriori=="N",ZB5->ZB5_PRDNAC,ZB5->ZB5_PRDIMP)
		//Inverte os codigos para saber qual e o produto secundario para ser contado estoque caso falte no prd1
		cCodSeq := Iif(cPriori=="N",ZB5->ZB5_PRDIMP,ZB5->ZB5_PRDNAC)
		//Retorna pedido original pois nao existe priorizacao cadastrada na ZB5
	Else
		aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 , .t.}
		RestArea(aAreaSB1)
		RestArea(aAreaSB2)
		Return (aRet) 
	Endif
Endif

//Verifica aqui o Bloqueio por Demanda
cBlqDem:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YBLQDEM")
lBlqDem:= Iif(Alltrim(cBlqDem)== "S",.T.,.F.) 

If !lBlqDem
	//Verifica aqui a Situacao do Produto onde:
	//PV - Nao consulta estoque, venda liberada
	//NPV - Consulta estoque, venda so acontece com disponibilidade do produto
	cPV:= POSICIONE("SB1",1,xFilial("SB1")+cCodPrio,"B1_YSITUAC")
	lConEst:= Iif(Alltrim(cPV)== "PV",.f.,.t.)

	If lConEst
		dbSelectArea("SB2")
		dbSetOrder(1)
		SB2->(dbGoTop())

		nSldPik := HFATSALDO(cCodPrio,cAmzPik)
		nSldPul := HFATSALDO(cCodPrio,cAmzPul)

		nSldSecPik := HFATSALDO(cCodSeq,cAmzPik)
		nSldSecPul := HFATSALDO(cCodSeq,cAmzPul)

		//If SB2->(DbSeek(xFilial("SB2")+cCodPrio))
		If nQtdOrig<= nSldPik+nSldPul//SB2->B2_QATU
			aRet:= { .f., cCodPrio, nQtdOrig, cPrd2, nQtd2 ,.t.}
		Else
			nPriQatu:= nSldPik + nSldPul
			nSecQatu:= nSldSecPik + nSldSecPul
			nSldAtu := nQtdOrig - nPriQatu

			If nSecQatu<= nSldAtu
				nQtd2:= nSldAtu - nSecQatu
				aRet:= { .f., cCodPrio, nQtdOrig, cCodSeq, nQtd2, .t. }
			Else
				Conout('Produtos: '+cCodPrio+' e '+cCodSeq+' nao possuem estoque para contemplar pedido.')
				aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2 ,.f.}
				RestArea(aAreaSB1)
				RestArea(aAreaSB2)
				Return (aRet)
				//Produtos (Nac+Imp) juntos nao contem a quantidade total necessaria do pedido.
			Endif	
		Endif
	Else
		//Retornar aqui quantidade original + produto priorizado 
		aRet:= { .f., cCodPrio, nQtdOrig, cPrd2, nQtd2 , .t.} 
	Endif
Else
	Conout('Produto: '+cCodPrio+' com bloqueio de demanda.')
	aRet:= { .f., cPrd1, nQtdOrig, cPrd2, nQtd2, .t. } 
	//Retornar aqui pedido original - o pedido nao podera ser feito por conta do bloqueio por demanda
Endif

RestArea(aAreaSB1)
RestArea(aAreaSB2)

Return (aRet) 

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATSALDO �Autor  �R.Melo - Totalit � Data � 28/09/2016  ���
�������������������������������������������������������������������������͹��
���Desc      �Busca saldo na SB2  										 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function HFATSALDO(cProduto,cArmazem)
Local nRet := 0

DbSelectArea("SB2")
DbSetOrder(1)
If SB2->(DbSeek(xFilial("SB2")+cProduto+cArmazem))
	nRet := SB2->B2_QATU-SB2->B2_RESERVA-SB2->B2_QEMP-SB2->B2_XRESERV //SaldoSb2()
EndIf

Return nRet