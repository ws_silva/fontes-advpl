#INCLUDE "PROTHEUS.CH"
#INCLUDE "APWEBSRV.CH"

/* ===============================================================================
WSDL Location    http://hope.ecservice.rakuten.com.br/ikcwebservice/produtocategoria.asmx?WSDL
Gerado em        02/06/17 14:47:34
Observa��es      C�digo-Fonte gerado por ADVPL WSDL Client 1.120703
                 Altera��es neste arquivo podem causar funcionamento incorreto
                 e ser�o perdidas caso o c�digo-fonte seja gerado novamente.
=============================================================================== */

User Function _SSJPJXJ ; Return  // "dummy" function - Internal Use 

/* -------------------------------------------------------------------------------
WSDL Service WSProdutoCategoria
------------------------------------------------------------------------------- */

WSCLIENT WSProdutoCategoria

	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD RESET
	WSMETHOD CLONE
	WSMETHOD Limpar
	WSMETHOD Salvar
	WSMETHOD Excluir
	WSMETHOD ListarCategoriasAtivasPorProduto

	WSDATA   _URL                      AS String
	WSDATA   _HEADOUT                  AS Array of String
	WSDATA   _COOKIES                  AS Array of String
	WSDATA   cCodIntPrd     AS string
	WSDATA   cA1                       AS string
	WSDATA   cA2                       AS string
	WSDATA   oWS                       AS SCHEMA
	WSDATA   cLimparResult             AS string
	WSDATA   nLojaCodigo               AS int
	WSDATA   cCodIntCtg   AS string
	WSDATA   oWSSalvarResult           AS ProdutoCategoria_clsRetornoOfclsProdutoCategoria
	WSDATA   oWSExcluirResult          AS ProdutoCategoria_clsRetornoOfclsProdutoCategoria
	WSDATA   cProdutoCodigoInterno     AS string
	WSDATA   oWSListarCategoriasAtivasPorProdutoResult AS ProdutoCategoria_clsRetornoOfclsCategoria

ENDWSCLIENT

WSMETHOD NEW WSCLIENT WSProdutoCategoria
::Init()
If !FindFunction("XMLCHILDEX")
	UserException("O C�digo-Fonte Client atual requer os execut�veis do Protheus Build [7.00.131227A-20160405 NG] ou superior. Atualize o Protheus ou gere o C�digo-Fonte novamente utilizando o Build atual.")
EndIf
If val(right(GetWSCVer(),8)) < 1.040504
	UserException("O C�digo-Fonte Client atual requer a vers�o de Lib para WebServices igual ou superior a ADVPL WSDL Client 1.040504. Atualize o reposit�rio ou gere o C�digo-Fonte novamente utilizando o reposit�rio atual.")
EndIf
Return Self

WSMETHOD INIT WSCLIENT WSProdutoCategoria
	::oWS                := NIL 
	::oWSSalvarResult    := ProdutoCategoria_CLSRETORNOOFCLSPRODUTOCATEGORIA():New()
	::oWSExcluirResult   := ProdutoCategoria_CLSRETORNOOFCLSPRODUTOCATEGORIA():New()
	::oWSListarCategoriasAtivasPorProdutoResult := ProdutoCategoria_CLSRETORNOOFCLSCATEGORIA():New()
Return

WSMETHOD RESET WSCLIENT WSProdutoCategoria
	::cCodIntPrd := NIL 
	::cA1                := NIL 
	::cA2                := NIL 
	::oWS                := NIL 
	::cLimparResult      := NIL 
	::nLojaCodigo        := NIL 
	::cCodIntCtg := NIL 
	::oWSSalvarResult    := NIL 
	::oWSExcluirResult   := NIL 
	::cProdutoCodigoInterno := NIL 
	::oWSListarCategoriasAtivasPorProdutoResult := NIL 
	::Init()
Return

WSMETHOD CLONE WSCLIENT WSProdutoCategoria
Local oClone := WSProdutoCategoria():New()
	oClone:_URL          := ::_URL 
	oClone:cCodIntPrd := ::cCodIntPrd
	oClone:cA1           := ::cA1
	oClone:cA2           := ::cA2
	oClone:cLimparResult := ::cLimparResult
	oClone:nLojaCodigo   := ::nLojaCodigo
	oClone:cCodIntCtg := ::cCodIntCtg
	oClone:oWSSalvarResult :=  IIF(::oWSSalvarResult = NIL , NIL ,::oWSSalvarResult:Clone() )
	oClone:oWSExcluirResult :=  IIF(::oWSExcluirResult = NIL , NIL ,::oWSExcluirResult:Clone() )
	oClone:cProdutoCodigoInterno := ::cProdutoCodigoInterno
	oClone:oWSListarCategoriasAtivasPorProdutoResult :=  IIF(::oWSListarCategoriasAtivasPorProdutoResult = NIL , NIL ,::oWSListarCategoriasAtivasPorProdutoResult:Clone() )
Return oClone

// WSDL Method Limpar of Service WSProdutoCategoria

WSMETHOD Limpar WSSEND cCodIntPrd,cA1,cA2,oWS WSRECEIVE cLimparResult WSCLIENT WSProdutoCategoria
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Limpar xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("CodigoInternoProduto", ::cCodIntPrd, cCodIntPrd , "string", .F. , .F., 0 , NIL, .F.) 
cSoap += "</Limpar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Limpar",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://hope.ecservice.rakuten.com.br/ikcwebservice/produtocategoria.asmx")

::Init()
::cLimparResult      :=  WSAdvValue( oXmlRet,"_LIMPARRESPONSE:_LIMPARRESULT:TEXT","string",NIL,NIL,NIL,NIL,NIL,NIL) 

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method Salvar of Service WSProdutoCategoria

WSMETHOD Salvar WSSEND nLojaCodigo,cCodIntPrd,cCodIntCtg,cA1,cA2,oWS WSRECEIVE oWSSalvarResult WSCLIENT WSProdutoCategoria
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Salvar xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("CodigoInternoProduto", ::cCodIntPrd, cCodIntPrd , "string", .F. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("CodigoInternoCategoria", ::cCodIntCtg, cCodIntCtg , "string", .F. , .F., 0 , NIL, .F.) 
cSoap += "</Salvar>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Salvar",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://hope.ecservice.rakuten.com.br/ikcwebservice/produtocategoria.asmx")

::Init()
::oWSSalvarResult:SoapRecv( WSAdvValue( oXmlRet,"_SALVARRESPONSE:_SALVARRESULT","clsRetornoOfclsProdutoCategoria",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method Excluir of Service WSProdutoCategoria

WSMETHOD Excluir WSSEND nLojaCodigo,cCodIntPrd,cCodIntCtg,cA1,cA2,oWS WSRECEIVE oWSExcluirResult WSCLIENT WSProdutoCategoria
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<Excluir xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("CodigoInternoProduto", ::cCodIntPrd, cCodIntPrd , "string", .F. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("CodigoInternoCategoria", ::cCodIntCtg, cCodIntCtg , "string", .F. , .F., 0 , NIL, .F.) 
cSoap += "</Excluir>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/Excluir",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://hope.ecservice.rakuten.com.br/ikcwebservice/produtocategoria.asmx")

::Init()
::oWSExcluirResult:SoapRecv( WSAdvValue( oXmlRet,"_EXCLUIRRESPONSE:_EXCLUIRRESULT","clsRetornoOfclsProdutoCategoria",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.

// WSDL Method ListarCategoriasAtivasPorProduto of Service WSProdutoCategoria

WSMETHOD ListarCategoriasAtivasPorProduto WSSEND nLojaCodigo,cProdutoCodigoInterno,cA1,cA2,oWS WSRECEIVE oWSListarCategoriasAtivasPorProdutoResult WSCLIENT WSProdutoCategoria
Local cSoap := "" , oXmlRet
Local cSoapHead := "" 

BEGIN WSMETHOD

cSoapHead += '<clsSoapHeader xmlns="http://www.ikeda.com.br">'
cSoapHead += WSSoapValue("A1", ::cA1, cA1 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("A2", ::cA2, cA2 , "string", .F. , .F., 0 , NIL, .F.) 
cSoapHead += WSSoapValue("", ::oWS, oWS , "SCHEMA", .F. , .F., 0 , NIL, .F.) 
cSoapHead +=  "</clsSoapHeader>"

cSoap += '<ListarCategoriasAtivasPorProduto xmlns="http://www.ikeda.com.br">'
cSoap += WSSoapValue("LojaCodigo", ::nLojaCodigo, nLojaCodigo , "int", .T. , .F., 0 , NIL, .F.) 
cSoap += WSSoapValue("ProdutoCodigoInterno", ::cProdutoCodigoInterno, cProdutoCodigoInterno , "string", .F. , .F., 0 , NIL, .F.) 
cSoap += "</ListarCategoriasAtivasPorProduto>"

oXmlRet := SvcSoapCall(	Self,cSoap,; 
	"http://www.ikeda.com.br/ListarCategoriasAtivasPorProduto",; 
	"DOCUMENT","http://www.ikeda.com.br",cSoapHead,,; 
	"http://hope.ecservice.rakuten.com.br/ikcwebservice/produtocategoria.asmx")

::Init()
::oWSListarCategoriasAtivasPorProdutoResult:SoapRecv( WSAdvValue( oXmlRet,"_LISTARCATEGORIASATIVASPORPRODUTORESPONSE:_LISTARCATEGORIASATIVASPORPRODUTORESULT","clsRetornoOfclsCategoria",NIL,NIL,NIL,NIL,NIL,NIL) )

END WSMETHOD

oXmlRet := NIL
Return .T.


// WSDL Data Structure clsRetornoOfclsProdutoCategoria

WSSTRUCT ProdutoCategoria_clsRetornoOfclsProdutoCategoria
	WSDATA   cAcao                     AS string OPTIONAL
	WSDATA   cData                     AS dateTime
	WSDATA   nCodigo                   AS int
	WSDATA   cDescricao                AS string OPTIONAL
	WSDATA   oWSLista                  AS ProdutoCategoria_ArrayOfClsProdutoCategoria OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_clsRetornoOfclsProdutoCategoria
	::Init()
Return Self

WSMETHOD INIT WSCLIENT ProdutoCategoria_clsRetornoOfclsProdutoCategoria
Return

WSMETHOD CLONE WSCLIENT ProdutoCategoria_clsRetornoOfclsProdutoCategoria
	Local oClone := ProdutoCategoria_clsRetornoOfclsProdutoCategoria():NEW()
	oClone:cAcao                := ::cAcao
	oClone:cData                := ::cData
	oClone:nCodigo              := ::nCodigo
	oClone:cDescricao           := ::cDescricao
	oClone:oWSLista             := IIF(::oWSLista = NIL , NIL , ::oWSLista:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_clsRetornoOfclsProdutoCategoria
	Local oNode5
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cAcao              :=  WSAdvValue( oResponse,"_ACAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cData              :=  WSAdvValue( oResponse,"_DATA","dateTime",NIL,"Property cData as s:dateTime on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::nCodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,"Property nCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cDescricao         :=  WSAdvValue( oResponse,"_DESCRICAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode5 :=  WSAdvValue( oResponse,"_LISTA","ArrayOfClsProdutoCategoria",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode5 != NIL
		::oWSLista := ProdutoCategoria_ArrayOfClsProdutoCategoria():New()
		::oWSLista:SoapRecv(oNode5)
	EndIf
Return

// WSDL Data Structure clsRetornoOfclsCategoria

WSSTRUCT ProdutoCategoria_clsRetornoOfclsCategoria
	WSDATA   cAcao                     AS string OPTIONAL
	WSDATA   cData                     AS dateTime
	WSDATA   nCodigo                   AS int
	WSDATA   cDescricao                AS string OPTIONAL
	WSDATA   oWSLista                  AS ProdutoCategoria_ArrayOfClsCategoria OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_clsRetornoOfclsCategoria
	::Init()
Return Self

WSMETHOD INIT WSCLIENT ProdutoCategoria_clsRetornoOfclsCategoria
Return

WSMETHOD CLONE WSCLIENT ProdutoCategoria_clsRetornoOfclsCategoria
	Local oClone := ProdutoCategoria_clsRetornoOfclsCategoria():NEW()
	oClone:cAcao                := ::cAcao
	oClone:cData                := ::cData
	oClone:nCodigo              := ::nCodigo
	oClone:cDescricao           := ::cDescricao
	oClone:oWSLista             := IIF(::oWSLista = NIL , NIL , ::oWSLista:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_clsRetornoOfclsCategoria
	Local oNode5
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::cAcao              :=  WSAdvValue( oResponse,"_ACAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cData              :=  WSAdvValue( oResponse,"_DATA","dateTime",NIL,"Property cData as s:dateTime on SOAP Response not found.",NIL,"S",NIL,NIL) 
	::nCodigo            :=  WSAdvValue( oResponse,"_CODIGO","int",NIL,"Property nCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cDescricao         :=  WSAdvValue( oResponse,"_DESCRICAO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode5 :=  WSAdvValue( oResponse,"_LISTA","ArrayOfClsCategoria",NIL,NIL,NIL,"O",NIL,NIL) 
	If oNode5 != NIL
		::oWSLista := ProdutoCategoria_ArrayOfClsCategoria():New()
		::oWSLista:SoapRecv(oNode5)
	EndIf
Return

// WSDL Data Structure ArrayOfClsProdutoCategoria

WSSTRUCT ProdutoCategoria_ArrayOfClsProdutoCategoria
	WSDATA   oWSclsProdutoCategoria    AS ProdutoCategoria_clsProdutoCategoria OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_ArrayOfClsProdutoCategoria
	::Init()
Return Self

WSMETHOD INIT WSCLIENT ProdutoCategoria_ArrayOfClsProdutoCategoria
	::oWSclsProdutoCategoria := {} // Array Of  ProdutoCategoria_CLSPRODUTOCATEGORIA():New()
Return

WSMETHOD CLONE WSCLIENT ProdutoCategoria_ArrayOfClsProdutoCategoria
	Local oClone := ProdutoCategoria_ArrayOfClsProdutoCategoria():NEW()
	oClone:oWSclsProdutoCategoria := NIL
	If ::oWSclsProdutoCategoria <> NIL 
		oClone:oWSclsProdutoCategoria := {}
		aEval( ::oWSclsProdutoCategoria , { |x| aadd( oClone:oWSclsProdutoCategoria , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_ArrayOfClsProdutoCategoria
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_CLSPRODUTOCATEGORIA","clsProdutoCategoria",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSclsProdutoCategoria , ProdutoCategoria_clsProdutoCategoria():New() )
			::oWSclsProdutoCategoria[len(::oWSclsProdutoCategoria)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure ArrayOfClsCategoria

WSSTRUCT ProdutoCategoria_ArrayOfClsCategoria
	WSDATA   oWSclsCategoria           AS ProdutoCategoria_clsCategoria OPTIONAL
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_ArrayOfClsCategoria
	::Init()
Return Self

WSMETHOD INIT WSCLIENT ProdutoCategoria_ArrayOfClsCategoria
	::oWSclsCategoria      := {} // Array Of  ProdutoCategoria_CLSCATEGORIA():New()
Return

WSMETHOD CLONE WSCLIENT ProdutoCategoria_ArrayOfClsCategoria
	Local oClone := ProdutoCategoria_ArrayOfClsCategoria():NEW()
	oClone:oWSclsCategoria := NIL
	If ::oWSclsCategoria <> NIL 
		oClone:oWSclsCategoria := {}
		aEval( ::oWSclsCategoria , { |x| aadd( oClone:oWSclsCategoria , x:Clone() ) } )
	Endif 
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_ArrayOfClsCategoria
	Local nRElem1, oNodes1, nTElem1
	::Init()
	If oResponse = NIL ; Return ; Endif 
	oNodes1 :=  WSAdvValue( oResponse,"_CLSCATEGORIA","clsCategoria",{},NIL,.T.,"O",NIL,NIL) 
	nTElem1 := len(oNodes1)
	For nRElem1 := 1 to nTElem1 
		If !WSIsNilNode( oNodes1[nRElem1] )
			aadd(::oWSclsCategoria , ProdutoCategoria_clsCategoria():New() )
			::oWSclsCategoria[len(::oWSclsCategoria)]:SoapRecv(oNodes1[nRElem1])
		Endif
	Next
Return

// WSDL Data Structure clsProdutoCategoria

WSSTRUCT ProdutoCategoria_clsProdutoCategoria
	WSDATA   nLojaCodigo               AS int
	WSDATA   oWSProdutoCategoriaStatus AS ProdutoCategoria_ProdutoCategoriaStatus
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_clsProdutoCategoria
	::Init()
Return Self

WSMETHOD INIT WSCLIENT ProdutoCategoria_clsProdutoCategoria
Return

WSMETHOD CLONE WSCLIENT ProdutoCategoria_clsProdutoCategoria
	Local oClone := ProdutoCategoria_clsProdutoCategoria():NEW()
	oClone:nLojaCodigo          := ::nLojaCodigo
	oClone:oWSProdutoCategoriaStatus := IIF(::oWSProdutoCategoriaStatus = NIL , NIL , ::oWSProdutoCategoriaStatus:Clone() )
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_clsProdutoCategoria
	Local oNode2
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::nLojaCodigo        :=  WSAdvValue( oResponse,"_LOJACODIGO","int",NIL,"Property nLojaCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	oNode2 :=  WSAdvValue( oResponse,"_PRODUTOCATEGORIASTATUS","ProdutoCategoriaStatus",NIL,"Property oWSProdutoCategoriaStatus as tns:ProdutoCategoriaStatus on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode2 != NIL
		::oWSProdutoCategoriaStatus := ProdutoCategoria_ProdutoCategoriaStatus():New()
		::oWSProdutoCategoriaStatus:SoapRecv(oNode2)
	EndIf
Return

// WSDL Data Structure clsCategoria

WSSTRUCT ProdutoCategoria_clsCategoria
	WSDATA   nLojaCodigo               AS int
	WSDATA   cCategoriaCodigoInterno   AS string OPTIONAL
	WSDATA   cPaiInterno               AS string OPTIONAL
	WSDATA   oWSCategoriaStatus        AS ProdutoCategoria_CategoriaStatus
	WSDATA   cNome                     AS string OPTIONAL
	WSDATA   nOrdem                    AS int
	WSMETHOD NEW
	WSMETHOD INIT
	WSMETHOD CLONE
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_clsCategoria
	::Init()
Return Self

WSMETHOD INIT WSCLIENT ProdutoCategoria_clsCategoria
Return

WSMETHOD CLONE WSCLIENT ProdutoCategoria_clsCategoria
	Local oClone := ProdutoCategoria_clsCategoria():NEW()
	oClone:nLojaCodigo          := ::nLojaCodigo
	oClone:cCategoriaCodigoInterno := ::cCategoriaCodigoInterno
	oClone:cPaiInterno          := ::cPaiInterno
	oClone:oWSCategoriaStatus   := IIF(::oWSCategoriaStatus = NIL , NIL , ::oWSCategoriaStatus:Clone() )
	oClone:cNome                := ::cNome
	oClone:nOrdem               := ::nOrdem
Return oClone

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_clsCategoria
	Local oNode4
	::Init()
	If oResponse = NIL ; Return ; Endif 
	::nLojaCodigo        :=  WSAdvValue( oResponse,"_LOJACODIGO","int",NIL,"Property nLojaCodigo as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
	::cCategoriaCodigoInterno :=  WSAdvValue( oResponse,"_CATEGORIACODIGOINTERNO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::cPaiInterno        :=  WSAdvValue( oResponse,"_PAIINTERNO","string",NIL,NIL,NIL,"S",NIL,NIL) 
	oNode4 :=  WSAdvValue( oResponse,"_CATEGORIASTATUS","CategoriaStatus",NIL,"Property oWSCategoriaStatus as tns:CategoriaStatus on SOAP Response not found.",NIL,"O",NIL,NIL) 
	If oNode4 != NIL
		::oWSCategoriaStatus := ProdutoCategoria_CategoriaStatus():New()
		::oWSCategoriaStatus:SoapRecv(oNode4)
	EndIf
	::cNome              :=  WSAdvValue( oResponse,"_NOME","string",NIL,NIL,NIL,"S",NIL,NIL) 
	::nOrdem             :=  WSAdvValue( oResponse,"_ORDEM","int",NIL,"Property nOrdem as s:int on SOAP Response not found.",NIL,"N",NIL,NIL) 
Return

// WSDL Data Enumeration ProdutoCategoriaStatus

WSSTRUCT ProdutoCategoria_ProdutoCategoriaStatus
	WSDATA   Value                     AS string
	WSDATA   cValueType                AS string
	WSDATA   aValueList                AS Array Of string
	WSMETHOD NEW
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_ProdutoCategoriaStatus
	::Value := NIL
	::cValueType := "string"
	::aValueList := {}
	aadd(::aValueList , "Nenhum" )
	aadd(::aValueList , "Ativo" )
	aadd(::aValueList , "Inativo" )
Return Self

WSMETHOD SOAPSEND WSCLIENT ProdutoCategoria_ProdutoCategoriaStatus
	Local cSoap := "" 
	cSoap += WSSoapValue("Value", ::Value, NIL , "string", .F. , .F., 3 , NIL, .F.) 
Return cSoap

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_ProdutoCategoriaStatus
	::Value := NIL
	If oResponse = NIL ; Return ; Endif 
	::Value :=  oResponse:TEXT
Return 

WSMETHOD CLONE WSCLIENT ProdutoCategoria_ProdutoCategoriaStatus
Local oClone := ProdutoCategoria_ProdutoCategoriaStatus():New()
	oClone:Value := ::Value
Return oClone

// WSDL Data Enumeration CategoriaStatus

WSSTRUCT ProdutoCategoria_CategoriaStatus
	WSDATA   Value                     AS string
	WSDATA   cValueType                AS string
	WSDATA   aValueList                AS Array Of string
	WSMETHOD NEW
	WSMETHOD CLONE
	WSMETHOD SOAPSEND
	WSMETHOD SOAPRECV
ENDWSSTRUCT

WSMETHOD NEW WSCLIENT ProdutoCategoria_CategoriaStatus
	::Value := NIL
	::cValueType := "string"
	::aValueList := {}
	aadd(::aValueList , "Nenhum" )
	aadd(::aValueList , "Ativo" )
	aadd(::aValueList , "Inativo" )
Return Self

WSMETHOD SOAPSEND WSCLIENT ProdutoCategoria_CategoriaStatus
	Local cSoap := "" 
	cSoap += WSSoapValue("Value", ::Value, NIL , "string", .F. , .F., 3 , NIL, .F.) 
Return cSoap

WSMETHOD SOAPRECV WSSEND oResponse WSCLIENT ProdutoCategoria_CategoriaStatus
	::Value := NIL
	If oResponse = NIL ; Return ; Endif 
	::Value :=  oResponse:TEXT
Return 

WSMETHOD CLONE WSCLIENT ProdutoCategoria_CategoriaStatus
Local oClone := ProdutoCategoria_CategoriaStatus():New()
	oClone:Value := ::Value
Return oClone


