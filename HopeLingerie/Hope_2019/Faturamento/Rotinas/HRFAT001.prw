#include "topconn.ch"
#INCLUDE "PROTHEUS.CH"
#DEFINE cEOL	Chr(13)+Chr(10)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HRFAT001  �Autor  �Daniel R. Melo      � Data � 24/08/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de Romaneio de Transporte.                        ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HRFAT001()

//��������������������������������������������������������������Ŀ
//� Define Variaveis                                             �
//����������������������������������������������������������������
	Local cDesc1     := "Este programa tem como objetivo imprimir relatorio "
	Local cDesc2     := "de acordo com os parametros informados pelo usuario."
	Local cDesc3 	   := ""
	Local cString    := "SF2"
	Local cPerg      := PADR("HRFAT001",10)

	Private tamanho  := "M"
	Private aOrd	   := {}
	Private aReturn  := {"Zebrado",1,"Administracao", 2, 2, 1, "",0 }
	Private li		   := 80, limite:=132, lRodape:=.F.
	Private wnrel    := "HRFAT001"
	Private titulo   := "          ROMANEIO DE TRANSPORTE          "
	Private nLin 	   := 100
	Private nCol 	   := 60
	Private nPula    := 60
	Private nfim     := 2500
	Private imprp	   := .F.

	Private oFont08  := TFont():New("Arial",,08,,.f.,,,,,.f.)
	Private oFont08n := TFont():New("Arial",,08,,.t.,,,,,.f.)
	Private oFont09  := TFont():New("Arial",,09,,.f.,,,,,.f.)
	Private oFont09n := TFont():New("Arial",,09,,.t.,,,,,.f.)
	Private oFont10  := TFont():New("Arial",,10,,.f.,,,,,.f.)
	Private oFont10n := TFont():New("Arial",,10,,.t.,,,,,.f.)
	Private oFont11  := TFont():New("Arial",,11,,.f.,,,,,.f.)
	Private oFont11n := TFont():New("Arial",,11,,.t.,,,,,.f.)
	Private oFont12  := TFont():New("Arial",,12,,.f.,,,,,.f.)
	Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)
	Private oFont14  := TFont():New("Arial",,14,,.f.,,,,,.f.)
	Private oFont14n := TFont():New("Arial",,14,,.t.,,,,,.f.)
	
	ValidPerg(cPerg)
	
	Pergunte(cPerg,.T.)
		
	wnrel:=SetPrint(cString,wnrel,cPerg,@Titulo,cDesc1,cDesc2,cDesc3,.F.,aOrd,.F.,Tamanho)
		
	If nLastKey == 27
		Return
	Endif
	
	SetDefault(aReturn,cString)
	
	If nLastKey == 27
		Return
	Endif

	oPrn 	 := TMSPrinter():New()

	Processa({||ImpRel()}, titulo, "Imprimindo, aguarde...")

	If	( aReturn[5] == 1 ) //1-Disco, 2-Impressora
		oPrn:Preview()
	Else
		oPrn:Setup()
		oPrn:Print()
	EndIf

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � ImpRel   �Autor  �Daniel R. Melo      � Data � 24/08/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � Chamada do Relatorio                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ImpRel()

	Local aArea		:= GetArea()
	Local cTransp		:= ""
	Local nVerEsp		:= 0
	Local aFieldFill	:= {}
	Local aEspecies	:= {}

	nLin	     := 120
	nCol	     := 50
	nPula1      := 50
	nPula2      := 65
	nPosTit     := 60

	IncProc()

	aRomaneio   := VERNFS()
		
	oPrn:StartPage()

	oPrn:Say(nLin,nPosTit,Titulo,oFont14n,100)

	nLin += nPula1
	oPrn:Box(nLin+20,50,nLin+23,nfim)

	nLin += nPula1

	oPrn:Say(nLin,080,"ROMANEIO: ",oFont10,100)
	oPrn:Say(nLin-5,450,AllTrim("???????"),oFont12n,100)

	For _x := 1 to len(aRomaneio)
		If AllTrim(aRomaneio[_x,16]) <> ""
			cTransp := AllTrim(aRomaneio[_x,16])
			Exit
		End
	Next _x

	oPrn:Say(nLin,900,"TRANSPORTADORA: ",oFont10,100)
	oPrn:Say(nLin,1330,cTransp +" - "+ Posicione("SA4",1,xFilial("SA4")+cTransp,"A4_NOME") ,oFont10n,100)

	nLin += nPula1
	oPrn:Box(nLin,50,nLin+3,nfim)
	nLin += nPula2

	oPrn:Say(nLin,080,"DATA: ",oFont10,100)
	oPrn:Say(nLin-5,400,dToC(dDataBase),oFont12n,100)
	nLin += nPula2

	oPrn:Say(nLin,080,"REMETENTE: ",oFont10,100)
	oPrn:Say(nLin-5,400,AllTrim(SM0->M0_NOMECOM),oFont12n,100)
	nLin += nPula2

	oPrn:Say(nLin,080,"ENDERE�O: ",oFont10,100)
	oPrn:Say(nLin-5,400,AllTrim(SM0->M0_ENDCOB),oFont12n,100)
	nLin += nPula2

	oPrn:Say(nLin,080,"CIDADE: ",oFont10,100)
	oPrn:Say(nLin-5,400,AllTrim(SM0->M0_CIDCOB),oFont12n,100)
	nLin += nPula2
		
	oPrn:Say(nLin,080,"ESTADO: ",oFont10,100)
	oPrn:Say(nLin,400,AllTrim(SM0->M0_ESTCOB),oFont12n,100)
	nLin += nPula2

	aColTab  := {050,250,930,1380,1580,1830,2080,2250,2500}
	aColTCab := {"NFS","NOME DO GERADOR","CIDADE","ESTADO","PESO LIQUIDO","PESO CUBADO","VOLUME","TOTAL"}

	nLin += nPula1
	For _x := 1 to len(aColTab)-1
		oPrn:Box(nLin-25,aColTab[_x],nLin-25+nPula2,aColTab[_x+1])
	Next _x

	For _x := 1 to len(aColTab)-1
		oPrn:Say(nLin,aColTab[_x]+25,aColTCab[_x],oFont08,100)
	Next _x
		
	For _y := 1 to len(aRomaneio)
		
		nLin += nPula1+15
		
		For _x := 1 to len(aColTab)-1
			oPrn:Box(nLin-25,aColTab[_x],nLin-25+nPula2,aColTab[_x+1])
		Next _x
		
		cTotVol	:= aRomaneio[_y,9]+aRomaneio[_y,10]+aRomaneio[_y,11]+aRomaneio[_y,12]
			
		oPrn:Say(nLin-5,aColTab[1]+25 ,AllTrim(aRomaneio[_y,1])																	,oFont08n,100)		//"NFS"
		oPrn:Say(nLin-5,aColTab[2]+25 ,Posicione("SA1",1,xFilial("SA1")+aRomaneio[_y,2]+aRomaneio[_y,3],"A1_NOME")		,oFont08n,100)		//"NOME DO GERADOR"
		oPrn:Say(nLin-5,aColTab[3]+25 ,Posicione("SA1",1,xFilial("SA1")+aRomaneio[_y,2]+aRomaneio[_y,3],"A1_MUN")		,oFont08n,100)		//"CIDADE"
		oPrn:Say(nLin-5,aColTab[4]+75,AllTrim(aRomaneio[_y,4])																	,oFont08n,100)		//"ESTADO"
		oPrn:Say(nLin-5,aColTab[5]+25 ,Transform(aRomaneio[_y,13],"@E 999,999.99999")										,oFont08n,100)		//"PESO LIQUIDO"
		oPrn:Say(nLin-5,aColTab[6]+25 ,Transform(aRomaneio[_y,14],"@E 999,999.99999")										,oFont08n,100)		//"PESO CUBADO"
		oPrn:Say(nLin-5,aColTab[7]+25 ,Transform(cTotVol,"@E 9,999")															,oFont08n,100)		//"VOLUME"
		oPrn:Say(nLin-5,aColTab[8]+25 ,Transform(aRomaneio[_y,15],"@E 999,999,999.99")										,oFont08n,100)		//"TOTAL"
			
	Next _y


	nLin += nPula2

	aColTab  := {050,2080,2500}
	aColTCab := {"DESCRI��O","QTD. VOLUME"}

	nLin += nPula1
	For _x := 1 to len(aColTab)-1
		oPrn:Box(nLin-25,aColTab[_x],nLin-25+nPula2,aColTab[_x+1])
	Next _x

	For _x := 1 to len(aColTab)-1
		oPrn:Say(nLin,aColTab[_x]+50,aColTCab[_x],oFont08,100)
	Next _x

	cPos		:= 0
	nSoma		:= 0
	nPesoLiqui	:= 0
	nPesoBruto	:= 0
	nValTotal	:= 0
	
	//Cria Arrey com todos os volumes
	For _x := 1 to len(aRomaneio)

		For _z := 1 to 4  //Especies
			If AllTrim(aRomaneio[_x,_z+4]) <> ""

				For _y := 1 To Len(aEspecies)
					If aEspecies[_y,1] == AllTrim(aRomaneio[_x,_z+4])
						cPos	:= _y
						Exit
					End
				Next _y
				
				If cPos > 0
					aEspecies[cPos,2] += aRomaneio[_x,_z+8]
				Else
					Aadd(aFieldFill, AllTrim(aRomaneio[_x,_z+4]))
					Aadd(aFieldFill, aRomaneio[_x,_z+8])
					Aadd(aFieldFill, .F.)
					Aadd(aEspecies, aFieldFill)
					aFieldFill :={}
				End
				
				cPos		:= 0
				nSoma		+= aRomaneio[_x,_z+8]
				nPesoLiqui	+= aRomaneio[_x,13]
				nPesoBruto	+= aRomaneio[_x,14]
				nValTotal	+= aRomaneio[_x,15]

			End
		Next _z

	Next _x

	For _y := 1 to len(aEspecies)
		
		nLin += nPula1+15
		
		For _x := 1 to len(aColTab)-1
			oPrn:Box(nLin-25,aColTab[_x],nLin-25+nPula2,aColTab[_x+1])
		Next _x
		
			oPrn:Say(nLin-5,aColTab[1]+50 ,AllTrim(aEspecies[_y,1])							,oFont08n,100)		//"ESPECIE"
			oPrn:Say(nLin-5,aColTab[2]+50 ,Transform(aEspecies[_y,2],"@E 9,999")			,oFont08n,100)		//"VOLUMES"
	
	Next _y

	nLin += nPula2+15
	oPrn:Box(nLin-25,aColTab[1],nLin-10+nPula2,aColTab[3])

	oPrn:Say(nLin,aColTab[1]+25,"Total de Volumes "+Replicate(".",200)					,oFont09n,100)
	oPrn:Say(nLin,aColTab[2]+100,Transform(nSoma,"@E 9,999")							,oFont09n,100)

	nLin += nPula2+50
	oPrn:Box(nLin-25,aColTab[1],nLin+300,aColTab[3])

	oPrn:Say(nLin,aColTab[1]+25,"Total de Pe�as "											,oFont08n,100)
	oPrn:Say(nLin,aColTab[1]+350,AllTrim(Transform(nSoma,"@E 9,999"))					,oFont08n,100)
	
	nQtdVolumes	:= VERNFSVOL()
	nLin += nPula1
	oPrn:Say(nLin,aColTab[1]+25,"Total de Volumes "										,oFont08n,100)
	oPrn:Say(nLin,aColTab[1]+350,AllTrim(Transform(nQtdVolumes,"@E 9,999"))			,oFont08n,100)
	
	nLin += nPula1
	oPrn:Say(nLin,aColTab[1]+25,"Numero de Notas "										,oFont08n,100)
	oPrn:Say(nLin,aColTab[1]+350,AllTrim(Transform(len(aRomaneio),"@E 9,999"))		,oFont08n,100)
	
	nLin += nPula1
	oPrn:Say(nLin,aColTab[1]+25,"Valor Total"												,oFont08n,100)
	oPrn:Say(nLin,aColTab[1]+350,AllTrim(Transform(nValTotal,"@E 999,999.99"))		,oFont08n,100)

	nLin += nPula1
	oPrn:Say(nLin,aColTab[1]+25,"Total Peso Liquido "									,oFont08n,100)
	oPrn:Say(nLin,aColTab[1]+350,AllTrim(Transform(nPesoLiqui,"@E 999,999.99999"))	,oFont08n,100)

	nLin += nPula1
	oPrn:Say(nLin,aColTab[1]+25,"Total Peso Bruto "										,oFont08n,100)
	oPrn:Say(nLin,aColTab[1]+350,AllTrim(Transform(nPesoBruto,"@E 999,999.99999"))	,oFont08n,100)

	nLin += nPula2+nPula2+nPula2
	oPrn:Say(nLin,aColTab[1]+25,"Declara��o"												,oFont10n,100)
	
	nLin += nPula2
	oPrn:Say(nLin,aColTab[1]+25,"Recebemos para transporte aos seus destinos, as cargas constantes nas notasfiscais supra relacionadas.",oFont10n,100)

	nLin += nPula2+nPula2+nPula2+nPula2
	oPrn:Say(nLin,aColTab[1]+25,Replicate("-",150)										,oFont08n,100)

	nLin += nPula2
	oPrn:Say(nLin,aColTab[1]+25,Posicione("SA4",1,xFilial("SA4")+cTransp,"A4_NOME")	,oFont10n,100)
	
	imprp	:= .T.

	RestArea(aArea)

	MS_FLUSH()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VERNFS   �Autor  �Daniel R. Melo      � Data � 24/08/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � Query para Carregar as Notas Fiscais                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERNFS()

	Local cQuery
	Local aFieldFill	:= {}
	Local aColsEx		:= {}
	MV_PAR02 := CTOD("31/12/2017")

	cQuery  := "SELECT F2_DOC,F2_CLIENTE,F2_LOJA,F2_EST, "+cEOL
	cQuery  += "		F2_ESPECI1,F2_ESPECI2,F2_ESPECI3,F2_ESPECI4, "+cEOL
	cQuery  += "		F2_VOLUME1,F2_VOLUME2,F2_VOLUME3,F2_VOLUME4, "+cEOL
	cQuery  += "		F2_PLIQUI,F2_PBRUTO,F2_VALBRUT,F2_TRANSP "+cEOL
	cQuery  += "FROM "+RetSQLName("SF2")+" SF2 "+cEOL
	cQuery  += "WHERE D_E_L_E_T_<>'*' "+cEOL
	cQuery  += "	AND F2_EMISSAO >= '"+ dToS(MV_PAR01)+"' "+cEOL
	cQuery  += "	AND F2_EMISSAO <= '"+ dToS(MV_PAR02)+"' "+cEOL
	cQuery  += "	AND F2_SERIE = '"+ AllTrim(MV_PAR03)+"' "+cEOL
	cQuery  += "	AND F2_DOC >= '"+ AllTrim(MV_PAR04)+"' "+cEOL
	cQuery  += "	AND F2_DOC <= '"+ AllTrim(MV_PAR05)+"' "+cEOL
	cQuery  += "	AND F2_CLIENTE = '"+ AllTrim(MV_PAR06)+"' "+cEOL

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")
	TMP->(DbGoTop())
		
	While TMP->(!EOF())
		Aadd(aFieldFill, TMP->F2_DOC)
		Aadd(aFieldFill, TMP->F2_CLIENTE)
		Aadd(aFieldFill, TMP->F2_LOJA)
		Aadd(aFieldFill, TMP->F2_EST)
		Aadd(aFieldFill, TMP->F2_ESPECI1)
		Aadd(aFieldFill, TMP->F2_ESPECI2)
		Aadd(aFieldFill, TMP->F2_ESPECI3)
		Aadd(aFieldFill, TMP->F2_ESPECI4)
		Aadd(aFieldFill, TMP->F2_VOLUME1)
		Aadd(aFieldFill, TMP->F2_VOLUME2)
		Aadd(aFieldFill, TMP->F2_VOLUME3)
		Aadd(aFieldFill, TMP->F2_VOLUME4)
		Aadd(aFieldFill, TMP->F2_PLIQUI)
		Aadd(aFieldFill, TMP->F2_PBRUTO)
		Aadd(aFieldFill, TMP->F2_VALBRUT)
		Aadd(aFieldFill, TMP->F2_TRANSP)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � VERNFS   �Autor  �Daniel R. Melo      � Data � 24/08/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � Query para carregar o Total de Volumes das Notas Fiscais   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERNFSVOL()

	Local cQuery
	Local cQtdVol	:= 0
	MV_PAR02 := CTOD("31/12/2017")

	cQuery  := "SELECT SUM(FT_QUANT) AS FT_QUANT"+cEOL
	cQuery  += "FROM "+RetSQLName("SFT")+" SFT "+cEOL
	cQuery  += "WHERE D_E_L_E_T_<>'*' "+cEOL
	cQuery  += "	AND FT_EMISSAO >= '"+ dToS(MV_PAR01)+"' "+cEOL
	cQuery  += "	AND FT_EMISSAO <= '"+ dToS(MV_PAR02)+"' "+cEOL
	cQuery  += "	AND FT_SERIE = '"+ AllTrim(MV_PAR03)+"' "+cEOL
	cQuery  += "	AND FT_NFISCAL >= '"+ AllTrim(MV_PAR04)+"' "+cEOL
	cQuery  += "	AND FT_NFISCAL <= '"+ AllTrim(MV_PAR05)+"' "+cEOL
	cQuery  += "	AND FT_CLIEFOR = '"+ AllTrim(MV_PAR06)+"' "+cEOL

	If Select("TMP") > 0
		TMP->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
	DbSelectArea("TMP")

	cQtdVol	:= TMP->FT_QUANT

Return (cQtdVol)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Fun��o    �VALIDPERG � Autor � AP5 IDE            � Data � 24/01/2017  ���
�������������������������������������������������������������������������͹��
���Descri��o � Verifica a existencia das perguntas criando-as caso seja   ���
���          � necessario (caso nao existam).                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ValidPerg(cPerg)

	Local _sAlias := Alias()
	Local aRegs := {}
	Local i,j

	dbSelectArea("SX1")
	dbSetOrder(1)
	cPerg := PADR(cPerg,10)

//��������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros                         �
//� mv_par01              Ordem de Corte                         �
//����������������������������������������������������������������

	//Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/                             Var01/ Def01     /Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/Var04/Def04/Cnt04/Var05/Def05/Cnt05/F3
	/*1*/aAdd(aRegs,{cPerg,"01","Data de                 ? ","","","MV_CH1","D",08,0,0,"G","","MV_PAR01",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","",""      })
	/*2*/aAdd(aRegs,{cPerg,"02","Data At�                ? ","","","MV_CH2","D",08,0,0,"G","","MV_PAR02",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","",""      })
	/*3*/aAdd(aRegs,{cPerg,"03","Serie                   ? ","","","MV_CH3","C",03,0,0,"G","","MV_PAR03",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","",""      })
	/*4*/aAdd(aRegs,{cPerg,"04","Nota de                 ? ","","","MV_CH4","C",09,0,0,"G","","MV_PAR04",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","","SD1NF" })
	/*5*/aAdd(aRegs,{cPerg,"05","Nota At�                ? ","","","MV_CH5","C",09,0,0,"G","","MV_PAR05",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","","SD1NF" })
	/*6*/aAdd(aRegs,{cPerg,"06","Cliente                 ? ","","","MV_CH6","C",06,0,0,"G","","MV_PAR06",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","","SA1"   })

			
	For i:=1 to Len(aRegs)
		If !dbSeek(cPerg+aRegs[i,2])
			RecLock("SX1",.T.)
			For j:=1 to FCount()
				If j <= Len(aRegs[i])
					FieldPut(j,aRegs[i,j])
				Endif
			Next
			MsUnlock()
		Endif
	Next

	DbSelectArea(_sAlias)

Return Nil