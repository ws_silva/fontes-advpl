// #########################################################################################
// Projeto: Hope
// Modulo : Faturamento
// Fonte  : HFATP007
// ---------+-------------------+-----------------------------------------------------------
// Data     | Autor             | Descricao
// ---------+-------------------+-----------------------------------------------------------
// 09/10/16 | Actual Trend      | Cadastro de Regi�es 
// ---------+-------------------+-----------------------------------------------------------

#include "rwmake.ch"

user function HFATP007
	local cVldAlt := ".T." 
	local cVldExc := ".T." 
	
	local cAlias
	
	cAlias := "SZ8"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	axCadastro(cAlias, "Cadastro de Regi�es", cVldExc, cVldAlt)
	
return
