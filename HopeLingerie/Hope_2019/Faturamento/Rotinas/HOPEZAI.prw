#INCLUDE "rwmake.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HOPEZAI   � Autor � Roberto Le�o       � Data �  08/12/16   ���
�������������������������������������������������������������������������͹��
���Descricao � Cadastro de Marca de Produto.                              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE LINGERIE                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function HOPEZAI()
Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������

Private cPerg   := "ZAI"
Private cString := "ZAI"

dbSelectArea("ZAI")
dbSetOrder(1)

cPerg   := "ZAI"

Pergunte(cPerg,.F.)
SetKey(123,{|| Pergunte(cPerg,.T.)}) // Seta a tecla F12 para acionamento dos parametros

AxCadastro(cString,"Marca de Produto",cVldExc,cVldAlt)

Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros


Return
