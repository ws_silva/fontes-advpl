#INCLUDE "PROTHEUS.CH"
#include 'parmtype.ch'
#include "rwmake.ch" 
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATA011  �Autor  �Weskley Silva      � Data �  24/09/18    ���
�������������������������������������������������������������������������͹��
���Desc.     � Rotina para informar dados de volumetria e faturar NF      ���
���          � 		               										  ���
�������������������������������������������������������������������������͹��
���Uso       � Hope Lingerie							                  ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

user function HFATA011()

	U_FILTRO2()

Return

/*
//���������������������������������������������������������������������������Ŀ
//�Rotina utilizada para alterar os dados da expedicao da nota fiscal de saida�
//�����������������������������������������������������������������������������
*/

User Function HFT011EXP(lExp)

	Local oButton1
	Local oButton2
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oGet6
	Local oGet7
	Local oGet8
	Local oGet9
	Local oMultiGe1
	Local oMultiGe2
	Local oComboBo1
	Local oComboBo2
	Local oComboBo3
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local oSay6
	Local oSay7
	Local oSay8
	Local oSay9
	Local oSay10
	Local oSay11
	Local oSay12
	Local oSay13
	Local oSay14
	Local oSay15
	Local _aAlias  
	Local cUf		:= ""
	Local aUf		:= {"",""}

	Private nTransp	:= TamSX3("C5_TRANSP")[1]
	Private nRedesp	:= TamSX3("C5_REDESP")[1]
	Private cNumPF 				:= ZZ9->ZZ9_NUMPF      		
	Private _nFilial         	
	Private _nFiscal         	
	Private _nSerie         	
	Private _nCliente			
	Private _nLoja				
	Private _cTransportadora 			
	Private _cRedespacho		
	Private _cData           	
	Private d_dtExp				
	Private c_horas				
	Private nPesoBruto			
	Private nPesoLiq			
	Private cVolume	
	Private cPriori		       	:= ""	
	Private cCombo1				
	Private aCombo1				:= {"CAIXA","PALLET","SACO","UNIDADE","CAIXA PAPELAO","BAU METAL","ROLOS"}
	Private cCombo2 			
	Private aCombo2				:= {}
	Private cCombo3				
	Private aCombo3				:= {"CIF","FOB","Por conta terceiros","Sem frete"}	//C=CIF;F=FOB;T=Por conta terceiros;S=Sem frete
	Private mMsgfis				//:= FwCutOff(AllTrim(SF2->F2_MENNOTA), .T.)
	Private mXMenNt2			//:= FwCutOff(AllTrim(SF2->F2_XMENNT2), .T.)
	Private _cQtdNF				//:= QtdNF(_nFilial,_nFiscal,_nSerie)
	Private lConf     			:= .T.
	Private lTOk     			:= .T.
	Private _xLidoTF			:= ""

	Static oDlg

	DbSelectArea("SC5")
	DbSetOrder(1)
	If !DbSeek(xfilial("SC5")+ZZ9->ZZ9_PEDIDO)
		MessageBox( "Pedido [ " + ZZ9->ZZ9_PEDIDO + " ] n�o encontrado na Filial [ " + xfilial("SC5") + " ]","Aten��o",48 )
		Return
	Endif

	If DbSeek(xfilial("SC5")+ZZ9->ZZ9_PEDIDO) .AND. ZZ9->ZZ9_STATUS == "B"		
		MessageBox( "Pedido [ " + ZZ9->ZZ9_PEDIDO + " ] com bloqueio comercial","Aten��o",48 )
		Return
	Endif

	_nFilial   			:= SC5->C5_FILIAL	
	_nFiscal   			:= SC5->C5_NUM     	
	_nSerie    			:= SC5->C5_SERIE     	
	_nCliente			:= SC5->C5_CLIENT		
	_nLoja				:= SC5->C5_LOJACLI		
	_cTransportadora 	:= SC5->C5_TRANSP		
	_cRedespacho		:= SC5->C5_REDESP
	_cData           	:= SC5->C5_EMISSAO				
	nPesoBruto			:= SC5->C5_PBRUTO
	nPesoLiq			:= SC5->C5_PESOL
	cVolume				:= SC5->C5_VOLUME1
	mMsgfis				:= "" //FwCutOff(AllTrim(SC5->C5_MENNOTA), .T.)	
	mXMenNt2			:= FwCutOff(AllTrim(SC5->C5_XMENNT2), .T.)
	_cQtdNF		    	:= QtdNF(_nFilial,_nFiscal,_nSerie)
	cCombo2         	:= SC5->C5_XTPPAG
	_cCondpag       	:= SC5->C5_CONDPAG
	_cTipoPed       	:= SC5->C5_TPPED
	_xLidoTF			:= SC5->C5_XLIDOTF

	dbSelectArea( "SA1" )
	dbSetOrder( 1 )
	If dbSeek( xFilial( "SA1" ) + _nCliente + _nLoja )
		cUf := Alltrim( Upper( SA1->A1_EST ) )
	Endif

	DbSelectArea("SX5")
	SX5->(DbSetOrder(1))
	SX5->(Dbseek('    ' + '24'))  //tabela de forma de pagamento
	while SX5->(!eof()) .and. SX5->X5_TABELA == '24'
		aadd(aCombo2,AllTrim(SX5->X5_DESCRI))
		SX5->(DbsKIP())
	Enddo

	If Len(aCombo2) == 0
		aCombo2 := {"BOLETO E-MAIL","BOLETO IMPRESSO","DEPOSITO BANCARIO","BOLETO BANCO","BOLETO E-COMMERCE","A VISTA","CARTAO","MOSTRUARIO"}
	Endif

	If lExp==.T. .or. TYPE("cRotina")=="U"

		_qry := " SELECT TOP 1 C5_NUM, C5_XPEDCLI,C5_XPEDRAK,C5_CONDPAG,C5_TPPED "
		_qry += " FROM "+RetSqlName("SC6")+" SC6 (NOLOCK) INNER JOIN "+RetSqlName("SC5")+" SC5 (NOLOCK) ON C6_FILIAL=C5_FILIAL AND C6_NUM=C5_NUM "
		_qry += " WHERE SC6.D_E_L_E_T_<>'*' AND C6_FILIAL='"+_nFilial+"' AND C6_NUM='"+_nFiscal+"' AND C6_CLI='"+_nCliente+"' "

		If Select("TMPSC6") > 0
			TMPSC6->(DbCloseArea())
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSC6",.T.,.T.)
		DbSelectArea("TMPSC6")
		DbGoTop()

		cCodCli		:= AllTrim(_nCliente)+"-"+AllTrim(_nLoja)
		cNumPed		:= TMPSC6->C5_NUM
		cNumPCli	:= TMPSC6->C5_XPEDCLI
		cNumWeb     := TMPSC6->C5_XPEDRAK
		cCondpag    := TMPSC6->C5_CONDPAG
		cTipoPed    := TMPSC6->C5_TPPED

		// Tipo do Pedido do E-COMMERCE
		if cTipoPed = "016"

			IF cCondpag $ GETMV("HP_AVISTA")

				cCombo2 := "A VISTA"

			ELSEIF cCondpag $ GETMV("HP_CARTAO")

				cCombo2 := "CARTAO"

			ELSEIF cCondpag $ GETMV("HP_BOLETO")

				cCombo2 := "CARTAO"

			ENDIF

			If cUf $ GETMV("HP_UFNORT")

				_cTransportadora := "99947"
				
				If _xLidoTF == "2"
					_cRedespacho := Space(nRedesp)
				Endif

			ElseIf cUf $ GETMV("HP_UFREGI")

				_cTransportadora := GETMV("HP_TRANSP") //"99926"

				If _xLidoTF == "2"
					If Empty( _cRedespacho ) .Or. Alltrim( _cRedespacho ) $ "999850#999851#999852"
						_cRedespacho := "99947"
					Endif
				Else
					_cRedespacho := "99947"
				Endif

			Endif

		Else 

			// Retorna Transportadora / Redespacho
			aUf := getTransp( cUf )

			_cTransportadora := aUf[ 1 ]
			_cRedespacho	 := aUf[ 2 ]
			
		Endif

		DbSelectArea("SC5")
		DbSetOrder(1)
		DbSeek(xfilial("SC5")+TMPSC6->C5_NUM)

		// Chamado ID 8873: 099-Beneficiamento, 353-Revenda de MP para as Oficinas e 345-Remessa Comodato
		If Alltrim( SC5->C5_POLCOM ) $ "099#345#353"
			_cTransportadora := "000002"	// Transporte Proprio
			_cRedespacho	 := Space(nRedesp)
		Endif

		If nPesoBruto = 0
			nPesoBruto		:= SC5->C5_PBRUTO		//SF2->F2_PBRUTO
		End
	
		If nPesoLiq = 0
			nPesoLiq		:= SC5->C5_PESOL		//SF2->F2_PLIQUI
		End

		If cVolume = 0
			cVolume			:= SC5->C5_VOLUME1
		End

		If AllTrim(cCombo1) == ""
			cCombo1			:= AllTrim(SC5->C5_ESPECI1)
		End
		
		if Alltrim(_nFiscal) != Alltrim(cNumPed) .OR. Empty(cNumPed)
			cNumPed := _nFiscal 
		endif

		If strTran(mMsgfis,"-","") <> ''
			If !("COD. CLIENTE:" $ mMsgfis)
				If AllTrim(cNumPCli) <> '' .AND. !Empty(cNumPF)
					_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+"  - PED. CLIENTE: "+cNumPCli+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
					mMsgfis	:= SubStr(mMsgfis +" - "+ _txtPad + Space(254),1,254)
				Elseif !Empty(cNumPF)
					_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
					mMsgfis	:= SubStr(mMsgfis +" - "+ _txtPad + Space(254),1,254)
				Else
					_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF:"+AllTrim(cNumPed)+"01 -"+AllTrim(cValToChar(_cQtdNF))+" P�S."
					mMsgfis	:= SubStr(mMsgfis +" - "+ _txtPad + Space(254),1,254)
				End
			Endif
		Else
			If AllTrim(cNumPCli) <> '' .AND. !Empty(cNumPF)
				_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+"  - PED. CLIENTE: "+cNumPCli+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
				mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
			ElseIF !Empty(cNumPF)
				_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF: "+Alltrim(cNumPF)+" - "+AllTrim(cValToChar(_cQtdNF))+" P�S."
				mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
			Else
				_txtPad := "COD. CLIENTE: "+cCodCli+" - PEDIDO: "+AllTrim(cNumPed)+" - NUMPF:"+AllTrim(cNumPed)+"01 -"+AllTrim(cValToChar(_cQtdNF))+" P�S."
				mMsgfis	:= SubStr(_txtPad + Space(254),1,254)
			End
		End
	End

	cPriori := GETMV("MV_XNUMSE")

	IF !Empty(cNumWeb)
		_txtPad := "PEDIDO ORACLE: "+Alltrim(cNumWeb)+" " 
		mXMenNt2	:= SubStr(_txtPad + Space(254),1,254)
	ENDIF
	
	If cCombo3 == 'C'
		cCombo3	:= "CIF"
	ElseIf cCombo3 == 'F'
		cCombo3	:= "FOB"
	ElseIf cCombo3 == 'T'
		cCombo3	:= "Por conta terceiros"
	ElseIf cCombo3 == 'S'
		cCombo3	:= "Sem frete"
	Else
		cCombo3	:= "CIF"
	End

	//���������������������������������������������������������������������������Ŀ
	//� Criacao da Interface                                                      �
	//�����������������������������������������������������������������������������

	DEFINE MSDIALOG oDlg TITLE "Expedi��o de Nota Fiscal e Mensagem para Nota" FROM 000, 000  TO 550, 500 COLORS 0, 16777215 PIXEL

	@ 008, 010 SAY oSay1 PROMPT "Pedido:   "+_nFiscal SIZE 064, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 008, 140 SAY oSay2 PROMPT "Total de Itens no Pedido:   "+cValToChar(_cQtdNF)+" pe�as" SIZE 098, 007 OF oDlg COLORS 0, 16777215 PIXEL

	@ 022, 010 SAY oSay5 PROMPT "Transportadora:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 032, 010 MSGET oGet3 VAR _cTransportadora SIZE 100, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 F3 "SA4" PIXEL
	@ 022, 140 SAY oSay6 PROMPT "Transportadora de re-despacho:" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 032, 140 MSGET oGet4 VAR _cRedespacho SIZE 100, 010 OF oDlg PICTURE "@!" COLORS 0, 16777215 F3 "SA4" PIXEL

	@ 051, 010 SAY oSay7 PROMPT "Peso Bruto:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 061, 010 MSGET oGet5 VAR nPesoBruto SIZE 100, 010 OF oDlg PICTURE "999999.9999" COLORS 0, 16777215 PIXEL
	@ 051, 140 SAY oSay8 PROMPT "Peso Liquido:" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 061, 140 MSGET oGet6 VAR nPesoLiq SIZE 100, 010 OF oDlg PICTURE "999999.9999" COLORS 0, 16777215 PIXEL

	@ 080, 010 SAY oSay9 PROMPT "Volume:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 090, 010 MSGET oGet7 VAR cVolume SIZE 100, 010 OF oDlg PICTURE "999999" COLORS 0, 16777215 PIXEL
	@ 080, 140 SAY oSay10 PROMPT "Especie:" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 090, 140 MSCOMBOBOX oComboBo1 VAR cCombo1 ITEMS aCombo1 SIZE 100, 012 OF oDlg COLORS 0, 16777215 PIXEL

	@ 109, 010 SAY oSay11 PROMPT "Forma de Pagamento:" SIZE 060, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 119, 010 MSCOMBOBOX oComboBo2 VAR cCombo2 ITEMS aCombo2 SIZE 100, 012 OF oDlg COLORS 0, 16777215 PIXEL
	@ 109, 140 SAY oSay12 PROMPT "Tipo Frete" SIZE 083, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 119, 140 MSCOMBOBOX oComboBo3 VAR cCombo3 ITEMS aCombo3 SIZE 100, 012 OF oDlg COLORS 0, 16777215 PIXEL

	//@ 137, 010 CHECKBOX oGet9 VAR cPriori  PROMPT "" SIZE 070,015 COLORS 0, 16777215 PIXEL 
	@ 137, 010 SAY oSay15 PROMPT "Sequencia:" SIZE 047, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 147, 010 MSGET oGet9 VAR cPriori SIZE 100, 010 OF oDlg PICTURE "9999999" COLORS 0, 16777215 PIXEL
	//@ 137, 140 CHECKBOX oConf VAR lConf  PROMPT "Expedido" SIZE 055,015 COLORS 0, 16777215 PIXEL OF oDlg WHEN lTOk

	@ 166, 010 SAY oSay13 PROMPT "Mensagem complementar para Nota:" SIZE 100, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 176, 010 MSGET oGet8 VAR mMsgfis SIZE 230, 010 OF oDlg PICTURE "@" COLORS 0, 16777215 PIXEL

	@ 196, 010 SAY oSay14 PROMPT "Mensagem complementar para Nota 2:" SIZE 100, 007 OF oDlg COLORS 0, 16777215 PIXEL
	@ 210, 010 GET oMultiGe2 VAR mXMenNt2 OF oDlg MULTILINE SIZE 230, 044 COLORS 0, 16777215 HSCROLL PIXEL

	If lExp == .T. //.or. TYPE("cRotina")=="U"
		@ 256, 160 BUTTON oButton2 PROMPT "Salvar" 		SIZE 037, 012 OF oDlg ACTION (SALVAR(), oDlg:End()) PIXEL
		@ 256, 204 BUTTON oButton1 PROMPT "Cancelar" 	SIZE 037, 012 OF oDlg Action oDlg:End() PIXEL
	Else
		@ 256, 204 BUTTON oButton1 PROMPT "Fechar" 		SIZE 037, 012 OF oDlg Action oDlg:End()  PIXEL
	EndIf

	ACTIVATE MSDIALOG oDlg CENTERED

	cPriori := SOMA1(cPriori)
	PutMV("MV_XNUMSE",cPriori)

Return()


/*
//���������������������������������������������������������������������������Ŀ
//�Atualiza dados de expedicao da NF.                                         �
//�����������������������������������������������������������������������������
*/

Static Function SALVAR()

	Local _cFrete	:= Iif(SubStr(cCombo3,1,1)<>'',SubStr(cCombo3,1,1),"C")
	Local _cQuery := ""
	Local cNumPF := " "
	lRet := .F.

	_cQuery := " SELECT TOP 1  ZJ_NUMPF FROM "+RetSqlName("SZJ")+" WHERE LEFT(ZJ_NUMPF,6) = '"+_nFiscal+"' AND ZJ_CLIENTE = '"+_nCliente+"' AND D_E_L_E_T_ = '' AND ZJ_DOC = '' "

	If Select("TMPSZJ") > 0
		TMPSZJ->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_cQuery),"TMPSZJ",.T.,.T.)
	DbSelectArea("TMPSZJ")
	DbGoTop()

	cNumPF := TMPSZJ->ZJ_NUMPF

	IF !EMPTY(cNumPF)
		Verifica()
	ELSE
		lRet := .T.	
	ENDIF

	ValiParc(_nFiscal)

	if lRet 

		if Empty(cNumPF)

			DbSelectarea("ZZZ")
			DbSetOrder(1)
			if DbSeek(xFilial("ZZZ")+_nFiscal+_nCliente+_nLoja)
				Msginfo("Pedido em processo de faturamento n�o pode ser alterado","Hope")
				lRet := .F. 
				return

			else	

				DbSelectarea("SC5")
				DbSetOrder(3)
				DbSeek(xFilial("SC5")+_nCliente+_nLoja+_nFiscal)

				If _nFiscal+_nCliente+_nLoja == SC5->(C5_NUM+C5_CLIENT+C5_LOJACLI) 
					RecLock("SC5",.F.)
					SC5->C5_TRANSP 		:= _cTransportadora
					SC5->C5_REDESP		:= _cRedespacho
					SC5->C5_PBRUTO		:= nPesoBruto
					SC5->C5_PESOL		:= nPesoLiq
					SC5->C5_VOLUME1		:= cVolume
					SC5->C5_ESPECI1		:= cCombo1
					SC5->C5_MENNOTA		:= mMsgfis
					SC5->C5_XMENNT2		:= mXMenNt2
					SC5->C5_TPFRETE		:= _cFrete
					SC5->C5_XEXPEDE     := "S"
					SC5->C5_XTPPAG      := cCombo2
					MSUNLOCK()

					Reclock("ZZZ", .T. )
					ZZZ->ZZZ_FILIAL  := XFILIAL("ZZZ")
					ZZZ->ZZZ_PEDIDO  := _nFiscal
					ZZZ->ZZZ_CLIENT  := _nCliente
					ZZZ->ZZZ_LOJA    := _nLoja
					ZZZ->ZZZ_STATUS  := "P"
					ZZZ->ZZZ_NUMPF   := cNumPF
					MSUNLOCK()		
					lRet := .T. 

					DbSelectarea("ZZ9")
					DbSetOrder(1)
					DbSeek(xFilial("ZZ9")+_nFiscal+_nCliente+_nLoja)

					RecLock("ZZ9",.F.)

					ZZ9->ZZ9_STATUS = 'A'

					MSUNLOCK()


				EndIF
			endif

		else 

			DbSelectarea("ZZZ")
			DbSetOrder(2)
			if DbSeek(xFilial("ZZZ")+cNumPF+_nCliente+_nLoja)
				Msginfo("Pedido em processo de faturamento n�o pode ser alterado","Hope")
				lRet := .F. 
				return

			else	

				DbSelectarea("SC5")
				DbSetOrder(3)
				DbSeek(xFilial("SC5")+_nCliente+_nLoja+_nFiscal)

				If _nFiscal+_nCliente+_nLoja == SC5->(C5_NUM+C5_CLIENT+C5_LOJACLI) 
					RecLock("SC5",.F.)
					SC5->C5_TRANSP 		:= _cTransportadora
					SC5->C5_REDESP		:= _cRedespacho
					SC5->C5_PBRUTO		:= nPesoBruto
					SC5->C5_PESOL		:= nPesoLiq
					SC5->C5_VOLUME1		:= cVolume
					SC5->C5_ESPECI1		:= cCombo1
					SC5->C5_MENNOTA		:= mMsgfis
					SC5->C5_XMENNT2		:= mXMenNt2
					SC5->C5_TPFRETE		:= _cFrete
					SC5->C5_XEXPEDE     := "S"
					SC5->C5_XTPPAG      := cCombo2
					MSUNLOCK()

					Reclock("ZZZ", .T. )
					ZZZ->ZZZ_FILIAL  := XFILIAL("ZZZ")
					ZZZ->ZZZ_PEDIDO  := _nFiscal
					ZZZ->ZZZ_CLIENT  := _nCliente
					ZZZ->ZZZ_LOJA    := _nLoja
					ZZZ->ZZZ_STATUS  := "P"
					ZZZ->ZZZ_NUMPF   := cNumPF
					MSUNLOCK()		
					lRet := .T. 

					DbSelectarea("ZZ9")
					DbSetOrder(2)
					DbSeek(xFilial("ZZ9")+cNumPF+_nCliente+_nLoja)

					RecLock("ZZ9",.F.)

					ZZ9->ZZ9_STATUS = 'A'

					MSUNLOCK()


				EndIF
			endif

		endif

	endif	
Return(lRet)


Static Function QtdNF(_nFilial,_nFiscal,_nSerie)

	_qry := " SELECT SUM(C9_QTDLIB) AS QTDNF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
	_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+_nFilial+"' AND C9_PEDIDO='"+_nFiscal+"' AND C9_NFISCAL = '' "

	If Select("TMPSFT") > 0
		TMPSFT->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSFT",.T.,.T.)
	DbSelectArea("TMPSFT")
	DbGoTop()

	cQtdNF	:= TMPSFT->QTDNF

Return(cQtdNF)


USER FUNCTION FILTRO2()

	Local lOk	:= .F.
	Local aParam := {}
	Local aRetParm	:= {}
	Local cResp := ""
	Local cExprFilTop := ""
	local cVldAlt	:= ".T." 
	local cVldExc	:= ".T." 
	Local cRotina	:= "HFATA011"
	Local cOpcao		:= "ALTERAR"
	Local nOpcE		:= 2
	Local nOpcG		:= 3
	Local aCores    := {}
	Local cCadastro	:= "Documento de Saida"
	Private cPerg     := "HFATA011"
	Private cTipos    := ""
	Private _astru    := {}

	AADD(aCores,{"ZZ9_STATUS == 'P' ", "VERDE" }) // Pendente de Faturamento
	AADD(aCores,{"ZZ9_STATUS == 'E' ", "PRETO" }) // Erro
	AADD(aCores,{"ZZ9_STATUS == 'F' ", "VERMELHO" }) // Faturado
	AADD(aCores,{"ZZ9_STATUS == 'A' ", "AMARELO" }) // EM FATURAMENTO
	AADD(aCores,{"ZZ9_STATUS == 'B' ", "PINK" }) // BLOQUEIO COMERCIAL

	MsgRun("Selecionando Registros, Aguarde...",,{|| GeraInfor()})

	aRotina := {	{ "Pesquisa"		,"AxPesqui"			,0, 1},;
	{ "Visualiza"		,'U_HFT011EXP(.F.)'	,0, 2},;
	{ "Expedi��o NF"	,'U_HFT011EXP(.T.)'	,0, 4},;
	{ "Legenda"			,'U_Legenda()'	,0, 4},;
	{ "Romaneio"		,'U_HPREL013'	,0, 1}}

	mBrowse( 6,1,22,75,"ZZ9",,,,,, aCores,,,,,,,,) 

RETURN 

User Function Legenda()	
Local aLegenda := {}

aAdd( aLegenda, { "BR_VERDE"		, "Pendente de Faturamento" })
aAdd( aLegenda, { "BR_VERMELHO"		, "Faturado" })
aAdd( aLegenda, { "BR_PRETO"		, "Erro" })
aAdd( aLegenda, { "BR_AMARELO"		, "EM FATURAMENTO" })
aAdd( aLegenda, { "BR_PINK"			, "Bloqueio Comercial" })


BrwLegenda( "Staus", "Legenda", aLegenda )

Return( Nil )

Static Function GeraInfor()

	If Select("TMPSC5") > 0
		TMPSC5->(dbCloseArea())
	EndIf

	_qry := " SELECT C5_NUM AS PEDIDO , C5_CLIENTE AS CODCLIENTE,C5_LOJACLI AS LOJA, C5_XNOMCLI AS CLIENTE ,C5_TPPED AS CODTIPO, Z1_DESC AS TIPOPEDIDO, " 
	_qry += " C5_CONDPAG AS CODPAGA,C9_XNUMPF AS NUMPF , C5_XDESCOD AS CONDICAO ,SUM(CAST(C9_QTDLIB AS int) ) AS QUANT,C9_XNUMPF,C5_XBLQ AS STATUS " 
	_qry += " FROM "+RetSqlName("SC5")+" WITH (NOLOCK) " 
	_qry += " JOIN "+RetSqlName("SC9")+" WITH (NOLOCK) ON C5_NUM = C9_PEDIDO AND C5_CLIENTE = C9_CLIENTE AND C5_LOJACLI = C9_LOJA "
	_qry += " AND C5_FILIAL = C9_FILIAL AND SC9010.D_E_L_E_T_ = '' "
	_qry += " JOIN "+RetSqlName("SZ1")+" WITH (NOLOCK) ON C5_TPPED = Z1_CODIGO AND SZ1010.D_E_L_E_T_ = '' "
	_qry += " WHERE C5_FILIAL = '" + xFilial("SC5") + "'"
	_qry += " AND SC5010.D_E_L_E_T_ = '' "
	_qry += " AND C9_NFISCAL = '' "
	//_qry += " AND C5_XBLQ = 'L' " 
	_qry += " GROUP BY C5_NUM, C5_CLIENTE,C5_XNOMCLI,C5_TPPED,Z1_DESC,C5_CONDPAG,C5_XDESCOD,C5_LOJACLI,C9_XNUMPF,C5_XBLQ "	

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSC5",.T.,.T.)

	dbSelectArea("TMPSC5")
	aCposBrw := {}
	aEstrut  := {}
	If !EOF()

		While !TMPSC5->(Eof())

			IF EMPTY(TMPSC5->NUMPF)

				DBSELECTAREA("ZZ9")
				DbSetOrder(1)

				If DbSeek(xFilial("ZZ9")+TMPSC5->PEDIDO+TMPSC5->CODCLIENTE)
					RecLock("ZZ9", .F.)
					ZZ9->ZZ9_FILIAL  := xFilial("ZZ9")
					ZZ9->ZZ9_PEDIDO  := TMPSC5->PEDIDO
					ZZ9->ZZ9_CODCLI  := TMPSC5->CODCLIENTE
					ZZ9->ZZ9_LOJA    := TMPSC5->LOJA
					ZZ9->ZZ9_NOMCLI  := TMPSC5->CLIENTE
					ZZ9->ZZ9_TPPEDI	 := TMPSC5->CODTIPO
					ZZ9->ZZ9_DESTP	 := TMPSC5->TIPOPEDIDO
					ZZ9->ZZ9_CONDPA  := TMPSC5->CODPAGA
					ZZ9->ZZ9_DESPAG	 := TMPSC5->CONDICAO
					ZZ9->ZZ9_QUANT	 := TMPSC5->QUANT

					If TMPSC5->STATUS == "L"
						ZZ9->ZZ9_STATUS  := 'P'
					Else
						ZZ9->ZZ9_STATUS  := 'B'
					EndIf

					ZZ9->(msUnlock())

				ELSE

					RecLock("ZZ9", .T.)
					ZZ9->ZZ9_FILIAL  := xFilial("ZZ9")
					ZZ9->ZZ9_PEDIDO  := TMPSC5->PEDIDO
					ZZ9->ZZ9_CODCLI  := TMPSC5->CODCLIENTE
					ZZ9->ZZ9_LOJA    := TMPSC5->LOJA
					ZZ9->ZZ9_NOMCLI  := TMPSC5->CLIENTE
					ZZ9->ZZ9_TPPEDI	 := TMPSC5->CODTIPO
					ZZ9->ZZ9_DESTP	 := TMPSC5->TIPOPEDIDO
					ZZ9->ZZ9_CONDPA  := TMPSC5->CODPAGA
					ZZ9->ZZ9_DESPAG	 := TMPSC5->CONDICAO
					ZZ9->ZZ9_QUANT	 := TMPSC5->QUANT
					
					If TMPSC5->STATUS == "L"
						ZZ9->ZZ9_STATUS  := 'P'
					Else
						ZZ9->ZZ9_STATUS  := 'B'
					EndIf
					
					ZZ9->(msUnlock())

				ENDIF

			ELSE

				DBSELECTAREA("ZZ9")
				DbSetOrder(2)

				If DbSeek(xFilial("ZZ9")+TMPSC5->NUMPF+TMPSC5->CODCLIENTE)
					RecLock("ZZ9", .F.)
					ZZ9->ZZ9_FILIAL  := xFilial("ZZ9")
					ZZ9->ZZ9_PEDIDO  := TMPSC5->PEDIDO
					ZZ9->ZZ9_CODCLI  := TMPSC5->CODCLIENTE
					ZZ9->ZZ9_LOJA    := TMPSC5->LOJA
					ZZ9->ZZ9_NOMCLI  := TMPSC5->CLIENTE
					ZZ9->ZZ9_TPPEDI	 := TMPSC5->CODTIPO
					ZZ9->ZZ9_DESTP	 := TMPSC5->TIPOPEDIDO
					ZZ9->ZZ9_CONDPA  := TMPSC5->CODPAGA
					ZZ9->ZZ9_DESPAG	 := TMPSC5->CONDICAO
					ZZ9->ZZ9_QUANT	 := TMPSC5->QUANT
					ZZ9->ZZ9_NUMPF   := TMPSC5->NUMPF

					If TMPSC5->STATUS == "L"
						ZZ9->ZZ9_STATUS  := 'P'
					Else
						ZZ9->ZZ9_STATUS  := 'B'
					EndIf
					
					ZZ9->(msUnlock())

				ELSE

					RecLock("ZZ9", .T.)
					ZZ9->ZZ9_FILIAL  := xFilial("ZZ9")
					ZZ9->ZZ9_PEDIDO  := TMPSC5->PEDIDO
					ZZ9->ZZ9_CODCLI  := TMPSC5->CODCLIENTE
					ZZ9->ZZ9_LOJA    := TMPSC5->LOJA
					ZZ9->ZZ9_NOMCLI  := TMPSC5->CLIENTE
					ZZ9->ZZ9_TPPEDI	 := TMPSC5->CODTIPO
					ZZ9->ZZ9_DESTP	 := TMPSC5->TIPOPEDIDO
					ZZ9->ZZ9_CONDPA  := TMPSC5->CODPAGA
					ZZ9->ZZ9_DESPAG	 := TMPSC5->CONDICAO
					ZZ9->ZZ9_QUANT	 := TMPSC5->QUANT
					If TMPSC5->STATUS == "L"
						ZZ9->ZZ9_STATUS  := 'P'
					Else
						ZZ9->ZZ9_STATUS  := 'B'
					EndIf
					ZZ9->ZZ9_NUMPF   := TMPSC5->NUMPF
					ZZ9->(msUnlock())

				ENDIF

			ENDIF

			TMPSC5->(DbSkip())
		Enddo

	EndIf

	If Select("TMPSC5") > 0
		TMPSC5->(dbCloseArea())
	EndIf

Return 

Static Function Verifica()	

	Private oFontN    := TFont():New("Arial",,20,,.t.,,,,,.f.)
	Private oFont2    := TFont():New("Arial",,12,,.t.,,,,,.f.)
	lRet := .T.
	_nPedido         	:= _nFiscal
	_nPreFat			:= cNumPF
	_nFilial         	:= _nFilial
	_cQtdNF				:= QtdPed(_nFilial,_nPreFat,1) 
	_cVlrNF				:= QtdPed(_nFilial,_nPreFat,2)
	_cQtdCF				:= QtdPed(_nFilial,_nPreFat,3)

	@ 156,367 To 400,934 Dialog mkwdlg Title OemToAnsi("Confer�ncia da Quantidade do Pedido")
	n_linha	:= 015
	n_latu	:= 013

	@ n_latu  ,018	Say OemToAnsi("Pedido:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
	@ n_latu  ,063	say _nPreFat Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
	n_latu +=  n_linha

	@ n_latu  ,018	Say OemToAnsi("Cliente:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
	@ n_latu  ,063	Say _nCliente Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
	n_latu +=  n_linha //+010

	DbSelectArea("SA1")
	DbSetOrder(1)
	DbSeek(xfilial("SA1")+_nCliente+_nLoja)

	@ n_latu  ,018	Say OemToAnsi("Nome:") Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
	@ n_latu  ,063	Say SA1->A1_NOME Size 100,12 COLOR CLR_BLACK FONT oFont2 PIXEL OF mkwdlg
	n_latu +=  n_linha //+010

	@ n_latu+8,018	Say OemToAnsi("Total de Itens Liberados: "+ cValToChar(_cQtdNF) +" pe�as") Size 200,120 COLOR CLR_BLACK FONT oFontN PIXEL OF mkwdlg
	n_latu +=  n_linha+010

	@ n_latu+8,018	Say OemToAnsi("Vr Total de Itens Liberados: R$ "+ cValToChar(_cVlrNF) +" ") Size 200,120 COLOR CLR_BLACK FONT oFontN PIXEL OF mkwdlg
	n_latu +=  n_linha+010

	@ n_latu,035 BUTTON oButton2  PROMPT "_Cancelar"    SIZE 036, 016 OF mkwdlg ACTION (lRet:=.F.,mkwdlg:End()) PIXEL MESSAGE "Cancelar"
	@ n_latu,075 BUTTON oButton2  PROMPT "C_ontinuar"    SIZE 036, 016 OF mkwdlg ACTION (lRet:=.T.,mkwdlg:End()) PIXEL MESSAGE "Continuar"

	Activate Dialog mkwdlg CENTERED 

	If _cQtdNF <> _cQtdCF
		MessageBox("Este pedido possui diferen�a entre as quantidades aptas a faturar e a confer�ncia do DAP. O mesmo n�o poder� ser faturado!","Aten��o", 48)	
		lRet := .F.
	Endif

Return(lRet)

Static Function QtdPed(_nFilial,_nPreFat,_tp)

	If _tp = 1
		_qry := " SELECT ISNULL(SUM(C9_QTDLIB),0) AS QTDNF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+_nFilial+"' AND C9_XNUMPF='"+_nPreFat+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' and C9_BLEST = '' "
	ElseIf _tp = 2
		_qry := " SELECT ISNULL(SUM(C9_QTDLIB*C9_PRCVEN),0) AS QTDNF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+_nFilial+"' AND C9_XNUMPF='"+_nPreFat+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' and C9_BLEST = '' "
	Else
		_qry := " Select Sum(ZJ_QTDSEP) as QTDNF from "+RetSQLName("SZJ")+" SZJ (NOLOCK) where SZJ.D_E_L_E_T_ = '' and ZJ_CONF = 'S' and ZJ_DOC = '' and ZJ_NUMPF in ( "
		_qry += " SELECT C9_XNUMPF FROM "+RetSQLName("SC9")+" SC9 (NOLOCK) "
		_qry += " WHERE D_E_L_E_T_<>'*' AND C9_FILIAL='"+_nFilial+"' AND C9_XNUMPF='"+_nPreFat+"' and C9_NFISCAL = '' and C9_XBLQ = 'L' "
		_qry += " group by C9_XNUMPF) "
	Endif

	If Select("TMPSFT") > 0
		TMPSFT->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSFT",.T.,.T.)
	DbSelectArea("TMPSFT")
	DbGoTop()

	cQtdNF	:= TMPSFT->QTDNF

	Return(cQtdNF) 

	Static Functio ValiParc(_nPedido)

	Local lRet		:= .F.
	Local nValTot		
	Local nPedido	:= _nPedido	
	Local cNovaCond	:= ""

	cQuery := " SELECT ISNULL(SUM(C9_QTDLIB*C9_PRCVEN),0) AS VALOR " 
	cQuery += " FROM "+RetSqlName("SC6")+" (NOLOCK) "  
	cQuery += " JOIN "+RetSqlName("SC9")+" (NOLOCK) ON  C6_NUM = C9_PEDIDO AND C6_CLI = C9_CLIENTE "
	cQuery += " AND C6_LOJA = C9_LOJA AND C6_PRODUTO = C9_PRODUTO AND C6_FILIAL = C9_FILIAL AND SC9010.D_E_L_E_T_ = '' "
	cQuery += " WHERE SC6010.D_E_L_E_T_ = '' "
	cQuery += " AND C9_PEDIDO = '"+nPedido+"' "
	cQuery += " AND C6_BLQ = '' "
	cQuery += " AND C9_NFISCAL = '' "
	cQuery += " AND C9_XBLQ = 'L' " 


	If Select("TMPVAL") > 0
		TMPVAL->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPVAL",.T.,.T.)
	DbSelectArea("TMPVAL")
	DbGoTop()

	nValTot	:= TMPVAL->VALOR


	DbSelectArea("SC5")
	_areaSC5 := GetArea()
	DbSetOrder(1)
	DbSeek(xfilial("SC5")+nPedido)

	If !(SC5->C5_POLCOM $ GetMV("MV_XPOL300"))

		dbSelectArea("SE4")
		DbSetOrder(1)
		DbSeek(xfilial("SE4")+SC5->C5_CONDPAG)
		cCondAtu	:= SE4->E4_CODIGO

		aParc := Condicao(nValTot,cCondAtu,,dDataBase)

		If Len(aParc) > 1

			While aParc[1,2] < Getmv("MV_XVALMIN") .AND. Len(aParc) > 1

				MessageBox("Valor m�nimo da Parcela n�o permitido. Favor corrigir a Condi��o de Pagamento. Parcela m�nima permitida: R$ "+AllTrim(Str(Getmv("MV_XVALMIN"))),"Aten��o",16)
				lRet := .F.

				If Pergunte("HFATP01",.T.)

					cNovaCond	:= MV_PAR01
					aParc := Condicao(nValTot,cNovaCond,,dDataBase)

				Else

					Return lRet

				End

			EndDo
		End

		If cNovaCond <> ""

			SC5->(Reclock("SC5",.F.))
			Replace SC5->C5_CONDPAG with cNovaCond
			Replace SC5->C5_XCONDOR with cCondAtu
			SC5->(MsUnLock())

			lRet := .T.

			RestArea(_areaSC5)

		End

	End 

Return lRet

Static Function QueryMarca(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
		SELECT R_E_C_N_O_ AS REGZZ9 FROM %table:ZZ9% (NOLOCK) ZZ9 WHERE ZZ9_FILIAL=%xFilial:ZZ9% AND ZZ9.ZZ9_OK = %Exp:cMarca% and ZZ9.D_E_L_E_T_ = ' '
		EndSql
Return (cAlias)->(!EOF())

Static Function LimparMarca()
	Local cAlias	:= GetNextAlias()
	QueryMarca(cAlias)
	While (cAlias)->(!EOF())
		ZZ9->(DbGoTo((cAlias)->REGZZ9))
		RecLock("ZZ9")
		ZZ9->ZZ9_OK		:= Space(Len(ZZ9->ZZ9_OK))
		MsUnLock()
		(cAlias)->(DbSkip())
	EndDO
Return

Static Function QueryDsMr(cAlias)
	Local cMarca	:= oMark:Mark()
	BeginSql Alias cAlias
		SELECT R_E_C_N_O_ AS REGZZ9 FROM %table:ZZ9% (NOLOCK) ZZ9 WHERE ZZ9_FILIAL=%xFilial:ZZ9% AND ZZ9.ZZ9_OK <> %Exp:cMarca% and ZZ9.D_E_L_E_T_ = ' '
		EndSql
Return (cAlias)->(!EOF())

Static Function Valid()
	Local cMarca	:= CVALTOCHAR(oMark:Mark())
	Local ncout := 0
	Local lRet := .F.
	Local cPed:= ""			

		cquery:= " SELECT ZZ9_PEDIDO FROM "+RetSqlName("ZZ9")+" (NOLOCK) ZZ9 WHERE ZZ9_FILIAL = '"+ZZ9->ZZ9_FILIAL+"' AND ZZ9.ZZ9_PEDIDO = '"+ZZ9->ZZ9_PEDIDO+"' and ZZ9.D_E_L_E_T_ = '' GROUP BY ZZ9_PEDIDO "
		
		If Select("T01") > 0
		T01->(DbCloseArea())
		EndIf

		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cquery),"T01",.T.,.T.)
		ncout := T01->(LASTREC())
		
		If ncout > 1
		cPed:=	T01->ZZ9_PEDIDO		
		Return lRet
		else
			lRet:= .T.
		EndIf

Return 	lRet

Static Function getTransp( cUf )
	
	Local aRet	:= { Space(nTransp), Space(nRedesp) }	// { [1]-Transportadora, [2]-Redespacho }

	Do Case 

		//Transportadora TAM/Argius 	 
		//�	S�o Paulo (SP)
		Case cUf $ "SP"
			aRet[1] := PADR("99926",nTransp)
			aRet[2] := PADR("96489",nRedesp)

		//Transportadora AZUL/Braspress
		//�	Distrito Federal (DF)
		//�	Rio de Janeiro (RJ)
		//�	Esp�rito Santo (ES)
		Case cUf $ "DF/RJ/ES"
			aRet[1] := PADR("999818",nTransp)
			aRet[2] := PADR("99977",nRedesp)

		//Transportadora TAM/Translovato
		//�	Paran� (PR)
		//�	Santa Catarina (SC)
		//�	Rio Grande do Sul (RS)
		//�	Minas Gerais (MG)
		Case cUf $ "PR/SC/RS/MG"
			aRet[1] := PADR("99926",nTransp)
			aRet[2] := PADR("00530",nRedesp)

		//Transportadora AZUL/TNT 
		//�	Mato Grosso (MT)
		//�	Mato Grosso do Sul (MS)
		//�	Tocantins (TO)
		//�	Goi�s (GO)
		Case cUf $ "MT/MS/TO/GO"
			aRet[1] := PADR("999818",nTransp)
			aRet[2] := PADR("99979",nRedesp)

		//Transpor. Braspress Nord. ( Sem Redespacho )
		//�	Cear� (CE)
		//�	Bahia (BA)
		//�	Alagoas (AL)
		//�	Para�ba (PB)
		//�	Maranh�o (MA)
		//�	Pernambuco (PE)
		//�	Piau� (PI)
		//�	Rio Grande do Norte (RN)
		//�	Sergipe (SE)
		Case cUf $ "CE/BA/AL/PB/MA/PE/PI/RN/SE"
			aRet[1] := PADR("99977",nTransp)
	
	EndCase

Return( aRet )

