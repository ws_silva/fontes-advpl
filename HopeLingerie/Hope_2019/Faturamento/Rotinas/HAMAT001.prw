#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

//0HR62890

/*/{Protheus.doc} HAMAT001 
Programa que valida a politica digitada de acordo com os cadastrosxamarracoes da SZ4
@author Melo
@since 17/01/2019
@version 1.0
/*/
User Function HAMAT001()

Local cEoL		:= Chr(13)+Chr(10)
Local cQuery	:= ""
Local cAliasSZ4 := GetNextAlias()
Local aAreaSZ4  := SZ4->(GetArea())
Local lRet      := .F.  
Local cUsrLibTP := Alltrim(SuperGetMV("HP_USLIBTP",.F.,"000036/000165/000535/000242/000401/000579/000201/000164/000363/000362")) //Usuarios Liberados para Digital qualquer tipo de pedido mesmo nao amarrados na SZ4

If !(__cUserID $ (cUsrLibTP))

    //Busca registros na SZ4 ligados ao campo Z4_POLITIC digitado pelo usuario
    cQuery += "SELECT *  
    cQuery += "  FROM "+RetSqlName("SZ4")+" SZ4"        +cEoL
    cQuery += " WHERE Z4_POLITIC = '"+M->C5_POLCOM+"'"  +cEoL
    cQuery += "   AND SZ4.D_E_L_E_T_ <> '*'"          	+cEoL
    cQuery := ChangeQuery(cQuery)

    DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),cAliasSZ4, .F., .T.)       

    (cAliasSZ4)->(dbgotop())

    While (cAliasSZ4)->(!Eof()) 
        If Alltrim(M->C5_TPPED) == Alltrim((cAliasSZ4)->Z4_TPPED)
            lRet:= .T.   
        Endif
        (cAliasSZ4)->(dbSkip()) 
    Enddo

    RestArea(aAreaSZ4)

    If !lRet
        MsgAlert("Tipo de Pedido Invalido!")
    Endif
Else
    lRet:= .T.
Endif

Return lRet