#INCLUDE "PROTHEUS.CH"
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"

User Function Teste1()

	Local cAlias        := ""
    Local cIdLincros    := ""
	
	// Configuracao de ambiente
	RpcClearEnv()
	RpcSetType( 3 )
	RpcSetEnv( "01","0101" )

    cAlias    := GetNextAlias()

	BEGINSQL ALIAS cAlias

        SELECT R_E_C_N_O_ REG
        FROM  SC5010
        WHERE 
            C5_FILIAL = '0101' AND 
            C5_XLIDOTF  = '' AND 
            C5_ORIGEM = 'B2C' AND 
            C5_EMISSAO >= '20190423' AND 
            C5_EMISSAO <= '20190423' AND 
            D_E_L_E_T_= ' '

    ENDSQL

/*
        SELECT R_E_C_N_O_ REG
        FROM  SC5010
        WHERE 
            C5_FILIAL = '0101' AND 
            C5_XLIDOTF IN ('') AND 
            C5_ORIGEM = 'B2C' AND 
            C5_EMISSAO >= '20190601' AND 
            C5_EMISSAO < '20190701' AND 
            D_E_L_E_T_= ' '
*/

    While (cAlias)->(!Eof())

		cIdLincros := cValToChar( Val( GetMV( "ID_LINCROS" ) ) + 1 )
		PutMV( "ID_LINCROS", cIdLincros )

        dbSelectArea( "SC5")
        SC5->( dbGoTo( (cAlias)->REG ) )
        RecLock( "SC5", .F. )
        SC5->C5_TRANSP  := ""
        SC5->C5_REDESP  := ""
        SC5->C5_XIDLINC := cIdLincros
        MsUnlock()

        dbSelectArea( cAlias )
        (cAlias)->( dbSkip() )

    Enddo

    (cAlias)->( dbCloseArea() )

Return
