#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"
#INCLUDE "topconn.ch"
#INCLUDE "shell.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HFATP110  �Autor  �Daniel Pereira Souza � Data � 01/12/16   ���
�������������������������������������������������������������������������͹��
���Desc.     �Programa utilizado para alterar tipo de pedido da WEB       ���
���          �com de para da tabela SZ1 para padrao PROTHEUS              ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function HFATP110()
Local dxData := dtos(date()+20)
Local nTP    := ''
lOCAL _yqry  := ""
Local _xqry  := "SELECT PEDIDO_SIGA,PEDIDO_MILLENNIUM,TIPO_PEDIDO,* FROM [MOBILE].[dbo].[PEDIDO_WEB] WHERE DT_EMISSAO between '20170401' and '"+dxData+"' AND PEDIDO_SIGA ='' "

Prepare Environment Empresa "01" Filial "0101"
	
If Select("XTMP") > 0
	XTMP->(DbCloseArea())
Endif

dbUseArea(.T.,"TOPCONN",TcGenQry(,,_xQry),"XTMP",.T.,.T.)

DbSelectArea("XTMP")
DbGoTop()
While !EOF()
   XTP:= ALLTRIM(TIPO_PEDIDO)
   _yqry := "SELECT Z1_CODIGO,Z1_DESC,Z1_CODMIL FROM SZ1010 WHERE Z1_CODMIL = '"+XTP+"' AND D_E_L_E_T_ = '' "
   If Select("YTMP") > 0
	  YTMP->(DbCloseArea())
   Endif
   dbUseArea(.T.,"TOPCONN",TcGenQry(,,_yQry),"YTMP",.T.,.T.)
   DbSelectArea("YTMP")
   If YTMP->(!Eof())        
      IF XTMP->TIPO_PEDIDO <> YTMP->Z1_CODIGO 
         xnumero:= alltrim(str(XTMP->NUM_PED))
	    _qry := "UPDATE [MOBILE].[dbo].[PEDIDO_WEB] SET TIPO_PEDIDO = '"+YTMP->Z1_CODIGO+"' WHERE PEDIDO_MILLENNIUM = '"+XTMP->PEDIDO_MILLENNIUM+"' AND NUM_PED = '"+xnumero+"' " 
	    TcSqlExec(_qry)
      Endif
   Endif   
   DbSelectArea("XTMP")
   dbskip()
Enddo        
	
	Reset Environment
	
Return       
