#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

//0HR62890

/*/{Protheus.doc} HATUSQL 
Rotina que Atualiza o campo ZL_STATUS liberando ou bloqueando o mesmo de acordo com 
as datas de liberacao e data de encerramento
@author Melo
@since 17/01/2019
@version 1.0
/*/
User Function HAE001()

Prepare Environment Empresa "01" Filial "0101"

//Libera produtos de acordo com as datas de liberacao
fLibSZL()

//Bloqueia produtos de acordo com as datas de encerramento
fBlqSZL()

Reset Environment

Return

/*/{Protheus.doc} fLibSZL 
Libera produtos de acordo com as datas de liberacao
@author Melo
@since 15/01/2019
@version 1.0
/*/
Static Function fLibSZL()
Local cEoL		:= Chr(13)+Chr(10)
Local cQuery	:= ""
Local cAliasSZL:= GetNextAlias()
Local aAreaSZL:= SZL->(GetArea())
Local aAreaSB1:= SB1->(GetArea())

//Liberacao
cQuery += "SELECT R_E_C_N_O_ as REG,ZL_PRODUTO+ZL_COR+ZL_TAM AS COD, *  
cQuery += "  FROM "+RetSqlName("SZL")+" SZL"                       			+cEoL
cQuery += " WHERE ZL_STATUS = 'PNV'"                                		+cEoL
cQuery += "   AND ZL_DTLIBER <= "+dtos(dDatabase)+""                    	+cEoL
cQuery += "   AND ZL_DTLIBER <> ''"                                 		+cEoL
cQuery += "   AND (ZL_DTENCER > "+dtos(dDatabase)+" OR ZL_DTENCER = '')"  	+cEoL
cQuery += "   AND SZL.D_E_L_E_T_ <> '*'"                           			+cEoL
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),cAliasSZL, .F., .T.)       

(cAliasSZL)->(dbgotop())

While (cAliasSZL)->(!Eof()) 
	dbSelectArea("SZL")
	SZL->(dbGoTo((cAliasSZL)->REG))
    
	RecLock("SZL",.F.)
	SZL->ZL_STATUS:= "PV"
	MsUnlock()
	
	/*DBSelectArea("SB1")
	dbSetorder(1)
	DBSEEK(XFILIAL("SB1")+(cAliasSZL)->COD)
	RecLock("SB1",.F.)
	SB1->B1_YSITUAC := "PV"
	MsUnlock()
	*/ 
	
	
	_cQry := " UPDATE SB1010 SET B1_YSITUAC = 'PV' WHERE B1_COD = '"+Alltrim((cAliasSZL)->COD)+"' AND D_E_L_E_T_ = ''  "
	TcSqlExec(_cQry)
	
	
	(cAliasSZL)->(dbSkip()) 
Enddo

RestArea(aAreaSZL)
RestArea(aAreaSB1)

Return



/*/{Protheus.doc} fBlqSZL 
Bloqueia produtos de acordo com as datas de encerramento
@since 15/01/2019
@version 1.0
/*/
Static Function fBlqSZL()
Local cEoL		:= Chr(13)+Chr(10)
Local cQuery	:= ""
Local cAliasSZL:= GetNextAlias()
//Bloqueio 
cQuery += "SELECT R_E_C_N_O_ as REG,ZL_PRODUTO+ZL_COR+ZL_TAM AS COD, *  
cQuery += "  FROM "+RetSqlName("SZL")+" SZL"            +cEoL
cQuery += " WHERE ZL_STATUS = 'PV'"            		  	+cEoL
cQuery += "   AND ZL_DTENCER <= "+dtos(dDatabase)+" " 	+cEoL
cQuery += "   AND ZL_DTENCER <> ''"             		+cEoL
cQuery += "   AND SZL.D_E_L_E_T_ <> '*'"        		+cEoL
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),cAliasSZL, .F., .T.)       

(cAliasSZL)->(dbgotop())

While (cAliasSZL)->(!Eof()) 
	dbSelectArea("SZL")
	SZL->(dbGoTo((cAliasSZL)->REG))

    RecLock("SZL",.F.)
	SZL->ZL_STATUS:= "PNV"
	MsUnlock()
	
	/*DBSelectArea("SB1")
	dbSetorder(1)
	DBSEEK(XFILIAL("SB1")+(cAliasSZL)->COD)
	RecLock("SB1",.F.)
	SB1->B1_YSITUAC := "PNV"
	MsUnlock()
	*/ 
	
	_cQry := " UPDATE SB1010 SET B1_YSITUAC = 'PNV' WHERE B1_COD = '"+Alltrim((cAliasSZL)->COD)+"' AND D_E_L_E_T_ = ''  "
	TcSqlExec(_cQry)
	
	(cAliasSZL)->(dbSkip()) 
Enddo

Return