#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"
#include "TOTVS.CH"

#define MB_ICONHAND                 16
#define MB_ICONEXCLAMATION          48

User Function HFATP032()
                        
	Local oButton1
	Local oButton2
	Local oButton3
	Local oGet1
	Local oGet2
	Local oGet3
	Local oGet4
	Local oGet5
	Local oSay1
	Local oSay2
	Local oSay3
	Local oSay4
	Local oSay5
	Local cUsers		:= ""
	Local lAltera		:= .F.
	Private cGetTabela	:= DA0->DA0_CODTAB
	Private cGetDescTab	:= DA0->DA0_DESCRI
	Private cGetVigDe	:= DA0->DA0_DATDE
	Private cGetVigAte	:= DA0->DA0_DATATE
	Private cGetCodPrd	:= Space(8)
	Private cColuna		:= Space(6)

	If Alltrim(DA0->DA0_TABVIR) == ""
		
		cUsers		:= AllTrim(GetMv("MV_HTABPR1"))
//		cUsers		+= "|" + AllTrim(SuperGetMV("MV_TABPR2",.F.,"000000"))
//		cUsers		+= "|" + "000088"    //Codigo Daniel Melo
	
		If __cUserId $ cUsers
			If MsgYesNo("Desej� alterar os valores da Tabela de Pre�os?","A T E N � � O")
				lAltera		:= .T.
			End
		End
		
		Static oDlg
	
		If lAltera		
			DEFINE MSDIALOG oDlg TITLE "Ajuste de Pre�os - Alterar" 	FROM 000, 000  TO 555, 990 COLORS 0, 16777215 PIXEL
		Else
			DEFINE MSDIALOG oDlg TITLE "Ajuste de Pre�os - Visualizar" 	FROM 000, 000  TO 555, 990 COLORS 0, 16777215 PIXEL
		End
		
		@ 006, 007 SAY oSay1 PROMPT "Cod. Tabela.:"			SIZE 037, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 004, 044 MSGET oGet1 VAR cGetTabela				SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 006, 118 SAY oSay2 PROMPT "Descri��o:"			SIZE 031, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 004, 149 MSGET oGet2 VAR cGetDescTab				SIZE 341, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 023, 007 SAY oSay3 PROMPT "Vigencia de:"			SIZE 034, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 021, 044 MSGET oGet3 VAR cGetVigDe				SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 023, 120 SAY oSay4 PROMPT "Vigencia At�:"			SIZE 034, 007 OF oDlg COLORS 0, 16777215 PIXEL
		@ 021, 158 MSGET oGet4 VAR cGetVigAte				SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
		@ 023, 241 SAY oSay5 PROMPT "Refer�ncia:"			SIZE 034, 007 OF oDlg COLORS 0, 16777215 PIXEL
	
		If lAltera
			@ 021, 277 MSGET oGet5 VAR cGetCodPrd				SIZE 078, 010 OF oDlg VALID fMSNewGe1(1,lAltera) COLORS 0, 16777215 F3 "SB4" PIXEL
			fMSNewGe1(0,lAltera)
			@ 258, 363 BUTTON oButton3 PROMPT "Replicar Linha"	SIZE 037, 012 ACTION ReplyLn()		OF oDlg PIXEL
			@ 258, 408 BUTTON oButton2 PROMPT "Cancela"			SIZE 037, 012 ACTION Close(oDlg)	OF oDlg PIXEL
			@ 258, 453 BUTTON oButton1 PROMPT "Confirma"		SIZE 037, 012 ACTION Save()			OF oDlg PIXEL
		Else
			@ 021, 277 MSGET oGet5 VAR cGetCodPrd				SIZE 078, 010 OF oDlg VALID fMSNewGe1(1,lAltera) COLORS 0, 16777215 F3 "SB4" PIXEL
			fMSNewGe1(0,lAltera)
			@ 258, 408 BUTTON oButton2 PROMPT "Fechar"			SIZE 037, 012 ACTION Close(oDlg)	OF oDlg PIXEL
		End
		
		ACTIVATE MSDIALOG oDlg CENTERED
	  
	Else
	  	MessageBox("A Tabela "+cGetTabela+" selecionada e virtual, neste caso o ajuste de ver� ser efetuado na tabela principal "+DA0->DA0_TABVIR,"Atualiza��o Negada",MB_ICONHAND)
	End
	
Return

Static Function fMSNewGe1(_tp,Altera)

	Local nX
	Local aHeaderEx 	:= {}
	Local aColsEx 		:= {}
	Local aFieldFill 	:= {}
	Local aFields 		:= {"ZD_COR","ZD_DESCRIC"}
	Local aAlterFields 	:= {}
	Local cProduto		:= SubString(cGetCodPrd,1,15)
	Static oMSGrade
	
	cColuna		:= POSICIONE("SB4",1,xFilial("SB4")+cProduto,"B4_COLUNA")

  // Define field properties
	DbSelectArea("SX3")
	SX3->(DbSetOrder(2))
	If SX3->(DbSeek("ZD_COR"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_COR","@!",3,0,,SX3->X3_USADO,"C","","R","",""})
	Endif
	If SX3->(DbSeek("ZD_DESCRIC"))
		Aadd(aHeaderEx, {alltrim(X3Titulo())+Space(4),"ZD_DESCRIC","@!",20,0,,SX3->X3_USADO,"C","","R","",""})
	Endif

	If alltrim(cColuna) <> ""
		cQuery  := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WITH (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +cColuna +"' AND D_E_L_E_T_ = '' "
		cQuery  += "ORDER BY R_E_C_N_O_ "

		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)
	  
		DbSelectArea("TMPSBV")

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" WITH (NOLOCK) "
		cQuery2  += "WHERE D_E_L_E_T_ = ' ' AND ZD_FILIAL = '"+xFilial("SZD")+"' AND ZD_PRODUTO='"+SubString(cGetCodPrd,1,15)+"' "

		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)

		DbSelectArea("TMPSZD")
  	
		While !EOF() .and. TMPSBV->BV_TABELA == cColuna
			DbSelectArea("SX3")
			_campo		:= "ZH_TAM"+StrZero(len(aFields)-1,3)
			cZdVerif	:= 0
		
			Aadd(aFields,_campo)
			Aadd(aAlterFields,_campo)

			TMPSZD->(DbGoTop())

			While TMPSZD->(!EOF())
				If &("TMPSZD->ZD_TAM"+StrZero(len(aFields)-2,3))=="X"
					cZdVerif += 1
				
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo)) .and. cZdVerif==1
						Aadd(aHeaderEx, {alltrim(TMPSBV->BV_DESCRI)+Space(4),SX3->X3_CAMPO,"@E 9,999,999.999999",14,6,"",SX3->X3_USADO,"N","","R","",""})
					Endif
				Endif
	 		
				TMPSZD->(DbSkip())
			EndDo
  	
			DbSelectArea("TMPSBV")
			DbSkip()
		End
		DbClosearea()
	Else
		Aadd(aFields,"ZD_TAM001")
		Aadd(aAlterFields,"ZD_TAM001")

		DbSelectArea("SX3")
		SX3->(DbSetOrder(2))
		If SX3->(DbSeek("ZD_TAM001"))
			Aadd(aHeaderEx, {AllTrim("Unico"),SX3->X3_CAMPO,"@!",12,0,"",SX3->X3_USADO,"C","","R","",""})
		Endif
	Endif

	DbSelectArea("SZD")
	DbSetOrder(1)
	DbSeek(xfilial("SZD")+SubString(cGetCodPrd,1,15))
    		
	While !EOF() .and. AllTrim(SZD->ZD_PRODUTO) = AllTrim(cGetCodPrd)
  		
		Aadd(aFieldFill, SZD->ZD_COR)
		Aadd(aFieldFill, SZD->ZD_DESCRIC)
		For nX := 3 to Len(aHeaderEx)
			_campo := "ZD_TAM"+RIGHT(ALLTRIM(aHeaderEx[nx][2]),3)   //StrZero(nX,3)
			IF SZD->&_campo == "X"
				_tam := Substring(rettam(VAL(RIGHT(ALLTRIM(aHeaderEx[nx][2]),2)),cColuna,"C",1),1,4)
				DbSelectArea("DA1")
				DbSetOrder(1)
				DbSeek(xfilial("DA1")+cGetTabela+AllTrim(cGetCodPrd)+SZD->ZD_COR+_tam)
				If Found() .and. DA1->DA1_PRCVEN > 0
					_campx := DA1->DA1_PRCVEN
					Aadd(aFieldFill,_campx)
				Else
					Aadd(aFieldFill,0)
				Endif	
				
			Else
				_tam := Substring(rettam(VAL(RIGHT(ALLTRIM(aHeaderEx[nx][2]),2)),cColuna,"C",1),1,4)
				DbSelectArea("DA1")
				DbSetOrder(1)
				DbSeek(xfilial("DA1")+cGetTabela+AllTrim(cGetCodPrd)+SZD->ZD_COR+_tam)
				If Found() .and. DA1->DA1_PRCVEN > 0
					_campx := DA1->DA1_PRCVEN
					Aadd(aFieldFill,_campx)
				else	
					Aadd(aFieldFill,0)
				Endif		
			Endif
		Next
		Aadd(aFieldFill,0)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSelectArea("SZD")
		DbSkip()
	End
	
	If Altera
		oMSGrade := MsNewGetDados():New( 040, 007, 252, 491, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
	Else
		oMSGrade := MsNewGetDados():New( 040, 007, 252, 491, , "AllwaysTrue", "AllwaysTrue", "+Field1+Field2", aAlterFields,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlg, aHeaderEx, aColsEx)
	End
   
	If _tp == 1
		oDlg:refresh()
		oMSGrade:refresh()
	End
Return

Static Function rettam(cPar1,xcoluna,_tp,Qry)
// Rotina para retornar o nome da coluna de Tamanho
// O mesmo n�mero da coluna � posi��o do SBV.
	_bvTam := ""
	
	If Qry	==	1
		cQuery  := "SELECT Top "+AllTrim(str(cPar1))+" BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WITH (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' "
		cQuery	+= "ORDER BY R_E_C_N_O_"
	ElseIf Qry	==	2
		cQuery  := "SELECT Top 1 BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI FROM " +RetSQLName("SBV") + " WITH (NOLOCK) WHERE "
		cQuery  += "BV_FILIAL = '" +xFilial("SBV") +"' AND BV_TABELA = '" +xcoluna+ "' AND D_E_L_E_T_ = '' AND BV_DESCRI='"+cPar1+"'"
		cQuery += "ORDER BY R_E_C_N_O_"
	End

	If Select("TMPSBV") > 0
		TMPSBV->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPSBV",.T.,.T.)

	DbSelectArea("TMPSBV")
	DbGoTop()
	While !TMPSBV->(EOF())
		If _tp == "C"
			_bvTam := TMPSBV->BV_CHAVE
		Else
			_bvTam := TMPSBV->BV_DESCRI
		Endif
		DbSkip()
	End

	DbCloseArea()

Return(_bvTam)


Static Function ReplyLn()
// Replica valor do tamanho selecionado para os outros tamanhos na mesma cor

	Local nX

	If MsgYesNo("Ser�o replicadas as informa��es da Cor: "+oMSGrade:aCols[oMSGrade:nat,1]+" - Tamanho: "+AllTrim(oMSGrade:aHeader[oMSGrade:oBrowse:ColPos,1])+" para os outros tamanhos?")
		For nX := 3 to len(oMSGrade:aCols[oMSGrade:nat])-1
			If nX <> oMSGrade:oBrowse:ColPos
				oMSGrade:aCols[oMSGrade:nat,nX] := oMSGrade:aCols[oMSGrade:nat,oMSGrade:oBrowse:ColPos]
			End
		Next nX
	End

	oMSGrade:refresh()

Return

Static Function Save()

	local i
	local w
	local y
	local x
	Local nItem		:= 0
	Local lRet		:= .F.
	Local aTabVirt	:= {}
	Local cTabVirt	:= ""
	Local nPreco	:= 0
	
	aTabVirt := TabVirt()
	
	For i := 1 to Len(OMSGrade:ACOLS) 
		//TODO Comentado por Weskley Silva 31/07/2018 [Retirado a verifica��o da ultima posi��o do vetor, pois tem cores que n�o existe para todos os tamanhos]
		//If OMSGrade:ACOLS[i,len(OMSGrade:AHEADER)+1] == 0
			For w := 3 to len(OMSGrade:aHeader)

				_tam := Substring(rettam(AllTrim(OMSGrade:AHEADER[w,1]),cColuna,"C",2),1,4)
				
				For y := 1 to Len(aTabVirt)

					DbSelectArea("DA1")
					DbSetOrder(1)
					DbSeek(xfilial("DA1")+aTabVirt[y,2,2]+AllTrim(cGetCodPrd)+OMSGrade:ACOLS[i,1]+_tam)
					
					If aTabVirt[y,3,2] <> 0
						nPreco	:= OMSGrade:ACOLS[i,w] * aTabVirt[y,3,2]
					Else
						nPreco	:= OMSGrade:ACOLS[i,w]
					End
					
					If Found() .and. alltrim(DA1->DA1_CODPRO) == AllTrim(cGetCodPrd)+OMSGrade:ACOLS[i,1]+_tam //.and. DA1->DA1_PRCVEN <> OMSGrade:ACOLS[i,w]
						
						RecLock("DA1",.F.)
						Replace DA1->DA1_PRCVEN		with nPreco
						MsUnLock()
						lRet	:= .T.
	
					ElseIf !Found() .and. OMSGrade:ACOLS[i,w] <> 0
						
						nItem	:= ItemDA0(aTabVirt[y,2,2])
						RecLock("DA1",.T.)
						Replace DA1->DA1_FILIAL		with xfilial("DA1")
						Replace DA1->DA1_ITEM		with strzero(nItem,6)
						Replace DA1->DA1_CODTAB		with aTabVirt[y,2,2]
						Replace DA1->DA1_CODPRO		with AllTrim(cGetCodPrd)+OMSGrade:ACOLS[i,1]+_tam
						Replace DA1->DA1_PRCVEN		with nPreco
						Replace DA1->DA1_ATIVO		with "1"
						Replace DA1->DA1_TPOPER		with "4"
						Replace DA1->DA1_QTDLOT		with 999999.99
						Replace DA1->DA1_INDLOT		with "000000000999999.99"
						Replace DA1->DA1_MOEDA		with 1
						Replace DA1->DA1_DATVIG		with dDataBase
						MsUnLock()
						lRet	:= .T.
						
					Endif
				Next y
			Next w
		//Endif i
	Next i
	
	For x := 1 to Len(aTabVirt)
		If aTabVirt[x,2,2] <> cGetTabela
			cTabVirt	+= aTabVirt[x,2,2] + "/"
		End
	Next x
	
	If lRet .and. Len(cTabVirt) == 0
		MessageBox("Referencia "+cGetCodPrd+", atualizada na tabela de Pre�os "+cGetTabela,"Atualiza��o Efetuada",MB_ICONEXCLAMATION)
	ElseIf lRet .and. Len(cTabVirt) > 0
		MessageBox("Referencia "+cGetCodPrd+", atualizada na tabela de Pre�os "+cGetTabela+" e Tabelas Virtuais "+Substring(cTabVirt,1,len(cTabVirt)-1),"Atualiza��o Efetuada",MB_ICONEXCLAMATION)
	End
	
	cGetCodPrd	:= Space(8)
	fMSNewGe1()
	oMSGrade:refresh()
	oDlg:refresh()
	
Return
 

Static Function ItemDA0(cTabela)

	_nItem := 0
	
	cQuery  := "SELECT MAX(DA1_ITEM) AS ITEM FROM "+RetSQLName("DA1")+" WITH (NOLOCK) "
	cQuery  += "WHERE D_E_L_E_T_<>'*' AND DA1_FILIAL='"+xFilial("DA1")+"'AND DA1_CODTAB='"+cTabela+"' "

	If Select("TMPDA1") > 0
		TMPDA1->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPDA1",.T.,.T.)

	DbSelectArea("TMPDA1")
	DbGoTop()
	
	_nItem := VAL(TMPDA1->ITEM) + 1

	DbCloseArea()

Return(_nItem)


Static Function TabVirt()

	Local cQuery		:= ""
	Local aRegs			:= {}
	Local aTabVirt		:= {}
	
	aadd(aRegs,{"TABVIR"		, cGetTabela			,Nil})
	aadd(aRegs,{"CODTAB"		, cGetTabela			,Nil})
	aadd(aRegs,{"FATOR"			, 0						,Nil})
	aadd(aTabVirt , aRegs)
	aRegs := {}
	
	cQuery := "SELECT DA0_TABVIR, DA0_CODTAB, DA0_FATOR FROM "+RetSQLName("DA0")+" DA0 WITH (NOLOCK) " 
	cQuery += "WHERE D_E_L_E_T_<>'*' AND DA0_TABVIR='"+cGetTabela+"' ORDER BY 1 "

	If Select("TMPDA0") > 0
		TMPDA0->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMPDA0",.T.,.T.)

	While !EOF()

		aadd(aRegs,{"TABVIR"		, TMPDA0->DA0_TABVIR			,Nil})
		aadd(aRegs,{"CODTAB"		, TMPDA0->DA0_CODTAB			,Nil})
		aadd(aRegs,{"FATOR"			, TMPDA0->DA0_FATOR				,Nil})
		aadd(aTabVirt , aRegs)
		aRegs := {}
		
		DbSkip()
				
	EndDo
	
Return aTabVirt