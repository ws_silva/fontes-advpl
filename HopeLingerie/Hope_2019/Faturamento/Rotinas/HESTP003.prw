#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

user function HESTP003()

Local _afields := {}     
Local _carq    := ""         
Local cQuery   := ""

Private lMarcar   := .F.
Private cAlias    :="SZJ"               
Private cCadastro := "Conferencia" 
Private oMark                                                                                                   
Private cPerg     := "HESTA003"
Private lContinua := .F.
Private cMark     := GetMark()
Private aCampos   := {}
Private aRotina := {{"Apontar","U_HP03PCPAP()",0,2},;
					{"Zerar Conferencia","U_HP03EXC(1)",0,2},;
					{"Estornar Liberacao","U_HP03EST(1)",0,2},;
					{"Conferir Tudo","U_HP03CONF(TRB->ZJ_NUMPF)",0,2},;
	                {"Legenda"   ,"U_HP03LGEST()",0,2}}
                    
                    //{"Apontar"   ,"IF(U_HP03PCPAP(),U_HESTREV02(TRB->ZJ_LOTDAP),)",0,2},;
                     
Private aCores := {{"TRB->ACONFERIR='Nao'","DISABLE"},;
					{"TRB->ACONFERIR='Sim'.And.TRB->EMCONFERE='Nao'.And.TRB->CONFERIDO='Nao'","ENABLE"},;
					{"TRB->ACONFERIR='Sim'.And.TRB->EMCONFERE='Sim'","BR_AMARELO"},;
					{"TRB->ACONFERIR='Sim'.And.TRB->CONFERIDO='Sim'.And.TRB->FATURADO='Nao'","BR_AZUL"},;
					{"TRB->ACONFERIR='Sim'.And.TRB->FATURADO='Sim'","BR_PRETO"}}

Private _cIndex := ""
Private _cChave := ""

AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
	Return
EndIf

lGera := .T.

Processa( {|| lGera := GeraDados() }, "Aguarde...", "Processando...",.F.)

If !lGera
	MsgInfo("Nao existem conferencias em aberto.","Aviso")
	Return
EndIf
					
dbSelectArea("TRB")					
					
/*_cIndex:=Criatrab(Nil,.F.)
_cChave:="ZJ_LOTDAP+ZJ_NUMPF"
Indregua("TRB",_cIndex,_cchave,,,"Selecionando Registros...")
dBSETINDEX(_cIndex+ordbagext())*/
_cIndex:=Criatrab(Nil,.F.)
_cChave:="ZJ_NUMPF"
Indregua("TRB",_cIndex,_cchave,,,"Selecionando Registros...")
dBSETINDEX(_cIndex+ordbagext())

dbSelectArea("TRB")
dbGoTop()

aCampos:={{"Lote DAP"      ,"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2],X3Picture("ZJ_LOTDAP")},;
          {"Pre-Fatura."   ,"ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2],X3Picture("ZJ_NUMPF"  )},;
          {"Cliente"       ,"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2],X3Picture("ZJ_CLIENTE")},;
          {"Loja"          ,"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2],X3Picture("ZJ_LOJA"   )},;
          {"Nome"          ,"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2],X3Picture("A1_NOME"   )},;
          {"Qtd. Pecas"    ,"ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2],X3Picture("ZJ_QTDLIB" )},;
          {"A Conferir"    ,"ACONFERIR" ,"C",3,0,""},;
          {"Em Conferencia","EMCONFERE" ,"C",3,0,""},;
          {"Conferido"     ,"CONFERIDO" ,"C",3,0,""},;
          {"Efetivado"     ,"EFETIVADO" ,"C",3,0,""},;	      
          {"Faturado"      ,"FATURADO"  ,"C",3,0,""},;
          {"Ult. Usuario"  ,"USUARIO"   ,TAMSX3("D3_USUARIO")[3],TAMSX3("D3_USUARIO")[1],TAMSX3("D3_USUARIO")[2],X3Picture("D3_USUARIO")}}

mBrowse(,,,,"TRB",aCampos,,,,,aCores)

TRB->(DbCloseArea())
TMP->(DbCloseArea())

Return

Static Function Geradados()
Local _astru := {}
Local cQuery := ""
Local lRet   := .F.

cQuery := "select ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,ZJ_LOTDAP,ZJ_CONF,ZJ_DOC,ZJ_SERIE,SUM(ZJ_QTDLIB) AS ZJ_QTDLIB, "
cQuery += CRLF + "(select TOP 1 ZR_USR from "+RetSqlName("SZR")+" SZR WITH(NOLOCK) where ZR_NUMPF = ZJ_NUMPF and SZR.D_E_L_E_T_ = '' order by ZR_DATA,ZR_HORA DESC) AS ZR_USR "
cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ WITH(NOLOCK) "
cQuery += CRLF + "inner join "+RetSqlName("SA1")+" SA1 WITH(NOLOCK) " 
cQuery += CRLF + "on A1_COD = ZJ_CLIENTE "
cQuery += CRLF + "and A1_LOJA = ZJ_LOJA "
cQuery += CRLF + "and SA1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where SZJ.D_E_L_E_T_ = '' " 
cQuery += CRLF + "and ZJ_DATA between '"+DTOS(mv_par03)+"' and '"+DTOS(mv_par04)+"' "
If mv_par01 = 2
	cQuery += CRLF + "and ZJ_DOC = '' "
EndIf
If mv_par02 = 2
	cQuery += CRLF + "and ZJ_CONF  = 'L' "
	Else
	cQuery += CRLF + "and ZJ_CONF IN ('S','L') "
EndIf

cQuery += CRLF + "group by ZJ_NUMPF,ZJ_PEDIDO,ZJ_CLIENTE,ZJ_LOJA,A1_NOME,ZJ_LOTDAP,ZJ_CONF,ZJ_DOC,ZJ_SERIE"
cQuery += CRLF + "order by ZJ_LOTDAP,ZJ_NUMPF"

MemoWrite("HESTA003.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMP", .F., .T.)

//Estrutura da tabela temporaria
AADD(_astru,{"ZJ_LOTDAP" ,TAMSX3("ZJ_LOTDAP" )[3],TAMSX3("ZJ_LOTDAP" )[1],TAMSX3("ZJ_LOTDAP" )[2]})
AADD(_astru,{"ZJ_NUMPF"  ,TAMSX3("ZJ_NUMPF"  )[3],TAMSX3("ZJ_NUMPF"  )[1],TAMSX3("ZJ_NUMPF"  )[2]})
AADD(_astru,{"ZJ_CLIENTE",TAMSX3("ZJ_CLIENTE")[3],TAMSX3("ZJ_CLIENTE")[1],TAMSX3("ZJ_CLIENTE")[2]})
AADD(_astru,{"ZJ_LOJA"   ,TAMSX3("ZJ_LOJA"   )[3],TAMSX3("ZJ_LOJA"   )[1],TAMSX3("ZJ_LOJA"   )[2]}) 
AADD(_astru,{"A1_NOME"   ,TAMSX3("A1_NOME"   )[3],TAMSX3("A1_NOME"   )[1],TAMSX3("A1_NOME"   )[2]})
AADD(_astru,{"ZJ_QTDLIB" ,TAMSX3("ZJ_QTDLIB" )[3],TAMSX3("ZJ_QTDLIB" )[1],TAMSX3("ZJ_QTDLIB" )[2]})
AADD(_astru,{"ACONFERIR" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"EMCONFERE" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"CONFERIDO" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"EFETIVADO" ,"C"                    ,3                      ,0                      })
AADD(_astru,{"FATURADO"  ,"C"                    ,3                      ,0                      })
AADD(_astru,{"USUARIO"   ,TAMSX3("D3_USUARIO")[3],TAMSX3("D3_USUARIO")[1],TAMSX3("D3_USUARIO")[2]})

cArqTrab  := CriaTrab(_astru,.T.)
dbUseArea(.T.,,cArqTrab,"TRB")//, .F., .F. )

nReg := 0

Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	While TMP->(!EOF())
		nReg++
		TMP->(DbSkip())
	EndDo
EndIf	

ProcRegua(nReg)

//Atribui a tabela temporaria ao alias TRB
Dbselectarea("TMP")
TMP->(DbGoTop())
If TMP->(!EOF())
	lRet := .T.
	While TMP->(!EOF())
		IncProc()
	    cAConferir := "Sim"
	    cEmConfere := "Nao"
	    cConferido := "Nao"
	    cFaturado  := "Nao"
        cEfetivado := "Nao"	    
        nSldTran   := 0
	    cPedC9     := SubStr(TMP->ZJ_NUMPF,1,6)
	    
	    If TMP->ZJ_CONF = "S"
	    	cConferido := "Sim"
	    Else	
		    DbSelectArea("SZP")
		    DbSetOrder(1)
		    If DbSeek(xFilial("SZP")+TMP->ZJ_NUMPF)
		    	cEmConfere := "Sim"
		    EndIf
	    EndIf
//TODO FLITRO PARA PEDIDOS EFETIVADOS. - RONALDO 04/10/18
	    DbSelectArea("SZP")
		DbSetOrder(1)
		If DbSeek(xFilial("SZP")+TMP->ZJ_NUMPF)
		    If (SZP->ZP_QTDLIB > 0)
		    	cEfetivado := "Sim"
		    EndIf
		EndIf
	    	    		    
	    nSldTran := SLDSZM(TMP->ZJ_LOTDAP)	
		If nSldTran > 0
			cAConferir := "Nao"
		EndIf
		
		lFat := .F.
		
		If !Empty(TMP->ZJ_DOC)
			cFaturado := "Sim"
		EndIf
	    	
		DbSelectArea("TRB")        
		RecLock("TRB",.T.) 
		TRB->ZJ_LOTDAP  := TMP->ZJ_LOTDAP
		TRB->ZJ_NUMPF   := TMP->ZJ_NUMPF
		TRB->ZJ_CLIENTE := TMP->ZJ_CLIENTE
		TRB->ZJ_LOJA    := TMP->ZJ_LOJA
		TRB->A1_NOME    := TMP->A1_NOME
		TRB->ZJ_QTDLIB  := TMP->ZJ_QTDLIB
		TRB->ACONFERIR  := cAConferir
		TRB->EMCONFERE  := cEmConfere
		TRB->CONFERIDO  := cConferido
        TRB->EFETIVADO  := cEfetivado		
        TRB->FATURADO   := cFaturado
		TRB->USUARIO    := UsrRetName(TMP->ZR_USR)
		MsUnlock()

		TMP->(DbSkip())
	EndDo	
EndIf

Return lRet

User Function HP03CONF(cNumPF)
Local aArea := GetArea()
Local aSZJ  := {}
Local nx    := 0
Local cItem := "00"
Local cLtPd := AllTrim(SuperGetMV("MV_XLOTPAD",.F.,"000000"))

DbSelectArea("SZR")
DbSetOrder(1)
If DbSeek(xFilial("SZR")+cNumPF)
	MsgAlert("Ja existe uma conferencia em andamento para este pre-faturamento.","Atencao")
	Return
EndIf

If !MsgYesNo("Confirma conferencia total do pre-faturamento selecionado?","Confirmao")
	Return
EndIf

aSZJ := DADOS2(cNumPF,.T.)

//{TMPSZJ->B1_CODBAR,TMPSZJ->ZJ_PRODUTO+" - "+SubStr(TMPSZJ->B1_DESC,1,40),nSldSep,TMPSZJ->ZJ_QTDLIB,.F.}

DbSelectArea("SZJ")
DbSetOrder(1)
DbSeek(xFilial("SZJ")+cNumPF)

If Len(aSZJ) > 0
	For nx := 1 to Len(aSZJ)
		cItem := SOMA1(cItem,2)
		
		DbSelectArea("SZR") 
		RecLock("SZR",.T.)
		SZR->ZR_FILIAL  := xFilial("SZR")
		SZR->ZR_NUMPF   := cNumPF
		SZR->ZR_NUMPED   := SubStr(cNumPF,1,6)
		SZR->ZR_ITEM    := cItem
		SZR->ZR_CODBAR  := aSZJ[nx][1]
		SZR->ZR_PRODUTO := SubStr(aSZJ[nx][2],1,15)
		SZR->ZR_QUANT   := aSZJ[nx][4]
		SZR->ZR_USR     := __cUserID
		SZR->ZR_NOMEUSR := UsrRetName(ALLTRIM(__cUserID) )
		SZR->ZR_DATA    := DDATABASE
		SZR->ZR_HORA    := Time() 
		MsUnlock()
		
		DbSelectArea("SZP")
		RecLock("SZP",.T.)
		SZP->ZP_FILIAL  := xFilial("SZP")
		SZP->ZP_LOTDAP  := SZJ->ZJ_LOTDAP
		SZP->ZP_NUMPF   := cNumPF
		SZP->ZP_PEDIDO  := SZJ->ZJ_PEDIDO
		SZP->ZP_CODBAR  := aSZJ[nx][1]
		SZP->ZP_PRODUTO := SubStr(aSZJ[nx][2],1,15)
		SZP->ZP_LOTECTL := cLtPd
		SZP->ZP_QUANT   := aSZJ[nx][4]
		SZP->ZP_DATA 	:= DDATABASE
		SZP->ZP_HORA	:= Time()
		MsUnlock()
	Next
	
	DbSelectArea("SZJ")
	DbSetOrder(1)
	If DbSeek(xFilial("SZJ")+cNumPF)
		While SZJ->(!EOF()) .And. SZJ->ZJ_NUMPF = cNumPF
			nSZJLib := SZJ->ZJ_QTDLIB 
			RecLock("SZJ",.F.)
			SZJ->ZJ_QTDSEP := nSZJLib
			MsUnlock()
			SZJ->(DbSkip())
		EndDo
	EndIf
	
	RecLock("TRB",.F.)
	TRB->EMCONFERE := "Sim"
	MsUnlock()
EndIf

RestArea(aArea)

Return

Static Function SLDSZM(cLotDAP)
Local aArea   := GetArea()
Local cQuery  := ""
Local nRet    := 0

cQuery := "select ZM_LOTDAP,SUM(ZM_QUANT) AS ZM_QUANT,SUM(ZM_QTDTRAN) AS ZM_QTDTRAN"
cQuery += CRLF + "from "+RetSqlName("SZM")+" SZM WITH(NOLOCK) "
cQuery += CRLF + "where SZM.D_E_L_E_T_ = '' "
cQuery += CRLF + "and ZM_LOTDAP = '"+cLotDAP+"' "
cQuery += CRLF + "group by ZM_LOTDAP "

MemoWrite("HPCPA003_SLDSZM.txt",cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSZM", .F., .T.)

DbSelectArea("TMPSZM")

If TMPSZM->(!EOF())
	nRet := TMPSZM->ZM_QUANT - TMPSZM->ZM_QTDTRAN
EndIf

TMPSZM->(DbCloseArea())

RestArea(aArea)

Return nRet

User Function HP03EXC(_tp,_perro)
Local aArea    := GetArea()
Local lZera    := .F.
Local lEstorna := .F.
Local cPedido  := SubStr(TRB->ZJ_NUMPF,1,6)
Local cAmzPik  := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local nQtdXRes := 0

DbSelectArea("SZJ")
DbSetOrder(1)
If DbSeek(xFilial("SZJ")+TRB->ZJ_NUMPF)
	If !Empty(SZJ->ZJ_DOC)
		RecLock("TRB",.F.)
		TRB->FATURADO = "Sim"
		MsUnlock()
	EndIf
EndIf

If _tp = 1
	If TRB->FATURADO = "Sim"
		MsgAlert("Pedido ja faturado. Nao e possivel excluir a conferencia.","Atencao")
	ElseIf TRB->CONFERIDO = "Sim"
		If MsgYesNo("Pedido ja liberado para faturamento. Confirma estorno e exclusao da conferencia?","Confirmacao")
			lEstorna := .T.
		EndIf
	ElseIf TRB->EMCONFERE = "Nao"
		MsgAlert("Conferencia nao iniciada.","Atencao")
	Else
		If MsgYesNo("Confirma exclusao da conferencia em andamento?","Confirmacao")
			lZera := .T.
		EndIf
	EndIf
Else
//	If MsgYesNo("Pedido ja liberado para faturamento. Confirma estorno?","Confirmacao")
		lEstorna 	:= .T.
		lZera		:= .T.
//	EndIf
Endif

If lEstorna
	DbSelectArea("SC9")
	DbSetOrder(1)
	If DbSeek(xFilial("SC9")+cPedido)
		While SC9->(!EOF()) .And. SC9->C9_PEDIDO = cPedido
			If Empty(SC9->C9_NFISCAL)
				SC9->(a460Estorna())
				lZera := .T.			
			EndIf
			SC9->(DbSkip())
		EndDo
	EndIf	
EndIf

If lZera 
	DbSelectArea("SZR")
	DbSetOrder(1)
	If DbSeek(xFilial("SZR")+TRB->ZJ_NUMPF) .and. _tp = 1
		While SZR->(!EOF()) .and. SZR->ZR_NUMPF = TRB->ZJ_NUMPF
			RecLock("SZR",.F.)
			SZR->(DBDELETE())
			MsUnlock()
			SZR->(DbSkip())
		EndDo
	EndIf
	DbSelectArea("SZP")
	DbSetOrder(1)
	If DbSeek(xFilial("SZP")+TRB->ZJ_NUMPF)
		While SZP->(!EOF()) .and. SZP->ZP_NUMPF = TRB->ZJ_NUMPF
			If _tp = 1
				RecLock("SZP",.F.)
				SZP->(DBDELETE())
				MsUnlock()
			Else
				RecLock("SZP",.F.)
				Replace ZP_QTDLIB	with 0
				MsUnlock()
			Endif
			SZP->(DbSkip())
		EndDo
	EndIf
	DbSelectArea("SZJ")
	DbSetOrder(1)
	If DbSeek(xFilial("SZJ")+TRB->ZJ_NUMPF)
		While SZJ->(!EOF()) .and. SZJ->ZJ_NUMPF = TRB->ZJ_NUMPF
			nQtdXRes := 0
			
			RecLock("SZJ",.F.)
			IF _tp = 1
				SZJ->ZJ_QTDSEP := 0
			Endif
			SZJ->ZJ_CONF   := "L"
			MsUnlock()
			
			If _tp = 2
				If len(_perro) > 0
					nPos := Ascan(_perro,{|x| x = SZJ->ZJ_PRODUTO})
					
					If nPos = 0			
						DbSelectArea("SB2")
						DbSetOrder(2)
						If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)
							nQtdXRes := SB2->B2_XRESERV + SZJ->ZJ_QTDLIB
							
							RecLock("SB2",.F.)
							SB2->B2_XRESERV := nQtdXRes						
							MsUnlock()
							
						EndIf
					Endif
				Else
					DbSelectArea("SB2")
					DbSetOrder(2)
					If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)
						nQtdXRes := SB2->B2_XRESERV + SZJ->ZJ_QTDLIB
														
						RecLock("SB2",.F.)
						SB2->B2_XRESERV := nQtdXRes
						MsUnlock()
					EndIf
				Endif
			Else
				DbSelectArea("SB2")
				DbSetOrder(2)
				If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)
					nQtdXRes := SB2->B2_XRESERV + SZJ->ZJ_QTDLIB
						
					RecLock("SB2",.F.)
					SB2->B2_XRESERV := nQtdXRes
					MsUnlock()
				EndIf
			Endif

			SZJ->(DbSkip())
		EndDo
	EndIf
	
	DbSelectArea("TRB")
	RecLock("TRB",.F.)
	TRB->CONFERIDO := "Nao"
	TRB->EMCONFERE := "Nao"
    TRB->EFETIVADO := "Nao"	
    MsUnlock()
	If _tp = 1
		MsgInfo("Conferencia zerada com sucesso!","Aviso")
	Else
		MsgInfo("Pedido nao efetivado!","Aviso")
	Endif
EndIf 

RestArea(aArea)

Return


User Function HP03EST(_tp,_perro)
Local aArea    := GetArea()
Local lZera    := .F.
Local lEstorna := .F.
Local cPedido  := SubStr(TRB->ZJ_NUMPF,1,6)
Local cNumPF   := TRB->ZJ_NUMPF
Local cAmzPik  := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local nQtdXRes := 0

If TRB->CONFERIDO = "Sim"
	If MsgYesNo("Pedido ja liberado para faturamento. Confirma estorno da conferencia?","Confirmacao")
		lEstorna := .T.
	EndIf
Else
	MsgAlert("Pedido nao liberado ainda.","Atencao")
	lEstorna 	:= .F.
Endif

If lEstorna
	DbSelectArea("SC9")
	DbSetOrder(1)
	If DbSeek(xFilial("SC9")+cPedido)
		While SC9->(!EOF()) .And. SC9->C9_PEDIDO = cPedido .and. SC9->C9_XNUMPF = cNumPF
			If Empty(SC9->C9_NFISCAL)
				SC9->(a460Estorna())
				lZera := .T.			
			EndIf
			SC9->(DbSkip())
		EndDo
	EndIf	
EndIf

If lZera 
	DbSelectArea("SZP")
	DbSetOrder(1)
	If DbSeek(xFilial("SZP")+TRB->ZJ_NUMPF)
		While SZP->(!EOF()) .and. SZP->ZP_NUMPF = TRB->ZJ_NUMPF
			RecLock("SZP",.F.)
			Replace ZP_QTDLIB	with 0
			MsUnlock()
			SZP->(DbSkip())
		EndDo
	EndIf
	DbSelectArea("SZJ")
	DbSetOrder(1)
	If DbSeek(xFilial("SZJ")+TRB->ZJ_NUMPF)
		While SZJ->(!EOF()) .and. SZJ->ZJ_NUMPF = TRB->ZJ_NUMPF
			
			RecLock("SZJ",.F.)
			SZJ->ZJ_CONF   := "L"
			MsUnlock()
			
			DbSelectArea("SB2")
			DbSetOrder(2)
			If DbSeek(xFilial("SB2")+cAmzPik+SZJ->ZJ_PRODUTO)
				nQtdXRes := SB2->B2_XRESERV + SZJ->ZJ_QTDLIB
				
				RecLock("SB2",.F.)
				SB2->B2_XRESERV := nQtdXRes
				MsUnlock()
			EndIf

			SZJ->(DbSkip())
		EndDo
	EndIf
	
	DbSelectArea("TRB")
	RecLock("TRB",.F.)
	TRB->ACONFERIR := 'Sim'
	TRB->EMCONFERE := 'Sim'
	TRB->EFETIVADO := "Nao"
	MsUnlock()
	MsgInfo("Liberaco estornada com sucesso!","Aviso")
EndIf 

RestArea(aArea)

Return

User Function HP03PCPAP()
Local cQuery := ""
Local oBrw
Local _afields := {}     
Local _carq    := ""
local nX
Local nI
Local cCombo
Local aArea := GetArea()
Local lRet  := .F.
Private oDlg
Private oGetDados   
Private nOpc      := 0
Private nStr      := 0 
Private nPos      := 0 
Private aColAux1  := {}   
Private aColAux2  := {}
Private lRefresh  := .T.
Private aHeader1  := {}
Private aCols1    := {}
Private aHeader2  := {}
Private aCols2    := {}
Private aFields1  := {} 
Private aFields2  := {}    
Private cArqTMP   
Private nCol 	  := 0 
Private cLegenda  := ""
Private nTamanho  := 0         
Private aRotina   := {}
Private cArqTrab
Private cApont    := SPACE(23) //13 Digitos para EAN + 10 digitos para Lote
Private oFont20n  := TFont():New("Arial",,20,,.t.,,,,,.f.)
Private oFont18n  := TFont():New("Arial",,18,,.t.,,,,,.f.)
Private lConfirm  := .F.
Private cVolume   := SPACE(5)
Private cTpVol    := SPACE(3)
Private cDescVol  := SPACE(30)
Private lConf     := .T.    
Private lImpAut   := .F. //Marcio
Private lTOk      := .F.
Private nTotLib   := 0
Private nTotSep   := 0
Private lHabilita := .T.

Private oGetDados1
Private oGetDados2

If TRB->ACONFERIR = "Nao"
	MsgAlert("Lote DAP possui transferencias pendentes e nao podera ser conferido.","Atencao")
	Return
EndIf

// TODO LIBERAR FLAG PARA EFETIVAR PEDIDO. -- RONALDO PEREIRA 28/09/2018
If TRB->CONFERIDO = "Sim" .And. TRB->EFETIVADO = "Sim" 
 		lHabilita := .F.
 	ElseIf TRB->CONFERIDO = "Sim" .And. TRB->EFETIVADO = "Nao"	
 		lHabilita := .T.
EndIf

//TODO FLAG IMPRESSAO DE PRE�O AUTOMATICO -- RONALDO PEREIRA 13/11/2018
	DbSelectArea("SC5")
	SC5->(DbSetOrder(1)) 
	SC5->(DbSeek(xFilial("SC5")+left((TRB->ZJ_NUMPF),6))) 
	If SC5->C5_TPPED = '016'
		lImpAut := .F.
		ELSE
		lImpAut := .T.
	EndIf   



//BEGIN TRANSACTION

//-------------------------------------------------------
//Estrutura do primeiro aCols
//-------------------------------------------------------

aFields1 := {"ZR_ITEM","B1_CODBAR","ZR_PRODUTO","ZR_QUANT","ZR_VOLUME","ZR_TPVOL","DESCVOL"}

aAdd(aColAux1,{"","","","",0,SPACE(3),SPACE(3),"",.F.})

If lHabilita            
	aAlterFd1 := {"ZR_QUANT","ZR_VOLUME","ZR_TPVOL"}
Else
	aAlterFd1 := {}
EndIf

DbSelectArea("SX3")
SX3->(DbSetOrder(2))
For nX := 1 to Len(aFields1)
	If SX3->(DbSeek(aFields1[nX]))
		nTamanho := SX3->X3_TAMANHO
		cValid   := ""
		Do Case
			Case aFields1[nX] = "B1_CODBAR"
				cTit := "Codigo"	
			Case aFields1[nX] = "ZR_PRODUTO"
				cTit := "Produto"
				nTamanho := 80
			Case aFields1[nX] = "ZR_QUANT"
				cTit   := "Saldo"
				//cValid := "U_HP3VDQTDE(N1)"
			Case aFields1[nX] = "ZR_TPVOL"
				cTit := "Tp Vol"
				cValid := "U_HP3PESQV(M->ZR_TPVOL,.T.,oGetDados1:nAt)"
			Otherwise
	    		cTit := Trim(X3Titulo())
		EndCase
		Aadd(aHeader1,{cTit,SX3->X3_CAMPO,SX3->X3_PICTURE,nTamanho,SX3->X3_DECIMAL,cValid,"",SX3->X3_TIPO,"",""})
	Else
		Do Case
			Case aFields1[nX] = "DESCVOL"
				Aadd(aHeader1,{"Descri"+chr(65533)+chr(65533)+"o","ZJ_NUMPF","@!",40,0,"","","C","",""})	
		EndCase
	EndIf
Next nX

Aadd(aCols1,Array(Len(aHeader1)+1))

For nI := 1 To Len(aHeader1)
	If aHeader1[nI][2] = "B1_CODBAR"
		aCols1[1][nI] := "000001"
	EndIf
    aCols1[1][nI] := CriaVar(aHeader1[nI][2])
Next nI

aCols1[1][Len(aHeader1)+1] := .F.

aColAux1 := DADOS1(TRB->ZJ_NUMPF)

//-------------------------------------------------------
//Estrutura do segundo aCols
//-------------------------------------------------------

aFields2 := {"B1_CODBAR","ZJ_PRODUTO","ZJ_QTDLIB","ZJ_QTDSEP"}

aAdd(aColAux2,{"","","",0,0,.F.})
            
aAlterFd2 := {}

DbSelectArea("SX3")
SX3->(DbSetOrder(2))
For nX := 1 to Len(aFields2)
	If SX3->(DbSeek(aFields2[nX]))
		nTamanho := SX3->X3_TAMANHO
		Do Case
			Case aFields2[nX] = "B1_CODBAR"
				cTit := "Codigo"
			Case aFields2[nX] = "ZJ_PRODUTO"
				cTit := "Produto"
				nTamanho := 80
			Case aFields2[nX] = "ZJ_QTDLIB"
				cTit := "Saldo"
			Case aFields2[nX] = "ZJ_QTDSEP"
				cTit := "Qtde. Original"	
			Otherwise
	    		cTit := Trim(X3Titulo())
		EndCase
		Aadd(aHeader2,{cTit,SX3->X3_CAMPO,SX3->X3_PICTURE,nTamanho,SX3->X3_DECIMAL,"","",SX3->X3_TIPO,"",""})
	EndIf
Next nX	

Aadd(aCols2,Array(Len(aHeader2)+1))

For nI := 1 To Len(aHeader2)
	If aHeader2[nI][2] = "B1_CODBAR"
		aCols2[1][nI] := "000001"
	EndIf
    aCols2[1][nI] := CriaVar(aHeader2[nI][2])
Next nI

aCols2[1][Len(aHeader2)+1] := .F.

aColAux2 := DADOS2(TRB->ZJ_NUMPF,.F.)




//-------------------------------------------------------------------------

//Apontamento: 13 Digitos para EAN (B1_CODBAR) + 6 digitos para Lote

DEFINE MSDIALOG oDlg TITLE "Conferencia" FROM 00,00 TO 630,1200 PIXEL  
          
@ 005, 005 Say "Pre-Faturamento: "+TRB->ZJ_NUMPF Size 200,010 COLORS 16711680,16777215 FONT oFont20n PIXEL OF oDlg
//@ 005, 250 Say "Cliente: "+TRB->ZJ_CLIENTE+"/"+TRB->ZJ_LOJA+" - "+TRB->A1_NOME Size 400,010 COLORS 16711680,16777215 FONT oFont20n PIXEL OF oDlg
@ 005, 150 Say "Cliente: "+TRB->ZJ_CLIENTE+"/"+TRB->ZJ_LOJA+" - "+TRB->A1_NOME Size 400,010 COLORS 16711680,16777215 FONT oFont20n PIXEL OF oDlg

@ 020, 005 Say "Apontamento: "               Size 150,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDlg
@ 018, 060 MSGET  oApont    Var     cApont   SIZE 080,010 VALID IF(VALAPONT(cApont,TRB->ZJ_NUMPF,TRB->ZJ_LOTDAP,.F.,.T.),.T.,.T.) WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDlg
@ 017, 145 BUTTON oButton1  PROMPT "Incluir" SIZE 040,015 OF oDlg ACTION IF(VALAPONT(cApont,TRB->ZJ_NUMPF,TRB->ZJ_LOTDAP,.F.,.T.),.T.,.T.) WHEN lHabilita PIXEL MESSAGE "Incluir"

//TODO IMPRESSAO AUTOMATICA TOTALIT
@ 017, 195 BUTTON oButton4  PROMPT "Imprimir Etq." SIZE 040,015 OF oDlg ACTION HPPRTETQ(SubStr(AllTrim(oGetDados1:aCols[oGetDados1:oBrowse:nAt, 2]),1,13)+SPACE(2),TRB->ZJ_NUMPF) WHEN Len(oGetDados1:aCols)>0 PIXEL MESSAGE "Imprime Etiqueta" //Marcio
@ 018, 250 CHECKBOX oImpAut VAR lImpAut  PROMPT "Impressao automatica" SIZE 075,015 COLORS 0, 16777215 PIXEL OF oDlg  //

@ 020, 360 Say "Volume: "                    Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDlg
@ 018, 390 MSGET  oVolume   Var     cVolume  SIZE 020,010 COLOR CLR_BLACK Picture "@!" WHEN lHabilita PIXEL OF oDlg
@ 020, 420 Say "Tipo Volume: "               Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDlg
@ 018, 470 MSGET  oTpVol    Var     cTpVol   SIZE 020,010 VALID U_HP3PESQV(cTpVol,.F.) WHEN lHabilita COLOR CLR_BLACK Picture "@!" F3 "SZQ" PIXEL OF oDlg
@ 018, 500 MSGET  oDescVol  Var     cDescVol SIZE 100,010 COLOR CLR_BLACK Picture "@!" PIXEL OF oDlg WHEN .F.
@ 210, 518 BUTTON oButton1  PROMPT "Salvar"  SIZE 050,015 OF oDlg ACTION (Iif(SALVAAPTO(TRB->ZJ_NUMPF),(lConfirm:=.T.,oDlg:End()),)) WHEN lHabilita PIXEL MESSAGE "Salvar"
@ 240, 518 BUTTON oButton2  PROMPT "Apontamentos"  SIZE 050, 015 OF oDlg ACTION VISUALPTO(TRB->ZJ_NUMPF) PIXEL MESSAGE "Apontamentos"
@ 260, 518 BUTTON oButton4  PROMPT "Volumes"  SIZE 050,015 OF oDlg ACTION TOTALVOL(TRB->ZJ_NUMPF) PIXEL MESSAGE "Volumes"
@ 290, 518 BUTTON oButton3  PROMPT "Sair"    SIZE 050,015 OF oDlg ACTION (Iif(HESTSAI03(),(oDlg:End()),)) PIXEL MESSAGE "Sair"
@ 180, 490 Say "Qtde conferida: "+AllTrim(Str(nTotSep))+"/"+AllTrim(Str(nTotLib)) Size 150,010 COLOR CLR_BLACK FONT oFont20n PIXEL OF oDlg
@ 200, 520 CHECKBOX oConf VAR lConf  PROMPT "Conferindo" SIZE 055,015 COLORS 0, 16777215 PIXEL OF oDlg WHEN lTOk

//oGetDados1 := MsNewGetDados():New(025,000,175,503,GD_INSERT+GD_DELETE+GD_UPDATE,"AllwaysTrue","AllwaysTrue","",aAlterFd1,,999,"AllwaysTrue","","AllwaysTrue",oDlg,aHeader1,aColAux1)
oGetDados1 := MsNewGetDados():New(035,000,175,603,GD_UPDATE,"AllwaysTrue","AllwaysTrue","",aAlterFd1,,999,"AllwaysTrue","","AllwaysTrue",oDlg,aHeader1,aColAux1)
oGetDados1:aCols=aColAux1
N1 := oGetDados1:nAt

oGetDados2 := MsNewGetDados():New(180,000,310,485,,"AllwaysTrue","AllwaysTrue","",aAlterFd2,,999,"AllwaysTrue","","AllwaysTrue",oDlg,aHeader2,aColAux2)
oGetDados2:aCols=aColAux2
N2 := oGetDados2:nAt

//oGetDados:oBrowse:bHeaderClick := {|oGetDados,nCol| Ordena(nCol)}

ACTIVATE MSDIALOG oDlg CENTERED

//TMP->(DbCloseArea())
//TRB->(DbCloseArea())

RestArea(aArea)

//If !lConfirm
//	DisarmTransaction()
//EndIf

//END TRANSACTION	

Return lRet


Static Function HPPRTETQ(cCodBar,cPreFat) //Marcio

Local cDesc1        := "Este programa tem como objetivo imprimir relatorio "
Local cDesc2        := "de acordo com os parametros informados pelo usuario."
Local cDesc3        := "Conferencia"
Local cPict         := ""
Local titulo       	:= "Conferencia"
Local nLin         	:= 80
Local Cabec1       	:= "Conferencia"
Local Cabec2       	:= "Conferencia"
Local imprime      	:= .T.
Local aOrd 			:= {}

Local _oFile 	:= Nil
Local _cSaida	:= CriaTrab("",.F.)
Local _cPorta	:= "LPT1"  

Private lEnd        := .F.
Private lAbortPrint := .F.
Private CbTxt       := ""
Private limite      := 220
Private tamanho     := "G"
Private nomeprog    := "HESTA003" // Coloque aqui o nome do programa para impressao no cabecalho
Private nTipo       := 18
Private aReturn		:={"",1,"",1,3,"LPT2","",1}//aReturn     := { "Zebrado", 1, "Administracao", 2, 2, 1, "", 1}
Private nLastKey    := 0
Private cbtxt      	:= Space(10)
Private cbcont     	:= 00
Private CONTFL     	:= 01
Private m_pag      	:= 01
Private wnrel      	:= "AMWLEETQ" // Coloque aqui o nome do arquivo usado para impressao em disco
Private cString		:= "SZR" 
Private cTam 		:= "" 
/*
wnrel := SetPrint(cString,NomeProg,"",@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.T.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)


If AllTrim(Empty(cCodBar))   
	Return
Endif
*/
// TODO Validar se o produto pertence ao prefaturamento. Ronaldo pereira 23/01/19
DbSelectArea("SB1")
DbSetOrder(5) 
DbSeek(xFilial("SB1")+cCodBar)
DbSelectArea("SZJ")
DbSetOrder(3)
DbSeek(xFilial("SZJ")+cPreFat+SB1->B1_COD)
If SZJ->ZJ_PRODUTO == SB1->B1_COD

	If !Empty(AllTrim(cCodBar))
		DbSelectArea("SB1")
		SB1->(DbSetOrder(5)) //B1_FILIAL+B1_CODBAR
		If SB1->(DbSeek(xFilial("SB1")+cCodBar))
	
			//TODO Ajuste do tamanho do produto sem os Zeros iniciais. Ronaldo Pereira 21/01/2019
			Do case
				Case SUBSTR(SB1->B1_COD,12,3) = "000"
					cTam := AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"000",""))
				Case SUBSTR(SB1->B1_COD,12,2) = "00"
					cTam := AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"00",""))
				Case SUBSTR(SB1->B1_COD,12,1) = "0"
					cTam := AllTrim(SUBSTR(SB1->B1_COD,13,3))
				Otherwise
					cTam := AllTrim(SUBSTR(SB1->B1_COD,12,4))
			EndCase
		     
			DbSelectArea("DA1")
			DA1->(DbSetOrder(1))
			If DA1->( DbSeek( xFilial("DA1") + "557" + SB1->B1_COD ))	
	   		
				cPicture := "@E 999,999.99"
			
	   			cStrEtq := "^XA^MCY^XZ^XA^FWN^CFD,24^CI0^PR8^MNY^MTT^MMT^PW328^LL0200^LS0^MD10^PON^PMN^LRN^XZ^XA^MCY^XZ^XA^LRN^LH10,100^FT7,28^A0N,20,19^FH^FD"
				cStrEtq += "REF:" + SUBSTR(SB1->B1_COD,1,8)
				cStrEtq += "^FS^FT137,28^A0N,20,19^FH^FD"
				cStrEtq += "COR:"  + SUBSTR(SB1->B1_COD,9,3)
				cStrEtq += "^FS^FT7,54^A0N,20,19^FH^FD"
				//cStrEtq += "TAM:" + AllTrim(STRTRAN(SUBSTR(SB1->B1_COD,12,4),"0",""))
				cStrEtq += "TAM:" + cTam
				cStrEtq += "^FS^FT7,80^A0N,20,19^FH^FD"
				cStrEtq += "R$ "  + Transform(DA1->DA1_PRCVEN,cPicture)//AllTrim(cValToChar(DA1->DA1_PRCVEN))//STR(DA1->DA1_PRCVEN,TamSX3("DA1_PRCVEN")[1],TamSX3("DA1_PRCVEN")[2])
				cStrEtq += "^FS^PQ1,0000,1,Y^FS^XZ"
	
	//        Alert(SB1->B1_COD)
	//		@ 00,000 PSAY CHR(2)+cStrEtq
			
	//		MS_FLUSH()   
	
				_oFile := FCreate(_cSaida,0)
				FWrite(_oFile,cStrEtq,Len(cStrEtq))				
				FClose(_oFile)
			
				Copy File &_cSaida to &_cPorta
			Else
				Alert( "Preco nao cadastrado na tabela de precos (557)" )
			Endif
		Else
			Alert( "Produto nao localizado. Codigo de barras: " + cCodBar )
		Endif
	Endif
EndIf

Return 

Static Function VISUALPTO(cNumPF)
Local oDialog        
Local aColsApt   := {}
local aAltFld    := {}  
local aHeaderApt := {} 
Local aFldApt    := {"ZP_CODBAR","ZP_PRODUTO","B1_DESC","ZP_LOTECTL","ZP_QUANT"}
local cCampo     := "" 
Local aAuxApt    := {}
Local nx,ni      := 0  
Local nTamanho   := 0
Private cExclui  := SPACE(23)
Private cVol     := SPACE(05)
Private oGetDados3               
                     
DbSelectArea("SX3")
SX3->(DbSetOrder(2))
For nX := 1 to Len(aFldApt)
	If SX3->(DbSeek(aFldApt[nX]))
		nTamanho := SX3->X3_TAMANHO
		Do Case    
			Case aFldApt[nX] = "ZP_PRODUTO"
				cCampo := "Produto"
			Case aFldApt[nX] = "B1_DESC"
				cCampo := "Descricao"
				nTamanho := 100	
			Otherwise
	    		cCampo := Trim(X3Titulo())       
		EndCase
		Aadd(aHeaderApt,{cCampo,;
		              SX3->X3_CAMPO,;                      
		              SX3->X3_PICTURE,;                      
		              nTamanho,;                      
		              SX3->X3_DECIMAL,;                      
		              SX3->X3_VALID,;                      
		              "",;                      
		              SX3->X3_TIPO,;                      
		              "",;                      
		              "" })
	EndIf
Next nX	

Aadd(aColsApt,Array(Len(aHeaderApt)+1))

For nI := 1 To Len(aHeaderApt)
    aColsApt[1][nI] := CriaVar(aHeaderApt[nI][2])
Next nI

aColsApt[1][Len(aHeaderApt)+1] := .F.             

aAuxApt := ARRAYAPT(cNumPF)

DEFINE MSDIALOG oDialog TITLE "Apontamentos Realizados" FROM 00,00 TO 630,1200 PIXEL

//@ 295, 010 MSGET  oExclui   Var     cExclui  SIZE 080,010 VALID EXCLUIAPT(cExclui,cNumPF) WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDialog
@ 295, 010 Say "V.Barras:"                   Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDialog
@ 295, 080 MSGET  oExclui   Var     cExclui  SIZE 080,010 WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDialog
@ 295, 180 Say "Volume: "                    Size 100,010 COLOR CLR_BLACK FONT oFont18n PIXEL OF oDialog
@ 295, 250 MSGET  oVol      Var     cVol     SIZE 080,010 WHEN lHabilita COLOR CLR_BLACK Picture "@!" PIXEL OF oDialog
@ 295, 450 BUTTON oButton1  PROMPT "Excluir Pe"+chr(65533)+"a" SIZE 050,015 OF oDialog ACTION EXCLUIAPT(cExclui,cNumPF,cVol) WHEN lHabilita PIXEL MESSAGE "Excluir Pe"+chr(65533)+"a"
@ 295, 555 BUTTON oButton2  PROMPT "Sair" SIZE 040, 015 OF oDialog ACTION (oDialog:End(),oGetDados2:Refresh()) PIXEL MESSAGE "Sair"    

oGetDados3 := MsNewGetDados():New(000,000,290,603,GD_UPDATE,"AllwaysTrue","AllwaysTrue","",aAltFld,, 999, "AllwaysTrue", "", "AllwaysTrue",oDialog, aHeaderApt, aColsApt)

oGetDados3:aCols=aAuxApt

ACTIVATE MSDIALOG oDialog CENTERED 

Return 

//_____________________________________________________________________________
/*/{Protheus.doc} Resumo por Volumes
Rotina responsavel por mostrar a qtde de pecas por volume;

@author Ronaldo Pereira
@since 30 de Dezembro de 2018
@version V.01
/*/
//_____________________________________________________________________________

Static Function TOTALVOL(cNumPF)
Local oButton1
Local oSay1
Static oDlgVol
Local nX, nI 	 := 0 
Local aHeaderVol := {}
Local aColsVol 	 := {}
Local aFieldFill := {}
Local aFldVol 	 := {"ZR_VOLUME","ZR_QUANT","ZR_TPVOL"}
Local aAlterFld  := {}
Local aAuxVol    := {}
Private oFont12n := TFont():New("Arial",,12,,.t.,,,,,.f.)
Static oGeraVol

  // Define field properties
  DbSelectArea("SX3")
  SX3->(DbSetOrder(2))
  For nX := 1 to Len(aFldVol)
    If SX3->(DbSeek(aFldVol[nX]))
    	nTamanho := SX3->X3_TAMANHO
    	Do Case    
			Case aFldVol[nX] = "ZR_VOLUME"
				cCampo := "Volume"
				nTamanho := 10
			Case aFldVol[nX] = "ZR_QUANT"
				cCampo := "Quantidade"
				nTamanho := 10
			Case aFldVol[nX] = "ZR_TPVOL"
				cCampo := "Tipo Caixa"
			Otherwise
	    		cCampo := Trim(X3Titulo())       
		EndCase
        Aadd(aHeaderVol,{cCampo,;
		              SX3->X3_CAMPO,;                      
		              SX3->X3_PICTURE,;                      
		              nTamanho,;                      
		              SX3->X3_DECIMAL,;                      
		              SX3->X3_VALID,;                      
		              "",;                      
		              SX3->X3_TIPO,;                      
		              "",;                      
		              "" })                            
    Endif
  Next nX
  
  
  Aadd(aColsVol,Array(Len(aHeaderVol)+1))

  For nI := 1 To Len(aHeaderVol)
    	aColsVol[1][nI] := CriaVar(aHeaderVol[nI][2])
  Next nI

  aColsVol[1][Len(aHeaderVol)+1] := .F.
  
  aAuxVol := ARRAYVOL(cNumPF) 
  
    DEFINE MSDIALOG oDlgVol TITLE "VOLUMES" FROM 000, 000  TO 500, 400 COLORS 0, 16777215 PIXEL

    @ 233, 152 BUTTON oButton1 PROMPT "Fechar" SIZE 037, 012 OF oDlgVol ACTION (oDlgVol:End(),oGetDados2:Refresh()) PIXEL MESSAGE "Fechar"
    @ 007, 006 SAY oSay1 PROMPT "RESUMO DE VOLUMES:" SIZE 065, 007 COLOR CLR_BLACK FONT oFont12n PIXEL OF oDlgVol
    

    oGeraVol := MsNewGetDados():New(  017, 004, 225, 192, GD_UPDATE, "AllwaysTrue", "AllwaysTrue", "", aAlterFld,, 999, "AllwaysTrue", "", "AllwaysTrue", oDlgVol, aHeaderVol, aColsVol)

    oGeraVol:aCols=aAuxVol
 
   ACTIVATE MSDIALOG oDlgVol CENTERED

Return


Static Function ARRAYVOL(cNumPF) 
Local aArrayVol   := {}

	cQry := " SELECT ZR_NUMPF, ZR_VOLUME, SUM(ZR_QUANT) AS QUANT, ZR_TPVOL " 
	cQry += " FROM "+RetSqlName("SZR")+" WITH (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZR_NUMPF = '"+cNumPF+"' "
	cQry += " GROUP BY ZR_NUMPF, ZR_VOLUME, ZR_TPVOL  "
	
	If Select("TMPVOL") > 0
		TMPVOL->(DbCloseArea())
	EndIf

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQry),"TMPVOL",.T.,.T.)
	DbSelectArea("TMPVOL")
	DbGoTop()
	
	While TMPVOL->(!EOF()) .And. TMPVOL->ZR_NUMPF = cNumPF 
 		aAdd(aArrayVol,{TMPVOL->ZR_VOLUME,TMPVOL->QUANT,TMPVOL->ZR_TPVOL,.F.})
 		TMPVOL->(DbSkip())
 	EndDo
                               

Return aArrayVol

Static Function EXCLUIAPT(cExc,cNumPF,cVolume)
Local aAux1 := aClone(oGetDados1:aCols)
Local aAux2 := aClone(oGetDados2:aCols)
Local aAux3 := aClone(oGetDados3:aCols)
Local nx    := 0
Local nQtde := 0
Local nPos1 := 0
Local nPos2 := 0
Local nPos3 := 0
Local nLen  := 0
Local cCodB := SubStr(cExc,1,13)+SPACE(2)
Local cLote := SubStr(cExc,14,10)
Local cLtPd := AllTrim(SuperGetMV("MV_XLOTPAD",.F.,"000000"))
Local cPrd  := ""
Local nQtdSep := 0

If Empty(cLote)
	cLote := cLtPd
EndIf

If !Empty(cExc)
	nLen := Len(aAux3)
	If nLen > 0
		nPos3 := Ascan(aAux3,{|x| x[1]+x[4] = cCodB+cLote})
		If nPos3 > 0
			cPrd := aAux3[nPos3][2] 
		
			nQtde := aAux3[nPos3][5] - 1
			aAux3[nPos3][5] := nQtde
			
			If nQtde = 0
				aDel(aAux3,nPos3)
				aSize(aAux3,Len(aAux3)-1)
				nLen := nLen - 1
			EndIf
			
			oGetDados3:SetArray(aAux3,.T.) 
			oGetDados3:Refresh()
			
			DbSelectArea("SZP")
			DbSetOrder(1)
			If DbSeek(xFilial("SZP")+cNumPF+cCodB+cLote)
				Reclock("SZP",.F.)
				If nQtde <= 0
					SZP->(DBDELETE())
				Else
					SZP->ZP_QUANT := nQtde
				EndIf
				MsUnlock()
			EndIf
			
			DbSelectArea("SZJ")
			DbSetOrder(3)
			If DbSeek(xFilial("SZJ")+cNumPF+cPrd)
				If SZJ->ZJ_QTDSEP > 0
					nQtdSep := SZJ->ZJ_QTDSEP - 1 
					RecLock("SZJ",.F.)
					SZJ->ZJ_QTDSEP := nQtdSep
					MsUnlock()
				EndIf	
			EndIf

			nPos2 := Ascan(aAux2,{|z| AllTrim(z[1]) = AllTrim(cCodB)})
			If nPos2 > 0
				nQtde := aAux2[nPos2][3] + 1
				aAux2[nPos2][3] := nQtde
			EndIf
			oGetDados2:SetArray(aAux2,.T.) 
			oGetDados2:Refresh()
			
			nPos1 := Ascan(aAux1,{|y| y[2]+y[5] = cCodB+cVolume})
			If nPos1 > 0
				nQtde := aAux1[nPos1][4] - 1
				aAux1[nPos1][4] := nQtde
			EndIf
			
			nTotSep := nTotSep - 1
			
			oGetDados1:SetArray(aAux1,.T.) 
			oGetDados1:Refresh()
		Else
			MsgAlert("Produto nao encontrado.","Atencao")
		EndIf
	EndIf
EndIf

cExclui := SPACE(23)

Return

Static Function ARRAYAPT(cNumPF)
Local aArray   := {}
Local cDescPrd := ""
                               
DbSelectArea("SZP")
DbSetOrder(1)
If DbSeek(xFilial("SZP")+cNumPF)
 	While SZP->(!EOF()) .And. SZP->ZP_NUMPF = cNumPF
 		cDescPrd := GetAdvFVal("SB1","B1_DESC",xFilial("SB1")+SZP->ZP_PRODUTO,1,"") 
 		aAdd(aArray,{SZP->ZP_CODBAR,SZP->ZP_PRODUTO,cDescPrd,SZP->ZP_LOTECTL,SZP->ZP_QUANT,.F.})
 		SZP->(DbSkip())
 	EndDo
Else
	aAdd(aArray,{"","","","",0,.F.})
EndIf	

Return aArray


User Function HP3PESQV(cCodVol,lPesq,nLinha)
Local aAux1 := aClone(oGetDados1:aCols)
Local lRet  := .T.
Local cDescric := ""

If lPesq
	If !Empty(cCodVol)
		cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cCodVol,1,"")
		aAux1[nLinha][7] := cDescric
		If Empty(cDescric)
			lRet := .F.
			MsgAlert("Tipo de volume inexistente!","Atencao")
		EndIf
	Else
		aAux1[nLinha][7] := ""		
	EndIf
	oGetDados1:SetArray(aAux1,.T.) 
	oGetDados1:Refresh()
Else
	If !Empty(cCodVol)
		cDescVol := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cCodVol,1,"")
		If Empty(cDescVol)
			lRet := .F.
			MsgAlert("Tipo de volume inexistente!","Atencao")
		EndIf
	Else
		cDescVol := ""		
	EndIf
EndIf

Return lRet

User Function HP3VDQTDE(nPosAtu)
Local aAux1 := aClone(oGetDados1:aCols)
Local aAux2 := aClone(oGetDados2:aCols)
Local cCodBar := aAux1[nPosAtu][1]
Local nQtde   := aAux1[nPosAtu][3]
Local nPosEAN := 0
Local nx      := 0
Local nQtdTot := 0
Local nQtdOri := 0
Local lRet    := .T.

If !Empty(cCodBar)
	For nx := 1 to Len(aAux1)
		If aAux1[nx][1] = cCodBar .And. nx <> nPosAtu
			nQtdTot += aAux1[nx][3]
		EndIf
	Next
	nQtdTot += M->ZJ_QTDLIB
	nPosEAN := Ascan(aAux2,{|x| x[1] = cCodBar})
	If nPosEAN > 0
		nQtdOri := aAux2[nPosEAN][4]
		If nQtdTot > nQtdOri
			MsgAlert("Saldo insuficiente para distribuir.","Atencao")
			lRet := .F.
		Else
			aAux2[nPosEAN][3] := nQtdOri - nQtdTot 
		EndIf
		oGetDados1:SetArray(aAux1,.T.) 
		oGetDados1:Refresh()
		
		oGetDados2:SetArray(aAux2,.T.) 
		oGetDados2:Refresh()
	EndIf
EndIf

Return lRet

Static Function DADOS1(cNumPF)
Local cQuery   := ""
Local aRet     := {}
Local nSldSep  := 0
Local cDescric := "" 

IF SELECT("TMPSZR") > 0
	TMPSZR->(DbCloseArea())
ENDIF

cQuery := "select ZR_NUMPF,ZR_ITEM,ZR_CODBAR,ZR_PRODUTO,B1_DESC,ZR_QUANT,ZR_VOLUME,ZR_TPVOL"
cQuery += CRLF + "from "+RetSqlName("SZR")+" SZR WITH(NOLOCK) "
cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) "
cQuery += CRLF + "on  B1_COD = ZR_PRODUTO "
cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where SZR.D_E_L_E_T_ = '' "
cQuery += CRLF + "and ZR_FILIAL = '"+xFilial("SZR")+"' "
cQuery += CRLF + "and ZR_NUMPF  = '"+cNumPF+"' "
cQuery += CRLF + "order by ZR_NUMPF,ZR_ITEM "

MemoWrite("HPCPA003_DADOS1.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSZR", .F., .T.)

DbSelectArea("TMPSZR")

If TMPSZR->(!EOF())
	While TMPSZR->(!EOF())
		nTotSep += TMPSZR->ZR_QUANT
		cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+TMPSZR->ZR_TPVOL,1,"") 
		aAdd(aRet,{TMPSZR->ZR_ITEM,TMPSZR->ZR_CODBAR,TMPSZR->ZR_PRODUTO+" - "+TMPSZR->B1_DESC,TMPSZR->ZR_QUANT,IF(EMPTY(TMPSZR->ZR_VOLUME),SPACE(3),TMPSZR->ZR_VOLUME),IF(EMPTY(TMPSZR->ZR_TPVOL),SPACE(3),TMPSZR->ZR_TPVOL),cDescric,.F.})
		TMPSZR->(DbSkip())
	EndDo
Else
	aAdd(aRet,{"","","",0,SPACE(3),SPACE(3),"",.F.})
EndIf

TMPSZR->(DbCloseArea())

Return aRet

Static Function DADOS2(cNumPF,lConf)
Local cQuery  := ""
Local aRet    := {}
Local nSldSep := 0

IF SELECT("TMPSZJ") > 0
	TMPSZJ->(DbCloseArea())
ENDIF

cQuery := "select ZJ_PRODUTO,B1_DESC,B1_CODBAR,SUM(ZJ_QTDLIB) AS ZJ_QTDLIB,SUM(ZJ_QTDSEP) AS ZJ_QTDSEP"
cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ WITH(NOLOCK) "
cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) "
cQuery += CRLF + "on  B1_COD = ZJ_PRODUTO "
cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where SZJ.D_E_L_E_T_ = ''" 
cQuery += CRLF + "and ZJ_FILIAL = '"+xFilial("SZJ")+"' "
cQuery += CRLF + "and ZJ_NUMPF  = '"+cNumPF+"' "
cQuery += CRLF + "group by ZJ_PRODUTO,B1_DESC,B1_CODBAR "
cQuery += CRLF + "order by ZJ_PRODUTO "

MemoWrite("HPCPA003_DADOS2.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPSZJ", .F., .T.)

DbSelectArea("TMPSZJ")

If TMPSZJ->(!EOF())
	While TMPSZJ->(!EOF())
		If !lConf
			nTotLib += TMPSZJ->ZJ_QTDLIB
		EndIf	
		nSldSep := TMPSZJ->ZJ_QTDLIB-TMPSZJ->ZJ_QTDSEP
		If nSldSep <> 0	// TODO Nao carrega o produto 100 % conferido [Ronaldo Pereira 06/09/2018]
			aAdd(aRet,{TMPSZJ->B1_CODBAR,TMPSZJ->ZJ_PRODUTO+" - "+SubStr(TMPSZJ->B1_DESC,1,40),nSldSep,TMPSZJ->ZJ_QTDLIB,.F.})
        EndIf
		TMPSZJ->(DbSkip())
	EndDo
EndIf

If !lConf
	If nTotSep >= nTotLib .And. lHabilita
		lTOk := .T.
	EndIf
EndIf

TMPSZJ->(DbCloseArea())

Return aRet

//Static Function VALAPONT(cApto,cPreFat,cLotDAP,lSalva)
Static Function VALAPONT(cApto,cPreFat,cLotDAP,lSalva,lAutoS)
Local aAux1    := aClone(oGetDados1:aCols)
Local aAux2    := aClone(oGetDados2:aCols)
Local aAux3    := {}
Local nx,ny    := 0
Local i	       := 0
Local cCodBar  := SubStr(cApto,1,13)+SPACE(2)
Local cLote    := SubStr(cApto,14,10)
Local nPosEAN  := 0
Local cProduto := ""
Local nQtde    := 0
Local cChave   := cCodBar+cVolume+cTpVol
Local nQtdLot  := 0
Local cAmzPik  := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local cDescric := ""
Local nLen     := 0
Local lAtu     := .F.
Local nTotConf := 0
Local lContinua := .T.
Local cBarras   := ""
Local nPosBar   := 0
Local cLtPd     := AllTrim(SuperGetMV("MV_XLOTPAD",.F.,"000000"))
Local lRet      := .T.
Local nSldApto  := 0  

If Empty(cLote)
	cLote := cLtPd
EndIf

If Len(aAux1) > 0 .and. lAutoS
	If !Empty(cApto) .And. lContinua .And. !lSalva
		DbSelectArea("SB1")
		DbSetOrder(5) //B1_FILIAL+B1_CODBAR
		If DbSeek(xFilial("SB1")+cCodBar)
			DbSelectArea("SB8")
			DbSetOrder(3)
			If DbSeek(xFilial("SB8")+SB1->B1_COD+cAmzPik+cLote)
				nPosEAN := Ascan(aAux2,{|x| x[1] = cCodBar})
				If nPosEAN > 0
					nSldApto := aAux2[nPosEAN][3] //Saldo a apontar
					If nSldApto > 0
						cProduto := SB1->B1_COD +" - "+SB1->B1_DESC
						cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cTpVol,1,"") 
						If Len(aAux1) = 1 .And. Empty(aAux1[1][2])
							aAux1[1][1] := "01"
							aAux1[1][2] := cCodBar
							aAux1[1][3] := cProduto
							aAux1[1][4] := 1
							aAux1[1][5] := Alltrim(cVolume)
							aAux1[1][6] := cTpVol
							aAux1[1][7] := cDescric
							aAux1[1][8] := .F.
						Else
							nPosChv := Ascan(aAux1,{|x| x[2]+x[5]+x[6] == cChave})
							If nPosChv > 0
								//If Empty(aAux1[nPosChv][4])
									nQtde := aAux1[nPosChv][4]
									aAux1[nPosChv][4] := nQtde + 1
								//Else
								//	aAdd(aAux1,{cCodBar,cProduto,1,space(3),space(1),.F.})
								//EndIf	
							Else
								aAdd(aAux1,{SOMA1(aAux1[len(aAux1)][1],2),cCodBar,cProduto,1,cVolume,cTpVol,cDescric,.F.})
							EndIf
														
						/*
							If aAux1[Len(aAux1)][1] = cCodBar
								If Empty(aAux1[Len(aAux1)][4])
									nQtde := aAux1[Len(aAux1)][3]
									aAux1[Len(aAux1)][3] := nQtde + 1	
								Else
									aAdd(aAux1,{cCodBar,cProduto,1,space(3),space(1),.F.})
								EndIf
							Else
								aAdd(aAux1,{cCodBar,cProduto,1,space(3),space(1),.F.})
							EndIf*/
						EndIf
						
						//If Empty(cVolume) .Or. Empty(cTpVol) 
							//cChv := xFilial("SZP")+cPreFat+cCodBar+cLote
						//Else
							//cChv := xFilial("SZP")+cPreFat+cCodBar+cLote+cVolume+cTpVol
						//EndIf
								
						
						nTotSep := nTotSep + 1
						
						aAux2[nPosEAN][3] := nSldApto - 1
						
						// TODO Nao carrega o produto 100 % conferido [Weskley Silva 06/09/2018]
						If aAux2[nPosEAN][3] = 0
							aDel(aAux2,nPosEAN)
							aSize(aAux2,Len(aAux2)-1)
							nLen := nLen - 1
						EndIf
									
						
						oGetDados2:SetArray(aAux2,.T.) 
						oGetDados2:Refresh()
						
						oApont:SetFocus()
					Else
						MsgAlert("Produto ja foi 100% conferido!","Atencao")
					EndIf	
				Else
					MsgAlert("Produto nao pertence a este Pre-Faturamento ou ja foi 100% conferido!","Atencao")
					//cApont := SPACE(19)
					//Return 
				EndIf
			Else
				MsgAlert("Lote nao encontrado para o produto!","Atencao")
				//cApont := SPACE(19)
				//Return
			EndIf	
		Else
			MsgAlert("Codigo de barras nao encontrado!","Atencao")
			//cApont := SPACE(19)
			//Return
		EndIf
	Else
		If Len(AllTrim(cApto)) > 0 //.And. Len(AllTrim(cApto)) <= 13
			MsgAlert("Apontamento com 13 digitos ou menos","Atencao")
		EndIf
	EndIf

	nx := len(aAux1)

	If aAux1[nx][4] > 0
		DbSelectArea("SZR")
		DbSetOrder(1) //Padr(cNumSoli,TamSx3("C1_NUM")[1])
		If !DbSeek(xFilial("SZR")+Padr(cPreFat,TamSx3("ZR_NUMPF")[1])+Padr(aAux1[nx][1],TamSx3("ZR_ITEM")[1])+Padr(SubStr(aAux1[nx][3],1,15),TamSx3("ZR_PRODUTO")[1])) 
			RecLock("SZR",.T.)
			SZR->ZR_FILIAL  := xFilial("SZR")
			SZR->ZR_NUMPF   := cPreFat
			SZR->ZR_NUMPED  := SubStr(cPreFat,1,6)
			SZR->ZR_ITEM    := aAux1[nx][1]
			SZR->ZR_CODBAR  := aAux1[nx][2]
			SZR->ZR_PRODUTO := SubStr(aAux1[nx][3],1,15)
			SZR->ZR_QUANT   := aAux1[nx][4]
			SZR->ZR_VOLUME  := aAux1[nx][5]
			SZR->ZR_TPVOL   := aAux1[nx][6]
			SZR->ZR_USR     := __cUserID
			SZR->ZR_DATA    := DDATABASE
			SZR->ZR_HORA    := Time()
			MsUnlock()
		Else
			RecLock("SZR",.F.)
			SZR->ZR_FILIAL  := xFilial("SZR")
			SZR->ZR_ITEM    := aAux1[nx][1]
			SZR->ZR_CODBAR  := aAux1[nx][2]
			SZR->ZR_PRODUTO := SubStr(aAux1[nx][3],1,15)
			SZR->ZR_QUANT   := aAux1[nx][4]
			SZR->ZR_VOLUME  := aAux1[nx][5]
			SZR->ZR_TPVOL   := aAux1[nx][6]
			SZR->ZR_USR     := __cUserID
			SZR->ZR_DATA    := DDATABASE
			SZR->ZR_HORA    := Time()
			MsUnlock()
		Endif		
		
	
		DbSelectArea("SB1")
		DbSetOrder(5) //B1_FILIAL+B1_CODBAR
		DbSeek(xFilial("SB1")+alltrim(cCodBar))

// TODO VERIFICACAO DE SALDO PARA NAO DUPLICAR A SZP.   -- WESKLEY SILVA 02/05/2018		
		IF nSldApto > 0 
		
		IF FOUND() .and. substr(SB1->B1_COD,1,2) <> "SV"
			RecLock("SZP",.T.)
			SZP->ZP_FILIAL  := xFilial("SZP")
			SZP->ZP_LOTDAP  := cLotDAP
			SZP->ZP_NUMPF   := cPreFat
			SZP->ZP_PEDIDO  := SubStr(cPreFat,1,6)
			SZP->ZP_CODBAR  := cCodBar
			SZP->ZP_PRODUTO := SB1->B1_COD
			SZP->ZP_LOTECTL := cLote
			SZP->ZP_QUANT   := 1
			SZP->ZP_DATA 	:= DDATABASE
			SZP->ZP_HORA	:= Time()
			MsUnlock()
		ENDIF
		
		
		DbSelectArea("SZP")
		SZP->(DbSetOrder(2))
		DbGoTop()
		If SZP->(Dbseek(xFilial("SZP")+cLotDaP+cPreFat))
			nSZJLib := 0
			While SZP->(!EOF()) .And. SZP->ZP_LOTDAP+SZP->ZP_NUMPF = cLotDaP+cPreFat
				If SZP->ZP_PRODUTO == SubStr(aAux1[nx][3],1,15)
					nSZJLib += 1
				Endif 
				SZP->(DbSkip())
			EndDo
		
		
			DbSelectArea("SZJ")
			DbSetOrder(1)
			DbSeek(xFilial("SZJ")+cPreFat)
			If Found()
				While !EOF() .and. cPreFat == SZJ->ZJ_NUMPF	
					If SZJ->ZJ_PRODUTO == SubStr(aAux1[nx][3],1,15)
						RecLock("SZJ",.F.)
							SZJ->ZJ_QTDSEP := nSZJLib
						MsUnlock()
					Endif
					DbSkip()
				End
			Endif
		EndIf
	ENDIF
		nLen := Len(aAux1)
		For nx := 2 to nLen
			If aAux1[nx][4] <= 0
				aDel(aAux1,nx)
				aSize(aAux1,Len(aAux1)-1)
				nLen := nLen - 1
				lAtu := .T.
			EndIf
			If nLen = nx
				Exit
			EndIf
		Next
		
		aAux3 := aClone(aAux1)
		aProd := {}
		
		Aadd(aProd,{SPACE(15),0})
		
		For nx := 1 to Len(aAux1)
			cw := "00"
			For i := 1 to nx
				cw := SOMA1(cw,2) 
			Next
			If lAtu
				aAux1[nx][1] := cw //StrZero(nx,2)
			EndIf
			nTotConf := 0
			//nTotConf += aAux1[nx][4]
			cBarras := aAux1[nx][2]
			nPosBar := Ascan(aProd,{|x| x[1] = cBarras})
			If nPosBar = 0
				For ny := 1 to Len(aAux3)
					If aAux3[ny][2] = cBarras
						nTotConf += aAux3[ny][4]
					EndIf 
				Next
				aAdd(aProd,{cBarras,nTotConf})
			EndIf
		Next
		
		aErro := {}
		
		If Len(aProd) > 1 
			For nx := 2 to Len(aProd)
				nTotPC := 0
				DbSelectArea("SZP")
				DbSetOrder(1)
				If DbSeek(xFilial("SZP")+cPreFat+aProd[nx][1])
					While SZP->(!EOF()) .And. SZP->ZP_NUMPF+SZP->ZP_CODBAR = cPreFat+aProd[nx][1]
						nTotPC += SZP->ZP_QUANT
						SZP->(DbSkip())
					EndDo
	//				IF !lautoS
						If nTotPC <> aProd[nx][2]
							aAdd(aErro,aProd[nx][1])
						EndIf
	//				Endif
				EndIf 
			Next
		EndIf
		
		If Len(aErro) > 0
			cMsgErro := "Os seguintes produtos nao estao distribuidos corretamente:"
			For nx := 1 to Len(aErro)
				cMsgErro += CRLF + aErro[nx]
			Next
			MsgAlert(cMsgErro,"Aten"+chr(65533)+chr(65533)+"o")
			lContinua := .F.
			lRet := .F.
		Else
		
			If lImpAut//Marcio
				HPPRTETQ(cCodBar,cPreFat)
			Endif
			
		EndIf
	Endif
	
	oGetDados1:SetArray(aAux1,.T.) 
	oGetDados1:Refresh()
	
	oGetDados2:SetArray(aAux2,.T.) 
	oGetDados2:Refresh()
	
	If nTotSep >= nTotLib
		lTOk := .T.
	Else
		lTOk := .F.	
	EndIf
	
	cApont := SPACE(23)


Else
	nLen := Len(aAux1)
	For nx := 2 to nLen
		If aAux1[nx][4] <= 0
			aDel(aAux1,nx)
			aSize(aAux1,Len(aAux1)-1)
			nLen := nLen - 1
			lAtu := .T.
		EndIf
		If nLen = nx
			Exit
		EndIf
	Next
	
	aAux3 := aClone(aAux1)
	aProd := {}
	
	Aadd(aProd,{SPACE(15),0})
	
	For nx := 1 to Len(aAux1)
		cw := "00"
		For i := 1 to nx
			cw := SOMA1(cw,2) 
		Next
		
		If lAtu
			aAux1[nx][1] := cw //StrZero(nx,2)
	    EndIf
		
		nTotConf := 0
		//nTotConf += aAux1[nx][4]
		cBarras := aAux1[nx][2]
		nPosBar := Ascan(aProd,{|x| x[1] = cBarras})
		
		If nPosBar = 0
			For ny := 1 to Len(aAux3)
				If aAux3[ny][2] = cBarras
					nTotConf += aAux3[ny][4]
				EndIf 
			Next
			aAdd(aProd,{cBarras,nTotConf})
		EndIf
	Next
	
	aErro := {}
	
	If Len(aProd) > 1 
		For nx := 2 to Len(aProd)
			nTotPC := 0
			DbSelectArea("SZP")
			DbSetOrder(1)
			If DbSeek(xFilial("SZP")+cPreFat+aProd[nx][1])
				While SZP->(!EOF()) .And. SZP->ZP_NUMPF+SZP->ZP_CODBAR = cPreFat+aProd[nx][1]
					nTotPC += SZP->ZP_QUANT
					SZP->(DbSkip())
				EndDo
				IF !lautoS
					If nTotPC <> aProd[nx][2]
						aAdd(aErro,aProd[nx][1])
					EndIf
				Endif
			EndIf 
		Next
	EndIf
	
	If Len(aErro) > 0
		cMsgErro := "Os seguintes produtos nao distribuidos corretamente:"
		For nx := 1 to Len(aErro)
			cMsgErro += CRLF + aErro[nx]
		Next
		MsgAlert(cMsgErro,"Aten"+chr(65533)+chr(65533)+"o")
		lContinua := .F.
		lRet := .F.
	EndIf
	
	If !Empty(cApto) .And. lContinua .And. !lSalva
		DbSelectArea("SB1")
		DbSetOrder(5) //B1_FILIAL+B1_CODBAR
		If DbSeek(xFilial("SB1")+cCodBar)
			DbSelectArea("SB8")
			DbSetOrder(3)
			If DbSeek(xFilial("SB8")+SB1->B1_COD+cAmzPik+cLote)
				nPosEAN := Ascan(aAux2,{|x| x[1] = cCodBar})
				If nPosEAN > 0
					nSldApto := aAux2[nPosEAN][3] //Saldo a apontar
					If nSldApto > 0
						cProduto := SB1->B1_COD +" - "+SB1->B1_DESC
						cDescric := GetAdvFVal("SZQ","ZQ_DESCRIC",xFilial("SZQ")+cTpVol,1,"") 
						If Len(aAux1) = 1 .And. Empty(aAux1[1][2])
							aAux1[1][1] := "01"
							aAux1[1][2] := cCodBar
							aAux1[1][3] := cProduto
							aAux1[1][4] := 1
							aAux1[1][5] := Alltrim(cVolume)
							aAux1[1][6] := cTpVol
							aAux1[1][7] := cDescric
							aAux1[1][8] := .F.
						Else
							nPosChv := Ascan(aAux1,{|x| x[2]+x[5]+x[6] == cChave})
							If nPosChv > 0
								//If Empty(aAux1[nPosChv][4])
									nQtde := aAux1[nPosChv][4]
									aAux1[nPosChv][4] := nQtde + 1
								//Else
								//	aAdd(aAux1,{cCodBar,cProduto,1,space(3),space(1),.F.})
								//EndIf	
							Else
								aAdd(aAux1,{SOMA1(aAux1[len(aAux1)][1],2),cCodBar,cProduto,1,cVolume,cTpVol,cDescric,.F.})
							EndIf
							
						/*
							If aAux1[Len(aAux1)][1] = cCodBar
								If Empty(aAux1[Len(aAux1)][4])
									nQtde := aAux1[Len(aAux1)][3]
									aAux1[Len(aAux1)][3] := nQtde + 1	
								Else
									aAdd(aAux1,{cCodBar,cProduto,1,space(3),space(1),.F.})
								EndIf
							Else
								aAdd(aAux1,{cCodBar,cProduto,1,space(3),space(1),.F.})
							EndIf*/
						EndIf
						
						//If Empty(cVolume) .Or. Empty(cTpVol) 
							//cChv := xFilial("SZP")+cPreFat+cCodBar+cLote
						//Else
							//cChv := xFilial("SZP")+cPreFat+cCodBar+cLote+cVolume+cTpVol
						//EndIf
								
						DbSelectArea("SZP")
						SZP->(DbSetOrder(2))
						If SZP->(Dbseek(xFilial("SZP")+cLotDaP+cPreFat+SB1->B1_COD+cLote))
							nQtdLot := SZP->ZP_QUANT
							Reclock("SZP",.F.)
							SZP->ZP_QUANT   := nQtdLot + 1
							MsUnlock()
						Else
							RecLock("SZP",.T.)
							SZP->ZP_FILIAL  := xFilial("SZP")
							SZP->ZP_LOTDAP  := cLotDAP
							SZP->ZP_NUMPF   := cPreFat
							SZP->ZP_PEDIDO  := SubStr(cPreFat,1,6)
							SZP->ZP_CODBAR  := cCodBar
							SZP->ZP_PRODUTO := SB1->B1_COD
							SZP->ZP_LOTECTL := cLote
							SZP->ZP_QUANT   := 1
							SZP->ZP_DATA 	:= DDATABASE
							SZP->ZP_HORA	:= Time()
							MsUnlock()
						EndIf
						
						nTotSep := nTotSep + 1
						
						aAux2[nPosEAN][3] := nSldApto - 1
						
						oApont:SetFocus()
						oGetDados1:Refresh()
					Else
						MsgAlert("Produto ja foi 100% conferido!","Atencao")
						oGetDados1:Refresh()
					EndIf	
				Else
					MsgAlert("Produto nao pertence a este Pre-Faturamento!","Atencao")
					//cApont := SPACE(19)
					//Return 
				EndIf
			Else
				MsgAlert("Lote nao encontrado para o produto!","Atencao")
				//cApont := SPACE(19)
				//Return
			EndIf	
		Else
			MsgAlert("Codigo de barras nao encontrado!","Atencao")
			//cApont := SPACE(19)
			//Return
		EndIf
	Else
		If Len(AllTrim(cApto)) > 0 //.And. Len(AllTrim(cApto)) <= 13
			MsgAlert("Apontamento com 13 digitos ou menos","Atencao")
		EndIf
	EndIf
	
	oGetDados1:SetArray(aAux1,.T.) 
	oGetDados1:Refresh()
	
	oGetDados2:SetArray(aAux2,.T.) 
	oGetDados2:Refresh()
	
	If nTotSep >= nTotLib
		lTOk := .T.
	Else
		lTOk := .F.	
	EndIf
	
	cApont := SPACE(23)
Endif

Return lRet

Static Function SALVAAPTO(cNumPF)
Local cQuery   := ""
Local lRet     := .T.
Local nSldDist := 0
Local aAux1    := aClone(oGetDados1:aCols)
Local aAux2    := aClone(oGetDados2:aCols)
Local nx       := 0
Local lCont    := .F.
Local cPedido  := ""

lCont := VALAPONT(cApont,TRB->ZJ_NUMPF,TRB->ZJ_LOTDAP,.F.,.F.)

//DbSelectArea("SC5")
//DbSetOrder(1)
//DbSeek(xfilial("SC5")+SubStr(cNumPF,1,6))

//If SC5->C5_XBLQ <> "L" .and. !lConf
//	MsgAlert("Pedido com Bloqueio Comercial.","Atencao")
//	Return
//Endif

If !lCont
	Return
EndIf

IF SELECT("TMPAPTO") > 0
	TMPAPTO->(DbCloseArea())
ENDIF

cQuery := "select ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO,SUM(ZP_QUANT) AS ZP_QUANT"
cQuery += CRLF + "from "+RetSqlName("SZP")+" SZP WITH(NOLOCK)  "
cQuery += CRLF + "where SZP.D_E_L_E_T_ = '' "
cQuery += CRLF + "and ZP_FILIAL = '"+xFilial("SZP")+"' "
cQuery += CRLF + "and ZP_NUMPF  = '"+cNumPF+"' "
cQuery += CRLF + "group by ZP_NUMPF,ZP_PEDIDO,ZP_PRODUTO "
cQuery += CRLF + "order by ZP_NUMPF,ZP_PRODUTO "

MemoWrite("HPCPA003_SALVAAPTO.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPAPTO", .F., .T.)

DbSelectArea("TMPAPTO")

If TMPAPTO->(!EOF())
	If MsgYesNo("Confirma conferencia?","Confirmacao")
	//TODO WESKLEY SILVA 01/08/2018 [Begin transactio para garantir a efetivacao do pedido]
	Begin Transaction
	
		cPedido := TMPAPTO->ZP_PEDIDO		
		While TMPAPTO->(!EOF())
			DbSelectArea("SZJ")
			DbSetOrder(3)
			If DbSeek(xFilial("SZJ")+TMPAPTO->ZP_NUMPF+TMPAPTO->ZP_PRODUTO)
				nSldDist := TMPAPTO->ZP_QUANT
				While SZJ->(!EOF()) .And. SZJ->ZJ_PRODUTO = TMPAPTO->ZP_PRODUTO .And. nSldDist > 0
					If nSldDist <= SZJ->ZJ_QTDLIB
						RecLock("SZJ",.F.)
						SZJ->ZJ_QTDSEP := nSldDist
						If lTOk .and. !lConf
							SZJ->ZJ_CONF := "S"
						EndIf
						MsUnlock()
						nSldDist := 0
					Else
						RecLock("SZJ",.F.)
						SZJ->ZJ_QTDSEP := SZJ->ZJ_QTDLIB
						If lTOk .and. !lConf
							SZJ->ZJ_CONF := "S"
						EndIf
						MsUnlock()
						nSldDist := nSldDist - SZJ->ZJ_QTDLIB
					EndIf
					SZJ->(DbSkip())
				EndDo
			EndIf
			TMPAPTO->(DbSkip())
		EndDo
		If Len(aAux1) > 0
			For nx := 1 to Len(aAux1)
				DbSelectArea("SZR")
				DbSetOrder(1)
				If DbSeek(xFilial("SZR")+cNumPF+aAux1[nx][1])
					RecLock("SZR",.F.)
					SZR->ZR_CODBAR  := aAux1[nx][2]
					SZR->ZR_PRODUTO := SubStr(aAux1[nx][3],1,15)
					SZR->ZR_QUANT   := aAux1[nx][4]
					SZR->ZR_VOLUME  := aAux1[nx][5]
					SZR->ZR_TPVOL   := aAux1[nx][6]
					SZR->ZR_USR     := __cUserID
					SZR->ZR_DATA    := DDATABASE
					SZR->ZR_HORA    := Time()
					MsUnlock()
				Else
					RecLock("SZR",.T.)
					SZR->ZR_FILIAL  := xFilial("SZR")
					SZR->ZR_NUMPF   := cNumPF
					SZR->ZR_ITEM    := aAux1[nx][1]
					SZR->ZR_CODBAR  := aAux1[nx][2]
					SZR->ZR_PRODUTO := SubStr(aAux1[nx][3],1,15)
					SZR->ZR_QUANT   := aAux1[nx][4]
					SZR->ZR_VOLUME  := aAux1[nx][5]
					SZR->ZR_TPVOL   := aAux1[nx][6]
					SZR->ZR_USR     := __cUserID
					SZR->ZR_DATA    := DDATABASE
					SZR->ZR_HORA    := Time()
					MsUnlock()
				EndIf
			Next
			//Atualizo a tabela deletando os resgistro caso o array tenha sido redimencionado para menos.
			DbSelectArea("SZR")
			DbSetOrder(1)
			If DbSeek(xFilial("SZR")+cNumPF)
				While SZR->(!EOF()) .And. SZR->ZR_NUMPF = cNumPF
					If SZR->ZR_ITEM > aAux1[Len(aAux1)][1]
						RecLock("SZR",.F.)
						SZR->(DBDELETE())
						MsUnlock()
					EndIf
					SZR->(DbSkip())
				EndDo
			EndIf
				
		EndIf
		DbSelectArea("TRB")
		If lTOk .and. !lConf
			lValid := .F.
			LIBERA(cNumPF,cPedido,lValid)
			RecLock("TRB",.F.)
			TRB->CONFERIDO := "Sim"
			TRB->EMCONFERE := "Nao"
			TRB->EFETIVADO := "Sim"
			MsUnlock()
		Else
			RecLock("TRB",.F.)
			TRB->EMCONFERE := "Sim"
			MsUnlock()
		EndIf	
	end Transaction 
	Else
		lRet := .F.
	EndIf	
Else
	MsgAlert("Nao foi realizado nenhum apontamento.","Atencao")
	lRet := .F.	
EndIf

TMPAPTO->(DbCloseArea())

Return lRet

Static Function LIBERA(cNumPF,cPedido,lValid)
Local cQuery  := ""
Local nQtdLib := 0
Local nQtdRes := 0
Local lValid := .T.
Local cAmzPik := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local nTotPed := 0
Local nTotLib := 0

IF SELECT("TMPLIB") > 0
	TMPLIB->(DbCloseArea())
ENDIF

cQuery := "select ZJ_NUMPF,ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,B1_CODBAR,SUM(ZJ_QTDSEP) AS ZJ_QTDSEP"
cQuery += CRLF + "from "+RetSqlName("SZJ")+" SZJ WITH (NOLOCK) "
cQuery += CRLF + "inner join "+RetSqlName("SB1")+" SB1 WITH (NOLOCK) "
cQuery += CRLF + "on B1_COD = ZJ_PRODUTO "
cQuery += CRLF + "and SB1.D_E_L_E_T_ = '' "
cQuery += CRLF + "where SZJ.D_E_L_E_T_ = '' " 
cQuery += CRLF + "and ZJ_FILIAL = '"+xFilial("SZJ")+"' "
cQuery += CRLF + "and ZJ_NUMPF  = '"+cNumPF+"' "
cQuery += CRLF + "group by ZJ_NUMPF,ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO,B1_CODBAR "
cQuery += CRLF + "order by ZJ_PEDIDO,ZJ_ITEM,ZJ_ITEMGRD,ZJ_PRODUTO "

MemoWrite("HESTA003_LIBERA.txt",cQuery)

//cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TMPLIB", .F., .T.)

DbSelectArea("SC6")
DbSetOrder(1)
If DbSeek(xFilial("SC6")+cPedido)
	While SC6->(!EOF()) .And. SC6->C6_NUM = cPedido
		nTotPed += SC6->C6_QTDVEN
		SC6->(DbSkip())
	EndDo
EndIf

DbSelectArea("TMPLIB")

lProd := .T.

_aprode := {}

If TMPLIB->(!EOF())
	Begin Transaction
	While TMPLIB->(!EOF())
		DbSelectArea("SC6")
		DbSetOrder(1)
		If DBSeek(xFilial("SC6")+TMPLIB->ZJ_PEDIDO+TMPLIB->ZJ_ITEM+TMPLIB->ZJ_PRODUTO)
			aEmpenho := GERAARRAY(TMPLIB->ZJ_NUMPF,TMPLIB->B1_CODBAR,TMPLIB->ZJ_QTDSEP,TMPLIB->ZJ_PRODUTO)
			If Len(aEmpenho) > 0
			
				//Essa funcao MaLibDoFat e padrao. Ela que gera o SC9.
				nQtdLib := MaLibDoFat(SC6->(RecNo()),TMPLIB->ZJ_QTDSEP,.F.,.F.,.T.,1,.T.,.F.,,,)//aEmpenho)

// TODO Rotina para eliminar a duplicidade na SC9 quando houver o mesmo produto repetido na grade, a principio o rpo corrigiu por isso que esta comentado  [Caio 13/06/2018 ]
/*
				_nDifLib := 0
				_qry := "Select C9_FILIAL, C9_PEDIDO, C9_ITEM, C9_QTDLIB, ZJ_QTDSEP, C9_PRODUTO, C9_LOCAL, C9_LOTECTL, C9_NUMLOTE "
				_qry += "from "+RetSqlName("SC9")+" SC9 with (nolock) "
				_qry += "inner join "+RetSqlName("SZJ")+" SZJ with (nolock) on ZJ_DOC = '' and ZJ_CONF = 'S' and SZJ.D_E_L_E_T_ = '' and ZJ_PEDIDO = C9_PEDIDO and ZJ_ITEM = C9_ITEM and ZJ_PRODUTO = C9_PRODUTO "
				_qry += "where SC9.D_E_L_E_T_ = '' and C9_NFISCAL = '' and C9_PEDIDO = '"+SC6->C6_NUM+"' and C9_PRODUTO = '"+SC6->C6_PRODUTO+"' and C9_ITEM = '"+SC6->C6_ITEM+"' "
				_qry += "and ZJ_QTDSEP <> C9_QTDLIB "

				If Select("TMPDIF") > 0                                 
    				TMPDIF->(dbclosearea())
				EndIf

				DbUseArea( .T., 'TOPCONN', TCGENQRY(,,_qry),"TMPDIF", .F., .T.)
				DbSelectArea("TMPDIF")
				DbGoTop()
				
				If !EOF()
					_nDifLib := (TMPDIF->C9_QTDLIB-TMPDIF->ZJ_QTDSEP)-nQtdLib
				Endif


				If _nDifLib <> 0
					_qry := "update "+RetSqlName("SC9")+" set C9_QTDLIB = C9_QTDLIB + "+str(_nDifLib)+" where D_E_L_E_T_ = '' and C9_NFISCAL = '' and C9_PEDIDO = '"+SC6->C6_NUM+"' and C9_PRODUTO = '"+SC6->C6_PRODUTO+"' and C9_ITEM = '"+SC6->C6_ITEM+"' "
					TcSqlExec(_qry)
					
					DbSelectArea("SB8")
					DbSetOrder(2)
					If DbSeek(xFilial("SB8")+TMPDIF->C9_NUMLOTE+TMPDIF->C9_LOTECTL+SC6->C6_PRODUTO+cAmzPik)
						RecLock("SBF",.F.)
						SB8->B8_EMPENHO := SB8->B8_EMPENHO+_nDifLib
						SB8->B8_EMPENH2 := SB8->B8_EMPENH2+_nDifLib
						MsUnlock()
					EndIf                      

					DbSelectArea("SDC")
					DbSetOrder(1)
					DbSeek(xfilial("SDC")+SC6->C6_PRODUTO+cAmzPik+"SC6"+SC6->C6_NUM+SC6->C6_ITEM)
					
					DbSelectArea("SBF")
					DbSetOrder(1)
					If DbSeek(xFilial("SBF")+cAmzPik+SDC->DC_LOCALIZ+SC6->C6_PRODUTO+TMPDIF->C9_NUMLOTE+TMPDIF->C9_LOTECTL)
						RecLock("SBF",.F.)
						SBF->BF_EMPENHO := SBF->BF_EMPENHO+_nDifLib
						SBF->BF_EMPEN2  := SBF->BF_EMPEN2+_nDifLib
						MsUnlock()
					EndIf                      

				Endif

   				TMPDIF->(dbclosearea())
*/				
				DbSelectArea("SB2")
				DbSetOrder(2)
				If DbSeek(xFilial("SB2")+cAmzPik+SC6->C6_PRODUTO)
					nQtdRes := SB2->B2_XRESERV
					
								
					RecLock("SB2",.F.)
					SB2->B2_XRESERV := IF((nQtdRes - nQtdLib) < 0,0,(nQtdRes - nQtdLib)) 
			
					
					//SB2->B2_XRESERV := IF((nQtdRes - (nQtdLib+_nDifLib)) < 0,0,(nQtdRes - (nQtdLib+_nDifLib)))
					//SB2->B2_RESERVA := SB2->B2_RESERVA+_nDifLib
				    //SB2->B2_RESERV2 := SB2->B2_RESERV2+_nDifLib
					MsUnlock()
				EndIf                      
				
//				nTotLib += nQtdLib+_nDifLib
				nTotLib += nQtdLib
			Else
				MsgAlert("Falha na liberacao de estoque do produto: "+TMPLIB->ZJ_PRODUTO,"Atencao")
				Aadd(_aprode,TMPLIB->ZJ_PRODUTO)
				lProd := .F.
				lValid := .F.	
			EndIf		
		EndIf	
		TMPLIB->(DbSkip())
	EndDo
	If nTotLib > 0
		cBloq := ""
		DbSelectArea("SC5")
		DbSetOrder(1)
		If DbSeek(xFilial("SC5")+cPedido)
			cBloq := SC5->C5_XBLQ
			If nTotPed = nTotLib
				If lProd
					RecLock("SC5",.F.)
					SC5->C5_LIBEROK := "S"
					MsUnlock()
				Endif	
			EndIf 
		EndIf
		DbSelectArea("SC9")
		DbSetOrder(1)
		If DbSeek(xFilial("SC9")+cPedido)
			While SC9->(!EOF()) .And. SC9->C9_PEDIDO = cPedido
				If Empty(SC9->C9_NFISCAL) 
					RecLock("SC9",.F.)
					SC9->C9_XBLQ := cBloq
					SC9->C9_XNUMPF := cNumPF
					MsUnlock()
				EndIf
				SC9->(DbSkip())
			EndDo
		EndIf
	EndIf
	End Transaction
EndIf

TMPLIB->(DbCloseArea())

IF !lprod
	U_HP03EXC(2,_aprode)
Endif

Return

Static Function GERAARRAY(cNumPF,cCodBar,nQtdALib,cProd)
Local aRet     := {}
Local cQuery   := ""
Local cAmzPik  := AllTrim(SuperGetMV("MV_XAMZPIC",.F.,"E0")) //Armazem padrao picking
Local dDtValid := DDATABASE
Local nQtdLot  := 0

DbSelectArea("SZP")
DbSetOrder(1)
If DbSeek(xFilial("SZP")+cNumPF+cCodBar)
	While SZP->(!EOF()) .And. SZP->ZP_NUMPF+SZP->ZP_CODBAR = cNumPF+cCodBar
		nQtdLot := SZP->ZP_QUANT
		
		DbSelectArea("SB8")
		DbSetOrder(3)
		If DbSeek(xFilial("SB8")+SZP->ZP_PRODUTO+cAmzPik+SZP->ZP_LOTECTL)
			While SB8->(!EOF()) .And. SB8->B8_PRODUTO+SB8->B8_LOCAL+SB8->B8_LOTECTL = SZP->ZP_PRODUTO+cAmzPik+SZP->ZP_LOTECTL
				
				nLib := nQtdLot
				
				// TODO WESKLEY 10/05 VERIFICACAO DO SALDO 
				
				//If nQtdLot > 0 .And. SB8->B8_SALDO > 0
				//	If nQtdLot <= SB8->B8_SALDO
					//	nLib := nQtdLot
					//Else
					//	nLib := SB8->B8_SALDO	
				//	EndIf
					//LOTECTL,NUMLOTE,,,QTDELIB,QTDLIB2,DTVALID,,,,POTENCIA
					
					
					aAdd(aRet,{SB8->B8_LOTECTL,SB8->B8_NUMLOTE,"","",nLib,0,SB8->B8_DTVALID,"","","",cAmzPik,0})
					
					nQtdLot := nQtdLot - nLib
					
					RecLock("SZP",.F.)
					SZP->ZP_QTDLIB := SZP->ZP_QTDLIB + nLib
					MsUnlock()
				
				
				
				SB8->(DbSkip())
			
			EndDo
		EndIf
		
		SZP->(DbSkip())
	EndDo 
EndIf

Return aRet


Static Function HESTSAI03()
Local lRet := .F.

If MsgYesNo("Deseja realmente sair?","Conferencia")
 	lRet := .T.
EndIf

Return lRet 


User Function HP03LGEST()

Local aCores := {}
                                                                   
//BR_AMARELO,BR_AZUL,BR_BRANCO,BR_CINZA,BR_LARANJA,BR_MARROM,BR_VERDE,BR_VERMELHO,BR_PINK,BR_PRETO,BR_VIOLETA

aAdd(aCores,{"BR_VERMELHO","Transferencia em aberto" })
aAdd(aCores,{"BR_VERDE"   ,"Apto a Conferir" })
aAdd(aCores,{"BR_AMARELO" ,"Em Conferencia" })
aAdd(aCores,{"BR_AZUL"    ,"Conferido" })
aAdd(aCores,{"BR_PRETO"   ,"Faturado" })

BrwLegenda("Conferencia","Legenda",aCores)

Return .T.

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Mostra ja Faturados ?","","","Mv_ch1","N",1,0,2,"C","","","","N","Mv_par01","Sim","","","","Nao","","","","","","","","","","","","","","","","","","",{"",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)

	
return