#INCLUDE "rwmake.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HOPEZA7   � Autor � Roberto Le�o       � Data �  10/11/16   ���
�������������������������������������������������������������������������͹��
���Descricao � Cadastro de Tipos de Tecido.                               ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE LINGERIE                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function HOPEZA7()

Local cVldAlt := ".T." // Validacao para permitir a alteracao. Pode-se utilizar ExecBlock.
Local cVldExc := ".T." // Validacao para permitir a exclusao. Pode-se utilizar ExecBlock.

//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������

Private cPerg   := "ZA7"

Private cString := "ZA7"

dbSelectArea("ZA7")
dbSetOrder(1)

cPerg   := "ZA7"

Pergunte(cPerg,.F.)
SetKey(123,{|| Pergunte(cPerg,.T.)}) // Seta a tecla F12 para acionamento dos parametros

AxCadastro(cString,"Cadastro de Tipos de Tecido",cVldExc,cVldAlt)

Set Key 123 To // Desativa a tecla F12 do acionamento dos parametros


Return
