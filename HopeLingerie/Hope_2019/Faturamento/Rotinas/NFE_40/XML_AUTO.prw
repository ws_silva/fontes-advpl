#INCLUDE "XMLXFUN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "TBICONN.CH" 
#INCLUDE "TOPCONN.CH"

//#DEFINE MAXJOBNOAR 20   
//#DEFINE CTE_MOLEDO "57"
//#DEFINE SPED_MOLEDO "ALL"
  

User Function XML_AUTO() 
  
  Local cQuery := "" 
  Local cXMLDados := ""  
  Local cErro     := ""
  Local cAviso    := ""
  Local cDirMail    := IIf(IsSrvUnix(),"/mailtemplate/", "\mailtemplate\")
  Local cNFeSPED_WF  := GetSrvProfString("NFESPED_WF","0")
  Static __nHdlSMTP := 0
  
//  PREPARE ENVIRONMENT EMPRESA '01' FILIAL '0101' 
  
  cQuery := " SELECT R_E_C_N_O_ AS REC FROM TSS.dbo.SPED050 (NOLOCK) WHERE STATUSMAIL = '1' AND EMAIL <> '' AND STATUS = '6' AND NFE_ID = '2  000733947                                 '  AND D_E_L_E_T_ = '' " 
  
  
     If Select("EML") > 0 
		DbSelectArea("EML")
		EML->(DbCloseArea())
	EndIf
   
	dbUseArea(.T.,'TOPCONN', TCGenQry(,,cQuery),'EML', .F., .F.)     
     
	DbSelectArea("EML")
	DbGoTop() 

	While !Eof()

		USE SPED050 ALIAS SPED050 SHARED NEW VIA "TOPCONN"
		DBSELECTAREA("SPED050")
		DBGOTO(EML->REC)
	     nTimes := 0
	     cXmlDados := SPED050->XML_SIG
	    // cDtTime := DTOS(DDATABASE) + SubStr(Time(),1,2) + SubStr(Time(),4,2) + SubStr(Time(),7,2)
	    // MemoWrite("\LOG\NFE\"+cDtTime+"_"+"123"+".xml",EncodeUTF8(cXmlDados))
	     
		 cXmlDados := NfeProcNfe(SPED050->XML_SIG,cXMLDados,SpedNfeId(SPED050->XML_ERP,"versao"))
		 oXmlResult:= TSSXmlParser(cXmlDados,"_",@cErro,@cAviso)       
		 cAssunto:= "NFe Nacional"                                  
				cAnexo    := cDirMail+SubStr(SpedNfeId(SPED050->XML_SIG,"Id"),4,44)+"-nfe.xml"
				cAnexo2   := cDirMail+SpedNfeId(SPED050->XML_SIG,"Id")+"-nfe.xml"
				nHdlXml   := FCreate(cAnexo,0)
				nHdlXml2  := FCreate(cAnexo2,0)
				If nHdlXml > 0 .And. nHdlXml2 > 0
					FWrite(nHdlXml,cXmlDados)
					FWrite(nHdlXml2,SPED050->XML_SIG)
					FClose(nHdlXml)
					FClose(nHdlXml2)
					HSmtp(IIF(Empty(SPED050->EMAIL+";weskley.silva@hopelingerie.com.br"),Nil,{SPED050->EMAIL+";weskley.silva@hopelingerie.com.br"}),cAssunto,NfeMail(oXmlResult,oXmlNota),@cMailError,cAnexo,,@cEmailDest,"weskley.silva@hopelingerie.com.br")
					
					If Empty(cMailError)
						Begin Transaction
						RecLock("SPED050")
						SPED050->STATUSMAIL := 2
						SPED050->(MsUnLock())
						End Transaction	
						SPED050->(MsUnLock())
					Else
						cMailError := ""
						HSmtp(Nil,"Erro no Envio de e-mail. Verifique se o eMail do destinatário é válido - "+cEmailDest ,NfeMail(oXmlResult,oXmlNota),@cMailError,cAnexo,,)									
						If Empty(cMailError)
							Begin Transaction
							RecLock("SPED050")
							SPED050->STATUSMAIL := 2
							SPED050->(MsUnLock())
							End Transaction	
							SPED050->(MsUnLock())
						Else                             
							Exit
						EndIf										
					EndIf
					FErase(cAnexo)
					FErase(cAnexo2)
				EndIf
				FClose(nHdlXml)
				FClose(nHdlXml2)

				SPED050->(MsUnLock())
				MsUnLockAll()
				
				DbSelectArea("EML")
				DbSkip()
EndDo

			dbSelectArea("EML")
			dbCloseArea()
			DbSelectArea("SPED050")
			SMTPDisconnect()
			
			If cNFeSPED_WF=="1" .Or. (cNFeSPED_WF=="2" .And. nCount>0 )
				ConOut(cMsgModelo + IIF(cThreadID=="1","[ThreadID:"+AllTrim(Str(ThreadID(),10))+"] ","") + "SMTP ("+cIdEnt+"-"+cTime+" a "+Time()+"): "+AllTrim(Str(nCount,10)))
			EndIf	
	
Return()

Static Function HSmtp(aTo,cSubject,cMensagem,cError,cAnexo,cAnexo2,cEmailDest,cEmailBCC)

Local cMailServer := SpedGetMV("MV_SMTPSRV",SPED001->ID_ENT)
Local cLogin      := SpedGetMV("MV_SMTPAAC",SPED001->ID_ENT)
Local cMailConta  := SpedGetMV("MV_SMTPFAC",SPED001->ID_ENT)
Local cMailSenha  := SpedGetMV("MV_SMTPFPS",SPED001->ID_ENT)
Local cThreadID   := GetSrvProfString("SPED_THREADID","0")
Local lSMTPAuth   := SpedGetMV("MV_SMTPAUT",SPED001->ID_ENT)=="S"
Local lSSL        := SpedGetMV("MV_SMTPSSL",SPED001->ID_ENT)=="S"
Local lTLS        := SpedGetMV("MV_SMTPTLS",SPED001->ID_ENT)=="S"
Local lOk         := .F.
Local lSendOk     := .F.
Local nX          := 0

DEFAULT cSubject  := "Mensagem de Teste"
DEFAULT cMensagem := "Este é um email enviado automaticamente pelo Gerenciador de Contas do Totvs Services durante o teste das configurações da sua conta SMTP."
DEFAULT aTo       := {cMailConta}
DEFAULT cError    := ""
Default cEmailDest:= ""
If !Empty(cMailServer) .And. !Empty(cMailConta) .And. !Empty(cMailSenha) .And. !Empty(cLogin)
	//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
	//| Conecta ao servidor de SMTP                                                      |
	//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
	If __nHdlSMTP == 0
		If lSSL .And. lTLS	
			CONNECT SMTP SERVER cMailServer ACCOUNT cLogin PASSWORD cMailSenha RESULT lOk TLS SSL
		ElseIf lSSL
			CONNECT SMTP SERVER cMailServer ACCOUNT cLogin PASSWORD cMailSenha RESULT lOk SSL
		ElseIf lTLS
			CONNECT SMTP SERVER cMailServer ACCOUNT cLogin PASSWORD cMailSenha RESULT lOk TLS
		Else
			CONNECT SMTP SERVER cMailServer ACCOUNT cLogin PASSWORD cMailSenha RESULT lOk
		EndIf
		If lOk
			__nHdlSMTP := 1
		Else
			__nHdlSMTP := 0
		EndIf	
	Else 
		lSMTPAuth := .F.
	EndIf
	//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
	//| Verifica se há necessidade de autenticacao                                       |
	//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
	If __nHdlSMTP <> 0
		If ( lSMTPAuth )
			lOk := MailAuth(cLogin,cMailSenha)
		Else
			lOk := .T.
		EndIf
		If lOk
			__nHdlSMTP := 1
		Else
			GET MAIL ERROR cError
			ConOut(IIF(cThreadID=="1",'ThreadID='+AllTrim(Str(ThreadID(),15)),"")+" - Log SMTP: " + cError)
			SmtpDisconnect()
		EndIf	
	EndIf
	If __nHdlSMTP <> 0
		For nX := 1 To Len(aTo)
			If !("@" $ aTo[nX])                                        
				cSubject += " - email de destino inválido ("+aTo[nX]+")"
				aTo[nX]  := cMailConta
			EndIf		
			If !Empty(cAnexo2)
				SEND MAIL FROM cMailConta to AllTrim(aTo[nX]) Bcc cEmailBCC SUBJECT cSubject BODY cMensagem ATTACHMENT cAnexo,cAnexo2 RESULT lSendOk
			ElseIf !Empty(cAnexo)
				SEND MAIL FROM cMailConta to AllTrim(aTo[nX]) Bcc cEmailBCC SUBJECT cSubject BODY cMensagem ATTACHMENT cAnexo RESULT lSendOk
			Else
				SEND MAIL FROM cMailConta to AllTrim(aTo[nX]) Bcc cEmailBCC SUBJECT cSubject BODY cMensagem RESULT lSendOk
			EndIf
			If !lSendOk
				//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
				//³Erro no Envio do e-mail                                                 ³
				//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
				cEmailDest := aTo[nX]
				GET MAIL ERROR cError
				ConOut(IIF(cThreadID=="1",'ThreadID='+AllTrim(Str(ThreadID(),15)),"")+" - Log SMTP: " + cError)
				SMTPDisconnect()
				Exit
			EndIf
		Next nX
	Else
		//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
		//³ Erro na conexao com o SMTP Server ou na autenticacao da conta          ³
		//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
		If Empty(cError)
			GET MAIL ERROR cError
			ConOut(IIF(cThreadID=="1",'ThreadID='+AllTrim(Str(ThreadID(),15)),"")+" - Log SMTP: " + cError)
		EndIf
	EndIf
EndIf
Return(__nHdlSMTP)

Static Function NfeProcNfe(cXMLNFe,cXMLProt,cVersao,cNFMod)

Local aArea     := GetArea()
Local nAt       := 0      
Local nAy		:= 0
Local cXml      := ""
Local lDistrCanc:=.F.

cNFMod := IIf(Empty(cNFMod),"55",cNFMod)

//ÚÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄ¿
//³ Montagem da mensagem                                         ³
//ÀÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÄÙ
nAt := At("?>",cXmlProt)
If nAt > 0
	nAt +=2
Else
	nAt := 1
EndIf
Do Case
	Case cNFMod == "57"
		If !Empty(cXMLNFe)
			cXml := '<?xml version="1.0" encoding="UTF-8"?>'
			Do Case
				Case cVersao >= "1.03"
					cXml += '<cteProc xmlns="http://www.portalfiscal.inf.br/cte" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.portalfiscal.inf.br/cte procCTe_v1.00.xsd" versao="1.03">'
				OtherWise
					cXml += '<cteProc xmlns="http://www.portalfiscal.inf.br/cte" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.portalfiscal.inf.br/cte procCTe_v1.00.xsd" versao="'+cVersao+'">'
			EndCase
			cXml += cXmlNFe
		Else
			cXml := ""
		EndIf
		Do Case
			Case "retConsSitCTe" $ cXmlProt
				cXml += StrTran(SubStr(cXmlProt,nAt),"retConsSitCTe","protCTe")
			Case "retCancCTe" $ cXmlProt
				cXml += StrTran(SubStr(cXmlProt,nAt),"retCancCTe","protCTe")
			Case "retInutCTe" $ cXmlProt
				cXml += StrTran(SubStr(cXmlProt,nAt),"retInutCTe","protCTe")
			Case "protCTe" $ cXmlProt
				cXml += cXmlProt
			OtherWise
				cXml += "<protCTe>"
				cXml += cXmlProt
				cXml += "</protCTe>"
		EndCase
		If !Empty(cXMLNFe)
			cXml += '</cteProc>
		EndIf	
	OtherWise
		If !Empty(cXMLNFe)
			cXml := '<?xml version="1.0" encoding="UTF-8"?>'
			Do Case
				Case cVersao <= "1.07"
					cXml += '<nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" xmlns:ds="http://www.w3.org/2000/09/xmldsig#" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.portalfiscal.inf.br/nfe procNFe_v1.00.xsd" versao="1.00">'
				Case cVersao >= "2.00" .And. "cancNFe" $ cXmlNfe
					cXml += '<procCancNFe xmlns="http://www.portalfiscal.inf.br/nfe" versao="'+cVersao+'">'
					lDistrCanc:= .T.					
				OtherWise
					cXml += '<nfeProc xmlns="http://www.portalfiscal.inf.br/nfe" versao="'+cVersao+'">'
			EndCase
			cXml += cXmlNFe
		Else
			cXml := ""
		EndIf
		Do Case  
			Case "retConsSitNFe" $ cXmlProt	 .And. cVersao<"2.00"						
				cXmlProt := GeraProtNF(cXmlProt,cVersao,"retConsSitNFe")		
				cXml += cXmlProt
			Case "retCancNFe" $ cXmlProt .And. cVersao<"2.00"
				cXml += StrTran(SubStr(cXmlProt,nAt),"retCancNFe","protNFe")
			Case "retInutNFe" $ cXmlProt .And. cVersao<"2.00"
				cXml += StrTran(SubStr(cXmlProt,nAt),"retInutNFe","protNFe")
			Case "protNFe" $ cXmlProt .And. cVersao<"2.00" 				
				cXmlProt:= GeraProtNF(cXmlProt,cVersao,"retInutNFe")		
				cXml += cXmlProt 
			Case "PROTNFE" $ Upper(cXmlProt) .And. cVersao>="2.00" 				
				nAt:= At("<PROTNFE",Upper(cXmlProt))
				nAy:= RAt("</PROTNFE>",Upper(cXmlProt))       				
				cXmlProt:= SubStr(cXmlProt,nAt,nAy-nAt+10)
				cXmlProt:= GeraProtNF(cXmlProt,cVersao,"PROTNFE")
				cXml += cXmlProt				 
			Case "RETCANCNFE" $ Upper(cXmlProt) .And. cVersao>="2.00"
				cXmlProt:= GeraProtNF(cXmlProt,cVersao,"RETCANCNFE")
				cXml += cXmlProt				
			Case "RETINUTNFE" $ Upper(cXmlProt) .And. cVersao>="2.00"
				cXml += StrTran(SubStr(cXmlProt,nAt),"RETINUTNFE","protNFe")
			Case "RETCONSSITNFE" $ Upper(cXmlProt)	 .And. cVersao>="2.00"			
				cXml += StrTran(SubStr(cXmlProt,nAt),"RETCONSSITNFE","protNFe")		
			OtherWise
				cXml += "<protNFe>"
				cXml += cXmlProt
				cXml += "</protNFe>"
		EndCase              
		If cVersao < "2.00"
			nAt := At("versao=",cXml)
			cXml := StrTran(cXml,SubStr(cXml,nAt,13),'versao="'+cVersao+'"',,1)
        EndIf
		If !Empty(cXMLNFe)
			If lDistrCanc
				cXml += '</procCancNFe>'				
			Else 
				cXml += '</nfeProc>'
			EndIf
		EndIf
EndCase
RestArea(aArea)
Return(AllTrim(cXml))

Static Function SpedNfeId(cXML,cAttId)
Local nAt  := 0
Local cURI := ""
Local nSoma:= Len(cAttId)+2

nAt := At(cAttId+'=',cXml)
cURI:= SubStr(cXml,nAt+nSoma)
nAt := At('"',cURI)
If nAt == 0
	nAt := At("'",cURI)
EndIf
cURI:= SubStr(cURI,1,nAt-1)
Return(cUri)

Static Function TSSXmlParser( cXml, cExp, cAviso, cErro )

Local oXml    

DEFAULT cExp	:= ""                             
DEFAULT cAviso	:= ""  
DEFAULT cErro	:= "" 

/*Retira e valida algumas informações e caracteres indesejados para o parse*/
cXml := XmlClean(cXml)  
 
/*Faz o parser do XML*/
oXml := XmlParser(cXml,"_",@cAviso,@cErro)    

Return oXml

Static Function XmlClean( cXml )
    
Local cRetorno		:= "" 

DEFAULT cXml		:= ""

If ( !Empty(cXml) )
	/*Retira caractere '&' substitui por '&amp;'*/
	cRetorno := StrTran(cXml,"&","&amp;amp;")
EndIf

Return cRetorno     

Static Function SmtpDisconnect()

If __nHdlSMTP == 1
 DISCONNECT SMTP SERVER
 __nHdlSMTP := 0
EndIf
Return
