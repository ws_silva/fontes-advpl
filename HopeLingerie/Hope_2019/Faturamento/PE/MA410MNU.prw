#include "rwmake.ch"        
#include "TbiConn.ch"
#include "TbiCode.ch"
#include "protheus.ch" 


User function MA410MNU()
	
	AADD(aRotina, {"Entrada de Beneficiamento","U_RETORNO()",0,4,0,NIL})
	
return


User Function RETORNO()

	Local cNF 	 := SC5->C5_NOTA
	Local cForne := SC5->C5_CLIENT
	Local cSerie := SC5->C5_SERIE
	Local aParam := {}
	Local aRetParm	:= {}
	Local cFilOpc	:= Space(GetSx3Cache("F1_FILIAL","X3_TAMANHO"))
	
	
	If Empty(cNF) .AND. Empty(cSerie)
		MsgInfo("Pedido de Venda n�o Faturado! Favor faturar para realizar o retorno", "Hope")
	else
		
	aAdd(aParam,{1,"Filial"		,cFilOpc	,GetSx3Cache("F1_FILIAL","X3_PICTURE"),"existCpo('SM0')","SM0",".T.",50,.F.})
	If ParamBox(aParam,"Filial",@aRetParm,{||.T.},,,,,,"Filial",.T.,.T.)
			lOk	:= .T.
			cFilOpc := aRetParm[1]
			
			Sf2Trans(cFilOpc)
		else
			MsgAlert("Cancelado pelo usuario","Hope")
			return
		endif
	endif
Return

Static Function Sf2Trans(cFilOpc)
	Local c_Filial		:= SC5->C5_FILIAL
	Local cSerie		:= SC5->C5_SERIE
	Local cNum			:= SC5->C5_NOTA
	Local dEmissao		:= SC5->C5_EMISSAO
	Local cCliente		:= SC5->C5_CLIENT
	Local cLoja			:= SC5->C5_LOJACLI
	Local cCgcSA2
	Local lOk			:= .F.
	Local cCodFrn
	Local cLojFrn
	Local cFilOld
	Local nOpc
	Local lVersao		:= (VAL(GetVersao(.F.)) == 11 .And. GetRpoRelease() >= "R6" .Or. VAL(GetVersao(.F.)) > 11)
	Local nBakMod		:= nModulo
	Local cBakMod		:= cModulo
	Local cBackFunName	:= FUNNAME()

	Private aCabec		:= {}
	Private aItens		:= {}
	Private aLinha		:= {}
	Private lMsErroAuto	:= .F.

	nModulo	:= 5
	cModulo	:= "FAT"
	
	cFilDest := cFilOpc
	
	cCgcSA2	:= FWArrFilAtu(SM0->M0_CODIGO, cFilant)[18]

	//Localiza o Fornecedor:
	//Caso exista o Fornecedor da-se prosseguimento, caso contrario nao sera criado a Pre-Nota.
	SA2->(dbSetOrder(3))

	If SA2->(dbSeek(xFilial("SA2")+cCgcSA2))
		lOk		:= .T.
		cCodFrn	:= SA2->A2_COD
		cLojFrn	:= SA2->A2_LOJA
	Else
		lOk		:= .F.
		MsgAlert("Pre-Nota nao sera criada por nao existir Fornecedor cadastrado com o CNPJ " + cCgcSA2 + ".","Hope")
	EndIf

	//ALERT ("Fornecedor: " + cCodFrn)

	If lOk
		//	#########################################################################################
		//	 Array do cabecalho para Pre-Nota
		//	#########################################################################################
		aAdd(aCabec,	{'F1_FILIAL'		,cFilDest		,NIL})
		aAdd(aCabec,	{'F1_TIPO'			,'N'			,NIL})
		aAdd(aCabec,	{'F1_FORMUL'		,'N'			,NIL})
		aAdd(aCabec,	{'F1_DOC'			,cNum    		,NIL})
		aAdd(aCabec,	{'F1_SERIE'			,cSerie		    ,NIL})
		aAdd(aCabec,	{'F1_EMISSAO'		,dEmissao		,NIL})
		aAdd(aCabec,	{'F1_FORNECE'		,cCodFrn		,NIL})
		aAdd(aCabec,	{'F1_LOJA'			,cLojFrn		,NIL})
		aAdd(aCabec,	{'F1_ESPECIE'		,"SPED"	 	    ,NIL})

		DbSelectArea("SC6")
		SC6->(DbSetOrder(4))	//C6_FILIAL+C6_NOTA+C6_SERIE
		SC6->(DbSeek(c_Filial+cNum+cSerie))
		//Posiciona o cFilAnt para a Filial de destino.
		//Essa informacao e necessaria, pois se nao informado o sistema cria os itens (SD1) para a Filial de Origem
		cFilOld		:= cFilAnt
		cFilAnt		:= cFilDest

		While !SC6->(Eof()) .AND. c_Filial+cNum+cSerie+cCliente+cLoja==SC6->(C6_FILIAL+C6_NOTA+C6_SERIE+C6_CLI+C6_LOJA)
			aLinha := {}
			//	 Array dos itens para Pre-Nota
			aAdd(aLinha,	{'D1_FILIAL'	,cFilDest								,NIL})
			aAdd(aLinha,	{'D1_COD'		,SC6->C6_PRODUTO						,NIL})
			aAdd(aLinha,	{'D1_QUANT'		,SC6->C6_QTDVEN							,NIL})
			aAdd(aLinha,	{'D1_VUNIT'		,SC6->C6_PRCVEN							,NIL})
			aAdd(aLinha,	{'D1_TOTAL'		,SC6->C6_VALOR							,NIL})
			aAdd(aLinha,	{'D1_LOCAL'		,'H2'									,NIL})			
			AAdd(aItens, aLinha)

			SB2->(dbSetOrder(1))

			If (!SB2->(DbSeek(xFilial("SB2")+SC6->C6_PRODUTO+SC6->C6_LOCAL)))
				CriaSB2(SC6->C6_PRODUTO,SC6->C6_LOCAL,cFilDest)
			EndIf

			If lVersao
				NNR->(dbSetOrder(1))
				NNR->(DBGOTOP())
				If (!NNR->(DbSeek(xFilial("SC6")+SC6->C6_LOCAL)))
					AGRA045(;
						{{"NNR_CODIGO",SC6->C6_LOCAL,NIL};
						,{"NNR_DESCRI","Armazem "+SC6->C6_LOCAL,NIL};
						};
						,3,{},{})
				EndIf
			EndIf
			SB2->(MsUnLock())
			SC6->(dbSkip())
		EndDo
		
		SC6->(dbCloseArea())

		//Opcao para Inclusao da ExecAuto Mata140: 3
		nOpc := 3
		//Rotina de criacao automatica para cria Pre-Nota
		
		MSExecAuto({|x,y,z| MATA140(x,y,z)}, aCabec, aItens, nOpc)

		//Retorna o cFilAnt para a Filial de origem.
		cFilAnt := cFilOld

		If lMsErroAuto
			AutoGrLog("Nao foi possivel criar a pre-nota na Filial de Destino!")
			Mostraerro()
		Else
			MsgInfo("Pre-Nota numero " + cNum + " criada com sucesso, para a Filial " + cFilDest + ".")
		EndIf
	EndIf
	
	nModulo	:= nBakMod
	cModulo	:= cBakMod
	
Return