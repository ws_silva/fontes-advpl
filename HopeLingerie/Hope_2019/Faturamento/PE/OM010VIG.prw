#include 'protheus.ch'
#include 'parmtype.ch'

user function OM010VIG()

Local lRet := .F. 
Local cDataTab := DA0->DA0_DATATE
Local cDataPed := SC5->C5_EMISSAO
Local _area         := GetArea()


if cDataTab >= cDataPed

	lRet := .T.

else 

	MsgAlert("Tabela fora da data da virgencia","HOPE")

Endif

RestArea(_area)

return lRet