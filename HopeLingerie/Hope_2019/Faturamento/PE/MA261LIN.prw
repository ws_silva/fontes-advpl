User Function MA261LIN( )
Local aArea  := GetArea()
Local lRet   := .T.
Local nLinha := PARAMIXB[1]  // numero da linha do aCols
Local cProd  := aCols[nLinha][1]
Local cAmz   := aCols[nLinha][4]
Local nQtde  := aCols[nLinha][16]
Local nDisp  := 0
          
If AllTrim(cAmz) $ "E0/E1"
	DbSelectArea("SB2")
	DbSetOrder(1)
	If DbSeek(xFilial("SB2")+cProd+cAmz)
		nDisp := SB2->B2_QATU-(SB2->B2_QEMP+SB2->B2_RESERVA+SB2->B2_XRESERV)
		
		If nQtde > nDisp
			MsgAlert("A quantidade a transferir � maior que a quantidade dispon�vel.","ATEN��O")
			lRet := .F.
		EndIf  
	EndIf
EndIf	

RestArea(aArea)

Return lRet