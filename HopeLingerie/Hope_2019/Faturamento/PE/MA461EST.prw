//Bibliotecas
#Include 'Protheus.ch'
#Include 'RwMake.ch'
 
/*------------------------------------------------------------------------------------------------------*
 | P.E.:  MA461EST                                                                                      |
 | Desc:  Valida estorno da libera��o de Estoque no Doc.Sa�da                                           |
 | Links: http://tdn.totvs.com/pages/releaseview.action�pageId=6784293                                  |
 *------------------------------------------------------------------------------------------------------*/
 
User Function MA461EST()
    Local aArea := GetArea()
    Local aAreaC9 := SC9->(GetArea())
    Local aAreaC6 := SC6->(GetArea())
    Local aAreaC5 := SC5->(GetArea())
    Local lRet    := .T.
     
    lRet := MsgYesNo("Deseja estornar� Pedido "+SC9->C9_PEDIDO, "Aten��o")
     
    RestArea(aAreaC5)
    RestArea(aAreaC6)
    RestArea(aAreaC9)
    RestArea(aArea)
Return lRet