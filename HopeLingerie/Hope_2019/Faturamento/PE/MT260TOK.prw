#INCLUDE "RWMAKE.CH"

User Function MT260TOK
Local aArea := GetArea()
Local lRet  := .T.
Local nDisp := 0
cLoteDigiD  := PARAMIXB[1]	   

If AllTrim(CLOCORIG) $ "E0/E1"
	DbSelectArea("SB2")
	DbSetOrder(1)
	If DbSeek(xFilial("SB2")+CCODORIG+CLOCORIG)
		nDisp := SB2->B2_QATU-(SB2->B2_QEMP+SB2->B2_RESERVA+SB2->B2_XRESERV)
		
		If NQUANT260 > nDisp
			MsgAlert("A quantidade a transferir � maior que a quantidade dispon�vel.","ATEN��O")
			lRet := .F.
		EndIf  
	EndIf
EndIf	

RestArea(aArea)

Return lRet