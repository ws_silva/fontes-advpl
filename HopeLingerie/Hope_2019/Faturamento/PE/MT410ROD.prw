#include 'protheus.ch'
#include 'parmtype.ch'

/*--------------------------------------------------------------------------------------------------------------*
 | P.E.:  MT410ROD                                                                                              |
 | Desc:  Fun��o para alterar os dados do rodap� do pedido                                                      |
 | Link:  http://tdn.totvs.com/pages/releaseview.action?pageId=6784352                                          | 
 | Author: Weskley Silva                                                                                        |
 *--------------------------------------------------------------------------------------------------------------*/
 

user function MT410ROD()

    Local aArea     := GetArea()
    Local oObjOrig  := ParamIXB[1]
    Local cDescCli  := ParamIXB[2]
    Local nValBruto := ParamIXB[3]
    Local nValDescA := ParamIXB[4]
    Local nValLiq   := ParamIXB[5]
      
    //Altera as descri��es
     Eval(oObjOrig,cDescCli, Iif(nValLiq > nValBruto, nValLiq - (nValDescA),nValBruto),nValDescA,nValLiq)
     
    RestArea(aArea)
Return