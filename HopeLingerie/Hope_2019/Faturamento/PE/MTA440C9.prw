/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � MTA440C9 �Autor  �Daniel R. Melo      � Data �  12/06/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Ponto de entrada durante a liberacao do pedido de venda     ���
���          �Utilizado para atualizar o campo C9_XBLQ                    ���
�������������������������������������������������������������������������͹��
���Uso       �Especifico HOPE.                                            ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function MTA440C9()

	Local aArea := GetArea()
	Local cQuery := ""
	Local cNumPF := ""


	IF SELECT("TMP") > 0
		TMP->(DbCloseArea())
	ENDIF

	cQuery := " SELECT TOP 1 C9_XNUMPF FROM "+RetSqlName("SC9")+" (NOLOCK) WHERE C9_NFISCAL ='"+Alltrim(SF2->F2_DOC)+"' AND D_E_L_E_T_ = '*' "
	cQuery += "AND C9_PRODUTO ='"+Alltrim(SC6->C6_PRODUTO)+"' AND C9_XNUMPF <> ''"

	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.F.,.T.)

	If Funname() == Alltrim("HESTA003")
		cNumPF := MV_PAR01 //SZJ->ZJ_NUMPF
	ElseIf Funname() == Alltrim("MATA521A")	//.OR. Funname() == Alltrim("MATA461")
		cNumPF := TMP->C9_XNUMPF
	EndIf

	DbSelectArea('SC9')
	DbSetOrder(1) // SC9_FILIAL, C9_PEDIDO,i C9_ITEM, C9_SEQUEN, C9_PRODUTO, R_E_C_N_O_, D_E_L_E_T_

	If DbSeek(xFilial('SC9')+SC5->C5_NUM+SC6->C6_ITEM+SC9->C9_SEQUEN+SC6->C6_PRODUTO)

		While SC9->(!EOF()) .and. SC9->C9_PEDIDO = SC5->C5_NUM .AND. EMPTY(SC9->C9_NFISCAL)


			If (ALLTRIM(SC5->C5_ORIGEM)='MANUAL' .AND. (SC5->C5_TIPO$'B/D' .OR. SC5->C5_TPPED $ GETMV("MV_HTPPED") .OR. SC5->C5_POLCOM $ GETMV("MV_HPOLCOM"))) .OR. SC5->C5_XBLQ == 'L'

				RecLock('SC9', .F.)
				SC9->C9_XBLQ   := SC5->C5_XBLQ
				SC9->C9_YORIGEM   := Funname()
				If EMPTY(SC9->C9_XNUMPF) .AND. EMPTY(SC9->C9_NFISCAL)
					SC9->C9_XNUMPF := cNumPF
				EndIF
				MsUnLock()

				If SC6->C6_ENTREG == Ctod("  /  /    ")
					RecLock('SC9', .F.)
					SC9->C9_DATENT := SC5->C5_FECENT
					MsUnLock()
				EndIf

			Else
				RecLock('SC9', .F.)
				SC9->C9_XBLQ   := SC5->C5_XBLQ
				SC9->C9_YORIGEM   := Funname()
				If EMPTY(SC9->C9_XNUMPF) .AND. EMPTY(SC9->C9_NFISCAL)
					SC9->C9_XNUMPF := cNumPF
				EndIF
				MsUnLock()

			EndIf

			SC9->(DbSkip())

		EndDo

	EndIf

	RestArea(aArea)

Return .T.