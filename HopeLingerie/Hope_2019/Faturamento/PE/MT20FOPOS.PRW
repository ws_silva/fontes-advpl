#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �MT20FOPOS �Autor  �DANIEL R. MELO      � Data �  15/12/2016 ���
�������������������������������������������������������������������������͹��
���Desc.     �PEs executados na chamada da Inclusao/alteracao/Exclus�o    ���
���          �no Cadastro de fornecedores para inclus�o do Item Cont�bil. ���
�������������������������������������������������������������������������͹��
���Uso       �(PEs) MAT20FOPOS - Cadastro de fornecedores                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User function MT20FOPOS()

nOpcA			:= PARAMIXB[1]  //3=Incluir / 4=Alterar / 5=Excluir

If nOpcA==3

	dbSelectArea("CTD")
	("CTD")->(dbSetOrder(1))
	Dbseek(xFilial("CTD")+"F"+SA2->A2_COD+SA2->A2_LOJA)

	If !Found()
	
		RecLock("CTD",.T.)

			CTD_FILIAL	:= xFilial("CTD")					//Filial		
			CTD_ITEM	:= "2"+SA2->A2_COD+SA2->A2_LOJA			 		//Item Conta		
			CTD_CLASSE	:= '2'								//Classe				1=Sintetico;2=Analitico
			CTD_NORMAL	:= '1'								//Cond Normal			0=Nenhum;1=Despesa;2=Receita
			CTD_DESC01	:= SA2->A2_NOME						//Desc Moeda 1		
			CTD_BLOQ	:= "2"								//Item Bloq				1=Bloqueado;2=N�o Bloqueado
			CTD_DTEXIS	:= cToD("01/01/2000")				//Dt Ini Exist		
	
		MsUnLock()

	EndIf

EndIf

Return(.t.)