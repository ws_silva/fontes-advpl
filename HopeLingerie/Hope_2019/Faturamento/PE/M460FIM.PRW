#include "rwmake.ch" 
#INCLUDE "TOPCONN.CH"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

User Function M460FIM()

	local _area := GetArea()
	
	//Chama a rotina para atualizar os dados da expedi��o da NF.
	If GetEnvServer() != "JOBFAT"
		U_HFT003EXP(.T.)
	EndIf

	//Chama a rotina para atualizar os dados de Exporta��o da NF.
    If SF2->F2_EST == 'EX'
		NWAtualCDL()
	EndIF

	// Calcular comissoes - TOTALIT Solutions 09/2019
	U_RFATM01("F")
	
	RestArea(_area)

Return


Static Function NWAtualCDL()

     ////////////////////////////////////////////////////////////////////////////////////////////////// 
     ////////////////////////////////////////////////////////////////////////////////////////////////// 
     //                                                                                              // 
     //      Data:      02/02/2018                                                                   //
     //      Autor:     Daniel R. Melo                                                               // 
     //      Fun��o:    Alimenta a Tabela CDL, quando for exporta��o, para o Sefaz Autorizar a Nota  // 
     //      Altera��o: ACTUAL TREND / HOPE LINGERIE                                                 // 
     //                                                                                              // 
     ////////////////////////////////////////////////////////////////////////////////////////////////// 
     ////////////////////////////////////////////////////////////////////////////////////////////////// 

	Local cNota       := SF2->F2_DOC
	Local cSerie      := SF2->F2_SERIE
	Local cCliente    := SF2->F2_CLIENTE
	Local cLoja       := SF2->F2_LOJA
	Local _cUFemb     := "CE"
	Local _cLocEmb    := "FORTALEZA"
	Local _LocDes     := "AEROPORTO PINTO MARTINS"

	dbSelectArea("CDL")
	CDL->(dbSetOrder(1))  //Filial+Serie+Num NF+Cliente+Loja
	CDL->(dbSeek(xFilial("CDL")+cSerie+cNota+cCliente+cLoja))

	If !CDL->(Found())    //Se n�o encontrou registro com aquela Nota e Serie

		dbSelectArea("SD2")
		SD2->(dbSetOrder(3)) //Filia, Nota, Serie, Cliente, Loja, Codigo do Prod., Item
		SD2->(dbSeek(xFilial("SD2")+cNota+cSerie+cCliente+cLoja))

		While !SD2->(EOF()) .AND. SD2->D2_DOC == cNota .AND. SD2->D2_SERIE == cSerie .AND. SD2->D2_CLIENTE = cCliente .AND. SD2->D2_LOJA == cLoja

			RecLock("CDL",.T.) //Inclus�o
			Replace CDL_FILIAL         With xFilial("CDL")
			Replace CDL_DOC            With cNota
			Replace CDL_SERIE          With cSerie
			Replace CDL_ESPEC          With "SPED"
			Replace CDL_CLIENT         With cCliente
			Replace CDL_LOJA           With cLoja
            Replace CDL_PAIS           With SUBSTR(SA1->A1_CODPAIS,2,4) 
			Replace CDL_UFEMB          With _cUFemb
			Replace CDL_LOCEMB         With _cLocEmb
            Replace CDL_QTDEXP         With SD2->D2_QUANT
			Replace CDL_PRODNF         With SD2->D2_COD			
			Replace CDL_ITEMNF         With SD2->D2_ITEM
			Replace CDL_LOCDES         With _LocDes
			Replace CDL_INDDOC         With '0'
			Replace CDL_DTDE           With ddatabase
			Replace CDL_NATEXP         With '0'
			Replace CDL_DTREG          With ddatabase
			Replace CDL_CHCEMB         With '1'
			Replace CDL_DTCHC          With ddatabase
			Replace CDL_DTAVB          With ddatabase
			Replace CDL_TPCHC          With '01'
			Replace CDL_NRMEMO         With '11'
			Replace CDL_EMIEXP         With ddatabase
			Replace CDL_SDOC           With '2'
			CDL->(MsUnlock())

			SD2->(dbSkip())

		EndDo

		SD2->(dbCloseArea())

	Else

		dbSelectArea("SD2")
		SD2->(dbSetOrder(3))   //Filia, Nota, Serie, Cliente, Loja, Codigo do Prod., Item
		SD2->(dbSeek(xFilial("SD2")+cNota+cSerie+cCliente+cLoja))
		While !SD2->(EOF()) .AND. SD2->D2_DOC == cNota .AND. SD2->D2_SERIE == cSerie .AND. SD2->D2_CLIENTE = cCliente .AND. SD2->D2_LOJA == cLoja

			RecLock("CDL",.F.) //Inclus�o
			Replace CDL_FILIAL         With xFilial("CDL")
			Replace CDL_DOC            With cNota
			Replace CDL_SERIE          With cSerie
			Replace CDL_ESPEC          With "SPED"
			Replace CDL_CLIENT         With cCliente
			Replace CDL_LOJA           With cLoja
            Replace CDL_PAIS           With SA1->A1_CODPAIS 
			Replace CDL_UFEMB          With _cUFemb
			Replace CDL_LOCEMB         With _cLocEmb
            Replace CDL_QTDEXP         With SD2->D2_QUANT 
			Replace CDL_PRODNF         With SD2->D2_COD
			Replace CDL_ITEMNF         With SD2->D2_ITEM
			Replace CDL_LOCDES         With _LocDes
			Replace CDL_INDDOC         With '0'
			Replace CDL_DTDE           With ddatabase
			Replace CDL_NATEXP         With '0'
			Replace CDL_DTREG          With ddatabase
			Replace CDL_CHCEMB         With '1'
			Replace CDL_DTCHC          With ddatabase
			Replace CDL_DTAVB          With ddatabase
			Replace CDL_TPCHC          With '01'
			Replace CDL_NRMEMO         With '11'
			Replace CDL_EMIEXP         With ddatabase
			Replace CDL_SDOC           With '2'
			CDL->(MsUnlock())

			SD2->(dbSkip())

		EndDo

		SD2->(dbCloseArea())

	EndIf

	CDL->(dbCloseArea())

Return