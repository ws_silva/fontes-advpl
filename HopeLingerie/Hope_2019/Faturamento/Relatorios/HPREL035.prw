#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

/*/{Protheus.doc} HPREL035 
RELATÓRIO DE ENTRADAS - SIGET
@author Ronaldo Pereira
@since 20/09/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL035()

Private oReport
Private cPergCont	:= 'HPREL035' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Ronaldo Pereira
@since 20 de Setembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
	
Static Function ReportDef()

	Local oReport
	Local oSection1

	oReport := TReport():New( 'RDE', 'RELATÓRIO DE ENTRADAS - SIGET', , {|oReport| ReportPrint( oReport ), 'RELATÓRIO DE ENTRADAS - SIGET' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATÓRIO DE ENTRADAS - SIGET', {'RDE','SF3','SA2'})
			
	TRCell():New( oSection1, 'FILIAL'	    	    ,'RDE', 		'FILIAL',							"@!"                        ,04)
	TRCell():New( oSection1, 'ENTRADA'				,'RDE', 		'ENTRADA',       					"@!"		                ,15)
	TRCell():New( oSection1, 'NFISCAL'				,'RDE', 	    'NFISCAL',       					"@!"		                ,15)
	TRCell():New( oSection1, 'SERIE' 		    	,'RDE', 		'SERIE',              				"@!"						,03)
	TRCell():New( oSection1, 'COD_FORNEC' 		    ,'RDE', 		'COD_FORNEC',              			"@!"						,06)
	TRCell():New( oSection1, 'RAZAO'         		,'RDE', 		'RAZAO',			           		"@!"						,60)
	TRCell():New( oSection1, 'LOJA'         		,'RDE', 		'LOJA',			           			"@!"						,04)
	TRCell():New( oSection1, 'CNPJ'					,'RDE', 		'CNPJ',       						"@!"		                ,15)
	TRCell():New( oSection1, 'UF'	        		,'RDE', 		'UF',	   			        		"@!"						,02)
	TRCell():New( oSection1, 'COD_FISCAL'      		,'RDE', 		'COD_FISCAL',      					"@"				    		,04)
	TRCell():New( oSection1, 'EMISSAO'      		,'RDE', 		'EMISSAO',      					"@"				    		,15)
	TRCell():New( oSection1, 'ALIQ_ICMS' 			,'RDE', 	    'ALIQ_ICMS',     					"@E 999,99"		    	    ,15)
	TRCell():New( oSection1, 'VLR_CONTABIL' 		,'RDE', 	    'VLR_CONTABIL',     				"@E 999,99.99"		    	,15)
	TRCell():New( oSection1, 'BASE_ICMS' 			,'RDE', 	    'BASE_ICMS',     					"@E 999,99.99"		    	,15)
	TRCell():New( oSection1, 'VLR_ICMS' 			,'RDE', 	    'VLR_ICMS',     					"@E 999,99.99"		    	,15)
	TRCell():New( oSection1, 'CHAVE_NFE'      		,'RDE', 		'CHAVE_NFE',      					"@"				    		,60)
		
Return( oReport )
	
//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Ronaldo Pereira
@since 20 de Setembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("RDE") > 0
		RDE->(dbCloseArea())
	Endif 
      
    cQuery := " SELECT F3.F3_FILIAL AS FILIAL, "
    cQuery += " CONVERT(CHAR, CAST(F3.F3_ENTRADA AS SMALLDATETIME), 103) AS ENTRADA, "
    cQuery += " F3.F3_NFISCAL AS NFISCAL, "
    cQuery += " F3.F3_SERIE AS SERIE, "
	cQuery += " F3.F3_TIPO, "
    cQuery += " F3.F3_CLIEFOR AS COD_FORNEC, "
    cQuery += " CASE F3.F3_TIPO WHEN 'N' THEN A1.A1_NOME ELSE A2.A2_NOME END AS RAZAO, "
	cQuery += " F3.F3_LOJA AS LOJA, "
    cQuery += " CASE F3.F3_TIPO WHEN 'N' THEN A1.A1_CGC ELSE A2.A2_CGC END AS CNPJ, "
    cQuery += " F3.F3_ESTADO AS UF, "
    cQuery += " F3.F3_CFO AS COD_FISCAL, "
    cQuery += " CONVERT(CHAR, CAST(F3.F3_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQuery += " F3.F3_ALIQICM AS ALIQ_ICMS, "
    cQuery += " F3.F3_VALCONT AS VLR_CONTABIL, "
    cQuery += " F3.F3_BASEICM AS BASE_ICMS, "
    cQuery += " F3.F3_VALICM AS VLR_ICMS, "
    cQuery += " F3.F3_CHVNFE AS CHAVE_NFE "
    cQuery += " FROM "+RetSqlName("SF3")+" F3 WITH (NOLOCK) "
    cQuery += " INNER JOIN "+RetSqlName("SA1")+" A1 WITH (NOLOCK) ON (A1.A1_COD = F3.F3_CLIEFOR AND A1.D_E_L_E_T_ = '') "
    cQuery += " LEFT JOIN "+RetSqlName("SA2")+" A2 WITH (NOLOCK) ON (A2.A2_COD = F3.F3_CLIEFOR AND A2.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SF1")+" F1 WITH (NOLOCK) ON (F1.F1_FILIAL = F3.F3_FILIAL AND F1.F1_DOC = F3.F3_NFISCAL AND F1.F1_FORNECE = F3.F3_CLIEFOR AND F1.D_E_L_E_T_ = '') "
    cQuery += " WHERE F3.D_E_L_E_T_ = '' AND LEFT(F3.F3_CFO, 1) <= '4' AND F3.F3_ENTRADA BETWEEN '"+ DTOS(MV_PAR01) +"'  AND '"+ DTOS(MV_PAR02) + "' "

    IF !EMPTY(MV_PAR03)	
		cQuery += " AND F3.F3_NFISCAL BETWEEN  '"+MV_PAR03+"' AND '"+MV_PAR04+"' "	
	ENDIF

    cQuery += " UNION ALL " 

    cQuery += " SELECT F3.F3_FILIAL AS FILIAL, "
    cQuery += " CONVERT(CHAR, CAST(F3.F3_ENTRADA AS SMALLDATETIME), 103) AS ENTRADA, "
    cQuery += " F3.F3_NFISCAL AS NFISCAL, "
    cQuery += " F3.F3_SERIE AS SERIE, "
	cQuery += " F3.F3_TIPO, "
    cQuery += " F3.F3_CLIEFOR AS COD_FORNEC, "
    cQuery += " CASE F3.F3_TIPO WHEN 'N' THEN A1.A1_NOME ELSE A2.A2_NOME END AS RAZAO, "
	cQuery += " F3.F3_LOJA AS LOJA, "
    cQuery += " CASE F3.F3_TIPO WHEN 'N' THEN A1.A1_CGC ELSE A2.A2_CGC END AS CNPJ, "
    cQuery += " F3.F3_ESTADO AS UF, "
    cQuery += " F3.F3_CFO AS COD_FISCAL, "
    cQuery += " CONVERT(CHAR, CAST(F3.F3_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQuery += " F3.F3_ALIQICM AS ALIQ_ICMS, "
    cQuery += " F3.F3_VALCONT AS VLR_CONTABIL, "
    cQuery += " F3.F3_BASEICM AS BASE_ICMS, "
    cQuery += " F3.F3_VALICM AS VLR_ICMS, "
    cQuery += " F3.F3_CHVNFE AS CHAVE_NFE "
    cQuery += " FROM "+RetSqlName("SF3")+" F3 WITH (NOLOCK) "
    cQuery += " INNER JOIN "+RetSqlName("SA1")+" A1 WITH (NOLOCK) ON (A1.A1_COD = F3.F3_CLIEFOR AND A1.D_E_L_E_T_ = '') "
    cQuery += " LEFT JOIN "+RetSqlName("SA2")+" A2 WITH (NOLOCK) ON (A2.A2_COD = F3.F3_CLIEFOR AND A2.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SF2")+" F2 WITH (NOLOCK) ON (F2.F2_FILIAL = F3.F3_FILIAL AND F2.F2_DOC = F3.F3_NFISCAL AND F2.F2_CLIENTE = F3.F3_CLIEFOR AND F2.D_E_L_E_T_ = '') "
    cQuery += " WHERE F3.D_E_L_E_T_ = '' AND LEFT(F3.F3_CFO, 1) <= '4' AND F3.F3_ENTRADA BETWEEN '"+ DTOS(MV_PAR01) +"'  AND '"+ DTOS(MV_PAR02) + "' "
    
    IF !EMPTY(MV_PAR03)	
		cQuery += " AND F3.F3_NFISCAL BETWEEN  '"+MV_PAR03+"' AND '"+MV_PAR04+"' "	
	ENDIF
    
       	
	TCQUERY cQuery NEW ALIAS RDE
	
	While RDE->(!EOF())
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(RDE->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection1:Cell("ENTRADA"):SetValue(RDE->ENTRADA)
		oSection1:Cell("ENTRADA"):SetAlign("LEFT")		
					
		oSection1:Cell("NFISCAL"):SetValue(RDE->NFISCAL)
		oSection1:Cell("NFISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("SERIE"):SetValue(RDE->SERIE)
		oSection1:Cell("SERIE"):SetAlign("LEFT")
		
		oSection1:Cell("COD_FORNEC"):SetValue(RDE->COD_FORNEC)
		oSection1:Cell("COD_FORNEC"):SetAlign("LEFT")
		
		oSection1:Cell("RAZAO"):SetValue(RDE->RAZAO)
		oSection1:Cell("RAZAO"):SetAlign("LEFT")
		
		oSection1:Cell("LOJA"):SetValue(RDE->LOJA)
		oSection1:Cell("LOJA"):SetAlign("LEFT")
		
		oSection1:Cell("CNPJ"):SetValue(Transform(RDE->CNPJ, "@R 99.999.999/9999-99"))
		oSection1:Cell("CNPJ"):SetAlign("LEFT")
				
		oSection1:Cell("UF"):SetValue(RDE->UF)
		oSection1:Cell("UF"):SetAlign("LEFT")
		
		oSection1:Cell("COD_FISCAL"):SetValue(RDE->COD_FISCAL)
		oSection1:Cell("COD_FISCAL"):SetAlign("LEFT")
		
		oSection1:Cell("EMISSAO"):SetValue(RDE->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")
				
		oSection1:Cell("ALIQ_ICMS"):SetValue(RDE->ALIQ_ICMS)
		oSection1:Cell("ALIQ_ICMS"):SetAlign("LEFT")
		
		oSection1:Cell("VLR_CONTABIL"):SetValue(RDE->VLR_CONTABIL)
		oSection1:Cell("VLR_CONTABIL"):SetAlign("LEFT")
				
		oSection1:Cell("BASE_ICMS"):SetValue(RDE->BASE_ICMS)
		oSection1:Cell("BASE_ICMS"):SetAlign("LEFT")
		
		oSection1:Cell("VLR_ICMS"):SetValue(RDE->VLR_ICMS)
		oSection1:Cell("VLR_ICMS"):SetAlign("LEFT")
		
		oSection1:Cell("CHAVE_NFE"):SetValue(RDE->CHAVE_NFE)
		oSection1:Cell("CHAVE_NFE"):SetAlign("LEFT")
										
		oSection1:PrintLine()
		
		RDE->(DBSKIP()) 
	enddo
	RDE->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Ronaldo Pereira
@since 20 de Setembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Data de ?"		        		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data ate ?"                     ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Nota Fiscal de ?"		        ,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Nota Fiscal ate ?"		        ,""		,""		,"mv_ch4","C",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")		
	
return