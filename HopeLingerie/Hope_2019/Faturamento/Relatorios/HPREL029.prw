#Include "TopConn.CH"
#Include "Protheus.CH"
#Include "TOTVS.CH"
#Include "RWMAKE.CH"
#include 'parmtype.ch'
#INCLUDE "TBICONN.CH" 
#INCLUDE "TBICODE.CH"
#INCLUDE "FWMBROWSE.CH"
#INCLUDE "FWMVCDEF.CH" 
#INCLUDE 'FONT.CH'
#INCLUDE 'COLORS.CH'       
#INCLUDE "RPTDEF.CH"
#INCLUDE "FWPrintSetup.ch"


user function HPREL029(_nFiscal,_nSerie,_nCliente,_nLoja)


	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local oPrinter
	Local cLocal          := "c:\TEMP\"				
	Local oFont1 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont2 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Local oFont3 	:= TFont():New( "Arial",,16,,.T.,,,,,  .F. )
	Local cFilePrint := ""
	Local nLin   := 100
	Local nTmLin := 10
	Local nSalto := 10
	Local cLinha := ''
	
	
	//Private cPergCont	:= 'HPREL013' 

	************************
	*Monta pergunte do Log *
	************************
	//AjustaSX1(cPergCont)
	//If !Pergunte(cPergCont, .T.)
	//	Return
	//Endif
	
	U_HPREL1(_nFiscal,_nSerie,_nCliente,_nLoja)			
	
Return

User Function HPREL1(_nFiscal,_nSerie,_nCliente,_nLoja)

	Local lAdjustToLegacy := .F.
	Local lDisableSetup  := .F.
	Local cLocal          := "c:\TEMP\"					
	Local cFilePrint := "Romaneio_faturamento.pdf"
	Local xArea := GetArea()	
	Local cLogo := "\system\logohope.png"
	Local cLinha := ""
	Local _nFiscal
	Local _nSerie
	Local _nCliente
	Local _nLoja
	Private oPrinter
	Private oFont1 	:= TFont():New( "Arial",,11,,.T.,,,,,  .F. )
	Private oFont2 	:= TFont():New( "Arial",,10,,.F.,,,,,  .F. )
	Private oFont3 	:= TFont():New( "Arial",,14,,.T.,,,,,  .F. )
	Private nQuantNf := 0
	Private nValNF := 0
	Private nCont := 0
	Private nLin   := 25
	Private nTmLin := 15
	Private nSalto := 20	
	
	if Select("T02") > 0
		T02->(dbCloseArea())
	endif

	cQuery := " SELECT D2_DOC,D2_SERIE,D2_CLIENTE,D2_LOJA  " 
	cQuery += " FROM "+ RetSqlName("SD2") +" SD2 (NOLOCK) JOIN "+RetSqlname("SB1")+ " SB1 (NOLOCK) ON (B1_COD = D2_COD AND SB1.D_E_L_E_T_ = '' ) "
	cQuery += " WHERE SD2.D_E_L_E_T_ = '' AND D2_DOC = '"+Alltrim(_nFiscal)+"' AND D2_SERIE = '"+Alltrim(_nSerie)+"' AND D2_CLIENTE = '"+Alltrim(_nCliente)+"' AND D2_LOJA = '"+Alltrim(_nLoja)+"' " 
		
	cQuery += "  GROUP BY SD2.D2_DOC,D2_SERIE,D2_CLIENTE,D2_LOJA "
	
	
	TcQuery cQuery New Alias T02
	
	
	oPrinter := FWMSPrinter():New(cFilePrint, IMP_SPOOL, lAdjustToLegacy,cLocal, lDisableSetup, , , , , , .F., )
	oPrinter:SetPortrait()
	oPrinter:StartPage()	
	
	While !T02->(EOF()) 
	
	
	IF nLin >= 750
	   nLin   := 25
	   oPrinter:EndPage()
	   oPrinter:StartPage()
	ENDIF
	

	ProcRegua(0)

	oPrinter:Say ( nLin, 200, "ROMANEIO DE ENTREGA",  oFont3 )
	oPrinter:SayBitmap( nLin-010, 050, cLogo, 100, 058)
	nLin := nLin + nTmLin
		
	cNF := T02->D2_DOC
	cSerie :=T02->D2_SERIE
	cCliente := T02->D2_CLIENTE
	cLoja := T02->D2_LOJA
	dData := date() 

	oPrinter:Say ( nLin, 200, "Nome Cliente: ", oFont3 )
	oPrinter:Say ( nLin, 300, SUBSTR(ALLTRIM(POSICIONE("SA1",1,XFILIAL("SA1")+cCliente+cLoja ,"A1_NREDUZ")),0,50), oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Nota Fiscal:  ", oFont3 )
	oPrinter:Say ( nLin, 300, cNF , oFont2 )
	nLin := nLin + nTmLin 
	
	oPrinter:Say ( nLin, 200, "Data ", oFont3 )
	oPrinter:Say ( nLin, 250, Dtoc(dData), oFont2 )
	nLin := nLin + nTmLin 
		
	
	if Select("T01") > 0
		T01->(dbCloseArea())
	endif

	cQuery := " SELECT B1_CODBAR AS BARRA,D2_COD AS COD,BV_CHAVE AS COD_COR,BV_DESCRI AS COR,REPLACE(SUBSTRING(B1_COD,13,3),'00','') AS TAMANHO,B1_DESC AS DESCR,D2_SERIE,D2_PRCVEN AS VALOR,D2_QUANT AS QTD,D2_DOC " 
	cQuery += " FROM "+ RetSqlName("SD2") +" SD2 (NOLOCK) JOIN "+RetSqlname("SB1")+ " SB1 (NOLOCK) ON (B1_COD = D2_COD AND SB1.D_E_L_E_T_ = '' ) "
	cQuery += " JOIN "+ RetSqlName("SBV") +" (NOLOCK) ON SUBSTRING(B1_COD,9,3) = BV_CHAVE AND BV_TABELA = 'COR' AND SBV010.D_E_L_E_T_ = '' AND "
	cQuery += " SD2.D_E_L_E_T_ = '' AND D2_DOC = '"+cNF+"' AND D2_SERIE = '"+cSerie+"' " 		
	cQuery += "  GROUP BY SD2.D2_DOC,B1_CODBAR,D2_COD,BV_CHAVE,BV_DESCRI,B1_COD,B1_DESC,D2_PRCVEN,SD2.D2_QUANT,D2_SERIE "
	
	TcQuery cQuery New Alias T01	
	
	nLin := nLin + 25
	oPrinter:Say ( nLin, 20,  "BARRA",       			oFont1 )
	oPrinter:Say ( nLin, 80,  "COD PRODUTO",  			oFont1 )
	oPrinter:Say ( nLin, 165, "COR",   					oFont1 )
	oPrinter:Say ( nLin, 230, "DESCRICAO DO MATERIAL", 	oFont1 )
	oPrinter:Say ( nLin, 520, "VLR ITEM", 				oFont1 )
	oPrinter:Say ( nLin, 570, "QTD ITEM",       		oFont1 )
	
	

	While !T01->(EOF())

		IF nLin >= 750
			nLin   := 25
			oPrinter:EndPage()
			oPrinter:StartPage()
		ENDIF

		oPrinter:Say ( nLin+10,  20,  T01->BARRA,                      oFont2 )
		oPrinter:Say ( nLin+10,  80,  T01->COD,                	   	   oFont2 )
		oPrinter:Say ( nLin+10,  165, T01->COR,              		   oFont2 )
		oPrinter:Say ( nLin+10,  230, T01->DESCR,         		       oFont2 )
		oPrinter:Say ( nLin+10,  530, cValToChar(T01->VALOR),		   oFont2 )
		oPrinter:Say ( nLin+10,  590, cValToChar(T01->QTD), 		   oFont2 )
		
		nQuantNf += T01->QTD
		nValNF += T01->VALOR
		

		T01->(dbSkip())
		nLin := nLin + 10
		nCont++
				
	ENDDO
	
	FunRdp(cNF,cSerie)
	
	T01->(DbCloseArea())
	
	T02->(DBSKIP())
	
	ENDDO
	
	T02->(DbCloseArea())

	oPrinter:EndPage()

	oPrinter:Preview()
	oPrinter:SetViewPDF ( .T. )
	Restarea(xArea)	
return

Static Function FunRdp(cNF,cSerie)

   IF nLin >= 750
		nLin   := 25
		oPrinter:EndPage()
		oPrinter:StartPage()
   ENDIF
	
	nValNF := POSICIONE("SF2",1,XFILIAL("SF2")+cNF+cSerie, "F2_VALBRUT")
	nDesco := POSICIONE("SF2",1,XFILIAL("SF2")+cNF+cSerie, "F2_DESCONT")
	nCont := nVAlNF + nDesco
	
	nLin := nLin + 50	
	oPrinter:Say ( nLin, 20, "QTD PE�A",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nQuantNf, "@E 999,999,999"),20),  oFont2 )	
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "VLR BRUTO",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nCont, "@E 9999,999,999.99"),20),  oFont2 )
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "DESCONTO",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nDesco, "@E 9999,999,999.99"),20),  oFont2 )	
	
	nLin := nLin + nTmLin				
	oPrinter:Say ( nLin, 20, "VLR LIQUIDO",  oFont1 )
	oPrinter:Say ( nLin, 100, padl(TransForm(nValNF, "@E 9999,999,999.99"),20),  oFont2 )	
	
	nLin   := 25
	oPrinter:EndPage()
	oPrinter:StartPage()
				
Return
