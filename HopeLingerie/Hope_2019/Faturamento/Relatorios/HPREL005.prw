#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} RF013 
Relatorio Atendimento DAP - Ecommerce 
@author Weskley Silva
@since 30/10/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/
user function HPREL005()

local cMvPar05
Local nx := 0
Private oReport
Private cPergCont	:= 'HPREL005' 
Private cTipos := " "


/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif


cMvPar05:= AllTrim(mv_par05) 

If !Empty(cMvPar05)
	For nx := 1 to len(cMvPar05) step 3
		cAux := SubStr(cMvPar05,nx,3)
		If cAux <> '***'
			cTipos += "'"+cAux+"',"
		EndIf	
	Next
EndIf

cTipos := SubStr(cTipos,1,Len(cTipos)-1)

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()

Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 30 de Outubro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1


	oReport := TReport():New( 'DAP', 'ATENDIMENTO DAP POR SKU', cPergCont, {|oReport| ReportPrint( oReport ), 'ATENDIMENTO DAP POR SKU' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'ATENDIMENTO DAP POR SKU', {'DAP', 'SC5','SC6','SA1','SB1','ZAA','ZAH','SBV','SZ1','SZ2'})
			
	TRCell():New( oSection1, 'ORIGEM'		        ,'DAP', 		'ORIGEM',							"@!"                        ,06)
	TRCell():New( oSection1, 'EMISSAO'     	        ,'DAP', 		'EMISSAO',	    					"@!"				        ,10)
	TRCell():New( oSection1, 'ENTREGA'     	        ,'DAP', 		'ENTREGA',	    					"@!"				        ,10)
	TRCell():New( oSection1, 'PEDIDO'     			,'DAP', 		'PEDIDO',	         				"@!"						,10)	
	TRCell():New( oSection1, 'COD_CLIENTE'     	    ,'DAP', 		'COD_CLIENTE',	         			"@!"						,08)
	TRCell():New( oSection1, 'CLIENTE'     			,'DAP', 		'CLIENTE',	         				"@!"						,70)	
	TRCell():New( oSection1, 'PRODUTOS'	     		,'DAP', 		'PRODUTOS',         				"@!"						,20)
	TRCell():New( oSection1, 'DESCR'    	       	,'DAP', 		'DESCR',	                  		"@!"						,80)
	TRCell():New( oSection1, 'REFERENCIA'    		,'DAP', 		'REFERENCIA',         				"@!"						,20)
	TRCell():New( oSection1, 'COR'	        		,'DAP', 		'COR' ,		        				"@!"		                ,10)
	TRCell():New( oSection1, 'TAMANHO'			    ,'DAP', 		'TAMANHO',			   				"@!"						,06)
	TRCell():New( oSection1, 'COLECAO' 				,'DAP', 		'COLECAO',	   						"@!"		                ,40)
	TRCell():New( oSection1, 'SUBCOLECAO' 			,'DAP', 		'SUBCOLECAO' ,        				"@!"		                ,20)
	TRCell():New( oSection1, 'QTDE_VENDA'			,'DAP', 		'QTDE_VENDA' ,	    				"@E 9999.999"	        	,10)
	TRCell():New( oSection1, 'QTDE_ENTREGUE'		,'DAP', 		'QTDE_ENTREGUE',  				  	"@E 9999.999"            	,10)
	TRCell():New( oSection1, 'QTDE_PERDA'			,'DAP', 		'QTDE_PERDA' ,	    				"@E 9999.999"	        	,10)
	TRCell():New( oSection1, 'A_ENTREGAR'			,'DAP', 		'A_ENTREGAR',	    				"@E 9999.999"            	,10)
	TRCell():New( oSection1, 'QTDE_PREFAT'			,'DAP', 		'QTDE_PREFAT',		    			"@E 9999.999"            	,10)
	TRCell():New( oSection1, 'FALTA_ESTOQUE'		,'DAP', 		'FALTA_ESTOQUE',		    		"@E 9999.999"            	,10)
	TRCell():New( oSection1, 'SALDO'				,'DAP', 		'SALDO',		    				"@E 9999.999"            	,10)
	TRCell():New( oSection1, 'VALOR'	   			,'DAP', 		'VALOR',					   		"@E 999.999,99"				,10)
	TRCell():New( oSection1, 'TIPO_PEDIDO'	  		,'DAP', 		'TIPO_PEDIDO',			   			"@!"						,30)
	TRCell():New( oSection1, 'COND_PGTO'	       	,'DAP', 		'COND_PGTO',         				"@!"		                ,30)
	TRCell():New( oSection1, 'POLITICA'	  	     	,'DAP', 		'POLITICA',         				"@!"		                ,30)
	TRCell():New( oSection1, 'SITUACAO_PROD'        ,'DAP', 		'SITUACAO_PROD' ,		        	"@!"		                ,30)
					

Return( oReport )



//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 30 de Outubro de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("DAP") > 0
		DAP->(dbCloseArea())
	Endif
      
	cQuery := " SELECT R.C5_ORIGEM AS ORIGEM, " 
    cQuery += " CONVERT(CHAR, CAST(R.C5_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQuery += " CONVERT(CHAR, CAST(R.C5_FECENT AS SMALLDATETIME), 103) AS ENTREGA, "
    cQuery += " R.PEDIDO, "
    cQuery += " R.COD_CLIENTE, "
    cQuery += " R.CLIENTE, "
    cQuery += " R.C6_PRODUTO AS PRODUTOS, R.REFERENCIA,  "
	cQuery += " R.C6_DESCRI AS DESCR, "
    cQuery += " R.COR, "
    cQuery += " R.TAMANHO, "
    cQuery += " R.COLECAO, "
    cQuery += " R.SUBCOLECAO, "
    cQuery += " R.QTDE_VENDA, "
    cQuery += " R.C6_QTDENT AS QTDE_ENTREGUE, "
    cQuery += " R.QTDE_PERDA, "
    cQuery += " ((R.QTDE_VENDA - R.C6_QTDENT)- R.QTDE_PERDA) AS A_ENTREGAR, "
    cQuery += " R.QTDE_PREFAT, "
    cQuery += " R.FALTA_ESTOQUE, "    
    cQuery += " (((R.QTDE_VENDA - R.C6_QTDENT)- R.QTDE_PERDA) - R.QTDE_PREFAT) AS SALDO, "
    cQuery += " R.VALOR, "
    cQuery += " R.TIPO_PEDIDO, "
    cQuery += " R.COND_PGTO, "
    cQuery += " R.POLITICA, "
    cQuery += " R.B1_YSITUAC AS SITUACAO_PROD "
    cQuery += " FROM " 
    cQuery += " (SELECT C5_ORIGEM, "
    cQuery += " C5_EMISSAO, "
    cQuery += " C5_FECENT, "
    cQuery += " C5_NUM AS PEDIDO, "
    cQuery += " C6.C6_PRODUTO, LEFT(C6.C6_PRODUTO,8) AS REFERENCIA, "
	cQuery += " C6.C6_DESCRI, "
    cQuery += " ISNULL(BV_DESCRI,'-') AS COR, "
    cQuery += " CASE "
    cQuery += " WHEN RIGHT(C6.C6_PRODUTO,4) LIKE '000%' THEN REPLACE(RIGHT(C6.C6_PRODUTO,4),'000','') "
    cQuery += " WHEN RIGHT(C6.C6_PRODUTO,4) LIKE '00%' THEN REPLACE(RIGHT(C6.C6_PRODUTO,4),'00','') "
    cQuery += " WHEN RIGHT(C6.C6_PRODUTO,4) LIKE '0%' THEN SUBSTRING(RIGHT(C6.C6_PRODUTO,4),2,4) "
    cQuery += " ELSE REPLACE(RIGHT(C6.C6_PRODUTO,4),'00','') "
    cQuery += " END TAMANHO, "
    cQuery += " ISNULL(ZAA_DESCRI,'-') AS COLECAO, "
    cQuery += " ISNULL(ZAH_DESCRI,'-')AS SUBCOLECAO, "
    cQuery += " SUM(C6.C6_QTDVEN) AS QTDE_VENDA, "
    
    cQuery += " ISNULL((SELECT SUM(ZJ_QTDLIB) "
    cQuery += " FROM "+RetSqlName("SZJ")+" (NOLOCK) "
    cQuery += " WHERE D_E_L_E_T_ = '' "
    cQuery += " AND ZJ_PEDIDO = C6.C6_NUM "
    cQuery += " AND ZJ_PRODUTO = C6.C6_PRODUTO "
    cQuery += " AND ZJ_DOC = '' ),0)AS QTDE_PREFAT, "
    
    cQuery += " ISNULL((SELECT SUM(ZJ_QTDLIB - ZJ_QTDSEP) FROM "+RetSqlName("SZJ")+" (NOLOCK) WHERE D_E_L_E_T_ = '' " 
    cQuery += " AND ZJ_PEDIDO = C6.C6_NUM  "
    cQuery += " AND ZJ_PRODUTO = C6.C6_PRODUTO "
    cQuery += " AND ZJ_FALTA = 'F' "
    cQuery += " AND ZJ_DOC = '' ),0)AS FALTA_ESTOQUE, "    
    
    cQuery += " C6.C6_QTDENT, "
    cQuery += " ISNULL(CC6.C6_QTDVEN - CC6.C6_QTDENT,0) AS QTDE_PERDA, "
    cQuery += " C6.C6_VALOR AS VALOR, "
    cQuery += " A1_COD AS COD_CLIENTE, "
    cQuery += " A1_NOME AS CLIENTE, "
    cQuery += " C5_CONDPAG + ' - ' + E4_DESCRI AS COND_PGTO, "
    cQuery += " C5_POLCOM + ' - ' + Z2_DESC AS POLITICA, "
    cQuery += " Z1_CODIGO + ' - ' + Z1_DESC AS TIPO_PEDIDO, "
    cQuery += " B1_YSITUAC "
    cQuery += " FROM "+RetSqlName("SC6")+" (NOLOCK) C6 "
    cQuery += " INNER JOIN "+RetSqlName("SC5")+" (NOLOCK) C5 ON (C5_NUM = C6_NUM "
    cQuery += " AND C5.D_E_L_E_T_ <> '*') "
    cQuery += " LEFT JOIN "+RetSqlName("SC6")+" (NOLOCK) CC6 ON (C6.C6_NUM = CC6.C6_NUM AND C6.C6_CLI = CC6.C6_CLI  AND C6.C6_FILIAL = CC6.C6_FILIAL AND C6.C6_PRODUTO = CC6.C6_PRODUTO "
    cQuery += " AND CC6.D_E_L_E_T_ <> '*' AND CC6.C6_BLQ = 'R') "
    cQuery += " INNER JOIN "+RetSqlName("SA1")+" (NOLOCK) A1 ON (A1.A1_COD = C5.C5_CLIENTE "
    cQuery += " AND A1.D_E_L_E_T_ <> '*') "
    cQuery += " INNER JOIN "+RetSqlName("SZ1")+" (NOLOCK) Z1 ON (Z1_CODIGO = C5_TPPED "
    cQuery += " AND Z1.D_E_L_E_T_ <> '*') "
    cQuery += " LEFT JOIN "+RetSqlName("SZ2")+" (NOLOCK) Z2 ON (Z2_CODIGO = C5_POLCOM "
    cQuery += " AND Z2.D_E_L_E_T_ <> '*') "
    cQuery += " LEFT JOIN "+RetSqlName("SE4")+" (NOLOCK) E4 ON (E4.E4_CODIGO = C5_CONDPAG "
    cQuery += " AND E4.D_E_L_E_T_ <> '*') "
    cQuery += " LEFT JOIN "+RetSqlName("SB1")+" (NOLOCK) B1 ON (B1_COD = C6.C6_PRODUTO "
    cQuery += " AND B1.D_E_L_E_T_ <> '*') "
    cQuery += " LEFT JOIN "+RetSqlName("SBV")+" (NOLOCK) BV ON SUBSTRING(B1_COD,9,3) = BV_CHAVE AND BV_TABELA = 'COR' AND BV.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN "+RetSqlName("ZAA")+" (NOLOCK) ZAA ON B1_YCOLECA = ZAA_CODIGO AND ZAA.D_E_L_E_T_ = '' "
    cQuery += " LEFT JOIN "+RetSqlName("ZAH")+" (NOLOCK) ON B1_YSUBCOL = ZAH_CODIGO AND ZAH010.D_E_L_E_T_ = '' "
    cQuery += " WHERE C6.D_E_L_E_T_ <> '*' AND C5_EMISSAO BETWEEN '"+ DTOS(mv_par01) +"' AND '"+ DTOS(mv_par02) +"' " 
    cQuery += " AND C5_POLCOM NOT IN ('099','352','353','350') "
    
     if !Empty(mv_par08)
    cQuery += " AND  C5_NUM IN (SELECT ZJ_PEDIDO FROM "+RetSqlName('SZJ')+" (NOLOCK) WHERE D_E_L_E_T_ = '' AND ZJ_LOTDAP BETWEEN '"+Alltrim(mv_par08)+"' AND '"+Alltrim(mv_par09)+"'  GROUP BY ZJ_PEDIDO ) "
    ENDIF
    
    if !Empty(mv_par03) .OR. !Empty(mv_par04)
		cQuery += " AND C5_NUM BETWEEN '"+ mv_par03 +"' AND '"+ mv_par04 +"' " 
	endif
    
    If !Empty(cTipos)
		cQuery += CRLF + " AND C5_TPPED IN ("+cTipos+") "
	EndIf
	
	IF !Empty(mv_par06)
		cQuery += " AND B1_COD BETWEEN '"+Alltrim(mv_par06)+"' AND '"+Alltrim(mv_par07)+"' "
	endif
	
	IF !Empty(mv_par10)
		cQuery += " AND A1_COD = '"+Alltrim(mv_par10)+"' "
	Endif
    
    cQuery += " GROUP BY C5_ORIGEM,C5_EMISSAO,C5_FECENT,C5_NUM,C6.C6_PRODUTO,C6.C6_DESCRI,BV_DESCRI,ZAA_DESCRI,ZAH_DESCRI,C6.C6_QTDVEN,CC6.C6_QTDVEN,C6.C6_NUM,C6.C6_QTDENT,CC6.C6_QTDENT, "
    cQuery += " C6.C6_VALOR,A1_NOME,C5_CONDPAG + ' - ' + E4_DESCRI,C5_POLCOM + ' - ' + Z2_DESC,Z1_CODIGO + ' - ' + Z1_DESC,B1_YSITUAC,A1_COD) R  "
          				
	TCQUERY cQuery NEW ALIAS DAP

	While DAP->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()
		
		oSection1:Cell("ORIGEM"):SetValue(DAP->ORIGEM)
		oSection1:Cell("ORIGEM"):SetAlign("LEFT")
		
		oSection1:Cell("EMISSAO"):SetValue(DAP->EMISSAO)
		oSection1:Cell("EMISSAO"):SetAlign("LEFT")
		
		oSection1:Cell("ENTREGA"):SetValue(DAP->ENTREGA)
		oSection1:Cell("ENTREGA"):SetAlign("LEFT")		
		
		oSection1:Cell("PEDIDO"):SetValue(DAP->PEDIDO)
		oSection1:Cell("PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_CLIENTE"):SetValue(DAP->COD_CLIENTE)
		oSection1:Cell("COD_CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("CLIENTE"):SetValue(DAP->CLIENTE)
		oSection1:Cell("CLIENTE"):SetAlign("LEFT")
		
		oSection1:Cell("PRODUTOS"):SetValue(DAP->PRODUTOS)
		oSection1:Cell("PRODUTOS"):SetAlign("LEFT")
		
		oSection1:Cell("DESCR"):SetValue(DAP->DESCR)
		oSection1:Cell("DESCR"):SetAlign("LEFT")
		
		oSection1:Cell("REFERENCIA"):SetValue(DAP->REFERENCIA)
		oSection1:Cell("REFERENCIA"):SetAlign("LEFT")
		
		oSection1:Cell("COR"):SetValue(DAP->COR)
		oSection1:Cell("COR"):SetAlign("LEFT")	
		
		oSection1:Cell("TAMANHO"):SetValue(DAP->TAMANHO)
		oSection1:Cell("TAMANHO"):SetAlign("LEFT")	
		
		oSection1:Cell("COLECAO"):SetValue(DAP->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
			
		oSection1:Cell("SUBCOLECAO"):SetValue(DAP->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_VENDA"):SetValue(DAP->QTDE_VENDA)
		oSection1:Cell("QTDE_VENDA"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_ENTREGUE"):SetValue(DAP->QTDE_ENTREGUE)
		oSection1:Cell("QTDE_ENTREGUE"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PERDA"):SetValue(DAP->QTDE_PERDA)
		oSection1:Cell("QTDE_PERDA"):SetAlign("LEFT")
		
		oSection1:Cell("A_ENTREGAR"):SetValue(DAP->A_ENTREGAR)
		oSection1:Cell("A_ENTREGAR"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_PREFAT"):SetValue(DAP->QTDE_PREFAT)
		oSection1:Cell("QTDE_PREFAT"):SetAlign("LEFT")

		oSection1:Cell("FALTA_ESTOQUE"):SetValue(DAP->FALTA_ESTOQUE)
		oSection1:Cell("FALTA_ESTOQUE"):SetAlign("LEFT")		
		
		oSection1:Cell("SALDO"):SetValue(DAP->SALDO)
		oSection1:Cell("SALDO"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR"):SetValue(DAP->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")
		
		oSection1:Cell("TIPO_PEDIDO"):SetValue(DAP->TIPO_PEDIDO)
		oSection1:Cell("TIPO_PEDIDO"):SetAlign("LEFT")
		
		oSection1:Cell("COND_PGTO"):SetValue(DAP->COND_PGTO)
		oSection1:Cell("COND_PGTO"):SetAlign("LEFT")
		
		oSection1:Cell("POLITICA"):SetValue(DAP->POLITICA)
		oSection1:Cell("POLITICA"):SetAlign("LEFT")

		oSection1:Cell("SITUACAO_PROD"):SetValue(DAP->SITUACAO_PROD)
		oSection1:Cell("SITUACAO_PROD"):SetAlign("LEFT")
		
					
		oSection1:PrintLine()
		
		DAP->(DBSKIP()) 
	enddo
	DAP->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 30 de Outubro de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Dt Inicial"		        ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt Final"			    ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Pedido de "		    ,""		,""		,"mv_ch3","C",09,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Pedido At� "			    ,""		,""		,"mv_ch4","C",09,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Tipo dos Pedidos     ","","","Mv_ch5","C",99,0,0,"G","U_HF002TP()","","","N","Mv_par05","","","","","","","","","","","","","","","","","","","","","","","",{"Selecione os tipos de pedido.",""},{""},{""},"")
	PutSx1(cPergCont, "06","Produto de ? "		    ,""		,""		,"mv_ch6","C",15,0,1,"G",""	,""	,"","","mv_par06"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "07","Produto ate ? "		    ,""		,""		,"mv_ch7","C",15,0,1,"G",""	,""	,"","","mv_par07"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "08","Lote de ? "		        ,""		,""		,"mv_ch8","C",06,0,1,"G",""	,""	,"","","mv_par08"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "09","Lote ate ? "		    ,""		,""		,"mv_ch9","C",06,0,1,"G",""	,""	,"","","mv_par09"," ","","","","","","","","","","","","","","","")
Return
