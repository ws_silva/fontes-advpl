//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio de NFs de Faturamento
Rotina para gerar as de Notas Fiscais de Sa�da Emitidas;

@author Ronaldo Pereira
@since 23 de Mar�o de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL049()

	Local aRet := {}
	Local aPerg := {}

	aAdd(aPerg,{1,"Data Inicio: " 	,dDataBase,PesqPict("SF2", "F2_EMISSAO")	,'.T.'	,""		,'.T.',50,.F.})
	aAdd(aPerg,{1,"Data Fim: "		,dDataBase,PesqPict("SF2", "F2_EMISSAO")	,'.T.'	,""		,'.T.',50,.F.}) 
	aAdd(aPerg,{1,"Filial: "		,Space(04),""	,""		,""		,""		,0	,.F.})
	aAdd(aPerg,{1,"NF: "			,Space(10),""	,""		,""		,""		,0	,.F.})
	
	If ParamBox(aPerg,"Par�metros...",@aRet) 

	 	Processa({|| Geradados(aRet)}, "Exportando dados")
		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function Geradados(aRet)

    Local aArea     	:= GetArea()
    Local cQry    		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'Notas Fiscais '+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "NF"
    Local cTable1    	:= "Notas Fiscais" 

        
    //Dados tabela 1          
    cQry := "SELECT F2.F2_FILIAL AS FILIAL, "
    cQry += "       CONVERT(CHAR, CAST(F2.F2_EMISSAO AS SMALLDATETIME), 103) AS EMISSAO, "
    cQry += "       F2.F2_DOC AS NOTA_FISCAL, "
    cQry += "       F2.F2_SERIE AS SERIE, "
    cQry += "       D2.D2_CF +' - '+ X5.X5_DESCRI AS CFOP, "
    cQry += "       F2.F2_CLIENTE AS COD_CLI, "
    cQry += "       A1.A1_NOME AS CLIENTE, "
    cQry += "       F2.F2_EST AS UF, "
    cQry += "       F2.F2_FRETE AS FRETE, "
    
    cQry += "       CASE "
    cQry += "           WHEN F2.F2_TPFRETE = 'C' THEN 'CIF' "
    cQry += "           ELSE CASE "
    cQry += "                    WHEN F2.F2_TPFRETE = 'F' THEN 'FOB' "
    cQry += "                    ELSE '' "
    cQry += "                END "
    cQry += "       END AS TIPO_FRETE, "
    cQry += "       F2.F2_DESCONT AS DESCONTO, "

    cQry += "  (SELECT SUM(D2.D2_QUANT) "
    cQry += "   FROM SD2010 D2 WITH (NOLOCK) "
    cQry += "   WHERE D2.D2_FILIAL = F2.F2_FILIAL "
    cQry += "     AND D2.D2_DOC = F2.F2_DOC "
    cQry += "     AND D2.D2_SERIE = F2.F2_SERIE "
    cQry += "     AND D2.D2_CLIENTE = F2.F2_CLIENTE "
    cQry += "     AND D2.D_E_L_E_T_ = '' ) AS QTDE, "
    
    cQry += "       F2.F2_VALBRUT AS VALOR_TOTAL, "
    cQry += "       F2.F2_COND AS COND_PGTO, "
    cQry += "       F2.F2_VOLUME1 AS VOLUME, "
    cQry += "       Z1.Z1_DESC AS TIPO_PEDIDO, "
    cQry += "       CASE "
    cQry += "           WHEN C5.C5_TPPED = '016' THEN ISNULL(CONVERT(VARCHAR(2047), C5.C5_XOBSINT), '') "
    cQry += "           ELSE '' "
    cQry += "       END OBS "    
    cQry += "FROM SF2010 AS F2 WITH (NOLOCK) "
    cQry += "INNER JOIN SA1010 A1 WITH (NOLOCK) ON (A1.A1_COD = F2.F2_CLIENTE "
    cQry += "                                       AND A1.D_E_L_E_T_ = '') "
    cQry += "INNER JOIN SD2010 D2 WITH (NOLOCK) ON (D2.D2_FILIAL = F2.F2_FILIAL "
    cQry += "                                       AND D2.D2_DOC = F2.F2_DOC "
    cQry += "                                       AND D2.D2_CLIENTE = F2.F2_CLIENT "
    cQry += "                                       AND D2.D2_LOJA = F2.F2_LOJA "
    cQry += "                                       AND D2.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN SFM010 FM WITH (NOLOCK) ON (FM.FM_TS = D2.D2_TES "
    cQry += "                                      AND FM.D_E_L_E_T_ = '') "
    cQry += "LEFT JOIN SX5010 X5 WITH (NOLOCK) ON (X5.X5_TABELA = '13' "
    cQry += "                                      AND X5.X5_CHAVE = D2.D2_CF "
    cQry += "                                      AND X5.D_E_L_E_T_ = '') "
    cQry += "INNER JOIN SC5010 C5 WITH (NOLOCK) ON (C5.C5_FILIAL = F2.F2_FILIAL "
    cQry += "                                       AND C5.C5_NUM = D2.D2_PEDIDO "
    cQry += "                                       AND C5.D_E_L_E_T_ = '') "
    cQry += "INNER JOIN SZ1010 Z1 WITH (NOLOCK) ON (Z1.Z1_CODIGO = C5.C5_TPPED "
    cQry += "                                       AND Z1.D_E_L_E_T_ = '') "
    cQry += "WHERE F2.D_E_L_E_T_ = '' "
    cQry += "  AND F2.F2_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"' AND '"+ DTOS(aRet[2]) +"' "
    
    If !Empty(aRet[3])
    	cQry += " AND F2.F2_FILIAL = '"+ Alltrim(aRet[3]) +"' "
    EndIf
    
    If !Empty(aRet[4])
    	cQry += " AND F2.F2_DOC = '"+ Alltrim(aRet[4]) +"' "
    EndIf
    
    cQry += "GROUP BY F2.F2_FILIAL, "
    cQry += "         F2.F2_EMISSAO, "
    cQry += "         F2.F2_DOC, "
    cQry += "         F2.F2_SERIE, "
    cQry += "         D2.D2_CF, "
    cQry += "         X5.X5_DESCRI, "
    cQry += "         F2.F2_CLIENTE, "
    cQry += "         A1.A1_NOME, "
    cQry += "         F2.F2_EST, "
    cQry += "         F2.F2_FRETE, "
    cQry += "         F2.F2_TPFRETE, "
    cQry += "         F2.F2_DESCONT, "
    cQry += "         F2.F2_VALBRUT, "
    cQry += "         F2.F2_COND, "
    cQry += "         Z1.Z1_DESC, "
    cQry += "         C5.C5_TPPED, "
    cQry += "         ISNULL(CONVERT(VARCHAR(2047), C5.C5_XOBSINT), ''), "
    cQry += "         F2.F2_VOLUME1 "
    cQry += "ORDER BY F2.F2_EMISSAO, "
    cQry += "         F2.F2_DOC "

                              
    TCQuery cQry New Alias "QRY"
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FILIAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"EMISS�O",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NOTA_FISCAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SERIE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CFOP",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_CLI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"CLIENTE",1)        
        oFWMsExcel:AddColumn(cPlan1,cTable1,"UF",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"FRETE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO_FRETE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCONTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTDE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"VALOR_TOTAL",1)                
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COND_PGTO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"VOLUME",1)         
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TIPO_PEDIDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"OBS",1)

                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;
                                               QRY->FILIAL,;
                                               QRY->EMISSAO,;
                                               QRY->NOTA_FISCAL,;
                                               QRY->SERIE,;
                                               QRY->CFOP,;
                                               QRY->COD_CLI,;
                                               QRY->CLIENTE,;                                               
                                               QRY->UF,;
                                               QRY->FRETE,;
                                               QRY->TIPO_FRETE,;
                                               QRY->DESCONTO,;
                                               QRY->QTDE,;
                                               QRY->VALOR_TOTAL,;
                                               QRY->COND_PGTO,;
                                               QRY->VOLUME,;
                                               QRY->TIPO_PEDIDO,;
                                               QRY->OBS;
            })
         
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return