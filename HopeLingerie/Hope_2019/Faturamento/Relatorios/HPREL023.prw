#include 'protheus.ch'
#include 'parmtype.ch'
#include "topconn.ch"

/*/{Protheus.doc} HPREL019 
Saldo por Armazem
@author Weskley Silva
@since 14/11/2017
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL023()

	Private oReport
	Private cPergCont	:= 'HPREL023' 

	************************
	*Monta pergunte do Log *
	************************
	AjustaSX1(cPergCont)
	If !Pergunte(cPergCont, .T.)
		Return
	Endif

	oReport := ReportDef()
	If oReport == Nil
		Return( Nil )
	EndIf

	oReport:PrintDialog()
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportDef()

	Local oReport
	Local oSection1
	Local oSection2


	oReport := TReport():New( 'EXP', 'RELATORIO VIPP ', cPergCont, {|oReport| ReportPrint( oReport ), 'RELATORIO VIPP' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATORIO VIPP', { 'EXP', 'SC5','SA1','SF2','CC2'})
			
	TRCell():New( oSection1, 'NOME'		    	    ,'EXP', 		'NOME',								"@!"                        ,40)
	TRCell():New( oSection1, 'ENDE'      	        ,'EXP', 		'ENDE',		    					"@!"				        ,80)
	TRCell():New( oSection1, 'NUMERO'          		,'EXP', 		'NUMERO',              				"@!"						,06)
	TRCell():New( oSection1, 'COMPLEMENTO'         	,'EXP', 		'COMPLEMENTO',              		"@!"						,15)
	TRCell():New( oSection1, 'BAIRRO' 				,'EXP', 		'BAIRRO' ,	        				"@!"		                ,20)
	TRCell():New( oSection1, 'MUNICIPIO'	       	,'EXP', 		'MUNICIPIO',          				"@!"		                ,20)
	TRCell():New( oSection1, 'ESTADO' 				,'EXP', 		'ESTADO' ,	    					"@!"		                ,15)
	TRCell():New( oSection1, 'CEP'	        		,'EXP', 		'CEP' ,		        				"@!"		                ,10)
	TRCell():New( oSection1, 'TELEFONE'			    ,'EXP', 		'TELEFONE',			   				"@!"						,15)
	TRCell():New( oSection1, 'FINANCEIRO'			,'EXP', 		'FINANCEIRO' ,	    				"@!"	                    ,20)
	TRCell():New( oSection1, 'REGISTRO'				,'EXP', 		'REGISTRO' ,	  				  	"@!"		                ,20)
	TRCell():New( oSection1, 'PESO'					,'EXP', 		'PESO',						    	"@!"		        		,10)
	TRCell():New( oSection1, 'ALTURA'				,'EXP', 		'ALTURA' ,			    			"@!"	                    ,08)
	TRCell():New( oSection1, 'LARGURA'		        ,'EXP', 		'LARGURA',				   			"@!"						,15)
	TRCell():New( oSection1, 'COMPRIMENTO'		    ,'EXP', 		'COMPRIMENTO',			   			"@!"						,15)
	TRCell():New( oSection1, 'CUBICO'		        ,'EXP', 		'CUBICO',				   			"@!"						,15)
    TRCell():New( oSection1, 'NF'			        ,'EXP', 		'NF',					   			"@!"						,15)
    TRCell():New( oSection1, 'SERIE'		        ,'EXP', 		'SERIE',				   			"@!"						,15)
	TRCell():New( oSection1, 'VALOR'		        ,'EXP', 		'VALOR',				   			"@!"						,15)
	TRCell():New( oSection1, 'VALOR_A_COBRAR'		,'EXP', 		'VALOR_A_COBRAR',				   	"@E 999,999.99"				,15)
	TRCell():New( oSection1, 'ADICIONAIS'			,'EXP', 		'ADICIONAIS',					   	"@!"						,15)
    TRCell():New( oSection1, 'CONTRATO'				,'EXP', 		'CONTRATO',						   	"@!"						,15)
	TRCell():New( oSection1, 'ADMINISTRATIVO'		,'EXP', 		'ADMINISTRATIVO',				   	"@!"						,15)
	TRCell():New( oSection1, 'CARTAO'				,'EXP', 		'CARTAO',				   			"@!"						,15)
	TRCell():New( oSection1, 'EMAIL'				,'EXP', 		'EMAIL',				   			"@!"						,15)
	TRCell():New( oSection1, 'OBS1'					,'EXP', 		'OBS1',				   				"@!"						,15)
	TRCell():New( oSection1, 'OBS2'					,'EXP', 		'OBS2',				   				"@!"						,15)
	TRCell():New( oSection1, 'OBS3'					,'EXP', 		'OBS3',				   				"@!"						,15)
	TRCell():New( oSection1, 'DESC_OBJETO'		    ,'EXP', 		'DESC_OBJETO',		   				"@!"						,15)
	TRCell():New( oSection1, 'ID_VOLUME'			,'EXP', 		'ID_VOLUME',		   				"@!"						,15)
	TRCell():New( oSection1, 'QTDE_VOLUME'			,'EXP', 		'QTDE_VOLUME',		   				"@!"						,15)
	TRCell():New( oSection1, 'COD_CLIENTE_VISUAL'	,'EXP', 		'COD_CLIENTE_VISUAL',  				"@!"						,15)
	TRCell():New( oSection1, 'CELULAR'			    ,'EXP', 		'CELULAR',			   				"@!"						,15)
	
								

Return( oReport )



//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local oSection2 := oReport:Section(2)
	Local cQuery := ""


	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("EXP") > 0
		EXP->(dbCloseArea())
	Endif
      
	 cQuery := " SELECT "  
	 cQuery += " SA1010.A1_NOME AS NOME, "
	 cQuery += " SA1010.A1_END AS ENDE, " 
	 cQuery += " '' AS NUMERO, "  
	 cQuery += " SA1010.A1_COMPLEM AS COMPLEMENTO , "
	 cQuery += " SA1010.A1_BAIRRO AS BAIRRO, "
	 cQuery += " SA1010.A1_MUN AS MUNICIPIO, "
	 cQuery += " SA1010.A1_EST AS ESTADO, " 
	 cQuery += " SA1010.A1_CEP AS CEP, "
	 cQuery += " SA1010.A1_DDD+SA1010.A1_TELE AS TELEFONE, " 
	 cQuery += " '' AS FINANCEIRO, " 
	 cQuery += " '' AS REGISTRO, "
	 cQuery += " REPLACE(CAST(SF2010.F2_PBRUTO  AS NUMERIC(15,3)),'.','')  AS PESO, " 
	 cQuery += " CASE WHEN RIGHT(ZQ_ALTURA,2) = '.1' THEN '10' ELSE REPLACE(RIGHT(ZQ_ALTURA,2),'.','') END AS ALTURA, "
	 cQuery += " ISNULL(REPLACE(RIGHT(ZQ_LARGURA,2),'.',''),0) AS LARGURA, "  
	 cQuery += " ISNULL(REPLACE(RIGHT(ZQ_CUMPRIM,2),'.',''),0) AS COMPRIMENTO, "  
	 cQuery += " '' AS CUBICO, " 
	 cQuery += " SC5010.C5_NOTA AS NF, " 
	 cQuery += " SC5010.C5_SERIE AS SERIE, " 
	 cQuery += " CASE WHEN SF2010.F2_VALBRUT  <= '19.50' THEN '1950' ELSE REPLACE(CONVERT(VARCHAR, CAST(SF2010.F2_VALBRUT AS MONEY)),'.','') END AS VALOR, " 
	 cQuery += " '' AS VALOR_A_COBRAR, " 
	 cQuery += " '' AS ADICIONAIS, "  
	 cQuery += " '9912246333' AS CONTRATO, "  
	 cQuery += " '' AS ADMINISTRATIVO, " 
	 cQuery += " CC2_XCPOST AS CARTAO, "
	 cQuery += " SA1010.A1_EMAIL AS EMAIL, "
	 cQuery += " SC5010.C5_NUM AS OBS1, "
	 cQuery += " SC5010.C5_XPEDWEB AS OBS2, "
	 cQuery += " ISNULL(CASE " 
     cQuery += " WHEN SC5010.C5_POLCOM = '092'  THEN 'BONIFICADO' "  
     cQuery += " WHEN SC5010.C5_POLCOM = '310'  THEN 'BONIFICADO' " 
     cQuery += " WHEN SC5010.C5_POLCOM = '240'  THEN 'BONIFICADO' "
     cQuery += " WHEN SC5010.C5_POLCOM = '007'  THEN 'MOSTRUARIO' "
     cQuery += " WHEN SC5010.C5_POLCOM = '013'  THEN 'MATERIAL'  "
     cQuery += " WHEN SC5010.C5_POLCOM = '027'  THEN 'MATERIAL' "
     cQuery += " WHEN SC5010.C5_POLCOM = '014' THEN 'BRINDE' "
	 cQuery += " WHEN SC5010.C5_POLCOM = '012' THEN 'TROCA' "
	 cQuery += " WHEN SC5010.C5_POLCOM = '063' THEN 'SAC' "
	 cQuery += " WHEN SC5010.C5_POLCOM = '015' THEN 'SAC' "
     cQuery += " END,'') AS OBS3, "
	 cQuery += " '' AS DESC_OBJETO, "  
	 cQuery += " '' AS ID_VOLUME,  "
	 cQuery += " F2_VOLUME1 AS QTDE_VOLUME, " 
	 cQuery += " '' AS COD_CLIENTE_VISUAL, " 
	 cQuery += " '' AS CELULAR  "
 	 cQuery += " FROM "+RetSqlName('SC5')+" SC5010  "
	 cQuery += " JOIN "+RetSqlName('SA1')+" SA1010 (NOLOCK) ON SA1010.A1_COD=SC5010.C5_CLIENT AND SA1010.A1_LOJA = SC5010.C5_LOJACLI AND SA1010.D_E_L_E_T_ = '' "
	 cQuery += " JOIN "+RetSqlName('SF2')+" SF2010 (NOLOCK) ON SF2010.F2_DOC=SC5010.C5_NOTA AND SF2010.F2_SERIE=SC5010.C5_SERIE AND SC5010.D_E_L_E_T_ = '' "
	 cQuery += " JOIN "+RetSqlName('CC2')+" CC2010 (NOLOCK) ON CC2010.CC2_CODMUN = SA1010.A1_COD_MUN AND CC2010.CC2_EST = SA1010.A1_EST AND CC2010.D_E_L_E_T_ = ''  "
	 cQuery += " LEFT JOIN "+RetSqlName('SZJ')+" (NOLOCK) ON F2_DOC = ZJ_DOC AND F2_CLIENTE = ZJ_CLIENTE AND F2_SERIE = ZJ_SERIE AND SZJ010.D_E_L_E_T_ = '' "
	 cQuery += " LEFT JOIN "+RetSqlName('SZR')+" (NOLOCK) ON SZR010.ZR_NUMPF = SZJ010.ZJ_NUMPF AND SZR010.D_E_L_E_T_ = '' "
	 cQuery += " LEFT JOIN "+RetSqlName('SZQ')+" (NOLOCK) ON (ZR_TPVOL = ZQ_CODIGO)  AND SZQ010.D_E_L_E_T_ = '' "	 
	 
	 
	 cQuery += " WHERE SC5010.C5_POLCOM IN ("+GETMV("HP_POLVIPE")+") "
	 	 
	 cQuery += " AND SF2010.F2_EMISSAO BETWEEN '"+ DTOS(mv_par01)+ "' AND  '"+ DTOS(mv_par02) +"' "
	
	if Empty(mv_par03) .or. Empty(mv_par04)
		cQuery += " AND SF2010.F2_DOC <> '' "
	else
		cQuery += " AND SF2010.F2_DOC BETWEEN '"+ mv_par03 +"' AND '"+ mv_par04 +"' " 
	endif
	cQuery += " AND SF2010.D_E_L_E_T_ = '' " 
	
	cQuery += "   GROUP BY SA1010.A1_NOME, " 
	cQuery += "   SA1010.A1_END, "
	cQuery += "   SA1010.A1_COMPLEM,"
	cQuery += "   SA1010.A1_BAIRRO,"
	cQuery += "   SA1010.A1_MUN,"
	cQuery += "   SA1010.A1_EST,"
	cQuery += "   SA1010.A1_CEP,"
	cQuery += "	  SA1010.A1_DDD,"
	cQuery += "	  SA1010.A1_TELE,"
	cQuery += "	  SF2010.F2_PBRUTO,"
	cQuery += "	  SZQ010.ZQ_ALTURA,"
	cQuery += "	  SZQ010.ZQ_LARGURA,"
	cQuery += "	  SZQ010.ZQ_CUMPRIM,"
	cQuery += "	  SC5010.C5_NOTA,"
	cQuery += "	  SC5010.C5_SERIE,"
	cQuery += "	  SF2010.F2_VALBRUT,"
	cQuery += "	  CC2010.CC2_XCPOST,"
	cQuery += "	  SA1010.A1_EMAIL,"
	cQuery += "	  SC5010.C5_NUM,"
	cQuery += "	  SC5010.C5_XPEDWEB,"
	cQuery += "	  SC5010.C5_POLCOM,"
	cQuery += "	  SF2010.F2_VOLUME1"

	TCQUERY cQuery NEW ALIAS EXP

	While EXP->(!EOF())

		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("NOME"):SetValue(EXP->NOME)
		oSection1:Cell("NOME"):SetAlign("LEFT")

		oSection1:Cell("ENDE"):SetValue(EXP->ENDE)
		oSection1:Cell("ENDE"):SetAlign("LEFT")
		
		oSection1:Cell("NUMERO"):SetValue(EXP->NUMERO)
		oSection1:Cell("NUMERO"):SetAlign("LEFT")
		
		oSection1:Cell("COMPLEMENTO"):SetValue(EXP->COMPLEMENTO)
		oSection1:Cell("COMPLEMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("BAIRRO"):SetValue(EXP->BAIRRO)
		oSection1:Cell("BAIRRO"):SetAlign("LEFT")
			
		oSection1:Cell("MUNICIPIO"):SetValue(EXP->MUNICIPIO)
		oSection1:Cell("MUNICIPIO"):SetAlign("LEFT")
				
		oSection1:Cell("ESTADO"):SetValue(EXP->ESTADO)
		oSection1:Cell("ESTADO"):SetAlign("LEFT")
				
		oSection1:Cell("CEP"):SetValue(EXP->CEP)
		oSection1:Cell("CEP"):SetAlign("LEFT")
		
		oSection1:Cell("TELEFONE"):SetValue(EXP->TELEFONE)
		oSection1:Cell("TELEFONE"):SetAlign("LEFT")
						
		oSection1:Cell("FINANCEIRO"):SetValue(EXP->FINANCEIRO)
		oSection1:Cell("FINANCEIRO"):SetAlign("LEFT")
				
		oSection1:Cell("REGISTRO"):SetValue(EXP->REGISTRO)
		oSection1:Cell("REGISTRO"):SetAlign("LEFT")
				
		oSection1:Cell("PESO"):SetValue(EXP->PESO)
		oSection1:Cell("PESO"):SetAlign("LEFT")
		
		oSection1:Cell("ALTURA"):SetValue(EXP->ALTURA)
		oSection1:Cell("ALTURA"):SetAlign("LEFT")
		
		oSection1:Cell("COMPRIMENTO"):SetValue(EXP->COMPRIMENTO)
		oSection1:Cell("COMPRIMENTO"):SetAlign("LEFT")
		
		oSection1:Cell("CUBICO"):SetValue(EXP->CUBICO)
		oSection1:Cell("CUBICO"):SetAlign("LEFT")
		
		oSection1:Cell("NF"):SetValue(EXP->NF)
		oSection1:Cell("NF"):SetAlign("LEFT")
		
		oSection1:Cell("SERIE"):SetValue(EXP->SERIE)
		oSection1:Cell("SERIE"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR"):SetValue(EXP->VALOR)
		oSection1:Cell("VALOR"):SetAlign("LEFT")
		
		oSection1:Cell("VALOR_A_COBRAR"):SetValue(EXP->VALOR_A_COBRAR)
		oSection1:Cell("VALOR_A_COBRAR"):SetAlign("LEFT")
		
		oSection1:Cell("ADICIONAIS"):SetValue(EXP->ADICIONAIS)
		oSection1:Cell("ADICIONAIS"):SetAlign("LEFT")
		
		oSection1:Cell("CONTRATO"):SetValue(EXP->CONTRATO)
		oSection1:Cell("CONTRATO"):SetAlign("LEFT")
		
		oSection1:Cell("ADMINISTRATIVO"):SetValue(EXP->ADMINISTRATIVO)
		oSection1:Cell("ADMINISTRATIVO"):SetAlign("LEFT")
		
		oSection1:Cell("CARTAO"):SetValue(EXP->CARTAO)
		oSection1:Cell("CARTAO"):SetAlign("LEFT")
		
		oSection1:Cell("EMAIL"):SetValue(EXP->EMAIL)
		oSection1:Cell("EMAIL"):SetAlign("LEFT")
		
		oSection1:Cell("OBS1"):SetValue(EXP->OBS1)
		oSection1:Cell("OBS1"):SetAlign("LEFT")
		
		oSection1:Cell("OBS2"):SetValue(EXP->OBS2)
		oSection1:Cell("OBS2"):SetAlign("LEFT")
		
		oSection1:Cell("OBS3"):SetValue(EXP->OBS3)
		oSection1:Cell("OBS3"):SetAlign("LEFT")
		
		oSection1:Cell("DESC_OBJETO"):SetValue(EXP->DESC_OBJETO)
		oSection1:Cell("DESC_OBJETO"):SetAlign("LEFT")
		
		oSection1:Cell("ID_VOLUME"):SetValue(EXP->ID_VOLUME)
		oSection1:Cell("ID_VOLUME"):SetAlign("LEFT")
		
		oSection1:Cell("QTDE_VOLUME"):SetValue(EXP->QTDE_VOLUME)
		oSection1:Cell("QTDE_VOLUME"):SetAlign("LEFT")
		
		oSection1:Cell("COD_CLIENTE_VISUAL"):SetValue(EXP->COD_CLIENTE_VISUAL)
		oSection1:Cell("COD_CLIENTE_VISUAL"):SetAlign("LEFT")
		
		oSection1:Cell("CELULAR"):SetValue(EXP->CELULAR)
		oSection1:Cell("CELULAR"):SetAlign("LEFT")
				
		oSection1:PrintLine()
		
		EXP->(DBSKIP()) 
	enddo
	EXP->(DBCLOSEAREA())
Return( Nil )


//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Weskley Silva
@since 11 de Agosto de 2017
@version P12
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Dt Inicial"		        ,""		,""		,"mv_ch1","D",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Dt Final"			    ,""		,""		,"mv_ch2","D",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Nota Inicial"		    ,""		,""		,"mv_ch3","C",09,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Nota Final"			    ,""		,""		,"mv_ch4","C",09,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Cartao"			        ,""		,""		,"mv_ch5","C",08,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")
Return