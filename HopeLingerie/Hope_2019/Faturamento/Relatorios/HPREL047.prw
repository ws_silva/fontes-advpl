#Include "Protheus.ch"
#Include "topconn.ch"

/*???????????????????????????????????????????????????????????????????????????
?????????????????????????????????????????????????????????????????????????????
????????????????????????????????????????????????????????????????????????????
???Programa  ?HPREL010  ? Autor ? Bruno Parreira     ? Data ?  16/06/17   ???
????????????????????????????????????????????????????????????????????????????
???Descricao ? Relatorio de romaneio.                                     ???
????????????????????????????????????????????????????????????????????????????
???Uso       ? Especifico HOPE                                            ???
????????????????????????????????????????????????????????????????????????????
?????????????????????????????????????????????????????????????????????????????
???????????????????????????????????????????????????????????????????????????*/

User Function HPREL047()
Local cDesc1       := "Este programa tem como objetivo imprimir relatorio "
Local cDesc2       := "de acordo com os parametros informados pelo usuario."
Local cDesc3       := "Relat�rio de Pr�-Faturamentos gerados"
Local cPict        := ""
Local titulo       := "Romaneio"
Local nLin         := 80                                                                                                                                                              
					    //0         1         2         3         4         5         6         7         8         9         0         1         2         3         4         5         6         7         8         9         0
					    //012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890
Local Cabec1       := ""//"VOLUME  CODIGO     DESCRI??O                                       COR            TAMANHO     QUANTIDADE"
Local Cabec2       := ""//"PRE-FAT      PEDIDO        VOLUME              CODIGO         COR         DESCRICAO      QUANTIDADE"                     
Local imprime      := .T.
Local aOrd         := {}
Local lPerg        := .T.
					//  1   2   3   4   5   6   7   8   9  10  11  12  13  14   
Private aCol       := {000,010,020,035,091,105,110,130,150}
Private lEnd       := .F.
Private lAbortPrint:= .F.
Private CbTxt      := ""
Private limite     := 132//220
Private tamanho    := "M"//"G"
Private nomeprog   := "HPREL047"
Private nTipo      := 15
Private aReturn    := { "Zebrado", 1, "Administracao", 1, 1, 2, "", 1}
Private nLastKey   := 0
Private cbtxt      := Space(10)
Private cbcont     := 00
Private CONTFL     := 01
Private m_pag      := 01
Private wnrel      := "HPREL047"
Private cPerg      := 'HPREL047'
Private cString    := "SZJ"
Private cArqTEMP

If !Pergunte(cPerg)
	Return
EndIf

titulo := "Romaneio do Pr�-Faturamento: "+mv_par01

wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.F.)

If nLastKey == 27
	Return
Endif

SetDefault(aReturn,cString)

If nLastKey == 27
	Return
Endif

nTipo := If(aReturn[4]==1,15,18)

RptStatus({|| RunReport(Cabec1,Cabec2,Titulo,nLin) },Titulo)

//Roda(cbcont,cbtxt,tamanho)		
	
//??????????????????????????????????????????????????????????????????????
//? Finaliza a execucao do relatorio...                                 ?
//???????????????????????????????????????????????????????????????????????

SET DEVICE TO SCREEN

//??????????????????????????????????????????????????????????????????????
//? Se impressao em disco, chama o gerenciador de impressao...          ?
//???????????????????????????????????????????????????????????????????????

If aReturn[5]==1
	DbCommitAll()
	SET PRINTER TO
	OurSpool(wnrel)
Endif

MS_FLUSH()

Return

/*???????????????????????????????????????????????????????????????????????????
?????????????????????????????????????????????????????????????????????????????
????????????????????????????????????????????????????????????????????????????
???Funcao    ?RUNREPORT ? Autor ? Bruno Parreira     ? Data ?  16/06/17   ???
????????????????????????????????????????????????????????????????????????????
???Descricao ? Busca registros no banco e gera relatorio                  ???
????????????????????????????????????????????????????????????????????????????
???Uso       ? Especifico HOPE                                            ???
????????????????????????????????????????????????????????????????????????????
?????????????????????????????????????????????????????????????????????????????
???????????????????????????????????????????????????????????????????????????*/

Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)
Local cQuery   := ""
Local nTotReg  := 0
Local nContar  := 0
Local nTotQPed := 0
Local nTotVPed := 0
Local nTotQPF  := 0
Local nTotVPF  := 0
Local nCont    := 0
Local nQtdeC6  := 0
Local nVlrC6   := 0
Private cTotalVol := 0

cQuery1 := " SELECT TOP 1 ZR_VOLUME  T FROM SZR010 WHERE D_E_L_E_T_ = '' " 

IF Upper(mv_par02) == 'S'

cQuery1 += " AND LEFT(ZR_NUMPF,6) = '"+AllTrim(mv_par01)+"' ORDER BY CAST(ZR_VOLUME AS INT ) DESC "

ELSE

cQuery1 += " AND ZR_NUMPF = '"+AllTrim(mv_par01)+"' ORDER BY CAST(ZR_VOLUME AS INT ) DESC "

endif

DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery1),"TRB1",.F.,.T.)
DbSelectArea("TRB1")

cTotalVol := TRB1->T

TRB1->(DBCloseArea())


cQuery := "SELECT C5_NUM,ZR_NUMPF,ZR_VOLUME,ZR_PRODUTO,B4_COD,B4_DESC, "
cQuery += CRLF + "(select BV_DESCRI from "+RetSqlName("SBV")+" SBV WITH(NOLOCK) where BV_TABELA=B4_LINHA and BV_CHAVE=SUBSTRING(ZR_PRODUTO,9,3) and SBV.D_E_L_E_T_ = '') AS COR, "
cQuery += CRLF + "(select BV_DESCRI from "+RetSqlName("SBV")+" SBV WITH(NOLOCK) where BV_TABELA=B4_COLUNA and BV_CHAVE=SUBSTRING(ZR_PRODUTO,12,4) and SBV.D_E_L_E_T_ = '') AS TAMANHO, "
cQuery += CRLF + "ZR_QUANT,ZR_TPVOL,ZQ_DESCRIC,A1_COD,A1_LOJA,A1_NOME "
cQuery += CRLF + "from "+RetSqlName("SZR")+" SZR WITH(NOLOCK) "
cQuery += CRLF + "inner join "+RetSqlName("SB4")+" SB4 WITH(NOLOCK) "
cQuery += CRLF + "on B4_COD = SUBSTRING(ZR_PRODUTO,1,8) "
cQuery += CRLF + "and SB4.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SC5")+" SC5 WITH(NOLOCK) "
cQuery += CRLF + "on C5_NUM = SUBSTRING(ZR_NUMPF,1,6) "
cQuery += CRLF + "and SC5.D_E_L_E_T_ = '' "
cQuery += CRLF + "inner join "+RetSqlName("SA1")+" SA1 WITH(NOLOCK) "
cQuery += CRLF + "on A1_COD = C5_CLIENTE "
cQuery += CRLF + "and A1_LOJA = C5_LOJACLI "
cQuery += CRLF + "and SC5.D_E_L_E_T_ = '' "
cQuery += CRLF + "left join "+RetSqlName("SZQ")+" SZQ WITH(NOLOCK) "
cQuery += CRLF + "on ZQ_CODIGO = ZR_TPVOL " 
cQuery += CRLF + "and SZQ.D_E_L_E_T_ = ''  "

IF Upper(mv_par02) == 'S'

cQuery += CRLF + "where LEFT(ZR_NUMPF,6) = '"+AllTrim(mv_par01)+"' "

else

cQuery += CRLF + "where ZR_NUMPF = '"+AllTrim(mv_par01)+"' "

endif
cQuery += CRLF + "and SZR.D_E_L_E_T_ = '' "
cQuery += CRLF + "order by CAST(ZR_VOLUME AS INT ),ZR_PRODUTO "

MemoWrite("HPREL010.TXT",cQuery)

//cQuery  := ChangeQuery(cQuery)

DbUseArea(.T.,'TOPCONN',TCGENQRY(,,cQuery),"TRB",.F.,.T.)

DbSelectArea("TRB")
TRB->(DbEval({ || nTotReg++ },,{|| !Eof()}))
TRB->(DbGoTop())

nTotPecas := 0
nTotGeral := 0

If TRB->(!EOF())
	ProcRegua(nTotReg)
	While TRB->(!EOF())
		IncProc("Gerando relat�rio, regs : "+StrZero(++nContar,6)+" de "+StrZero(nTotReg,6))
		
		If lAbortPrint
			@ nLin,000 PSAY "*** CANCELADO PELO OPERADOR ***"
			Exit
		Endif
		
		If nLin > 55
			Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
			nLin := 6
			If nCont = 0
				@ nLin, aCol[1]  PSay "Pr�-Faturamento: "+TRB->ZR_NUMPF
				nLin++
				@ nLin, aCol[1]  PSay " Pedido: "+TRB->C5_NUM
				nLin++
				@ nLin, aCol[1]  PSay "Cliente: "+TRB->A1_COD+"/"+TRB->A1_LOJA+" - "+TRB->A1_NOME
				nLin++
				nLin++
				//@ nLin,000 PSay __PrtThinLine()
			EndIf
			@ nLin, aCol[1]  PSay "VOLUME  TOTAL VOLUME    CODIGO     DESCRICAO                     COR                           TAMANHO           QUANTIDADE"
			nLin++
			@ nLin,000 PSay __PrtThinLine()
			nLin++
		Endif
		
		cVolume := TRB->ZR_VOLUME
		
		//"VOLUME     CODIGO     DESCRI??O                                            COR            TAMANHO     QUANTIDADE"
		
		@ nLin, aCol[1]  PSay TRB->ZR_VOLUME
		@ nLin, aCol[2]  PSay cTotalVol
 		@ nLin, aCol[3]  PSay TRB->B4_COD
		@ nLin, aCol[4]  PSay TRB->B4_DESC
		@ nLin, aCol[5]-10  PSay TRB->COR
		@ nLin, aCol[6]  PSay TRB->TAMANHO
		@ nLin, aCol[7]  PSay Transform(TRB->ZR_QUANT,"@E 9,999,999.99")
		
		nLin++
		nCont++
		
		nTotPecas += TRB->ZR_QUANT
		nTotGeral += TRB->ZR_QUANT
		
		TRB->(DbSkip())
		
		If cVolume <> TRB->ZR_VOLUME
			@ nLin,000 PSay __PrtThinLine()
			nLin++
			@ nLin, aCol[4]  PSay "Qtd. Pe�as do Volume "+cVolume+":"
			@ nLin, aCol[6]  PSay Transform(nTotPecas,"@E 9,999,999.99")
			nTotPecas := 0
			nLin++
			nLin++
		EndIf
	EndDo

	@ nLin,000 PSay __PrtThinLine()
	nLin++
	@ nLin, aCol[4]    PSay "Qtd. Pe�as Total: "
	@ nLin, aCol[6]-1  PSay Transform(nTotGeral,"@E 9,999,999.99")
		
EndIf

TRB->(DbCloseArea())                                  

Return