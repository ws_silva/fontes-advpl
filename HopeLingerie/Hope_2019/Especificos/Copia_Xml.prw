#Include 'Protheus.ch'

User Function Copia_Xml()
#Include 'Protheus.ch'
#Include 'FWMVCDef.ch'
#Include 'PLSAREXP.ch'
 
//-------------------------------------------------------------------
/*/{Protheus.doc} ExportaXML
Exporta��o XML
 
@author David
 
@since 27/04/2014
@version 1.0
/*/
//-------------------------------------------------------------------
User Function ExportaXML()
Local cArqLocal := ""
Local cXML      := ""
Local nHdl      := 0
 
//Nome do Arquivo
cArqLocal := "Arquivo.XML"//
 
//Cria uma arquivo vazio em disco
nHdl := Fcreate("\Data\"+cArqLocal)
 
cXML += '<?xml version="1.0" encoding="UTF-8" ?>'
 
//...Aqui fica o corpo do XML
cXML += '<operadora>'
cXml += '<registroCodigo>'+"1000299"+'</registroCodigo>'
cXml += '<cnpjOperadora>'+"48539407007201"+'</cnpjOperadora>'
cXML += '<solicitacao>'
cXML += '<nossoNumero>'+"123312"+'</nossoNumero>'
cXML += '</solicitacao>'
cXML += '</operadora>'
 
//Fun��o escreve no arquivo TXT.
FWRITE(nHdl,cXML)               
//Fecha o arquivo em disco
FCLOSE(nHdl)
 
//Copia o arquivo da pasta \DATA\ do protheus para a maquina local.
If CpyS2T( "\Data\"+cArqLocal, "C:\temp\", .F. )
    //Excluir arquivo em disco da pasta \DATA\ para n�o ocupar espa�o do servidor
    If FERASE("\Data\"+cArqLocal) == -1
        MsgStop('Falha na dele��o do Arquivo')
    Else
        //Apresenta mensagem informando que foi exportado
        MSGALERT("Arquivos exportados com sucesso.", "Exporta��o")//"Arquivos exportados com sucesso." - "Exporta��o"
    EndIf
Else
    MsgStop('N�o foi poss�vel copiar o arquivo')
EndIf  
             
Return


