#include "protheus.ch"
#include "rwmake.ch"
#include "TbiConn.ch"

/*/{Protheus.doc} HATUSQL 
Rotina que cria CTD baseado nos clientes e fornecedores
que ainda nao possuem conta contabil 
@author Melo
@since 15/01/2019
@version 1.0
/*/

User Function HATUSQL()
	
	//Prepare Environment Empresa "01" Filial "0101"
	
	//Cria CTD atraves da SA2 
	fCriaForCTD()

	//Cria CTD atraves da SA1 
	fCriaCliCTD()
	
	//Reset Environment
	
Return


/*/{Protheus.doc} HATUSQL 
Rotina que cria CTD baseado nos fornecedores que ainda nao possuem conta contabil 
@author Melo
@since 15/01/2019
@version 1.0
/*/
Static Function fCriaForCTD()
Local cEoL		:= Chr(13)+Chr(10)
Local aAreaCTD 	:= CTD->(GetArea())
Local cForParam	:= Alltrim(SuperGetMV("HP_CONTFOR",.F.,"2"))
Local cQuery      := ""
Local cAliasSA2:= GetNextAlias()

cQuery += "SELECT "+cEoL
cQuery += "A2_COD, A2_LOJA, LEFT(A2_NOME,40) A2_NOME"+cEoL
cQuery += "FROM "+RetSqlName("SA2")+" SA2 WHERE D_E_L_E_T_ = '' AND "+cEoL
cQuery += "(SELECT TOP 1 CTD_ITEM FROM "+RetSqlName("CTD")+" CTD  "+cEoL
cQuery += "WHERE CTD.D_E_L_E_T_ = '' AND CTD_ITEM = '"+cForParam+"'+A2_COD+A2_LOJA) IS NULL "+cEoL

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),cAliasSA2, .F., .T.)       

(cAliasSA2)->(dbgotop())

While (cAliasSA2)->(!Eof()) 

	dbSelectArea("CTD")
	CTD->(dbSetOrder(1))
	CTD->(dbGoTop())

	If !Dbseek(xFilial("CTD")+cForParam+(cAliasSA2)->A2_COD+(cAliasSA2)->A2_LOJA)
		RecLock("CTD",.T.)
		CTD->CTD_FILIAL	:= xFilial("CTD")											//Filial		
		CTD->CTD_ITEM	:= cForParam+(cAliasSA2)->A2_COD+(cAliasSA2)->A2_LOJA		//Item Conta		
		CTD->CTD_CLASSE	:= '2'														//Classe				1=Sintetico;2=Analitico
		CTD->CTD_NORMAL	:= '1'														//Cond Normal			0=Nenhum;1=Despesa;2=Receita
		CTD->CTD_DESC01	:= (cAliasSA2)->A2_NOME										//Desc Moeda 1		
		CTD->CTD_BLOQ	:= "2"														//Item Bloq				1=Bloqueado;2=N�o Bloqueado
		CTD->CTD_DTEXIS	:= Stod("20010101")												//Dt Ini Exist		
		CTD->(MsUnlock())
	EndIf
	(cAliasSA2)->(dbSkip()) 
Enddo

Return 

/*/{Protheus.doc} HATUSQL 
Rotina que cria CTD baseado nos clientes que ainda nao possuem conta contabil 
@author Melo
@since 15/01/2019
@version 1.0
/*/
Static Function fCriaCliCTD()
Local cEoL		:= Chr(13)+Chr(10)
Local aAreaCTD 	:= CTD->(GetArea())
Local cCliParam	:= Alltrim(SuperGetMV("HP_CONTCLI",.F.,"1"))
Local cQuery	:= ""
Local cAliasSA1:= GetNextAlias()

cQuery += "SELECT "+cEoL
cQuery += "A1_COD, A1_LOJA,LEFT(A1_NOME,40) A1_NOME "+cEoL
cQuery += "FROM "+RetSqlName("SA1")+" SA1  WHERE D_E_L_E_T_ = '' AND "+cEoL
cQuery += "(SELECT TOP 1 CTD_ITEM FROM "+RetSqlName("CTD")+" CTD  "   +cEoL
cQuery += "WHERE CTD.D_E_L_E_T_ = '' AND CTD_ITEM = '"+cCliParam+"' +A1_COD+A1_LOJA) IS NULL"   +cEoL
cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),cAliasSA1, .F., .T.)       

(cAliasSA1)->(dbgotop())

While (cAliasSA1)->(!Eof()) 
	dbSelectArea("CTD")
	CTD->(dbSetOrder(1))
	CTD->(dbGoTop())

	If !Dbseek(xFilial("CTD")+cCliParam+(cAliasSA1)->A1_COD+(cAliasSA1)->A1_LOJA)
		RecLock("CTD",.T.)
		CTD->CTD_FILIAL	:= xFilial("CTD")										//Filial		
		CTD->CTD_ITEM	:= cCliParam+(cAliasSA1)->A1_COD+(cAliasSA1)->A1_LOJA	//Item Conta		
		CTD->CTD_CLASSE	:= '2'													//Classe				1=Sintetico;2=Analitico
		CTD->CTD_NORMAL	:= '1'													//Cond Normal			0=Nenhum;1=Despesa;2=Receita
		CTD->CTD_DESC01	:= (cAliasSA1)->A1_NOME									//Desc Moeda 1
		CTD->CTD_BLOQ	:= "2"													//Item Bloq				1=Bloqueado;2=N�o Bloqueado
		CTD->CTD_DTEXIS	:= stod("20010101")												//Dt Ini Exist)
	EndIf
	(cAliasSA1)->(dbSkip()) 
Enddo

Return 