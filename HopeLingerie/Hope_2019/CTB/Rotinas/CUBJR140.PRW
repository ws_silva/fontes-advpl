#INCLUDE "PROTHEUS.CH"
/*
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
���������������������������������������������������������������������������Ŀ��
���Fun��o    � CUBJR140     �Autor � Geraldo Sabino       �Data� 11.08.17   ���
���������������������������������������������������������������������������Ĵ��
���Descri��o � Relatorio para verificacao da retencao de impostos PCC       ���
���������������������������������������������������������������������������Ĵ��
���         ATUALIZACOES SOFRIDAS DESDE A CONSTRU�AO INICIAL.               ���
���������������������������������������������������������������������������Ĵ��
���Programador � Data   � BOPS �  Manutencao Efetuada                       ���
���������������������������������������������������������������������������Ĵ��
���            �        �      �                                            ���
�������������������������������������������������������������������������������
�������������������������������������������������������������������������������
*/
user Function CUBJR140()
//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������
Local cDesc1         := "Este programa tem como objetivo imprimir relatorio "
Local cDesc2         := "Demonstra Retencoes PCC de acordo com os parametros informaDos pelo usuario."
Local cDesc3         := "Relatorio Demonstrativo das Retencoes PCC"
Local cPict          := " "
Local nLin           := 80

Local imprime        := .T.
Local aPergunta      := {}
Private Titulo       := "Rel. Demonstrativo Pis, Cofins, Csll"

Private Titulo0      := "Rel. Demonstrativo Pis, Cofins, Csll (Filiais selecionadas para o relatorio)"

Private aOrd			:= {}
private aCampos      := {}
private cNomArq      := ""
Private lAbortPrint  := .F.
Private limite       := 80
Private tamanho      := "G"
Private nomeprog     := "FINR895"
Private nTipo        := 18
Private aReturn      := { "ZebraDo", 1, "Administracao", 2, 2, 1, "", 1} //"ZebraDo"###"Administracao"
Private nLastKey     := 0
Private cbtxt        := Space(10)
Private cbcont       := 00
Private wnrel        := "FIR895"
Private cString      := "SE2"
Private cPerg        := "FIR895"
Private m_pag        := 01
private cArqTRB      := " "

lProcessou := .F.

Pergunte("FIR895",.T.)
xFINR895Rpt()
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������Ŀ��
���Fun��o    � XFINR895Rpt   �Autor � Cristiano Denardi    �Data� 02.05.05 ���
�������������������������������������������������������������������������Ĵ��
���Descri��o � Processamento do relatorio								           ���
��������������������������������������������������������������������������ٱ�
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function XFINR895Rpt()
Local nPIS 			:= 0
Local nCOF 			:= 0
Local nCSL 			:= 0
Local nPISRet		:= 0
Local nCOFRet		:= 0
Local nCSLRet		:= 0
Local nBase		:= 0
Local nTotPIS		:= 0
Local nTotCOF		:= 0
Local nTotCSL		:= 0
Local nTotPISRet	:= 0
Local nTotCOFRet	:= 0
Local nTotCSLRet	:= 0
Local lFina374		:= IsInCallStack("FINA374")
Local aArqs		    := {}
Local aAreaSM0
Local lGestao       := ( FWSizeFilial() > 2 )
Local lSE2Excl      := Iif( lGestao, FWModeAccess("SE2",1) == "E", FWModeAccess("SE2",3) == "E")
Local aSM0          := {}
Local aSelFil       := {}
Local nC := 0
Local lPrtFil       := .T.
Local cFilialAtu    := cFilAnt

aStruct:={}
aAdd(aStruct,{"EMPRESA"    	,"C"	,04                  ,0})
aAdd(aStruct,{"FORNECE"    	,"C"	,06                  ,0})
aAdd(aStruct,{"LOJA"       	,"C"	,04                  ,0})
aAdd(aStruct,{"NOME"    	,"C"	,30                  ,0})
aAdd(aStruct,{"PREFIXO"    	,"C"	,03                  ,2})
aAdd(aStruct,{"NUMERO"    	,"C"	,09    				 ,2})
aAdd(aStruct,{"PARCELA"    	,"C"	,03                  ,2})
aAdd(aStruct,{"TIPO"      	,"C"	,03    				 ,2})
aAdd(aStruct,{"DTENTR"     	,"D"	,8                   ,2})
aAdd(aStruct,{"BAIXA"      	,"D"	,8                   ,2})
aAdd(aStruct,{"PIS"        	,"N"	,13                  ,2})
aAdd(aStruct,{"COF"        	,"N"	,13                  ,2})
aAdd(aStruct,{"CSL"        	,"N"	,13                  ,2})
aAdd(aStruct,{"PISRET"     	,"N"	,13                  ,2})
aAdd(aStruct,{"COFRET"     	,"N"	,13                  ,2})
aAdd(aStruct,{"CSLRET"     	,"N"	,13                  ,2})
aAdd(aStruct,{"ISS"        	,"N"	,13                  ,2})
aAdd(aStruct,{"IRRF"       	,"N"	,13                  ,2})
aAdd(aStruct,{"INSS"       	,"N"	,13                  ,2})
aAdd(aStruct,{"BASEPCC"    	,"N"	,13                  ,2})
aAdd(aStruct,{"OBS"        	,"C"	,40                  ,0})

If (Select("TRB3") <> 0)
	dbSelectArea("TRB3")
	TRB3->(dbCloseArea ())
Endif

cArqTrb  := CriaTrab(aStruct,.T.)
cIndice  := Alltrim(CriaTrab(,.F.))
dbUseArea(.T.,"DBFCDX",cArqTrb ,"TRB3",.T.,.F.)
IndRegua("TRB3", cIndice , "EMPRESA+FORNECE+LOJA+PREFIXO+NUMERO" 	,,, "Indice Descricao...")


dBSelectarea("SM0")
dBGotop() 
aSelFil:={} 
                                           
AADD(aSelFil,Alltrim(SM0->M0_CODFIL))

While ! Eof()
         
      AADD(aSelFil,Alltrim(SM0->M0_CODFIL))
 
      dBSelectarea("SM0")
      dBSkip()
 Enddo


For nC := 1 To Len(aSelFil)
	cFilAnt     := aSelFil[nC]
	
	aArqs		:= {}
	lProcessou := .F.
	
	If Select("TMP") > 0
		TMP->(dbCloseArea())
	EndIf
	
	If !lProcessou
		aArqs := F374TRB(@lProcessou)
	Endif
	
	
	dDataIni := CToD("01/"+Str(mv_par01)+"/"+Str(mv_par02))
	dDataFim := LastDay(CTOD("01/"+Str(mv_par01)+"/"+Str(mv_par02), "ddmmyy"))
	
	cAliasTmp := GetNextAlias()
	cQuery := "SELECT R_E_C_N_O_ RECSE2 FROM "+RetSqlName("SE2")+" SE2A WHERE "
 	cQuery += " E2_FORNECE BETWEEN '" + mv_par03 + "' AND '" + mv_par04 + "'  AND  "   // (E2_PIS+E2_COFINS+E2_CSLL) = 0 AND "
	cQuery += " ( E2_IRRF > 0 OR E2_INSS > 0 OR E2_ISS > 0 ) AND "
	//cQuery += " ((E2_NUMBOR <> '' AND E2_DTBORDE BETWEEN '"+Dtos( dDataIni) +"' AND '"+Dtos( dDataFim) +"') OR "
	cQuery += "  E2_EMIS1 BETWEEN '"+Dtos( dDataIni) +"' AND '"+Dtos( dDataFim) +"' AND "
	//cQuery += "(E2_PRETPIS <> '1' OR E2_PRETCOF <> '1' OR E2_PRETCSL <> '1' ) AND "
	cQuery += " SE2A.D_E_L_E_T_ = ' ' "
	cQuery:= ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasTMP,.F.,.T.)
	
	dbselectarea (cAliasTMP)
	dbGoTop()
	
	While !(cAliasTMP)->(EOF())
		
		dbselectarea(cAliasTMP)		
		DbSelectArea("SE2")
		dbSetOrder(1)
		DbGoto((cAliasTMP)->RECSE2)
		
		//ALERT(SE2->E2_NOMFOR)
		
		
		IF lSE2Excl
		   IF !alltrim(SE2->E2_FILIAL) == Alltrim(aSelFil[nc]) 
			  //ALERT("NAP ACHEI "+SE2->E2_NOMFOR)
			  dbselectarea(cAliasTMP)
		      dBSkip()                      
		      
		      Loop
		   Endif
		    
		ELSE
		                   
		   IF !alltrim(SE2->E2_FILORIG) == Alltrim(aSelFil[nc]) 
			  dbselectarea(cAliasTMP)		
		      dBSkip()                      
		      Loop
		      //ALERT(SE2->E2_NOMFOR)
		   Endif
		
		ENDIF
		
		
		cFornece 	:=	SE2->E2_FORNECE
		cLoja		:=  SE2->E2_LOJA
		
		//Posiciona SA2
		SA2->(dbSetOrder(1))
		SA2->(MsSeek(xFilial("SA2")+cFornece+cLoja))
	                
	    IF cFornece = "001898"
	       //alert("achei")
	    endif 
	    
	    IF SA2->A2_COD = "001898"
	       //ALERT("PPPP")
	    ENDIF
		
		//ARQUIVO ANALITICO
		RecLock("TRB",.T.)
		TRB->MES 		:= STR(mv_par01)
		TRB->ANO   		:= STR(mv_par02)
		TRB->FORNECE	:= cFornece
		TRB->LOJA     	:= cLoja
		TRB->NOME  		:= SA2->A2_NOME
		TRB->PREFIXO	:= SE2->E2_PREFIXO
		TRB->NUM 		:= SE2->E2_NUM
		TRB->PARCELA	:= SE2->E2_PARCELA
		TRB->TIPO		:= SE2->E2_TIPO
		TRB->BORDERO	:= SE2->E2_NUMBOR
		TRB->DTBORD		:= SE2->E2_DTBORDE
		TRB->BAIXA 		:= SE2->E2_BAIXA
		TRB->SALDO	 	:= SE2->E2_SALDO
		TRB->RECSE2     := SE2->(RECNO())
		MsUnlock()
		
		dbselectarea(cAliasTMP)
		dbSkip()
	EndDo
	
	dbSelectArea(cAliasTMP)
	dbCloseArea()               
	//ALERT("SSSS")
	
	DbSelectArea("TRB")
	dBGotop()
	While ! Eof()
		
		dBSelectarea("SE2")
		dBGoto(TRB->RECSE2)

		// Busca a Nota Fiscal Original
		dBSelectarea("SD1")
		dBSetOrder(1)
		_dDataEnt:=ctod("")
		IF dBSeek(SE2->E2_FILORIG+LEFT(TRB->NUM,9)+LEFT(TRB->PREFIXO,3)+TRB->FORNECE+TRB->LOJA,.T.)
			_dDataEnt := SD1->D1_DTDIGIT
		Endif
		
		dBSelectarea("TRB3")
		dBSetOrder(1)
		IF !dBSeek(SE2->E2_FILORIG+TRB->(FORNECE+LOJA+PREFIXO+NUM))
			TRB3->(Reclock("TRB3",.T.))   
			
			IF SE2->E2_FORNECE="001898"
	//		ALERT(LEN(TRB->FORNECE))
	//		ALERT(LEN(TRB->LOJA))
	//		ALERT(LEN(TRB->PREFIXO))
	//		ALERT(LEN(TRB->NUM))
	//		FINAL("SSSS")			  
			ENDIF
		Else
			TRB3->(Reclock("TRB3",.F.))
		Endif
		                           
		                     
		IF TRB->FORNECE = '001898'         
		//ALERT("SSSS")
		//ALERT(SE2->E2_NOMFOR)     
		ENDIF
		
		TRB3->EMPRESA  := SE2->E2_FILORIG
		TRB3->FORNECE  := TRB->FORNECE
		TRB3->LOJA     := TRB->LOJA
		TRB3->NOME     := TRB->NOME 
		TRB3->PREFIXO  := TRB->PREFIXO
		TRB3->NUMERO   := TRB->NUM
		TRB3->PARCELA  := TRB->PARCELA
		TRB3->TIPO     := TRB->TIPO
		
		IF !Empty(_dDataEnt)
			TRB3->DTENTR   := _dDataEnt
		ENDIF
		TRB3->BAIXA    := TRB->BAIXA
		TRB3->PIS      := TRB->PIS
		TRB3->COF      := TRB->COF
		TRB3->CSL      := TRB->CSL
		TRB3->PISRET   := TRB->PISRET
		TRB3->COFRET   := TRB->COFRET
		TRB3->CSLRET   := TRB->CSLRET
		TRB3->BASEPCC  := TRB->BASEPCC
		TRB3->ISS      := SE2->E2_ISS 
		TRB3->IRRF     := SE2->E2_IRRF
		TRB3->INSS     := SE2->E2_INSS		

        IF TRB3->(PIS+COF+CSL)=0 
     	   _cObs          := " Obtidos em Query Complementar "
        ELSE
     	   _cObs          := " Obtidos com Query da Rotina "
        ENDIF
        
		TRB3->OBS      := _cObs
		TRB3->(MSUnlock())
		
		
		dBSelectarea("TRB")
		dBSkip()
	Enddo
	
	If !lFina374
		dbSelectArea("TRB")
		dbCloseArea()
		fErase(aArqs[1]+GetDBExtension())
		fErase(aArqs[1]+OrdbagExt())
		
		dbSelectArea("TRB2")
		dbCloseArea()
		fErase(aArqs[2]+GetDBExtension())
		fErase(aArqs[2]+OrdbagExt())
	Endif
	cFilAnt := cFilialAtu
Next

FISImpExcel()
Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �FISIMPEXCEL  �Autor  �Microsiga        � Data �  11/30/15   ���
�������������������������������������������������������������������������͹��
���Desc.     �                                                            ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � AP                                                        ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
STATIC Function FISIMPEXCEL()
Local oExcel
Local oExcelApp
Local cArq	   		:= ""
Local nI       		:= 0
Local nX       		:= 0
Local cDirTmp  		:= "C:\temp"
Local cPlan			:= ""
Local lDataCpo		:=.F.
Local lStringCpo	:=.F.
Local aDados:={}
Local cDestino	:= cGetFile(, "Selecione o Destino do arquivo",0,"",.F.,GETF_LOCALFLOPPY+GETF_LOCALHARD+GETF_NETWORKDRIVE+GETF_RETDIRECTORY)

//Cria objeto
oExcel := FWMSEXCEL():New()

//Inclui nova Planilha no Arquivo
cPlan	:= "RELACAO DOS TITULOS  "
oExcel:AddworkSheet(cPlan)

//Adiciona Tabela
aColumns 	:= {"Empresa","Estabelecimento","Cod Fornecedor","Loja","Razao Social","Pref","Numero","Parc","Tipo","Data.Entrada","Baixa","Pis","Cof","Csll","Pisret","Cofret","Cslret","BasePcc","ISS","IRRF","INSS","Observa��o"}
cTab	:= "LISTAGEM DE TITULOS - GERADO por "+Alltrim(cUserName)+" em "+DtoC(Date())+" as "+Time()
oExcel:AddTable(cPlan,cTab)

// Cria Cabecalho das informa��es
For nI := 1 To Len(aColumns)
	oExcel:AddColumn(cPlan,cTab,aColumns[nI],1,1)
Next nI

dBselectarea("TRB3")
dBGotop()

_aAreaSM0:=SM0->(GetArea())

While ! TRB3->(Eof())
	       
	
	
	
	dBSelectarea("SM0")
	dBSetOrder(1)
	dBSeek(SM0->M0_CODIGO+ALLTRIM(TRB3->EMPRESA),.T.)
	
	//ALERT(NOME)
	
	_aDados:={}
	_c01       := TRB3->EMPRESA
	_c02       := SM0->M0_FILIAL
	_c03       := TRB3->FORNECE
	_c04       := TRB3->LOJA
	_c05       := TRB3->NOME
	_c06       := TRB3->PREFIXO
	_c07       := TRB3->NUMERO
	_c08       := TRB3->PARCELA
	_c09       := TRB3->TIPO
	_c10       := TRB3->DTENTR
	_c11       := TRB3->BAIXA
	_c12       := TRB3->PIS
	_c13       := TRB3->COF
	_c14       := TRB3->CSL
	_c15       := TRB3->PISRET
	_c16       := TRB3->COFRET
	_c17       := TRB3->CSLRET
	_c18       := TRB3->BASEPCC
	
	_c19       := TRB3->ISS
	_c20       := TRB3->IRRF
	_c21       := TRB3->INSS
	
	_c22       := TRB3->OBS
	
	
	aDados:={}
	aadd(aDados,      _c01                  ,"C", 04,0) // EMPRESA
	aadd(adados,      _c02                  ,"C", 30,0) // RAZAO EMPRESA
	aadd(adados,      _c03                  ,"C", 06,0) // FORNECEDOR
	aadd(adados,      _c04                  ,"C", 04,0) // LOJA
	aadd(adados,      _c05                  ,"C", 50,0) // NOME
	aadd(adados,      _c06                  ,"C", 03,0) // PREFIXO
	aadd(adados,      _c07                  ,"C", 09,0) // NUMERO
	aadd(adados,      _c08                  ,"C", 03,0) // PARCELA
	aadd(adados,      _c09                  ,"C", 03,0) // TIPO
	aadd(adados,      _c10                  ,"D", 08,0) // DATA ENTRADA
	aadd(adados,      _c11                  ,"D", 08,0) // DATA BAIXA
	aadd(adados,      _c12                  ,"N", 13,2) // PIS
	aadd(adados,      _c13                  ,"N", 06,2) // COF
	aadd(adados,      _c14                  ,"N", 13,2) // CSL
	aadd(adados,      _c15                  ,"N", 13,2) // PISRET
	aadd(adados,      _c16                  ,"N", 13,2) // COFRET
	aadd(adados,      _c17                  ,"N", 13,2) // CSL RET
	aadd(adados,      _c18                  ,"N", 13,2) // BASE PCC
	
	aadd(adados,      _c19                  ,"N", 13,2) // ISS
	aadd(adados,      _c20                  ,"N", 13,2) // IRRF
	aadd(adados,      _c21                  ,"N", 13,2) // INSS
	
	aadd(adados,      _c22                  ,"C", 40,0) // OBS
	
	oExcel:AddRow(cPlan,cTab,aDados)
	
	
	dBselectarea("TRB3")
	dBskip()
	
Enddo
RestArea(_aAreaSM0)

dBselectarea("TRB3")
dBclosearea()

oExcel:Activate()

//Cria nome para arquivo
cArq := CriaTrab( NIL, .F. ) + ".xml"
cArq := "HOP_RETENC_" + DtoS(Date()) + "_" + StrTran(Time(),":") + ".XML"
oExcel:GetXMLFile( cArq )

//Copia arquivo para diret�rio tempor�rio do usu�rio
//e realiza abertura do mesmo
If __CopyFile( cArq, cDestino+"\"+ cArq )
	MsgInfo( "Relacao Retencao na Pasta " + cDestino )
Else
	MsgInfo( "Arquivo n�o copiado para tempor�rio do usu�rio." )
Endif

Return

