User Function LP_620tx()
Local aArea	:= GetArea()
Local nVltx := posicione("SE1",1,xFilial("SE1")+SF2->F2_SERIE+SF2->F2_DOC,"SE1->E1_VLTXCC")
Local nValor:= 0
Private nRet:= ""

//IIF(posicione("SE1",1,xFilial("SE1")+SF2->F2_SERIE+SF2->F2_DOC,"SE1->E1_XTXCC")>0,SE1->E1_VLTXCC,0)

IF nVltx <> 0
   DbSelectarea("SE1")
   SE1->(DbSetOrder(1))
   SE1->(DbSeek(xfilial("SE1")+SF2->F2_SERIE+SF2->F2_DOC))
   While !EOF() .and. SF2->F2_SERIE+SF2->F2_DOC+SF2->F2_CLIENTE+SF2->F2_LOJA == SE1->E1_PREFIXO+SE1->E1_NUM+SE1->E1_CLIENTE+SE1->E1_LOJA
       nValor := nValor + SE1->E1_VLTXCC
       SE1->(DBSKIP())
   Enddo    
Endif
nRet:= nValor
RestArea( aArea )
Return(nRet)