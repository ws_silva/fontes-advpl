#Include "Protheus.ch"
#Include "TopConn.ch"
#INCLUDE "rwmake.ch"
#include "shell.ch"
#include "bitmap.ch"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RHESTP001 �Autor  �DANIEL R. MELO      � Data �  08/07/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Relatorio de Medidas e Tamanhos                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function RHEST002(cOrigem)

	Local cDesc1	:= "Este programa tem como objetivo imprimir relatorio "
	Local cDesc2	:= "de acordo com os parametros informados pelo usuario."
	Local cDesc3	:= ""
	Local titulo	:= "Relatorio de Medidas e Tamanhos"
	Local nLin		:= 80
	Local Cabec1	:= " "
//	Local Cabec1	:= " CODIGO          DESCRI��O DA MP                                          COR_MP  DESC_COR_MP       NOVA_COR  DESC_NOVA_COR        UN  TAM.NOVO"
	Local Cabec2	:= " "
	Local aOrd		:= {}

	Private lAbortPrint	:= .F.
	Private tamanho		:= "M"
	Private nomeprog	:= "RHEST002" // Coloque aqui o nome do programa para impressao no cabecalho
	Private nTipo		:= 18
	Private aReturn		:= { "Zebrado", 1, "Estoque/Custos", 2, 2, 1, "", 1}
	Private nLastKey	:= 0
	Private cPerg		:= PADR("RHEST002",10)
	Private m_pag		:= 01
	Private wnrel		:= "RHEST002" // Coloque aqui o nome do arquivo usado para impressao em disco
	Private cString 	:= ""
	Private cOrdemServ := ""

	If cOrigem
		
		cOrdemServ	:= SB4->B4_COD

	Else
	
		ValidPerg()
	
		Pergunte(cPerg,.T.)
		
		cOrdemServ	:= 	AllTrim(MV_PAR01)

	End

	wnrel := SetPrint(cString,NomeProg,cPerg,@titulo,cDesc1,cDesc2,cDesc3,.T.,aOrd,.T.,Tamanho,,.T.)

	If nLastKey == 27
		Return
	Endif

	SetDefault(aReturn,cString)

	If nLastKey == 27
		Return
	Endif

	nTipo := If(aReturn[4]==1,15,18)

	RptStatus({|| RunReport(Cabec1,Cabec2,Titulo,nLin) },Titulo)

Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �RUNREPORT �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Impress�o pelo Prothueus                                    ���
�������������������������������������������������������������������������ͼ��          '
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function RunReport(Cabec1,Cabec2,Titulo,nLin)

	Local _Modelo	   := AllTrim(cOrdemServ)
	Local limite	   := 132
	Local cCompo     := ""
	Local cCompos    := ""
	Local cCompos1   := ""
	Local cCompos2   := ""
	Local cCompos3   := ""
	Local _cTamanhos := ""
	Local _cCores    := ""
	Local _cNcol     := 0
	Local _cHora 	 := ""
	Local _nTotHora  := 0
	Local ccodcomp 	 := ""
	Local _i 		:= 0
	Local _x 		:= 0

	DbSelectArea("SB4")
	DbSetOrder(1)
	DbSeek(xfilial("SB4")+_Modelo)
	
	Cabec1	:= "MODELO = '"+AllTrim(_Modelo)+"'    DESCRICAO = '"+AllTrim(SB4->B4_DESC)+"' "
	
	If !SB4->(Eof())

		If lAbortPrint
			@nLin,00 PSAY "*** CANCELADO PELO OPERADOR ***"
			//Exit
		End

		If nLin > 55
			Cabec(Titulo,Cabec1,Cabec2,NomeProg,Tamanho,nTipo)
			nLin := 9
		End
		
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++
		@nLin, 000 PSAY "|"
		@nLin, 002 PSAY "TECIDO PRINCIPAL = " + AllTrim(SB4->B4_YTECIDO)
		@nLin, 080 PSAY "|"
//		@nLin, 082 PSAY "COLE��O = " + AllTrim(SB4->B4_YCOLECA) + " - " + AllTrim(SB4->B4_YNCOLEC)
		@nLin, 082 PSAY "CICLO: = " + AllTrim(SB4->B4_YCICLO) + " - " + AllTrim(SB4->B4_YNCICLO)
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		DbSelectArea("SZD")
		DbSetOrder(1)
		DbSeek(xfilial("SZD")+_Modelo)
		@nLin, 000 PSAY "|"
		@nLin, 002 PSAY "INSTRU��ES DE LAVAGEM:" + AllTrim(SZD->ZD_LAVAGE)
//		@nLin, 025 PSAY "\system\Imagens\01.jpg" //AllTrim(SZD->ZD_LAVAGE)
		@nLin, 080 PSAY "|"
		@nLin, 082 PSAY "SUB COLE��O: = " + AllTrim(SB4->B4_YNSUBCO) + " - " + AllTrim(SB4->B4_YSUBCOL)
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		cTexto	:= "COMPOSI��O: "
		For _i := 1 to len(AllTrim(SZD->ZD_COMPOS))
			cCompo  := SubStr(AllTrim(SZD->ZD_COMPOS),_i,10)
			cCompos := SubStr(cCompo,7,3) + "% "
			ccodcomp := SubStr(cCompo,1,5)
			cCompos += POSICIONE("ZAQ",1,xFilial("ZAQ")+ccodcomp,"ZAQ_DESC") //AllTrim(POSICIONE("SX5",1,xFilial("SX5")+'Z1'+ SubStr(cCompo,1,5),"X5_DESCRI"))
			cCompos += " / "
			If (Len(cTexto) + Len(cCompos1) + Len(cCompos)) < 130
				cCompos1 += cCompos
			ElseIf (Len(cTexto) + Len(cCompos2) + Len(cCompos)) < 130
				cCompos2 += cCompos
			Else
				cCompos3 += cCompos
			End
			_i += 9
		Next
		@nLin, 000 PSAY "|"
		@nLin, 002 PSAY cTexto + SubStr(cCompos1,1,len(cCompos1)-2)
		@nLin, 131 PSAY "|"
		nLin++
		If cCompos2<>""
			@nLin, 000 PSAY "|"
			@nLin, 014 PSAY SubStr(cCompos2,1,len(cCompos2)-2)
			@nLin, 131 PSAY "|"
			nLin++
		End
		If cCompos3<>""
			@nLin, 000 PSAY "|"
			@nLin, 014 PSAY SubStr(cCompos3,1,len(cCompos3)-2)
			@nLin, 131 PSAY "|"
			nLin++
		End
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		aTaman	:= VERTAM()
		For _i := 1 to Len(aTaman)
			_cTamanhos += "  " + AllTrim(aTaman[_i][1]) + "  "
		Next
		@nLin, 000 PSAY "|"
		@nLin, 002 PSAY "	TAMANHOS: " + _cTamanhos
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		@nLin, 000 PSAY "|"
		@nLin, 021 PSAY "CORES"
		@nLin, 045 PSAY "|"
		@nLin, 056 PSAY "COLOCA��O DE ETIQUETAS"
		@nLin, 088 PSAY "|"
		@nLin, 101 PSAY "COLOCA��O DE TAGS"
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		aCores	:= VERCOR()
		For _i := 1 to Len(aCores)
			@nLin, 000 PSAY "|"
			@nLin, 004 PSAY AllTrim(aCores[_i][1]) + "  " + AllTrim(aCores[_i][2])
			@nLin, 045 PSAY "|"
			@nLin, 088 PSAY "|"
			@nLin, 131 PSAY "|"
			nLin++
		Next
		@nLin, 000 PSAY "|"
		@nLin, 045 PSAY "|"
		@nLin, 088 PSAY "|"
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		@nLin, 000 PSAY "|"
		@nLin, 020 PSAY "DESENHO T�CNICO"
		@nLin, 053 PSAY "|"
		@nLin, 089 PSAY "MEDIDAS"
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		aMedidas	:= VERMED()
		@nLin, 000 PSAY "|"
		@nLin, 053 PSAY "|"
		_cNcol	:= 085
		For _x := 1 to Len(aTaman)
			@nLin, _cNcol PSAY AllTrim(aTaman[_x,1])
			_cNcol += 7
		Next _i
		@nLin, 131 PSAY "|"
		nLin++

		For _i := 1 to Len(aMedidas)
			@nLin, 000 PSAY "|"
			@nLin, 053 PSAY "|"
			@nLin, 055 PSAY SubStr(AllTrim(aMedidas[_i,1]),1,30)
			_cNcol	:= 085
			For _x := 1 to (Len(aMedidas[_i])-2)
				If AllTrim(aMedidas[_i,_x+1])<>""
					@nLin, _cNcol PSAY AllTrim(aMedidas[_i,_x+1])
				End
				_cNcol += 7
			Next _x
			@nLin, 131 PSAY "|"
			nLin++
		Next _i
		@nLin, 000 PSAY "|"
		@nLin, 053 PSAY "|"
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		@nLin, 000 PSAY "|"
		@nLin, 056 PSAY "SEQ��NCIA OPERACIONAL"
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

		aRoteiro	:= VERROT()
		For _i := 1 to Len(aRoteiro)
			@nLin, 000 PSAY "|"
			@nLin, 002 PSAY SubStr(AllTrim(aRoteiro[_i,1]),1,65)
			@nLin, 072 PSAY SubStr(AllTrim(aRoteiro[_i,2]),1,45)
			_cHora := StrZero(aRoteiro[_i,3],4)
			@nLin, 125 PSAY SubStr(_cHora,1,2)+":"+SubStr(_cHora,3,2)
			_nTotHora += aRoteiro[_i,3]
			@nLin, 131 PSAY "|"
			nLin++
		Next _i
		@nLin, 000 PSAY "|"
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++
		
		aServ	:= VERSERV()
		@nLin, 000 PSAY "|"
		@nLin, 004 PSAY "TEMPO TOTAL/ESTRUTURA =  " + Alltrim(str(aServ[1,4]))
		@nLin, 105 PSAY "TEMPO TOTAL =  " + Alltrim(str(Round(_nTotHora/60,2)))
		@nLin, 131 PSAY "|"
		nLin++
		@nLin, 000 PSAY REPLICATE("-",limite)
		nLin++

	End

	SB4->(dbCloseArea())

	SET DEVICE TO SCREEN

	If aReturn[5]==1
		dbCommitAll()
		SET PRINTER TO
		OurSpool(wnrel)
	Endif

	MS_FLUSH()

Return


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �VERTAM    �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para Pegar os tamanhos utilizados                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERTAM()

	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery1
	Local cQuery2
	Local aFields   := {}
	Local aHeaderEx := {}
	Local _x := 0
	Local _i := 0

	cColuna	:= POSICIONE("SB4",1,xFilial("SB4")+AllTrim(cOrdemServ),"B4_COLUNA")
 
//	Define field properties
	If alltrim(cColuna) <> ""
		cQuery1 := "SELECT BV_FILIAL, BV_TABELA, BV_CHAVE, BV_DESCRI "+cEOL
		cQuery1 += "FROM "+RetSQLName("SBV")+" "+cEOL
		cQuery1 += "WHERE D_E_L_E_T_ = '' "+cEOL
		cQuery1 += "		AND BV_FILIAL = '" +xFilial("SBV") +"' "+cEOL
		cQuery1 += "		AND BV_TABELA = '"+SB4->B4_COLUNA+"' "+cEOL
		cQuery1 += "ORDER BY R_E_C_N_O_ "+cEOL
	  
		If Select("TMPSBV") > 0
			TMPSBV->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSBV",.T.,.T.)

		cQuery2  := "SELECT * FROM "+RetSQLName("SZD")+" "+cEOL
		cQuery2  += "WHERE D_E_L_E_T_ = ' ' "+cEOL
		cQuery2  += "		AND ZD_FILIAL = '"+xFilial("SZD")+"' "+cEOL
		cQuery2  += "		AND ZD_PRODUTO='"+AllTrim(cOrdemServ)+"' "+cEOL

		If Select("TMPSZD") > 0
			TMPSZD->(DbCloseArea())
		EndIf
		dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery2),"TMPSZD",.T.,.T.)
		DbSelectArea("TMPSZD")
		TMPSZD->(DbGoTop())
		
		While TMPSZD->(!EOF())
			For _i := 1 to 200
				If &("TMPSZD->ZD_TAM"+StrZero(_i,3))=="X"
					_campo	:= "ZF_TAM"+StrZero(_i,3)
					
					DbSelectArea("TMPSBV")
					TMPSBV->(DbGoTop())
					For _x := 1 to _i
						_desc	:= TMPSBV->BV_DESCRI
						TMPSBV->(DbSkip())
					Next nX
					DbSelectArea("SX3")
					SX3->(DbSetOrder(2))
					If SX3->(DbSeek(_campo))
						nX := ASCAN( aFields, {|x| alltrim(x) == alltrim(_campo)})
						If nX = 0
							Aadd(aFields,_campo) //TMPSBV->BV_CHAVE)
							Aadd(aHeaderEx, {Space(5)+ alltrim(_desc)+ Space(5),SX3->X3_CAMPO,"@E 99",2,0,"",SX3->X3_USADO,"N","","R","",""})
						Endif
					Endif
				Endif
			Next
			TMPSZD->(DbSkip())
		EndDo
	End

Return (aHeaderEx)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �VERCOR    �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para pegar as cores novas                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERCOR()

	Local cEOL			:= +Chr(13)+Chr(10)
	Local cQuery1
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	cQuery1  := "SELECT ZD_COR, ZD_DESCRIC FROM "+RetSQLName("SZD")+" "+cEOL
	cQuery1  += "WHERE D_E_L_E_T_ = ' ' "+cEOL
	cQuery1  += "		AND ZD_FILIAL = '"+xFilial("SZD")+"' "+cEOL
	cQuery1  += "		AND ZD_PRODUTO='"+AllTrim(cOrdemServ)+"' "+cEOL

	If Select("TMPSZD") > 0
		TMPSZD->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSZD",.T.,.T.)
	DbSelectArea("TMPSZD")
	TMPSZD->(DbGoTop())
		
	While TMPSZD->(!EOF())
		Aadd(aFieldFill, TMPSZD->ZD_COR)
		Aadd(aFieldFill, TMPSZD->ZD_DESCRIC)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �VERMED    �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para pegar as medidas                                 ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERMED()

	Local cEOL	     := +Chr(13)+Chr(10)
	Local cQuery1
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}
	Local _i := 0

	aTaman	:= VERTAM()

	cQuery1 := "SELECT ZN_PARTES"
	For _i := 1 to Len(aTaman)
		cQuery1 += ", ZN_" + Substr(AllTrim(aTaman[_i,2]),4,10) +" AS 'ZN_"+ AllTrim(aTaman[_i,1]) +"' "
	Next
	cQuery1 += " "+cEOL
	cQuery1 += "FROM "+RetSQLName("SZN")+" "+cEOL
	cQuery1 += "WHERE D_E_L_E_T_ = '' "+cEOL
	cQuery1 += "		AND ZN_FILIAL = '" +xFilial("SZN") +"' "+cEOL
	cQuery1 += "		AND ZN_PRODUTO = '"+AllTrim(cOrdemServ)+"' "+cEOL

	If Select("TMPSZN") > 0
		TMPSZN->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSZN",.T.,.T.)
	DbSelectArea("TMPSZN")
	TMPSZN->(DbGoTop())
		
	While TMPSZN->(!EOF())
		Aadd(aFieldFill, TMPSZN->ZN_PARTES)
		For _i := 1 to Len(aTaman)
			_cCampo  := "TMPSZN->ZN_" + AllTrim(aTaman[_i,1])
			Aadd(aFieldFill, AllTrim(&_cCampo) )
		Next
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �VERROT    �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para pegar o roteiro de opera��es                     ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERROT()

	Local cEOL			:= +Chr(13)+Chr(10)
	Local cQuery1
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}

	cQuery1  := "SELECT ZO_DESCRI, ZO_OBSMAQ, ZO_TEMPO FROM "+RetSQLName("SZO")+" "+cEOL
	cQuery1  += "WHERE D_E_L_E_T_ = ' ' "+cEOL
	cQuery1  += "		AND ZO_FILIAL = '"+xFilial("SZD")+"' "+cEOL
	cQuery1  += "		AND ZO_PRODUTO='"+AllTrim(cOrdemServ)+"' "+cEOL

	If Select("TMPSZO") > 0
		TMPSZO->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSZO",.T.,.T.)
	DbSelectArea("TMPSZO")
	TMPSZO->(DbGoTop())
		
	While TMPSZO->(!EOF())
		Aadd(aFieldFill, TMPSZO->ZO_DESCRI)
		Aadd(aFieldFill, TMPSZO->ZO_OBSMAQ)
		Aadd(aFieldFill, TMPSZO->ZO_TEMPO)
		Aadd(aFieldFill, .F.)
		Aadd(aColsEx, aFieldFill)
		aFieldFill :={}
		DbSkip()
	EndDo
		
Return (aColsEx)


/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �VERSERV   �Autor  �DANIEL R. MELO      � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Desc.     �Query para pegar o tempo de Servi�o                         ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function VERSERV()

	Local cEOL			:= +Chr(13)+Chr(10)
	Local cQuery1
	Local aHeaderEx	:= {}
	Local aFieldFill	:= {}
	Local aColsEx		:= {}
	Local _tempo		:= 0
	Local cCampo		:= ""
	Local _i := 0

	aTaman	:= VERTAM()

	cQuery1 := "SELECT ZE_PRODUTO AS 'FICHA_TECNICA', ZE_COMP AS 'COMP', ZE_DESCCOM AS 'DESCCOMP' "+cEOL
	For _i := 1 to Len(aTaman)
		cQuery1 += ", ZF_" + Substr(AllTrim(aTaman[_i,2]),4,10) +" AS '"+ Substr(AllTrim(aTaman[_i,2]),4,10) +"' "
	Next
	cQuery1 += " "+cEOL
	cQuery1 +="FROM "+RetSQLName("SZE")+" SZE (NOLOCK) "+cEOL
	cQuery1 +="	INNER JOIN "+RetSQLName("SB4")+" AS SB4 (NOLOCK) ON B4_COD=ZE_PRODUTO AND SB4.D_E_L_E_T_<>'*' "+cEOL
	cQuery1 +="	INNER JOIN "+RetSQLName("SZG")+" AS SZG (NOLOCK) ON ZG_PRODUTO=ZE_PRODUTO "+cEOL
	cQuery1 +="			AND ZG_REVISAO=ZE_REVISAO AND ZE_COMP=ZG_COMP AND SZG.D_E_L_E_T_<>'*' "+cEOL
	cQuery1 +="	INNER JOIN "+RetSQLName("SZF")+" AS SZF (NOLOCK) ON ZF_PRODUTO=ZE_PRODUTO AND ZF_REVISAO=ZE_REVISAO "+cEOL
	cQuery1 +="			AND ZF_COMP=ZG_COMP AND ZG_COR=ZF_COR AND SZF.D_E_L_E_T_<>'*' "+cEOL
	cQuery1 +="WHERE SZE.D_E_L_E_T_<>'*' "+cEOL
	cQuery1 +="		AND ZE_PRODUTO='"+AllTrim(cOrdemServ)+"' "+cEOL
	cQuery1 +="		AND ZE_COMP LIKE 'SERV%' "+cEOL
	cQuery1 +="GROUP BY ZE_PRODUTO, ZE_COMP, ZE_DESCCOM "
	For _i := 1 to Len(aTaman)
		cQuery1 += ", ZF_" + Substr(AllTrim(aTaman[_i,2]),4,10)
	Next
	cQuery1 += " "+cEOL

	If Select("TMPSZE") > 0
		TMPSZE->(DbCloseArea())
	EndIf
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery1),"TMPSZE",.T.,.T.)
	DbSelectArea("TMPSZE")
	
	Aadd(aFieldFill, TMPSZE->FICHA_TECNICA)
	Aadd(aFieldFill, TMPSZE->COMP)
	Aadd(aFieldFill, TMPSZE->DESCCOMP)
	For _i := 1 to Len(aTaman)
		cCampo  := "TMPSZE->"+Substr(AllTrim(aTaman[_i,2]),4,10)
		If _tempo < &cCampo
			_tempo  := &cCampo
		end
	Next
	Aadd(aFieldFill, _tempo)
	Aadd(aFieldFill, .F.)
	Aadd(aColsEx, aFieldFill)
	aFieldFill :={}
	DbSkip()
		
Return (aColsEx)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Fun��o    �VALIDPERG � Autor � AP5 IDE            � Data �  24/01/2017 ���
�������������������������������������������������������������������������͹��
���Descri��o � Verifica a existencia das perguntas criando-as caso seja   ���
���          � necessario (caso nao existam).                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
Static Function ValidPerg()

	Local _sAlias := Alias()
	Local aRegs := {}
	Local i,j

	dbSelectArea("SX1")
	dbSetOrder(1)
	cPerg := PADR(cPerg,10)
//��������������������������������������������������������������Ŀ
//� Variaveis utilizadas para parametros                         �
//� mv_par01              Produto                                �
//� mv_par02              Servi�o Linha                          �
//����������������������������������������������������������������

	//Grupo/Ordem/Pergunta/Variavel/Tipo/Tamanho/Decimal/Presel/GSC/Valid/                             Var01/ Def01     /Cnt01/Var02/Def02/Cnt02/Var03/Def03/Cnt03/Var04/Def04/Cnt04/Var05/Def05/Cnt05/F3
	/*1*/aAdd(aRegs,{cPerg,"01","Produto                   ? ","","","MV_CH1","C",15,0,0,"G","","MV_PAR01",""   ,"","",""        ,"",""   ,"","","","","","","","","","","","","","","","","","","",""   })

	For i:=1 to Len(aRegs)
		If !dbSeek(cPerg+aRegs[i,2])
			RecLock("SX1",.T.)
			For j:=1 to FCount()
				If j <= Len(aRegs[i])
					FieldPut(j,aRegs[i,j])
				Endif
			Next
			MsUnlock()
		Endif
	Next

	DbSelectArea(_sAlias)

Return Nil