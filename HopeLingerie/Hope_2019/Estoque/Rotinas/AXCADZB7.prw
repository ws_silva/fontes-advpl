#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} AXCADZB7
Fun��o de Teste
@Desc PROGRAMA PARA MONTAR TELA DE CADASTRO DA TABELA ZB7 - Ciclo SKU 
@type function
@author R. Melo 
@since 12/06/2019
@version 1.0
/*/
User Function AXCADZB7()
    Local aArea    := GetArea()
    Local aAreaZB7 := ZB7->(GetArea())
    Local cDelOk   := ".T."
    Local cFunTOk  := ".T."
     
    //Chamando a tela de cadastros
    AxCadastro('ZB7', 'Ciclo SKU', cDelOk, cFunTOk)
     
    RestArea(aAreaZB7)
    RestArea(aArea)
Return