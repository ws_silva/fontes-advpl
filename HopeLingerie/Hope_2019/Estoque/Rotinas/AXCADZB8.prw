#include 'protheus.ch'
#include 'parmtype.ch'

/*/{Protheus.doc} AXCADZB8
Fun��o de Teste
@Desc PROGRAMA PARA MONTAR TELA DE CADASTRO DA TABELA ZB8 - Esta��o 
@type function
@author R. Melo 
@since 12/06/2019
@version 1.0
/*/
User Function AXCADZB8()
    Local aArea    := GetArea()
    Local aAreaZB8 := ZB8->(GetArea())
    Local cDelOk   := ".T."
    Local cFunTOk  := ".T."
     
    //Chamando a tela de cadastros
    AxCadastro('ZB8', 'Esta��o SKU', cDelOk, cFunTOk)
     
    RestArea(aAreaZB8)
    RestArea(aArea)
Return