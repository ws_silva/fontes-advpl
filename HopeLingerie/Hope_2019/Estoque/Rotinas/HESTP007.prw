#INCLUDE "TOPCONN.CH"
#include "rwmake.ch"
#INCLUDE "TBICONN.CH"
#INCLUDE "PROTHEUS.CH"

#Define CRLF  CHR(13)+CHR(10)


User Function HESTP007()

Local cQuery   := ""

Prepare Environment Empresa "01" Filial "0101"

If Select("TRB") > 0 
	TRB->(DbCloseArea()) 
EndIf                     

cQuery := "UPDATE "+RetSqlName("SB2")+" SET B2_XRES = 0 WHERE D_E_L_E_T_ = '' AND B2_XRES <> 0"
TcSqlExec(cQuery)

cQuery := "UPDATE SB2010 SET B2_XRES =  "
cQuery += "(SELECT Sum(D4_XRES) from "+RetSqlName("SD4")+" SD4 WITH (NOLOCK) "
cQuery += "inner join "+RetSqlName("SC2")+" SC2 (NOLOCK) on SC2.D_E_L_E_T_ = '' and C2_NUM = left(D4_OP,6) and C2_ITEM = Substring(D4_OP,7,2) and C2_SEQUEN = Substring(D4_OP,9,3) and C2_ITEMGRD = SubString(D4_OP,12,3) "
cQuery += "inner join "+RetSqlName("SB1")+" SB1 (NOLOCK) ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "
cQuery += "where SD4.D_E_L_E_T_ = '' and C2_XRES in ('L') and Substring(D4_COD,1,1) <> 'B' and D4_FILIAL = '"+XfILIAL("SD4")+"' "
cQuery += "AND B1_TIPO NOT IN ('MI','PI') "
cQuery += "and D4_COD = B2_COD) "
cQuery += "FROM "+RetSqlName("SB2")+" SB2 WITH (NOLOCK) WHERE SB2.D_E_L_E_T_ = '' AND B2_LOCAL = 'A1' AND B2_COD IN ( "
cQuery += "SELECT D4_COD from "+RetSqlName("SD4")+" SD4 WITH (NOLOCK) "
cQuery += "inner join "+RetSqlName("SC2")+" SC2 (NOLOCK) on SC2.D_E_L_E_T_ = '' and C2_NUM = left(D4_OP,6) and C2_ITEM = Substring(D4_OP,7,2) and C2_SEQUEN = Substring(D4_OP,9,3) and C2_ITEMGRD = SubString(D4_OP,12,3) "
cQuery += "inner join "+RetSqlName("SB1")+" SB1 (NOLOCK) ON SB1.D_E_L_E_T_ = '' AND B1_COD = D4_COD "
cQuery += "where SD4.D_E_L_E_T_ = '' and C2_XRES in ('L') and Substring(D4_COD,1,1) <> 'B' and D4_FILIAL = '"+XFILIAL("SD4")+"' "
cQuery += "AND B1_TIPO NOT IN ('MI','PI') "
cQuery += "group by D4_COD) "

TcSqlExec(cQuery)

cQuery := "SELECT	B2_COD, B2_RESERVA, B2_XRESERV, ISNULL(SUM(ZJ_QTDLIB),0) AS ZJ_QTDLIB, ISNULL(SUM(ZJ_QTDLIB),0)-B2_RESERVA AS QTDECORRETA "+CRLF
//cQuery += "			, ISNULL(SUM(C9_QTDLIB),0) As C9_QTDLIB "+CRLF
cQuery += "FROM "+RetSqlName("SB2")+" SB2 (NOLOCK) "+CRLF
cQuery += "		INNER JOIN "+RetSqlName("SB1")+" SB1 (NOLOCK) "+CRLF
cQuery += "				ON B1_COD = B2_COD AND SB1.D_E_L_E_T_ = '' AND B1_TIPO IN ('PF','KT','ME') "+CRLF
cQuery += "		LEFT JOIN "+RetSqlName("SZJ")+" SZJ (NOLOCK) "+CRLF
cQuery += "				ON ZJ_PRODUTO = B2_COD AND SZJ.D_E_L_E_T_ = '' AND ZJ_DOC = '' AND ZJ_FILIAL = B2_FILIAL "+CRLF
//cQuery += "		LEFT JOIN "+RetSqlName("SC9")+" SC9 "+CRLF
//cQuery += "				ON C9_PRODUTO = B2_COD AND SC9.D_E_L_E_T_ = '' AND C9_NFISCAL = '' AND C9_FILIAL = B2_FILIAL "+CRLF
//cQuery += "				AND C9_PEDIDO = ZJ_PEDIDO AND C9_ITEM = ZJ_ITEM "+CRLF
cQuery += "WHERE SB2.D_E_L_E_T_ = ''"+CRLF
cQuery += "		AND B2_FILIAL = '"+xFilial("SB2")+"' "+CRLF
cQuery += "		AND B2_LOCAL = 'E0' "+CRLF
//If AllTrim(cProduto)<>''
//	cQuery += "		AND B2_COD = '"+AllTrim(cProduto)+"' "+CRLF
//End
cQuery += "GROUP BY B2_COD,B2_RESERVA,B2_XRESERV "+CRLF
cQuery += "ORDER BY B2_COD "+CRLF

MemoWrite("REFAZXRES.txt",cQuery)

cQuery := ChangeQuery(cQuery)

DbUseArea( .T., 'TOPCONN', TCGENQRY(,,cQuery),"TRB", .F., .T.)

DbSelectArea("TRB")

nCont := 0

If TRB->(!EOF())
	While TRB->(!EOF())
		nCont++
		TRB->(DbSkip())
	EndDo
EndIf

ProcRegua(nCont)

TRB->(DbGoTop())
If TRB->(!EOF())
	While TRB->(!EOF())
		IncProc()
		DbSelectArea("SB2")
		DbSetOrder(2)
		If DbSeek(xFilial("SB2")+"E0"+TRB->B2_COD)
			RecLock("SB2",.F.)
				SB2->B2_XRESERV := TRB->QTDECORRETA
			MsUnlock()
		EndIf
		TRB->(DbSkip())
	EndDo
	//MsgInfo("Rec�lculo realizado com sucesso.","Aviso")
Else
	//MsgAlert("N�o h� produtos a serem corrigidos.","Aten��o")	
EndIf

Reset Environment
Return