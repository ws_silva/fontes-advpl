#Include "Protheus.ch"
#Include "TopConn.ch"
#INCLUDE "rwmake.ch"
#include "shell.ch"

#Define cEOL	Chr(13)+Chr(10)
/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  � HFATP020 �Autor  �DANIEL R. MELO      � Data � 25/08/2017  ���
�������������������������������������������������������������������������͹��
���Desc.     � Cadastro de Situacoes                                      ���
�������������������������������������������������������������������������͹��
���Uso       � ACTUAL TREND /                                             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/
User Function HFATP020()

	local cVldAlt := ".T."
	local cVldExc := ".T."
	local cAlias
	
	cAlias := "ZA8"
	chkFile(cAlias)
	dbSelectArea(cAlias)
	dbSetOrder(1)
	private cCadastro := "Cadastro de Situa��es de Produtos"
	aRotina := {;
		{ "Pesquisar" , "AxPesqui", 0, 1},;
		{ "Visualizar", "u_CadSit(4,'')"	, 0, 2},;
		{ "Alterar"   , "u_CadSit(2,'')"	, 0, 4},;
		{ "Excluir"   , "u_CadSit(3,'')"	, 0, 5},;
		{ "Incluir"   , "u_CadSit(1,'')"	, 0, 3};
		}

	dbSelectArea(cAlias)
	mBrowse( 6, 1, 22, 75, cAlias)
	
return

User Function CadSit(_tp)

	Local oGet1
	Local oGet2
	Local oCheckBo01
	Local oCheckBo02
	Local oCheckBo03
	Local oCheckBo04
	Local oCheckBo05
	Local oCheckBo06
	Local oCheckBo07
	Local oCheckBo08
	Local oCheckBo09
	Local oCheckBo10
	Local oCheckBo11
	Local oCheckBo12
	Local oCheckBo13
	Local oCheckBo14
	Local oGroup1
	Local oGroup2
	Local oGroup3
	Local oSay1
	Local oSay2
	Local oButton1
	Local oButton2
	Private cGet1 		:= Space(5)
	Private cGet2			:= Space(40)
	Private lCheckBo01	:= .F.
	Private lCheckBo02	:= .F.
	Private lCheckBo03	:= .F.
	Private lCheckBo04	:= .F.
	Private lCheckBo05	:= .F.
	Private lCheckBo06	:= .F.
	Private lCheckBo07	:= .F.
	Private lCheckBo08	:= .F.
	Private lCheckBo09	:= .F.
	Private lCheckBo10	:= .F.
	Private lCheckBo11	:= .F.
	Private lCheckBo12	:= .F.
	Private lCheckBo13	:= .F.
	Private lCheckBo14	:= .F.
	Static oDlg
	
	If _tp == 1
		
		DbSelectArea("ZA8")
		DBGoBottom()
		cGet1 	:= ZA8->ZA8_CODIGO

		While DbSeek(xfilial("ZA8")+cGet1)
			cGet1 := SOMA1(cGet1)
		End

	ElseIf _tp <> 1
						
		cGet1 		:= ZA8->ZA8_CODIGO
		cGet2		:= ZA8->ZA8_DESCRI
		lCheckBo01	:= ZA8->ZA8_PERMPV
		lCheckBo02	:= ZA8->ZA8_PERMFT
		lCheckBo03	:= ZA8->ZA8_FCHTEC
		lCheckBo04	:= ZA8->ZA8_PREVVD
		lCheckBo05	:= ZA8->ZA8_PREFAS
		lCheckBo06	:= ZA8->ZA8_INFUSU
		lCheckBo07	:= ZA8->ZA8_SITINI
		lCheckBo08	:= ZA8->ZA8_STPRDL
		lCheckBo09	:= ZA8->ZA8_STPRDI
		lCheckBo10	:= ZA8->ZA8_SITINA
		lCheckBo11	:= ZA8->ZA8_VEREST
		lCheckBo12	:= ZA8->ZA8_VERPLA
		lCheckBo13	:= ZA8->ZA8_DWEB
		lCheckBo14	:= ZA8->ZA8_DWEB2B
		
	Endif

	DEFINE MSDIALOG oDlg TITLE "Cadastro de Situa��es de Produtos" FROM 000, 000  TO 400, 900 COLORS 0, 16777215 PIXEL

	@ 015, 015 SAY oSay1 PROMPT "Codigo" SIZE 025, 007 OF oDlg COLORS 0, 16777215 PIXEL
	If _tp <> 1
		@ 013, 046 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	Else
		@ 013, 046 MSGET oGet1 VAR cGet1 SIZE 060, 010 OF oDlg VALID U_VldVxS(1,_tp,Alltrim(cGet1)) COLORS 0, 16777215 PIXEL
	Endif
	@ 015, 138 SAY oSay2 PROMPT "Descri��o" SIZE 038, 007 OF oDlg COLORS 0, 16777215 PIXEL
	If _tp <> 1
		@ 013, 180 MSGET oGet2 VAR cGet2 SIZE 251, 010 OF oDlg COLORS 0, 16777215 READONLY PIXEL
	Else
		@ 013, 180 MSGET oGet2 VAR cGet2 SIZE 251, 010 OF oDlg VALID U_VldVxS(2,_tp,Alltrim(cGet2)) COLORS 0, 16777215 PIXEL
	Endif
	@ 034, 013 GROUP oGroup1 TO 088, 434 PROMPT " Permiss�es do Status  " OF oDlg COLOR 0, 16777215 PIXEL
	@ 045, 023 CHECKBOX oCheckBo01 VAR lCheckBo01 PROMPT "Permite inclus�o/altera��o do Pedido de Venda/Consigna��o" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 045, 231 CHECKBOX oCheckBo02 VAR lCheckBo02 PROMPT "Permite inclus�o/altera��o do Faturamento" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 059, 023 CHECKBOX oCheckBo03 VAR lCheckBo03 PROMPT "Permite inclus�o/altera��o da Ficha T�cnica" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 059, 231 CHECKBOX oCheckBo04 VAR lCheckBo04 PROMPT "(N�o Utilizado) Permite Previs�o de Vendas" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 073, 023 CHECKBOX oCheckBo05 VAR lCheckBo05 PROMPT "Permite inclus�o/altera��o da Pr�-Fase" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 073, 231 CHECKBOX oCheckBo06 VAR lCheckBo06 PROMPT "(N�o Utilizado) Informar ao usu�rio que o produto e descontinuado" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 096, 013 GROUP oGroup2 TO 136, 434 PROMPT "Tipos de Situa��o  " OF oDlg COLOR 0, 16777215 PIXEL
	@ 108, 023 CHECKBOX oCheckBo07 VAR lCheckBo07 PROMPT "(N�o Utilizado) Situa��o Inicial" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 108, 231 CHECKBOX oCheckBo08 VAR lCheckBo08 PROMPT "Situa��o para Produtos (Data de Libera��o)" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 122, 023 CHECKBOX oCheckBo09 VAR lCheckBo09 PROMPT "Situa��o para Produtos (Data de Inativa��o)" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 122, 231 CHECKBOX oCheckBo10 VAR lCheckBo10 PROMPT "(N�o Utilizado) Situa��o do Produto Inativo" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 143, 013 GROUP oGroup3 TO 170, 434 PROMPT "Verifica��es" OF oDlg COLOR 0, 16777215 PIXEL
	@ 155, 023 CHECKBOX oCheckBo11 VAR lCheckBo11 PROMPT "Verificar Estoque Dispon�vel" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 155, 231 CHECKBOX oCheckBo12 VAR lCheckBo12 PROMPT "(N�o Utilizado) Verificar Planejamento" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL

	@ 178, 023 CHECKBOX oCheckBo13 VAR lCheckBo13 PROMPT "Disponivel para WEB" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL
	@ 178, 100 CHECKBOX oCheckBo14 VAR lCheckBo14 PROMPT "Disponivel para WEB - B2B" SIZE 182, 008 OF oDlg COLORS 0, 16777215 PIXEL

	If _tp == 4
		@ 178, 382 BUTTON oButton2 PROMPT "Cancela"		SIZE 053, 012 ACTION oDlg:End() OF oDlg PIXEL
	Else
		@ 178, 318 BUTTON oButton1 PROMPT "Salvar"		SIZE 053, 012 ACTION Save(_tp) OF oDlg PIXEL
		@ 178, 382 BUTTON oButton2 PROMPT "Cancela"		SIZE 053, 012 ACTION oDlg:End() OF oDlg PIXEL
	Endif

	ACTIVATE MSDIALOG oDlg CENTERED

Return

Static Function Save(_tp)

	DbSelectArea("ZA8")
	DbSetOrder(1)
	DbSeek(xfilial("ZA8")+cGet1)

	If _tp == 1
		
		If !Found()
		
			If Alltrim(cGet2)==""
				
				Alert("O campo DESCRI��O n�o foi prenchido!")

				Return(.F.)

			Else
				
				RecLock("ZA8",.T.)
				Replace ZA8->ZA8_FILIAL			with xfilial("ZA8")
				Replace ZA8->ZA8_CODIGO			with cGet1
				Replace ZA8->ZA8_DESCRI			with Alltrim(cGet2)
				Replace ZA8->ZA8_PERMPV			with lCheckBo01
				Replace ZA8->ZA8_PERMFT			with lCheckBo02
				Replace ZA8->ZA8_FCHTEC			with lCheckBo03
				Replace ZA8->ZA8_PREVVD			with lCheckBo04
				Replace ZA8->ZA8_PREFAS			with lCheckBo05
				Replace ZA8->ZA8_INFUSU			with lCheckBo06
				Replace ZA8->ZA8_SITINI			with lCheckBo07
				Replace ZA8->ZA8_STPRDL			with lCheckBo08
				Replace ZA8->ZA8_STPRDI			with lCheckBo09
				Replace ZA8->ZA8_SITINA			with lCheckBo10
				Replace ZA8->ZA8_VEREST			with lCheckBo11
				Replace ZA8->ZA8_VERPLA			with lCheckBo12
				Replace ZA8->ZA8_DWEB			with lCheckBo13
				Replace ZA8->ZA8_DWEB2B			with lCheckBo14
				MsUnLock()
				
				oDlg:End()
			
			End
			
		End
	
	ElseIf _tp == 2

		If Found()
			RecLock("ZA8",.F.)
			Replace ZA8->ZA8_DESCRI			with Alltrim(cGet2)
			Replace ZA8->ZA8_PERMPV			with lCheckBo01
			Replace ZA8->ZA8_PERMFT			with lCheckBo02
			Replace ZA8->ZA8_FCHTEC			with lCheckBo03
			Replace ZA8->ZA8_PREVVD			with lCheckBo04
			Replace ZA8->ZA8_PREFAS			with lCheckBo05
			Replace ZA8->ZA8_INFUSU			with lCheckBo06
			Replace ZA8->ZA8_SITINI			with lCheckBo07
			Replace ZA8->ZA8_STPRDL			with lCheckBo08
			Replace ZA8->ZA8_STPRDI			with lCheckBo09
			Replace ZA8->ZA8_SITINA			with lCheckBo10
			Replace ZA8->ZA8_VEREST			with lCheckBo11
			Replace ZA8->ZA8_VERPLA			with lCheckBo12
			Replace ZA8->ZA8_DWEB			with lCheckBo13
			Replace ZA8->ZA8_DWEB2B			with lCheckBo14
			MsUnLock()
			
			oDlg:End()
			
		Endif
	
	ElseIf _tp == 3

		If Found()
			RecLock("ZA8",.F.)
			DbDelete()
			MsUnlock()
		Endif
		
		oDlg:End()

	Endif

Return

User Function VldVxS(cNum,_tp,cVerifica)

	Local lRet := .T.

	If _tp == 1
	
		If Alltrim(cVerifica)==""
	
			lRet := .F.
			Return(lRet)
	
		Else
	
			If Select("TMP") > 0
				TMP->(DbCloseArea())
			EndIf
	
			cQuery  := " SELECT COUNT(R_E_C_N_O_) AS COUNT FROM " + RetSQLName("ZA8") + " " +cEOL
			cQuery  += " WHERE D_E_L_E_T_<>'*' " +cEOL
			If cNum == 1
				cQuery  += " 		AND ZA8_CODIGO='" + cVerifica + "' " +cEOL
			ElseIf cNum == 2
				cQuery  += " 		AND ZA8_DESCRI='" + cVerifica + "' " +cEOL
			End
				
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"TMP",.T.,.T.)
				
			If TMP->COUNT > 0
				Alert("A situa��o digitada, j� existe no cadastro.")
				lRet := .F.
				TMP->(DbCloseArea())
				Return(lRet)
			End
			
			TMP->(DbCloseArea())
	
		End
	
	End
		
Return(lRet)

User Function HFATP20A(_prod,_tipo,_alert)

	lRet := .T.
	_areax := GetArea()

	DbSelectArea("SB1")
	DbSetOrder(1)
	DbSeek(xfilial("SB1")+_prod)
	_situa := SB1->B1_YSITUAC
	
	If alltrim(_situa) <> ""
	
		DbSelectArea("ZA8")
		DbSetOrder(1)
		DbSeek(xfilial("ZA8")+_situa)
	
		/*----------------------------------------------------------------------------------------------------//
		// PROTHEUS        MILLENNIUM                 FUN��O                                                  //
		//----------------------------------------------------------------------------------------------------//
		// ZA8_PERMPV      PERMITE_PV                 Permite Inclus�o e Altera��o do Pedido de Venda         //
		// ZA8_PERMFT      PERMITE_FAT                Permite Inclus�o e Altera��o do Faturamento             //
		// ZA8_FCHTEC      PERMITE_FICHATEC           Permite Inclus�o e Altera��o das Estruturas             //
		// -ZA8_PREVVD     -PERMITE_PREVISAO          -N�o Utilizado no Millennium                            //
		// ZA8_PREFAS      PERMITE_PREFASE            Permite Inclus�o e Altera��o da Ordem de Corte          //
		// -ZA8_INFUSU     -INFO_DESCONTINUADO        -N�o Utilizado no Millennium                            //
		// -ZA8_SITINI     -SIT_PROD_INICIAL          -Situa��o de origem do produto                          //
		// ZA8_STPRDL      SIT_PROD_DT_LIB            Situa��o para Produtos com Data Libera��o               //
		// ZA8_STPRDI      SIT_PROD_INATIVO           Situa��o para Produtos com Data Inativa��o              //
		// -ZA8_SITINA     -SIT_PROD_DT_INAT          -Situa��o do Produto Inativo                            //
		// ZA8_VEREST      VERIFICA_ESTQ_DISPONIVEL   Verifica Estoque Dispon�vel (estoque - carteira)        //
		// -ZA8_VERPLA     -VERIFICA_PLANEJAMENTO     -N�o Utilizado no Millennium                            //
		//----------------------------------------------------------------------------------------------------*/
	
		If UPPER(_tipo) == "PEDIDO"
			If ZA8->ZA8_PERMPV == .F.
				lRet := .F.
			Endif
			If ZA8_VEREST == .F.
				lRet := .F.
			EndIf
		ElseIf UPPER(_tipo) == "FATURA"
			If ZA8->ZA8_PERMFT == .F.
				lRet := .F.
			Endif
		ElseIf UPPER(_tipo) == "FICHA"
			If ZA8->ZA8_FCHTEC == .F.
				lRet := .F.
			Endif
		ElseIf UPPER(_tipo) == "OPCORTE"
			If ZA8->ZA8_PREFAS == .F.
				lRet := .F.
			Endif
		ElseIf UPPER(_tipo) == "JOB"
			If ZA8->ZA8_STPRDL == .F.
				lRet := .F.
			Endif
			If ZA8->ZA8_STPRDI == .F.
				lRet := .F.
			Endif
		Endif
	Endif
	
	If _alert .and. !lRet
		Alert("Produto indisponivel, verifique a situa��o do Produto.")
	End

	RestArea(_areax)
	
Return lRet