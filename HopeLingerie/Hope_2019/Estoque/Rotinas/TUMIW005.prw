#INCLUDE "TOPCONN.CH"
#INCLUDE "PROTHEUS.CH"
#INCLUDE "RWMAKE.CH"

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �TUMIW001  �Autor  �CRISTIANO LIMA	   � Data �  28/10/2016   ���
�������������������������������������������������������������������������͹��
���Desc.     �Fun��o de Envio de WorkFlow de aprova��o do solicitacao     ���
���          �Aprovador                                                   ���
�������������������������������������������������������������������������͹��
���Uso       �HOPE LINGERIE                                               ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function TUMIW005(cNumSoli)
                                                                                                             
//Local aOrdBKP	:= SaveOrd({"SC7","SA2","SE4","SC1","SC8","SB1"})
Local aOrdBKP	:= SaveOrd({"SA2","SE4","SC1","SB1"})
//Local cFilSC1	:= xFilial("SC1")
Local cFilSCR	:= xFilial("SCR")
Local cFilSA2	:= xFilial("SA2")
Local cFilSE4	:= xFilial("SE4")
Local cFilSC1	:= xFilial("SC1")
Local cFilSC8	:= xFilial("SC8")
Local cFilSB1	:= xFilial("SB1")
Local cFilSAK	:= xFilial("SAK")
Local lFound	:= .F.
Local cCond		:= ""
Local nTotPed	:= 0           
Local nTotIPI	:= 0
Local nTotFrete	:= 0                                      
Local aCotac	:= {}                  
Local cNivelAt	:= ""
Local nRecSC1	:= 0     
Local _nx 		:= 1
Local cNomeFor	:= ""                                
Local nTotDesc	:= 0                                
Local aAprover	:= {}
Local clFilSC7	:= ""
Local clNumSc7 	:= ""                                  
Local clEmailAp	:= "" 
Local alEnd		:= {}
Local llFornece	:=  __NewMV(	"MV_WFFORNE",;
								.T.,;
								"L",;
								"Indica se envia workflow para fornecedor ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para fornecedor ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para fornecedor ap�s as an�lises de aprova��o",;
								.F. )
Local llRecebi	:=  __NewMV(	"MV_WFRECEB",;
								.T.,;
								"L",;
								"Indica se envia workflow para recebimento ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para recebimento ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para recebimento ap�s as an�lises de aprova��o",;
								.F. )

Local llCompra	:=  __NewMV(	"MV_WFCOMPR",;
								.T.,;
								"L",;
								"Indica se envia workflow para o comprador ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o comprador ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o comprador ap�s as an�lises de aprova��o",;
								.F. )

Local llSolici	:=  __NewMV(	"MV_WFSOLIC",;
								.T.,;
								"L",;
								"Indica se envia workflow para o solicitante ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o solicitante ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o solicitante ap�s as an�lises de aprova��o",;
								.F. )

Local clRecbto	:=  __NewMV(	"MV_EMAILRE",;
								"",;
								"C",;
								"E-mail do recebimento para receber o workflow informativo do pedido de compra",;
								"E-mail do recebimento para receber o workflow informativo do pedido de compra",;
								"E-mail do recebimento para receber o workflow informativo do pedido de compra",;
								.F. )
Local nlBkpSC7	:= 0          

                                    
//Cristiano
//Local cSuperior := PswRet()[1][11]   
LOCAL cAprov //:= STRTRAN(cSuperior,"|","")
Local cMailSup //:= UsrRetMail(cAprov)
Local cEmp := cFilant
Local cUser := __cUserID
Local cEmpant 

Default cNumSoli := "" 


DBSELECTAREA('SAI')
DBSETORDER(2)
DBSEEK(cEmp+cUser)   
cAprov 	 := SAI->AI_XAPROVD
cMailSup := UsrRetMail(cAprov)


//Cristiano

		If Empty(cMailSup)
			Aviso("HOPE","E-mail do Aprovador n�o cadastrado! Workflow n�o ser� enviado. Entre em contato com o Departamento de Compras.",{"Ok"})
			Return .F.
		EndIF                    
		
		                                   
		oProcess := TWFProcess():New( "000000", "Processo de Compras" )
		oProcess :NewTask( "Fluxo de Compras", "\workflow\html\SolApr.html" )
		oHtml    := oProcess:oHTML		    		
		
		oHtml:ValByName( "C1_NUM"     		, SC1->C1_NUM     							)
		oHtml:ValByName( "C1_FILIAL"   		, FWFilialName(cEmpant,SC1->C1_FILIAL)		)
		oHtml:ValByName( "C1_EMISSAO"  		, SC1->C1_EMISSAO  							)
		oHtml:ValByName( "NOMEAPROVADOR"    , UsrFullName(cAprov)  						)

		//�������������������������������Ŀ
		//�Preenche os dados do Fornecedor�
		//���������������������������������                                                             
		oHtml:ValByName( "C1_SOLICIT"    , SC1->C1_SOLICIT     )
				
		nRecSC1 := SC1->(Recno())      
		
		SC1->(DbSetOrder(1))
		SC1->(DbSeek(cFilSC1 + SC1->C1_NUM))                           
		
		//�������������������������������������Ŀ
		//�Monta os itens do Pedido no WorkFlow.�
		//���������������������������������������
		While SC1->(!EOF()) .AND. SC1->C1_FILIAL + SC1->C1_NUM == cFilSC1 + cNumSoli
			SC1->(DbSetOrder(1))
			SC1->(DbSeek(cFilSC1 + SC1->C1_NUM + SC1->C1_ITEM))
			
			SB1->(DbSetOrder(1))
			SB1->(DbSeek(cFilSB1 + SC1->C1_PRODUTO))
                                                     
			
			aAdd( (oHtml:ValByName( "it.item"    )), SC1->C1_ITEM       )
			aAdd( (oHtml:ValByName( "it.produto" )), SC1->C1_PRODUTO    )	
			aAdd( (oHtml:ValByName( "it.descri"  )), SC1->C1_DESCRI     )	
			aAdd( (oHtml:ValByName( "it.quant"   )), TRANSFORM( SC1->C1_QUANT,'@E 999999999.99' ) )
			aAdd( (oHtml:ValByName( "it.um"      )), SC1->C1_UM     )	                      
			aAdd( (oHtml:ValByName( "it.unit")), TRANSFORM( SC1->C1_XPRECO,'@E 999999999.99' ) )
			aAdd( (oHtml:ValByName( "it.total")), TRANSFORM( SC1->C1_XTOTAL,'@E 999999999.99' ) )
			aAdd( (oHtml:ValByName( "it.op")), SC1->C1_OP    )
			aAdd( (oHtml:ValByName( "it.centro"  )), Posicione("CTT",1,xFilial("CTT") + SC1->C1_CC,"CTT_DESC01") ) 
			aAdd( (oHtml:ValByName( "it.observacao")), SC1->C1_OBS    )		                      
			
			If ! Empty(cAprov)
				RecLock("SC1",.F.)
				C1_XCODAPRO := cAprov
				SC1->(MsUnlock())
			EndIf
		
	
			//������������������������������������������������������������������������������
			//�Atualiza cota��o com Valores estipulados pelo usuario e processo de workflow�
			//������������������������������������������������������������������������������ 
			RecLock("SC1",.F.)
			SC1->C1_YWFID  	:= oProcess:fProcessID						
			SC1->(MsUnlock())
		
			SC1->(DbSkip())
		EndDo                                           
	
		oProcess:cSubject := "Solicitacao de Compra - " + cNumSoli + " - Pendente de Aprovacao: " + AllTrim(SM0->M0_NOME)
		oProcess:cTo      := cMailSup
		oProcess:bReturn  := "U_W0001RET(1)"                                                //Define a Fun��o de Retorno    

		_cProcesso := "Aprova��o de Solicitacao de Compras"
		
		cOldTo  := oProcess:cTo
		cOldCC  := oProcess:cCC
		cOldBCC := oProcess:cBCC
		
		//Uso um endereco invalido, apenas para criar o processo de workflow, mas sem envia-lo
		oProcess:cTo  := SC1->C1_USER
		oProcess:cCC  := NIL
		oProcess:cBCC := NIL
		
		cMailId    := oProcess:Start("\workflow\http\messenger\confirmacao\") // Crio o processo e gravo o ID do processo de Workflow
			
		U_EnvLK(cMailID  ,cOldTo,cOldCC,cOldBCC,oProcess:cSubject, _cProcesso, UsrFullName(cAprov), oProcess,SC1->C1_USER )


//������������������������������������������������������������������������������
//�Restaura a Ordem Original dos arquivos, preservando a integridade do sistema�
//������������������������������������������������������������������������������
RestOrd(aOrdBKP)

Return .T.        

                     

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �W0001RET  �Autor  � 				     � Data �  09/27/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Fun��o que processa o retorno do Workflow de aprovacao do   ���
���          �pedido, enviado ao Aprovador                                ���
�������������������������������������������������������������������������͹��
���Uso       � 				                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function W0001RET(AOpcao, oProcRet)
                              
Local cCodProc	:= Alltrim(GetNewPar("CL_0000018","000002")) 								//Codigo do Processo do Workflow para Compras             
Local cStatTim	:= Alltrim(GetNewPar("CL_0000035","100007"))								//Status de Timeout de Pedido para Aprova��o
Local cStatRet	:= Alltrim(GetNewPar("CL_0000036","100006"))								//Status de Retorno de Pedido para Aprova��o 
Local cFilSC1 	:= xFilial("SC1")
Local cFilSAK 	:= xFilial("SAK")
Local lFound  	:= .F.                                                                                        
Local lLiberou	:= .F.
Local cNumSoli 	:= alltrim(oProcRet:oHtml:RetByName("C1_NUM"    	))
Local cAprov	:= alltrim(oProcRet:oHtml:RetByName("C1_XCODAPR"     	))
Local cObs		:= alltrim(oProcRet:oHtml:RetByName("lbmotivo"     	))
Local cGrupo	:= ""
Local nOpcAlc 	:= 0
Local aRetSaldo := {}
Local nSaldo	:= 0
Local nTotal	:= 0
Local nSalDif	:= 0                       
Local llCompra	:=  __NewMV(	"MV_WFCOMPR",;
								.T.,;
								"L",;
								"Indica se envia workflow para o comprador ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o comprador ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o comprador ap�s as an�lises de aprova��o",;
								.F. )

Local llSolici	:=  __NewMV(	"MV_WFSOLIC",;
								.T.,;
								"L",;
								"Indica se envia workflow para o solicitante ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o solicitante ap�s as an�lises de aprova��o",;
								"Indica se envia workflow para o solicitante ap�s as an�lises de aprova��o",;
								.F. )
Local clEmailApro	:= ""
Local clTitulo		:= ""
Local clMensagem	:= ""    
Local llSaldook		:= .T.   
LOCAL clEmailCom	:= 'cristiano.lima@hopelingerie.com.br'
LOCAL clEmailSol
Local cUsrComp     := ""	

                                    
//����������������������������������������������Ŀ
//�Trata a Variavel passada pelo workflow(aOpcao)�
//������������������������������������������������
if ValType(aOpcao) = "A" 
	aOpcao := aOpcao[1]
endif   
        
if aOpcao == NIL
	aOpcao := 0          
endIf

aItens := oProcRet:oHtml:RetByName('it.item')
aProds := oProcRet:oHtml:RetByName('it.produto')
                                          
DbSelectArea("SC1") 
cFilSC1 := xFilial("SC1")
SC1->(DbSetOrder(1))

If !SC1->(DbSeek(cFilSC1 + Padr(cNumSoli,6) ))  
	MSGALERT("NUM 1")
	Return
Endif

DbSelectArea("SY1")
DbSetOrder(1)
If DbSeek(xFilial("SY1")+SC1->C1_CODCOMP)
	cUsrComp := SY1->Y1_USER
EndIf                                                         
               
While SC1->(!Eof()) .AND. SC1->C1_FILIAL == cFilSC1 .AND. Alltrim(SC1->C1_NUM) == Alltrim(cNumSoli)
	If Alltrim(SC1->C1_XCODAPR) == Alltrim(cAprov)  
	MSGALERT("NUM 2")
		lFound := .T.
		Exit
	Endif	
	SC1->(DbSkip())
EndDo     
                       
                                                   
If oProcRet:oHtml:RetByName("Aprovacao") == "S"   
	 
	MSGALERT("NUM 6")
	nOpcAlc := 4
ElseIf oProcRet:oHtml:RetByName("Aprovacao") == "N"  
 
	MSGALERT("NUM 7")
	nOpcAlc := 6
Endif   

If nOpcAlc == 4

	If SC1->(DbSeek(cFilSC1 + cNumSoli))
	    
	    cCodUsr := SC1->C1_USER
		While SC1->(!Eof()) .AND. SC1->C1_FILIAL == cFilSC1 .AND. Alltrim(SC1->C1_NUM) == Alltrim(cNumSoli)
			
			//If 
			RecLock("SC1",.F.)          // GRAVANDO A LIBERACAO DA SOLICITACAO               
			SC1->C1_APROV	:= 'L' 
			SC1->C1_OBS     := cObs
			SC1->(MsUnLock())
		   	
			MSGALERT(SC1->C1_NUM)
		   	MSGALERT(cNumSoli)
		   	MSGALERT('LIBERANDO')
			
			//ENDIF    
			
			SC1->(DbSkip())   
			
		EndDo   
		
		ConOut("SOLICITACAO " + cNumSoli+ " APROVADA COM SUCESSO PELO WORKFLOW! ")
		SSCAprov(cNumSoli,.T.,cCodUsr,cObs,cUsrComp)
	endif                      
	
	//clEmailSol := UsrRetMail(SC1->C1_USER)		      
		//
	/*							            
	If !Empty(clEmailCom)

		clTitulo 		:= "Solicita��o de compra: " + Padr(cNumSoli,TamSx3("C1_NUM")[1]) + " APROVADA com sucesso !"

		clMensagem		:= "O processo de aprova��o da sua solicita��o " + Padr(cNumSoli,TamSx3("C1_NUM")[1]) + " foi APROVADA com "
		clMensagem		+= "sucesso! " + " <Br> </Br> " + " <Br> </Br> "

		If !Empty(clEmailSol)
			clCopia := clEmailSol
		EndIf

		U_SNDEMAIL(clTitulo,clMensagem,clEmailCom,'',{},' ') 
	
	ENDIF
	*/
	

	
	//AQUI TALVEZ SEJA O MOMENTO DE ENVIAR O E-MAIL PARA COMPRAS, INFORMANDO QUE EXISTE SOLICITACAO ABERTA
	//E TAMBEM PARA O SOLICITANTE, INFORMANDO QUE LIBEROU A SUA SOLICITACAO

ElseIf nOpcAlc == 6             

	If SC1->(DbSeek(cFilSC1 + cNumSoli))
	
		cCodUsr := SC1->C1_USER	               
		While SC1->(!Eof()) .AND. SC1->C1_FILIAL == cFilSC1 .AND. Alltrim(SC1->C1_NUM) == Alltrim(cNumSoli)
			
			//If 
			RecLock("SC1",.F.)          // GRAVANDO A LIBERACAO DA SOLICITACAO               
			SC1->C1_APROV	:= 'R'
			SC1->C1_OBS     := cObs
			SC1->(MsUnLock())
			//ENDIF    

			SC1->(DbSkip())   

		EndDo   

		ConOut("SOLICITACAO " + cNumSoli+ " REJEITADA PELO WORKFLOW! ")
		SSCAprov(cNumSoli,.F.,cCodUsr,cObs,cUsrComp)

	endif
	
	//clEmailSol := UsrRetMail(SC1->C1_USER)		      
	
	/*							            
	If !Empty(clEmailCom)

		clTitulo 		:= "Solicita��o de compra: " + Padr(cNumSoli,TamSx3("C1_NUM")[1]) + " REJEITADA !"

		clMensagem		:= "O processo de aprova��o da sua solicita��o " + Padr(cNumSoli,TamSx3("C1_NUM")[1]) + " foi REJEITADA. "
//		clMensagem		+= "sucesso! " + " <Br> </Br> " + " <Br> </Br> "
//		clmensagem 		+= "Por gentileza, aguarde o processo de an�lise de cota��es."

		If !Empty(clEmailSol)
			clCopia := clEmailSol
		EndIf

	U_SNDEMAIL(clTitulo,clMensagem,clEmailCom,'',{},' ') 
	
	ENDIF*/
	
Endif
                              

Return .T.

Static Function SSCAprov(cNum,lAprovado,cSC1User,cObserv,cUserComp)
Local nx       := 0
Local cEmailCC := ""

If !Empty(cUserComp)
	cEmailCC := UsrRetMail(cUserComp) //SuperGetMv("MV_XMAILWF",.F.,"")
EndIf	

CONOUT("LOGWF: Envia email de aprovacao: "+cNum)

oProcAprov := TWFProcess():New("000001","Pedido de Compras - Aprovacao")
If lAprovado 
	oProcAprov:NewTask("Solicitacao","\workflow\html\tumiw005p2.html")
	oProcAprov:cSubject := "Solicita��o de Compra N� "+cNum+" aprovada."
	CONOUT("LOGWF: Aprovada. Solicita��o: "+cNum)
Else
	oProcAprov:NewTask("Solicitacao","\workflow\html\tumiw005p3.html")
	oProcAprov:cSubject := "Solicita��o de Compra N� "+cNum+" reprovada."
	CONOUT("LOGWF: Reprovada. Solicita��o: "+cNum)
EndIf	
oProcAprov:cTo		:= UsrRetMail(cSC1User)
oProcAprov:cCC		:= cEmailCC 
oProcAprov:ohtml:valbyname("Num",cNum)
oProcAprov:ohtml:valbyname("Emissao",DtoC(Posicione("SC1",1,xFilial("SC1")+cNum,"C1_EMISSAO")))
oProcAprov:ohtml:valbyname("Req",UsrRetName(cSC1User))

For nx := 1 to Len(aItens)
	AAdd((oProcAprov:oHTML:ValByName("it.item")),aItens[nx])
	AAdd((oProcAprov:oHTML:ValByName("it.produto")),aProds[nx])
Next

oProcAprov:ohtml:valbyname("Motivo",cObserv)

oProcAprov:start()	
oProcAprov:Free()

Return                                                 

/*
���������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �SavNewMv  �Autor  � 				     � Data �  10/02/09   ���
�������������������������������������������������������������������������͹��
���Descri��o �Atualiza o arquivo de par�metros.                           ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � 			   	                                              ���
�������������������������������������������������������������������������͹��
���Parametro �ExpC1 = Nome do Par�metro                                   ���
���          �ExpX1 = Conte�do do Par�metro           	                  ���
���          �ExpC2 = Tipo do Par�metro                                   ���
���          �ExpC3 = Descri��o em portugues                              ���
���          �ExpC4 = Descri��o em espanhol                               ���
���          �ExpC5 = Descri��o em ingles                                 ���
���          �ExpL1 = Grava o conte�do se existir o par�metro             ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
���������������������������������������������������������������������������
*/
Static Function __NewMV( cMvPar, xValor, cTipo, cDescP, cDescS, cDescE, lAlter )

Local aAreaAtu	:= GetArea()
Local lRecLock	:= .F. 
Local xlReturn	

Default lAlter := .F.

If ( ValType( xValor ) == "D" )
	If "" $ xValor
		xValor := Dtoc( xValor, "ddmmyy" )
	Else
		xValor	:= Dtos( xValor )
	Endif
ElseIf ( ValType( xValor ) == "N" )
	xValor	:= AllTrim( Str( xValor ) )
ElseIf ( ValType( xValor ) == "L" )
	xValor	:= If ( xValor , ".T.", ".F." )
EndIf

DbSelectArea('SX6')
DbSetOrder(1)
lRecLock := !MsSeek( Space( Len( X6_FIL ) ) + Padr( cMvPar, Len( X6_VAR ) ) )
RecLock( "SX6", lRecLock )
FieldPut( FieldPos( "X6_VAR" ), cMvPar )
FieldPut( FieldPos( "X6_TIPO" ), cTipo )
FieldPut( FieldPos( "X6_PROPRI" ), "U" )
If !Empty( cDescP )
	FieldPut( FieldPos( "X6_DESCRIC" ), SubStr( cDescP, 1, Len( X6_DESCRIC ) ) )
	FieldPut( FieldPos( "X6_DESC1" ), SubStr( cDescP, Len( X6_DESC1 ) + 1, Len( X6_DESC1 ) ) )
	FieldPut( FieldPos( "X6_DESC2" ), SubStr( cDescP, ( Len( X6_DESC2 ) * 2 ) + 1, Len( X6_DESC2 ) ) )
EndIf
If !Empty( cDescS )
	FieldPut( FieldPos( "X6_DSCSPA" ), cDescS )
	FieldPut( FieldPos( "X6_DSCSPA1" ), SubStr( cDescS, Len( X6_DSCSPA1 ) + 1, Len( X6_DSCSPA1 ) ) )
	FieldPut( FieldPos( "X6_DSCSPA2" ), SubStr( cDescS, ( Len( X6_DSCSPA2 ) * 2 ) + 1, Len( X6_DSCSPA2 ) ) )
EndIf
If !Empty( cDescE )
	FieldPut( FieldPos( "X6_DSCENG" ), cDescE )
	FieldPut( FieldPos( "X6_DSCENG1" ), SubStr( cDescE, Len( X6_DSCENG1 ) + 1, Len( X6_DSCENG1 ) ) )
	FieldPut( FieldPos( "X6_DSCENG2" ), SubStr( cDescE, ( Len( X6_DSCENG2 ) * 2 ) + 1, Len( X6_DSCENG2 ) ) )
EndIf
If lRecLock .Or. lAlter
	FieldPut( FieldPos( "X6_CONTEUD" ), xValor )
	FieldPut( FieldPos( "X6_CONTSPA" ), xValor )
	FieldPut( FieldPos( "X6_CONTENG" ), xValor )
EndIf   

MsUnlock()

xlReturn := GetNewPar(cMvPar)

RestArea( aAreaAtu )

Return(xlReturn)

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �CTPW0007  �Autor  � 				     � Data �  09/21/10   ���
�������������������������������������������������������������������������͹��
���Desc.     �Funcao para envio de Workflow Via link				      ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � 				                                              ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

User Function EnvLK(cHtmlFile,cOldTo,cOldCC,cOldBCC,cSubject, pcDescProc, pcNomAprov, oP, cUserWF )
Local clEndHtml 		:= "\workflow\html\wflink.html"
Local clSrvWfInt		:=  __NewMV(	"MV_LINKINT",;
								"sistemas.hopelingerie.com.br:8088/confirmacao ",;
								"C",;
								"Server HTTP Interno: utilizado para WORKFLOW  ",;
								"Server HTTP Interno: utilizado para WORKFLOW  ",;
								"Server HTTP Interno: utilizado para WORKFLOW  ",;
								.F. )                                          
Local clSrvWFExt 		:=  __NewMV(	"MV_LINKEXT",;
								"sistemas.hopelingerie.com.br:8088",;
								"C",;
								"Server HTTP Externo: utilizado para WORKFLOW  ",;
								"Server HTTP Externo: utilizado para WORKFLOW  ",;
								"Server HTTP Externo: utilizado para WORKFLOW  ",;
								.F. )

Local clLinkInt 	:= "http://" 	+ 	clSrvWfInt + "/" + cHtmlFile + ".htm"                          
Local clLinkExt		:= "http://"	+	clSrvWFExt + "/wf/messenger/emp" + cEmpAnt + "/" + cUserWF + "/" + cHtmlFile + ".htm"
								                                          


oP:NewTask("Link de Processos Workflow", clEndHtml)  // Html com link para envio

oP:ohtml:ValByName("usuario",pcNomaprov)
oP:ohtml:ValByName("proc_link",clLinkInt)
oP:ohtml:ValByName("referente",pcDescProc)
oP:ohtml:ValByName("proc_link2",clLinkExt)						

oP:ohtml:ValByName("EMPRESA",SM0->(AllTrim(M0_NOME) ) )
			
oP:cTo  := cOldTo
oP:cCC  := cOldCC
oP:cBCC := cOldBCC  
               
cSubject	:= StrTran(cSubject,"�","a")
cSubject	:= StrTran(cSubject,"�","c")
cSubject	:= StrTran(cSubject,"�","e")

oP:csubject := cSubject

_cId := oP:start() 


Return()