#INCLUDE "protheus.ch"
#INCLUDE "rwmake.ch"
#INCLUDE "topconn.ch"

/*/
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �HESTA001  � Autor � Bruno Parreira     � Data �  19/01/17   ���
�������������������������������������������������������������������������͹��
���Descricao �Rotina de Inventario por movimentacao interna.              ���
���          �                                                            ���
�������������������������������������������������������������������������͹��
���Uso       � HOPE                                                       ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
/*/

User Function HESTA001()

//���������������������������������������������������������������������Ŀ
//� Declaracao de Variaveis                                             �
//�����������������������������������������������������������������������

Private cPerg       := "HESTA001"
Private oLeTxt

AjustaSX1(cPerg)

If !Pergunte(cPerg,.T.)
	Return
EndIf

//���������������������������������������������������������������������Ŀ
//� Montagem da tela de processamento.                                  �
//�����������������������������������������������������������������������

@ 200,1 TO 380,380 DIALOG oLeTxt TITLE OemToAnsi("Processamento de Invent�rio")
@ 02,10 TO 080,190
@ 10,018 Say " Este programa realiza as movimenta��es de invent�rio"
@ 18,018 Say " com base nos campos preenchidos na tabela SB7."
@ 26,018 Say " Selecionar n�mero de documento do SB7 nos par�metros."

@ 70,128 BMPBUTTON TYPE 01 ACTION BtnOk()
@ 70,158 BMPBUTTON TYPE 02 ACTION Close(oLeTxt)
@ 70,188 BMPBUTTON TYPE 05 ACTION Pergunte(cPerg,.T.)

Activate Dialog oLeTxt Centered

Return

Static Function BtnOk()
//���������������������������������������������������������������������Ŀ
//� Inicializa a regua de processamento                                 �
//�����������������������������������������������������������������������

Processa({|| GeraMov() },"Processando...","Realizando movimentos internos...",.T.)

Return

Static Function GeraMov()
Local cProd := ""
Local cLote := ""
Local cSubL := ""
Local cEnd  := ""
Local nQtde := ""
Local nCust := ""
Local aCabec    := {}
Local aItensSD3 := {}
Local cDtTime   := ""
Local cTime     := ""
Local nCont     := 0
local nErro     := 0
Local nReg      := 0

Private lMsErroAuto := .F.

cTm   	:= "040"
dData 	:= DDATABASE
cData   := DTOS(DDATABASE)
cTime   := Time()
cDtTime := cData + SubStr(cTime,1,2) + SubStr(cTime,4,2) + SubStr(cTime,7,2)
cDoc  	:= "INV"+SubStr(cData,3,2)+SubStr(cData,5,4)

//DbSelectArea("SBF")
//DbSetOrder(1)
//DbGoTop()
/*
_qry := "Select * from SBF010 where D_E_L_E_T_ = '' and BF_QUANT > 0 "
_qry += "and (Select B2_QATU from SB2010 SB2 where SB2.D_E_L_E_T_ = '' and B2_COD = BF_PRODUTO and B2_LOCAL = BF_LOCAL and B2_FILIAL = BF_FILIAL) >= BF_QUANT "
_qry += "and (Select B8_SALDO from SB8010 SB8 where SB8.D_E_L_E_T_ = '' and B8_PRODUTO = BF_PRODUTO and B8_LOCAL = BF_LOCAL and B8_FILIAL = BF_FILIAL and B8_LOTECTL = BF_LOTECTL and B8_NUMLOTE = BF_NUMLOTE) >= BF_QUANT "

cQuery := ChangeQuery(_qry)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSBF",.T.,.T.)

DbSelectArea("TMPSBF")
DbGoTop()
While !EOF()


	If TMPSBF->BF_QUANT > 0
	
//		BEGIN TRANSACTION
			
		ExpA1 := {}

		aadd(ExpA1,{"D3_TM","510",Nil})	
		aadd(ExpA1,{"D3_COD",TMPSBF->BF_PRODUTO,Nil})
		aadd(ExpA1,{"D3_LOCAL",TMPSBF->BF_LOCAL,Nil})	
		aadd(ExpA1,{"D3_LOTECTL",TMPSBF->BF_LOTECTL,Nil})
		aadd(ExpA1,{"D3_NUMLOTE",TMPSBF->BF_NUMLOTE,Nil}) 
		aadd(ExpA1,{"D3_LOCALIZ",TMPSBF->BF_LOCALIZ,Nil})
		aadd(ExpA1,{"D3_QUANT",TMPSBF->BF_QUANT,Nil}) 
//		aadd(ExpA1,{"D3_CUSTO1",nCust,Nil})
		aadd(ExpA1,{"D3_EMISSAO",ddatabase,Nil})		        
			                   
		MSExecAuto({|x,y| mata240(x,y)},ExpA1,3) // 3 - Inclusao
				
		If lMsErroAuto
			MostraErro("\log_inv\",cDtTime+"_"+AllTrim(Str(nErro))+"_"+AllTrim(Str(SB7->(RecNo())))+"_INV.log")
			lMsErroAuto := .F.
			nErro++
		Endif	
	Endif
	
	DbSelectArea("TMPSBF")
	DbSkip()
End

DbCloseArea()


_qry := "Select * from SB8010 SB8 where SB8.D_E_L_E_T_ = '' and B8_SALDO > 0 "
_qry += "and (Select B2_QATU from SB2010 SB2 where SB2.D_E_L_E_T_ = '' and B2_COD = B8_PRODUTO and B2_LOCAL = B8_LOCAL and B2_FILIAL = B8_FILIAL) >= B8_SALDO "

dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSB8",.T.,.T.)

DbSelectArea("TMPSB8")
DbGoTop()

While !EOF()
	
	If TMPSB8->B8_SALDO > 0
	
//		BEGIN TRANSACTION
			
		ExpA1 := {}

		aadd(ExpA1,{"D3_TM","510",Nil})	
		aadd(ExpA1,{"D3_COD",TMPSB8->B8_PRODUTO,Nil})
		aadd(ExpA1,{"D3_LOCAL",TMPSB8->B8_LOCAL,Nil})	
		aadd(ExpA1,{"D3_LOTECTL",TMPSB8->B8_LOTECTL,Nil})
		aadd(ExpA1,{"D3_NUMLOTE",TMPSB8->B8_NUMLOTE,Nil}) 
//		aadd(ExpA1,{"D3_LOCALIZ",SBF->BF_LOCALIZ,Nil})
		aadd(ExpA1,{"D3_QUANT",TMPSB8->B8_SALDO,Nil}) 
//		aadd(ExpA1,{"D3_CUSTO1",nCust,Nil})
		aadd(ExpA1,{"D3_EMISSAO",ddatabase,Nil})		        
			                   
		MSExecAuto({|x,y| mata240(x,y)},ExpA1,3) // 3 - Inclusao
				
		If lMsErroAuto
			MostraErro("\log_inv\",cDtTime+"_"+AllTrim(Str(nErro))+"_"+AllTrim(Str(SB7->(RecNo())))+"_INV.log")
			lMsErroAuto := .F.
			nErro++
		Endif	
	Endif
	
	DbSelectArea("TMPSB8")
	DbSkip()
End

DbCloseArea()

_qry := "Select * from SB2010 where D_E_L_E_T_ = '' and B2_QATU > 0 "
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_qry),"TMPSB2",.T.,.T.)

DbSelectArea("TMPSB2")
DbGoTop()

While !EOF()
	
	If TMPSB2->B2_QATU > 0
	
//		BEGIN TRANSACTION
			
		ExpA1 := {}

		aadd(ExpA1,{"D3_TM","510",Nil})	
		aadd(ExpA1,{"D3_COD",TMPSB2->B2_COD,Nil})
		aadd(ExpA1,{"D3_LOCAL",TMPSB2->B2_LOCAL,Nil})	
//		aadd(ExpA1,{"D3_LOTECTL",SB8->B8_LOTECTL,Nil})
//		aadd(ExpA1,{"D3_NUMLOTE",SB8->B8_NUMLOTE,Nil}) 
//		aadd(ExpA1,{"D3_LOCALIZ",SBF->BF_LOCALIZ,Nil})
		aadd(ExpA1,{"D3_QUANT",TMPSB2->B2_QATU,Nil}) 
//		aadd(ExpA1,{"D3_CUSTO1",nCust,Nil})
		aadd(ExpA1,{"D3_EMISSAO",ddatabase,Nil})		        
			                   
		MSExecAuto({|x,y| mata240(x,y)},ExpA1,3) // 3 - Inclusao
				
		If lMsErroAuto
			MostraErro("\log_inv\",cDtTime+"_"+AllTrim(Str(nErro))+"_"+AllTrim(Str(SB7->(RecNo())))+"_INV.log")
			lMsErroAuto := .F.
			nErro++
		Endif	
	Endif
	
	DbSelectArea("TMPSB2")
	DbSkip()
End

DbCloseArea()
*/

//DbSelectArea("SB7")
//DbsetOrder(3) //B7_FILIAL+B7_DOC+B7_DOC+B7_LOCAL
//DbGoTop()
//While SB7->(!EOF())
//	nReg++
//	SB7->(DbSkip())
//EndDo
ProcRegua(nReg)
_doc := MV_PAR01

DbSelectArea("SB7")
DbsetOrder(3) //B7_FILIAL+B7_DOC+B7_DOC+B7_LOCAL
If DbSeek(xFilial("SB7")+_doc)
//If DbSeek(xFilial("SB7")+"09.06.PRO00023840AVL0044")
	While SB7->(!EOF()) .And. SB7->B7_DOC = _doc //.and. SB7->B7_COD = '00023840AVL0044'
	
		cProd := SB7->B7_COD
		cLote := SB7->B7_LOTECTL
		cSubL := SB7->B7_NUMLOTE
		cEnd  := SB7->B7_LOCALIZ
		nQtde := SB7->B7_QUANT
		nCust := 10 //SB7->B7_XVALOR
		cArmz := SB7->B7_LOCAL
		cDtVl := "20491231"
		
		DbSelectArea("SB8")
		DbSetOrder(3)
		DbSeek(xfilial("SB8")+cProd+cArmz+cLote+cSubL)
		
		If !Found() .and. nQtde > 0
			If Empty(SB7->B7_STATUS)
				BEGIN TRANSACTION
				
				nCont++
			
				ExpA1 := {}
	
				aadd(ExpA1,{"D3_TM",cTm,Nil})	
				aadd(ExpA1,{"D3_COD",cProd,Nil})
				aadd(ExpA1,{"D3_LOCAL",cArmz,Nil})	
				aadd(ExpA1,{"D3_LOTECTL",cLote,Nil})
				aadd(ExpA1,{"D3_NUMLOTE",cSubL,Nil}) 
				aadd(ExpA1,{"D3_DTVALID",StoD(cDtVl),Nil})
				aadd(ExpA1,{"D3_LOCALIZ",cEnd,Nil})
				aadd(ExpA1,{"D3_QUANT",nQtde,Nil}) 
				aadd(ExpA1,{"D3_CUSTO1",nCust,Nil})
				aadd(ExpA1,{"D3_EMISSAO",ddata,Nil})		        
				                   
				MSExecAuto({|x,y| mata240(x,y)},ExpA1,3) // 3 - Inclusao
					
				If lMsErroAuto
					nErro++
					mostraerro()
					MostraErro("\log_inv\",cDtTime+"_"+AllTrim(Str(nErro))+"_"+AllTrim(Str(SB7->(RecNo())))+"_INV.log")
					lMsErroAuto := .F.
				Else
					RecLock("SB7",.F.)
					SB7->B7_STATUS := "X"
					MsUnlock()	
				Endif
				
				END TRANSACTION
		
			EndIf
		Endif
		
		DbSelectArea("SBF")
		DbSetOrder(1)
		DbSeek(xfilial("SBF")+cArmz+cEnd+cProd+Padr(" ",TAMSX3("B7_NUMSERI")[1])+cLote+cSubL)
		
		If !Found() .and. nQtde > 0
			If Empty(SB7->B7_STATUS)
				BEGIN TRANSACTION
				
				nCont++
			
				ExpA1 := {}
	
				aadd(ExpA1,{"D3_TM",cTm,Nil})	
				aadd(ExpA1,{"D3_COD",cProd,Nil})
				aadd(ExpA1,{"D3_LOCAL",cArmz,Nil})	
				aadd(ExpA1,{"D3_LOTECTL",cLote,Nil})
				aadd(ExpA1,{"D3_NUMLOTE",cSubL,Nil}) 
				aadd(ExpA1,{"D3_DTVALID",StoD(cDtVl),Nil})
				aadd(ExpA1,{"D3_LOCALIZ",cEnd,Nil})
				aadd(ExpA1,{"D3_QUANT",nQtde,Nil}) 
				aadd(ExpA1,{"D3_CUSTO1",nCust,Nil})
				aadd(ExpA1,{"D3_EMISSAO",ddata,Nil})		        
				                   
				MSExecAuto({|x,y| mata240(x,y)},ExpA1,3) // 3 - Inclusao
					
				If lMsErroAuto
					nErro++
					mostraerro()
					MostraErro("\log_inv\",cDtTime+"_"+AllTrim(Str(nErro))+"_"+AllTrim(Str(SB7->(RecNo())))+"_INV.log")
					lMsErroAuto := .F.
				Else
					RecLock("SB7",.F.)
					SB7->B7_STATUS := "X"
					MsUnlock()	
				Endif
				
				END TRANSACTION
		
			EndIf
		Endif		
		//If nCont > 1000
		//	Exit
		//EndIf	
		DbSelectArea("SB7")
		SB7->(DbSkip())
		//Incproc()	
	EndDo
	
	MsgInfo("Processados: "+AllTrim(Str(nCont))+" Erro: "+AllTrim(Str(nErro)),"Aviso")
Else
	MsgAlert("Documento n�o encontrado","Aviso")
EndIf

/*
If MV_PAR02 = 1
	DbSelectArea("SBF")
	DbGoTop()
//	DbSetOrder(2)
//	DbSeek(xfilial("SBF")+"00023840AVL0044")
	While !EOF() //.and. SBF->BF_PRODUTO = "00023840AVL0044"
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SBF->BF_PRODUTO)
		If Found() .and. B1_MSBLQL <> "1" .and. B1_TIPO $ ("KT|PF|ME|PI")  .and. (SBF->BF_LOCAL = "E0" .or. SBF->BF_LOCAL = "E1")  
		DbSelectArea("SB7")
		DbSetOrder(1)
		//DbSeek(xfilial("SB7")+dtos(MV_PAR03)+SBF->BF_PRODUTO+SBF->BF_LOCAL+SBF->BF_LOCALIZ+SBF->BF_NUMSERI+SBF->BF_LOTECTL+SBF->BF_NUMLOTE)
		DbSeek(xfilial("SB7")+"20170609"+SBF->BF_PRODUTO+SBF->BF_LOCAL+SBF->BF_LOCALIZ+SBF->BF_NUMSERI+SBF->BF_LOTECTL+SBF->BF_NUMLOTE)
		
		If !Found()
			cProd := SBF->BF_PRODUTO
			cLote := SBF->BF_LOTECTL
			cSubL := SBF->BF_NUMLOTE
			cEnd  := SBF->BF_LOCALIZ
			nQtde := SBF->BF_QUANT
			nCust := 10 //SB7->B7_XVALOR
			cArmz := SBF->BF_LOCAL
			cDtVl := "20491231"
			cTM	  := Iif(nQtde<0,"040","520")
			nCust := Iif(nQtde<0,10,0)
			If nQtde < 0
				nQtde := nQtde * (-1)
			Endif

			If nQtde <> 0
				BEGIN TRANSACTION
				
				nCont++
			
				ExpA1 := {}
	
				aadd(ExpA1,{"D3_TM",cTm,Nil})	
				aadd(ExpA1,{"D3_COD",cProd,Nil})
				aadd(ExpA1,{"D3_LOCAL",cArmz,Nil})	
				aadd(ExpA1,{"D3_LOTECTL",cLote,Nil})
				aadd(ExpA1,{"D3_NUMLOTE",cSubL,Nil}) 
				aadd(ExpA1,{"D3_DTVALID",StoD(cDtVl),Nil})
				aadd(ExpA1,{"D3_LOCALIZ",cEnd,Nil})
				aadd(ExpA1,{"D3_QUANT",nQtde,Nil}) 
				aadd(ExpA1,{"D3_CUSTO1",nCust,Nil})
				aadd(ExpA1,{"D3_EMISSAO",ddata,Nil})		        
				                   
				MSExecAuto({|x,y| mata240(x,y)},ExpA1,3) // 3 - Inclusao
					
				If lMsErroAuto
					nErro++
					//mostraerro()
					MostraErro("\log_inv\",cDtTime+"_"+AllTrim(Str(nErro))+"_"+AllTrim(Str(SB7->(RecNo())))+"_INV.log")
					lMsErroAuto := .F.
				Else
					RecLock("SB7",.F.)
					SB7->B7_STATUS := "X"
					MsUnlock()	
				Endif
				
				END TRANSACTION
			eNDIF
		Endif
		Endif
		DbSelectArea("SBF")
		DbSkip()
	End

	DbSelectArea("SB8")
	DbGoTop()
//	DbSetOrder(1)
//	DbSeek(xfilial("SB8")+"00023840AVL0044")
	While !EOF() //.and. SB8->B8_PRODUTO = "00023840AVL0044"
		DbSelectArea("SB1")
		DbSetOrder(1)
		DbSeek(xfilial("SB1")+SB8->B8_PRODUTO)
		If Found() .and. B1_MSBLQL <> "1"  .and. B1_TIPO $ ("KT|PF|ME|PI") .and. (SB8->B8_LOCAL = "E0" .or. SB8->B8_LOCAL = "E1")

		DbSelectArea("SB7")
		DbSetOrder(1)
//		DbSeek(xfilial("SB7")+dtos(MV_PAR03)+SB8->B8_PRODUTO+SB8->B8_LOCAL+Padr(" ",TAMSX3("B7_LOCALIZ")[1])+Padr(" ",TAMSX3("B7_NUMSERI")[1])+SB8->B8_LOTECTL+SB8->B8_NUMLOTE)
		DbSeek(xfilial("SB7")+"20170609"+SB8->B8_PRODUTO+SB8->B8_LOCAL+Padr(" ",TAMSX3("B7_LOCALIZ")[1])+Padr(" ",TAMSX3("B7_NUMSERI")[1])+SB8->B8_LOTECTL+SB8->B8_NUMLOTE)
		
		If !Found()
			cProd := SB8->B8_PRODUTO
			cLote := SB8->B8_LOTECTL
			cSubL := SB8->B8_NUMLOTE
			cEnd  := ""
			nQtde := SB8->B8_SALDO
			nCust := 10 //SB7->B7_XVALOR
			cArmz := SB8->B8_LOCAL
			cDtVl := "20491231"
			cTM	  := Iif(nQtde<0,"040","520")
			nCust := Iif(nQtde<0,10,0)

			If nQtde < 0
				nQtde := nQtde * (-1)
			Endif

			If nQtde <> 0
				BEGIN TRANSACTION
				
				nCont++
			
				ExpA1 := {}
	
				aadd(ExpA1,{"D3_TM",cTm,Nil})	
				aadd(ExpA1,{"D3_COD",cProd,Nil})
				aadd(ExpA1,{"D3_LOCAL",cArmz,Nil})	
				aadd(ExpA1,{"D3_LOTECTL",cLote,Nil})
				aadd(ExpA1,{"D3_NUMLOTE",cSubL,Nil}) 
				aadd(ExpA1,{"D3_DTVALID",StoD(cDtVl),Nil})
				aadd(ExpA1,{"D3_LOCALIZ",cEnd,Nil})
				aadd(ExpA1,{"D3_QUANT",nQtde,Nil}) 
				aadd(ExpA1,{"D3_CUSTO1",nCust,Nil})
				aadd(ExpA1,{"D3_EMISSAO",ddata,Nil})		        
				                   
				MSExecAuto({|x,y| mata240(x,y)},ExpA1,3) // 3 - Inclusao
					
				If lMsErroAuto
					nErro++
					//mostraerro()
					MostraErro("\log_inv\",cDtTime+"_"+AllTrim(Str(nErro))+"_"+AllTrim(Str(SB7->(RecNo())))+"_INV.log")
					lMsErroAuto := .F.
				Else
					RecLock("SB7",.F.)
					SB7->B7_STATUS := "X"
					MsUnlock()	
				Endif
				
				END TRANSACTION
			eNDIF
		Endif
		Endif
		DbSelectArea("SB8")
		DbSkip()
	End

Endif
*/
Return

/*
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
�������������������������������������������������������������������������ͻ��
���Programa  �AjustaSX1 �Autor  �Bruno Parreira      � Data �  05/01/17   ���
�������������������������������������������������������������������������͹��
���Desc.     �Cria perguntas da rotina.                                   ���
�������������������������������������������������������������������������ͼ��
�����������������������������������������������������������������������������
�����������������������������������������������������������������������������
*/

Static Function AjustaSX1(cPerg)

Local aAreaAtu	:= GetArea()
Local aAreaSX1	:= SX1->( GetArea() )

PutSx1(cPerg,"01","Documento SB7","","","Mv_ch1",TAMSX3("B7_DOC")[3],TAMSX3("B7_DOC")[1],TAMSX3("B7_DOC")[2],0,"G","","","","N","Mv_par01","","","","","","","","","","","","","","","","","","","","","","","",{"Documento SB7.",""},{""},{""},"")

RestArea( aAreaSX1 )
RestArea( aAreaAtu )

Return(cPerg)