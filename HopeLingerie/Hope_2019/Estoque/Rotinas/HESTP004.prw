#INCLUDE "RWMAKE.CH"
#INCLUDE "TBICONN.CH"

User Function HESTP004()
Local dEMISSAO:=DATE()
Local cPRODUTO:= '1'
Local cITEM   := '01'
Local aCabec     := {}
Local aItens     := {}

PRIVATE lMsErroAuto := .F.                        

dEMISSAO:= dDataBase

_qry := "Select * from TABELA_CLEONILSON where REQ_STATUS = ''"
_Qry := ChangeQuery(_qry)
dbUseArea(.T.,"TOPCONN",TcGenQry(,,_Qry),"TMPSCP",.T.,.T.)
	
DbSelectArea("TMPSCP")
DbGoTop()
_num := ""
_ITEM:= 0

While !EOF()
	If _num <> TMPSCP->REQ_CODIGO
		If _NUM <> ""
			lMsErroAuto := .F.
			MSExecAuto({|x,y,z| MATA105(x,y,z)},aCabec,aItens,3)     
	
			If lMsErroAuto     
				MOSTRAERRO()
			EndIf
		Endif
		
		_num := TMPSCP->REQ_CODIGO
		_TEIM:= 0
		
		aCabec := {}
		aadd(aCabec,{"CP_FILIAL" 	,xFilial("SCP") 	,Nil}) // C�d da Filial
		aadd(aCabec,{"CP_NUM"      	,TMPSCP->REQ_CODIGO		,Nil}) // Numero da SA (Calcular se necess�rio)
		aadd(aCabec,{"CP_SOLICIT" 	,TMPSCP->REQ_SOLICIT	,Nil}) // Nome do Solicitante (usu�rio logado)
		aadd(aCabec,{"CP_EMISSAO" 	,TMPSCP->REQ_DATA		,Nil}) // Data de Emiss�o
	
		aItens := {}
	Endif
	
	aAdd(aItens,{})
	aadd(aItens[len(aItens)],{"CP_PRODUTO" ,TMPSCP->REQ_PRODUTO,})
	aadd(aItens[len(aItens)],{"CP_QUANT"   ,TMPSCP->REQ_QUANT,})
	aadd(aItens[len(aItens)],{"CP_ITEM"    ,STRZERO(_ITEM+1,2),})
	aadd(aItens[len(aItens)],{"CP_OBS"     ,TMPSCP->REQ_OBS,})
	aadd(aItens[len(aItens)],{"CP_OP"      ,TMPSCP->REQ_OP,})
	_ITEM++
	
	DbSelectArea("TMPSCP")
	DbSkip()
End

Return Nil 