#include 'protheus.ch'
#include 'parmtype.ch'
#include 'topconn.ch'

/*/{Protheus.doc} HPREL034 
Relatorio Perdas de Qualidade
@author Ronaldo Pereira
@since 11/09/2018
@version 1.0
@example
(examples)
@see (links_or_references)
/*/

user function HPREL034()

Private oReport
Private cPergCont	:= 'HPREL034' 

/************************
*Monta pergunte do Log *
************************/

AjustaSX1(cPergCont)
If !Pergunte(cPergCont, .T.)
	Return
Endif

oReport := ReportDef()
If oReport == Nil
	Return( Nil )
EndIf

oReport:PrintDialog()
	
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} ReportDef
Monta impressao via TReport;
@author Ronaldo Pereira
@since 11 de Setembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
	
Static Function ReportDef()

	Local oReport
	Local oSection1

	oReport := TReport():New( 'RPD', 'RELATÓRIO DE PERDAS - QUALIDADE', , {|oReport| ReportPrint( oReport ), 'RELATÓRIO DE PERDAS - QUALIDADE' } ) 
	oReport:nfontbody:=10
	oReport:SetLandScape()
	oReport:SetTotalInLine(.F.)
	oReport:lParamReadOnly := .T.
	oReport:ShowHeader()
	
	oSection1 := TRSection():New( oReport, 'RELATÓRIO DE PERDAS - QUALIDADE', {'RPD','SH6','SC2','SB4','SA2','SB1'})
			
	TRCell():New( oSection1, 'FILIAL'	    	    ,'RPD', 		'FILIAL',							"@!"                        ,04)
	TRCell():New( oSection1, 'OP'					,'RPD', 		'OP',       						"@!"		                ,06)
	TRCell():New( oSection1, 'SKU'					,'RPD', 		'SKU',       						"@!"		                ,15)
	TRCell():New( oSection1, 'DESCRICAO' 		    ,'RPD', 		'DESCRICAO',              			"@!"						,60)
	TRCell():New( oSection1, 'COD_FORNECE' 		    ,'RPD', 		'COD_FORNECE',              		"@!"						,06)
	TRCell():New( oSection1, 'FORNECEDOR'         	,'RPD', 		'FORNECEDOR',			           	"@!"						,60)
	TRCell():New( oSection1, 'TIPO'         		,'RPD', 		'TIPO',			           			"@!"						,02)
	TRCell():New( oSection1, 'CICLO'				,'RPD', 		'CICLO',       						"@!"		                ,25)
	TRCell():New( oSection1, 'COLECAO'	        	,'RPD', 		'COLECAO',	   			        	"@!"						,20)
	TRCell():New( oSection1, 'SUBCOLECAO'      		,'RPD', 		'SUBCOLECAO',      					"@"				    		,25)
	TRCell():New( oSection1, 'GRUPO'      			,'RPD', 		'GRUPO',      						"@"				    		,25)
	TRCell():New( oSection1, 'PERDA' 				,'RPD', 	    'PERDA',     						"@E 999,99"		    	    ,15)
	
Return( oReport )
	
//_____________________________________________________________________________
/*/{Protheus.doc} ReportPrint
Rotina responsavel pela busca e carregamento dos dados a serem impressos;

@author Ronaldo Pereira
@since 11 de Setembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________
Static Function ReportPrint( oReport )

	Local oSection1 := oReport:Section(1)
	Local cQuery := ""

	oSection1:Init()
	oSection1:SetHeaderSection(.T.)	

	IF Select("RPD") > 0
		RPD->(dbCloseArea())
	Endif
       
    cQuery := " SELECT H6_FILIAL AS FILIAL, "
    cQuery += " SC2.C2_NUM AS OP, "
    cQuery += " SC2.C2_PRODUTO AS SKU, "
    cQuery += " SB4.B4_DESC AS DESCRICAO, "
    cQuery += " SC2.C2_YFORNEC AS COD_FORNECE, "
    cQuery += " SA2.A2_NOME AS FORNECEDOR, "
    cQuery += " SB1.B1_TIPO AS TIPO, "
    cQuery += " SB4.B4_YNCICLO AS CICLO, "
    cQuery += " SB4.B4_YNCOLEC AS COLECAO, "
    cQuery += " SB4.B4_YNSUBCO AS SUBCOLECAO, "
    cQuery += " SB4.B4_YNTPCOM AS GRUPO, "
    cQuery += " SUM(H6_QTDPERD) AS PERDA "
    cQuery += " FROM "+RetSqlName("SH6")+" SH6 WITH(NOLOCK) "
    cQuery += " INNER JOIN "+RetSqlName("SC2")+" SC2 WITH(NOLOCK) ON (SC2.C2_NUM+SC2.C2_ITEM+SC2.C2_SEQUEN+SC2.C2_ITEMGRD=SH6.H6_OP AND SC2.C2_PRODUTO=SH6.H6_PRODUTO AND SC2.C2_FILIAL = SH6.H6_FILIAL AND SC2.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SB4")+" SB4 WITH(NOLOCK) ON (SB4.B4_COD=LEFT(SH6.H6_PRODUTO, 8) AND SB4.D_E_L_E_T_='') "
    cQuery += " INNER JOIN "+RetSqlName("SA2")+" SA2 WITH(NOLOCK) ON (SA2.A2_COD=SC2.C2_YFORNEC AND SA2.D_E_L_E_T_ = '') "
    cQuery += " INNER JOIN "+RetSqlName("SB1")+" SB1 WITH(NOLOCK) ON (B1_COD = SH6.H6_PRODUTO AND SB1.D_E_L_E_T_ = '') "
    cQuery += " WHERE SH6.H6_OPERAC IN ('30','40') AND SH6.D_E_L_E_T_ = '' AND SH6.H6_QTDPERD > '0' AND SB1.B1_TIPO IN ('PF','PI','KT') " 
   
    cQuery += " AND SH6.H6_DTAPONT BETWEEN '"+ DTOS(MV_PAR01) +"' AND '"+ DTOS(MV_PAR02) + "' " 
    
    IF !EMPTY(MV_PAR03)
    	cQuery += " AND SC2.C2_NUM = '"+Alltrim(MV_PAR03)+"' "
    ENDIF
    
    cQuery += " GROUP BY H6_FILIAL,SC2.C2_NUM,SC2.C2_PRODUTO,SB4.B4_DESC,SC2.C2_YFORNEC,SA2.A2_NOME,SB1.B1_TIPO,SB4.B4_YNCICLO,SB4.B4_YNCOLEC,SB4.B4_YNSUBCO,SB4.B4_YNTPCOM "
     	
	TCQUERY cQuery NEW ALIAS RPD
	
	While RPD->(!EOF())
	
		IF oReport:Cancel()
			Exit
		EndIf
		oReport:IncMeter()

		oSection1:Cell("FILIAL"):SetValue(RPD->FILIAL)
		oSection1:Cell("FILIAL"):SetAlign("LEFT")
		
		oSection1:Cell("OP"):SetValue(RPD->OP)
		oSection1:Cell("OP"):SetAlign("LEFT")		
		
		oSection1:Cell("SKU"):SetValue(RPD->SKU)
		oSection1:Cell("SKU"):SetAlign("LEFT")		
		
		oSection1:Cell("DESCRICAO"):SetValue(RPD->DESCRICAO)
		oSection1:Cell("DESCRICAO"):SetAlign("LEFT")
		
		oSection1:Cell("COD_FORNECE"):SetValue(RPD->COD_FORNECE)
		oSection1:Cell("COD_FORNECE"):SetAlign("LEFT")
		
		oSection1:Cell("FORNECEDOR"):SetValue(RPD->FORNECEDOR)
		oSection1:Cell("FORNECEDOR"):SetAlign("LEFT")
				
		oSection1:Cell("TIPO"):SetValue(RPD->TIPO)
		oSection1:Cell("TIPO"):SetAlign("LEFT")
		
		oSection1:Cell("CICLO"):SetValue(RPD->CICLO)
		oSection1:Cell("CICLO"):SetAlign("LEFT")
		
		oSection1:Cell("COLECAO"):SetValue(RPD->COLECAO)
		oSection1:Cell("COLECAO"):SetAlign("LEFT")
				
		oSection1:Cell("SUBCOLECAO"):SetValue(RPD->SUBCOLECAO)
		oSection1:Cell("SUBCOLECAO"):SetAlign("LEFT")
		
		oSection1:Cell("GRUPO"):SetValue(RPD->GRUPO)
		oSection1:Cell("GRUPO"):SetAlign("LEFT")
				
		oSection1:Cell("PERDA"):SetValue(RPD->PERDA)
		oSection1:Cell("PERDA"):SetAlign("LEFT")
										
		oSection1:PrintLine()
		
		RPD->(DBSKIP()) 
	enddo
	RPD->(DBCLOSEAREA())
Return( Nil )

//_____________________________________________________________________________
/*/{Protheus.doc} AjustaSX1
Cria as perguntas no SX1;

@author Ronaldo Pereira
@since 11 de Setembro de 2018
@version 1.0
/*/
//_____________________________________________________________________________

Static Function AjustaSX1(cPergCont)
	PutSx1(cPergCont, "01","Data de ?"		        		,""		,""		,"mv_ch1","C",08,0,1,"G",""	,""	,"","","mv_par01"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "02","Data ate ?"                     ,""		,""		,"mv_ch2","C",08,0,1,"G",""	,""	,"","","mv_par02"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "03","Num Prefat. de ?"		        ,""		,""		,"mv_ch3","C",08,0,1,"G",""	,""	,"","","mv_par03"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "04","Num Prefat. ate ?"		        ,""		,""		,"mv_ch4","C",08,0,1,"G",""	,""	,"","","mv_par04"," ","","","","","","","","","","","","","","","")
	PutSx1(cPergCont, "05","Pedido ?"		                ,""		,""		,"mv_ch5","D",06,0,1,"G",""	,""	,"","","mv_par05"," ","","","","","","","","","","","","","","","")	
	
return