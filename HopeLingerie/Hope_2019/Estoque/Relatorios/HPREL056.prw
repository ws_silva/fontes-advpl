//Bibliotecas
#Include "Protheus.ch"
#Include "TopConn.ch"
#include "parmtype.ch"

//_____________________________________________________________________________
/*/{Protheus.doc} Relat�rio Mat�ria Prima Exclusiva.
Rotina para gerar mat�ria prima exclusiva;

@author Geyson Albano
@since 21 de Agosto de 2019
@version V.01
/*/
//_____________________________________________________________________________      

User Function HPREL056()

	Local aRet := {}
	Local aPerg := {}
	Local dDataini := ctod("")
	Local dDatafim := ctod("")
	 
	aAdd(aPerg ,{1,"Data Inicio",ddatabase,"",'.T.',"",'.T.',60,.F.})
	aAdd(aPerg ,{1,"Data Fim ",ddatabase,"",'.T.',"",'.T.',60,.F.})

	If ParamBox(aPerg,"Par�metros...",@aRet) 
	 	Processa({|| pdterc(aRet)}, "Exportando dados")		
	Else
		Alert("Cancelado Pelo Usu�rio!!!")
	Endif
    
Return    
    
Static Function pdterc(aRet)

    Local aArea     	:= GetArea()
    Local cQuery   		:= ""
    Local oFWMsExcel
    Local oExcel
    Local cArquivo  	:= GetTempPath()+'PD TERCEIROS'+DTOS(DDATABASE)+'_'+REPLACE(TIME(),':','')+'.xml''
    Local cPlan1		:= "Base"
    Local cTable1    	:= "PODER DE TERCEIROS" 

        
    //Dados tabela 1          
	cQuery:= "SELECT T.COD,																															"	
	cQuery+= "T.DESCRICAO,																															"
	cQuery+= "T.COD_CLI,																															"
	cQuery+= "T.NOME_CLI,																															"
	cQuery+= "T.DOC_ORIGINAL,																														"
	cQuery+= "T.QTD_ORIGINAL,																														"
	cQuery+= "T.QTD_ENTREGUE,																														"
    cQuery+= "T.SALDO,                                                                                                                              "
    cQuery+= "T.DT_EMISSAO,																															"
	cQuery+= "SUM(T.TOTAL_N_FISCAL) AS TOTAL_N_FISCAL ,																								"
	cQuery+= "SUM(T.TOTAL_DEVOLVIDO) AS TOTAL_DEVOLVIDO,																							"
	cQuery+= "T.OP 																																	"
	cQuery+= " FROM (																																"
	cQuery+= "SELECT B6_PRODUTO AS COD,																												"
	cQuery+= "B1_DESC AS DESCRICAO,																													"
	cQuery+= "B6_CLIFOR AS COD_CLI,																													"
	cQuery+= "A2_NOME AS NOME_CLI,																													"
	cQuery+= "B6_DOC AS DOC_ORIGINAL,																												"
	cQuery+= "B6_EMISSAO AS DT_EMISSAO,																												"
	cQuery+= "B6_QUANT AS QTD_ORIGINAL,																												"
	cQuery+= "B6_QUANT - B6_SALDO AS QTD_ENTREGUE,																									"
	cQuery+= "B6_SALDO AS SALDO,																													"
	cQuery+= "SUM(B6_QUANT * B6_PRUNIT )AS TOTAL_N_FISCAL,																							"
	cQuery+= "SUM((B6_QUANT - B6_SALDO) * B6_PRUNIT )AS TOTAL_DEVOLVIDO,																			"
	cQuery+= "LEFT(C5_XNUMOP,11) AS OP																												"
	cQuery+= "FROM "+ RetSqlName("SB6") +" (NOLOCK)																									"
	cQuery+= "JOIN "+ RetSqlName("SB1") +" (NOLOCK) ON B1_COD = B6_PRODUTO AND SB1010.D_E_L_E_T_ = ''												"
	cQuery+= "JOIN "+ RetSqlName("SA2") +" (NOLOCK) ON A2_COD = B6_CLIFOR AND A2_LOJA = B6_LOJA AND SA2010.D_E_L_E_T_ = ''							"
	cQuery+= "JOIN "+ RetSqlName("SD2") +" (NOLOCK) ON B6_DOC = D2_DOC AND B6_CLIFOR = D2_CLIENTE AND B6_LOJA = D2_LOJA AND B6_PRODUTO = D2_COD AND SD2010.D_E_L_E_T_ = ''	"
	cQuery+= "JOIN "+ RetSqlName("SC6") +" (NOLOCK) ON C6_NOTA = D2_DOC AND C6_NUM = D2_PEDIDO AND C6_CLI = D2_CLIENTE 								"
	cQuery+=  "AND C6_LOJA = D2_LOJA AND C6_PRODUTO = D2_COD AND C6_FILIAL = D2_FILIAL AND SC6010.D_E_L_E_T_ = ''									"
	cQuery+= "JOIN "+ RetSqlName("SC5") +" (NOLOCK) ON C5_NUM = C6_NUM AND C5_CLIENT = C6_CLI AND C5_LOJACLI = C6_LOJA AND C5_FILIAL = C6_FILIAL AND SC5010.D_E_L_E_T_ = ''	"
	cQuery+= "WHERE SB6010.D_E_L_E_T_ = '' AND B6_FILIAL = '0101' 																					"
	cQuery+= "AND C5_TIPO = 'B'																														"
	cQuery+= "AND B6_EMISSAO BETWEEN '"+ DTOS(aRet[1]) +"'  AND '"+ DTOS(aRet[2]) +"' 										"																			"
	cQuery+= "GROUP BY B6_PRODUTO,B1_DESC,B6_CLIFOR,A2_NOME,B6_DOC,B6_EMISSAO,B6_QUANT,B6_SALDO,C5_XNUMOP,B6_PRUNIT  ) T							"
	cQuery+= "GROUP BY T.COD,T.COD_CLI,T.DESCRICAO,T.DOC_ORIGINAL,T.DT_EMISSAO,T.NOME_CLI,T.OP,T.QTD_ENTREGUE,T.QTD_ORIGINAL,T.SALDO				"
                              
	TCQuery cQuery New Alias "QRY"

	ProcRegua(RecCount())
	QRY->(dbGotop())
                         
    //Criando o objeto que ir� gerar o conte�do do Excel
    oFWMsExcel := FWMSExcel():New()
     
    //Alterando atributos
    oFWMsExcel:SetFontSize(10)     //Tamanho Geral da Fonte
    oFWMsExcel:SetFont("Arial")    //Fonte utilizada
    oFWMsExcel:SetTitleBold(.T.)   //T�tulo Negrito

     
    //Aba 01 - Teste
    oFWMsExcel:AddworkSheet(cPlan1) //N�o utilizar n�mero junto com sinal de menos. Ex.: 1-
        //Criando a Tabela
        oFWMsExcel:AddTable(cPlan1,cTable1)
        //Criando Colunas
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DESCRICAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"COD_CLI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"NOME_CLI",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DOC_ORIGINAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTD_ORIGINAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"QTD_ENTREGUE",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"SALDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"DT_EMISSAO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TOTAL_N_FISCAL",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"TOTAL_DEVOLVIDO",1)
        oFWMsExcel:AddColumn(cPlan1,cTable1,"OP",1)
              
                                       
        //Criando as Linhas... Enquanto n�o for fim da query
        While !(QRY->(EoF()))
            oFWMsExcel:AddRow(cPlan1,cTable1,{;                                             
                                               COD,;
                                               DESCRICAO,;
                                               COD_CLI,;
                                               NOME_CLI,;
                                               DOC_ORIGINAL,;
                                               QTD_ORIGINAL,;
                                               QTD_ENTREGUE,;
                                               SALDO,;
                                               STOD(DT_EMISSAO),;
                                               TOTAL_N_FISCAL,;
                                               TOTAL_DEVOLVIDO,;
											   OP;
											   })
                                                 
            //Pulando Registro
            QRY->(DbSkip())
        EndDo
            
     
    //Ativando o arquivo e gerando o xml
    oFWMsExcel:Activate()
    oFWMsExcel:GetXMLFile(cArquivo)
         
    //Abrindo o excel e abrindo o arquivo xml
    oExcel:= MsExcel():New()           //Abre uma nova conex�o com Excel
    oExcel:WorkBooks:Open(cArquivo)     //Abre uma planilha
    oExcel:SetVisible(.T.)              //Visualiza a planilha
    oExcel:Destroy()                    //Encerra o processo do gerenciador de tarefas   
    
    QRY->(DbCloseArea())
    RestArea(aArea)
    
Return