#INCLUDE "PROTHEUS.CH"
/*
-----------------------------------------------------------------------------
| Programa  | MA330MOD   | Autor | TOTALIT SOLUTIONS    | Data | 01/07/2018 |
-----------------------------------------------------------------------------
| Desc.     | Rotina para Filtragem dos Grupos de Custo do CT1 para conside-|
|           | rar MOD                                                       |
-----------------------------------------------------------------------------
*/
USER FUNCTION MA330MOD() 

Local _aArea:=GetArea()
Local cCodPesq:=ParamIXB[1]
Local nx := 0
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Atualiza o log de processamento			    �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
ProcLogAtu("MENSAGEM",OemToAnsi("Iniciando Recalculo do Custo da Mao de Obra"),OemToAnsi("Iniciando Recalculo do Custo da Mao de Obra")) //"Iniciando Recalculo do Custo da Mao de Obra"
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Zera os saldos de MOD para recalcular                        �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
lGrupo := .T.
cGrupo := "CUSTOFAB"
cCodcc :=" "
dTermino := ddatabase
cMoeda330C := SuperGetMv('MV_MOEDACM',.F.,"2345")
nPercInc   := 0
cCodant :=""          
nTamCC  := LEN(CQ3->CQ3_CCUSTO)
dbSelectArea("SB2")
dbSetOrder(1)
dbSeek(xFilial("SB2")+cCodPesq)
While !Eof() .And. B2_FILIAL+Substr(B2_COD,1,3)== xFilial("SB2")+cCodPesq
	If SB2->B2_COD # cCodAnt
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Pega os saldos do centro de custo                            �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		aSaldos := XMA330SalCC(SubStr(SB2->B2_COD,4,nTamCC),dTermino,cGrupo,lGrupo)
		cCodAnt := SB2->B2_COD
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Aplica o % de aumento no custo da MOD                        �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		For nX := 1 to Len(aSaldos)
			// Verifica se moeda devera ser considerada
			If nx # 1 .And. !(Str(nx,1,0) $ cMoeda330C)
				Loop
			EndIf
			aSaldos[nX] += (aSaldos[nX] * (nPercInc/100))
		Next nX
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Quantidade total do produto MOD                              �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		nQuantTot:=0
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Filtra movimentos validos do produto                         �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		dbSelectArea("SD3")
		dbSetOrder(1)
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Movimentacao do Cursor                                       �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		//		If !lBat .And. !IsBlind()
		//			IncProc(OemToAnsi(STR0154)) //"Acertando o Custo da Mao de Obra"
		//		EndIf
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Produtos Mao-de-Obra com a nomenclatura "MOD"                |
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		If Empty(cCodCC)
			//-- Somatoria das Devolucoes do periodo
			cAliasDEV := CriaTrab(NIL,.F.)
			cQuery := "SELECT SUM(D3_QUANT) TOTDEV FROM "+RetSqlName("SD3")+ " SD3 "
			cQuery +=    "WHERE D3_FILIAL = '" + xFilial("SD3") + "' AND "
			cQuery +=          "D3_COD = '" + cCodAnt + "' AND "
			cQuery +=          "D3_EMISSAO >= '" + dtos(dInicio) + "' AND "
			cQuery +=          "D3_EMISSAO <= '" + dtos(dTermino) + "' AND "
			cQuery +=          "D3_ESTORNO = ' ' AND "
			cQuery +=          "D3_TM <= '500' AND "
			cQuery +=          "D3_CF NOT IN('PR0','PR1') AND "
			cQuery +=          "D_E_L_E_T_ = ' ' "
			cQuery := ChangeQuery(cQuery)
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasDEV,.T.,.T.)
			nQuantTot += (cAliasDEV)->TOTDEV
			dbSelectArea(cAliasDEV)
			dbCloseArea()
			//-- Somatoria das Requisoes do periodo
			cAliasREQ := CriaTrab(NIL,.F.)
			cQuery := "SELECT SUM(D3_QUANT) TOTREQ FROM "+RetSqlName("SD3")+ " SD3 "
			cQuery +=    "WHERE D3_FILIAL = '" + xFilial("SD3") + "' AND "
			cQuery +=          "D3_COD = '" + cCodAnt + "' AND "
			cQuery +=          "D3_EMISSAO >= '" + dtos(dInicio) + "' AND "
			cQuery +=          "D3_EMISSAO <= '" + dtos(dTermino) + "' AND "
			cQuery +=          "D3_ESTORNO = ' ' AND "
			cQuery +=          "D3_TM > '500' AND "
			cQuery +=          "D3_CF NOT IN('PR0','PR1') AND "
			cQuery +=          "D_E_L_E_T_ = ' ' "
			cQuery := ChangeQuery(cQuery)
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasREQ,.T.,.T.)
			nQuantTot -= (cAliasREQ)->TOTREQ
			dbSelectArea(cAliasREQ)
			dbCloseArea()
			//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
			//� Produtos Mao-de-Obra sem a nomenclatura "MOD"                |
			//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		Else
			//-- Somatoria das Devolucoes do periodo
			cAliasDEV := CriaTrab(NIL,.F.)
			cQuery := "SELECT SUM(D3_QUANT) TOTDEV FROM "+RetSqlName("SD3")+ " SD3 ,"+RetSqlName("SB1")+ " SB1 "
			cQuery +=    "WHERE D3_FILIAL = '" + xFilial("SD3") + "' AND "
			cQuery +=          "B1_FILIAL = '" + xFilial("SB1") + "' AND "
			cQuery +=          "B1_COD = D3_COD AND "
			cQuery +=          "D3_EMISSAO >= '" + dtos(dInicio) + "' AND "
			cQuery +=          "D3_EMISSAO <= '" + dtos(dTermino) + "' AND "
			cQuery +=          "B1_CCCUSTO = '" + cCodCC + "' AND "
			If lGrupo
				cQuery +=      "B1_GCCUSTO = '" + cGrupo + "' AND "
			EndIf
			cQuery +=          "D3_ESTORNO = ' ' AND "
			cQuery +=          "D3_TM <= '500' AND "
			cQuery +=          "D3_CF NOT IN('PR0','PR1') AND "
			cQuery +=          "SD3.D_E_L_E_T_ = ' ' AND "
			cQuery +=          "SB1.D_E_L_E_T_ = ' ' "
			cQuery := ChangeQuery(cQuery)
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasDEV,.T.,.T.)
			nQuantTot += (cAliasDEV)->TOTDEV
			dbSelectArea(cAliasDEV)
			dbCloseArea()
			//-- Somatoria das Devolucoes do periodo
			cAliasREQ := CriaTrab(NIL,.F.)
			cQuery := "SELECT SUM(D3_QUANT) TOTREQ FROM "+RetSqlName("SD3")+ " SD3 ,"+RetSqlName("SB1")+ " SB1 "
			cQuery +=    "WHERE D3_FILIAL = '" + xFilial("SD3") + "' AND "
			cQuery +=          "B1_FILIAL = '" + xFilial("SB1") + "' AND "
			cQuery +=          "B1_COD = D3_COD AND "
			cQuery +=          "D3_EMISSAO >= '" + dtos(dInicio) + "' AND "
			cQuery +=          "D3_EMISSAO <= '" + dtos(dTermino) + "' AND "
			cQuery +=          "B1_CCCUSTO = '" + cCodCC + "' AND "
			If lGrupo
				cQuery +=      "B1_GCCUSTO = '" + cGrupo + "' AND "
			EndIf
			cQuery +=          "D3_ESTORNO = ' ' AND "
			cQuery +=          "D3_TM > '500' AND "
			cQuery +=          "D3_CF NOT IN('PR0','PR1') AND "
			cQuery +=          "SD3.D_E_L_E_T_ = ' ' AND "
			cQuery +=          "SB1.D_E_L_E_T_ = ' ' "
			cQuery := ChangeQuery(cQuery)
			dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasREQ,.T.,.T.)
			nQuantTot -= (cAliasREQ)->TOTREQ
			dbSelectArea(cAliasREQ)
			dbCloseArea()
		EndIf
	EndIf
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//� Quantidade do produto nesse armazem                          �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	nQuant := 0
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//� Movimentacao do Cursor                                       �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	//	If !lBat .And. !IsBlind()
	IncProc(OemToAnsi("Acertando o Custo da Mao de Obra")) //"Acertando o Custo da Mao de Obra"
	//	EndIf
	//-- Somatoria das Devolucoes do periodo
	cAliasDEV := CriaTrab(NIL,.F.)
	cQuery := "SELECT SUM(D3_QUANT) TOTDEV FROM "+RetSqlName("SD3")+ " SD3 "
	cQuery +=    "WHERE D3_FILIAL = '" + xFilial("SD3") + "' AND "
	cQuery +=          "D3_COD = '" + cCodAnt + "' AND "
	cQuery +=          "D3_LOCAL = '" + SB2->B2_LOCAL + "' AND "
	cQuery +=          "D3_EMISSAO >= '" + dtos(dInicio) + "' AND "
	cQuery +=          "D3_EMISSAO <= '" + dtos(dTermino) + "' AND "
	cQuery +=          "D3_ESTORNO = ' ' AND "
	cQuery +=          "D3_TM <= '500' AND "
	cQuery +=          "D3_CF NOT IN('PR0','PR1') AND "
	cQuery +=          "D_E_L_E_T_ = ' ' "
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasDEV,.T.,.T.)
	nQuant += (cAliasDEV)->TOTDEV
	dbSelectArea(cAliasDEV)
	dbCloseArea()
	//-- Somatoria das Requisoes do periodo
	cAliasREQ := CriaTrab(NIL,.F.)
	cQuery := "SELECT SUM(D3_QUANT) TOTREQ FROM "+RetSqlName("SD3")+ " SD3 "
	cQuery +=    "WHERE D3_FILIAL = '" + xFilial("SD3") + "' AND "
	cQuery +=          "D3_COD = '" + cCodAnt + "' AND "
	cQuery +=          "D3_LOCAL = '" + SB2->B2_LOCAL + "' AND "
	cQuery +=          "D3_EMISSAO >= '" + dtos(dInicio) + "' AND "
	cQuery +=          "D3_EMISSAO <= '" + dtos(dTermino) + "' AND "
	cQuery +=          "D3_ESTORNO = ' ' AND "
	cQuery +=          "D3_TM > '500' AND "
	cQuery +=          "D3_CF NOT IN('PR0','PR1') AND "
	cQuery +=          "D_E_L_E_T_ = ' ' "
	cQuery := ChangeQuery(cQuery)
	dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),cAliasREQ,.T.,.T.)
	nQuant -= (cAliasREQ)->TOTREQ
	dbSelectArea(cAliasREQ)
	dbCloseArea()
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//� Atualizando o Saldo em Estoque                               �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	dbSelectArea("SB2")
	RecLock("SB2",.F.)
	
	// Atualizacao do campo B2_VFIM
	Replace B2_VFIM1 With aSaldos[01]*ABS(nQuant/nQuantTot)
	Replace B2_VFIM2 With If("2" $ cMoeda330C,aSaldos[02]*ABS(nQuant/nQuantTot),0)
	Replace B2_VFIM3 With If("3" $ cMoeda330C,aSaldos[03]*ABS(nQuant/nQuantTot),0)
	Replace B2_VFIM4 With If("4" $ cMoeda330C,aSaldos[04]*ABS(nQuant/nQuantTot),0)
	Replace B2_VFIM5 With If("5" $ cMoeda330C,aSaldos[05]*ABS(nQuant/nQuantTot),0)
	
	// Atualiza o campo B2_CM somente para manter legado
	Replace B2_CM1 With B2_VFIM1/ABS(nQuant)
	Replace B2_CM2 With If("2" $ cMoeda330C,B2_VFIM2/ABS(nQuant),0)
	Replace B2_CM3 With If("3" $ cMoeda330C,B2_VFIM3/ABS(nQuant),0)
	Replace B2_CM4 With If("4" $ cMoeda330C,B2_VFIM4/ABS(nQuant),0)
	Replace B2_CM5 With If("5" $ cMoeda330C,B2_VFIM5/ABS(nQuant),0)
	
	// Atualiza o campo B2_CMFIM
	Replace B2_CMFIM1 With B2_VFIM1/ABS(nQuant)
	Replace B2_CMFIM2 With If("2" $ cMoeda330C,B2_VFIM2/ABS(nQuant),0)
	Replace B2_CMFIM3 With If("3" $ cMoeda330C,B2_VFIM3/ABS(nQuant),0)
	Replace B2_CMFIM4 With If("4" $ cMoeda330C,B2_VFIM4/ABS(nQuant),0)
	Replace B2_CMFIM5 With If("5" $ cMoeda330C,B2_VFIM5/ABS(nQuant),0)
	
	Replace B2_QFIM With nQuant
	
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//� A330QTMO - Ponto de entrada utilizado para manipular a       |
	//|            quantidade da mao de obra apurada.                �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
//	If lA330QTMO
//		ExecBlock("A330QTMO",.F.,.F.)
//	EndIf
	
	MsUnlock()
	
	TTFimComMO({SB2->B2_VFIM1,SB2->B2_VFIM2,SB2->B2_VFIM3,SB2->B2_VFIM4,SB2->B2_VFIM5})
	TTFimQtdMO()
	dbSkip()
EndDo
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Devolve ordem principal dos arquivos                         �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
dbSelectArea("SD3")
dbSetOrder(1)

RestArea(_aArea)

//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
//� Atualiza o log de processamento			    �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
ProcLogAtu("MENSAGEM",OemToAnsi("kkkkkkkkkkkkkkkkkkk"),OemToAnsi("kkkkkkkkkkkkkkkkk")) //"Termino do Recalculo do Custo da Mao de Obra"
RETURN




Static Function XMA330SalCC(cCod,dData,cGrupo,lGrupo)
Local aSaldos[5]
Local nCont
Local lMA330Filtra := ExistBlock("M330FCC")
Local lRetExec :=.T.
Local lContab  := .F. 
Local cCTBIni  := Space(TamSX3("CQ1_CONTA")[1])
Local cCTBFim  := Space(TamSX3("CQ1_CONTA")[1]) 
Local cFuncSubs:= "SUBSTRING"
Local cMoeda330C := SuperGetMv('MV_MOEDACM',.F.,"2345")
 
//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
//� Tratamento para SUBSTRING em diferentes BD's �
//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
If TCGetDB() $ "ORACLE/POSTGRES/DB2"
	cFuncSubs  := "SUBSTR"
EndIf

AFILL(aSaldos,0)

If lContab
	//-- Utiliza modulo SIGACTB
	
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//� Coloco o CT1 em ordem de Centro de Custo                     �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	dbSelectArea("CT1")
	dbSetOrder(7)
	dbSeek(xFilial("CT1")+cCod)
	While !Eof() .And. AllTrim(xFilial("CT1")+cCod) == AllTrim(CT1_FILIAL+CT1_CC)
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Filtro de contas a serem inibidas (contas de transferencia)  �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		If CT1_CONTA >= cCTBIni .And. CT1_CONTA <= cCTBFim
			dbSkip()
			Loop
		EndIf
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Filtro de contas atraves do grupo                            �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		If !Empty(cGrupo) .And. CT1_GRUPO <> cGrupo
			dbSkip()
			Loop
		EndIf
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Executa filtragem por execblock                              �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		If lMA330Filtra
		lRetExec:=ExecBlock("M330FCC",.F.,.F.,"CT1")
			If ValType(lRetExec) == "L" .And. !lRetExec
				dbSkip()
				Loop
			EndIf
		EndIf
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Calculo dos Saldos nas 5 Moedas                              �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		For nCont := 1 To 5
			// Verifica se moeda devera ser considerada 	
			If nCont # 1 .And. !(Str(nCont,1,0) $ cMoeda330C)
                	Loop
			EndIf
			// a tabela CT7 foi descontinuada na vers�o 12, sendo substituida pela tabela CQ1, porem, as fun寤es permanecem com o mesmo nome pelo legado
			aSaldoFim	:= SaldoCT7(CT1_CONTA,dData,StrZero(nCont,2),"1")
			aSaldoIni	:= SaldoCT7(CT1_CONTA,dInicio,StrZero(nCont,2),"1")
			nDebito	:= aSaldoFim[4] - aSaldoIni[7]
			nCredito	:= aSaldoFim[5] - aSaldoIni[8]
			aSaldos[nCont] += nDebito - nCredito
		Next nCont
		dbSelectArea("CT1")
			dbSkip()
		EndDo
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
		//� Devolvo a ordem original do arquivo                          �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
		dbSetOrder(1)
	
Else
	//-- Utiliza modulo SIGACTB
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
	//� Se for Centro de Custo Extra-Contabil -> Saldos 		         �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
	
	cCod := Left(cCod + Space(Len(CTT->CTT_CUSTO)), Len(CTT->CTT_CUSTO))  
	
	#IFDEF TOP
		If TcSrvType() <> "AS/400"
			If lMA330Filtra
				cFiltro := ExecBlock("M330FCC",.F.,.F., "CQ3")
				If ValType(cFiltro) <> "C"
					cFiltro := ""
				Else
					cFiltro += If(Right(cFiltro, 3) <> "AND", " AND ", " ")
				EndIf
			Else
				cFiltro := ""
			EndIf   
			
			For nCont := 1 To 5
				// Verifica se moeda devera ser considerada 	
				If nCont # 1 .And. !(Str(nCont,1,0) $ cMoeda330C)
                	Loop
   				EndIf
			//-- Somo o saldo atual DEBITO - CREDITO
				cQuery := "SELECT SUM(CQ3.CQ3_DEBITO) DEBITO, SUM(CQ3.CQ3_CREDIT) CREDITO "
				cQuery += " FROM "+RetSqlName("CQ3")+" CQ3 "
				cQuery += " WHERE EXISTS(SELECT CT1_CONTA FROM "+RetSqlName("CT1") 
				cQuery += " WHERE CT1_FILIAL = '" + xFilial("CT1") + "' AND "
					//-- Considera grupo na filtragem caso tenha conteudo definido
					If !Empty(cGrupo) .And. lGrupo
						cQuery += " CT1_GRUPO = '"+cGrupo+"' AND "					
					EndIf  
				cQuery += "CT1_CONTA = CQ3.CQ3_CONTA AND D_E_L_E_T_ = ' ') AND "
				If !lCusEmp	
					cQuery += "	CQ3.CQ3_FILIAL ='"+xFilial("CQ3")+"' AND "
				Else
					If FWSM0Layout() <> "FF"
						cQuery += cFuncSubs+"(CQ3.CQ3_FILIAL,1,"+cvaltochar(len(fwcodemp()))+") = '"+FwCodEmp()+"' And "
					EndIf
				EndIf
				cQuery += " CQ3.CQ3_CCUSTO = '" + cCod + "' AND "
				Do Case
					Case !Empty(cCTBIni) .And. !Empty(cCTBFim)
						cQuery += " (CQ3.CQ3_CONTA < '"+cCTBIni+"'  OR "								
						cQuery += " CQ3.CQ3_CONTA > '"+cCTBFim+"')  AND "					
					Case !Empty(cCTBIni) .And. Empty(cCTBFim)
						cQuery += " CQ3.CQ3_CONTA < '"+cCTBIni+"'  AND "													
					Case Empty(cCTBIni) .And. !Empty(cCTBFim)
						cQuery += " CQ3.CQ3_CONTA > '"+cCTBFim+"'  AND "					
				EndCase
				cQuery += If(! Empty(cFiltro), cFiltro, "")	  
				cQuery += "CQ3.CQ3_MOEDA ='"+StrZero(nCont,2)+"' AND "
				cQuery += "CQ3.CQ3_TPSALD ='1' AND "
				cQuery += "CQ3.D_E_L_E_T_ = ' ' AND "
				cQuery += "CQ3.CQ3_DATA > '"+DTOS(dInicio-1)+"' AND "
				cQuery += "CQ3.CQ3_DATA < '"+DTOS(dData+1)+"'"
				
				dbUseArea(.T.,"TOPCONN",TcGenQry(,,cQuery),"SLDATU",.T.,.F.)
				aSaldos[nCont] 	+= (SLDATU->DEBITO - SLDATU->CREDITO)
				DbCloseArea()					 
			Next     
	    Else
	#ENDIF	       
			dbSelectArea("CTT")
			dbSetOrder(1)
			dbSeek(xFilial("CTT")+cCod)
			While !Eof() .And. xFilial("CTT")+cCod == CTT_FILIAL+CTT_CUSTO
				//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴커
				//� Filtro de contas a serem inibidas (contas de transferencia)  �
				//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴켸
				For nCont := 1 To 5
					// Verifica se moeda devera ser considerada 	
					If nCont # 1 .And. !(Str(nCont,1,0) $ cMoeda330C)
						Loop
					EndIf
					// a tabela CT3 foi descontinuada na vers�o 12, sendo substituida pela tabela CQ3, porem, as fun寤es permanecem com o mesmo nome pelo legado
					aSaldos[nCont] 	+= MovMesCT3(	CTT->CTT_CUSTO,CTT->CTT_CUSTO	,;
													Repl(" ", Len(CT1->CT1_CONTA))	,;
													Repl("Z", Len(CT1->CT1_CONTA))	,;
													dData,StrZero(nCont,2),"1"		,;
													cCTBIni,cCTBFim)
				Next nCont
				dbSelectArea("CTT")
				dbSetOrder(1)
				dbSkip()
			EndDo             
	#IFDEF TOP
		EndIf
	#ENDIF
EndIf

Return aSaldos



Static Function TTFimComMO(aCusto)
Local nV,nX,aVFim[5],aCM[5],nMultiplic := 1
Local bBloco := { |nV,nX| Trim(nV)+Str(nX,1) }
Local nDec:=Set(3,8)
Local aArea:=GetArea()  
Private cMoeda330C := SuperGetMv('MV_MOEDACM',.F.,"2345")
PRIVATE lCusFil    := AllTrim(SuperGetMV('MV_CUSFIL' ,.F.,"A")) == "F"
PRIVATE lCusEmp    := AllTrim(SuperGetMv('MV_CUSFIL' ,.F.,"A")) == "E"

If lCusFil .Or. lCusEmp
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� Posiciona no local a ser atualizado                   �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	dbSelectArea("TRT")
	If !MsSeek(If(lCusEmp,Space(Len(cFilAnt)),cFilAnt)+SB2->B2_COD)
		CriaTRT(If(lCusEmp,Space(Len(cFilAnt)),cFilAnt),SB2->B2_COD)
	EndIf
	RecLock("TRT",.F.)
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� Pega o custo do campo e soma o custo da entrada       �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	If aCusto <> NIL
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//� Pega o custo do campo e soma o custo da entrada       �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		For nX := 1 to 5
			// Verifica se moeda devera ser considerada 	
			If nx # 1 .And. !(Str(nx,1,0) $ cMoeda330C)
               	Loop
			EndIf
			aVfim[nX] := &(Eval(bBloco,"TRT->TRB_VFIM",nX)) + aCusto[nX]
		Next nX
	EndIf
	Replace TRB_VFIM1 With aVFim[01]
	If "2" $ cMoeda330C
		Replace TRB_VFIM2 With aVFim[02]
	EndIf
	If "3" $ cMoeda330C	
		Replace TRB_VFIM3 With aVFim[03]
	EndIf
	If "4" $ cMoeda330C
		Replace TRB_VFIM4 With aVFim[04]
	EndIf
	If "5" $ cMoeda330C
		Replace TRB_VFIM5 With aVFim[05]
	EndIf
	MsUnlock()
EndIf
Set(3,nDec)
RestArea(aArea)
Return
	


Static Function TTFimQtdMO()
LOCAL nV,nX,aVFim[5],aCM[5],nMultiplic := 1
LOCAL bBloco := { |nV,nX| Trim(nV)+Str(nX,1) }
LOCAL nDec:=Set(3,8)
LOCAL aArea:=GetArea()                          
Private cMoeda330C := SuperGetMv('MV_MOEDACM',.F.,"2345")
PRIVATE lCusFil    := AllTrim(SuperGetMV('MV_CUSFIL' ,.F.,"A")) == "F"
PRIVATE lCusEmp    := AllTrim(SuperGetMv('MV_CUSFIL' ,.F.,"A")) == "E"

If lCusFil .Or. lCusEmp
	//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	//� Posiciona no local a ser atualizado                   �
	//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
	dbSelectArea("TRT")
	If !MsSeek(If(lCusEmp,Space(Len(cFilAnt)),cFilAnt)+SB2->B2_COD)
		CriaTRT(If(lCusEmp,Space(Len(cFilAnt)),cFilAnt),SB2->B2_COD)
	EndIf
	RecLock("TRT",.F.)
	Replace TRB_QFIM  With TRB_QFIM + SB2->B2_QFIM
	aCM[01] := TRB_CM1
	aCM[02] := TRB_CM2
	aCM[03] := TRB_CM3
	aCM[04] := TRB_CM4
	aCM[05] := TRB_CM5
	For nX := 1 to 5
		// Verifica se moeda devera ser considerada 	
		If nx # 1 .And. !(Str(nx,1,0) $ cMoeda330C)
           	Loop
    	EndIf
		//旼컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		//� Pega o custo final do campo correto                   �
		//읕컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴컴�
		aVfim[nX] := &(Eval(bBloco,"TRT->TRB_VFIM",nX))
		aCM[nX]   := aVFIM[nX]/ABS(TRB_QFIM)
	Next nX
	Replace TRB_CM1 With aCM[01]
	If "2" $ cMoeda330C
		Replace TRB_CM2 With aCM[02]
	EndIf
	If "3" $ cMoeda330C
		Replace TRB_CM3 With aCM[03]
	EndIf
	If "4" $ cMoeda330C
		Replace TRB_CM4 With aCM[04]
	EndIf
	If "5" $ cMoeda330C
		Replace TRB_CM5 With aCM[05]
	EndIf
	MsUnlock()
EndIf
Set(3,nDec)
RestArea(aArea)

Return

