#include 'protheus.ch'
#include 'parmtype.ch'

user function MTA010OK()

_cRet := .T.
dbSelectArea("SG1")
dbSetOrder(1)
If dbSeek(xFilial()+SB1->B1_COD)
	   MsgInfo("Produto possui Estrutura e n�o pode ser excluido!","Hope")
	  _cRet:=.F.
else
		_cRet:=.T.
endif  

Return(_cRet)       
	